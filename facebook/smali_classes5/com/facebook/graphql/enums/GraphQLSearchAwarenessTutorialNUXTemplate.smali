.class public final enum Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

.field public static final enum BASIC:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

.field public static final enum CARD:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

.field public static final enum INTRO:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 740003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    .line 740004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    const-string v1, "BASIC"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->BASIC:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    .line 740005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    const-string v1, "INTRO"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->INTRO:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    .line 740006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    const-string v1, "CARD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->CARD:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    .line 740007
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->BASIC:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->INTRO:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->CARD:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740008
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;
    .locals 1

    .prologue
    .line 740009
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    .line 740010
    :goto_0
    return-object v0

    .line 740011
    :cond_1
    const-string v0, "BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740012
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->BASIC:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    goto :goto_0

    .line 740013
    :cond_2
    const-string v0, "INTRO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740014
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->INTRO:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    goto :goto_0

    .line 740015
    :cond_3
    const-string v0, "CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740016
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->CARD:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    goto :goto_0

    .line 740017
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;
    .locals 1

    .prologue
    .line 740018
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;
    .locals 1

    .prologue
    .line 740019
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    return-object v0
.end method
