.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum COMPANY_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum CUSTOM:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum DOB:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum FIRST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum GENDER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum JOB_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum LAST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum MARITIAL_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum MESSENGER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum MILITARY_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum POST_CODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum RELATIONSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum STREET_ADDRESS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum WORK_EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum WORK_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

.field public static final enum ZIP:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727892
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727893
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727894
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "COMPANY_NAME"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COMPANY_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727895
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "COUNTRY"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727896
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727897
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "DOB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->DOB:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727898
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "EMAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727899
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "FIRST_NAME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FIRST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "FULL_NAME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "GENDER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->GENDER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "JOB_TITLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->JOB_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727903
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "LAST_NAME"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->LAST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727904
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "MARITIAL_STATUS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MARITIAL_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727905
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "PHONE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "POST_CODE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->POST_CODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727907
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "PROVINCE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "RELATIONSHIP_STATUS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->RELATIONSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "STATE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "STREET_ADDRESS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STREET_ADDRESS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "ZIP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->ZIP:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "WORK_EMAIL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "MILITARY_STATUS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MILITARY_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "WORK_PHONE_NUMBER"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727915
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    const-string v1, "MESSENGER"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727916
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COMPANY_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->DOB:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FIRST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->GENDER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->JOB_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->LAST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MARITIAL_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->POST_CODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->RELATIONSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STREET_ADDRESS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->ZIP:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MILITARY_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
    .locals 1

    .prologue
    .line 727918
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    .line 727919
    :goto_0
    return-object v0

    .line 727920
    :cond_1
    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727921
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727922
    :cond_2
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727923
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->CITY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727924
    :cond_3
    const-string v0, "COMPANY_NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727925
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COMPANY_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727926
    :cond_4
    const-string v0, "COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727928
    :cond_5
    const-string v0, "DOB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727929
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->DOB:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727930
    :cond_6
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727931
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727932
    :cond_7
    const-string v0, "GENDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727933
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->GENDER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727934
    :cond_8
    const-string v0, "FIRST_NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727935
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FIRST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727936
    :cond_9
    const-string v0, "FULL_NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727938
    :cond_a
    const-string v0, "JOB_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->JOB_TITLE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727940
    :cond_b
    const-string v0, "LAST_NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727941
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->LAST_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto :goto_0

    .line 727942
    :cond_c
    const-string v0, "MARITIAL_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727943
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MARITIAL_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727944
    :cond_d
    const-string v0, "PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727945
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727946
    :cond_e
    const-string v0, "POST_CODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->POST_CODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727948
    :cond_f
    const-string v0, "PROVINCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PROVINCE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727950
    :cond_10
    const-string v0, "RELATIONSHIP_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727951
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->RELATIONSHIP_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727952
    :cond_11
    const-string v0, "STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727953
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STATE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727954
    :cond_12
    const-string v0, "STREET_ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 727955
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STREET_ADDRESS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727956
    :cond_13
    const-string v0, "ZIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 727957
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->ZIP:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727958
    :cond_14
    const-string v0, "WORK_EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 727959
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727960
    :cond_15
    const-string v0, "MILITARY_STATUS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 727961
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MILITARY_STATUS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727962
    :cond_16
    const-string v0, "WORK_PHONE_NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 727963
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->WORK_PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727964
    :cond_17
    const-string v0, "MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 727965
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0

    .line 727966
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
    .locals 1

    .prologue
    .line 727967
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;
    .locals 1

    .prologue
    .line 727968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    return-object v0
.end method
