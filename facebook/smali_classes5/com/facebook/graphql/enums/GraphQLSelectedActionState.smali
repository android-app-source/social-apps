.class public final enum Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSelectedActionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

.field public static final enum NEGATIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

.field public static final enum POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

.field public static final enum UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 740032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 740033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    const-string v1, "NEGATIVE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->NEGATIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 740034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    const-string v1, "UNSELECTED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 740035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    const-string v1, "POSITIVE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 740036
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->NEGATIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
    .locals 1

    .prologue
    .line 740020
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    .line 740021
    :goto_0
    return-object v0

    .line 740022
    :cond_1
    const-string v0, "NEGATIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->NEGATIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    goto :goto_0

    .line 740024
    :cond_2
    const-string v0, "UNSELECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    goto :goto_0

    .line 740026
    :cond_3
    const-string v0, "POSITIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740027
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    goto :goto_0

    .line 740028
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
    .locals 1

    .prologue
    .line 740030
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
    .locals 1

    .prologue
    .line 740029
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    return-object v0
.end method
