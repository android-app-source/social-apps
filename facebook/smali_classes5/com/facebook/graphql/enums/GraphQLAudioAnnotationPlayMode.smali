.class public final enum Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public static final enum AMBIENT:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public static final enum ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 723477
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    const-string v1, "AMBIENT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->AMBIENT:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 723478
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    const-string v1, "ON_EXPAND"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 723479
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->NORMAL:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 723480
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->AMBIENT:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->NORMAL:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1

    .prologue
    .line 723483
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 723484
    :goto_0
    return-object v0

    .line 723485
    :cond_1
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->NORMAL:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    goto :goto_0

    .line 723487
    :cond_2
    const-string v0, "ON_EXPAND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    goto :goto_0

    .line 723489
    :cond_3
    const-string v0, "AMBIENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723490
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->AMBIENT:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    goto :goto_0

    .line 723491
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1

    .prologue
    .line 723482
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1

    .prologue
    .line 723481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    return-object v0
.end method
