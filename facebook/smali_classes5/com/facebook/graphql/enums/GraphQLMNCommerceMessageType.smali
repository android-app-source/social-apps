.class public final enum Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AD_ADMIN_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AD_BUBBLE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AD_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_BOARDINGPASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_CHECKIN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum AUDIO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum BLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum BLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum BUTTON:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum COMMERCE_COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum CONTENT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum FILE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum FORCED_FETCH_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum GENERIC_LEGACY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum GET_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum GIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum IMAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum INITIAL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum INSTANT_GAMES_SCORE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum INSTANT_GAMES_SHARE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum LINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum LOCATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum MESSENGER_BOT_BASE_GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum MESSENGER_TEAM_BOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum NON_AD:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum REFERER_PROMO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RETAIL_PRODUCT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_INTENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_NO_DRIVER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_ORDER_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_REENGAGEMENT_FIRST_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_REQUESTED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_SIGNUP:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum RIDE_WELCOME:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum ROBOT_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum SENDER_ACTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum TEMPLATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum UNBLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum UNBLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum UNLINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

.field public static final enum WEAK_CONSENT_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728543
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728544
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728545
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728546
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728547
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AUDIO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728548
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "FILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FILE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728549
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "ROBOT_TEXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->ROBOT_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728550
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "LOCATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728551
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "FORCED_FETCH_MESSAGE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FORCED_FETCH_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728552
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RETAIL_PRODUCT_SUBSCRIPTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PRODUCT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728553
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RETAIL_PROMOTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728554
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RETAIL_CANCELLATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728555
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RETAIL_SHIPMENT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "SHIPMENT_TRACKING_EVENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_CHECKIN_REMINDER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_BOARDING_PASS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728559
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_CHECKIN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_BOARDINGPASS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDINGPASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_UPDATE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AIRLINE_ITINERARY"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "CONTENT_SUBSCRIPTION"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->CONTENT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728564
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AD_ADMIN_TEXT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_ADMIN_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728565
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AD_TEXT"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728566
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "AD_BUBBLE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_BUBBLE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728567
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "NON_AD"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->NON_AD:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728568
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728569
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "OTHER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->OTHER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728570
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "GENERIC"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728571
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "BUTTON"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728572
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RECEIPT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728573
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "GENERIC_LEGACY"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC_LEGACY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728574
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_INTENT"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_INTENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728575
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_SIGNUP"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_SIGNUP:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728576
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_WELCOME"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_WELCOME:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728577
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_ORDER_CONFIRMATION"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ORDER_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728578
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_REQUESTED"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REQUESTED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728579
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_DRIVER_ON_THE_WAY"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728580
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_NO_DRIVER"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_NO_DRIVER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728581
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_DRIVER_ARRIVING"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728582
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_DRIVER_CANCELED"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728583
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_RIDER_CANCELED"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728584
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_COMPLETE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728585
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_ADMIN_MESSAGE"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728586
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_RECEIPT"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728587
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "RIDE_REENGAGEMENT_FIRST_RIDE"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REENGAGEMENT_FIRST_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728588
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "GET_RIDE"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GET_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728589
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "GIVE_RIDE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728590
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "REFERER_PROMO"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->REFERER_PROMO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728591
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "LINK"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728592
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "UNLINK"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNLINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728593
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "BLOCK_ALL"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728594
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "UNBLOCK_ALL"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728595
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "BLOCK_PROMOTION"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728596
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "UNBLOCK_PROMOTION"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728597
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "INITIAL_PROMOTION"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INITIAL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728598
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "WEAK_CONSENT_ADMIN_MESSAGE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->WEAK_CONSENT_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728599
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "MESSENGER_TEAM_BOT_MESSAGE"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_TEAM_BOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728600
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "INSTANT_GAMES_SHARE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SHARE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728601
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "INSTANT_GAMES_SCORE"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SCORE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728602
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "LIST"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728603
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "COMMERCE_COMPACT_LIST"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMMERCE_COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728604
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "MESSENGER_BOT_BASE_GENERIC"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_BOT_BASE_GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728605
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "COMPACT_LIST"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728606
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "SENDER_ACTION"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SENDER_ACTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728607
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    const-string v1, "TEMPLATE"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEMPLATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728608
    const/16 v0, 0x41

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AUDIO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FILE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->ROBOT_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FORCED_FETCH_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PRODUCT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDINGPASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->CONTENT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_ADMIN_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_BUBBLE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->NON_AD:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->OTHER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC_LEGACY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_INTENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_SIGNUP:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_WELCOME:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ORDER_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REQUESTED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_NO_DRIVER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REENGAGEMENT_FIRST_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GET_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->REFERER_PROMO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNLINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INITIAL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->WEAK_CONSENT_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_TEAM_BOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SHARE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SCORE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMMERCE_COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_BOT_BASE_GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SENDER_ACTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEMPLATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728609
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;
    .locals 2

    .prologue
    .line 728370
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    .line 728371
    :goto_0
    return-object v0

    .line 728372
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 728373
    packed-switch v0, :pswitch_data_0

    .line 728374
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728375
    :pswitch_1
    const-string v0, "AIRLINE_CHECKIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728376
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728377
    :cond_2
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->OTHER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728379
    :cond_3
    const-string v0, "MESSENGER_TEAM_BOT_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728380
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_TEAM_BOT_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728381
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728382
    :pswitch_2
    const-string v0, "BLOCK_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728384
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728385
    :pswitch_3
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728387
    :cond_6
    const-string v0, "MESSENGER_BOT_BASE_GENERIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->MESSENGER_BOT_BASE_GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728389
    :cond_7
    const-string v0, "COMMERCE_COMPACT_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMMERCE_COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728391
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto :goto_0

    .line 728392
    :pswitch_4
    const-string v0, "RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 728393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728394
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728395
    :pswitch_5
    const-string v0, "LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 728396
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LOCATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728397
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728398
    :pswitch_6
    const-string v0, "AIRLINE_CHECKIN_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 728399
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_CHECKIN_REMINDER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728400
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728401
    :pswitch_7
    const-string v0, "ROBOT_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 728402
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->ROBOT_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728403
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728404
    :pswitch_8
    const-string v0, "CONTENT_SUBSCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 728405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->CONTENT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728406
    :cond_d
    const-string v0, "RIDE_INTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 728407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_INTENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728408
    :cond_e
    const-string v0, "RIDE_REENGAGEMENT_FIRST_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 728409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REENGAGEMENT_FIRST_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728410
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728411
    :pswitch_9
    const-string v0, "RIDE_RECEIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 728412
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RECEIPT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728413
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728414
    :pswitch_a
    const-string v0, "INITIAL_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 728415
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INITIAL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728416
    :cond_11
    const-string v0, "WEAK_CONSENT_ADMIN_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 728417
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->WEAK_CONSENT_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728418
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728419
    :pswitch_b
    const-string v0, "RETAIL_SHIPMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 728420
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_SHIPMENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728421
    :cond_13
    const-string v0, "RIDE_SIGNUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 728422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_SIGNUP:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728423
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728424
    :pswitch_c
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 728425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728426
    :cond_15
    const-string v0, "RIDE_NO_DRIVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 728427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_NO_DRIVER:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728428
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728429
    :pswitch_d
    const-string v0, "NON_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 728430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->NON_AD:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728431
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728432
    :pswitch_e
    const-string v0, "SENDER_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 728433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SENDER_ACTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728434
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728435
    :pswitch_f
    const-string v0, "RETAIL_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 728436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728437
    :cond_19
    const-string v0, "UNBLOCK_ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 728438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728439
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728440
    :pswitch_10
    const-string v0, "SHIPMENT_TRACKING_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 728441
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->SHIPMENT_TRACKING_EVENT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728442
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728443
    :pswitch_11
    const-string v0, "AUDIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 728444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AUDIO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728445
    :cond_1c
    const-string v0, "RETAIL_CANCELLATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 728446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_CANCELLATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728447
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728448
    :pswitch_12
    const-string v0, "UNBLOCK_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 728449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNBLOCK_PROMOTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728450
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728451
    :pswitch_13
    const-string v0, "AIRLINE_ITINERARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 728452
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_ITINERARY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728453
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728454
    :pswitch_14
    const-string v0, "RIDE_ORDER_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 728455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ORDER_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728456
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728457
    :pswitch_15
    const-string v0, "GENERIC_LEGACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 728458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC_LEGACY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728459
    :cond_21
    const-string v0, "RIDE_REQUESTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 728460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_REQUESTED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728461
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728462
    :pswitch_16
    const-string v0, "RETAIL_PRODUCT_SUBSCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 728463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RETAIL_PRODUCT_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728464
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728465
    :pswitch_17
    const-string v0, "RIDE_RIDER_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 728466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_RIDER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728467
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728468
    :pswitch_18
    const-string v0, "AIRLINE_BOARDINGPASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 728469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDINGPASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728470
    :cond_25
    const-string v0, "RIDE_DRIVER_CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 728471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_CANCELED:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728472
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728473
    :pswitch_19
    const-string v0, "AIRLINE_BOARDING_PASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 728474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_BOARDING_PASS:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728475
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728476
    :pswitch_1a
    const-string v0, "FILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 728477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FILE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728478
    :cond_28
    const-string v0, "AD_BUBBLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 728479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_BUBBLE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728480
    :cond_29
    const-string v0, "LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 728481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728482
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728483
    :pswitch_1b
    const-string v0, "IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 728484
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728485
    :cond_2b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728486
    :pswitch_1c
    const-string v0, "AIRLINE_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 728487
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AIRLINE_UPDATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728488
    :cond_2c
    const-string v0, "GET_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 728489
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GET_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728490
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728491
    :pswitch_1d
    const-string v0, "GENERIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 728492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728493
    :cond_2e
    const-string v0, "GIVE_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 728494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->GIVE_RIDE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728495
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728496
    :pswitch_1e
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 728497
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728498
    :cond_30
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728499
    :pswitch_1f
    const-string v0, "RIDE_DRIVER_ON_THE_WAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 728500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ON_THE_WAY:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728501
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728502
    :pswitch_20
    const-string v0, "REFERER_PROMO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 728503
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->REFERER_PROMO:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728504
    :cond_32
    const-string v0, "UNLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 728505
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNLINK:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728506
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728507
    :pswitch_21
    const-string v0, "AD_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 728508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728509
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728510
    :pswitch_22
    const-string v0, "FORCED_FETCH_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 728511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->FORCED_FETCH_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728512
    :cond_35
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728513
    :pswitch_23
    const-string v0, "TEMPLATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 728514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->TEMPLATE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728515
    :cond_36
    const-string v0, "INSTANT_GAMES_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 728516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SHARE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728517
    :cond_37
    const-string v0, "INSTANT_GAMES_SCORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 728518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->INSTANT_GAMES_SCORE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728519
    :cond_38
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728520
    :pswitch_24
    const-string v0, "RIDE_WELCOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 728521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_WELCOME:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728522
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728523
    :pswitch_25
    const-string v0, "AD_ADMIN_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 728524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->AD_ADMIN_TEXT:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728525
    :cond_3a
    const-string v0, "BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 728526
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728527
    :cond_3b
    const-string v0, "RIDE_COMPLETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 728528
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_COMPLETE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728529
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728530
    :pswitch_26
    const-string v0, "COMPACT_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 728531
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->COMPACT_LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728532
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728533
    :pswitch_27
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 728534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->LIST:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728535
    :cond_3e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728536
    :pswitch_28
    const-string v0, "RIDE_DRIVER_ARRIVING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 728537
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_DRIVER_ARRIVING:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728538
    :cond_3f
    const-string v0, "RIDE_ADMIN_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 728539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->RIDE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728540
    :cond_40
    const-string v0, "BLOCK_ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 728541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->BLOCK_ALL:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    .line 728542
    :cond_41
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_1a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_23
        :pswitch_0
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_28
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;
    .locals 1

    .prologue
    .line 728369
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;
    .locals 1

    .prologue
    .line 728368
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    return-object v0
.end method
