.class public final enum Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

.field public static final enum DASHBOARD_UPSELL:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

.field public static final enum INFORMATIONAL_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

.field public static final enum NEARBY_FRIENDS_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 729474
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 729475
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const-string v1, "DASHBOARD_UPSELL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->DASHBOARD_UPSELL:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 729476
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const-string v1, "NEARBY_FRIENDS_NUX"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->NEARBY_FRIENDS_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 729477
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    const-string v1, "INFORMATIONAL_NUX"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->INFORMATIONAL_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 729478
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->DASHBOARD_UPSELL:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->NEARBY_FRIENDS_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->INFORMATIONAL_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729479
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .locals 1

    .prologue
    .line 729480
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 729481
    :goto_0
    return-object v0

    .line 729482
    :cond_1
    const-string v0, "DASHBOARD_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->DASHBOARD_UPSELL:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    goto :goto_0

    .line 729484
    :cond_2
    const-string v0, "NEARBY_FRIENDS_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729485
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->NEARBY_FRIENDS_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    goto :goto_0

    .line 729486
    :cond_3
    const-string v0, "INFORMATIONAL_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729487
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->INFORMATIONAL_NUX:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    goto :goto_0

    .line 729488
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .locals 1

    .prologue
    .line 729489
    const-class v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .locals 1

    .prologue
    .line 729490
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    return-object v0
.end method
