.class public final enum Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "MARRIED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "ENGAGED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "STARTED_JOB"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "GRADUATED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    const-string v1, "OTHER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->OTHER:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728160
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->OTHER:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;
    .locals 1

    .prologue
    .line 728164
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 728165
    :goto_0
    return-object v0

    .line 728166
    :cond_1
    const-string v0, "MARRIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0

    .line 728168
    :cond_2
    const-string v0, "ENGAGED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0

    .line 728170
    :cond_3
    const-string v0, "STARTED_JOB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0

    .line 728172
    :cond_4
    const-string v0, "GRADUATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0

    .line 728174
    :cond_5
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->OTHER:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0

    .line 728176
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;
    .locals 1

    .prologue
    .line 728162
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;
    .locals 1

    .prologue
    .line 728161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    return-object v0
.end method
