.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public static final enum CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public static final enum DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public static final enum REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public static final enum SCHEDULEED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737481
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737482
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737483
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737484
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    const-string v1, "SCHEDULEED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->SCHEDULEED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737485
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    const-string v1, "REQUESTED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737486
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->SCHEDULEED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;
    .locals 1

    .prologue
    .line 737468
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 737469
    :goto_0
    return-object v0

    .line 737470
    :cond_1
    const-string v0, "CANCELLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    goto :goto_0

    .line 737472
    :cond_2
    const-string v0, "DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737473
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    goto :goto_0

    .line 737474
    :cond_3
    const-string v0, "SCHEDULEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737475
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->SCHEDULEED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    goto :goto_0

    .line 737476
    :cond_4
    const-string v0, "REQUESTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    goto :goto_0

    .line 737478
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;
    .locals 1

    .prologue
    .line 737479
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;
    .locals 1

    .prologue
    .line 737480
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    return-object v0
.end method
