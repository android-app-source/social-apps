.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum INJECTED:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum STALE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum UNREAD:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "INJECTED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->INJECTED:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->PENDING:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "STALE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->STALE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "UNREAD"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNREAD:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    const-string v1, "GROUP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->GROUP:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728955
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->INJECTED:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->PENDING:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->STALE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNREAD:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->GROUP:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728971
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;
    .locals 1

    .prologue
    .line 728958
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    .line 728959
    :goto_0
    return-object v0

    .line 728960
    :cond_1
    const-string v0, "INJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728961
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->INJECTED:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0

    .line 728962
    :cond_2
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728963
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->PENDING:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0

    .line 728964
    :cond_3
    const-string v0, "STALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728965
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->STALE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0

    .line 728966
    :cond_4
    const-string v0, "UNREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728967
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNREAD:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0

    .line 728968
    :cond_5
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728969
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->GROUP:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0

    .line 728970
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;
    .locals 1

    .prologue
    .line 728957
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;
    .locals 1

    .prologue
    .line 728956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    return-object v0
.end method
