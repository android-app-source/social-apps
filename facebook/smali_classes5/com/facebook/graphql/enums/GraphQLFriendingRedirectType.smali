.class public final enum Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

.field public static final enum PROFILE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

.field public static final enum REQUESTS:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 726700
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    .line 726701
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    .line 726702
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    const-string v1, "REQUESTS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->REQUESTS:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    .line 726703
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->REQUESTS:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726704
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;
    .locals 1

    .prologue
    .line 726705
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    .line 726706
    :goto_0
    return-object v0

    .line 726707
    :cond_1
    const-string v0, "PROFILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    goto :goto_0

    .line 726709
    :cond_2
    const-string v0, "REQUESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726710
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->REQUESTS:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    goto :goto_0

    .line 726711
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;
    .locals 1

    .prologue
    .line 726712
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;
    .locals 1

    .prologue
    .line 726713
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    return-object v0
.end method
