.class public final enum Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum ACTIVITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum APPLICATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum BOOKS_MAGAZINES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum BRANDS_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum COMPANY_ORGANIZATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum DEPRECATED_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum DRINKABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum DRINKABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum EDIBLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum EDIBLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum FOOD_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum GEOGRAPHY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum GEO_HUB:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum GOINGTOABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum GOINGTOABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum LISTENABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum LISTENABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum LOCAL_ATTRIBUTES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum LOCAL_TOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum MOVIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum MUSIC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__ARTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__BAR:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__BREAKFAST:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__BRUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__CASUAL_DINING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__COFFEE_SHOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__DESSERT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__DINNER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__FAST_FOOD:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__GROCERY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__HOTEL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__LUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__NIGHTLIFE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__OUTDOORS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__PIZZA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__RESTAURANT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NEARBY_PLACES__SIGHTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NON_REVIEWABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__ARTS_ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__AUTOMOTIVE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__CITY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__COMMUNITY_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__LODGING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__MEDICAL_HEALTH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__PLACE_TO_EAT_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__PUBLIC_STRUCTURE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__REGION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__RELIGIOUS_CENTER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__SCHOOL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__SPA_BEAUTY_PERSONAL_CARE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__SPORTS_RECREATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__TRAVEL_TRANSPORTATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum P0__WORKPLACE_OFFICE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PLACE_TOPICS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PLAYABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PLAYABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PQI_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum PROFESSIONAL_SERVICE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum READABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum READABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum REQUEST_TIME_STYLE_SERVICE_PROVIDERS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum SPORTS_TEAMS_LEAGUES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum SUPPORTABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum SUPPORTABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TOO_BROAD_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TOO_BROAD_CE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TOPIC_LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TRAVELLABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TRAVELLABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum TV:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum V6_PRIMARY_ENTITY_GROUP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum WATCHABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum WATCHABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public static final enum WEBSITE_BLOGS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 576958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "ACTIVITIES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ACTIVITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "APPLICATIONS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->APPLICATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "BOOKS_MAGAZINES"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BOOKS_MAGAZINES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "BRANDS_PRODUCTS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BRANDS_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "CELEBRITIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "COMPANY_ORGANIZATIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->COMPANY_ORGANIZATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576965
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "DEPRECATED_CATEGORIES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DEPRECATED_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576966
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "DRINKABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576967
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "DRINKABLE_EXPERIMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576968
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "EDIBLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576969
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "EDIBLE_EXPERIMENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "ENTERTAINMENT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "FOOD_DRINK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->FOOD_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "GEO_HUB"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEO_HUB:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "GEOGRAPHY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEOGRAPHY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "GOINGTOABLE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "GOINGTOABLE_EXPERIMENT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "LISTENABLE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "LISTENABLE_EXPERIMENT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "LOCAL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "LOCAL_ATTRIBUTES"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_ATTRIBUTES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "LOCAL_TOP"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_TOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "MOVIES"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "MUSIC"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__ARTS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ARTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__BAR"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BAR:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__BREAKFAST"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BREAKFAST:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__BRUNCH"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BRUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__CASUAL_DINING"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__CASUAL_DINING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__COFFEE_SHOP"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__COFFEE_SHOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__DESSERT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DESSERT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__DINNER"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DINNER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__ENTERTAINMENT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__FAST_FOOD"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__FAST_FOOD:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__GROCERY"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__GROCERY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__HOTEL"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__HOTEL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__LUNCH"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__LUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__NIGHTLIFE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__NIGHTLIFE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__OUTDOORS"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__OUTDOORS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__PIZZA"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PIZZA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__PROFESSIONAL_SERVICES"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__RESTAURANT"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__RESTAURANT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__SHOPPING"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NEARBY_PLACES__SIGHTS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SIGHTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NON_REVIEWABLE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "OTHER"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__ARTS_ENTERTAINMENT"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__ARTS_ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577008
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__AUTOMOTIVE"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__AUTOMOTIVE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577009
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__CITY"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__CITY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577010
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__COMMUNITY_GOVERNMENT"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__COMMUNITY_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577011
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__LODGING"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__LODGING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577012
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__MEDICAL_HEALTH"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__MEDICAL_HEALTH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577013
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__OTHER"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577014
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__PLACE_TO_EAT_DRINK"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PLACE_TO_EAT_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577015
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__PROFESSIONAL_SERVICES"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577016
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__PUBLIC_STRUCTURE"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PUBLIC_STRUCTURE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577017
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__REGION"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__REGION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577018
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__RELIGIOUS_CENTER"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RELIGIOUS_CENTER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__RESIDENCE"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__SCHOOL"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SCHOOL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__SHOPPING"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__SPA_BEAUTY_PERSONAL_CARE"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPA_BEAUTY_PERSONAL_CARE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__SPORTS_RECREATION"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPORTS_RECREATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__TRAVEL_TRANSPORTATION"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__TRAVEL_TRANSPORTATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0__WORKPLACE_OFFICE"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__WORKPLACE_OFFICE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "P0_CATEGORIES"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PAGE_CATEGORIES"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PLACE_TOPICS"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLACE_TOPICS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PLAYABLE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PLAYABLE_EXPERIMENT"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "READABLE"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "READABLE_EXPERIMENT"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "SPORTS_TEAMS_LEAGUES"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SPORTS_TEAMS_LEAGUES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "SUPPORTABLE"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "SUPPORTABLE_EXPERIMENT"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TOO_BROAD_CATEGORIES"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TOPIC_LOCAL"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOPIC_LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TRAVELLABLE"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TRAVELLABLE_EXPERIMENT"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TV"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TV:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "WATCHABLE"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "WATCHABLE_EXPERIMENT"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "WEBSITE_BLOGS"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WEBSITE_BLOGS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "TOO_BROAD_CE"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PROFESSIONAL_SERVICE_PROVIDER"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PROFESSIONAL_SERVICE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577046
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "REQUEST_TIME_STYLE_SERVICE_PROVIDERS"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->REQUEST_TIME_STYLE_SERVICE_PROVIDERS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "PQI_PRICE_RANGE"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PQI_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const-string v1, "V6_PRIMARY_ENTITY_GROUP"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->V6_PRIMARY_ENTITY_GROUP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 577049
    const/16 v0, 0x5b

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ACTIVITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->APPLICATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BOOKS_MAGAZINES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BRANDS_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->COMPANY_ORGANIZATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DEPRECATED_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->FOOD_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEO_HUB:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEOGRAPHY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_ATTRIBUTES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_TOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ARTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BAR:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BREAKFAST:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BRUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__CASUAL_DINING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__COFFEE_SHOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DESSERT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DINNER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__FAST_FOOD:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__GROCERY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__HOTEL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__LUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__NIGHTLIFE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__OUTDOORS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PIZZA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__RESTAURANT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SIGHTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__ARTS_ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__AUTOMOTIVE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__CITY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__COMMUNITY_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__LODGING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__MEDICAL_HEALTH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PLACE_TO_EAT_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PUBLIC_STRUCTURE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__REGION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RELIGIOUS_CENTER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SCHOOL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPA_BEAUTY_PERSONAL_CARE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPORTS_RECREATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__TRAVEL_TRANSPORTATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__WORKPLACE_OFFICE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLACE_TOPICS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SPORTS_TEAMS_LEAGUES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOPIC_LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TV:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WEBSITE_BLOGS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PROFESSIONAL_SERVICE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->REQUEST_TIME_STYLE_SERVICE_PROVIDERS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PQI_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->V6_PRIMARY_ENTITY_GROUP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 576957
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 2

    .prologue
    .line 576717
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 576718
    :goto_0
    return-object v0

    .line 576719
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 576720
    packed-switch v0, :pswitch_data_0

    .line 576721
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576722
    :pswitch_1
    const-string v0, "TV"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 576723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TV:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576724
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576725
    :pswitch_2
    const-string v0, "NEARBY_PLACES__NIGHTLIFE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 576726
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__NIGHTLIFE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576727
    :cond_3
    const-string v0, "NEARBY_PLACES__PIZZA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 576728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PIZZA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576729
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576730
    :pswitch_3
    const-string v0, "EDIBLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 576731
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576732
    :cond_5
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 576733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576734
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576735
    :pswitch_4
    const-string v0, "NEARBY_PLACES__CASUAL_DINING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 576736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__CASUAL_DINING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576737
    :cond_7
    const-string v0, "REQUEST_TIME_STYLE_SERVICE_PROVIDERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 576738
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->REQUEST_TIME_STYLE_SERVICE_PROVIDERS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto :goto_0

    .line 576739
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576740
    :pswitch_5
    const-string v0, "DRINKABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 576741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576742
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576743
    :pswitch_6
    const-string v0, "LOCAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 576744
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576745
    :cond_a
    const-string v0, "LOCAL_TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 576746
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_TOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576747
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576748
    :pswitch_7
    const-string v0, "NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 576749
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE_CATEGORIES_FOR_UNOWNED_PAGES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576750
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576751
    :pswitch_8
    const-string v0, "P0__OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 576752
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__OTHER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576753
    :cond_d
    const-string v0, "P0__SPA_BEAUTY_PERSONAL_CARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 576754
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPA_BEAUTY_PERSONAL_CARE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576755
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576756
    :pswitch_9
    const-string v0, "GOINGTOABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 576757
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576758
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576759
    :pswitch_a
    const-string v0, "GEO_HUB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 576760
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEO_HUB:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576761
    :cond_10
    const-string v0, "P0__REGION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 576762
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__REGION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576763
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576764
    :pswitch_b
    const-string v0, "LISTENABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 576765
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576766
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576767
    :pswitch_c
    const-string v0, "NEARBY_PLACES__BAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 576768
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BAR:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576769
    :cond_13
    const-string v0, "P0__SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 576770
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SCHOOL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576771
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576772
    :pswitch_d
    const-string v0, "PLAYABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 576773
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576774
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576775
    :pswitch_e
    const-string v0, "NEARBY_PLACES__DESSERT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 576776
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DESSERT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576777
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576778
    :pswitch_f
    const-string v0, "NEARBY_PLACES__DINNER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 576779
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__DINNER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576780
    :cond_17
    const-string v0, "READABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 576781
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576782
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576783
    :pswitch_10
    const-string v0, "NEARBY_PLACES__BREAKFAST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 576784
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BREAKFAST:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576785
    :cond_19
    const-string v0, "P0__ARTS_ENTERTAINMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 576786
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__ARTS_ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576787
    :cond_1a
    const-string v0, "P0__RELIGIOUS_CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 576788
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RELIGIOUS_CENTER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576789
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576790
    :pswitch_11
    const-string v0, "NEARBY_PLACES__RESTAURANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 576791
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__RESTAURANT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576792
    :cond_1c
    const-string v0, "TOPIC_LOCAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 576793
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOPIC_LOCAL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576794
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576795
    :pswitch_12
    const-string v0, "P0__COMMUNITY_GOVERNMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 576796
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__COMMUNITY_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576797
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576798
    :pswitch_13
    const-string v0, "SUPPORTABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 576799
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576800
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576801
    :pswitch_14
    const-string v0, "NEARBY_PLACES__ENTERTAINMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 576802
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576803
    :cond_20
    const-string v0, "NEARBY_PLACES__HOTEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 576804
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__HOTEL:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576805
    :cond_21
    const-string v0, "TRAVELLABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 576806
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576807
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576808
    :pswitch_15
    const-string v0, "GEOGRAPHY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 576809
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GEOGRAPHY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576810
    :cond_23
    const-string v0, "P0__SPORTS_RECREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 576811
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SPORTS_RECREATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576812
    :cond_24
    const-string v0, "WATCHABLE_EXPERIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 576813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE_EXPERIMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576814
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576815
    :pswitch_16
    const-string v0, "ACTIVITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 576816
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ACTIVITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576817
    :cond_26
    const-string v0, "NEARBY_PLACES__COFFEE_SHOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 576818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__COFFEE_SHOP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576819
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576820
    :pswitch_17
    const-string v0, "APPLICATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 576821
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->APPLICATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576822
    :cond_28
    const-string v0, "NEARBY_PLACES__LUNCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 576823
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__LUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576824
    :cond_29
    const-string v0, "P0__MEDICAL_HEALTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 576825
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__MEDICAL_HEALTH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576826
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576827
    :pswitch_18
    const-string v0, "CELEBRITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 576828
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576829
    :cond_2b
    const-string v0, "NEARBY_PLACES__BRUNCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 576830
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__BRUNCH:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576831
    :cond_2c
    const-string v0, "P0__TRAVEL_TRANSPORTATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 576832
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__TRAVEL_TRANSPORTATION:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576833
    :cond_2d
    const-string v0, "PROFESSIONAL_SERVICE_PROVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 576834
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PROFESSIONAL_SERVICE_PROVIDER:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576835
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576836
    :pswitch_19
    const-string v0, "V6_PRIMARY_ENTITY_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 576837
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->V6_PRIMARY_ENTITY_GROUP:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576838
    :cond_2f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576839
    :pswitch_1a
    const-string v0, "BOOKS_MAGAZINES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 576840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BOOKS_MAGAZINES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576841
    :cond_30
    const-string v0, "BRANDS_PRODUCTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 576842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->BRANDS_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576843
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576844
    :pswitch_1b
    const-string v0, "P0__CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 576845
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__CITY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576846
    :cond_32
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576847
    :pswitch_1c
    const-string v0, "MOVIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 576848
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576849
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576850
    :pswitch_1d
    const-string v0, "NEARBY_PLACES__FAST_FOOD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 576851
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__FAST_FOOD:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576852
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576853
    :pswitch_1e
    const-string v0, "COMPANY_ORGANIZATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 576854
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->COMPANY_ORGANIZATIONS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576855
    :cond_35
    const-string v0, "FOOD_DRINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 576856
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->FOOD_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576857
    :cond_36
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576858
    :pswitch_1f
    const-string v0, "DEPRECATED_CATEGORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 576859
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DEPRECATED_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576860
    :cond_37
    const-string v0, "EDIBLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 576861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->EDIBLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576862
    :cond_38
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576863
    :pswitch_20
    const-string v0, "DRINKABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 576864
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->DRINKABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576865
    :cond_39
    const-string v0, "NEARBY_PLACES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 576866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576867
    :cond_3a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576868
    :pswitch_21
    const-string v0, "LOCAL_ATTRIBUTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 576869
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LOCAL_ATTRIBUTES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576870
    :cond_3b
    const-string v0, "PLACE_TOPICS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 576871
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLACE_TOPICS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576872
    :cond_3c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576873
    :pswitch_22
    const-string v0, "P0_CATEGORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 576874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576875
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576876
    :pswitch_23
    const-string v0, "NEARBY_PLACES__GROCERY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 576877
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__GROCERY:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576878
    :cond_3e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576879
    :pswitch_24
    const-string v0, "PAGE_CATEGORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 576880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576881
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576882
    :pswitch_25
    const-string v0, "GOINGTOABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 576883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->GOINGTOABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576884
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576885
    :pswitch_26
    const-string v0, "NEARBY_PLACES__ARTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 576886
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__ARTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576887
    :cond_41
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576888
    :pswitch_27
    const-string v0, "MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 576889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576890
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576891
    :pswitch_28
    const-string v0, "NEARBY_PLACES__SIGHTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 576892
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SIGHTS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576893
    :cond_43
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576894
    :pswitch_29
    const-string v0, "LISTENABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 576895
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->LISTENABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576896
    :cond_44
    const-string v0, "WEBSITE_BLOGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 576897
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WEBSITE_BLOGS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576898
    :cond_45
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576899
    :pswitch_2a
    const-string v0, "NEARBY_PLACES__OUTDOORS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 576900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__OUTDOORS:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576901
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576902
    :pswitch_2b
    const-string v0, "PLAYABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 576903
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PLAYABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576904
    :cond_47
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576905
    :pswitch_2c
    const-string v0, "P0__LODGING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 576906
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__LODGING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576907
    :cond_48
    const-string v0, "SPORTS_TEAMS_LEAGUES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 576908
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SPORTS_TEAMS_LEAGUES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576909
    :cond_49
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576910
    :pswitch_2d
    const-string v0, "P0__SHOPPING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 576911
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576912
    :cond_4a
    const-string v0, "READABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 576913
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->READABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576914
    :cond_4b
    const-string v0, "TOO_BROAD_CATEGORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 576915
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576916
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576917
    :pswitch_2e
    const-string v0, "P0__PROFESSIONAL_SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 576918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576919
    :cond_4d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576920
    :pswitch_2f
    const-string v0, "NON_REVIEWABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 576921
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NON_REVIEWABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576922
    :cond_4e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576923
    :pswitch_30
    const-string v0, "P0__RESIDENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 576924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576925
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576926
    :pswitch_31
    const-string v0, "P0__AUTOMOTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 576927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__AUTOMOTIVE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576928
    :cond_50
    const-string v0, "SUPPORTABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 576929
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->SUPPORTABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576930
    :cond_51
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576931
    :pswitch_32
    const-string v0, "PQI_PRICE_RANGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 576932
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->PQI_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576933
    :cond_52
    const-string v0, "TRAVELLABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 576934
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TRAVELLABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576935
    :cond_53
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576936
    :pswitch_33
    const-string v0, "P0__PLACE_TO_EAT_DRINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 576937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PLACE_TO_EAT_DRINK:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576938
    :cond_54
    const-string v0, "TOO_BROAD_CE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 576939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->TOO_BROAD_CE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576940
    :cond_55
    const-string v0, "WATCHABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 576941
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->WATCHABLE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576942
    :cond_56
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576943
    :pswitch_34
    const-string v0, "ENTERTAINMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 576944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->ENTERTAINMENT:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576945
    :cond_57
    const-string v0, "NEARBY_PLACES__SHOPPING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 576946
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__SHOPPING:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576947
    :cond_58
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576948
    :pswitch_35
    const-string v0, "NEARBY_PLACES__PROFESSIONAL_SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 576949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->NEARBY_PLACES__PROFESSIONAL_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576950
    :cond_59
    const-string v0, "P0__PUBLIC_STRUCTURE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 576951
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__PUBLIC_STRUCTURE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576952
    :cond_5a
    const-string v0, "P0__WORKPLACE_OFFICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 576953
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->P0__WORKPLACE_OFFICE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    .line 576954
    :cond_5b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_0
        :pswitch_0
        :pswitch_34
        :pswitch_35
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 1

    .prologue
    .line 576956
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 1

    .prologue
    .line 576955
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method
