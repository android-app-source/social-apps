.class public final enum Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum HIDDEN_REASON:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum INVITABLE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum INVITABLE_FOF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum NOT_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum NOT_GROUP_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum NOT_GROUP_MEMBER:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum OVER_LIMIT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

.field public static final enum VIEWER_SELF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725890
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725891
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "INVITABLE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725892
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "INVITABLE_FOF"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE_FOF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725893
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725894
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725895
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "MAYBE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725896
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "NOT_GOING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725897
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "REMOVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725898
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "VIEWER_SELF"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->VIEWER_SELF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725899
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "OVER_LIMIT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->OVER_LIMIT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "NOT_FRIEND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "NOT_GROUP_MEMBER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_MEMBER:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "NOT_GROUP_FRIEND"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725903
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "HIDDEN_REASON"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->HIDDEN_REASON:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725904
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "INELIGIBLE_FOR_EVENT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725905
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "SUBSCRIBED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725907
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE_FOF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->VIEWER_SELF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->OVER_LIMIT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_MEMBER:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->HIDDEN_REASON:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725852
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
    .locals 1

    .prologue
    .line 725853
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    .line 725854
    :goto_0
    return-object v0

    .line 725855
    :cond_1
    const-string v0, "INVITABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725856
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725857
    :cond_2
    const-string v0, "INVITABLE_FOF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITABLE_FOF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725859
    :cond_3
    const-string v0, "INVITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725861
    :cond_4
    const-string v0, "GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725862
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725863
    :cond_5
    const-string v0, "MAYBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725864
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725865
    :cond_6
    const-string v0, "NOT_GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725867
    :cond_7
    const-string v0, "REMOVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725869
    :cond_8
    const-string v0, "VIEWER_SELF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725870
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->VIEWER_SELF:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725871
    :cond_9
    const-string v0, "OVER_LIMIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 725872
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->OVER_LIMIT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725873
    :cond_a
    const-string v0, "NOT_FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 725874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725875
    :cond_b
    const-string v0, "NOT_GROUP_MEMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 725876
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_MEMBER:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto :goto_0

    .line 725877
    :cond_c
    const-string v0, "NOT_GROUP_FRIEND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 725878
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GROUP_FRIEND:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0

    .line 725879
    :cond_d
    const-string v0, "HIDDEN_REASON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 725880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->HIDDEN_REASON:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0

    .line 725881
    :cond_e
    const-string v0, "INELIGIBLE_FOR_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 725882
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0

    .line 725883
    :cond_f
    const-string v0, "SUBSCRIBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 725884
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0

    .line 725885
    :cond_10
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 725886
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0

    .line 725887
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
    .locals 1

    .prologue
    .line 725888
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;
    .locals 1

    .prologue
    .line 725889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    return-object v0
.end method
