.class public final enum Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public static final enum ADD_LOCATION_TAG:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public static final enum ADD_MESSAGE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public static final enum ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725285
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 725286
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const-string v1, "ADD_MESSAGE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_MESSAGE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 725287
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const-string v1, "ADD_LOCATION_TAG"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_LOCATION_TAG:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 725288
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    const-string v1, "ADD_TEXT_FORMATTING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 725289
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_MESSAGE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_LOCATION_TAG:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725290
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .locals 1

    .prologue
    .line 725291
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    .line 725292
    :goto_0
    return-object v0

    .line 725293
    :cond_1
    const-string v0, "ADD_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_MESSAGE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    goto :goto_0

    .line 725295
    :cond_2
    const-string v0, "ADD_LOCATION_TAG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_LOCATION_TAG:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    goto :goto_0

    .line 725297
    :cond_3
    const-string v0, "ADD_TEXT_FORMATTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->ADD_TEXT_FORMATTING:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    goto :goto_0

    .line 725299
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .locals 1

    .prologue
    .line 725300
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;
    .locals 1

    .prologue
    .line 725301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEditPostActionLinkType;

    return-object v0
.end method
