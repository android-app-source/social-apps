.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

.field public static final enum CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

.field public static final enum DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

.field public static final enum DELETE_APP_ONLY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738358
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    .line 738359
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    const-string v1, "DELETE_APP_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_ONLY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    .line 738360
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    const-string v1, "DELETE_APP_AND_POSTS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    .line 738361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    const-string v1, "CHANGE_PRIVACY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    .line 738362
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_ONLY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738357
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;
    .locals 1

    .prologue
    .line 738346
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    .line 738347
    :goto_0
    return-object v0

    .line 738348
    :cond_1
    const-string v0, "DELETE_APP_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738349
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_ONLY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    goto :goto_0

    .line 738350
    :cond_2
    const-string v0, "DELETE_APP_AND_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    goto :goto_0

    .line 738352
    :cond_3
    const-string v0, "CHANGE_PRIVACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738353
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    goto :goto_0

    .line 738354
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;
    .locals 1

    .prologue
    .line 738356
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;
    .locals 1

    .prologue
    .line 738355
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    return-object v0
.end method
