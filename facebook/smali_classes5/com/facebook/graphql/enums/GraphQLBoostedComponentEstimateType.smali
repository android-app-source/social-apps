.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

.field public static final enum CLICK:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

.field public static final enum REACH:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 723869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const-string v1, "CLICK"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->CLICK:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 723870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const-string v1, "REACH"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->REACH:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 723871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->LIKE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 723872
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->CLICK:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->REACH:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->LIKE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 1

    .prologue
    .line 723876
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    .line 723877
    :goto_0
    return-object v0

    .line 723878
    :cond_1
    const-string v0, "CLICK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->CLICK:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    goto :goto_0

    .line 723880
    :cond_2
    const-string v0, "REACH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723881
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->REACH:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    goto :goto_0

    .line 723882
    :cond_3
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->LIKE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    goto :goto_0

    .line 723884
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 1

    .prologue
    .line 723874
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;
    .locals 1

    .prologue
    .line 723873
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    return-object v0
.end method
