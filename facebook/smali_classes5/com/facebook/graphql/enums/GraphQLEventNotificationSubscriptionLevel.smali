.class public final enum Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public static final enum ALL:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public static final enum HOST_ONLY:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public static final enum MOST_IMPORTANT:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

.field public static final enum UNSUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->ALL:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const-string v1, "MOST_IMPORTANT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->MOST_IMPORTANT:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const-string v1, "HOST_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->HOST_ONLY:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    const-string v1, "UNSUBSCRIBED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725913
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->ALL:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->MOST_IMPORTANT:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->HOST_ONLY:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725914
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .locals 1

    .prologue
    .line 725915
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 725916
    :goto_0
    return-object v0

    .line 725917
    :cond_1
    const-string v0, "ALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->ALL:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0

    .line 725919
    :cond_2
    const-string v0, "MOST_IMPORTANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->MOST_IMPORTANT:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0

    .line 725921
    :cond_3
    const-string v0, "HOST_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->HOST_ONLY:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0

    .line 725923
    :cond_4
    const-string v0, "UNSUBSCRIBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0

    .line 725925
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .locals 1

    .prologue
    .line 725926
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;
    .locals 1

    .prologue
    .line 725927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    return-object v0
.end method
