.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public static final enum AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public static final enum HYBRID:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public static final enum SATELLITE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public static final enum STANDARD:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->STANDARD:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const-string v1, "SATELLITE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->SATELLITE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    const-string v1, "HYBRID"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->HYBRID:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725118
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->STANDARD:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->SATELLITE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->HYBRID:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .locals 1

    .prologue
    .line 725120
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 725121
    :goto_0
    return-object v0

    .line 725122
    :cond_1
    const-string v0, "AUTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725123
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    goto :goto_0

    .line 725124
    :cond_2
    const-string v0, "STANDARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->STANDARD:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    goto :goto_0

    .line 725126
    :cond_3
    const-string v0, "SATELLITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->SATELLITE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    goto :goto_0

    .line 725128
    :cond_4
    const-string v0, "HYBRID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->HYBRID:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    goto :goto_0

    .line 725130
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .locals 1

    .prologue
    .line 725131
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;
    .locals 1

    .prologue
    .line 725132
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    return-object v0
.end method
