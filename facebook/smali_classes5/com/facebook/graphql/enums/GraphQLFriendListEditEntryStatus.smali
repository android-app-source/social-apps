.class public final enum Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

.field public static final enum LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

.field public static final enum NON_LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

.field public static final enum PRESELECTED_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

.field public static final enum SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 726633
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726634
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const-string v1, "LIST_MEMBER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726635
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const-string v1, "PRESELECTED_SUGGESTION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->PRESELECTED_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726636
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const-string v1, "SUGGESTION"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726637
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    const-string v1, "NON_LIST_MEMBER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->NON_LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726638
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->PRESELECTED_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->NON_LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726639
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
    .locals 1

    .prologue
    .line 726640
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    .line 726641
    :goto_0
    return-object v0

    .line 726642
    :cond_1
    const-string v0, "LIST_MEMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726643
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    goto :goto_0

    .line 726644
    :cond_2
    const-string v0, "PRESELECTED_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->PRESELECTED_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    goto :goto_0

    .line 726646
    :cond_3
    const-string v0, "SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->SUGGESTION:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    goto :goto_0

    .line 726648
    :cond_4
    const-string v0, "NON_LIST_MEMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726649
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->NON_LIST_MEMBER:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    goto :goto_0

    .line 726650
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
    .locals 1

    .prologue
    .line 726651
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;
    .locals 1

    .prologue
    .line 726652
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    return-object v0
.end method
