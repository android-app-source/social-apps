.class public final enum Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 577050
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577051
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "ICON"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577052
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "OBJECT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "PERSON"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "PEOPLE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    const-string v1, "PLACE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577056
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 577057
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;
    .locals 1

    .prologue
    .line 577058
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 577059
    :goto_0
    return-object v0

    .line 577060
    :cond_1
    const-string v0, "ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 577061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0

    .line 577062
    :cond_2
    const-string v0, "OBJECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 577063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0

    .line 577064
    :cond_3
    const-string v0, "PERSON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 577065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0

    .line 577066
    :cond_4
    const-string v0, "PEOPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 577067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0

    .line 577068
    :cond_5
    const-string v0, "PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 577069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0

    .line 577070
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;
    .locals 1

    .prologue
    .line 577071
    const-class v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;
    .locals 1

    .prologue
    .line 577072
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    return-object v0
.end method
