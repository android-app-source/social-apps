.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

.field public static final enum TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

.field public static final enum UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738401
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 738402
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    const-string v1, "TAGGEES"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 738403
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 738404
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738405
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 738406
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 738407
    :goto_0
    return-object v0

    .line 738408
    :cond_1
    const-string v0, "TAGGEES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    goto :goto_0

    .line 738410
    :cond_2
    const-string v0, "UNSPECIFIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    goto :goto_0

    .line 738412
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 738413
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 738414
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    return-object v0
.end method
