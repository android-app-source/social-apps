.class public final enum Lcom/facebook/graphql/enums/GraphQLEventVisibility;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventVisibility;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum FRIENDS_OF_GUESTS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum INVITE_ONLY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum PAGE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public static final enum USER_PUBLIC:Lcom/facebook/graphql/enums/GraphQLEventVisibility;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726108
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726109
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "FRIENDS_OF_FRIENDS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "FRIENDS_OF_GUESTS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_GUESTS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726112
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "INVITE_ONLY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->INVITE_ONLY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->PAGE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "USER_PUBLIC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->USER_PUBLIC:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    const-string v1, "COMMUNITY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726116
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_GUESTS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->INVITE_ONLY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->PAGE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->USER_PUBLIC:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726088
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 1

    .prologue
    .line 726089
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 726090
    :goto_0
    return-object v0

    .line 726091
    :cond_1
    const-string v0, "FRIENDS_OF_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726092
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726093
    :cond_2
    const-string v0, "FRIENDS_OF_GUESTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->FRIENDS_OF_GUESTS:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726095
    :cond_3
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726097
    :cond_4
    const-string v0, "INVITE_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->INVITE_ONLY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726099
    :cond_5
    const-string v0, "PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->PAGE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726101
    :cond_6
    const-string v0, "USER_PUBLIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->USER_PUBLIC:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726103
    :cond_7
    const-string v0, "COMMUNITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0

    .line 726105
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 1

    .prologue
    .line 726106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .locals 1

    .prologue
    .line 726107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    return-object v0
.end method
