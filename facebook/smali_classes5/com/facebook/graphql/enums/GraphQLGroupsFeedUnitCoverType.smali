.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

.field public static final enum COLLEGE_COMMUNITY_SALE_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

.field public static final enum COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

.field public static final enum COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 639045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 639046
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    const-string v1, "COLLEGE_COMMUNITY_SALE_GROUPS_COVER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SALE_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 639047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    const-string v1, "COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 639048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    const-string v1, "COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 639049
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SALE_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 639050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;
    .locals 1

    .prologue
    .line 639051
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    .line 639052
    :goto_0
    return-object v0

    .line 639053
    :cond_1
    const-string v0, "COLLEGE_COMMUNITY_SALE_GROUPS_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 639054
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SALE_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    goto :goto_0

    .line 639055
    :cond_2
    const-string v0, "COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 639056
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_GROUPS_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    goto :goto_0

    .line 639057
    :cond_3
    const-string v0, "COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 639058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->COLLEGE_COMMUNITY_SUGGESTED_PEOPLE_COVER:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    goto :goto_0

    .line 639059
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;
    .locals 1

    .prologue
    .line 639060
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;
    .locals 1

    .prologue
    .line 639061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    return-object v0
.end method
