.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

.field public static final enum BOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

.field public static final enum LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

.field public static final enum REGULAR:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

.field public static final enum SEMIBOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->REGULAR:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const-string v1, "SEMIBOLD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->SEMIBOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    const-string v1, "BOLD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->BOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738938
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->REGULAR:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->SEMIBOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->BOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
    .locals 1

    .prologue
    .line 738942
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    .line 738943
    :goto_0
    return-object v0

    .line 738944
    :cond_1
    const-string v0, "LIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738945
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    goto :goto_0

    .line 738946
    :cond_2
    const-string v0, "REGULAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->REGULAR:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    goto :goto_0

    .line 738948
    :cond_3
    const-string v0, "SEMIBOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->SEMIBOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    goto :goto_0

    .line 738950
    :cond_4
    const-string v0, "BOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738951
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->BOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    goto :goto_0

    .line 738952
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
    .locals 1

    .prologue
    .line 738940
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;
    .locals 1

    .prologue
    .line 738939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    return-object v0
.end method
