.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum COLOR_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum COMPOSITE_BLOCK:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum CULTURAL_MOMENT_POPULAR_MEDIA:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum DIVIDER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum ELEMENT_GROUP:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum EXPANDABLE_SECTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum FOOTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum HEADER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum LOCAL_IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum STATEFUL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum STORE_LOCATOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum TITLE_AND_DATE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727665
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727666
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "RICH_TEXT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727667
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727668
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "BUTTON"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727669
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "SLIDESHOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727670
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "DIVIDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->DIVIDER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727671
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "EXPANDABLE_SECTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->EXPANDABLE_SECTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727672
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "STATEFUL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STATEFUL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727673
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727674
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "COLOR_SELECTOR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COLOR_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727675
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "TOGGLE_BUTTON"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727676
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "HEADER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->HEADER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727677
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "COMPOSITE_BLOCK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COMPOSITE_BLOCK:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727678
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "FOOTER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->FOOTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727679
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "ELEMENT_GROUP"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ELEMENT_GROUP:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727680
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "TITLE_AND_DATE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TITLE_AND_DATE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727681
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "STORE_LOCATOR"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STORE_LOCATOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727682
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "SCRUBBABLE_GIF"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727683
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "LOCAL_IMAGE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->LOCAL_IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727684
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const-string v1, "CULTURAL_MOMENT_POPULAR_MEDIA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->CULTURAL_MOMENT_POPULAR_MEDIA:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727685
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->DIVIDER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->EXPANDABLE_SECTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STATEFUL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COLOR_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->HEADER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COMPOSITE_BLOCK:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->FOOTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ELEMENT_GROUP:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TITLE_AND_DATE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STORE_LOCATOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->LOCAL_IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->CULTURAL_MOMENT_POPULAR_MEDIA:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727686
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
    .locals 1

    .prologue
    .line 727687
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 727688
    :goto_0
    return-object v0

    .line 727689
    :cond_1
    const-string v0, "RICH_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727690
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727691
    :cond_2
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727693
    :cond_3
    const-string v0, "BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727694
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727695
    :cond_4
    const-string v0, "SLIDESHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727696
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727697
    :cond_5
    const-string v0, "DIVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->DIVIDER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727699
    :cond_6
    const-string v0, "EXPANDABLE_SECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->EXPANDABLE_SECTION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727701
    :cond_7
    const-string v0, "STATEFUL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727702
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STATEFUL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727703
    :cond_8
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727704
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727705
    :cond_9
    const-string v0, "COLOR_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727706
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COLOR_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727707
    :cond_a
    const-string v0, "TOGGLE_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TOGGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727709
    :cond_b
    const-string v0, "HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727710
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->HEADER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto :goto_0

    .line 727711
    :cond_c
    const-string v0, "COMPOSITE_BLOCK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727712
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->COMPOSITE_BLOCK:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727713
    :cond_d
    const-string v0, "FOOTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727714
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->FOOTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727715
    :cond_e
    const-string v0, "ELEMENT_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727716
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ELEMENT_GROUP:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727717
    :cond_f
    const-string v0, "TITLE_AND_DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727718
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->TITLE_AND_DATE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727719
    :cond_10
    const-string v0, "STORE_LOCATOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727720
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->STORE_LOCATOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727721
    :cond_11
    const-string v0, "SCRUBBABLE_GIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727722
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727723
    :cond_12
    const-string v0, "LOCAL_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 727724
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->LOCAL_IMAGE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727725
    :cond_13
    const-string v0, "CULTURAL_MOMENT_POPULAR_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 727726
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->CULTURAL_MOMENT_POPULAR_MEDIA:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0

    .line 727727
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
    .locals 1

    .prologue
    .line 727728
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
    .locals 1

    .prologue
    .line 727729
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    return-object v0
.end method
