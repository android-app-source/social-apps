.class public final enum Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum ACTION_FEEDBACK_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum HREF:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum INLINE_COMMENT_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729385
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "ACTION_TYPE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729387
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "TITLE_TEXT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "HREF"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->HREF:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729389
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "METADATA"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729390
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "FRIENDING_REDIRECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729391
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "STORY_FEEDBACK_ID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "ACTION_FEEDBACK_PRIMARY_TEXT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "ACTION_FEEDBACK_SECONDARY_TEXT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "LEGACY_API_POST_ID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    const-string v1, "INLINE_COMMENT_TEXT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->INLINE_COMMENT_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729396
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->HREF:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->INLINE_COMMENT_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729397
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;
    .locals 1

    .prologue
    .line 729398
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    .line 729399
    :goto_0
    return-object v0

    .line 729400
    :cond_1
    const-string v0, "ACTION_TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_TYPE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729402
    :cond_2
    const-string v0, "TITLE_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729404
    :cond_3
    const-string v0, "HREF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->HREF:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729406
    :cond_4
    const-string v0, "METADATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->METADATA:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729408
    :cond_5
    const-string v0, "FRIENDING_REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729410
    :cond_6
    const-string v0, "STORY_FEEDBACK_ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729412
    :cond_7
    const-string v0, "LEGACY_API_POST_ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 729413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729414
    :cond_8
    const-string v0, "ACTION_FEEDBACK_PRIMARY_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 729415
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729416
    :cond_9
    const-string v0, "ACTION_FEEDBACK_SECONDARY_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 729417
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729418
    :cond_a
    const-string v0, "INLINE_COMMENT_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 729419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->INLINE_COMMENT_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0

    .line 729420
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;
    .locals 1

    .prologue
    .line 729421
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;
    .locals 1

    .prologue
    .line 729422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    return-object v0
.end method
