.class public final enum Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

.field public static final enum COLLAPSED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

.field public static final enum EXPANDED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728668
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    .line 728669
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    const-string v1, "COLLAPSED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->COLLAPSED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    .line 728670
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    .line 728671
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->COLLAPSED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728672
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;
    .locals 1

    .prologue
    .line 728673
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    .line 728674
    :goto_0
    return-object v0

    .line 728675
    :cond_1
    const-string v0, "COLLAPSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728676
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->COLLAPSED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    goto :goto_0

    .line 728677
    :cond_2
    const-string v0, "EXPANDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728678
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->EXPANDED:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    goto :goto_0

    .line 728679
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;
    .locals 1

    .prologue
    .line 728680
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;
    .locals 1

    .prologue
    .line 728681
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    return-object v0
.end method
