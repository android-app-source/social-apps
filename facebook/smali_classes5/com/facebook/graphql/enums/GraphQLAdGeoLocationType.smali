.class public final enum Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum COUNTRY_GROUP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum ELECTORAL_DISTRICT:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum MARKET:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum PLACE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum REGION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

.field public static final enum ZIP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "COUNTRY_GROUP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY_GROUP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723211
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "COUNTRY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723212
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "REGION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->REGION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723213
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723214
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "ZIP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ZIP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723215
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "MARKET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->MARKET:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723216
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "ELECTORAL_DISTRICT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ELECTORAL_DISTRICT:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723217
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "PLACE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->PLACE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723218
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    const-string v1, "CUSTOM_LOCATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723219
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY_GROUP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->REGION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ZIP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->MARKET:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ELECTORAL_DISTRICT:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->PLACE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
    .locals 1

    .prologue
    .line 723221
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 723222
    :goto_0
    return-object v0

    .line 723223
    :cond_1
    const-string v0, "COUNTRY_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723224
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY_GROUP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723225
    :cond_2
    const-string v0, "COUNTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->COUNTRY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723227
    :cond_3
    const-string v0, "REGION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->REGION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723229
    :cond_4
    const-string v0, "CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723231
    :cond_5
    const-string v0, "ZIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ZIP:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723233
    :cond_6
    const-string v0, "MARKET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723234
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->MARKET:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723235
    :cond_7
    const-string v0, "ELECTORAL_DISTRICT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ELECTORAL_DISTRICT:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723237
    :cond_8
    const-string v0, "PLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 723238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->PLACE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723239
    :cond_9
    const-string v0, "CUSTOM_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 723240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0

    .line 723241
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
    .locals 1

    .prologue
    .line 723242
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
    .locals 1

    .prologue
    .line 723243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    return-object v0
.end method
