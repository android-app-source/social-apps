.class public final enum Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public static final enum CONTACT_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public static final enum OFFSITE_LINK:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public static final enum ONSITE_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 724378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const-string v1, "CONTACT_MERCHANT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->CONTACT_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 724379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const-string v1, "OFFSITE_LINK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->OFFSITE_LINK:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 724380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    const-string v1, "ONSITE_CHECKOUT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->ONSITE_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 724381
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->CONTACT_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->OFFSITE_LINK:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->ONSITE_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724376
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .locals 1

    .prologue
    .line 724382
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 724383
    :goto_0
    return-object v0

    .line 724384
    :cond_1
    const-string v0, "CONTACT_MERCHANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724385
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->CONTACT_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    goto :goto_0

    .line 724386
    :cond_2
    const-string v0, "OFFSITE_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724387
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->OFFSITE_LINK:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    goto :goto_0

    .line 724388
    :cond_3
    const-string v0, "ONSITE_CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->ONSITE_CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    goto :goto_0

    .line 724390
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .locals 1

    .prologue
    .line 724374
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .locals 1

    .prologue
    .line 724375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    return-object v0
.end method
