.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738867
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738868
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738872
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "LARGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738873
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    const-string v1, "EXTRA_LARGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738874
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 1

    .prologue
    .line 738876
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 738877
    :goto_0
    return-object v0

    .line 738878
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738880
    :cond_2
    const-string v0, "EXTRA_SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738881
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738882
    :cond_3
    const-string v0, "SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738884
    :cond_4
    const-string v0, "MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738885
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738886
    :cond_5
    const-string v0, "LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738887
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738888
    :cond_6
    const-string v0, "EXTRA_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0

    .line 738890
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 1

    .prologue
    .line 738891
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .locals 1

    .prologue
    .line 738892
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    return-object v0
.end method
