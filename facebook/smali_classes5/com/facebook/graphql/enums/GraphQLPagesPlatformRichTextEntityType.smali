.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum CURRENCY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum DATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum STAR_RATING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum TIMESPAN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public static final enum URL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737527
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737528
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "CURRENCY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737529
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->DATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737530
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "ICON"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737531
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "STAR_RATING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->STAR_RATING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737532
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "TIMESPAN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->TIMESPAN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737533
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "URL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->URL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737534
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const-string v1, "PHONE_NUMBER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737535
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->DATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->STAR_RATING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->TIMESPAN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->URL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737536
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
    .locals 1

    .prologue
    .line 737537
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 737538
    :goto_0
    return-object v0

    .line 737539
    :cond_1
    const-string v0, "CURRENCY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737540
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737541
    :cond_2
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737542
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->DATE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737543
    :cond_3
    const-string v0, "ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737544
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737545
    :cond_4
    const-string v0, "PHONE_NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->PHONE_NUMBER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737547
    :cond_5
    const-string v0, "STAR_RATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->STAR_RATING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737549
    :cond_6
    const-string v0, "TIMESPAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->TIMESPAN:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737551
    :cond_7
    const-string v0, "URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737552
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->URL:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0

    .line 737553
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
    .locals 1

    .prologue
    .line 737554
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
    .locals 1

    .prologue
    .line 737555
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    return-object v0
.end method
