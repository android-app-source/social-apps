.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

.field public static final enum GOING:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

.field public static final enum INTERESTED:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

.field public static final enum MAYBE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->GOING:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const-string v1, "INTERESTED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    const-string v1, "MAYBE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739024
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->GOING:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739018
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
    .locals 1

    .prologue
    .line 739025
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    .line 739026
    :goto_0
    return-object v0

    .line 739027
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739028
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    goto :goto_0

    .line 739029
    :cond_2
    const-string v0, "GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739030
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->GOING:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    goto :goto_0

    .line 739031
    :cond_3
    const-string v0, "INTERESTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739032
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    goto :goto_0

    .line 739033
    :cond_4
    const-string v0, "MAYBE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 739034
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    goto :goto_0

    .line 739035
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
    .locals 1

    .prologue
    .line 739017
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;
    .locals 1

    .prologue
    .line 739016
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    return-object v0
.end method
