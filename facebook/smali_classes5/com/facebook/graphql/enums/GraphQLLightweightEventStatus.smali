.class public final enum Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

.field public static final enum CREATED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

.field public static final enum DELETED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728208
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 728209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->CREATED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 728210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    const-string v1, "DELETED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->DELETED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 728211
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->CREATED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->DELETED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
    .locals 1

    .prologue
    .line 728213
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 728214
    :goto_0
    return-object v0

    .line 728215
    :cond_1
    const-string v0, "CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->CREATED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    goto :goto_0

    .line 728217
    :cond_2
    const-string v0, "DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->DELETED:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    goto :goto_0

    .line 728219
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
    .locals 1

    .prologue
    .line 728220
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;
    .locals 1

    .prologue
    .line 728221
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    return-object v0
.end method
