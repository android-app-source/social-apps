.class public final enum Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLUserChatContextType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum CELEBRATION:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum LISTENING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum NEIGHBOURHOOD:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum OG_COMPOSER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum PLAYING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum PRESENCE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum TRAVELING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

.field public static final enum VISITING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 741095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "NEARBY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "VISITING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->VISITING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "TRAVELING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "CELEBRATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->CELEBRATION:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "NEIGHBOURHOOD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEIGHBOURHOOD:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741102
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "LISTENING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->LISTENING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741103
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "PLAYING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PLAYING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741104
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "PRESENCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PRESENCE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741105
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "OTHER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OTHER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741106
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    const-string v1, "OG_COMPOSER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OG_COMPOSER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741107
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->VISITING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->CELEBRATION:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEIGHBOURHOOD:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->LISTENING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PLAYING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PRESENCE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OTHER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OG_COMPOSER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
    .locals 1

    .prologue
    .line 741109
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    .line 741110
    :goto_0
    return-object v0

    .line 741111
    :cond_1
    const-string v0, "NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741112
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741113
    :cond_2
    const-string v0, "VISITING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741114
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->VISITING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741115
    :cond_3
    const-string v0, "TRAVELING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741116
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741117
    :cond_4
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 741118
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741119
    :cond_5
    const-string v0, "CELEBRATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 741120
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->CELEBRATION:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741121
    :cond_6
    const-string v0, "NEIGHBOURHOOD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741122
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEIGHBOURHOOD:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741123
    :cond_7
    const-string v0, "LISTENING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 741124
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->LISTENING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741125
    :cond_8
    const-string v0, "PLAYING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741126
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PLAYING:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741127
    :cond_9
    const-string v0, "PRESENCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 741128
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->PRESENCE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741129
    :cond_a
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 741130
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OTHER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741131
    :cond_b
    const-string v0, "OG_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 741132
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->OG_COMPOSER:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0

    .line 741133
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
    .locals 1

    .prologue
    .line 741134
    const-class v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLUserChatContextType;
    .locals 1

    .prologue
    .line 741135
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    return-object v0
.end method
