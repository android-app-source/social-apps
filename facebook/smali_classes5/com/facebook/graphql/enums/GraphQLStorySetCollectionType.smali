.class public final enum Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum ARTICLE_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum DEPRECATED_6:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum FALLBACK:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum FIRST_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum FRIEND_EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum FRIEND_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum GROUP_TOP_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum HOT_TOPIC_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum LIVE_EVENT_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum MIXED_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum PAGES_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

.field public static final enum VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "VIDEO_STORIES"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "FRIEND_VIDEO_STORIES"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "ARTICLE_STORIES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ARTICLE_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "LINK_ONLY_STORIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "DEPRECATED_6"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->DEPRECATED_6:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "PAGES_STORIES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->PAGES_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "GROUP_TOP_STORIES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->GROUP_TOP_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "MIXED_STORIES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->MIXED_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "LIVE_VIDEO_STORIES"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "FIRST_VIDEO_STORIES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FIRST_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "HOT_TOPIC_STORIES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->HOT_TOPIC_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "EVENT_STORIES"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "SINGLE_CREATOR_VIDEO_STORIES"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "FRIEND_EVENT_STORIES"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    const-string v1, "LIVE_EVENT_VIDEO_STORIES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_EVENT_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740146
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ARTICLE_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->DEPRECATED_6:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->PAGES_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->GROUP_TOP_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->MIXED_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FIRST_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->HOT_TOPIC_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_EVENT_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;
    .locals 1

    .prologue
    .line 740150
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 740151
    :goto_0
    return-object v0

    .line 740152
    :cond_1
    const-string v0, "FALLBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740153
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740154
    :cond_2
    const-string v0, "VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740156
    :cond_3
    const-string v0, "FIRST_VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740157
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FIRST_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740158
    :cond_4
    const-string v0, "FRIEND_VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740159
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740160
    :cond_5
    const-string v0, "ARTICLE_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ARTICLE_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740162
    :cond_6
    const-string v0, "LINK_ONLY_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740164
    :cond_7
    const-string v0, "PAGES_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740165
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->PAGES_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740166
    :cond_8
    const-string v0, "GROUP_TOP_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->GROUP_TOP_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740168
    :cond_9
    const-string v0, "MIXED_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->MIXED_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740170
    :cond_a
    const-string v0, "LIVE_VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 740171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740172
    :cond_b
    const-string v0, "HOT_TOPIC_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 740173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->HOT_TOPIC_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto :goto_0

    .line 740174
    :cond_c
    const-string v0, "EVENT_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 740175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto/16 :goto_0

    .line 740176
    :cond_d
    const-string v0, "FRIEND_EVENT_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 740177
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FRIEND_EVENT_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto/16 :goto_0

    .line 740178
    :cond_e
    const-string v0, "SINGLE_CREATOR_VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 740179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto/16 :goto_0

    .line 740180
    :cond_f
    const-string v0, "LIVE_EVENT_VIDEO_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 740181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_EVENT_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto/16 :goto_0

    .line 740182
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;
    .locals 1

    .prologue
    .line 740148
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;
    .locals 1

    .prologue
    .line 740147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    return-object v0
.end method
