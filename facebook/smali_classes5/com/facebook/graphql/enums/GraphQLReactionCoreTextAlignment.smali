.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

.field public static final enum CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

.field public static final enum LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

.field public static final enum RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738916
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 738917
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 738918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 738919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 738920
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738921
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
    .locals 1

    .prologue
    .line 738922
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    .line 738923
    :goto_0
    return-object v0

    .line 738924
    :cond_1
    const-string v0, "LEFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738925
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    goto :goto_0

    .line 738926
    :cond_2
    const-string v0, "CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738927
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    goto :goto_0

    .line 738928
    :cond_3
    const-string v0, "RIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738929
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    goto :goto_0

    .line 738930
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
    .locals 1

    .prologue
    .line 738931
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;
    .locals 1

    .prologue
    .line 738932
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    return-object v0
.end method
