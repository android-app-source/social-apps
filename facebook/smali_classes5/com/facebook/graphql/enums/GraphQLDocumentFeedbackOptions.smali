.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public static final enum LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public static final enum LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 725072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 725073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const-string v1, "LIKES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 725074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    const-string v1, "LIKES_AND_COMMENTS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 725075
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1

    .prologue
    .line 725077
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 725078
    :goto_0
    return-object v0

    .line 725079
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725080
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    goto :goto_0

    .line 725081
    :cond_2
    const-string v0, "LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725082
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    goto :goto_0

    .line 725083
    :cond_3
    const-string v0, "LIKES_AND_COMMENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    goto :goto_0

    .line 725085
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1

    .prologue
    .line 725086
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1

    .prologue
    .line 725087
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method
