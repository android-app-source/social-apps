.class public final enum Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

.field public static final enum DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

.field public static final enum GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

.field public static final enum INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 728204
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    const-string v1, "GOING"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 728205
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 728206
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 728207
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728202
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;
    .locals 1

    .prologue
    .line 728193
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    .line 728194
    :goto_0
    return-object v0

    .line 728195
    :cond_1
    const-string v0, "GOING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728196
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    goto :goto_0

    .line 728197
    :cond_2
    const-string v0, "INVITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    goto :goto_0

    .line 728199
    :cond_3
    const-string v0, "DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728200
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    goto :goto_0

    .line 728201
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;
    .locals 1

    .prologue
    .line 728191
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;
    .locals 1

    .prologue
    .line 728192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    return-object v0
.end method
