.class public final enum Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

.field public static final enum PAST_MONTH:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

.field public static final enum PAST_WEEK:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739554
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    .line 739555
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    const-string v1, "PAST_WEEK"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_WEEK:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    .line 739556
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    const-string v1, "PAST_MONTH"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_MONTH:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    .line 739557
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_WEEK:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_MONTH:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
    .locals 1

    .prologue
    .line 739561
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    .line 739562
    :goto_0
    return-object v0

    .line 739563
    :cond_1
    const-string v0, "PAST_WEEK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739564
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_WEEK:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    goto :goto_0

    .line 739565
    :cond_2
    const-string v0, "PAST_MONTH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739566
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->PAST_MONTH:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    goto :goto_0

    .line 739567
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
    .locals 1

    .prologue
    .line 739559
    const-class v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;
    .locals 1

    .prologue
    .line 739558
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLRecentSaveTimeframe;

    return-object v0
.end method
