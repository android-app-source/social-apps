.class public final enum Lcom/facebook/graphql/enums/GraphQLBlockStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBlockStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum BLOCKED_BY_MANY:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum BLOCKED_BY_SOME:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum BLOCKED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

.field public static final enum WHITELISTED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723570
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723571
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723572
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "BLOCKED_BY_VIEWER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723573
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "BLOCKED_BY_SOME"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_SOME:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723574
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "BLOCKED_BY_MANY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_MANY:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723575
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    const-string v1, "WHITELISTED_BY_VIEWER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->WHITELISTED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723576
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_SOME:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_MANY:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->WHITELISTED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723577
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .locals 1

    .prologue
    .line 723578
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    .line 723579
    :goto_0
    return-object v0

    .line 723580
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723581
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0

    .line 723582
    :cond_2
    const-string v0, "BLOCKED_BY_VIEWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723583
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0

    .line 723584
    :cond_3
    const-string v0, "BLOCKED_BY_SOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723585
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_SOME:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0

    .line 723586
    :cond_4
    const-string v0, "BLOCKED_BY_MANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723587
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->BLOCKED_BY_MANY:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0

    .line 723588
    :cond_5
    const-string v0, "WHITELISTED_BY_VIEWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723589
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->WHITELISTED_BY_VIEWER:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0

    .line 723590
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .locals 1

    .prologue
    .line 723591
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBlockStatus;
    .locals 1

    .prologue
    .line 723592
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    return-object v0
.end method
