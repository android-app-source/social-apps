.class public final enum Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFeedSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum ADMIN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum ALL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum APPLICATION:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum CURATED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum DIODE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum FACEBOOK:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum GAMES:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum GROUPS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum IMMERSIVE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum INJECTED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum LINKS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum OUTSIDE_WORLD:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum TODAY_IN_HISTORY:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum TOP_NEWS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum UNSEEN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726520
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726521
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "TOP_NEWS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TOP_NEWS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726522
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "IMMERSIVE_FEED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->IMMERSIVE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726523
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "LINKS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726524
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "GAMES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726525
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "PHOTOS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726526
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "VIDEOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726527
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "SUBSCRIPTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726528
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "ALL_FRIENDS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ALL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726529
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "FRIEND_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726530
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "GROUPS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726531
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "MOST_RECENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726532
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "FACEBOOK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726533
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "OUTSIDE_WORLD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->OUTSIDE_WORLD:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726534
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "TODAY_IN_HISTORY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TODAY_IN_HISTORY:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726535
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "CURATED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->CURATED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726536
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "INJECTED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->INJECTED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726537
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "APPLICATION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726538
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "UNSEEN_FEED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSEEN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726539
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "EXPLORE_FEED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726540
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "ADMIN_FEED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ADMIN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726541
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "DIODE_FEED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->DIODE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726542
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    const-string v1, "NEWS_FEED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726543
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TOP_NEWS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->IMMERSIVE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ALL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->OUTSIDE_WORLD:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TODAY_IN_HISTORY:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->CURATED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->INJECTED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSEEN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ADMIN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->DIODE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726544
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .locals 1

    .prologue
    .line 726473
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    .line 726474
    :goto_0
    return-object v0

    .line 726475
    :cond_1
    const-string v0, "TOP_NEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726476
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TOP_NEWS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726477
    :cond_2
    const-string v0, "LINKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726478
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->LINKS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726479
    :cond_3
    const-string v0, "GAMES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726480
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GAMES:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726481
    :cond_4
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726482
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726483
    :cond_5
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726484
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726485
    :cond_6
    const-string v0, "SUBSCRIPTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726487
    :cond_7
    const-string v0, "ALL_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726488
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ALL_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726489
    :cond_8
    const-string v0, "FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 726490
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726491
    :cond_9
    const-string v0, "GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726493
    :cond_a
    const-string v0, "MOST_RECENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 726494
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726495
    :cond_b
    const-string v0, "FACEBOOK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 726496
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto :goto_0

    .line 726497
    :cond_c
    const-string v0, "TODAY_IN_HISTORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->TODAY_IN_HISTORY:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726499
    :cond_d
    const-string v0, "CURATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 726500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->CURATED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726501
    :cond_e
    const-string v0, "INJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 726502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->INJECTED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726503
    :cond_f
    const-string v0, "IMMERSIVE_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 726504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->IMMERSIVE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726505
    :cond_10
    const-string v0, "OUTSIDE_WORLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 726506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->OUTSIDE_WORLD:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726507
    :cond_11
    const-string v0, "APPLICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 726508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->APPLICATION:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726509
    :cond_12
    const-string v0, "UNSEEN_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 726510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSEEN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726511
    :cond_13
    const-string v0, "EXPLORE_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 726512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->EXPLORE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726513
    :cond_14
    const-string v0, "ADMIN_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 726514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->ADMIN_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726515
    :cond_15
    const-string v0, "DIODE_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 726516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->DIODE_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726517
    :cond_16
    const-string v0, "NEWS_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 726518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->NEWS_FEED:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0

    .line 726519
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .locals 1

    .prologue
    .line 726472
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFeedSectionType;
    .locals 1

    .prologue
    .line 726471
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    return-object v0
.end method
