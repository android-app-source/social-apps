.class public final enum Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum CHECKBOX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum CONSTANTSUM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum CUSTOM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum DROPDOWN:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum FLOWNODE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum ICONSCALE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum INVALID:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum PORT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum RATINGMATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum STARS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740412
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740413
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740414
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "PORT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->PORT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740415
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->INVALID:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "RADIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "CHECKBOX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "DROPDOWN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->DROPDOWN:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "TEXT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "RATINGMATRIX"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RATINGMATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "STARS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->STARS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740423
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "LIKERT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740424
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "CONSTANTSUM"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CONSTANTSUM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740425
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "FLOWNODE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->FLOWNODE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740426
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    const-string v1, "ICONSCALE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->ICONSCALE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740427
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->PORT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->INVALID:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->DROPDOWN:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RATINGMATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->STARS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CONSTANTSUM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->FLOWNODE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->ICONSCALE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740411
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
    .locals 1

    .prologue
    .line 740378
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    .line 740379
    :goto_0
    return-object v0

    .line 740380
    :cond_1
    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740381
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740382
    :cond_2
    const-string v0, "PORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->PORT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740384
    :cond_3
    const-string v0, "INVALID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740385
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->INVALID:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740386
    :cond_4
    const-string v0, "RADIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740387
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740388
    :cond_5
    const-string v0, "CHECKBOX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740390
    :cond_6
    const-string v0, "DROPDOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->DROPDOWN:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740392
    :cond_7
    const-string v0, "TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->TEXT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740394
    :cond_8
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740395
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740396
    :cond_9
    const-string v0, "RATINGMATRIX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740397
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RATINGMATRIX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740398
    :cond_a
    const-string v0, "STARS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 740399
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->STARS:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740400
    :cond_b
    const-string v0, "LIKERT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 740401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->LIKERT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto :goto_0

    .line 740402
    :cond_c
    const-string v0, "CONSTANTSUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 740403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CONSTANTSUM:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto/16 :goto_0

    .line 740404
    :cond_d
    const-string v0, "FLOWNODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 740405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->FLOWNODE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto/16 :goto_0

    .line 740406
    :cond_e
    const-string v0, "ICONSCALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 740407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->ICONSCALE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto/16 :goto_0

    .line 740408
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
    .locals 1

    .prologue
    .line 740410
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;
    .locals 1

    .prologue
    .line 740409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    return-object v0
.end method
