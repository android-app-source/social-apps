.class public final enum Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum COMMERCE_FAQ_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum COMMERCE_NUX_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum IN_MESSENGER_SHOPPING_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum STRUCTURED_MENU_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

.field public static final enum USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724391
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "COMMERCE_FAQ_ENABLED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_FAQ_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "IN_MESSENGER_SHOPPING_ENABLED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->IN_MESSENGER_SHOPPING_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "COMMERCE_NUX_ENABLED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_NUX_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "STRUCTURED_MENU_ENABLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->STRUCTURED_MENU_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    const-string v1, "USER_CONTROL_TOPIC_MANAGE_ENABLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724398
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_FAQ_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->IN_MESSENGER_SHOPPING_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_NUX_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->STRUCTURED_MENU_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724399
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;
    .locals 1

    .prologue
    .line 724400
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 724401
    :goto_0
    return-object v0

    .line 724402
    :cond_1
    const-string v0, "COMMERCE_FAQ_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_FAQ_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724404
    :cond_2
    const-string v0, "IN_MESSENGER_SHOPPING_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->IN_MESSENGER_SHOPPING_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724406
    :cond_3
    const-string v0, "COMMERCE_NUX_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_NUX_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724408
    :cond_4
    const-string v0, "NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724410
    :cond_5
    const-string v0, "STRUCTURED_MENU_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->STRUCTURED_MENU_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724412
    :cond_6
    const-string v0, "USER_CONTROL_TOPIC_MANAGE_ENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0

    .line 724414
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;
    .locals 1

    .prologue
    .line 724415
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;
    .locals 1

    .prologue
    .line 724416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    return-object v0
.end method
