.class public final enum Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum NT_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 741205
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741206
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CHANCE_FLURRIES"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741207
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CHANCE_RAIN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741208
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CHANCE_SLEET"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741209
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CHANCE_SNOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741210
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CHANCE_TSTORMS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741211
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CLEAR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741212
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "CLOUDY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741213
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "FLURRIES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741214
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "FOG"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741215
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "HAZY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741216
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "MOSTLY_CLOUDY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741217
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "MOSTLY_SUNNY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741218
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "PARTLY_CLOUDY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741219
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "PARTLY_SUNNY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741220
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "RAIN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741221
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "SLEET"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741222
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "SNOW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741223
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "SUNNY"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741224
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "TSTORMS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741225
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CHANCE_FLURRIES"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741226
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CHANCE_RAIN"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741227
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CHANCE_SLEET"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741228
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CHANCE_SNOW"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741229
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CHANCE_TSTORMS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741230
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CLEAR"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741231
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_CLOUDY"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741232
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_FLURRIES"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741233
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_FOG"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741234
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_HAZY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741235
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_MOSTLY_CLOUDY"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741236
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_MOSTLY_SUNNY"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741237
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_PARTLY_CLOUDY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741238
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_PARTLY_SUNNY"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741239
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_RAIN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741240
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_SLEET"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741241
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_SNOW"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741242
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_SUNNY"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741243
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const-string v1, "NT_TSTORMS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741244
    const/16 v0, 0x27

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741245
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .locals 2

    .prologue
    .line 741246
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 741247
    :goto_0
    return-object v0

    .line 741248
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 741249
    packed-switch v0, :pswitch_data_0

    .line 741250
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741251
    :pswitch_1
    const-string v0, "CHANCE_RAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741252
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741253
    :cond_2
    const-string v0, "MOSTLY_SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741254
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741255
    :cond_3
    const-string v0, "SNOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741257
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741258
    :pswitch_2
    const-string v0, "MOSTLY_CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 741259
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741260
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741261
    :pswitch_3
    const-string v0, "FOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 741262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741263
    :cond_6
    const-string v0, "NT_SLEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 741264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741265
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741266
    :pswitch_4
    const-string v0, "PARTLY_SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 741267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto :goto_0

    .line 741268
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741269
    :pswitch_5
    const-string v0, "PARTLY_CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 741270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741271
    :cond_9
    const-string v0, "SLEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 741272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741273
    :cond_a
    const-string v0, "NT_CLEAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 741274
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741275
    :cond_b
    const-string v0, "NT_MOSTLY_SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 741276
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741277
    :cond_c
    const-string v0, "NT_PARTLY_SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 741278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741279
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741280
    :pswitch_6
    const-string v0, "NT_CHANCE_SNOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 741281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741282
    :cond_e
    const-string v0, "NT_MOSTLY_CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 741283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_MOSTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741284
    :cond_f
    const-string v0, "NT_PARTLY_CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 741285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_PARTLY_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741286
    :cond_10
    const-string v0, "NT_TSTORMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 741287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741288
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741289
    :pswitch_7
    const-string v0, "NT_FLURRIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 741290
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741291
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741292
    :pswitch_8
    const-string v0, "NT_RAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 741293
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741294
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741295
    :pswitch_9
    const-string v0, "RAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 741296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741297
    :cond_14
    const-string v0, "TSTORMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 741298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741299
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741300
    :pswitch_a
    const-string v0, "NT_CHANCE_SLEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 741301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741302
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741303
    :pswitch_b
    const-string v0, "NT_CHANCE_TSTORMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 741304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741305
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741306
    :pswitch_c
    const-string v0, "NT_CHANCE_FLURRIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 741307
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741308
    :cond_18
    const-string v0, "NT_FOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 741309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_FOG:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741310
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741311
    :pswitch_d
    const-string v0, "NT_CHANCE_RAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 741312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CHANCE_RAIN:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741313
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741314
    :pswitch_e
    const-string v0, "CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 741315
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741316
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741317
    :pswitch_f
    const-string v0, "HAZY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 741318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741319
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741320
    :pswitch_10
    const-string v0, "CLEAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 741321
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CLEAR:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741322
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741323
    :pswitch_11
    const-string v0, "CHANCE_SNOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 741324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741325
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741326
    :pswitch_12
    const-string v0, "CHANCE_SLEET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 741327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_SLEET:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741328
    :cond_1f
    const-string v0, "FLURRIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 741329
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741330
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741331
    :pswitch_13
    const-string v0, "NT_HAZY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 741332
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_HAZY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741333
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741334
    :pswitch_14
    const-string v0, "NT_SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 741335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741336
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741337
    :pswitch_15
    const-string v0, "CHANCE_TSTORMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 741338
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_TSTORMS:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741339
    :cond_23
    const-string v0, "NT_CLOUDY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 741340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_CLOUDY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741341
    :cond_24
    const-string v0, "NT_SNOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 741342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->NT_SNOW:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741343
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741344
    :pswitch_16
    const-string v0, "CHANCE_FLURRIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 741345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->CHANCE_FLURRIES:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741346
    :cond_26
    const-string v0, "SUNNY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 741347
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->SUNNY:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    .line 741348
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .locals 1

    .prologue
    .line 741349
    const-class v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .locals 1

    .prologue
    .line 741350
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    return-object v0
.end method
