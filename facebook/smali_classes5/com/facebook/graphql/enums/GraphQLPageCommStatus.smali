.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCommStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCommStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public static final enum DONE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public static final enum FOLLOW_UP:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public static final enum SPAM:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public static final enum TODO:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 736849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736850
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    const-string v1, "FOLLOW_UP"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->FOLLOW_UP:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736851
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->DONE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736852
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    const-string v1, "TODO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->TODO:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736853
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    const-string v1, "SPAM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->SPAM:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736854
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->FOLLOW_UP:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->DONE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->TODO:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->SPAM:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 736855
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommStatus;
    .locals 1

    .prologue
    .line 736856
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 736857
    :goto_0
    return-object v0

    .line 736858
    :cond_1
    const-string v0, "FOLLOW_UP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 736859
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->FOLLOW_UP:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    goto :goto_0

    .line 736860
    :cond_2
    const-string v0, "DONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 736861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->DONE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    goto :goto_0

    .line 736862
    :cond_3
    const-string v0, "TODO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 736863
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->TODO:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    goto :goto_0

    .line 736864
    :cond_4
    const-string v0, "SPAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 736865
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->SPAM:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    goto :goto_0

    .line 736866
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommStatus;
    .locals 1

    .prologue
    .line 736867
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCommStatus;
    .locals 1

    .prologue
    .line 736868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    return-object v0
.end method
