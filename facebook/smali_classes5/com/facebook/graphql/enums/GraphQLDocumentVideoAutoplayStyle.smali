.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public static final enum AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public static final enum NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725202
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 725203
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    const-string v1, "AUTOPLAY"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 725204
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    const-string v1, "NO_AUTOPLAY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 725205
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725215
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1

    .prologue
    .line 725208
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 725209
    :goto_0
    return-object v0

    .line 725210
    :cond_1
    const-string v0, "AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725211
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    goto :goto_0

    .line 725212
    :cond_2
    const-string v0, "NO_AUTOPLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725213
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    goto :goto_0

    .line 725214
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1

    .prologue
    .line 725207
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1

    .prologue
    .line 725206
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    return-object v0
.end method
