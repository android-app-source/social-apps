.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

.field public static final enum CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

.field public static final enum NOTICE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727773
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    .line 727774
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    const-string v1, "NOTICE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->NOTICE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    .line 727775
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    const-string v1, "CONFIRMATION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    .line 727776
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->NOTICE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727772
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
    .locals 1

    .prologue
    .line 727777
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    .line 727778
    :goto_0
    return-object v0

    .line 727779
    :cond_1
    const-string v0, "NOTICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->NOTICE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    goto :goto_0

    .line 727781
    :cond_2
    const-string v0, "CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    goto :goto_0

    .line 727783
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
    .locals 1

    .prologue
    .line 727771
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
    .locals 1

    .prologue
    .line 727770
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    return-object v0
.end method
