.class public final enum Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum DOC:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum FILE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum MULTIMEDIA:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum MULTI_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum NOTE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum OFFER:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum PHOTO_SET:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum QUESTION:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum SINGLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum SINGLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738293
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738294
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "TEXT_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738295
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "SINGLE_PHOTO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738296
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "MULTI_PHOTO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTI_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738297
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "SINGLE_SHARE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738298
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "SINGLE_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738299
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "OFFER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->OFFER:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738300
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "MULTIMEDIA"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTIMEDIA:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738301
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "PRODUCT_ITEM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738302
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "DOC"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->DOC:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738303
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "QUESTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738304
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "PHOTO_SET"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PHOTO_SET:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738305
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "FILE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->FILE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738306
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738307
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "LEARNING_MODULE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738308
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    const-string v1, "NOTE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738309
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTI_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->OFFER:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTIMEDIA:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->DOC:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PHOTO_SET:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->FILE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738310
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
    .locals 1

    .prologue
    .line 738311
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    .line 738312
    :goto_0
    return-object v0

    .line 738313
    :cond_1
    const-string v0, "TEXT_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738315
    :cond_2
    const-string v0, "SINGLE_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738316
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738317
    :cond_3
    const-string v0, "MULTI_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738318
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTI_PHOTO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738319
    :cond_4
    const-string v0, "SINGLE_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738320
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738321
    :cond_5
    const-string v0, "SINGLE_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738323
    :cond_6
    const-string v0, "OFFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->OFFER:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738325
    :cond_7
    const-string v0, "MULTIMEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 738326
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->MULTIMEDIA:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738327
    :cond_8
    const-string v0, "PRODUCT_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 738328
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738329
    :cond_9
    const-string v0, "DOC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 738330
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->DOC:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738331
    :cond_a
    const-string v0, "QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738332
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->QUESTION:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738333
    :cond_b
    const-string v0, "PHOTO_SET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 738334
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->PHOTO_SET:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto :goto_0

    .line 738335
    :cond_c
    const-string v0, "FILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 738336
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->FILE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto/16 :goto_0

    .line 738337
    :cond_d
    const-string v0, "LEARNING_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 738338
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->LEARNING_MODULE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto/16 :goto_0

    .line 738339
    :cond_e
    const-string v0, "NOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 738340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->NOTE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto/16 :goto_0

    .line 738341
    :cond_f
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 738342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto/16 :goto_0

    .line 738343
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
    .locals 1

    .prologue
    .line 738344
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;
    .locals 1

    .prologue
    .line 738345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    return-object v0
.end method
