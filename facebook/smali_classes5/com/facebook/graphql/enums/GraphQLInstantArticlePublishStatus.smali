.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

.field public static final enum DRAFT:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

.field public static final enum LIVE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727546
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    .line 727547
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    const-string v1, "DRAFT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    .line 727548
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    .line 727549
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;
    .locals 1

    .prologue
    .line 727552
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    .line 727553
    :goto_0
    return-object v0

    .line 727554
    :cond_1
    const-string v0, "DRAFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727555
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    goto :goto_0

    .line 727556
    :cond_2
    const-string v0, "LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    goto :goto_0

    .line 727558
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;
    .locals 1

    .prologue
    .line 727551
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;
    .locals 1

    .prologue
    .line 727550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    return-object v0
.end method
