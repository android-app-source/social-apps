.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum DEFAULT:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum GAMING:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum POLITICIANS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum PROF_SERVICES:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum RESTAURANTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum SHOPS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737749
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737750
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737751
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "EVENTS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737752
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "GAMING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->GAMING:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737753
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "POLITICIANS"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->POLITICIANS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737754
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "PROF_SERVICES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->PROF_SERVICES:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737755
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "RESTAURANTS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->RESTAURANTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737756
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    const-string v1, "SHOPS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->SHOPS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737757
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->GAMING:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->POLITICIANS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->PROF_SERVICES:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->RESTAURANTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->SHOPS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737748
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .locals 1

    .prologue
    .line 737731
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 737732
    :goto_0
    return-object v0

    .line 737733
    :cond_1
    const-string v0, "DEFAULT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737734
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->DEFAULT:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737735
    :cond_2
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737736
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737737
    :cond_3
    const-string v0, "GAMING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737738
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->GAMING:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737739
    :cond_4
    const-string v0, "POLITICIANS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737740
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->POLITICIANS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737741
    :cond_5
    const-string v0, "PROF_SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737742
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->PROF_SERVICES:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737743
    :cond_6
    const-string v0, "RESTAURANTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737744
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->RESTAURANTS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737745
    :cond_7
    const-string v0, "SHOPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737746
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->SHOPS:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0

    .line 737747
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .locals 1

    .prologue
    .line 737730
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;
    .locals 1

    .prologue
    .line 737729
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    return-object v0
.end method
