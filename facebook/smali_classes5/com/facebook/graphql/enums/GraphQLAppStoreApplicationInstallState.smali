.class public final enum Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum NOT_INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723352
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723353
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "INSTALLING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723354
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "FAILED_INSTALL"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723355
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "NOT_INSTALLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->NOT_INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723356
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "INSTALLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723357
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    const-string v1, "CANCELED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723358
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->NOT_INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723359
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
    .locals 1

    .prologue
    .line 723360
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 723361
    :goto_0
    return-object v0

    .line 723362
    :cond_1
    const-string v0, "NOT_INSTALLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723363
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->NOT_INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723364
    :cond_2
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723366
    :cond_3
    const-string v0, "DOWNLOADING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723368
    :cond_4
    const-string v0, "INSTALLING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723370
    :cond_5
    const-string v0, "FAILED_INSTALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723371
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723372
    :cond_6
    const-string v0, "INSTALLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723374
    :cond_7
    const-string v0, "CANCELED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0

    .line 723376
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
    .locals 1

    .prologue
    .line 723377
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;
    .locals 1

    .prologue
    .line 723378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    return-object v0
.end method
