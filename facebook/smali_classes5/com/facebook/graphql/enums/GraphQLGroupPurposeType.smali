.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum CLUB:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum COUPLE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum CUSTOM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum EVENT_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum FITNESS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum FRATERNITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum NEIGHBORS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum PARENTS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum PROJECT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum REAL_WORLD:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum REAL_WORLD_AT_WORK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum SCHOOL_CLASS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum SORORITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum STUDY_GROUP:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum SUPPORT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum TEAMMATES:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum TRAVEL_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum WORK_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum WORK_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum WORK_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum WORK_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field public static final enum WORK_TEAM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "FAMILY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "CLOSE_FRIENDS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "NEIGHBORS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->NEIGHBORS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "TEAMMATES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TEAMMATES:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "FOR_SALE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "EVENT_PLANNING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->EVENT_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727121
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "SUPPORT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SUPPORT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "CLUB"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLUB:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727123
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "PROJECT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PROJECT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "SORORITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SORORITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "FRATERNITY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FRATERNITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "STUDY_GROUP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->STUDY_GROUP:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "SCHOOL_CLASS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SCHOOL_CLASS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727128
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "TRAVEL_PLANNING"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TRAVEL_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "COUPLE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->COUPLE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "PARENTS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PARENTS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "CUSTOM"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "WORK_TEAM"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_TEAM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "WORK_FEEDBACK"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "WORK_ANNOUNCEMENT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "WORK_SOCIAL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "WORK_MULTI_COMPANY"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "FITNESS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "REAL_WORLD"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    const-string v1, "REAL_WORLD_AT_WORK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD_AT_WORK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727140
    const/16 v0, 0x1a

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->NEIGHBORS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TEAMMATES:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->EVENT_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SUPPORT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLUB:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PROJECT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SORORITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FRATERNITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->STUDY_GROUP:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SCHOOL_CLASS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TRAVEL_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->COUPLE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PARENTS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_TEAM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD_AT_WORK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 1

    .prologue
    .line 727144
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 727145
    :goto_0
    return-object v0

    .line 727146
    :cond_1
    const-string v0, "FAMILY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727148
    :cond_2
    const-string v0, "CLOSE_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727150
    :cond_3
    const-string v0, "NEIGHBORS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727151
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->NEIGHBORS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727152
    :cond_4
    const-string v0, "TEAMMATES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727153
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TEAMMATES:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727154
    :cond_5
    const-string v0, "FOR_SALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727156
    :cond_6
    const-string v0, "EVENT_PLANNING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727157
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->EVENT_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727158
    :cond_7
    const-string v0, "SUPPORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727159
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SUPPORT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727160
    :cond_8
    const-string v0, "CLUB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CLUB:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727162
    :cond_9
    const-string v0, "PROJECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PROJECT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727164
    :cond_a
    const-string v0, "SORORITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727165
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SORORITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727166
    :cond_b
    const-string v0, "FRATERNITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FRATERNITY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto :goto_0

    .line 727168
    :cond_c
    const-string v0, "STUDY_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->STUDY_GROUP:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727170
    :cond_d
    const-string v0, "SCHOOL_CLASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SCHOOL_CLASS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727172
    :cond_e
    const-string v0, "TRAVEL_PLANNING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->TRAVEL_PLANNING:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727174
    :cond_f
    const-string v0, "COUPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->COUPLE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727176
    :cond_10
    const-string v0, "PARENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727177
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->PARENTS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727178
    :cond_11
    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727180
    :cond_12
    const-string v0, "WORK_TEAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 727181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_TEAM:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727182
    :cond_13
    const-string v0, "WORK_FEEDBACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 727183
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_FEEDBACK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727184
    :cond_14
    const-string v0, "WORK_ANNOUNCEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 727185
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_ANNOUNCEMENT:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727186
    :cond_15
    const-string v0, "WORK_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 727187
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727188
    :cond_16
    const-string v0, "WORK_MULTI_COMPANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 727189
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->WORK_MULTI_COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727190
    :cond_17
    const-string v0, "FITNESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 727191
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FITNESS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727192
    :cond_18
    const-string v0, "REAL_WORLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 727193
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727194
    :cond_19
    const-string v0, "REAL_WORLD_AT_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 727195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->REAL_WORLD_AT_WORK:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0

    .line 727196
    :cond_1a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 1

    .prologue
    .line 727142
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;
    .locals 1

    .prologue
    .line 727141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    return-object v0
.end method
