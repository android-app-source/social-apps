.class public final enum Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum BACKGROUND_ENROLL_AND_REDIRECT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum CLAIM:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum CLAIM_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum COUPON_ACTIVATE_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum COUPON_ASSIGN_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum COUPON_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum DELETE_PROGRESS:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum ENROLL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum ENROLL_AND_CLAIM_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum ENROLL_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum KICKEDOUT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum KICKEDOUT_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum MARK_INCOMPLETE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum MEETS_REQUIREMENT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum PRE_VALIDATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum PROMOTION_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum PROMOTION_GROUP_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum RENDER_TIP:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum TARGETING_VALIDATED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum UPDATE_PROGRESS_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum UPDATE_REMAINING_SPOTS_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum VIEW_ENROLL_SUCCESS_DIALOG:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

.field public static final enum VIEW_ENROLL_SUCCESS_MOBILE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724803
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "BACKGROUND_ENROLL_AND_REDIRECT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->BACKGROUND_ENROLL_AND_REDIRECT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "CLAIM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724806
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "CLAIM_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724807
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "COUPON_ASSIGN_FAIL"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ASSIGN_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724808
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "COUPON_ACTIVATE_FAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ACTIVATE_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724809
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "COUPON_IMPRESSION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724810
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "MARK_INCOMPLETE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MARK_INCOMPLETE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724811
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "DELETE_PROGRESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->DELETE_PROGRESS:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724812
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "ENROLL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724813
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "ENROLL_AND_CLAIM_FAIL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_AND_CLAIM_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724814
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "ENROLL_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724815
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "KICKEDOUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724816
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "KICKEDOUT_FAILED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724817
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "MEETS_REQUIREMENT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MEETS_REQUIREMENT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724818
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "PRE_VALIDATION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PRE_VALIDATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724819
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "PROMOTION_CREATION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724820
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "PROMOTION_GROUP_CREATION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_GROUP_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724821
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "RENDER_TIP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->RENDER_TIP:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724822
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "TARGETING_VALIDATED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->TARGETING_VALIDATED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724823
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "UPDATE_PROGRESS_FAILED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_PROGRESS_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724824
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "UPDATE_REMAINING_SPOTS_FAIL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_REMAINING_SPOTS_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724825
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "VIEW_ENROLL_SUCCESS_DIALOG"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_DIALOG:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724826
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    const-string v1, "VIEW_ENROLL_SUCCESS_MOBILE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_MOBILE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724827
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->BACKGROUND_ENROLL_AND_REDIRECT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ASSIGN_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ACTIVATE_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MARK_INCOMPLETE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->DELETE_PROGRESS:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_AND_CLAIM_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MEETS_REQUIREMENT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PRE_VALIDATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_GROUP_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->RENDER_TIP:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->TARGETING_VALIDATED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_PROGRESS_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_REMAINING_SPOTS_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_DIALOG:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_MOBILE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724828
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .locals 1

    .prologue
    .line 724829
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    .line 724830
    :goto_0
    return-object v0

    .line 724831
    :cond_1
    const-string v0, "BACKGROUND_ENROLL_AND_REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724832
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->BACKGROUND_ENROLL_AND_REDIRECT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724833
    :cond_2
    const-string v0, "CLAIM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724834
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724835
    :cond_3
    const-string v0, "CLAIM_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724836
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->CLAIM_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724837
    :cond_4
    const-string v0, "COUPON_ASSIGN_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ASSIGN_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724839
    :cond_5
    const-string v0, "COUPON_ACTIVATE_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724840
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_ACTIVATE_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724841
    :cond_6
    const-string v0, "COUPON_IMPRESSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->COUPON_IMPRESSION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724843
    :cond_7
    const-string v0, "MARK_INCOMPLETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724844
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MARK_INCOMPLETE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724845
    :cond_8
    const-string v0, "DELETE_PROGRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724846
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->DELETE_PROGRESS:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724847
    :cond_9
    const-string v0, "ENROLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724848
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724849
    :cond_a
    const-string v0, "ENROLL_AND_CLAIM_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724850
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_AND_CLAIM_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724851
    :cond_b
    const-string v0, "ENROLL_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 724852
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->ENROLL_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto :goto_0

    .line 724853
    :cond_c
    const-string v0, "KICKEDOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 724854
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724855
    :cond_d
    const-string v0, "KICKEDOUT_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 724856
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->KICKEDOUT_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724857
    :cond_e
    const-string v0, "MEETS_REQUIREMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 724858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->MEETS_REQUIREMENT:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724859
    :cond_f
    const-string v0, "PRE_VALIDATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 724860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PRE_VALIDATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724861
    :cond_10
    const-string v0, "PROMOTION_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 724862
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724863
    :cond_11
    const-string v0, "PROMOTION_GROUP_CREATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 724864
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->PROMOTION_GROUP_CREATION:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724865
    :cond_12
    const-string v0, "RENDER_TIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 724866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->RENDER_TIP:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724867
    :cond_13
    const-string v0, "TARGETING_VALIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 724868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->TARGETING_VALIDATED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724869
    :cond_14
    const-string v0, "UPDATE_PROGRESS_FAILED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 724870
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_PROGRESS_FAILED:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724871
    :cond_15
    const-string v0, "UPDATE_REMAINING_SPOTS_FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 724872
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UPDATE_REMAINING_SPOTS_FAIL:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724873
    :cond_16
    const-string v0, "VIEW_ENROLL_SUCCESS_DIALOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 724874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_DIALOG:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724875
    :cond_17
    const-string v0, "VIEW_ENROLL_SUCCESS_MOBILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 724876
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->VIEW_ENROLL_SUCCESS_MOBILE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0

    .line 724877
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .locals 1

    .prologue
    .line 724878
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;
    .locals 1

    .prologue
    .line 724879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCouponPromotionEventName;

    return-object v0
.end method
