.class public final enum Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

.field public static final enum CUISINE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

.field public static final enum PARKING:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

.field public static final enum SERVICES:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

.field public static final enum SPECIALTY:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    const-string v1, "CUISINE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->CUISINE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    const-string v1, "PARKING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->PARKING:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    const-string v1, "SPECIALTY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SPECIALTY:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    const-string v1, "SERVICES"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SERVICES:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724041
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->CUISINE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->PARKING:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SPECIALTY:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SERVICES:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724042
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;
    .locals 1

    .prologue
    .line 724043
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    .line 724044
    :goto_0
    return-object v0

    .line 724045
    :cond_1
    const-string v0, "CUISINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724046
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->CUISINE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    goto :goto_0

    .line 724047
    :cond_2
    const-string v0, "PARKING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724048
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->PARKING:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    goto :goto_0

    .line 724049
    :cond_3
    const-string v0, "SPECIALTY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724050
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SPECIALTY:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    goto :goto_0

    .line 724051
    :cond_4
    const-string v0, "SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724052
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SERVICES:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    goto :goto_0

    .line 724053
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;
    .locals 1

    .prologue
    .line 724054
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;
    .locals 1

    .prologue
    .line 724055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    return-object v0
.end method
