.class public final enum Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum DEPRECATED_13:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum DEPRECATED_14:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum FLOAT_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum GREATER_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum GREATER_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum IS_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum IS_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum LESS_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum LESS_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum MAX_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum MAX_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum MIN_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum MIN_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum NOT_BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum NOT_CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum NOT_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "GREATER_THAN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728049
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "GREATER_THAN_OR_EQUAL_TO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728050
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "LESS_THAN"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728051
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "LESS_THAN_OR_EQUAL_TO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728052
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "EQUAL_TO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "NOT_EQUAL_TO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "BETWEEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "NOT_BETWEEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728056
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "IS_NUMBER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "FLOAT_NUMBER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->FLOAT_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "CONTAIN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "NOT_CONTAIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728060
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "DEPRECATED_13"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->DEPRECATED_13:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728061
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "DEPRECATED_14"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->DEPRECATED_14:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728062
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "MAX_COUNT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "MIN_COUNT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728064
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "MAX_DATE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728065
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "MIN_DATE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    const-string v1, "IS_DATE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728067
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->FLOAT_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->DEPRECATED_13:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->DEPRECATED_14:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728068
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;
    .locals 1

    .prologue
    .line 728069
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    .line 728070
    :goto_0
    return-object v0

    .line 728071
    :cond_1
    const-string v0, "GREATER_THAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728072
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728073
    :cond_2
    const-string v0, "GREATER_THAN_OR_EQUAL_TO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728074
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->GREATER_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728075
    :cond_3
    const-string v0, "LESS_THAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728076
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728077
    :cond_4
    const-string v0, "LESS_THAN_OR_EQUAL_TO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728078
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->LESS_THAN_OR_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728079
    :cond_5
    const-string v0, "EQUAL_TO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728080
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728081
    :cond_6
    const-string v0, "NOT_EQUAL_TO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728082
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_EQUAL_TO:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728083
    :cond_7
    const-string v0, "BETWEEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728085
    :cond_8
    const-string v0, "NOT_BETWEEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 728086
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_BETWEEN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728087
    :cond_9
    const-string v0, "IS_NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 728088
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728089
    :cond_a
    const-string v0, "FLOAT_NUMBER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 728090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->FLOAT_NUMBER:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728091
    :cond_b
    const-string v0, "CONTAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 728092
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto :goto_0

    .line 728093
    :cond_c
    const-string v0, "NOT_CONTAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 728094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->NOT_CONTAIN:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728095
    :cond_d
    const-string v0, "MAX_COUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 728096
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728097
    :cond_e
    const-string v0, "MIN_COUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 728098
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_COUNT:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728099
    :cond_f
    const-string v0, "MAX_DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 728100
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MAX_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728101
    :cond_10
    const-string v0, "MIN_DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 728102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->MIN_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728103
    :cond_11
    const-string v0, "IS_DATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 728104
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->IS_DATE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0

    .line 728105
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;
    .locals 1

    .prologue
    .line 728106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;
    .locals 1

    .prologue
    .line 728107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    return-object v0
.end method
