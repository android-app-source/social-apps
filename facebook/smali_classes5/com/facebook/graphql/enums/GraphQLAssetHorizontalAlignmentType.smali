.class public final enum Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public static final enum CENTER:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public static final enum LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public static final enum RIGHT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 723397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 723398
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 723399
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->RIGHT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 723400
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->RIGHT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723401
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 1

    .prologue
    .line 723402
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    .line 723403
    :goto_0
    return-object v0

    .line 723404
    :cond_1
    const-string v0, "LEFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->LEFT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    goto :goto_0

    .line 723406
    :cond_2
    const-string v0, "RIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->RIGHT:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    goto :goto_0

    .line 723408
    :cond_3
    const-string v0, "CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723409
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    goto :goto_0

    .line 723410
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 1

    .prologue
    .line 723411
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .locals 1

    .prologue
    .line 723412
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    return-object v0
.end method
