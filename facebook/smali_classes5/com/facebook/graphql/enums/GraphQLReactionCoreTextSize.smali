.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    const-string v1, "EXTRA_LARGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738998
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
    .locals 1

    .prologue
    .line 738976
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    .line 738977
    :goto_0
    return-object v0

    .line 738978
    :cond_1
    const-string v0, "EXTRA_SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738979
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0

    .line 738980
    :cond_2
    const-string v0, "SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0

    .line 738982
    :cond_3
    const-string v0, "MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738983
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0

    .line 738984
    :cond_4
    const-string v0, "LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738985
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0

    .line 738986
    :cond_5
    const-string v0, "EXTRA_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0

    .line 738988
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
    .locals 1

    .prologue
    .line 738990
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;
    .locals 1

    .prologue
    .line 738989
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    return-object v0
.end method
