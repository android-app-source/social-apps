.class public final enum Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

.field public static final enum RECENTS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

.field public static final enum TOP_FRIENDS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    .line 728806
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    const-string v1, "RECENTS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->RECENTS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    .line 728807
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    const-string v1, "TOP_FRIENDS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->TOP_FRIENDS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    .line 728808
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->RECENTS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->TOP_FRIENDS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728809
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
    .locals 1

    .prologue
    .line 728810
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    .line 728811
    :goto_0
    return-object v0

    .line 728812
    :cond_1
    const-string v0, "RECENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->RECENTS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    goto :goto_0

    .line 728814
    :cond_2
    const-string v0, "TOP_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->TOP_FRIENDS:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    goto :goto_0

    .line 728816
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
    .locals 1

    .prologue
    .line 728817
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;
    .locals 1

    .prologue
    .line 728818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessengerBroadcastSectionType;

    return-object v0
.end method
