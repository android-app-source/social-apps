.class public final enum Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

.field public static final enum FIXED:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

.field public static final enum UNIT:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 726015
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    .line 726016
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    const-string v1, "UNIT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNIT:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    .line 726017
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    const-string v1, "FIXED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->FIXED:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    .line 726018
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNIT:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->FIXED:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726014
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;
    .locals 1

    .prologue
    .line 726005
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    .line 726006
    :goto_0
    return-object v0

    .line 726007
    :cond_1
    const-string v0, "UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726008
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNIT:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    goto :goto_0

    .line 726009
    :cond_2
    const-string v0, "FIXED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726010
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->FIXED:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    goto :goto_0

    .line 726011
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;
    .locals 1

    .prologue
    .line 726013
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;
    .locals 1

    .prologue
    .line 726012
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    return-object v0
.end method
