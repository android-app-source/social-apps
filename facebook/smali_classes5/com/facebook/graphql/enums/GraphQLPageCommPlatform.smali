.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

.field public static final enum FACEBOOK:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

.field public static final enum INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

.field public static final enum MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 736832
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 736833
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 736834
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 736835
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    const-string v1, "INSTAGRAM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 736836
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 736848
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;
    .locals 1

    .prologue
    .line 736839
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 736840
    :goto_0
    return-object v0

    .line 736841
    :cond_1
    const-string v0, "FACEBOOK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 736842
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    goto :goto_0

    .line 736843
    :cond_2
    const-string v0, "MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 736844
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    goto :goto_0

    .line 736845
    :cond_3
    const-string v0, "INSTAGRAM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 736846
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->INSTAGRAM:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    goto :goto_0

    .line 736847
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;
    .locals 1

    .prologue
    .line 736838
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;
    .locals 1

    .prologue
    .line 736837
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    return-object v0
.end method
