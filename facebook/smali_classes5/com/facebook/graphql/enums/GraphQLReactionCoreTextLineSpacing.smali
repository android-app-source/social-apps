.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    const-string v1, "EXTRA_LARGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738959
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738960
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
    .locals 1

    .prologue
    .line 738961
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    .line 738962
    :goto_0
    return-object v0

    .line 738963
    :cond_1
    const-string v0, "EXTRA_SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738964
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0

    .line 738965
    :cond_2
    const-string v0, "SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738966
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0

    .line 738967
    :cond_3
    const-string v0, "MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0

    .line 738969
    :cond_4
    const-string v0, "LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0

    .line 738971
    :cond_5
    const-string v0, "EXTRA_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738972
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0

    .line 738973
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
    .locals 1

    .prologue
    .line 738974
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;
    .locals 1

    .prologue
    .line 738975
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    return-object v0
.end method
