.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum CANNOT_SEE_CONTENT:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum CONNECTION_UNSAFE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum INTERNAL_GROUPS_DISABLED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726900
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726901
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "CANNOT_SEE_CONTENT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CANNOT_SEE_CONTENT:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726902
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "CONNECTION_UNSAFE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CONNECTION_UNSAFE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726903
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "LOGIN_APPROVALS_REQUIRED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726904
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "INTERNAL_GROUPS_DISABLED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->INTERNAL_GROUPS_DISABLED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726905
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726906
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CANNOT_SEE_CONTENT:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CONNECTION_UNSAFE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->INTERNAL_GROUPS_DISABLED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;
    .locals 1

    .prologue
    .line 726908
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    .line 726909
    :goto_0
    return-object v0

    .line 726910
    :cond_1
    const-string v0, "CANNOT_SEE_CONTENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726911
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CANNOT_SEE_CONTENT:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0

    .line 726912
    :cond_2
    const-string v0, "CONNECTION_UNSAFE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726913
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->CONNECTION_UNSAFE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0

    .line 726914
    :cond_3
    const-string v0, "LOGIN_APPROVALS_REQUIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726915
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->LOGIN_APPROVALS_REQUIRED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0

    .line 726916
    :cond_4
    const-string v0, "INTERNAL_GROUPS_DISABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726917
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->INTERNAL_GROUPS_DISABLED:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0

    .line 726918
    :cond_5
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726919
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0

    .line 726920
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;
    .locals 1

    .prologue
    .line 726921
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;
    .locals 1

    .prologue
    .line 726922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;

    return-object v0
.end method
