.class public final enum Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

.field public static final enum HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

.field public static final enum VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738191
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    .line 738192
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    .line 738193
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    .line 738194
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738190
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;
    .locals 1

    .prologue
    .line 738196
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    .line 738197
    :goto_0
    return-object v0

    .line 738198
    :cond_1
    const-string v0, "HORIZONTAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738199
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->HORIZONTAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    goto :goto_0

    .line 738200
    :cond_2
    const-string v0, "VERTICAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738201
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    goto :goto_0

    .line 738202
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;
    .locals 1

    .prologue
    .line 738203
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;
    .locals 1

    .prologue
    .line 738195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    return-object v0
.end method
