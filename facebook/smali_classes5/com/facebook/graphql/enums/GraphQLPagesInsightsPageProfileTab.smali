.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum ABOUT:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum GROUPS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum HOME:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum LIKES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum NOTES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum OTHER:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum POSTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum REVIEWS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737415
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->OTHER:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->HOME:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "POSTS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->POSTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "ABOUT"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->ABOUT:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "PHOTOS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "VIDEOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "REVIEWS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737423
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "EVENTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737424
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "NOTES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->NOTES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737425
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "LIKES"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->LIKES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737426
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    const-string v1, "GROUPS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->GROUPS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737427
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->OTHER:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->HOME:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->POSTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->ABOUT:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->NOTES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->LIKES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->GROUPS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737428
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;
    .locals 1

    .prologue
    .line 737429
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    .line 737430
    :goto_0
    return-object v0

    .line 737431
    :cond_1
    const-string v0, "OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->OTHER:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737433
    :cond_2
    const-string v0, "HOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->HOME:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737435
    :cond_3
    const-string v0, "POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->POSTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737437
    :cond_4
    const-string v0, "ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->ABOUT:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737439
    :cond_5
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737441
    :cond_6
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737443
    :cond_7
    const-string v0, "REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->REVIEWS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737445
    :cond_8
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->EVENTS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737447
    :cond_9
    const-string v0, "NOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 737448
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->NOTES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737449
    :cond_a
    const-string v0, "LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 737450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->LIKES:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737451
    :cond_b
    const-string v0, "GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 737452
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->GROUPS:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0

    .line 737453
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;
    .locals 1

    .prologue
    .line 737454
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;
    .locals 1

    .prologue
    .line 737455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    return-object v0
.end method
