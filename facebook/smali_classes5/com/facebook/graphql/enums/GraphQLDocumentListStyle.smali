.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

.field public static final enum ORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

.field public static final enum UNORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 725100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    const-string v1, "ORDERED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->ORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 725101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    const-string v1, "UNORDERED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 725102
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->ORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .locals 1

    .prologue
    .line 725104
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 725105
    :goto_0
    return-object v0

    .line 725106
    :cond_1
    const-string v0, "ORDERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725107
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->ORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    goto :goto_0

    .line 725108
    :cond_2
    const-string v0, "UNORDERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    goto :goto_0

    .line 725110
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .locals 1

    .prologue
    .line 725111
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;
    .locals 1

    .prologue
    .line 725112
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    return-object v0
.end method
