.class public final enum Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum CREDIT_CARDS:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

.field public static final enum VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737304
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737305
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "VISA"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737306
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "CASH_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737307
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "MASTERCARD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737308
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "AMEX"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737309
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "DISCOVER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737310
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    const-string v1, "CREDIT_CARDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CREDIT_CARDS:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737311
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CREDIT_CARDS:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;
    .locals 1

    .prologue
    .line 737289
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 737290
    :goto_0
    return-object v0

    .line 737291
    :cond_1
    const-string v0, "CASH_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737292
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737293
    :cond_2
    const-string v0, "VISA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737295
    :cond_3
    const-string v0, "AMEX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737296
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737297
    :cond_4
    const-string v0, "MASTERCARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737299
    :cond_5
    const-string v0, "DISCOVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737301
    :cond_6
    const-string v0, "CREDIT_CARDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CREDIT_CARDS:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0

    .line 737303
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;
    .locals 1

    .prologue
    .line 737288
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;
    .locals 1

    .prologue
    .line 737287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    return-object v0
.end method
