.class public final enum Lcom/facebook/graphql/enums/GraphQLPageActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum ADS_MANAGER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum COPY_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_AD:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_EVENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_GROUP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_MILESTONE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum CREATE_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum EDIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum FAVOURITES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum GET_DIRECTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum GET_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum INVITE_EMAIL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_EMAIL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_LISTEN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LEGACY_CTA_WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum LIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum MENTIONS_PROMPT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum MODAL_PAGE_INVITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum OPEN_WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PAGE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PAGE_OPEN_ACTION_EDIT_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PLACE_CLAIM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PLACE_CLAIM_REFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum POST:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PRIMARY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum PROMOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum REPORT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum REQUEST_RIDE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SEND_AS_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SETTINGS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SHARE_MESSAGE_SHORTLINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SHOW_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SUGGEST_EDITS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum SWITCH_REGION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_BOOK_PREVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_ENDORSEMENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_ISSUES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_LIKES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_MUSIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_NOTES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum UNLIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum VIEW_AS_PUBLIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum VIEW_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum VIEW_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public static final enum VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729660
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729661
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729662
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "FOLLOW"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729663
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729664
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SAVE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729665
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CHECKIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729666
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "GET_NOTIFICATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729667
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SHARE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729668
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "REVIEW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729669
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SEND_AS_MESSAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SEND_AS_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729670
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "VIEW_AS_PUBLIC"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_AS_PUBLIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729671
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SHOW_LINK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHOW_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729672
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PLACE_CLAIM"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729673
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SUGGEST_EDITS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SUGGEST_EDITS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729674
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "REPORT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REPORT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729675
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_PAGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729676
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PAGES_FEED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729677
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SWITCH_REGION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SWITCH_REGION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729678
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "POST"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->POST:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729679
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PHOTO"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729680
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_EVENT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_EVENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729681
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "ADS_MANAGER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ADS_MANAGER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729682
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PROMOTE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PROMOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729683
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "EDIT_PAGE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->EDIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729684
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SETTINGS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729685
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "FAVOURITES"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FAVOURITES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729686
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "COPY_LINK"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729687
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CALL"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729688
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "GET_DIRECTION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_DIRECTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729689
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "OPEN_WEBSITE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->OPEN_WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729690
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_GROUP"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_GROUP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729691
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "VIEW_MENU"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729692
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_SHORTCUT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729693
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SHARE_MESSAGE_SHORTLINK"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_MESSAGE_SHORTLINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729694
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_ABOUT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729695
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_ACTIVITY"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729696
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_BOOK_PREVIEW"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_BOOK_PREVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729697
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_CUSTOM"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729698
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_EVENTS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729699
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_GROUPS"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729700
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_HOME"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729701
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_ISSUES"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ISSUES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729702
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_LOCATIONS"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729703
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_MENU"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729704
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_MUSIC"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MUSIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729705
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_OFFERS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729706
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_PHOTOS"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729707
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_POSTS"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729708
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_REVIEWS"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729709
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_SERVICES"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729710
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_SHOP"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729711
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_VIDEOS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729712
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LIKE_AS_YOUR_PAGE"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729713
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "INVITE_FRIENDS"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729714
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "BLOCK_PAGE"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729715
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "UNLIKE_AS_YOUR_PAGE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNLIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "VIEW_INSIGHTS"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "INVITE_EMAIL_CONTACTS"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_EMAIL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_COMMUNITY"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729719
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PLACE_CLAIM_REFER"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM_REFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "MENTIONS_PROMPT"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MENTIONS_PROMPT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PRIMARY_CTA"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PRIMARY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729722
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PAGE_ADMIN_MESSAGE"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729723
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_LIKES"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIKES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729724
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_JOBS"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729725
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_NOTES"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_NOTES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729726
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PAGE_PREVIEW_ONLY_ACTION"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729727
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "PAGE_OPEN_ACTION_EDIT_ACTION"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_OPEN_ACTION_EDIT_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729728
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_FUNDRAISERS"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729729
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "DELETE_TAB"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729730
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "REORDER_TABS"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729731
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "SHARE_TAB"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729732
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729733
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_FUNDRAISER"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729734
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "COPY_TAB_LINK"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729735
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "VISIT_PAGE"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729736
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_LIVE_VIDEOS"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729737
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "TAB_ENDORSEMENTS"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ENDORSEMENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729738
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_ADD_BUTTON"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729739
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_BOOK_APPOINTMENT"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729740
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_BOOK_NOW"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729741
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_CALL_NOW"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729742
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_CHARITY_DONATE"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729743
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_CONTACT_US"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729744
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_DONATE_NOW"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729745
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_EMAIL"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_EMAIL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729746
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_GET_DIRECTIONS"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729747
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_GET_OFFER"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729748
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_GET_OFFER_VIEW"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729749
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_LEARN_MORE"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729750
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_LISTEN"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LISTEN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729751
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_MAKE_RESERVATION"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729752
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_MESSAGE"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729753
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_OPEN_APP"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729754
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_ORDER_NOW"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729755
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_PLAY_NOW"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729756
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_READ_ARTICLES"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729757
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_REQUEST_APPOINTMENT"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729758
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_REQUEST_QUOTE"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729759
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_SHOP_NOW"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729760
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_SIGN_UP"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729761
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_VIDEO_CALL"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729762
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_WATCH_NOW"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729763
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_AD"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_AD:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729764
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "REQUEST_RIDE"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REQUEST_RIDE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729765
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "MODAL_PAGE_INVITE"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MODAL_PAGE_INVITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729766
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "LEGACY_CTA_BUY_TICKETS"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729767
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const-string v1, "CREATE_MILESTONE"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_MILESTONE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729768
    const/16 v0, 0x6c

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SEND_AS_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_AS_PUBLIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHOW_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SUGGEST_EDITS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REPORT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SWITCH_REGION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->POST:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_EVENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ADS_MANAGER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PROMOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->EDIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FAVOURITES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_DIRECTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->OPEN_WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_GROUP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_MESSAGE_SHORTLINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_BOOK_PREVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ISSUES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MUSIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNLIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_EMAIL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM_REFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MENTIONS_PROMPT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PRIMARY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIKES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_NOTES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_OPEN_ACTION_EDIT_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ENDORSEMENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_EMAIL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LISTEN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_AD:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REQUEST_RIDE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MODAL_PAGE_INVITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_MILESTONE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 2

    .prologue
    .line 729772
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 729773
    :goto_0
    return-object v0

    .line 729774
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 729775
    packed-switch v0, :pswitch_data_0

    .line 729776
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729777
    :pswitch_1
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729778
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->POST:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729779
    :cond_2
    const-string v0, "LEGACY_CTA_CHARITY_DONATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729780
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CHARITY_DONATE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729781
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729782
    :pswitch_2
    const-string v0, "VIEW_AS_PUBLIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729783
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_AS_PUBLIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729784
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729785
    :pswitch_3
    const-string v0, "CREATE_FUNDRAISER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729786
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_FUNDRAISER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729787
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729788
    :pswitch_4
    const-string v0, "UNLIKE_AS_YOUR_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729789
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNLIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729790
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729791
    :pswitch_5
    const-string v0, "REPORT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 729792
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REPORT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729793
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto :goto_0

    .line 729794
    :pswitch_6
    const-string v0, "GET_DIRECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 729795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_DIRECTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729796
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729797
    :pswitch_7
    const-string v0, "MENTIONS_PROMPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 729798
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MENTIONS_PROMPT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729799
    :cond_9
    const-string v0, "CREATE_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 729800
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_AD:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729801
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729802
    :pswitch_8
    const-string v0, "GET_NOTIFICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 729803
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->GET_NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729804
    :cond_b
    const-string v0, "TAB_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 729805
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729806
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729807
    :pswitch_9
    const-string v0, "TAB_SHOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 729808
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729809
    :cond_d
    const-string v0, "DELETE_TAB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 729810
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729811
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729812
    :pswitch_a
    const-string v0, "LEGACY_CTA_GET_OFFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 729813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729814
    :cond_f
    const-string v0, "LEGACY_CTA_SIGN_UP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 729815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SIGN_UP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729816
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729817
    :pswitch_b
    const-string v0, "PLACE_CLAIM_REFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 729818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM_REFER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729819
    :cond_11
    const-string v0, "LEGACY_CTA_LISTEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 729820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LISTEN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729821
    :cond_12
    const-string v0, "LEGACY_CTA_OPEN_APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 729822
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_OPEN_APP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729823
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729824
    :pswitch_c
    const-string v0, "LEGACY_CTA_EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 729825
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_EMAIL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729826
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729827
    :pswitch_d
    const-string v0, "SWITCH_REGION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 729828
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SWITCH_REGION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729829
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729830
    :pswitch_e
    const-string v0, "LEGACY_CTA_ADD_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 729831
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ADD_BUTTON:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729832
    :cond_16
    const-string v0, "LEGACY_CTA_BOOK_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 729833
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729834
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729835
    :pswitch_f
    const-string v0, "FOLLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 729836
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729837
    :cond_18
    const-string v0, "LEGACY_CTA_VIDEO_CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 729838
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_VIDEO_CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729839
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729840
    :pswitch_10
    const-string v0, "PAGES_FEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 729841
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGES_FEED:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729842
    :cond_1a
    const-string v0, "LEGACY_CTA_REQUEST_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 729843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729844
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729845
    :pswitch_11
    const-string v0, "LEGACY_CTA_MAKE_RESERVATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 729846
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MAKE_RESERVATION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729847
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729848
    :pswitch_12
    const-string v0, "PAGE_PREVIEW_ONLY_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 729849
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729850
    :cond_1d
    const-string v0, "SHARE_TAB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 729851
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729852
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729853
    :pswitch_13
    const-string v0, "FAVOURITES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 729854
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FAVOURITES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729855
    :cond_1f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729856
    :pswitch_14
    const-string v0, "PAGE_OPEN_ACTION_EDIT_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 729857
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_OPEN_ACTION_EDIT_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729858
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729859
    :pswitch_15
    const-string v0, "REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 729860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729861
    :cond_21
    const-string v0, "COPY_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 729862
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729863
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729864
    :pswitch_16
    const-string v0, "INVITE_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 729865
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729866
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729867
    :pswitch_17
    const-string v0, "COPY_TAB_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 729868
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729869
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729870
    :pswitch_18
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 729871
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729872
    :cond_25
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729873
    :pswitch_19
    const-string v0, "BLOCK_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 729874
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729875
    :cond_26
    const-string v0, "TAB_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 729876
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729877
    :cond_27
    const-string v0, "TAB_ACTIVITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 729878
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729879
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729880
    :pswitch_1a
    const-string v0, "SETTINGS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 729881
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SETTINGS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729882
    :cond_29
    const-string v0, "LEGACY_CTA_BOOK_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 729883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729884
    :cond_2a
    const-string v0, "LEGACY_CTA_CALL_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 729885
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729886
    :cond_2b
    const-string v0, "LEGACY_CTA_PLAY_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 729887
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_PLAY_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729888
    :cond_2c
    const-string v0, "LEGACY_CTA_SHOP_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 729889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729890
    :cond_2d
    const-string v0, "TAB_COMMUNITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 729891
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729892
    :cond_2e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729893
    :pswitch_1b
    const-string v0, "CREATE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 729894
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729895
    :cond_2f
    const-string v0, "EDIT_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 729896
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->EDIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729897
    :cond_30
    const-string v0, "LEGACY_CTA_ORDER_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 729898
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_ORDER_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729899
    :cond_31
    const-string v0, "LEGACY_CTA_WATCH_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 729900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_WATCH_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729901
    :cond_32
    const-string v0, "TAB_JOBS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 729902
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729903
    :cond_33
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729904
    :pswitch_1c
    const-string v0, "VIEW_MENU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 729905
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729906
    :cond_34
    const-string v0, "LEGACY_CTA_DONATE_NOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 729907
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729908
    :cond_35
    const-string v0, "TAB_LIKES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 729909
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIKES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729910
    :cond_36
    const-string v0, "TAB_NOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 729911
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_NOTES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729912
    :cond_37
    const-string v0, "TAB_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 729913
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729914
    :cond_38
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729915
    :pswitch_1d
    const-string v0, "LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 729916
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729917
    :cond_39
    const-string v0, "INVITE_EMAIL_CONTACTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 729918
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->INVITE_EMAIL_CONTACTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729919
    :cond_3a
    const-string v0, "TAB_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 729920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729921
    :cond_3b
    const-string v0, "TAB_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 729922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729923
    :cond_3c
    const-string v0, "TAB_ISSUES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 729924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ISSUES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729925
    :cond_3d
    const-string v0, "TAB_OFFERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 729926
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729927
    :cond_3e
    const-string v0, "TAB_PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 729928
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729929
    :cond_3f
    const-string v0, "TAB_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 729930
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729931
    :cond_40
    const-string v0, "REORDER_TABS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 729932
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729933
    :cond_41
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729934
    :pswitch_1e
    const-string v0, "TAB_REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 729935
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729936
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729937
    :pswitch_1f
    const-string v0, "SUGGEST_EDITS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 729938
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SUGGEST_EDITS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729939
    :cond_43
    const-string v0, "TAB_BOOK_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 729940
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_BOOK_PREVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729941
    :cond_44
    const-string v0, "TAB_SERVICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 729942
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729943
    :cond_45
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729944
    :pswitch_20
    const-string v0, "PLACE_CLAIM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 729945
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PLACE_CLAIM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729946
    :cond_46
    const-string v0, "CREATE_MILESTONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 729947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_MILESTONE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729948
    :cond_47
    const-string v0, "LEGACY_CTA_CONTACT_US"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 729949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729950
    :cond_48
    const-string v0, "LEGACY_CTA_GET_OFFER_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 729951
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_OFFER_VIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729952
    :cond_49
    const-string v0, "TAB_LOCATIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 729953
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729954
    :cond_4a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729955
    :pswitch_21
    const-string v0, "MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 729956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729957
    :cond_4b
    const-string v0, "LEGACY_CTA_BUY_TICKETS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 729958
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729959
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729960
    :pswitch_22
    const-string v0, "VIEW_INSIGHTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 729961
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VIEW_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729962
    :cond_4d
    const-string v0, "TAB_FUNDRAISERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 729963
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_FUNDRAISERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729964
    :cond_4e
    const-string v0, "TAB_LIVE_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 729965
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIVE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729966
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729967
    :pswitch_23
    const-string v0, "SHOW_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 729968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHOW_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729969
    :cond_50
    const-string v0, "LEGACY_CTA_READ_ARTICLES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 729970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_READ_ARTICLES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729971
    :cond_51
    const-string v0, "TAB_CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 729972
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729973
    :cond_52
    const-string v0, "TAB_ENDORSEMENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 729974
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ENDORSEMENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729975
    :cond_53
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729976
    :pswitch_24
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 729977
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729978
    :cond_54
    const-string v0, "PROMOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 729979
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PROMOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729980
    :cond_55
    const-string v0, "LEGACY_CTA_GET_DIRECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 729981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729982
    :cond_56
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729983
    :pswitch_25
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 729984
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729985
    :cond_57
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729986
    :pswitch_26
    const-string v0, "LEGACY_CTA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 729987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729988
    :cond_58
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729989
    :pswitch_27
    const-string v0, "OPEN_WEBSITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 729990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->OPEN_WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729991
    :cond_59
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729992
    :pswitch_28
    const-string v0, "TAB_HOME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 729993
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729994
    :cond_5a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729995
    :pswitch_29
    const-string v0, "LIKE_AS_YOUR_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 729996
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE_AS_YOUR_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729997
    :cond_5b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 729998
    :pswitch_2a
    const-string v0, "REQUEST_RIDE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 729999
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REQUEST_RIDE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730000
    :cond_5c
    const-string v0, "MODAL_PAGE_INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 730001
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->MODAL_PAGE_INVITE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730002
    :cond_5d
    const-string v0, "LEGACY_CTA_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 730003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730004
    :cond_5e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730005
    :pswitch_2b
    const-string v0, "ADS_MANAGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 730006
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ADS_MANAGER:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730007
    :cond_5f
    const-string v0, "TAB_MUSIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 730008
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MUSIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730009
    :cond_60
    const-string v0, "PRIMARY_CTA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 730010
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PRIMARY_CTA:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730011
    :cond_61
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730012
    :pswitch_2c
    const-string v0, "CREATE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 730013
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_EVENT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730014
    :cond_62
    const-string v0, "CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 730015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CALL:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730016
    :cond_63
    const-string v0, "VISIT_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 730017
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730018
    :cond_64
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730019
    :pswitch_2d
    const-string v0, "CHECKIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 730020
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730021
    :cond_65
    const-string v0, "LEGACY_CTA_LEARN_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 730022
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730023
    :cond_66
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730024
    :pswitch_2e
    const-string v0, "SEND_AS_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 730025
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SEND_AS_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730026
    :cond_67
    const-string v0, "PAGE_ADMIN_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 730027
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_ADMIN_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730028
    :cond_68
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730029
    :pswitch_2f
    const-string v0, "CREATE_SHORTCUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 730030
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_SHORTCUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730031
    :cond_69
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730032
    :pswitch_30
    const-string v0, "CREATE_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 730033
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->CREATE_GROUP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730034
    :cond_6a
    const-string v0, "SHARE_MESSAGE_SHORTLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 730035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_MESSAGE_SHORTLINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730036
    :cond_6b
    const-string v0, "LEGACY_CTA_REQUEST_QUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 730037
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LEGACY_CTA_REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    .line 730038
    :cond_6c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 1

    .prologue
    .line 729770
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 1

    .prologue
    .line 729769
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method
