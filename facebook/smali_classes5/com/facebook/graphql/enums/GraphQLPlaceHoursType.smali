.class public final enum Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public static final enum ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public static final enum NO_HOURS:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public static final enum OPEN_FOR_SELECTED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public static final enum PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738172
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738173
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    const-string v1, "PERMANENTLY_CLOSED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738174
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    const-string v1, "ALWAYS_OPEN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738175
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    const-string v1, "NO_HOURS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->NO_HOURS:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738176
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    const-string v1, "OPEN_FOR_SELECTED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->OPEN_FOR_SELECTED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738177
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->NO_HOURS:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->OPEN_FOR_SELECTED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;
    .locals 1

    .prologue
    .line 738179
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 738180
    :goto_0
    return-object v0

    .line 738181
    :cond_1
    const-string v0, "PERMANENTLY_CLOSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    goto :goto_0

    .line 738183
    :cond_2
    const-string v0, "ALWAYS_OPEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->ALWAYS_OPEN:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    goto :goto_0

    .line 738185
    :cond_3
    const-string v0, "NO_HOURS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->NO_HOURS:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    goto :goto_0

    .line 738187
    :cond_4
    const-string v0, "OPEN_FOR_SELECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->OPEN_FOR_SELECTED:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    goto :goto_0

    .line 738189
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;
    .locals 1

    .prologue
    .line 738170
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;
    .locals 1

    .prologue
    .line 738171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    return-object v0
.end method
