.class public final enum Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

.field public static final enum LIVE_BUTTON:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

.field public static final enum LIVE_LOGO:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 726454
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    .line 726455
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->NONE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    .line 726456
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    const-string v1, "LIVE_LOGO"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_LOGO:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    .line 726457
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    const-string v1, "LIVE_BUTTON"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_BUTTON:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    .line 726458
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->NONE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_LOGO:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_BUTTON:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726459
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;
    .locals 1

    .prologue
    .line 726460
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    .line 726461
    :goto_0
    return-object v0

    .line 726462
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->NONE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    goto :goto_0

    .line 726464
    :cond_2
    const-string v0, "LIVE_LOGO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726465
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_LOGO:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    goto :goto_0

    .line 726466
    :cond_3
    const-string v0, "LIVE_BUTTON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->LIVE_BUTTON:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    goto :goto_0

    .line 726468
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;
    .locals 1

    .prologue
    .line 726469
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;
    .locals 1

    .prologue
    .line 726470
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFacecastNuxVideoOverlayTextEventFeatureName;

    return-object v0
.end method
