.class public final enum Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public static final enum ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public static final enum GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public static final enum MARS:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public static final enum MOON:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 575442
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575443
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const-string v1, "GENERAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575444
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const-string v1, "ALDRIN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575445
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const-string v1, "MOON"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MOON:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575446
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    const-string v1, "MARS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MARS:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575447
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MOON:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MARS:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 575441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .locals 1

    .prologue
    .line 575430
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575431
    :goto_0
    return-object v0

    .line 575432
    :cond_1
    const-string v0, "GENERAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0

    .line 575434
    :cond_2
    const-string v0, "ALDRIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 575435
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0

    .line 575436
    :cond_3
    const-string v0, "MOON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 575437
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MOON:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0

    .line 575438
    :cond_4
    const-string v0, "MARS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 575439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->MARS:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0

    .line 575440
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .locals 1

    .prologue
    .line 575429
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .locals 1

    .prologue
    .line 575428
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    return-object v0
.end method
