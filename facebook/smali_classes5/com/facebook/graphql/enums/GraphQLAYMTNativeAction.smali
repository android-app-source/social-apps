.class public final enum Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum ADD_GROUP_ADMINS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum ADD_GROUP_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum ADD_GROUP_MEMBERS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum ADD_PAGE_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum ADD_PAGE_PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum CREATE_GROUP_GENERAL_ROOM:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum CREATE_GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum CREATE_GROUP_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum CREATE_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum INVITE_FRIENDS_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum MANAGE_ALL_PAGES:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

.field public static final enum WRITE_GROUP_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "ADD_PAGE_COVER_PHOTO"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "ADD_PAGE_PROFILE_PIC"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "CREATE_PAGE_POST"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "INVITE_FRIENDS_TO_LIKE_PAGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->INVITE_FRIENDS_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "MANAGE_ALL_PAGES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->MANAGE_ALL_PAGES:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "ADD_GROUP_ADMINS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_ADMINS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "ADD_GROUP_COVER_PHOTO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "ADD_GROUP_MEMBERS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_MEMBERS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "CREATE_GROUP_POLL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "CREATE_GROUP_POST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "WRITE_GROUP_DESCRIPTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->WRITE_GROUP_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    const-string v1, "CREATE_GROUP_GENERAL_ROOM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_GENERAL_ROOM:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723144
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->INVITE_FRIENDS_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->MANAGE_ALL_PAGES:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_ADMINS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_MEMBERS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->WRITE_GROUP_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_GENERAL_ROOM:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
    .locals 1

    .prologue
    .line 723146
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    .line 723147
    :goto_0
    return-object v0

    .line 723148
    :cond_1
    const-string v0, "ADD_PAGE_COVER_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723149
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723150
    :cond_2
    const-string v0, "ADD_PAGE_PROFILE_PIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723151
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_PAGE_PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723152
    :cond_3
    const-string v0, "CREATE_PAGE_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723153
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723154
    :cond_4
    const-string v0, "INVITE_FRIENDS_TO_LIKE_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723155
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->INVITE_FRIENDS_TO_LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723156
    :cond_5
    const-string v0, "MANAGE_ALL_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723157
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->MANAGE_ALL_PAGES:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723158
    :cond_6
    const-string v0, "CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723159
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_BUSINESS_ACTIVITY_FEED_PERF_NOTIF:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723160
    :cond_7
    const-string v0, "ADD_GROUP_ADMINS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_ADMINS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723162
    :cond_8
    const-string v0, "ADD_GROUP_COVER_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 723163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723164
    :cond_9
    const-string v0, "ADD_GROUP_MEMBERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 723165
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_MEMBERS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723166
    :cond_a
    const-string v0, "CREATE_GROUP_POLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 723167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723168
    :cond_b
    const-string v0, "CREATE_GROUP_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 723169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto :goto_0

    .line 723170
    :cond_c
    const-string v0, "WRITE_GROUP_DESCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 723171
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->WRITE_GROUP_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto/16 :goto_0

    .line 723172
    :cond_d
    const-string v0, "CREATE_GROUP_GENERAL_ROOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 723173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_GENERAL_ROOM:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto/16 :goto_0

    .line 723174
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
    .locals 1

    .prologue
    .line 723175
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;
    .locals 1

    .prologue
    .line 723176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    return-object v0
.end method
