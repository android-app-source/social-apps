.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum DATE_HEADER:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum EVENT_COLLECTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum FACEBOOK_EVENT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum MULTI_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum NATIVE_AD:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum RELATED_ARTICLES:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum SUBSCRIPTION_OPTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

.field public static final enum WEBVIEW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "RICH_TEXT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "SLIDESHOW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "MAP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "LIST_ITEM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "WEBVIEW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->WEBVIEW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "MULTI_TEXT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MULTI_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "NATIVE_AD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->NATIVE_AD:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "RELATED_ARTICLES"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RELATED_ARTICLES:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "SUBSCRIPTION_OPTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SUBSCRIPTION_OPTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "FACEBOOK_EVENT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->FACEBOOK_EVENT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "DATE_HEADER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->DATE_HEADER:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    const-string v1, "EVENT_COLLECTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->EVENT_COLLECTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725039
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->WEBVIEW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MULTI_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->NATIVE_AD:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RELATED_ARTICLES:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SUBSCRIPTION_OPTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->FACEBOOK_EVENT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->DATE_HEADER:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->EVENT_COLLECTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725023
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 725040
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 725041
    :goto_0
    return-object v0

    .line 725042
    :cond_1
    const-string v0, "RICH_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725043
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725044
    :cond_2
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725045
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725046
    :cond_3
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725047
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725048
    :cond_4
    const-string v0, "SLIDESHOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725049
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725050
    :cond_5
    const-string v0, "MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725051
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725052
    :cond_6
    const-string v0, "LIST_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725053
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725054
    :cond_7
    const-string v0, "WEBVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725055
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->WEBVIEW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725056
    :cond_8
    const-string v0, "MULTI_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725057
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MULTI_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725058
    :cond_9
    const-string v0, "NATIVE_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 725059
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->NATIVE_AD:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725060
    :cond_a
    const-string v0, "RELATED_ARTICLES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 725061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RELATED_ARTICLES:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725062
    :cond_b
    const-string v0, "SUBSCRIPTION_OPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 725063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SUBSCRIPTION_OPTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto :goto_0

    .line 725064
    :cond_c
    const-string v0, "FACEBOOK_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 725065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->FACEBOOK_EVENT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto/16 :goto_0

    .line 725066
    :cond_d
    const-string v0, "DATE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 725067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->DATE_HEADER:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto/16 :goto_0

    .line 725068
    :cond_e
    const-string v0, "EVENT_COLLECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 725069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->EVENT_COLLECTION:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto/16 :goto_0

    .line 725070
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 725022
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 725021
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method
