.class public final enum Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum BASIC:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum SELECTED:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 578073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "BASIC"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->BASIC:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578075
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->SELECTED:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "MOST_RECENT"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "FRIEND_LIST"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "COMMUNITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "QUERY_SNAPSHOT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const-string v1, "EXPANDABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578081
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->BASIC:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->SELECTED:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 578053
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 1

    .prologue
    .line 578056
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578057
    :goto_0
    return-object v0

    .line 578058
    :cond_1
    const-string v0, "BASIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578059
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->BASIC:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578060
    :cond_2
    const-string v0, "SELECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 578061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->SELECTED:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578062
    :cond_3
    const-string v0, "MOST_RECENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 578063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->MOST_RECENT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578064
    :cond_4
    const-string v0, "FRIEND_LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 578065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->FRIEND_LIST:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578066
    :cond_5
    const-string v0, "COMMUNITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 578067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578068
    :cond_6
    const-string v0, "QUERY_SNAPSHOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 578069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578070
    :cond_7
    const-string v0, "EXPANDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 578071
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0

    .line 578072
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 1

    .prologue
    .line 578055
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 1

    .prologue
    .line 578054
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-object v0
.end method
