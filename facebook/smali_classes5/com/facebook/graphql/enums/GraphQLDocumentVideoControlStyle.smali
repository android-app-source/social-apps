.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field public static final enum CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field public static final enum NO_CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 725216
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 725217
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    const-string v1, "CONTROLS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 725218
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    const-string v1, "NO_CONTROLS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->NO_CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 725219
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->NO_CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1

    .prologue
    .line 725221
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 725222
    :goto_0
    return-object v0

    .line 725223
    :cond_1
    const-string v0, "CONTROLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725224
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    goto :goto_0

    .line 725225
    :cond_2
    const-string v0, "NO_CONTROLS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->NO_CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    goto :goto_0

    .line 725227
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1

    .prologue
    .line 725228
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1

    .prologue
    .line 725229
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    return-object v0
.end method
