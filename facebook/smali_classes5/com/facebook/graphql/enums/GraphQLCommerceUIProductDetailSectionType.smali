.class public final enum Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum ABOUT_THE_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum INSIGHTS_AND_PROMOTION:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum MERCHANT_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum MERCHANT_TERMS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum MORE_FROM_SHOP:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum PHOTOS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum POLICIES:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum PRODUCT_AND_PURCHASE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum PRODUCT_NAME_AND_PRICE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum RECOMMENDED_PRODUCTS_CAROUSEL_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum RECOMMENDED_PRODUCTS_GRID_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum RECOMMENDED_PRODUCTS_STACK_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum SALES_POLICY:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum SHIPPING_AND_RETURNS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum USER_INTERACTIONS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

.field public static final enum VARIANTS_PICKER:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724478
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724479
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "ABOUT_THE_MERCHANT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->ABOUT_THE_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724480
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "CHECKOUT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724481
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "DETAILS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724482
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "INSIGHTS_AND_PROMOTION"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->INSIGHTS_AND_PROMOTION:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724483
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "MERCHANT_PAGE_INFO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724484
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "MERCHANT_TERMS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_TERMS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724485
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "MORE_FROM_SHOP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MORE_FROM_SHOP:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724486
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "PHOTOS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724487
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "POLICIES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->POLICIES:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724488
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "PRODUCT_AND_PURCHASE_DETAILS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_AND_PURCHASE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724489
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "PRODUCT_NAME_AND_PRICE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_NAME_AND_PRICE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724490
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "RECOMMENDED_PRODUCTS_CAROUSEL_VIEW"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_CAROUSEL_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724491
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "RECOMMENDED_PRODUCTS_GRID_VIEW"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_GRID_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724492
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "RECOMMENDED_PRODUCTS_STACK_VIEW"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_STACK_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724493
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "USER_INTERACTIONS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->USER_INTERACTIONS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724494
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "SALES_POLICY"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SALES_POLICY:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724495
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "SHIPPING_AND_RETURNS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SHIPPING_AND_RETURNS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724496
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    const-string v1, "VARIANTS_PICKER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->VARIANTS_PICKER:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724497
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->ABOUT_THE_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->INSIGHTS_AND_PROMOTION:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_TERMS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MORE_FROM_SHOP:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->POLICIES:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_AND_PURCHASE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_NAME_AND_PRICE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_CAROUSEL_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_GRID_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_STACK_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->USER_INTERACTIONS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SALES_POLICY:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SHIPPING_AND_RETURNS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->VARIANTS_PICKER:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 724499
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    .line 724500
    :goto_0
    return-object v0

    .line 724501
    :cond_1
    const-string v0, "ABOUT_THE_MERCHANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->ABOUT_THE_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724503
    :cond_2
    const-string v0, "CHECKOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->CHECKOUT:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724505
    :cond_3
    const-string v0, "DETAILS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724507
    :cond_4
    const-string v0, "MERCHANT_TERMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_TERMS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724509
    :cond_5
    const-string v0, "PHOTOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724511
    :cond_6
    const-string v0, "PRODUCT_NAME_AND_PRICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_NAME_AND_PRICE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724513
    :cond_7
    const-string v0, "SALES_POLICY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SALES_POLICY:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724515
    :cond_8
    const-string v0, "SHIPPING_AND_RETURNS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724516
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->SHIPPING_AND_RETURNS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724517
    :cond_9
    const-string v0, "VARIANTS_PICKER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->VARIANTS_PICKER:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724519
    :cond_a
    const-string v0, "RECOMMENDED_PRODUCTS_GRID_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724520
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_GRID_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724521
    :cond_b
    const-string v0, "RECOMMENDED_PRODUCTS_CAROUSEL_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 724522
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_CAROUSEL_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto :goto_0

    .line 724523
    :cond_c
    const-string v0, "RECOMMENDED_PRODUCTS_STACK_VIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 724524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->RECOMMENDED_PRODUCTS_STACK_VIEW:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724525
    :cond_d
    const-string v0, "INSIGHTS_AND_PROMOTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 724526
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->INSIGHTS_AND_PROMOTION:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724527
    :cond_e
    const-string v0, "MERCHANT_PAGE_INFO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 724528
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MERCHANT_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724529
    :cond_f
    const-string v0, "MORE_FROM_SHOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 724530
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MORE_FROM_SHOP:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724531
    :cond_10
    const-string v0, "USER_INTERACTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 724532
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->USER_INTERACTIONS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724533
    :cond_11
    const-string v0, "PRODUCT_AND_PURCHASE_DETAILS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 724534
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->PRODUCT_AND_PURCHASE_DETAILS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724535
    :cond_12
    const-string v0, "POLICIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 724536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->POLICIES:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0

    .line 724537
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 724538
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 724539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method
