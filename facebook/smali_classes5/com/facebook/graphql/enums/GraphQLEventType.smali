.class public final enum Lcom/facebook/graphql/enums/GraphQLEventType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum HARMONIZED:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum LEGACY:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum QUICK_INVITE:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public static final enum VENUE_GROUP:Lcom/facebook/graphql/enums/GraphQLEventType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "VENUE_GROUP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->VENUE_GROUP:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "HARMONIZED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->HARMONIZED:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "QUICK_INVITE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->QUICK_INVITE:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    const-string v1, "LEGACY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->LEGACY:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726072
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventType;->VENUE_GROUP:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventType;->HARMONIZED:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventType;->QUICK_INVITE:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventType;->LEGACY:Lcom/facebook/graphql/enums/GraphQLEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726065
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 1

    .prologue
    .line 726074
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 726075
    :goto_0
    return-object v0

    .line 726076
    :cond_1
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726077
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0

    .line 726078
    :cond_2
    const-string v0, "VENUE_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726079
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->VENUE_GROUP:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0

    .line 726080
    :cond_3
    const-string v0, "HARMONIZED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726081
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->HARMONIZED:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0

    .line 726082
    :cond_4
    const-string v0, "QUICK_INVITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726083
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->QUICK_INVITE:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0

    .line 726084
    :cond_5
    const-string v0, "LEGACY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726085
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->LEGACY:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0

    .line 726086
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 1

    .prologue
    .line 726087
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLEventType;
    .locals 1

    .prologue
    .line 726073
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLEventType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLEventType;

    return-object v0
.end method
