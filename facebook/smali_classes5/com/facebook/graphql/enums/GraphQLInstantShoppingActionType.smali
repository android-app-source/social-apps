.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum POST:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum SHARE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum SHOW_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum UNSAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "OPEN_URL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727621
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "POST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->POST:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727622
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "SHOW_SELECTOR"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHOW_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727623
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "ADD_TO_CART"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727624
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "SAVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727625
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "UNSAVE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const-string v1, "SHARE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727627
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->POST:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHOW_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727628
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
    .locals 1

    .prologue
    .line 727629
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 727630
    :goto_0
    return-object v0

    .line 727631
    :cond_1
    const-string v0, "OPEN_URL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727632
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727633
    :cond_2
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727634
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->POST:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727635
    :cond_3
    const-string v0, "SHOW_SELECTOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727636
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHOW_SELECTOR:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727637
    :cond_4
    const-string v0, "ADD_TO_CART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727638
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->ADD_TO_CART:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727639
    :cond_5
    const-string v0, "SAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727641
    :cond_6
    const-string v0, "UNSAVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727642
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727643
    :cond_7
    const-string v0, "SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727644
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0

    .line 727645
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
    .locals 1

    .prologue
    .line 727646
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
    .locals 1

    .prologue
    .line 727647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    return-object v0
.end method
