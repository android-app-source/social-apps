.class public final enum Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

.field public static final enum APP:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

.field public static final enum GAME:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

.field public static final enum PAGE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 728694
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    .line 728695
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    const-string v1, "APP"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->APP:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    .line 728696
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->PAGE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    .line 728697
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    const-string v1, "GAME"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->GAME:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    .line 728698
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->APP:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->PAGE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->GAME:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728684
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;
    .locals 1

    .prologue
    .line 728685
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    .line 728686
    :goto_0
    return-object v0

    .line 728687
    :cond_1
    const-string v0, "APP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728688
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->APP:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    goto :goto_0

    .line 728689
    :cond_2
    const-string v0, "PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728690
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->PAGE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    goto :goto_0

    .line 728691
    :cond_3
    const-string v0, "GAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->GAME:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    goto :goto_0

    .line 728693
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;
    .locals 1

    .prologue
    .line 728683
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;
    .locals 1

    .prologue
    .line 728682
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessageAttributionType;

    return-object v0
.end method
