.class public final enum Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

.field public static final enum REGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

.field public static final enum UNREGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    .line 741383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    const-string v1, "REGISTERED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->REGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    .line 741384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    const-string v1, "UNREGISTERED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNREGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    .line 741385
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->REGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNREGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;
    .locals 1

    .prologue
    .line 741386
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    .line 741387
    :goto_0
    return-object v0

    .line 741388
    :cond_1
    const-string v0, "REGISTERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->REGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    goto :goto_0

    .line 741390
    :cond_2
    const-string v0, "UNREGISTERED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNREGISTERED:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    goto :goto_0

    .line 741392
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;
    .locals 1

    .prologue
    .line 741380
    const-class v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;
    .locals 1

    .prologue
    .line 741379
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLZeroTokenRegStatus;

    return-object v0
.end method
