.class public final enum Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum AWAITING_PAYMENT:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum PAYMENT_APPROVED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum PAYMENT_SUBMITTED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum PENDING_APPROVAL:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum SHIPPED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public static final enum VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737344
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "AWAITING_PAYMENT_METHOD"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737345
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "AWAITING_PAYMENT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737346
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "PAYMENT_SUBMITTED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_SUBMITTED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737347
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "PAYMENT_EXPIRED"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737348
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "SHIPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->SHIPPED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737349
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "VOIDED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "PENDING_APPROVAL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PENDING_APPROVAL:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    const-string v1, "PAYMENT_APPROVED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_APPROVED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737352
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_SUBMITTED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->SHIPPED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PENDING_APPROVAL:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_APPROVED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737353
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;
    .locals 1

    .prologue
    .line 737354
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 737355
    :goto_0
    return-object v0

    .line 737356
    :cond_1
    const-string v0, "AWAITING_PAYMENT_METHOD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737357
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737358
    :cond_2
    const-string v0, "AWAITING_PAYMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737359
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737360
    :cond_3
    const-string v0, "PAYMENT_SUBMITTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737361
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_SUBMITTED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737362
    :cond_4
    const-string v0, "PAYMENT_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737363
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737364
    :cond_5
    const-string v0, "SHIPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->SHIPPED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737366
    :cond_6
    const-string v0, "VOIDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737368
    :cond_7
    const-string v0, "PENDING_APPROVAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 737369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PENDING_APPROVAL:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737370
    :cond_8
    const-string v0, "PAYMENT_APPROVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737371
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_APPROVED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0

    .line 737372
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;
    .locals 1

    .prologue
    .line 737373
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;
    .locals 1

    .prologue
    .line 737374
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    return-object v0
.end method
