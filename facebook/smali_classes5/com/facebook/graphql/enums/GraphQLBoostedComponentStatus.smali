.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "DRAFT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "PAUSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "REJECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "FINISHED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "EXTENDABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "DEPRECATED_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "NO_CTA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "CREATING"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "ERROR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    const-string v1, "PENDING_FUNDING_SOURCE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723965
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->DEPRECATED_9:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723950
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .locals 1

    .prologue
    .line 723966
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 723967
    :goto_0
    return-object v0

    .line 723968
    :cond_1
    const-string v0, "INACTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723969
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723970
    :cond_2
    const-string v0, "DRAFT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723971
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->DRAFT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723972
    :cond_3
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723973
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723974
    :cond_4
    const-string v0, "ACTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723975
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723976
    :cond_5
    const-string v0, "PAUSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723977
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723978
    :cond_6
    const-string v0, "REJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723979
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723980
    :cond_7
    const-string v0, "FINISHED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723981
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723982
    :cond_8
    const-string v0, "EXTENDABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 723983
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723984
    :cond_9
    const-string v0, "NO_CTA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 723985
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->NO_CTA:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723986
    :cond_a
    const-string v0, "CREATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 723987
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723988
    :cond_b
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 723989
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto :goto_0

    .line 723990
    :cond_c
    const-string v0, "PENDING_FUNDING_SOURCE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 723991
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING_FUNDING_SOURCE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto/16 :goto_0

    .line 723992
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .locals 1

    .prologue
    .line 723948
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .locals 1

    .prologue
    .line 723949
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    return-object v0
.end method
