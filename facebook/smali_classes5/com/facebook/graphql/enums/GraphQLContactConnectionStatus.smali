.class public final enum Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public static final enum CANNOT_ADD_AS_CONTACT:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public static final enum CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public static final enum NO_CONNECTION:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 626142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const-string v1, "NO_CONNECTION"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->NO_CONNECTION:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const-string v1, "CANNOT_ADD_AS_CONTACT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CANNOT_ADD_AS_CONTACT:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626146
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->NO_CONNECTION:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CANNOT_ADD_AS_CONTACT:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 626158
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 1

    .prologue
    .line 626149
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 626150
    :goto_0
    return-object v0

    .line 626151
    :cond_1
    const-string v0, "CONNECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 626152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0

    .line 626153
    :cond_2
    const-string v0, "CANNOT_ADD_AS_CONTACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 626154
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CANNOT_ADD_AS_CONTACT:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0

    .line 626155
    :cond_3
    const-string v0, "NO_CONNECTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 626156
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->NO_CONNECTION:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0

    .line 626157
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 1

    .prologue
    .line 626148
    const-class v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 1

    .prologue
    .line 626147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method
