.class public final enum Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public static final enum BOTTOM:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public static final enum CENTER:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public static final enum TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723430
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 723431
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 723432
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 723433
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 723434
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .locals 1

    .prologue
    .line 723436
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    .line 723437
    :goto_0
    return-object v0

    .line 723438
    :cond_1
    const-string v0, "TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->TOP:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    goto :goto_0

    .line 723440
    :cond_2
    const-string v0, "BOTTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723441
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->BOTTOM:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    goto :goto_0

    .line 723442
    :cond_3
    const-string v0, "CENTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723443
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->CENTER:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    goto :goto_0

    .line 723444
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .locals 1

    .prologue
    .line 723445
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .locals 1

    .prologue
    .line 723446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    return-object v0
.end method
