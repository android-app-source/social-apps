.class public final enum Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum ACCOUNT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum CREATIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum CREATIVE_IMAGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum INSTAGRAM_PREVIEW:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum SCHEDULE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 723916
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723917
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "CREATIVE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "CREATIVE_IMAGE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE_IMAGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723920
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "TARGETING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723921
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "BUDGET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723922
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "SCHEDULE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->SCHEDULE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "ACCOUNT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ACCOUNT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    const-string v1, "INSTAGRAM_PREVIEW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->INSTAGRAM_PREVIEW:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723925
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE_IMAGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->SCHEDULE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ACCOUNT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->INSTAGRAM_PREVIEW:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723926
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .locals 1

    .prologue
    .line 723927
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 723928
    :goto_0
    return-object v0

    .line 723929
    :cond_1
    const-string v0, "GENERIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723930
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723931
    :cond_2
    const-string v0, "CREATIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723932
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723933
    :cond_3
    const-string v0, "TARGETING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723934
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->TARGETING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723935
    :cond_4
    const-string v0, "BUDGET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 723936
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723937
    :cond_5
    const-string v0, "SCHEDULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 723938
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->SCHEDULE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723939
    :cond_6
    const-string v0, "ACCOUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723940
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ACCOUNT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723941
    :cond_7
    const-string v0, "INSTAGRAM_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723942
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->INSTAGRAM_PREVIEW:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723943
    :cond_8
    const-string v0, "CREATIVE_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 723944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->CREATIVE_IMAGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0

    .line 723945
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .locals 1

    .prologue
    .line 723946
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .locals 1

    .prologue
    .line 723947
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    return-object v0
.end method
