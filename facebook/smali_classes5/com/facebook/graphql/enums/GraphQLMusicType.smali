.class public final enum Lcom/facebook/graphql/enums/GraphQLMusicType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMusicType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum ALBUM:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum MUSICIAN:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum PLAYLIST:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum RADIO_STATION:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum SONG:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 729467
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729468
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "SONG"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->SONG:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729469
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729470
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "MUSICIAN"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->MUSICIAN:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729471
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->PLAYLIST:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729472
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    const-string v1, "RADIO_STATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMusicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->RADIO_STATION:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729473
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMusicType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMusicType;->SONG:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMusicType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMusicType;->MUSICIAN:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMusicType;->PLAYLIST:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMusicType;->RADIO_STATION:Lcom/facebook/graphql/enums/GraphQLMusicType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMusicType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 729466
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMusicType;
    .locals 1

    .prologue
    .line 729453
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 729454
    :goto_0
    return-object v0

    .line 729455
    :cond_1
    const-string v0, "SONG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 729456
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->SONG:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0

    .line 729457
    :cond_2
    const-string v0, "ALBUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 729458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->ALBUM:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0

    .line 729459
    :cond_3
    const-string v0, "MUSICIAN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->MUSICIAN:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0

    .line 729461
    :cond_4
    const-string v0, "PLAYLIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 729462
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->PLAYLIST:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0

    .line 729463
    :cond_5
    const-string v0, "RADIO_STATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729464
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->RADIO_STATION:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0

    .line 729465
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMusicType;
    .locals 1

    .prologue
    .line 729452
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMusicType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMusicType;
    .locals 1

    .prologue
    .line 729451
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMusicType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMusicType;

    return-object v0
.end method
