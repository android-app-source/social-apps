.class public final enum Lcom/facebook/graphql/enums/GraphQLFriendListType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendListType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum APP_CREATED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum EDUCATION:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum FAMILY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum GAMER_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum NAMED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum RESTRICTED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum SUBSCRIBEES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public static final enum WORK:Lcom/facebook/graphql/enums/GraphQLFriendListType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726656
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726657
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "NAMED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->NAMED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726658
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "GOOD_FRIENDS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726659
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "CLOSE_FRIENDS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726660
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "ACQUAINTANCES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726661
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "RESTRICTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->RESTRICTED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726662
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "FAMILY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726663
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "SUBSCRIBEES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->SUBSCRIBEES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726664
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "GAMER_FRIENDS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GAMER_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726665
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "EDUCATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726666
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "WORK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->WORK:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726667
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "CURRENT_CITY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726668
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "APP_CREATED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->APP_CREATED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726669
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const-string v1, "QUERY_SNAPSHOT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendListType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726670
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLFriendListType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListType;->NAMED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendListType;->ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->RESTRICTED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->SUBSCRIBEES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GAMER_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->WORK:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->APP_CREATED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendListType;
    .locals 1

    .prologue
    .line 726671
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 726672
    :goto_0
    return-object v0

    .line 726673
    :cond_1
    const-string v0, "NAMED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->NAMED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726675
    :cond_2
    const-string v0, "GOOD_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726676
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726677
    :cond_3
    const-string v0, "CLOSE_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726678
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CLOSE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726679
    :cond_4
    const-string v0, "ACQUAINTANCES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726680
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726681
    :cond_5
    const-string v0, "RESTRICTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726682
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->RESTRICTED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726683
    :cond_6
    const-string v0, "FAMILY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726684
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->FAMILY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726685
    :cond_7
    const-string v0, "SUBSCRIBEES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726686
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->SUBSCRIBEES:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726687
    :cond_8
    const-string v0, "GAMER_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 726688
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->GAMER_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726689
    :cond_9
    const-string v0, "EDUCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726690
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726691
    :cond_a
    const-string v0, "WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 726692
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->WORK:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726693
    :cond_b
    const-string v0, "CURRENT_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 726694
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto :goto_0

    .line 726695
    :cond_c
    const-string v0, "APP_CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726696
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->APP_CREATED:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto/16 :goto_0

    .line 726697
    :cond_d
    const-string v0, "QUERY_SNAPSHOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 726698
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->QUERY_SNAPSHOT:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto/16 :goto_0

    .line 726699
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendListType;
    .locals 1

    .prologue
    .line 726654
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLFriendListType;
    .locals 1

    .prologue
    .line 726653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLFriendListType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLFriendListType;

    return-object v0
.end method
