.class public final enum Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

.field public static final enum ACCOUNT_SWITCHER:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

.field public static final enum DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

.field public static final enum INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 723113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    .line 723114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    const-string v1, "ACCOUNT_SWITCHER"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->ACCOUNT_SWITCHER:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    .line 723115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    const-string v1, "INTERSTITIAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    .line 723116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    const-string v1, "DIRECT_LOGIN"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    .line 723117
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->ACCOUNT_SWITCHER:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 723118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;
    .locals 1

    .prologue
    .line 723119
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    .line 723120
    :goto_0
    return-object v0

    .line 723121
    :cond_1
    const-string v0, "ACCOUNT_SWITCHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 723122
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->ACCOUNT_SWITCHER:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    goto :goto_0

    .line 723123
    :cond_2
    const-string v0, "INTERSTITIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 723124
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->INTERSTITIAL:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    goto :goto_0

    .line 723125
    :cond_3
    const-string v0, "DIRECT_LOGIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 723126
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->DIRECT_LOGIN:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    goto :goto_0

    .line 723127
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;
    .locals 1

    .prologue
    .line 723128
    const-class v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;
    .locals 1

    .prologue
    .line 723129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    return-object v0
.end method
