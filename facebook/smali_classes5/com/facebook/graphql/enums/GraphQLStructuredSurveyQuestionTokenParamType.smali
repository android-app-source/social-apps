.class public final enum Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum ANSWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum CONTEXT_DATA:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum GROUP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum PREVIOUS_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum QB_OPTIMIZED:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum QE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum SESSION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum TESSA_EVENT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

.field public static final enum VIEWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 740343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740344
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "ANSWER"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740345
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "VIEWER"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->VIEWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740346
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "QE"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740347
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "SESSION"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->SESSION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740348
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "TESSA_EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->TESSA_EVENT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740349
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "QB_OPTIMIZED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QB_OPTIMIZED:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "PREVIOUS_VALUE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->PREVIOUS_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "CONTEXT_DATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->CONTEXT_DATA:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740352
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->GROUP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740353
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->VIEWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->SESSION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->TESSA_EVENT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QB_OPTIMIZED:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->PREVIOUS_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->CONTEXT_DATA:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->GROUP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;
    .locals 1

    .prologue
    .line 740355
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    .line 740356
    :goto_0
    return-object v0

    .line 740357
    :cond_1
    const-string v0, "ANSWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740358
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->ANSWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740359
    :cond_2
    const-string v0, "VIEWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740360
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->VIEWER:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740361
    :cond_3
    const-string v0, "QE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740363
    :cond_4
    const-string v0, "SESSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 740364
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->SESSION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740365
    :cond_5
    const-string v0, "TESSA_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 740366
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->TESSA_EVENT:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740367
    :cond_6
    const-string v0, "QB_OPTIMIZED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 740368
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->QB_OPTIMIZED:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740369
    :cond_7
    const-string v0, "PREVIOUS_VALUE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->PREVIOUS_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740371
    :cond_8
    const-string v0, "CONTEXT_DATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 740372
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->CONTEXT_DATA:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740373
    :cond_9
    const-string v0, "GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 740374
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->GROUP:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0

    .line 740375
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;
    .locals 1

    .prologue
    .line 740376
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;
    .locals 1

    .prologue
    .line 740377
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionTokenParamType;

    return-object v0
.end method
