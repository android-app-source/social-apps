.class public final enum Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

.field public static final enum NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

.field public static final enum SOCIAL_SEARCH:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    .line 724119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    .line 724120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    const-string v1, "SOCIAL_SEARCH"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->SOCIAL_SEARCH:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    .line 724121
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->SOCIAL_SEARCH:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;
    .locals 1

    .prologue
    .line 724111
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    .line 724112
    :goto_0
    return-object v0

    .line 724113
    :cond_1
    const-string v0, "NORMAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724114
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    goto :goto_0

    .line 724115
    :cond_2
    const-string v0, "SOCIAL_SEARCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724116
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->SOCIAL_SEARCH:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    goto :goto_0

    .line 724117
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;
    .locals 1

    .prologue
    .line 724110
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;
    .locals 1

    .prologue
    .line 724109
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    return-object v0
.end method
