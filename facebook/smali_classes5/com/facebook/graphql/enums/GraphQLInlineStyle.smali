.class public final enum Lcom/facebook/graphql/enums/GraphQLInlineStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInlineStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum CODE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum ITALIC:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum UNDERLINE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727497
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727498
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727499
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "BOLD"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727500
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "ITALIC"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727501
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "UNDERLINE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727502
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "CODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->CODE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727503
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "STRIKETHROUGH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727504
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "SUBSCRIPT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727505
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    const-string v1, "SUPERSCRIPT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727506
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->CODE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727507
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineStyle;
    .locals 1

    .prologue
    .line 727508
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    .line 727509
    :goto_0
    return-object v0

    .line 727510
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727511
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727512
    :cond_2
    const-string v0, "BOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727514
    :cond_3
    const-string v0, "ITALIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->ITALIC:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727516
    :cond_4
    const-string v0, "UNDERLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727517
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNDERLINE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727518
    :cond_5
    const-string v0, "CODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727519
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->CODE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727520
    :cond_6
    const-string v0, "STRIKETHROUGH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->STRIKETHROUGH:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727522
    :cond_7
    const-string v0, "SUBSCRIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727523
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUBSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727524
    :cond_8
    const-string v0, "SUPERSCRIPT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727525
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->SUPERSCRIPT:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0

    .line 727526
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineStyle;
    .locals 1

    .prologue
    .line 727527
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInlineStyle;
    .locals 1

    .prologue
    .line 727528
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    return-object v0
.end method
