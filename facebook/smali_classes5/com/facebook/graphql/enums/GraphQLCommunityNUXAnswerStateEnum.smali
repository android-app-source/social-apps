.class public final enum Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

.field public static final enum CREATED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

.field public static final enum DISMISSED:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

.field public static final enum JOINED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 724540
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    .line 724541
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->DISMISSED:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    .line 724542
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    const-string v1, "CREATED_GROUP"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->CREATED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    .line 724543
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    const-string v1, "JOINED_GROUP"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->JOINED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    .line 724544
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->DISMISSED:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->CREATED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->JOINED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724547
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;
    .locals 1

    .prologue
    .line 724548
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    .line 724549
    :goto_0
    return-object v0

    .line 724550
    :cond_1
    const-string v0, "DISMISSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724551
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->DISMISSED:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    goto :goto_0

    .line 724552
    :cond_2
    const-string v0, "CREATED_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724553
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->CREATED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    goto :goto_0

    .line 724554
    :cond_3
    const-string v0, "JOINED_GROUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724555
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->JOINED_GROUP:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    goto :goto_0

    .line 724556
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;
    .locals 1

    .prologue
    .line 724546
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;
    .locals 1

    .prologue
    .line 724545
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCommunityNUXAnswerStateEnum;

    return-object v0
.end method
