.class public final enum Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public static final enum CUSTOM:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public static final enum DARK:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public static final enum LIGHT:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 739617
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 739618
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    const-string v1, "LIGHT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->LIGHT:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 739619
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    const-string v1, "DARK"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->DARK:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 739620
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 739621
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->LIGHT:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->DARK:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 739622
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;
    .locals 1

    .prologue
    .line 739623
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 739624
    :goto_0
    return-object v0

    .line 739625
    :cond_1
    const-string v0, "LIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739626
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->LIGHT:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    goto :goto_0

    .line 739627
    :cond_2
    const-string v0, "DARK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 739628
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->DARK:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    goto :goto_0

    .line 739629
    :cond_3
    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739630
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    goto :goto_0

    .line 739631
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;
    .locals 1

    .prologue
    .line 739632
    const-class v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;
    .locals 1

    .prologue
    .line 739633
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    return-object v0
.end method
