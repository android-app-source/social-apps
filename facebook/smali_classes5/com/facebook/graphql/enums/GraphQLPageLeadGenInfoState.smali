.class public final enum Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

.field public static final enum READ:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

.field public static final enum RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

.field public static final enum UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737264
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 737265
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const-string v1, "UNREAD"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 737266
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const-string v1, "READ"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->READ:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 737267
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    const-string v1, "RESPONDED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 737268
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->READ:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 1

    .prologue
    .line 737253
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    .line 737254
    :goto_0
    return-object v0

    .line 737255
    :cond_1
    const-string v0, "UNREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    goto :goto_0

    .line 737257
    :cond_2
    const-string v0, "READ"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->READ:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    goto :goto_0

    .line 737259
    :cond_3
    const-string v0, "RESPONDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    goto :goto_0

    .line 737261
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 1

    .prologue
    .line 737262
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 1

    .prologue
    .line 737263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    return-object v0
.end method
