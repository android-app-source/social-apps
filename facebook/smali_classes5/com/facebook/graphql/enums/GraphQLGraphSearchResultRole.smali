.class public final enum Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum AWARENESS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BLENDED_PHOTO_PUBLIC_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BLENDED_PHOTO_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum BLENDED_PHOTO_SOCIAL_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_DPA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMERCE_MARKETPLACE_C2C_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum COVER_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum CURRENCY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEBATE_FEELS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEBATE_ISSUES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEFINITION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEPRECATED_139:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEPRECATED_191:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEPRECATED_215:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DEPRECATED_97:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_COMMERCE_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_COMMERCE_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_EVENTS_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_EXPLICIT_INTENT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_EXPLICIT_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_COMMON:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_FUTURE_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_HOSTED_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_PYMK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_GROUPS_FRIENDS_JOINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_GROUPS_ONE_SHOULD_JOIN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_BARS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FRIENDS_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FRIENDS_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FRIENDS_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_LANDMARKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_PEOPLE_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_PEOPLE_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_PEOPLE_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_LOCATION_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_MY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_MY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_MUSIC_SONG:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_POSTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum DISCOVERY_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ELECTION_DATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum EMOTIONAL_ANALYSIS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum EMPTY_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum EXTERNAL_ACCOUNTS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FEATURED_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FEED_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FEED_POSTS_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FEED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FLEXIBLE_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum FOR_SALE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GLOBAL_SHARE_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GLOBAL_SHARE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_BY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_CV_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_CV_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_CV_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_LIKED_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_POPULAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_RECENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_PHOTO_SET_TAGGED_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum GROUP_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum INSTAGRAM_PHOTOS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum LINKEDIN_RESUME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MEDIA_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MEDIA_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MUTUALITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MUTUALLY_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MUTUALLY_LIKED_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MUTUALLY_LIKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_HEADLINE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_KEY_VOICES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_PIVOT_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NEWS_TOP_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum OPTIONAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum OPTIONALIZATION_INDICATOR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PHOTOS_WITH_MY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_CHAIN_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_CHAIN_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_SET_SEARCH:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_SPOTLIGHT_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PLACES_SPOTLIGHT_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_BEHIND_THE_SCENE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POST_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POST_SEARCH_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PREFILLED_COMPOSER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PRELOADED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PROGRAMMATIC_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PROMOTED_ENTITY_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PUBLIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PUBLIC_POSTS_LOAD_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PUBLIC_POST_SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum PXS_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum REACTION_UNIT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum RELATED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum RELATED_SHARES_WITH_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SAFETY_CHECK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SEARCH_SUGGESTIONS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SENTIMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SERP_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT_OTHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SHORTCUT_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SIMILAR_PEOPLE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORTS_DATA_PHOTO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORT_ENTRY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORT_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SPORT_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum SUBTOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TEST_NON_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TEST_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOPIC_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_POSTS_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TOP_VITAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum TRENDING_TOPIC_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEOS_NEWSY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEO_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum VOTING_ACTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final enum WORK_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 605906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605907
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "CENTRAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "WIKIPEDIA_CARD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "RELATED_ENTITIES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "LIVE_CONVERSATION_MODULE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SEARCH_SUGGESTIONS_MODULE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEARCH_SUGGESTIONS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TRENDING_TOPIC_MODULE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TRENDING_TOPIC_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "RELATED_TOPICS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605915
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_KEY_VOICES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_KEY_VOICES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605916
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_SOCIAL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605917
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_EYEWITNESSES"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605918
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_PUBLISHERS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605919
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_HEADLINE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_HEADLINE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605920
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_CONTEXT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605921
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PROGRAMMATIC_NEWS_CONTEXT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROGRAMMATIC_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605922
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FLEXIBLE_NEWS_CONTEXT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FLEXIBLE_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOPIC_MEDIA"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MEDIA_COMBINED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605925
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MEDIA_SOCIAL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605926
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MEDIA_WEB"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605927
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOPIC_METADATA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605928
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORT_MODULE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORT_ENTRY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_ENTRY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GAMETIME_FAN_FAVORITE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "OPTIONAL"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_VITAL"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VITAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_MAIN_MODULE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MAIN_MODULE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PREFILLED_COMPOSER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PREFILLED_COMPOSER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_TOP_VIDEO"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_TOP_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORTS_DATA_PHOTO"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORTS_DATA_PHOTO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605939
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GLOBAL_SHARE_METADATA"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605940
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GLOBAL_SHARE_POSTS"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605941
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_MODULE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605942
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PUBLIC_POSTS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605943
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FEED_POSTS"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605944
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PUBLIC_MEDIA"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605945
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FEED_MEDIA"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605946
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COVER_MEDIA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COVER_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605947
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_PIVOT"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605948
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "RELATED_SHARES"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605949
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "EMOTIONAL_ANALYSIS"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMOTIONAL_ANALYSIS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605950
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMON_PHRASES"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605951
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMON_QUOTES"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605952
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TEST_SPLITTABLE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605953
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TEST_NON_SPLITTABLE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_NON_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605954
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PROMOTED_ENTITY_MEDIA"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605955
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PROMOTED_ENTITY_RELATED_SEARCHES"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605956
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PXS_PIVOTS"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PXS_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605957
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SEE_MORE_PIVOT"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605958
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SECTION_HEADER"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605959
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SERP_HEADER"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SERP_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605960
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FEATURED_POST"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEATURED_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605961
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "RELATED_SHARES_WITH_POSTS"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES_WITH_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605962
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "EMPTY_CARD"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605963
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POST_SEARCH_SECTION_HEADER"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SEARCH_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605964
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORT_VIDEO"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605965
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORT_LINKS"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605966
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "REACTION_UNIT"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->REACTION_UNIT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605967
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEOS"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605968
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEOS_WEB"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605969
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEOS_LIVE"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605970
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEOS_MIXED"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605971
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEO_PUBLISHERS"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605972
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_VIDEOS"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605973
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FEED_VIDEOS"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605974
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEBATE_FEELS"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_FEELS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605975
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEBATE_ISSUES"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_ISSUES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605976
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MY_POSTS"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605977
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_PIVOTS"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605978
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "EMPTY_ENTITY"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605979
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_ABOUT"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605980
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_POSTS_BY"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_POSTS_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605981
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PHOTOS_WITH_MY_FRIENDS"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PHOTOS_WITH_MY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605982
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MUTUALLY_LIKED_POSTS"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605983
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MUTUALLY_COMMENTED_POSTS"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605984
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MUTUALLY_LIKED_COMMENTED_POSTS"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605985
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPORT_REDIRECT"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605986
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BIRTHDAY"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605987
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TIME"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605988
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ELECTIONS"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605989
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VOTING_ACTIONS"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VOTING_ACTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "CURRENCY"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605991
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "WEATHER"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605992
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "INSTAGRAM_PHOTOS_MODULE"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->INSTAGRAM_PHOTOS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605993
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "LINKEDIN_RESUME"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LINKEDIN_RESUME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605994
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEFINITION"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEFINITION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605995
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SENTIMENT"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SENTIMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605996
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_B2C"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605997
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "MUTUALITY_MODULE"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605998
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POST_CONTEXT"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 605999
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SAFETY_CHECK"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SAFETY_CHECK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606000
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FOR_SALE_PRODUCTS"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FOR_SALE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606001
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "EXTERNAL_ACCOUNTS_MODULE"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EXTERNAL_ACCOUNTS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606002
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NAVIGATIONAL_LINKS"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606003
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEPRECATED_97"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_97:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606004
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_CELEBRITIES"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606005
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_CELEBRITIES_MENTION"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606006
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_VITAL_AUTHORS"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606007
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_RELATED_AUTHORS"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606008
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_REVIEWS"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606009
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_REVIEWS_PEOPLE"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606010
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_EXPERIENTIAL"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606011
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_HOW_TO"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606012
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_RECIPES"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606013
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_MINUTIAE"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606014
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_FEATURED"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606015
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_GOVERNMENT"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606016
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_TOPIC_1"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606017
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_TOPIC_2"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606018
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_TOPIC_3"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606019
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_RECENT_TOP"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606020
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_COMMENTARY"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606021
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_ENGAGEMENT"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606022
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_LOCATION"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606023
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_BEHIND_THE_SCENE"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_BEHIND_THE_SCENE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606024
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POST_SET"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606025
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_COMBINED"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606026
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_C2C"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606027
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_DPA"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_DPA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606028
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SIMILAR_PEOPLE_MODULE"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SIMILAR_PEOPLE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606029
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606030
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_CONTENTS"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606031
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_SET_CONTENTS"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606032
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_USER"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606033
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_PAGES"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606034
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_EVENTS"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606035
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_PLACES"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606036
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_GROUPS"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606037
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ENTITY_APPS"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606038
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEO_MODULE"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606039
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_SET_SEARCH"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SET_SEARCH:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606040
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_IN"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606041
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_NEARBY"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606042
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TIMELINE_HEADER"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606043
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TIMELINE_HEADER_CARD"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606044
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_QUERY_ENTITY_MODULE"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606045
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEPRECATED_139"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_139:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606046
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "AWARENESS"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->AWARENESS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606047
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FRIENDS_VISITED"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606048
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FRIENDS_LIVED"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606049
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FRIENDS_FROM"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606050
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FRIENDS_SCHOOL"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606051
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FRIENDS_WORK"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606052
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_PEOPLE_VISITED"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606053
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_PEOPLE_LIVED"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606054
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_PEOPLE_FROM"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606055
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_PEOPLE_SCHOOL"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606056
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_PEOPLE_WORK"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606057
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_GROUPS"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606058
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_EVENTS"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606059
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_LANDMARKS"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_LANDMARKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606060
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_BARS"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_BARS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606061
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_LOCATION_FOR_SALE"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606062
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_PYMK"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_PYMK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606063
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_COMMON"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_COMMON:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606064
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_WORK"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606065
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_SCHOOL"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606066
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_CITY"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606067
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_HOMETOWN"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606068
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_WORK"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606069
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_SCHOOL"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606070
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_CITY"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606071
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_HOMETOWN"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606072
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_UPCOMING_EVENTS"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606073
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_GROUPS"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606074
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_HOSTED_EVENTS"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOSTED_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606075
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_MY_EVENTS"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606076
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_FRIENDS_FUTURE_EVENTS"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_FUTURE_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606077
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_EVENTS_NEARBY"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EVENTS_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606078
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_MY_GROUPS"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606079
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_GROUPS_FRIENDS_JOINED"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_FRIENDS_JOINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606080
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_GROUPS_ONE_SHOULD_JOIN"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_ONE_SHOULD_JOIN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606081
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "ELECTION_DATA"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTION_DATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606082
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "VIDEOS_NEWSY"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_NEWSY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606083
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_ENTITY_PAGES"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606084
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_ENTITY_EVENTS"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606085
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_ENTITY_GROUPS"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606086
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BLENDED_PHOTO_SOCIAL"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606087
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BLENDED_PHOTO_PUBLIC"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606088
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606089
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SPELLER"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606090
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "NEWS_PIVOT_POSTS"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606091
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_FEED_POSTS"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606092
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BLENDED_PHOTO_SOCIAL_EMPTY"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606093
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BLENDED_PHOTO_PUBLIC_EMPTY"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606094
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "TOP_PUBLIC_POSTS"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_CHAIN_NEARBY"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_CHAIN_IN"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEPRECATED_191"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_191:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606099
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606100
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC"

    const/16 v2, 0xc2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606101
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC"

    const/16 v2, 0xc3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606102
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_SPOTLIGHT_NEARBY"

    const/16 v2, 0xc4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606103
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PLACES_SPOTLIGHT_IN"

    const/16 v2, 0xc5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606104
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_CONSOLIDATED"

    const/16 v2, 0xc6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606105
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED"

    const/16 v2, 0xc7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606106
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606107
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606108
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PUBLIC_POST_SEE_MORE_PIVOT"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POST_SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606109
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PUBLIC_POSTS_LOAD_MORE"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS_LOAD_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606110
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "OPINION"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606111
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_EXPLICIT_INTENT_EVENTS"

    const/16 v2, 0xcd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606112
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_EXPLICIT_INTENT_GROUPS"

    const/16 v2, 0xce

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606113
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK"

    const/16 v2, 0xcf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606114
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE"

    const/16 v2, 0xd0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606115
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK"

    const/16 v2, 0xd1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606116
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_MUSIC_SONG"

    const/16 v2, 0xd2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_SONG:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606117
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "FEED_POSTS_COMBINED"

    const/16 v2, 0xd3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606118
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_COMMERCE_GROUPS"

    const/16 v2, 0xd4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606119
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE"

    const/16 v2, 0xd5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606120
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_RELATED_PAGES"

    const/16 v2, 0xd6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606121
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DEPRECATED_215"

    const/16 v2, 0xd7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_215:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606122
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY"

    const/16 v2, 0xd8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606123
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "POSTS_LINKS"

    const/16 v2, 0xd9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606124
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE"

    const/16 v2, 0xda

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606125
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SUBTOPIC"

    const/16 v2, 0xdb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SUBTOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606126
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "WORK_POSTS"

    const/16 v2, 0xdc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WORK_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606127
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "PRELOADED_POSTS"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PRELOADED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606128
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME"

    const/16 v2, 0xde

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606129
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY"

    const/16 v2, 0xdf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606130
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT_PEOPLE"

    const/16 v2, 0xe0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606131
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT_GROUPS"

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606132
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT_EVENTS"

    const/16 v2, 0xe2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606133
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT_PAGES"

    const/16 v2, 0xe3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606134
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "SHORTCUT_OTHER"

    const/16 v2, 0xe4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_OTHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606135
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GROUP_POSTS"

    const/16 v2, 0xe5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GROUP_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606136
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_RECENT"

    const/16 v2, 0xe6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_RECENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606137
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_POPULAR"

    const/16 v2, 0xe7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_POPULAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606138
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_LIKED_BY"

    const/16 v2, 0xe8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_LIKED_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606139
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_MARKETPLACE"

    const/16 v2, 0xe9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606140
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "COMMERCE_MARKETPLACE_C2C_COMBINED"

    const/16 v2, 0xea

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE_C2C_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606141
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_BY_ENTITY"

    const/16 v2, 0xeb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_BY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606142
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_TAGGED_ENTITY"

    const/16 v2, 0xec

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_TAGGED_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606143
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_PUBLIC"

    const/16 v2, 0xed

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606144
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_CV_1"

    const/16 v2, 0xee

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606145
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_CV_2"

    const/16 v2, 0xef

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606146
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "GRAMMAR_PHOTO_SET_CV_3"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606147
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "BLENDED_PHOTO_RELATED_SEARCHES"

    const/16 v2, 0xf1

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606148
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "OPTIONALIZATION_INDICATOR"

    const/16 v2, 0xf2

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONALIZATION_INDICATOR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606149
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_COMMERCE_INTENT_GROUPS"

    const/16 v2, 0xf3

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606150
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const-string v1, "DISCOVERY_PEOPLE_POSTS_CONSOLIDATED"

    const/16 v2, 0xf4

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_POSTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606151
    const/16 v0, 0xf5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEARCH_SUGGESTIONS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TRENDING_TOPIC_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_KEY_VOICES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_HEADLINE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROGRAMMATIC_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FLEXIBLE_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_ENTRY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VITAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PREFILLED_COMPOSER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_TOP_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORTS_DATA_PHOTO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COVER_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMOTIONAL_ANALYSIS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_NON_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PXS_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SERP_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEATURED_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES_WITH_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SEARCH_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->REACTION_UNIT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_FEELS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_ISSUES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_POSTS_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PHOTOS_WITH_MY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VOTING_ACTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->INSTAGRAM_PHOTOS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LINKEDIN_RESUME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEFINITION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SENTIMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SAFETY_CHECK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FOR_SALE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EXTERNAL_ACCOUNTS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_97:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_BEHIND_THE_SCENE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_DPA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SIMILAR_PEOPLE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SET_SEARCH:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_139:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->AWARENESS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_LANDMARKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_BARS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_PYMK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_COMMON:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOSTED_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_FUTURE_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EVENTS_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_FRIENDS_JOINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_ONE_SHOULD_JOIN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTION_DATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_NEWSY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_191:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POST_SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS_LOAD_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_SONG:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEPRECATED_215:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SUBTOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WORK_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PRELOADED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_OTHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GROUP_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_RECENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_POPULAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_LIKED_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE_C2C_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_BY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_TAGGED_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xed

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xee

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xef

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONALIZATION_INDICATOR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_POSTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 606745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2

    .prologue
    .line 606154
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 606155
    :goto_0
    return-object v0

    .line 606156
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x7f

    .line 606157
    packed-switch v0, :pswitch_data_0

    .line 606158
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606159
    :pswitch_1
    const-string v0, "SIMILAR_PEOPLE_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 606160
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SIMILAR_PEOPLE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606161
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606162
    :pswitch_2
    const-string v0, "NEWS_PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 606163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606164
    :cond_3
    const-string v0, "POST_SET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 606165
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606166
    :cond_4
    const-string v0, "DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 606167
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_FRIENDS_LIKE_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606168
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606169
    :pswitch_3
    const-string v0, "NEWS_CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 606170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606171
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606172
    :pswitch_4
    const-string v0, "SEARCH_SUGGESTIONS_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 606173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEARCH_SUGGESTIONS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606174
    :cond_7
    const-string v0, "POSTS_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 606175
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_ABOUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 606176
    :cond_8
    const-string v0, "DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 606177
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606178
    :cond_9
    const-string v0, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 606179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606180
    :cond_a
    const-string v0, "FLEXIBLE_NEWS_CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 606181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FLEXIBLE_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606182
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606183
    :pswitch_5
    const-string v0, "POST_CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 606184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606185
    :cond_c
    const-string v0, "OPINION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 606186
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606187
    :cond_d
    const-string v0, "DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 606188
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606189
    :cond_e
    const-string v0, "DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 606190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_ABOUT_TOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606191
    :cond_f
    const-string v0, "SPELLER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 606192
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606193
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606194
    :pswitch_6
    const-string v0, "BLENDED_PHOTO_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 606195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606196
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606197
    :pswitch_7
    const-string v0, "REACTION_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 606198
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->REACTION_UNIT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606199
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606200
    :pswitch_8
    const-string v0, "SERP_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 606201
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SERP_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606202
    :cond_13
    const-string v0, "GRAMMAR_PHOTO_SET_CV_1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 606203
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606204
    :cond_14
    const-string v0, "WEATHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 606205
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606206
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606207
    :pswitch_9
    const-string v0, "NEWS_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 606208
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606209
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606210
    :pswitch_a
    const-string v0, "POSTS_SET_TOPIC_3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 606211
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606212
    :cond_17
    const-string v0, "GRAMMAR_PHOTO_SET_POPULAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 606213
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_POPULAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606214
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606215
    :pswitch_b
    const-string v0, "DISCOVERY_PEOPLE_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 606216
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606217
    :cond_19
    const-string v0, "SECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 606218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606219
    :cond_1a
    const-string v0, "SHORTCUT_OTHER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 606220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_OTHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606221
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606222
    :pswitch_c
    const-string v0, "DISCOVERY_FRIENDS_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 606223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606224
    :cond_1c
    const-string v0, "POSTS_SET_GOVERNMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 606225
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606226
    :cond_1d
    const-string v0, "POSTS_SET_ENGAGEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 606227
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606228
    :cond_1e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606229
    :pswitch_d
    const-string v0, "TOP_VITAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 606230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VITAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606231
    :cond_1f
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 606232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606233
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606234
    :pswitch_e
    const-string v0, "MEDIA_WEB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 606235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606236
    :cond_21
    const-string v0, "POSTS_SET_RECENT_TOP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 606237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606238
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606239
    :pswitch_f
    const-string v0, "FEED_POSTS_COMBINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 606240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606241
    :cond_23
    const-string v0, "PROGRAMMATIC_NEWS_CONTEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 606242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROGRAMMATIC_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606243
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606244
    :pswitch_10
    const-string v0, "PUBLIC_POST_SEE_MORE_PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 606245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POST_SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606246
    :cond_25
    const-string v0, "OPTIONALIZATION_INDICATOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 606247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONALIZATION_INDICATOR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606248
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606249
    :pswitch_11
    const-string v0, "AWARENESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 606250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->AWARENESS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606251
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606252
    :pswitch_12
    const-string v0, "DISCOVERY_LOCATION_PEOPLE_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 606253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606254
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606255
    :pswitch_13
    const-string v0, "PLACES_SET_SEARCH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 606256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SET_SEARCH:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606257
    :cond_29
    const-string v0, "DISCOVERY_LOCATION_FRIENDS_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 606258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606259
    :cond_2a
    const-string v0, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 606260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_RECENTLY_RELEASED_SONGS_THIS_WEEK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606261
    :cond_2b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606262
    :pswitch_14
    const-string v0, "POSTS_SET_EXPERIENTIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 606263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606264
    :cond_2c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606265
    :pswitch_15
    const-string v0, "ELECTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 606266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606267
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606268
    :pswitch_16
    const-string v0, "COMMON_QUOTES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 606269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606270
    :cond_2e
    const-string v0, "ENTITY_APPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 606271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606272
    :cond_2f
    const-string v0, "DISCOVERY_PEOPLE_CONSOLIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 606273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606274
    :cond_30
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606275
    :pswitch_17
    const-string v0, "COMMON_PHRASES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 606276
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606277
    :cond_31
    const-string v0, "POSTS_SET_FEATURED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 606278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606279
    :cond_32
    const-string v0, "ENTITY_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 606280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606281
    :cond_33
    const-string v0, "VIDEOS_MIXED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 606282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606283
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606284
    :pswitch_18
    const-string v0, "DISCOVERY_LOCATION_PEOPLE_LIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 606285
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606286
    :cond_35
    const-string v0, "GROUP_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 606287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GROUP_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606288
    :cond_36
    const-string v0, "ENTITY_PIVOTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 606289
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606290
    :cond_37
    const-string v0, "ENTITY_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 606291
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606292
    :cond_38
    const-string v0, "ENTITY_PLACES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 606293
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606294
    :cond_39
    const-string v0, "ENTITY_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 606295
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606296
    :cond_3a
    const-string v0, "DISCOVERY_GROUPS_FRIENDS_JOINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 606297
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_FRIENDS_JOINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606298
    :cond_3b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606299
    :pswitch_19
    const-string v0, "DISCOVERY_LOCATION_FRIENDS_LIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 606300
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606301
    :cond_3c
    const-string v0, "DISCOVERY_PEOPLE_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 606302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606303
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606304
    :pswitch_1a
    const-string v0, "DISCOVERY_LOCATION_PEOPLE_VISITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 606305
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606306
    :cond_3e
    const-string v0, "DISCOVERY_FRIENDS_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 606307
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606308
    :cond_3f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606309
    :pswitch_1b
    const-string v0, "DISCOVERY_LOCATION_FRIENDS_VISITED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 606310
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606311
    :cond_40
    const-string v0, "MY_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 606312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606313
    :cond_41
    const-string v0, "DISCOVERY_EVENTS_NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 606314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EVENTS_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606315
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606316
    :pswitch_1c
    const-string v0, "BLENDED_PHOTO_SOCIAL_EMPTY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 606317
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606318
    :cond_43
    const-string v0, "BLENDED_PHOTO_PUBLIC_EMPTY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 606319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606320
    :cond_44
    const-string v0, "DISCOVERY_PEOPLE_POSTS_CONSOLIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 606321
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_POSTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606322
    :cond_45
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606323
    :pswitch_1d
    const-string v0, "PLACES_NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 606324
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606325
    :cond_46
    const-string v0, "EMOTIONAL_ANALYSIS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 606326
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMOTIONAL_ANALYSIS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606327
    :cond_47
    const-string v0, "TIMELINE_HEADER_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 606328
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606329
    :cond_48
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606330
    :pswitch_1e
    const-string v0, "GLOBAL_SHARE_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 606331
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606332
    :cond_49
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606333
    :pswitch_1f
    const-string v0, "TOP_POSTS_BY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 606334
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_POSTS_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606335
    :cond_4a
    const-string v0, "DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 606336
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_PAGES_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606337
    :cond_4b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606338
    :pswitch_20
    const-string v0, "DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 606339
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606340
    :cond_4c
    const-string v0, "DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 606341
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_EVENTS_CONSOLIDATED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606342
    :cond_4d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606343
    :pswitch_21
    const-string v0, "VIDEOS_NEWSY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 606344
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_NEWSY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606345
    :cond_4e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606346
    :pswitch_22
    const-string v0, "PLACES_CHAIN_NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 606347
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606348
    :cond_4f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606349
    :pswitch_23
    const-string v0, "SPORT_LINKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 606350
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606351
    :cond_50
    const-string v0, "POSTS_SET_COMMENTARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 606352
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606353
    :cond_51
    const-string v0, "GRAMMAR_PHOTO_SET_CV_2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 606354
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606355
    :cond_52
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606356
    :pswitch_24
    const-string v0, "COMMERCE_B2C"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 606357
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606358
    :cond_53
    const-string v0, "COMMERCE_C2C"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 606359
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606360
    :cond_54
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606361
    :pswitch_25
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 606362
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606363
    :cond_55
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606364
    :pswitch_26
    const-string v0, "MUTUALLY_LIKED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 606365
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606366
    :cond_56
    const-string v0, "PLACES_SPOTLIGHT_NEARBY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 606367
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606368
    :cond_57
    const-string v0, "WORK_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 606369
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WORK_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606370
    :cond_58
    const-string v0, "SHORTCUT_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 606371
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606372
    :cond_59
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606373
    :pswitch_27
    const-string v0, "FEED_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 606374
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606375
    :cond_5a
    const-string v0, "SPORT_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 606376
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606377
    :cond_5b
    const-string v0, "SHORTCUT_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 606378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606379
    :cond_5c
    const-string v0, "SHORTCUT_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 606380
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606381
    :cond_5d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606382
    :pswitch_28
    const-string v0, "MUTUALLY_COMMENTED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 606383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606384
    :cond_5e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606385
    :pswitch_29
    const-string v0, "TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 606386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606387
    :cond_5f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606388
    :pswitch_2a
    const-string v0, "NEWS_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 606389
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606390
    :cond_60
    const-string v0, "SAFETY_CHECK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 606391
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SAFETY_CHECK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606392
    :cond_61
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606393
    :pswitch_2b
    const-string v0, "DISCOVERY_LOCATION_PEOPLE_FROM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 606394
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606395
    :cond_62
    const-string v0, "SPORTS_DATA_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 606396
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORTS_DATA_PHOTO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606397
    :cond_63
    const-string v0, "DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 606398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606399
    :cond_64
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606400
    :pswitch_2c
    const-string v0, "NEWS_HEADLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 606401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_HEADLINE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606402
    :cond_65
    const-string v0, "LINKEDIN_RESUME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 606403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LINKEDIN_RESUME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606404
    :cond_66
    const-string v0, "DISCOVERY_LOCATION_FRIENDS_FROM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 606405
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606406
    :cond_67
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606407
    :pswitch_2d
    const-string v0, "DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 606408
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_GAMES_FOR_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606409
    :cond_68
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606410
    :pswitch_2e
    const-string v0, "SUBTOPIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 606411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SUBTOPIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606412
    :cond_69
    const-string v0, "MUTUALLY_LIKED_COMMENTED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 606413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALLY_LIKED_COMMENTED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606414
    :cond_6a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606415
    :pswitch_2f
    const-string v0, "DISCOVERY_LOCATION_FOR_SALE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 606416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606417
    :cond_6b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606418
    :pswitch_30
    const-string v0, "PUBLIC_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 606419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606420
    :cond_6c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606421
    :pswitch_31
    const-string v0, "GRAMMAR_PHOTO_SET_PUBLIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 606422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606423
    :cond_6d
    const-string v0, "VIDEOS_LIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 606424
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606425
    :cond_6e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606426
    :pswitch_32
    const-string v0, "POSTS_SET_MINUTIAE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 606427
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606428
    :cond_6f
    const-string v0, "VIDEO_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 606429
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606430
    :cond_70
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606431
    :pswitch_33
    const-string v0, "TEST_SPLITTABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 606432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606433
    :cond_71
    const-string v0, "CENTRAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 606434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606435
    :cond_72
    const-string v0, "TOP_MAIN_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 606436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606437
    :cond_73
    const-string v0, "TOPIC_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 606438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606439
    :cond_74
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606440
    :pswitch_34
    const-string v0, "LIVE_CONVERSATION_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 606441
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606442
    :cond_75
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606443
    :pswitch_35
    const-string v0, "DEFINITION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 606444
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEFINITION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606445
    :cond_76
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606446
    :pswitch_36
    const-string v0, "TOPIC_METADATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 606447
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606448
    :cond_77
    const-string v0, "PUBLIC_POSTS_LOAD_MORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 606449
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS_LOAD_MORE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606450
    :cond_78
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606451
    :pswitch_37
    const-string v0, "TEST_NON_SPLITTABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 606452
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TEST_NON_SPLITTABLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606453
    :cond_79
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606454
    :pswitch_38
    const-string v0, "POSTS_SET_REVIEWS_PEOPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 606455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606456
    :cond_7a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606457
    :pswitch_39
    const-string v0, "TRENDING_TOPIC_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 606458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TRENDING_TOPIC_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606459
    :cond_7b
    const-string v0, "PROMOTED_ENTITY_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 606460
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606461
    :cond_7c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606462
    :pswitch_3a
    const-string v0, "POSTS_SET_BEHIND_THE_SCENE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 606463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_BEHIND_THE_SCENE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606464
    :cond_7d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606465
    :pswitch_3b
    const-string v0, "SHORTCUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 606466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606467
    :cond_7e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606468
    :pswitch_3c
    const-string v0, "SENTIMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 606469
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SENTIMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606470
    :cond_7f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606471
    :pswitch_3d
    const-string v0, "GRAMMAR_PHOTO_SET_CV_3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 606472
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606473
    :cond_80
    const-string v0, "DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 606474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_SPORTS_INDIVIDUAL_GAME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606475
    :cond_81
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606476
    :pswitch_3e
    const-string v0, "PLACES_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 606477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606478
    :cond_82
    const-string v0, "OPTIONAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 606479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPTIONAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606480
    :cond_83
    const-string v0, "EMPTY_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 606481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606482
    :cond_84
    const-string v0, "GRAMMAR_PHOTO_SET_RECENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 606483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_RECENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606484
    :cond_85
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606485
    :pswitch_3f
    const-string v0, "DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 606486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_GROUPS_COMMUNITY_COLLEGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606487
    :cond_86
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606488
    :pswitch_40
    const-string v0, "SPORT_REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_87

    .line 606489
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_REDIRECT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606490
    :cond_87
    const-string v0, "MEDIA_SOCIAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 606491
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606492
    :cond_88
    const-string v0, "SEE_MORE_PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 606493
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606494
    :cond_89
    const-string v0, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    .line 606495
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_GENRE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606496
    :cond_8a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606497
    :pswitch_41
    const-string v0, "DISCOVERY_FRIENDS_COMMON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 606498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_COMMON:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606499
    :cond_8b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606500
    :pswitch_42
    const-string v0, "DISCOVERY_PEOPLE_HOMETOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 606501
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606502
    :cond_8c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606503
    :pswitch_43
    const-string v0, "DISCOVERY_FRIENDS_HOMETOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 606504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606505
    :cond_8d
    const-string v0, "PREFILLED_COMPOSER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8e

    .line 606506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PREFILLED_COMPOSER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606507
    :cond_8e
    const-string v0, "POSTS_SET_TOPIC_1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 606508
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606509
    :cond_8f
    const-string v0, "COMMERCE_COMBINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 606510
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606511
    :cond_90
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606512
    :pswitch_44
    const-string v0, "PLACES_CHAIN_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 606513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_CHAIN_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606514
    :cond_91
    const-string v0, "TIMELINE_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_92

    .line 606515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606516
    :cond_92
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606517
    :pswitch_45
    const-string v0, "CURRENCY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 606518
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CURRENCY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606519
    :cond_93
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606520
    :pswitch_46
    const-string v0, "DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 606521
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_TOP_SONGS_FOR_ARTIST_PAGE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606522
    :cond_94
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606523
    :pswitch_47
    const-string v0, "POSTS_SET_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 606524
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606525
    :cond_95
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606526
    :pswitch_48
    const-string v0, "PLACES_SPOTLIGHT_IN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 606527
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PLACES_SPOTLIGHT_IN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606528
    :cond_96
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606529
    :pswitch_49
    const-string v0, "DISCOVERY_GROUPS_ONE_SHOULD_JOIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 606530
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_ONE_SHOULD_JOIN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606531
    :cond_97
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606532
    :pswitch_4a
    const-string v0, "MEDIA_COMBINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 606533
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606534
    :cond_98
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606535
    :pswitch_4b
    const-string v0, "POST_SEARCH_SECTION_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 606536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SEARCH_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606537
    :cond_99
    const-string v0, "EMPTY_ENTITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 606538
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606539
    :cond_9a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606540
    :pswitch_4c
    const-string v0, "FEED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 606541
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606542
    :cond_9b
    const-string v0, "DEBATE_FEELS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 606543
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_FEELS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606544
    :cond_9c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606545
    :pswitch_4d
    const-string v0, "VIDEOS_WEB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 606546
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606547
    :cond_9d
    const-string v0, "FEED_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 606548
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606549
    :cond_9e
    const-string v0, "DEBATE_ISSUES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 606550
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DEBATE_ISSUES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606551
    :cond_9f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606552
    :pswitch_4e
    const-string v0, "POSTS_SET_CELEBRITIES_MENTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 606553
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606554
    :cond_a0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606555
    :pswitch_4f
    const-string v0, "COMMERCE_MARKETPLACE_C2C_COMBINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 606556
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE_C2C_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606557
    :cond_a1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606558
    :pswitch_50
    const-string v0, "WIKIPEDIA_CARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a2

    .line 606559
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606560
    :cond_a2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606561
    :pswitch_51
    const-string v0, "POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 606562
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606563
    :cond_a3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606564
    :pswitch_52
    const-string v0, "FOR_SALE_PRODUCTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 606565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FOR_SALE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606566
    :cond_a4
    const-string v0, "DISCOVERY_MY_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 606567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606568
    :cond_a5
    const-string v0, "DISCOVERY_MY_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 606569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606570
    :cond_a6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606571
    :pswitch_53
    const-string v0, "SPORT_ENTRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 606572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_ENTRY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606573
    :cond_a7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606574
    :pswitch_54
    const-string v0, "PXS_PIVOTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 606575
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PXS_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606576
    :cond_a8
    const-string v0, "DISCOVERY_ENTITY_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 606577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606578
    :cond_a9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606579
    :pswitch_55
    const-string v0, "POSTS_LINKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 606580
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606581
    :cond_aa
    const-string v0, "GRAMMAR_PHOTO_SET_LIKED_BY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 606582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_LIKED_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606583
    :cond_ab
    const-string v0, "DISCOVERY_LOCATION_BARS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 606584
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_BARS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606585
    :cond_ac
    const-string v0, "DISCOVERY_ENTITY_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 606586
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606587
    :cond_ad
    const-string v0, "DISCOVERY_ENTITY_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 606588
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606589
    :cond_ae
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606590
    :pswitch_56
    const-string v0, "PUBLIC_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_af

    .line 606591
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606592
    :cond_af
    const-string v0, "GRAMMAR_PHOTO_SET_BY_ENTITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 606593
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_BY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606594
    :cond_b0
    const-string v0, "DISCOVERY_FRIENDS_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 606595
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606596
    :cond_b1
    const-string v0, "VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 606597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606598
    :cond_b2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606599
    :pswitch_57
    const-string v0, "NEWS_PUBLISHERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 606600
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606601
    :cond_b3
    const-string v0, "NEWS_KEY_VOICES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 606602
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_KEY_VOICES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606603
    :cond_b4
    const-string v0, "DISCOVERY_LOCATION_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b5

    .line 606604
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606605
    :cond_b5
    const-string v0, "DISCOVERY_LOCATION_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 606606
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606607
    :cond_b6
    const-string v0, "DISCOVERY_COMMERCE_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 606608
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606609
    :cond_b7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606610
    :pswitch_58
    const-string v0, "NEWS_PIVOT_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 606611
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606612
    :cond_b8
    const-string v0, "TOP_VIDEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 606613
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606614
    :cond_b9
    const-string v0, "POSTS_CONTENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ba

    .line 606615
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606616
    :cond_ba
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606617
    :pswitch_59
    const-string v0, "NEWS_EYEWITNESSES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 606618
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606619
    :cond_bb
    const-string v0, "PRELOADED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bc

    .line 606620
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PRELOADED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606621
    :cond_bc
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606622
    :pswitch_5a
    const-string v0, "NEWS_TOP_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 606623
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_TOP_VIDEO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606624
    :cond_bd
    const-string v0, "COVER_MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_be

    .line 606625
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COVER_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606626
    :cond_be
    const-string v0, "RELATED_TOPICS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_bf

    .line 606627
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606628
    :cond_bf
    const-string v0, "BLENDED_PHOTO_RELATED_SEARCHES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 606629
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606630
    :cond_c0
    const-string v0, "GRAMMAR_PHOTO_SET_TAGGED_ENTITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c1

    .line 606631
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_TAGGED_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606632
    :cond_c1
    const-string v0, "DISCOVERY_LOCATION_LANDMARKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 606633
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_LANDMARKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606634
    :cond_c2
    const-string v0, "RELATED_SHARES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 606635
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606636
    :cond_c3
    const-string v0, "NAVIGATIONAL_LINKS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 606637
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606638
    :cond_c4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606639
    :pswitch_5b
    const-string v0, "DISCOVERY_PEOPLE_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c5

    .line 606640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606641
    :cond_c5
    const-string v0, "POSTS_SET_REVIEWS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 606642
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606643
    :cond_c6
    const-string v0, "POSTS_SET_RECIPES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 606644
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606645
    :cond_c7
    const-string v0, "COMMERCE_DPA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 606646
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_DPA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606647
    :cond_c8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606648
    :pswitch_5c
    const-string v0, "DISCOVERY_FRIENDS_PYMK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 606649
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_PYMK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606650
    :cond_c9
    const-string v0, "DISCOVERY_FRIENDS_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 606651
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606652
    :cond_ca
    const-string v0, "TOP_FEED_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 606653
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606654
    :cond_cb
    const-string v0, "POSTS_SET_TOPIC_2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 606655
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606656
    :cond_cc
    const-string v0, "POSTS_SET_CONTENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cd

    .line 606657
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606658
    :cond_cd
    const-string v0, "RELATED_ENTITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 606659
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606660
    :cond_ce
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606661
    :pswitch_5d
    const-string v0, "DISCOVERY_FRIENDS_HOSTED_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 606662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOSTED_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606663
    :cond_cf
    const-string v0, "DISCOVERY_FRIENDS_FUTURE_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 606664
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_FUTURE_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606665
    :cond_d0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606666
    :pswitch_5e
    const-string v0, "VOTING_ACTIONS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 606667
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VOTING_ACTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606668
    :cond_d1
    const-string v0, "POSTS_SET_HOW_TO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d2

    .line 606669
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606670
    :cond_d2
    const-string v0, "DISCOVERY_EXPLICIT_INTENT_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 606671
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606672
    :cond_d3
    const-string v0, "DISCOVERY_EXPLICIT_INTENT_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d4

    .line 606673
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EXPLICIT_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606674
    :cond_d4
    const-string v0, "DISCOVERY_COMMERCE_INTENT_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d5

    .line 606675
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_COMMERCE_INTENT_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606676
    :cond_d5
    const-string v0, "ELECTION_DATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 606677
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTION_DATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606678
    :cond_d6
    const-string v0, "TOP_PUBLIC_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 606679
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606680
    :cond_d7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606681
    :pswitch_5f
    const-string v0, "POSTS_SET_CELEBRITIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 606682
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606683
    :cond_d8
    const-string v0, "DISCOVERY_FRIENDS_UPCOMING_EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 606684
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606685
    :cond_d9
    const-string v0, "COMMERCE_MARKETPLACE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_da

    .line 606686
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606687
    :cond_da
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606688
    :pswitch_60
    const-string v0, "MAIN_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 606689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606690
    :cond_db
    const-string v0, "VIDEO_PUBLISHERS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 606691
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606692
    :cond_dc
    const-string v0, "BLENDED_PHOTO_PUBLIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 606693
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606694
    :cond_dd
    const-string v0, "PHOTOS_WITH_MY_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_de

    .line 606695
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PHOTOS_WITH_MY_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606696
    :cond_de
    const-string v0, "DISCOVERY_NON_PEOPLE_RELATED_PAGES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 606697
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606698
    :cond_df
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606699
    :pswitch_61
    const-string v0, "POSTS_SET_VITAL_AUTHORS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 606700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606701
    :cond_e0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606702
    :pswitch_62
    const-string v0, "POSTS_SET_RELATED_AUTHORS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 606703
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606704
    :cond_e1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606705
    :pswitch_63
    const-string v0, "DISCOVERY_LOCATION_PEOPLE_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 606706
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606707
    :cond_e2
    const-string v0, "GAMETIME_FAN_FAVORITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e3

    .line 606708
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606709
    :cond_e3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606710
    :pswitch_64
    const-string v0, "DISCOVERY_LOCATION_FRIENDS_WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e4

    .line 606711
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606712
    :cond_e4
    const-string v0, "MUTUALITY_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e5

    .line 606713
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MUTUALITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606714
    :cond_e5
    const-string v0, "EXTERNAL_ACCOUNTS_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 606715
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EXTERNAL_ACCOUNTS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606716
    :cond_e6
    const-string v0, "RELATED_SHARES_WITH_POSTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e7

    .line 606717
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES_WITH_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606718
    :cond_e7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606719
    :pswitch_65
    const-string v0, "SPORT_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e8

    .line 606720
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606721
    :cond_e8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606722
    :pswitch_66
    const-string v0, "INSTAGRAM_PHOTOS_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 606723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->INSTAGRAM_PHOTOS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606724
    :cond_e9
    const-string v0, "GLOBAL_SHARE_METADATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 606725
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606726
    :cond_ea
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606727
    :pswitch_67
    const-string v0, "GRAMMAR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_eb

    .line 606728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606729
    :cond_eb
    const-string v0, "DISCOVERY_NON_PEOPLE_MUSIC_SONG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ec

    .line 606730
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_MUSIC_SONG:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606731
    :cond_ec
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606732
    :pswitch_68
    const-string v0, "PROMOTED_ENTITY_RELATED_SEARCHES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 606733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_RELATED_SEARCHES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606734
    :cond_ed
    const-string v0, "GRAMMAR_QUERY_ENTITY_MODULE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 606735
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606736
    :cond_ee
    const-string v0, "SHORTCUT_PEOPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ef

    .line 606737
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606738
    :cond_ef
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606739
    :pswitch_69
    const-string v0, "ENTITY_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 606740
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606741
    :cond_f0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606742
    :pswitch_6a
    const-string v0, "FEATURED_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 606743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEATURED_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    .line 606744
    :cond_f1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_0
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_0
        :pswitch_3b
        :pswitch_3c
        :pswitch_0
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_0
        :pswitch_52
        :pswitch_53
        :pswitch_0
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_0
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_0
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 606153
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 606152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method
