.class public final enum Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum ACCEPT_PENDING_THREAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum ADD_CONTACT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum ADS_WELCOME_MSG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum AD_MANAGE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum AD_REPLY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum BOT_THREAD_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CAPY_AGENT_JOIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CAPY_SESSION_BEGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CAPY_SESSION_END:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_JOINABLE_SETTING:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_THREAD_ADMINS:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_THREAD_APPROVAL_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum CONFIRM_FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum DESTINATION_ETA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum GROUP_SMS_PARTICIPANT_CAPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum GROUP_SMS_PARTICIPANT_JOINED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum INVITE_ACCEPTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOINABLE_GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_BOT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_COLOR:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_LIKE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_NEW_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum JOURNEY_PROMPT_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum LIVE_VIDEO_CHAT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MADE_POLL_VOTE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_BOT_REVIEW_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_GROUP_DESCRITPION_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_INBOX2_BIRTHDAY_BUMP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_INVITE_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_NEW_USER_GET_STARTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_OMNIM_CREATE_FLOW:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_OMNIM_UPDATE_FLOW_STATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_PRECHECKED_PLUGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_STATION_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MESSENGER_USER_ALSO_ON_MESSENGER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MN_ACCOUNT_FORCED_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MN_ACCOUNT_LINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MN_ACCOUNT_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum MN_REF_SEND_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum M_AI_SURVEY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_ACCEPT_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_ADMIN_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_ADMIN_DECLINE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_APPOINTMENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_CREATE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_DECLINE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_REQUEST_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PAGES_PLATFORM_USER_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PHONE_CONTACT_UPLOAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PHONE_NUMBER_LOOKUP_NOTICE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum POKE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_PAID:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_RECEIPT_REJECTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_RECEIPT_UPLOADED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_SHIPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum PRODUCT_INVOICE_VOIDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum RAMP_UP_WELCOME_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum RIDE_ARRIVED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum RIDE_ORDERED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum RTC_INSTANT_VIDEO_LIFECYCLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum SAFETY_LOCATION_REQUEST_DENIED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum SAFETY_LOCATION_REQUEST_RESPONDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum SAFETY_LOCATION_REQUEST_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum SMS_PHONE_NUMBER_CHECK:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum SMS_PHONE_NUMBER_TOGGLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum TAGGED_PHOTO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum THREAD_JOINABLE_PROMOTION_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum TURN_ON_PUSH:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726358
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726359
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MADE_POLL_VOTE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MADE_POLL_VOTE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726360
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CONFIRM_FRIEND_REQUEST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CONFIRM_FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "ACCEPT_PENDING_THREAD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ACCEPT_PENDING_THREAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "RAMP_UP_WELCOME_MESSAGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RAMP_UP_WELCOME_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_THREAD_THEME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_THREAD_ICON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "GROUP_THREAD_CREATED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726366
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "THREAD_EPHEMERAL_SEND_MODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726367
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "INVITE_ACCEPTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INVITE_ACCEPTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726368
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_INVITE_SENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INVITE_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726369
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "TURN_ON_PUSH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TURN_ON_PUSH:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726370
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_COLOR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_COLOR:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_LIKE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_LIKE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726372
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_NICKNAME"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726373
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_SETUP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726374
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_NEW_SETUP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NEW_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOURNEY_PROMPT_BOT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_BOT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_THREAD_NICKNAME"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "BOT_THREAD_SUBSCRIPTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->BOT_THREAD_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "RTC_CALL_LOG"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "RIDE_ORDERED_MESSAGE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ORDERED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "DESTINATION_ETA_MESSAGE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->DESTINATION_ETA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "RIDE_ARRIVED_MESSAGE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ARRIVED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_CREATE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726383
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_DELETE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726384
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_NOTIFY"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726385
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726386
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_RSVP"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726387
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_UPDATE_LOCATION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726388
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_UPDATE_TIME"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726389
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_UPDATE_TITLE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726390
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIGHTWEIGHT_EVENT_UPDATE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726391
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "AD_MANAGE_MESSAGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_MANAGE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726392
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "AD_REPLY_MESSAGE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_REPLY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726393
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "TAGGED_PHOTO"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TAGGED_PHOTO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726394
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "GAME_SCORE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726395
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "INSTANT_GAME_UPDATE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726396
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "M_AI_SURVEY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->M_AI_SURVEY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726397
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_REQUEST_TEXT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_REQUEST_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726398
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PHONE_NUMBER_LOOKUP_NOTICE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_NUMBER_LOOKUP_NOTICE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726399
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_BOT_REVIEW_SENT"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_BOT_REVIEW_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726400
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "ADD_CONTACT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADD_CONTACT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726401
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_RECEIPT_REJECTED"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_REJECTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726402
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_PAID"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAID:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726403
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_PAYMENT_EXPIRED"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726404
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_RECEIPT_UPLOADED"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_UPLOADED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726405
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_VOIDED"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_VOIDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726406
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PRODUCT_INVOICE_SHIPPED"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_SHIPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726407
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_JOINABLE_SETTING"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_JOINABLE_SETTING:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726408
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_THREAD_ADMINS"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ADMINS:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726409
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CHANGE_THREAD_APPROVAL_MODE"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_APPROVAL_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726410
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CAPY_SESSION_BEGIN"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_BEGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726411
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CAPY_SESSION_END"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_END:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726412
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "CAPY_AGENT_JOIN"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_AGENT_JOIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726413
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "GROUP_SMS_PARTICIPANT_JOINED"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_JOINED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726414
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "GROUP_SMS_PARTICIPANT_CAPPED"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_CAPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726415
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "SMS_PHONE_NUMBER_CHECK"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_CHECK:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726416
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MN_ACCOUNT_LINKING_TEXT"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_LINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MN_ACCOUNT_UNLINKING_TEXT"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MN_ACCOUNT_FORCED_UNLINKING_TEXT"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_FORCED_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "ADS_WELCOME_MSG"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADS_WELCOME_MSG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "THREAD_JOINABLE_PROMOTION_TEXT"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_JOINABLE_PROMOTION_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_NEW_USER_GET_STARTED"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_NEW_USER_GET_STARTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "SMS_PHONE_NUMBER_TOGGLE"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_TOGGLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726423
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_CREATE_APPOINTMENT"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_CREATE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726424
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "JOINABLE_GROUP_THREAD_CREATED"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOINABLE_GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726425
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_ACCEPT_APPOINTMENT"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ACCEPT_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726426
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_DECLINE_APPOINTMENT"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_DECLINE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726427
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_USER_CANCEL"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_USER_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726428
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_ADMIN_CANCEL"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726429
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_GROUP_DESCRITPION_UPDATE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_GROUP_DESCRITPION_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726430
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "RTC_INSTANT_VIDEO_LIFECYCLE"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_INSTANT_VIDEO_LIFECYCLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726431
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_PRECHECKED_PLUGIN"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_PRECHECKED_PLUGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726432
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "GROUP_POLL"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726433
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_EXTENSION_ADD_CART"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726434
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_APPOINTMENT_REMINDER"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_APPOINTMENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726435
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PAGES_PLATFORM_ADMIN_DECLINE"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_DECLINE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726436
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MEDIA_SUBSCRIPTION_MANAGE"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726437
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "POKE_RECEIVED"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->POKE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726438
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_STATION_SUBSCRIPTION"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_STATION_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726439
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MN_REF_SEND_TEXT"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_REF_SEND_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726440
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_EXTENSION_ADD_FAVORITE"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726441
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PHONE_CONTACT_UPLOAD"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_CONTACT_UPLOAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726442
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "SAFETY_LOCATION_REQUEST_SENT"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726443
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "SAFETY_LOCATION_REQUEST_RESPONDED"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_RESPONDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726444
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "SAFETY_LOCATION_REQUEST_DENIED"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_DENIED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726445
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_USER_ALSO_ON_MESSENGER"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_USER_ALSO_ON_MESSENGER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726446
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_OMNIM_UPDATE_FLOW_STATE"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_UPDATE_FLOW_STATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726447
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_INBOX2_BIRTHDAY_BUMP"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INBOX2_BIRTHDAY_BUMP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726448
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "STARTED_SHARING_VIDEO"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726449
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "LIVE_VIDEO_CHAT"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIVE_VIDEO_CHAT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726450
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_OMNIM_CREATE_FLOW"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_CREATE_FLOW:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726451
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "PARTICIPANT_JOINED_GROUP_CALL"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726452
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    const-string v1, "MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726453
    const/16 v0, 0x5f

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MADE_POLL_VOTE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CONFIRM_FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ACCEPT_PENDING_THREAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RAMP_UP_WELCOME_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INVITE_ACCEPTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INVITE_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TURN_ON_PUSH:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_COLOR:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_LIKE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NEW_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_BOT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->BOT_THREAD_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ORDERED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->DESTINATION_ETA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ARRIVED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_MANAGE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_REPLY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TAGGED_PHOTO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->M_AI_SURVEY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_REQUEST_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_NUMBER_LOOKUP_NOTICE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_BOT_REVIEW_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADD_CONTACT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_REJECTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAID:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_UPLOADED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_VOIDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_SHIPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_JOINABLE_SETTING:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ADMINS:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_APPROVAL_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_BEGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_END:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_AGENT_JOIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_JOINED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_CAPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_CHECK:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_LINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_FORCED_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADS_WELCOME_MSG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_JOINABLE_PROMOTION_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_NEW_USER_GET_STARTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_TOGGLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_CREATE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOINABLE_GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ACCEPT_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_DECLINE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_USER_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_GROUP_DESCRITPION_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_INSTANT_VIDEO_LIFECYCLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_PRECHECKED_PLUGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_APPOINTMENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_DECLINE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->POKE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_STATION_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_REF_SEND_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_CONTACT_UPLOAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_RESPONDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_DENIED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_USER_ALSO_ON_MESSENGER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_UPDATE_FLOW_STATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INBOX2_BIRTHDAY_BUMP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIVE_VIDEO_CHAT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_CREATE_FLOW:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726357
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;
    .locals 2

    .prologue
    .line 726119
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 726120
    :goto_0
    return-object v0

    .line 726121
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x3f

    .line 726122
    packed-switch v0, :pswitch_data_0

    .line 726123
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726124
    :pswitch_1
    const-string v0, "RIDE_ORDERED_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726125
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ORDERED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726126
    :cond_2
    const-string v0, "RIDE_ARRIVED_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RIDE_ARRIVED_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726128
    :cond_3
    const-string v0, "MEDIA_SUBSCRIPTION_MANAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MEDIA_SUBSCRIPTION_MANAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726130
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726131
    :pswitch_2
    const-string v0, "RAMP_UP_WELCOME_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726132
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RAMP_UP_WELCOME_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726133
    :cond_5
    const-string v0, "LIGHTWEIGHT_EVENT_UPDATE_TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726134
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726135
    :cond_6
    const-string v0, "CAPY_AGENT_JOIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726136
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_AGENT_JOIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726137
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726138
    :pswitch_3
    const-string v0, "GROUP_POLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726139
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto :goto_0

    .line 726140
    :cond_8
    const-string v0, "CONFIRM_FRIEND_REQUEST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 726141
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CONFIRM_FRIEND_REQUEST:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726142
    :cond_9
    const-string v0, "LIGHTWEIGHT_EVENT_UPDATE_TITLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726143
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726144
    :cond_a
    const-string v0, "PHONE_NUMBER_LOOKUP_NOTICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 726145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_NUMBER_LOOKUP_NOTICE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726146
    :cond_b
    const-string v0, "SMS_PHONE_NUMBER_TOGGLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 726147
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_TOGGLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726148
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726149
    :pswitch_4
    const-string v0, "CHANGE_THREAD_ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726150
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ICON:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726151
    :cond_d
    const-string v0, "CAPY_SESSION_BEGIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 726152
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_BEGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726153
    :cond_e
    const-string v0, "PAGES_PLATFORM_ADMIN_DECLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 726154
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_DECLINE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726155
    :cond_f
    const-string v0, "LIVE_VIDEO_CHAT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 726156
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIVE_VIDEO_CHAT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726157
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726158
    :pswitch_5
    const-string v0, "RTC_INSTANT_VIDEO_LIFECYCLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 726159
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_INSTANT_VIDEO_LIFECYCLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726160
    :cond_11
    const-string v0, "JOURNEY_PROMPT_BOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 726161
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_BOT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726162
    :cond_12
    const-string v0, "MESSENGER_EXTENSION_ADD_FAVORITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 726163
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_FAVORITE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726164
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726165
    :pswitch_6
    const-string v0, "THREAD_EPHEMERAL_SEND_MODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 726166
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_EPHEMERAL_SEND_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726167
    :cond_14
    const-string v0, "MN_REF_SEND_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 726168
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_REF_SEND_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726169
    :cond_15
    const-string v0, "MESSENGER_OMNIM_UPDATE_FLOW_STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 726170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_UPDATE_FLOW_STATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726171
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726172
    :pswitch_7
    const-string v0, "MESSENGER_GROUP_DESCRITPION_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 726173
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_GROUP_DESCRITPION_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726174
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726175
    :pswitch_8
    const-string v0, "BOT_THREAD_SUBSCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 726176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->BOT_THREAD_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726177
    :cond_18
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726178
    :pswitch_9
    const-string v0, "JOURNEY_PROMPT_COLOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 726179
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_COLOR:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726180
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726181
    :pswitch_a
    const-string v0, "MESSENGER_INVITE_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 726182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INVITE_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726183
    :cond_1a
    const-string v0, "JOURNEY_PROMPT_SETUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 726184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726185
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726186
    :pswitch_b
    const-string v0, "CAPY_SESSION_END"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 726187
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CAPY_SESSION_END:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726188
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726189
    :pswitch_c
    const-string v0, "MN_ACCOUNT_LINKING_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 726190
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_LINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726191
    :cond_1d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726192
    :pswitch_d
    const-string v0, "ACCEPT_PENDING_THREAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 726193
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ACCEPT_PENDING_THREAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726194
    :cond_1e
    const-string v0, "JOURNEY_PROMPT_NEW_SETUP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 726195
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NEW_SETUP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726196
    :cond_1f
    const-string v0, "LIGHTWEIGHT_EVENT_RSVP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 726197
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726198
    :cond_20
    const-string v0, "MESSENGER_BOT_REVIEW_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 726199
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_BOT_REVIEW_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726200
    :cond_21
    const-string v0, "MN_ACCOUNT_UNLINKING_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 726201
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726202
    :cond_22
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726203
    :pswitch_e
    const-string v0, "INVITE_ACCEPTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 726204
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INVITE_ACCEPTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726205
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726206
    :pswitch_f
    const-string v0, "MESSENGER_EXTENSION_ADD_CART"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 726207
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_EXTENSION_ADD_CART:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726208
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726209
    :pswitch_10
    const-string v0, "GROUP_THREAD_CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 726210
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726211
    :cond_25
    const-string v0, "PAGES_PLATFORM_REQUEST_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 726212
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_REQUEST_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726213
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726214
    :pswitch_11
    const-string v0, "TURN_ON_PUSH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 726215
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TURN_ON_PUSH:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726216
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726217
    :pswitch_12
    const-string v0, "MN_ACCOUNT_FORCED_UNLINKING_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 726218
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MN_ACCOUNT_FORCED_UNLINKING_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726219
    :cond_28
    const-string v0, "POKE_RECEIVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 726220
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->POKE_RECEIVED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726221
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726222
    :pswitch_13
    const-string v0, "MESSENGER_PRECHECKED_PLUGIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 726223
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_PRECHECKED_PLUGIN:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726224
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726225
    :pswitch_14
    const-string v0, "SAFETY_LOCATION_REQUEST_SENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 726226
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_SENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726227
    :cond_2b
    const-string v0, "MESSENGER_USER_ALSO_ON_MESSENGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 726228
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_USER_ALSO_ON_MESSENGER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726229
    :cond_2c
    const-string v0, "MESSENGER_INBOX2_BIRTHDAY_BUMP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 726230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_INBOX2_BIRTHDAY_BUMP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726231
    :cond_2d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726232
    :pswitch_15
    const-string v0, "LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 726233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY_BEFORE_EVENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726234
    :cond_2e
    const-string v0, "PAGES_PLATFORM_CREATE_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 726235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_CREATE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726236
    :cond_2f
    const-string v0, "PAGES_PLATFORM_ACCEPT_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 726237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ACCEPT_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726238
    :cond_30
    const-string v0, "MESSENGER_STATION_SUBSCRIPTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 726239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_STATION_SUBSCRIPTION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726240
    :cond_31
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726241
    :pswitch_16
    const-string v0, "THREAD_JOINABLE_PROMOTION_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 726242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->THREAD_JOINABLE_PROMOTION_TEXT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726243
    :cond_32
    const-string v0, "PAGES_PLATFORM_DECLINE_APPOINTMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 726244
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_DECLINE_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726245
    :cond_33
    const-string v0, "PAGES_PLATFORM_USER_CANCEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 726246
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_USER_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726247
    :cond_34
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726248
    :pswitch_17
    const-string v0, "LIGHTWEIGHT_EVENT_UPDATE_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 726249
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726250
    :cond_35
    const-string v0, "M_AI_SURVEY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 726251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->M_AI_SURVEY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726252
    :cond_36
    const-string v0, "GROUP_SMS_PARTICIPANT_JOINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 726253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_JOINED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726254
    :cond_37
    const-string v0, "GROUP_SMS_PARTICIPANT_CAPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 726255
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GROUP_SMS_PARTICIPANT_CAPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726256
    :cond_38
    const-string v0, "PAGES_PLATFORM_ADMIN_CANCEL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 726257
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_ADMIN_CANCEL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726258
    :cond_39
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726259
    :pswitch_18
    const-string v0, "PHONE_CONTACT_UPLOAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 726260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PHONE_CONTACT_UPLOAD:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726261
    :cond_3a
    const-string v0, "PRODUCT_INVOICE_PAID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 726262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAID:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726263
    :cond_3b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726264
    :pswitch_19
    const-string v0, "PAGES_PLATFORM_APPOINTMENT_REMINDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 726265
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PAGES_PLATFORM_APPOINTMENT_REMINDER:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726266
    :cond_3c
    const-string v0, "PARTICIPANT_JOINED_GROUP_CALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 726267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726268
    :cond_3d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726269
    :pswitch_1a
    const-string v0, "PRODUCT_INVOICE_VOIDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 726270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_VOIDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726271
    :cond_3e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726272
    :pswitch_1b
    const-string v0, "PRODUCT_INVOICE_SHIPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 726273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_SHIPPED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726274
    :cond_3f
    const-string v0, "JOINABLE_GROUP_THREAD_CREATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 726275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOINABLE_GROUP_THREAD_CREATED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726276
    :cond_40
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726277
    :pswitch_1c
    const-string v0, "CHANGE_THREAD_ADMINS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 726278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_ADMINS:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726279
    :cond_41
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726280
    :pswitch_1d
    const-string v0, "MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 726281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_ONLY_PHONE_JOINED_WITH_NEW_ACCOUNT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726282
    :cond_42
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726283
    :pswitch_1e
    const-string v0, "MESSENGER_NEW_USER_GET_STARTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 726284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_NEW_USER_GET_STARTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726285
    :cond_43
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726286
    :pswitch_1f
    const-string v0, "ADS_WELCOME_MSG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 726287
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADS_WELCOME_MSG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726288
    :cond_44
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726289
    :pswitch_20
    const-string v0, "LIGHTWEIGHT_EVENT_NOTIFY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 726290
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726291
    :cond_45
    const-string v0, "PRODUCT_INVOICE_PAYMENT_EXPIRED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 726292
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726293
    :cond_46
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726294
    :pswitch_21
    const-string v0, "AD_REPLY_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 726295
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_REPLY_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726296
    :cond_47
    const-string v0, "GAME_SCORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 726297
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726298
    :cond_48
    const-string v0, "PRODUCT_INVOICE_RECEIPT_REJECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 726299
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_REJECTED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726300
    :cond_49
    const-string v0, "PRODUCT_INVOICE_RECEIPT_UPLOADED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 726301
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PRODUCT_INVOICE_RECEIPT_UPLOADED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726302
    :cond_4a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726303
    :pswitch_22
    const-string v0, "SAFETY_LOCATION_REQUEST_DENIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 726304
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_DENIED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726305
    :cond_4b
    const-string v0, "AD_MANAGE_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 726306
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->AD_MANAGE_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726307
    :cond_4c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726308
    :pswitch_23
    const-string v0, "SAFETY_LOCATION_REQUEST_RESPONDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 726309
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SAFETY_LOCATION_REQUEST_RESPONDED:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726310
    :cond_4d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726311
    :pswitch_24
    const-string v0, "CHANGE_THREAD_THEME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 726312
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_THEME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726313
    :cond_4e
    const-string v0, "TAGGED_PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 726314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->TAGGED_PHOTO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726315
    :cond_4f
    const-string v0, "MESSENGER_OMNIM_CREATE_FLOW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 726316
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MESSENGER_OMNIM_CREATE_FLOW:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726317
    :cond_50
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726318
    :pswitch_25
    const-string v0, "CHANGE_JOINABLE_SETTING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 726319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_JOINABLE_SETTING:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726320
    :cond_51
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726321
    :pswitch_26
    const-string v0, "CHANGE_THREAD_NICKNAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 726322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726323
    :cond_52
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726324
    :pswitch_27
    const-string v0, "MADE_POLL_VOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 726325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->MADE_POLL_VOTE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726326
    :cond_53
    const-string v0, "DESTINATION_ETA_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 726327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->DESTINATION_ETA_MESSAGE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726328
    :cond_54
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726329
    :pswitch_28
    const-string v0, "RTC_CALL_LOG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 726330
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->RTC_CALL_LOG:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726331
    :cond_55
    const-string v0, "INSTANT_GAME_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 726332
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->INSTANT_GAME_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726333
    :cond_56
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726334
    :pswitch_29
    const-string v0, "JOURNEY_PROMPT_LIKE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 726335
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_LIKE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726336
    :cond_57
    const-string v0, "ADD_CONTACT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 726337
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->ADD_CONTACT:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726338
    :cond_58
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726339
    :pswitch_2a
    const-string v0, "CHANGE_THREAD_APPROVAL_MODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 726340
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->CHANGE_THREAD_APPROVAL_MODE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726341
    :cond_59
    const-string v0, "STARTED_SHARING_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 726342
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726343
    :cond_5a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726344
    :pswitch_2b
    const-string v0, "JOURNEY_PROMPT_NICKNAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 726345
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->JOURNEY_PROMPT_NICKNAME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726346
    :cond_5b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726347
    :pswitch_2c
    const-string v0, "SMS_PHONE_NUMBER_CHECK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 726348
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->SMS_PHONE_NUMBER_CHECK:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726349
    :cond_5c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726350
    :pswitch_2d
    const-string v0, "LIGHTWEIGHT_EVENT_CREATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 726351
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726352
    :cond_5d
    const-string v0, "LIGHTWEIGHT_EVENT_DELETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 726353
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726354
    :cond_5e
    const-string v0, "LIGHTWEIGHT_EVENT_UPDATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 726355
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    .line 726356
    :cond_5f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_0
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_0
        :pswitch_0
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;
    .locals 1

    .prologue
    .line 726118
    const-class v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;
    .locals 1

    .prologue
    .line 726117
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    return-object v0
.end method
