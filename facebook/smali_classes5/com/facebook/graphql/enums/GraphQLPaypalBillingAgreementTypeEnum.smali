.class public final enum Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

.field public static final enum CIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

.field public static final enum MIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

.field public static final enum NOT_SPECIFIED:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 737846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    .line 737847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    const-string v1, "NOT_SPECIFIED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->NOT_SPECIFIED:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    .line 737848
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    const-string v1, "MIB"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->MIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    .line 737849
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    const-string v1, "CIB"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->CIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    .line 737850
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->NOT_SPECIFIED:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->MIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->CIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;
    .locals 1

    .prologue
    .line 737851
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    .line 737852
    :goto_0
    return-object v0

    .line 737853
    :cond_1
    const-string v0, "NOT_SPECIFIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737854
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->NOT_SPECIFIED:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    goto :goto_0

    .line 737855
    :cond_2
    const-string v0, "MIB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737856
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->MIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    goto :goto_0

    .line 737857
    :cond_3
    const-string v0, "CIB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737858
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->CIB:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    goto :goto_0

    .line 737859
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;
    .locals 1

    .prologue
    .line 737844
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;
    .locals 1

    .prologue
    .line 737843
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    return-object v0
.end method
