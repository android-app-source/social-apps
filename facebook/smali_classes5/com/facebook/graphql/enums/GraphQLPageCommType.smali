.class public final enum Lcom/facebook/graphql/enums/GraphQLPageCommType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageCommType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum DEPRECATED_2:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_AD_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_PAGE_POST_COMMENT:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_REVIEW:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum FB_USER_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum INSTAGRAM_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 736869
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736870
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_MESSAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736871
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "DEPRECATED_2"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->DEPRECATED_2:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736872
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_PAGE_POST"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736873
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "INSTAGRAM_POST"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->INSTAGRAM_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736874
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_USER_POST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_USER_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736875
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_REVIEW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_REVIEW:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736876
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_PAGE_POST_COMMENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST_COMMENT:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736877
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    const-string v1, "FB_AD_POST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCommType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_AD_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736878
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageCommType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommType;->DEPRECATED_2:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCommType;->INSTAGRAM_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_USER_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_REVIEW:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST_COMMENT:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_AD_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 736881
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommType;
    .locals 1

    .prologue
    .line 736882
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 736883
    :goto_0
    return-object v0

    .line 736884
    :cond_1
    const-string v0, "FB_MESSAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 736885
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736886
    :cond_2
    const-string v0, "FB_PAGE_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 736887
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736888
    :cond_3
    const-string v0, "INSTAGRAM_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 736889
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->INSTAGRAM_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736890
    :cond_4
    const-string v0, "FB_USER_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 736891
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_USER_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736892
    :cond_5
    const-string v0, "FB_REVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 736893
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_REVIEW:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736894
    :cond_6
    const-string v0, "FB_PAGE_POST_COMMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 736895
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_PAGE_POST_COMMENT:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736896
    :cond_7
    const-string v0, "FB_AD_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 736897
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->FB_AD_POST:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0

    .line 736898
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCommType;
    .locals 1

    .prologue
    .line 736880
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageCommType;
    .locals 1

    .prologue
    .line 736879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageCommType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageCommType;

    return-object v0
.end method
