.class public final enum Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

.field public static final enum LIVE_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

.field public static final enum LIVE_ENDED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

.field public static final enum LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741153
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 741154
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    const-string v1, "LIVE_STARTED"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 741155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    const-string v1, "LIVE_ENDED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_ENDED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 741156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    const-string v1, "LIVE_DELETED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 741157
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_ENDED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 741158
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .locals 1

    .prologue
    .line 741159
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 741160
    :goto_0
    return-object v0

    .line 741161
    :cond_1
    const-string v0, "LIVE_STARTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741162
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    goto :goto_0

    .line 741163
    :cond_2
    const-string v0, "LIVE_ENDED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 741164
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_ENDED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    goto :goto_0

    .line 741165
    :cond_3
    const-string v0, "LIVE_DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 741166
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_DELETED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    goto :goto_0

    .line 741167
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .locals 1

    .prologue
    .line 741168
    const-class v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .locals 1

    .prologue
    .line 741169
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    return-object v0
.end method
