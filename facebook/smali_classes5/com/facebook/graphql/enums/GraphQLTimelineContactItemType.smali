.class public final enum Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public static final enum ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public static final enum PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 740710
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 740711
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 740712
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 740713
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 740714
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;
    .locals 1

    .prologue
    .line 740716
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 740717
    :goto_0
    return-object v0

    .line 740718
    :cond_1
    const-string v0, "ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740719
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    goto :goto_0

    .line 740720
    :cond_2
    const-string v0, "PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740721
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    goto :goto_0

    .line 740722
    :cond_3
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 740723
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    goto :goto_0

    .line 740724
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;
    .locals 1

    .prologue
    .line 740725
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;
    .locals 1

    .prologue
    .line 740726
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    return-object v0
.end method
