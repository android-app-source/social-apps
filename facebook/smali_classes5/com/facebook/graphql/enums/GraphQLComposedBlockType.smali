.class public final enum Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLComposedBlockType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_FIVE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_FOUR:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_ONE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_SIX:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_THREE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum HEADER_TWO:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum MEDIA:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum PULLQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public static final enum UNSTYLED:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724626
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724627
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "UNSTYLED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSTYLED:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724628
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "PARAGRAPH"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724629
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "UNORDERED_LIST_ITEM"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724630
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "ORDERED_LIST_ITEM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724631
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "BLOCKQUOTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724632
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_ONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_ONE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724633
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_TWO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_TWO:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724634
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "CODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724635
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "MEDIA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724636
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "PULLQUOTE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PULLQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724637
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_THREE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_THREE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724638
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_FOUR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FOUR:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724639
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_FIVE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FIVE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724640
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    const-string v1, "HEADER_SIX"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_SIX:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724641
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSTYLED:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_ONE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_TWO:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PULLQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_THREE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FOUR:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FIVE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_SIX:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724625
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
    .locals 1

    .prologue
    .line 724594
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 724595
    :goto_0
    return-object v0

    .line 724596
    :cond_1
    const-string v0, "UNSTYLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSTYLED:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724598
    :cond_2
    const-string v0, "PARAGRAPH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724599
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724600
    :cond_3
    const-string v0, "UNORDERED_LIST_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724601
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724602
    :cond_4
    const-string v0, "ORDERED_LIST_ITEM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724603
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724604
    :cond_5
    const-string v0, "BLOCKQUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724605
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724606
    :cond_6
    const-string v0, "HEADER_ONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724607
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_ONE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724608
    :cond_7
    const-string v0, "HEADER_TWO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724609
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_TWO:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724610
    :cond_8
    const-string v0, "CODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724611
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724612
    :cond_9
    const-string v0, "MEDIA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724613
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->MEDIA:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724614
    :cond_a
    const-string v0, "PULLQUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724615
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PULLQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724616
    :cond_b
    const-string v0, "HEADER_THREE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 724617
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_THREE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto :goto_0

    .line 724618
    :cond_c
    const-string v0, "HEADER_FOUR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 724619
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FOUR:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto/16 :goto_0

    .line 724620
    :cond_d
    const-string v0, "HEADER_FIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 724621
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_FIVE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto/16 :goto_0

    .line 724622
    :cond_e
    const-string v0, "HEADER_SIX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 724623
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_SIX:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto/16 :goto_0

    .line 724624
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
    .locals 1

    .prologue
    .line 724592
    const-class v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
    .locals 1

    .prologue
    .line 724593
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    return-object v0
.end method
