.class public final enum Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum AMAZON_APP_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum FB_CANVAS:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum GOOGLE_PLAY:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum ITUNES:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum ITUNES_IPAD:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum WINDOWS_10_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

.field public static final enum WINDOWS_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724906
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724907
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "AMAZON_APP_STORE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->AMAZON_APP_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724908
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "GOOGLE_PLAY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->GOOGLE_PLAY:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724909
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "FB_CANVAS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_CANVAS:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724910
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "ITUNES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724911
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "ITUNES_IPAD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES_IPAD:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724912
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "WINDOWS_STORE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724913
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "WINDOWS_10_STORE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_10_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724914
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    const-string v1, "FB_ANDROID_STORE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724915
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->AMAZON_APP_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->GOOGLE_PLAY:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_CANVAS:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES_IPAD:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_10_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724916
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;
    .locals 1

    .prologue
    .line 724917
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    .line 724918
    :goto_0
    return-object v0

    .line 724919
    :cond_1
    const-string v0, "AMAZON_APP_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724920
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->AMAZON_APP_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724921
    :cond_2
    const-string v0, "GOOGLE_PLAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->GOOGLE_PLAY:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724923
    :cond_3
    const-string v0, "ITUNES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724924
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724925
    :cond_4
    const-string v0, "ITUNES_IPAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724926
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->ITUNES_IPAD:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724927
    :cond_5
    const-string v0, "FB_CANVAS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724928
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_CANVAS:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724929
    :cond_6
    const-string v0, "WINDOWS_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724930
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724931
    :cond_7
    const-string v0, "FB_ANDROID_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724932
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724933
    :cond_8
    const-string v0, "WINDOWS_10_STORE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724934
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->WINDOWS_10_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0

    .line 724935
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;
    .locals 1

    .prologue
    .line 724936
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;
    .locals 1

    .prologue
    .line 724937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    return-object v0
.end method
