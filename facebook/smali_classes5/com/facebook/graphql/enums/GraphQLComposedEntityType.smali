.class public final enum Lcom/facebook/graphql/enums/GraphQLComposedEntityType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLComposedEntityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum EMBED:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum EMOJI:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum EMOTICON:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum HASHTAG:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum IMAGE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum IMPLICIT_LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum MENTION:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum TOKEN:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724642
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724643
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724644
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "LINK"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724645
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "MENTION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->MENTION:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724646
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724647
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "EMOTICON"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOTICON:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724648
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "TOKEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->TOKEN:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724649
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "HASHTAG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->HASHTAG:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724650
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "IMPLICIT_LINK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMPLICIT_LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724651
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "EMBED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMBED:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724652
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    const-string v1, "EMOJI"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOJI:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724653
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->MENTION:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOTICON:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->TOKEN:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->HASHTAG:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMPLICIT_LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMBED:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOJI:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724654
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedEntityType;
    .locals 1

    .prologue
    .line 724655
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    .line 724656
    :goto_0
    return-object v0

    .line 724657
    :cond_1
    const-string v0, "MENTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724658
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->MENTION:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724659
    :cond_2
    const-string v0, "LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724660
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724661
    :cond_3
    const-string v0, "IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724662
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMAGE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724663
    :cond_4
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724664
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724665
    :cond_5
    const-string v0, "EMOTICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724666
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOTICON:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724667
    :cond_6
    const-string v0, "TOKEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724668
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->TOKEN:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724669
    :cond_7
    const-string v0, "HASHTAG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724670
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->HASHTAG:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724671
    :cond_8
    const-string v0, "IMPLICIT_LINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724672
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->IMPLICIT_LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724673
    :cond_9
    const-string v0, "EMBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMBED:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724675
    :cond_a
    const-string v0, "EMOJI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 724676
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->EMOJI:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0

    .line 724677
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedEntityType;
    .locals 1

    .prologue
    .line 724678
    const-class v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;
    .locals 1

    .prologue
    .line 724679
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    return-object v0
.end method
