.class public final enum Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum BLOCKED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum COMPOSER_DISABLED_BOT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum HAS_EMAIL_PARTICIPANT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum MONTAGE_NOT_AUTHOR:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum OBJECT_ORIGINATED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum READ_ONLY:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum RECIPIENTS_INACTIVE_WORK_ACC:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum RECIPIENTS_INVALID:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum RECIPIENTS_NOT_LOADABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum RECIPIENTS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

.field public static final enum VIEWER_NOT_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 728716
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728717
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728718
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "HAS_EMAIL_PARTICIPANT"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->HAS_EMAIL_PARTICIPANT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728719
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "OBJECT_ORIGINATED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->OBJECT_ORIGINATED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728720
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "READ_ONLY"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->READ_ONLY:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728721
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "COMPOSER_DISABLED_BOT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->COMPOSER_DISABLED_BOT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728722
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "VIEWER_NOT_SUBSCRIBED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->VIEWER_NOT_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728723
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "RECIPIENTS_NOT_LOADABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_NOT_LOADABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728724
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "RECIPIENTS_UNAVAILABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728725
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "RECIPIENTS_INVALID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INVALID:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728726
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "RECIPIENTS_INACTIVE_WORK_ACC"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INACTIVE_WORK_ACC:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728727
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    const-string v1, "MONTAGE_NOT_AUTHOR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->MONTAGE_NOT_AUTHOR:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728728
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->HAS_EMAIL_PARTICIPANT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->OBJECT_ORIGINATED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->READ_ONLY:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->COMPOSER_DISABLED_BOT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->VIEWER_NOT_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_NOT_LOADABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INVALID:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INACTIVE_WORK_ACC:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->MONTAGE_NOT_AUTHOR:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 728729
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .locals 1

    .prologue
    .line 728730
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    .line 728731
    :goto_0
    return-object v0

    .line 728732
    :cond_1
    const-string v0, "BLOCKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->BLOCKED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728734
    :cond_2
    const-string v0, "COMPOSER_DISABLED_BOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 728735
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->COMPOSER_DISABLED_BOT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728736
    :cond_3
    const-string v0, "HAS_EMAIL_PARTICIPANT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 728737
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->HAS_EMAIL_PARTICIPANT:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728738
    :cond_4
    const-string v0, "OBJECT_ORIGINATED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 728739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->OBJECT_ORIGINATED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728740
    :cond_5
    const-string v0, "READ_ONLY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 728741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->READ_ONLY:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728742
    :cond_6
    const-string v0, "VIEWER_NOT_SUBSCRIBED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 728743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->VIEWER_NOT_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728744
    :cond_7
    const-string v0, "RECIPIENTS_NOT_LOADABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 728745
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_NOT_LOADABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728746
    :cond_8
    const-string v0, "RECIPIENTS_UNAVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 728747
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728748
    :cond_9
    const-string v0, "RECIPIENTS_INVALID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 728749
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INVALID:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728750
    :cond_a
    const-string v0, "RECIPIENTS_INACTIVE_WORK_ACC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 728751
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->RECIPIENTS_INACTIVE_WORK_ACC:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728752
    :cond_b
    const-string v0, "MONTAGE_NOT_AUTHOR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 728753
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->MONTAGE_NOT_AUTHOR:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0

    .line 728754
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .locals 1

    .prologue
    .line 728755
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;
    .locals 1

    .prologue
    .line 728756
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    return-object v0
.end method
