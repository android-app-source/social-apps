.class public Lcom/facebook/graphql/enums/GraphQLObjectTypeDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 729597
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectTypeDeserializer;

    invoke-direct {v1}, Lcom/facebook/graphql/enums/GraphQLObjectTypeDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 729598
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 729599
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 729600
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 729601
    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 729602
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>()V

    .line 729603
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 729604
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 729605
    const/4 v0, 0x0

    .line 729606
    :goto_0
    move-object v0, v0

    .line 729607
    return-object v0

    .line 729608
    :cond_0
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_2

    .line 729609
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 729610
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 729611
    const/4 v2, 0x0

    .line 729612
    const-string p0, "name"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 729613
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-ne p0, p2, :cond_3

    :cond_1
    :goto_2
    invoke-static {v2}, LX/38I;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/graphql/enums/GraphQLObjectType;->name:Ljava/lang/String;

    .line 729614
    :goto_3
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 729615
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->f()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    goto :goto_0

    .line 729616
    :cond_3
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 729617
    :cond_4
    goto :goto_3
.end method
