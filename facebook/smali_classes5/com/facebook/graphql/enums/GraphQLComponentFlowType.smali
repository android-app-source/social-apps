.class public final enum Lcom/facebook/graphql/enums/GraphQLComponentFlowType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLComponentFlowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum COMPONENT_LIBRARY:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum CONTACT_US:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum EXTERNAL_SERVICE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum FANDANGO:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum NATIVE_BOOKING:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum NATIVE_LEAD_GEN_GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum NATIVE_ORDER_AHEAD:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum OPEN_TABLE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum SCREEN_PREVIEW:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724557
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724558
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "EXTERNAL_SERVICE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->EXTERNAL_SERVICE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724559
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "CONTACT_US"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724560
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "FANDANGO"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->FANDANGO:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724561
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "NATIVE_BOOKING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_BOOKING:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724562
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "NATIVE_LEAD_GEN_GET_QUOTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_LEAD_GEN_GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724563
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "NATIVE_ORDER_AHEAD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_ORDER_AHEAD:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724564
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "SCREEN_PREVIEW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->SCREEN_PREVIEW:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724565
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "OPEN_TABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->OPEN_TABLE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724566
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    const-string v1, "COMPONENT_LIBRARY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->COMPONENT_LIBRARY:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724567
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->EXTERNAL_SERVICE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->FANDANGO:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_BOOKING:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_LEAD_GEN_GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_ORDER_AHEAD:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->SCREEN_PREVIEW:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->OPEN_TABLE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->COMPONENT_LIBRARY:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724568
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComponentFlowType;
    .locals 1

    .prologue
    .line 724569
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    .line 724570
    :goto_0
    return-object v0

    .line 724571
    :cond_1
    const-string v0, "EXTERNAL_SERVICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724572
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->EXTERNAL_SERVICE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724573
    :cond_2
    const-string v0, "CONTACT_US"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724575
    :cond_3
    const-string v0, "FANDANGO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724576
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->FANDANGO:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724577
    :cond_4
    const-string v0, "NATIVE_BOOKING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724578
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_BOOKING:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724579
    :cond_5
    const-string v0, "NATIVE_LEAD_GEN_GET_QUOTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724580
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_LEAD_GEN_GET_QUOTE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724581
    :cond_6
    const-string v0, "NATIVE_ORDER_AHEAD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->NATIVE_ORDER_AHEAD:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724583
    :cond_7
    const-string v0, "OPEN_TABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724584
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->OPEN_TABLE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724585
    :cond_8
    const-string v0, "SCREEN_PREVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 724586
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->SCREEN_PREVIEW:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724587
    :cond_9
    const-string v0, "COMPONENT_LIBRARY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 724588
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->COMPONENT_LIBRARY:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0

    .line 724589
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComponentFlowType;
    .locals 1

    .prologue
    .line 724590
    const-class v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLComponentFlowType;
    .locals 1

    .prologue
    .line 724591
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    return-object v0
.end method
