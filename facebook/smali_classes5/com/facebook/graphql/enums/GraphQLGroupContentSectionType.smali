.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum COLLEGE_COMMUNITY_NUX:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum COMMUNITY_FORUM_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum CREATE_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum EVENTS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum FOR_SALE_ITEMS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum JOINED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum JOIN_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum LOCAL_MAP:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum LOCAL_NEWS_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum RANKED_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum RECENT_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum SUB_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum SUGGESTED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum TRENDING_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 726923
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726924
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "JOIN_UPSELL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOIN_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726925
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "RECENT_STORIES"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RECENT_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726926
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "RANKED_STORIES"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RANKED_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726927
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "TRENDING_STORIES"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->TRENDING_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726928
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "LOCAL_NEWS_STORIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_NEWS_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "COMMUNITY_FORUM_GROUPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COMMUNITY_FORUM_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726930
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "JOINED_CHILD_GROUPS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOINED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726931
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "SUGGESTED_CHILD_GROUPS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUGGESTED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726932
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "EVENTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726933
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "FOR_SALE_ITEMS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->FOR_SALE_ITEMS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726934
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "COLLEGE_COMMUNITY_NUX"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COLLEGE_COMMUNITY_NUX:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726935
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "INVITE_FRIENDS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726936
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "LOCAL_MAP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_MAP:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726937
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "SUB_GROUPS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUB_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726938
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-string v1, "CREATE_UPSELL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->CREATE_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726939
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOIN_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RECENT_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RANKED_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->TRENDING_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_NEWS_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COMMUNITY_FORUM_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOINED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUGGESTED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->FOR_SALE_ITEMS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COLLEGE_COMMUNITY_NUX:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_MAP:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUB_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->CREATE_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726940
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;
    .locals 1

    .prologue
    .line 726941
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    .line 726942
    :goto_0
    return-object v0

    .line 726943
    :cond_1
    const-string v0, "JOIN_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 726944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOIN_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726945
    :cond_2
    const-string v0, "CREATE_UPSELL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 726946
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->CREATE_UPSELL:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726947
    :cond_3
    const-string v0, "LOCAL_MAP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726948
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_MAP:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726949
    :cond_4
    const-string v0, "RECENT_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726950
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RECENT_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726951
    :cond_5
    const-string v0, "RANKED_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 726952
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->RANKED_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726953
    :cond_6
    const-string v0, "TRENDING_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 726954
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->TRENDING_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726955
    :cond_7
    const-string v0, "LOCAL_NEWS_STORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726956
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_NEWS_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726957
    :cond_8
    const-string v0, "COMMUNITY_FORUM_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 726958
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COMMUNITY_FORUM_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726959
    :cond_9
    const-string v0, "JOINED_CHILD_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 726960
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOINED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726961
    :cond_a
    const-string v0, "SUGGESTED_CHILD_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 726962
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUGGESTED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726963
    :cond_b
    const-string v0, "SUB_GROUPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 726964
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUB_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto :goto_0

    .line 726965
    :cond_c
    const-string v0, "EVENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726966
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto/16 :goto_0

    .line 726967
    :cond_d
    const-string v0, "FOR_SALE_ITEMS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 726968
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->FOR_SALE_ITEMS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto/16 :goto_0

    .line 726969
    :cond_e
    const-string v0, "COLLEGE_COMMUNITY_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 726970
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COLLEGE_COMMUNITY_NUX:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto/16 :goto_0

    .line 726971
    :cond_f
    const-string v0, "INVITE_FRIENDS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 726972
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto/16 :goto_0

    .line 726973
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;
    .locals 1

    .prologue
    .line 726974
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;
    .locals 1

    .prologue
    .line 726975
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    return-object v0
.end method
