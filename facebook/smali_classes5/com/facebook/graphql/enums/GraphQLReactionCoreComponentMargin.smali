.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 738841
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738842
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738843
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738844
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738845
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738846
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "LARGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738847
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    const-string v1, "EXTRA_LARGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738848
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738849
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 1

    .prologue
    .line 738850
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    .line 738851
    :goto_0
    return-object v0

    .line 738852
    :cond_1
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738853
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->NONE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738854
    :cond_2
    const-string v0, "EXTRA_SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738855
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738856
    :cond_3
    const-string v0, "SMALL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738857
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738858
    :cond_4
    const-string v0, "MEDIUM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738859
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738860
    :cond_5
    const-string v0, "LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 738861
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738862
    :cond_6
    const-string v0, "EXTRA_LARGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738863
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0

    .line 738864
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 1

    .prologue
    .line 738865
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;
    .locals 1

    .prologue
    .line 738866
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    return-object v0
.end method
