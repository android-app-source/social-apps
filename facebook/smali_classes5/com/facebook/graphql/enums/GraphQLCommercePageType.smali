.class public final enum Lcom/facebook/graphql/enums/GraphQLCommercePageType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLCommercePageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_AGENT:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_AIRLINE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_BANK:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_BUSINESS:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_NONE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum COMMERCE_PAGE_TYPE_RIDE_SHARE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 724417
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724418
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_NONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_NONE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724419
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_BUSINESS"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BUSINESS:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724420
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_AGENT"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AGENT:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724421
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_AIRLINE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AIRLINE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724422
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_RIDE_SHARE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_RIDE_SHARE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724423
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_BANK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BANK:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724424
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    const-string v1, "COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724425
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_NONE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BUSINESS:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AGENT:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AIRLINE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_RIDE_SHARE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BANK:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 724426
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .locals 1

    .prologue
    .line 724427
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 724428
    :goto_0
    return-object v0

    .line 724429
    :cond_1
    const-string v0, "COMMERCE_PAGE_TYPE_NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724430
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_NONE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724431
    :cond_2
    const-string v0, "COMMERCE_PAGE_TYPE_BUSINESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 724432
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BUSINESS:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724433
    :cond_3
    const-string v0, "COMMERCE_PAGE_TYPE_AGENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 724434
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AGENT:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724435
    :cond_4
    const-string v0, "COMMERCE_PAGE_TYPE_AIRLINE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 724436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AIRLINE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724437
    :cond_5
    const-string v0, "COMMERCE_PAGE_TYPE_RIDE_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 724438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_RIDE_SHARE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724439
    :cond_6
    const-string v0, "COMMERCE_PAGE_TYPE_BANK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BANK:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724441
    :cond_7
    const-string v0, "COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 724442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0

    .line 724443
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .locals 1

    .prologue
    .line 724444
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .locals 1

    .prologue
    .line 724445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    return-object v0
.end method
