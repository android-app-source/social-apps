.class public final enum Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum BANK_TRANSFER:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum NATIVE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum PAY_IN_PERSON:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

.field public static final enum UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "PAY_IN_PERSON"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->PAY_IN_PERSON:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "BANK_TRANSFER"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->BANK_TRANSFER:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    const-string v1, "NATIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->NATIVE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737381
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->PAY_IN_PERSON:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->BANK_TRANSFER:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->NATIVE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737382
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;
    .locals 1

    .prologue
    .line 737383
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    .line 737384
    :goto_0
    return-object v0

    .line 737385
    :cond_1
    const-string v0, "UNSPECIFIED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0

    .line 737387
    :cond_2
    const-string v0, "PAY_IN_PERSON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737388
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->PAY_IN_PERSON:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0

    .line 737389
    :cond_3
    const-string v0, "BANK_TRANSFER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->BANK_TRANSFER:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0

    .line 737391
    :cond_4
    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737392
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0

    .line 737393
    :cond_5
    const-string v0, "NATIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737394
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->NATIVE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0

    .line 737395
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;
    .locals 1

    .prologue
    .line 737396
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;
    .locals 1

    .prologue
    .line 737397
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPageProductTransactionPaymentTypeEnum;

    return-object v0
.end method
