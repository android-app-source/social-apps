.class public final enum Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 737487
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737488
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737489
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "CONFIRMED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737490
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737491
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737492
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    const-string v1, "REQUESTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737493
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737494
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .locals 1

    .prologue
    .line 737495
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 737496
    :goto_0
    return-object v0

    .line 737497
    :cond_1
    const-string v0, "CANCELLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 737498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CANCELLED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0

    .line 737499
    :cond_2
    const-string v0, "CONFIRMED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737500
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->CONFIRMED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0

    .line 737501
    :cond_3
    const-string v0, "DECLINED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737502
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0

    .line 737503
    :cond_4
    const-string v0, "PENDING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0

    .line 737505
    :cond_5
    const-string v0, "REQUESTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 737506
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0

    .line 737507
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .locals 1

    .prologue
    .line 737508
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .locals 1

    .prologue
    .line 737509
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    return-object v0
.end method
