.class public final enum Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum APP_AD:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum APP_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum UNKNOWN:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

.field public static final enum VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 725156
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725157
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725158
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "LINK_SHARE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725159
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "APP_AD"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_AD:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725160
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "APP_VIDEO"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725161
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725162
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "PHOTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725163
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "LINK_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725164
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    const-string v1, "MULTI_SHARE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725165
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_AD:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 725166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;
    .locals 1

    .prologue
    .line 725167
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    .line 725168
    :goto_0
    return-object v0

    .line 725169
    :cond_1
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNKNOWN:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725171
    :cond_2
    const-string v0, "LINK_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 725172
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725173
    :cond_3
    const-string v0, "APP_AD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725174
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_AD:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725175
    :cond_4
    const-string v0, "APP_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 725176
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725177
    :cond_5
    const-string v0, "VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 725178
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725179
    :cond_6
    const-string v0, "PHOTO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725180
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725181
    :cond_7
    const-string v0, "LINK_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 725182
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725183
    :cond_8
    const-string v0, "MULTI_SHARE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 725184
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0

    .line 725185
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;
    .locals 1

    .prologue
    .line 725186
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;
    .locals 1

    .prologue
    .line 725187
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    return-object v0
.end method
