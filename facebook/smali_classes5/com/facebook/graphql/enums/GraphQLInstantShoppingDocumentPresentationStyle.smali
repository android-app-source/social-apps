.class public final enum Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum AUDIO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum ENABLE_SWIPE_TO_OPEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum NO_AUDIO_MODE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum PREFETCH_SWIPE_TO_OPEN_WEBVIEW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727746
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727747
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "AUDIO_CONTROL_FLOATING"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727748
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "AUDIO_MUTED"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727749
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "ENABLE_SWIPE_TO_OPEN"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->ENABLE_SWIPE_TO_OPEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727750
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "PREFETCH_SWIPE_TO_OPEN_WEBVIEW"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->PREFETCH_SWIPE_TO_OPEN_WEBVIEW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727751
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    const-string v1, "NO_AUDIO_MODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->NO_AUDIO_MODE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727752
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->ENABLE_SWIPE_TO_OPEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->PREFETCH_SWIPE_TO_OPEN_WEBVIEW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->NO_AUDIO_MODE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;
    .locals 1

    .prologue
    .line 727730
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    .line 727731
    :goto_0
    return-object v0

    .line 727732
    :cond_1
    const-string v0, "AUDIO_CONTROL_FLOATING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727733
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0

    .line 727734
    :cond_2
    const-string v0, "AUDIO_MUTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727735
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0

    .line 727736
    :cond_3
    const-string v0, "ENABLE_SWIPE_TO_OPEN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727737
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->ENABLE_SWIPE_TO_OPEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0

    .line 727738
    :cond_4
    const-string v0, "PREFETCH_SWIPE_TO_OPEN_WEBVIEW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->PREFETCH_SWIPE_TO_OPEN_WEBVIEW:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0

    .line 727740
    :cond_5
    const-string v0, "NO_AUDIO_MODE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727741
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->NO_AUDIO_MODE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0

    .line 727742
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;
    .locals 1

    .prologue
    .line 727744
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;
    .locals 1

    .prologue
    .line 727743
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    return-object v0
.end method
