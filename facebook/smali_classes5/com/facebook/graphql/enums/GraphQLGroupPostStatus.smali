.class public final enum Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public static final enum CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public static final enum CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public static final enum CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727095
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 727096
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const-string v1, "CAN_POST_WITHOUT_APPROVAL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 727097
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const-string v1, "CAN_POST_AFTER_APPROVAL"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 727098
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const-string v1, "CANNOT_POST"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 727099
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727094
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 1

    .prologue
    .line 727085
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 727086
    :goto_0
    return-object v0

    .line 727087
    :cond_1
    const-string v0, "CAN_POST_WITHOUT_APPROVAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727088
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    goto :goto_0

    .line 727089
    :cond_2
    const-string v0, "CAN_POST_AFTER_APPROVAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727090
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    goto :goto_0

    .line 727091
    :cond_3
    const-string v0, "CANNOT_POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727092
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    goto :goto_0

    .line 727093
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 1

    .prologue
    .line 727083
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 1

    .prologue
    .line 727084
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method
