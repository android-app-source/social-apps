.class public final enum Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public static final enum CHOOSE_MULTIPLE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public static final enum CHOOSE_ONE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public static final enum NON_POLL:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public static final enum RANKED:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 738801
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738802
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const-string v1, "NON_POLL"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->NON_POLL:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738803
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const-string v1, "CHOOSE_ONE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_ONE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738804
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const-string v1, "CHOOSE_MULTIPLE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_MULTIPLE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738805
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    const-string v1, "RANKED"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->RANKED:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738806
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->NON_POLL:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_ONE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_MULTIPLE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->RANKED:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 738807
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
    .locals 1

    .prologue
    .line 738808
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 738809
    :goto_0
    return-object v0

    .line 738810
    :cond_1
    const-string v0, "NON_POLL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 738811
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->NON_POLL:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    goto :goto_0

    .line 738812
    :cond_2
    const-string v0, "CHOOSE_ONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 738813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_ONE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    goto :goto_0

    .line 738814
    :cond_3
    const-string v0, "CHOOSE_MULTIPLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->CHOOSE_MULTIPLE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    goto :goto_0

    .line 738816
    :cond_4
    const-string v0, "RANKED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 738817
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->RANKED:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    goto :goto_0

    .line 738818
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
    .locals 1

    .prologue
    .line 738819
    const-class v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;
    .locals 1

    .prologue
    .line 738820
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    return-object v0
.end method
