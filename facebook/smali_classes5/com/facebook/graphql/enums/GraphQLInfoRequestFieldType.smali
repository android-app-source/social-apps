.class public final enum Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum BOOK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum CAMERA_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum CAR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum COLLEGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum CURRENT_LOCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum DENTIST_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum DOCTOR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum EDUCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum EMAIL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum GENERIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum GRAD_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum HIGH_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum HOMETOWN:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum HOURS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum MESSAGE_RESPONSIVENESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum MOVIE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum MUSIC_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum NAME_PRONUNCIATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum OTHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PAGE_ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PAGE_PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PAGE_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PAGE_WEBSITE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PERSON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PHOTOGRAPHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PLACE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PLUMBER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum RECIPE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum RELATIONSHIP:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum RESTAURANT_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum SALON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum THINGS_TO_DO_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum TRAVEL_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum TV_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

.field public static final enum WORK:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 727341
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727342
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727343
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727344
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "BOOK_SUGGESTION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BOOK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727345
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "COLLEGE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727346
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "CURRENT_CITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727347
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "CURRENT_LOCATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_LOCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727348
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "EMAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727349
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "GENERIC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727350
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "GRAD_SCHOOL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GRAD_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727351
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "HIGH_SCHOOL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HIGH_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727352
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "HOMETOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOMETOWN:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727353
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "HOURS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOURS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727354
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "MESSAGE_RESPONSIVENESS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MESSAGE_RESPONSIVENESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727355
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "MOVIE_SUGGESTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MOVIE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727356
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "MUSIC_SUGGESTION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MUSIC_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727357
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "NAME_PRONUNCIATION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->NAME_PRONUNCIATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727358
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PAGE_ADDRESS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727359
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PAGE_CATEGORIES"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727360
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PAGE_PHONE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727361
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PAGE_PRICE_RANGE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727362
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PAGE_WEBSITE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_WEBSITE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727363
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PHONE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727364
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PROFILE_PIC"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727365
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "RELATIONSHIP"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RELATIONSHIP:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727366
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "TV_SUGGESTION"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TV_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727367
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "WORK"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->WORK:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727368
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "EDUCATION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727369
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "CAMERA_SUGGESTION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAMERA_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727370
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "CAR_SUGGESTION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727371
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "DENTIST_SUGGESTION"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DENTIST_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727372
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "DOCTOR_SUGGESTION"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DOCTOR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727373
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "OTHER_SUGGESTION"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->OTHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727374
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PERSON_SUGGESTION"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PERSON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727375
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PHOTOGRAPHER_SUGGESTION"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHOTOGRAPHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727376
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PLACE_SUGGESTION"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLACE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727377
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "PLUMBER_SUGGESTION"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLUMBER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727378
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "RECIPE_SUGGESTION"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RECIPE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727379
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "RESTAURANT_SUGGESTION"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RESTAURANT_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727380
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "SALON_SUGGESTION"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->SALON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727381
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "THINGS_TO_DO_SUGGESTION"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->THINGS_TO_DO_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727382
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    const-string v1, "TRAVEL_SUGGESTION"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TRAVEL_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727383
    const/16 v0, 0x2a

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BOOK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_LOCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GRAD_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HIGH_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOMETOWN:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOURS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MESSAGE_RESPONSIVENESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MOVIE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MUSIC_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->NAME_PRONUNCIATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_WEBSITE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RELATIONSHIP:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TV_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->WORK:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAMERA_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DENTIST_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DOCTOR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->OTHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PERSON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHOTOGRAPHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLACE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLUMBER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RECIPE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RESTAURANT_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->SALON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->THINGS_TO_DO_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TRAVEL_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 727340
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;
    .locals 2

    .prologue
    .line 727384
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 727385
    :goto_0
    return-object v0

    .line 727386
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    mul-int/lit16 v0, v0, 0x3c1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1f

    .line 727387
    packed-switch v0, :pswitch_data_0

    .line 727388
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727389
    :pswitch_1
    const-string v0, "EDUCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727390
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EDUCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727391
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727392
    :pswitch_2
    const-string v0, "HOMETOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 727393
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOMETOWN:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727394
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727395
    :pswitch_3
    const-string v0, "BOOK_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727396
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BOOK_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727397
    :cond_4
    const-string v0, "CAR_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 727398
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727399
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727400
    :pswitch_4
    const-string v0, "COLLEGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 727401
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727402
    :cond_6
    const-string v0, "CURRENT_LOCATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 727403
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_LOCATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727404
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto :goto_0

    .line 727405
    :pswitch_5
    const-string v0, "GRAD_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 727406
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GRAD_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727407
    :cond_8
    const-string v0, "CAMERA_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 727408
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CAMERA_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727409
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727410
    :pswitch_6
    const-string v0, "HIGH_SCHOOL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 727411
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HIGH_SCHOOL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727412
    :cond_a
    const-string v0, "DOCTOR_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 727413
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DOCTOR_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727414
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727415
    :pswitch_7
    const-string v0, "DENTIST_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 727416
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->DENTIST_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727417
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727418
    :pswitch_8
    const-string v0, "PAGE_ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 727419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727420
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727421
    :pswitch_9
    const-string v0, "GENERIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 727422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->GENERIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727423
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727424
    :pswitch_a
    const-string v0, "PAGE_CATEGORIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 727425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_CATEGORIES:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727426
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727427
    :pswitch_b
    const-string v0, "RELATIONSHIP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 727428
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RELATIONSHIP:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727429
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727430
    :pswitch_c
    const-string v0, "MOVIE_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 727431
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MOVIE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727432
    :cond_11
    const-string v0, "MUSIC_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727433
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MUSIC_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727434
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727435
    :pswitch_d
    const-string v0, "MESSAGE_RESPONSIVENESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 727436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->MESSAGE_RESPONSIVENESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727437
    :cond_13
    const-string v0, "PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 727438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727439
    :cond_14
    const-string v0, "WORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 727440
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->WORK:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727441
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727442
    :pswitch_e
    const-string v0, "BIRTHDAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 727443
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727444
    :cond_16
    const-string v0, "OTHER_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 727445
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->OTHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727446
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727447
    :pswitch_f
    const-string v0, "NAME_PRONUNCIATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 727448
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->NAME_PRONUNCIATION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727449
    :cond_18
    const-string v0, "PLACE_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 727450
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLACE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727451
    :cond_19
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727452
    :pswitch_10
    const-string v0, "TV_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 727453
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TV_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727454
    :cond_1a
    const-string v0, "PERSON_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 727455
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PERSON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727456
    :cond_1b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727457
    :pswitch_11
    const-string v0, "PLUMBER_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 727458
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PLUMBER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727459
    :cond_1c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727460
    :pswitch_12
    const-string v0, "ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 727461
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727462
    :cond_1d
    const-string v0, "PAGE_PHONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 727463
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PHONE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727464
    :cond_1e
    const-string v0, "RECIPE_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 727465
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RECIPE_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727466
    :cond_1f
    const-string v0, "SALON_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 727467
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->SALON_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727468
    :cond_20
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727469
    :pswitch_13
    const-string v0, "CURRENT_CITY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 727470
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727471
    :cond_21
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727472
    :pswitch_14
    const-string v0, "PAGE_WEBSITE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 727473
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_WEBSITE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727474
    :cond_22
    const-string v0, "TRAVEL_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 727475
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->TRAVEL_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727476
    :cond_23
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727477
    :pswitch_15
    const-string v0, "PROFILE_PIC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 727478
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PROFILE_PIC:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727479
    :cond_24
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727480
    :pswitch_16
    const-string v0, "PHOTOGRAPHER_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 727481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PHOTOGRAPHER_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727482
    :cond_25
    const-string v0, "RESTAURANT_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 727483
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->RESTAURANT_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727484
    :cond_26
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727485
    :pswitch_17
    const-string v0, "HOURS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 727486
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->HOURS:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727487
    :cond_27
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727488
    :pswitch_18
    const-string v0, "PAGE_PRICE_RANGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 727489
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->PAGE_PRICE_RANGE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727490
    :cond_28
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727491
    :pswitch_19
    const-string v0, "THINGS_TO_DO_SUGGESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 727492
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->THINGS_TO_DO_SUGGESTION:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727493
    :cond_29
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727494
    :pswitch_1a
    const-string v0, "EMAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 727495
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    .line 727496
    :cond_2a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;
    .locals 1

    .prologue
    .line 727339
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;
    .locals 1

    .prologue
    .line 727338
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    return-object v0
.end method
