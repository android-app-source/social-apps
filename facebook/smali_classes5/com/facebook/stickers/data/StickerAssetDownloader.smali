.class public Lcom/facebook/stickers/data/StickerAssetDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:Lcom/facebook/stickers/data/StickerAssetDownloader;


# instance fields
.field private final b:LX/1Er;

.field private final c:LX/3e5;

.field public final d:LX/3ej;

.field public final e:LX/00I;

.field private final f:LX/3ek;

.field private final g:LX/3en;

.field private final h:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620741
    const-class v0, Lcom/facebook/stickers/data/StickerAssetDownloader;

    sput-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Er;LX/3e5;LX/3ej;LX/00I;LX/3ek;LX/3en;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620688
    iput-object p1, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->b:LX/1Er;

    .line 620689
    iput-object p2, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->c:LX/3e5;

    .line 620690
    iput-object p3, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->d:LX/3ej;

    .line 620691
    iput-object p4, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->e:LX/00I;

    .line 620692
    iput-object p5, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->f:LX/3ek;

    .line 620693
    iput-object p6, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->g:LX/3en;

    .line 620694
    iput-object p7, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->h:LX/0So;

    .line 620695
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/data/StickerAssetDownloader;
    .locals 11

    .prologue
    .line 620725
    sget-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->i:Lcom/facebook/stickers/data/StickerAssetDownloader;

    if-nez v0, :cond_1

    .line 620726
    const-class v1, Lcom/facebook/stickers/data/StickerAssetDownloader;

    monitor-enter v1

    .line 620727
    :try_start_0
    sget-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->i:Lcom/facebook/stickers/data/StickerAssetDownloader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620728
    if-eqz v2, :cond_0

    .line 620729
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620730
    new-instance v3, Lcom/facebook/stickers/data/StickerAssetDownloader;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v4

    check-cast v4, LX/1Er;

    invoke-static {v0}, LX/3e5;->a(LX/0QB;)LX/3e5;

    move-result-object v5

    check-cast v5, LX/3e5;

    .line 620731
    new-instance v7, LX/3ej;

    const/16 v6, 0x1808

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {v7, v8, v6}, LX/3ej;-><init>(LX/0Ot;LX/03V;)V

    .line 620732
    move-object v6, v7

    .line 620733
    check-cast v6, LX/3ej;

    const-class v7, LX/00I;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/00I;

    invoke-static {v0}, LX/3ek;->a(LX/0QB;)LX/3ek;

    move-result-object v8

    check-cast v8, LX/3ek;

    invoke-static {v0}, LX/3en;->a(LX/0QB;)LX/3en;

    move-result-object v9

    check-cast v9, LX/3en;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/stickers/data/StickerAssetDownloader;-><init>(LX/1Er;LX/3e5;LX/3ej;LX/00I;LX/3ek;LX/3en;LX/0So;)V

    .line 620734
    move-object v0, v3

    .line 620735
    sput-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->i:Lcom/facebook/stickers/data/StickerAssetDownloader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620736
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620737
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620738
    :cond_1
    sget-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->i:Lcom/facebook/stickers/data/StickerAssetDownloader;

    return-object v0

    .line 620739
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Ljava/io/File;
    .locals 10

    .prologue
    .line 620696
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "sticker_storage_download"

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    .line 620697
    iget-object v0, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v8

    .line 620698
    const/4 v6, 0x0

    .line 620699
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->c:LX/3e5;

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, LX/3e5;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/3e1;Z)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 620700
    :try_start_1
    iget-object v1, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->b:LX/1Er;

    const-string v2, "sticker"

    const-string v3, ".tmp"

    sget-object v4, LX/46h;->REQUIRE_SDCARD:LX/46h;

    invoke-virtual {v1, v2, v3, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 620701
    if-nez v0, :cond_2

    .line 620702
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "destFile cannot be created and is null."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 620703
    :catch_0
    move-exception v3

    move-object v6, v0

    .line 620704
    :goto_1
    sget-object v0, Lcom/facebook/stickers/data/StickerAssetDownloader;->a:Ljava/lang/Class;

    const-string v1, "Failed to download sticker asset for sticker %s, asset type: %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    const/4 v4, 0x1

    aput-object p3, v2, v4

    invoke-static {v0, v3, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620705
    iget-object v0, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->g:LX/3en;

    move-object v1, p2

    move-object v2, p3

    move-wide v4, v8

    .line 620706
    invoke-static {v0, v1, v2, v4, v5}, LX/3en;->b(LX/3en;Ljava/lang/String;LX/3e1;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 620707
    const-string v8, "download_status"

    const-string v9, "failure"

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620708
    const-string v8, "exception_type"

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620709
    iget-object v8, v0, LX/3en;->a:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 620710
    if-eqz v6, :cond_0

    .line 620711
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 620712
    :cond_0
    instance-of v0, v3, Ljava/io/IOException;

    if-eqz v0, :cond_3

    .line 620713
    check-cast v3, Ljava/io/IOException;

    throw v3

    .line 620714
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 620715
    :cond_2
    :try_start_2
    new-instance v2, LX/8ja;

    new-instance v3, LX/8jZ;

    invoke-direct {v3, p0, v1}, LX/8jZ;-><init>(Lcom/facebook/stickers/data/StickerAssetDownloader;Ljava/io/File;)V

    invoke-direct {v2, p0, p4, v3, v7}, LX/8ja;-><init>(Lcom/facebook/stickers/data/StickerAssetDownloader;Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 620716
    iget-object v3, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->f:LX/3ek;

    invoke-virtual {v3, v2}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    .line 620717
    invoke-static {v1, v0}, LX/04M;->a(Ljava/io/File;Ljava/io/File;)V

    .line 620718
    iget-object v1, p0, Lcom/facebook/stickers/data/StickerAssetDownloader;->g:LX/3en;

    .line 620719
    invoke-static {v1, p2, p3, v8, v9}, LX/3en;->b(LX/3en;Ljava/lang/String;LX/3e1;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 620720
    const-string v3, "download_status"

    const-string v4, "success"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 620721
    iget-object v3, v1, LX/3en;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 620722
    return-object v0

    .line 620723
    :cond_3
    invoke-static {v3}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 620724
    :catch_1
    move-exception v3

    goto :goto_1
.end method
