.class public Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;
.super LX/1Eg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final c:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile s:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;


# instance fields
.field public final f:LX/0SG;

.field private final g:LX/0qK;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/0aG;

.field private final k:Ljava/util/concurrent/ExecutorService;

.field private final l:LX/0ka;

.field private final m:LX/2V5;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/2V6;

.field private final p:LX/0ad;

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/2V7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 574759
    const-class v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->d:Ljava/lang/Class;

    .line 574760
    const-class v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    const-string v1, "sticker_background_fetch"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 574761
    sget-object v0, LX/2V4;->p:LX/0Tn;

    const-string v1, "last_partial_download_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a:LX/0Tn;

    .line 574762
    sget-object v0, LX/2V4;->p:LX/0Tn;

    const-string v1, "last_wifi_connection"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b:LX/0Tn;

    .line 574763
    sget-object v0, LX/2V4;->p:LX/0Tn;

    const-string v1, "download_complete_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0ka;LX/2V5;LX/0Or;LX/2V6;LX/0ad;LX/0Ot;LX/2V7;)V
    .locals 6
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/data/CanSaveStickerAssetsToDisk;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/login/annotations/IsLoggedOutRemotely;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0aG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0ka;",
            "LX/2V5;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2V6;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/2V7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574744
    const-string v2, "STICKERS_ASSETS_DOWNLOAD_BACKGROUND_TASK"

    invoke-direct {p0, v2}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 574745
    iput-object p1, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    .line 574746
    iput-object p2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->h:LX/0Or;

    .line 574747
    iput-object p7, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->m:LX/2V5;

    .line 574748
    new-instance v2, LX/0qK;

    const/16 v3, 0x14

    const-wide/32 v4, 0xea60

    invoke-direct {v2, p1, v3, v4, v5}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->g:LX/0qK;

    .line 574749
    iput-object p3, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574750
    iput-object p4, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->j:LX/0aG;

    .line 574751
    iput-object p5, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->k:Ljava/util/concurrent/ExecutorService;

    .line 574752
    iput-object p6, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->l:LX/0ka;

    .line 574753
    iput-object p8, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->n:LX/0Or;

    .line 574754
    iput-object p9, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->o:LX/2V6;

    .line 574755
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->p:LX/0ad;

    .line 574756
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->q:LX/0Ot;

    .line 574757
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->r:LX/2V7;

    .line 574758
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;
    .locals 3

    .prologue
    .line 574734
    sget-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->s:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    if-nez v0, :cond_1

    .line 574735
    const-class v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    monitor-enter v1

    .line 574736
    :try_start_0
    sget-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->s:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574737
    if-eqz v2, :cond_0

    .line 574738
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b(LX/0QB;)Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->s:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574739
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574740
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574741
    :cond_1
    sget-object v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->s:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    return-object v0

    .line 574742
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)Z
    .locals 12

    .prologue
    .line 574720
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->l:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 574721
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574722
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 574723
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 574724
    iget-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->r:LX/2V7;

    .line 574725
    iget-object v6, v4, LX/2V7;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0W3;

    .line 574726
    sget-wide v8, LX/0X5;->gf:J

    const-wide/16 v10, 0x1

    invoke-interface {v6, v8, v9, v10, v11}, LX/0W4;->a(JJ)J

    move-result-wide v6

    .line 574727
    const-wide/32 v8, 0x5265c00

    mul-long/2addr v6, v8

    move-wide v4, v6

    .line 574728
    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/4 v0, 0x1

    .line 574729
    :goto_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 574730
    iget-object v1, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->b:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 574731
    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 574732
    return v0

    .line 574733
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;LX/0Px;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 574709
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 574710
    const/4 v4, 0x0

    .line 574711
    invoke-static {v0}, LX/2V5;->b(Lcom/facebook/stickers/model/Sticker;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 574712
    :cond_0
    :goto_1
    move v0, v4

    .line 574713
    if-eqz v0, :cond_1

    .line 574714
    const/4 v0, 0x1

    .line 574715
    :goto_2
    return v0

    .line 574716
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 574717
    goto :goto_2

    .line 574718
    :cond_3
    invoke-static {v0}, LX/2V5;->a(Lcom/facebook/stickers/model/Sticker;)LX/03R;

    move-result-object v5

    sget-object p0, LX/03R;->NO:LX/03R;

    if-eq v5, p0, :cond_0

    .line 574719
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private static b(LX/0QB;)Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;
    .locals 13

    .prologue
    .line 574707
    new-instance v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v2, 0x157e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v6

    check-cast v6, LX/0ka;

    invoke-static {p0}, LX/2V5;->a(LX/0QB;)LX/2V5;

    move-result-object v7

    check-cast v7, LX/2V5;

    const/16 v8, 0x1523

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/2V6;->b(LX/0QB;)LX/2V6;

    move-result-object v9

    check-cast v9, LX/2V6;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const/16 v11, 0x245

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/2V7;->b(LX/0QB;)LX/2V7;

    move-result-object v12

    check-cast v12, LX/2V7;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;-><init>(LX/0SG;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0ka;LX/2V5;LX/0Or;LX/2V6;LX/0ad;LX/0Ot;LX/2V7;)V

    .line 574708
    return-object v0
.end method

.method public static n(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;)V
    .locals 4

    .prologue
    .line 574705
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->c:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 574706
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 574704
    const-class v0, Lcom/facebook/stickers/background/StickerTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 574703
    const-class v0, Lcom/facebook/stickers/service/StickersQueue;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    const-wide/32 v6, 0x1b7740

    const-wide/16 v4, 0x0

    .line 574663
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574664
    const-wide/16 v0, -0x1

    .line 574665
    :goto_0
    return-wide v0

    .line 574666
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->r:LX/2V7;

    invoke-virtual {v0}, LX/2V7;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574667
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    add-long/2addr v0, v8

    goto :goto_0

    .line 574668
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->c:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 574669
    cmp-long v2, v0, v4

    if-nez v2, :cond_4

    .line 574670
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 574671
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 574672
    iget-object v2, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    add-long/2addr v0, v6

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 574673
    :cond_2
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 574674
    add-long/2addr v0, v6

    goto :goto_0

    .line 574675
    :cond_3
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    goto :goto_0

    .line 574676
    :cond_4
    add-long/2addr v0, v8

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574702
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 574687
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->r:LX/2V7;

    invoke-virtual {v0}, LX/2V7;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574688
    :cond_0
    :goto_0
    return v2

    .line 574689
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->g:LX/0qK;

    invoke-virtual {v0}, LX/0qK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574690
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574691
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574692
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->c:LX/0Tn;

    invoke-interface {v0, v3, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 574693
    cmp-long v0, v4, v10

    if-nez v0, :cond_4

    move v0, v1

    .line 574694
    :goto_1
    iget-object v3, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    add-long/2addr v4, v8

    cmp-long v3, v6, v4

    if-lez v3, :cond_5

    move v3, v1

    .line 574695
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    .line 574696
    :cond_2
    if-eqz v0, :cond_3

    .line 574697
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a:LX/0Tn;

    invoke-interface {v0, v3, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 574698
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long/2addr v4, v8

    cmp-long v0, v6, v4

    if-ltz v0, :cond_0

    .line 574699
    :cond_3
    invoke-direct {p0, v1}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 574700
    goto :goto_1

    :cond_5
    move v3, v2

    .line 574701
    goto :goto_2
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 574677
    iget-object v6, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->c:LX/0Tn;

    invoke-interface {v6, v7}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a:LX/0Tn;

    iget-object v8, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->f:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 574678
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->l:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574679
    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->p:LX/0ad;

    sget v2, LX/8j3;->c:I

    invoke-interface {v0, v2, v1}, LX/0ad;->a(II)I

    move-result v0

    .line 574680
    :goto_0
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-direct {v1, v2, v3}, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;-><init>(LX/3do;LX/0rS;)V

    .line 574681
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 574682
    const-string v3, "fetchStickerPacksAndStickersParams"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 574683
    iget-object v1, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->j:LX/0aG;

    const-string v3, "fetch_sticker_packs_and_stickers"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, 0x29fa5770

    invoke-static {v1, v3, v2, v4, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 574684
    new-instance v2, LX/3dp;

    invoke-direct {v2, p0, v0}, LX/3dp;-><init>(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;I)V

    iget-object v0, p0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->k:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 574685
    new-instance v1, LX/3dq;

    invoke-direct {v1, p0}, LX/3dq;-><init>(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 574686
    goto :goto_0
.end method
