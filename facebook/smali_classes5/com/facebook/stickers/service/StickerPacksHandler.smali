.class public Lcom/facebook/stickers/service/StickerPacksHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/stickers/service/StickerPacksHandler;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:LX/3dt;

.field private final e:LX/3dx;

.field private final f:LX/3e2;

.field public final g:LX/18V;

.field public final h:LX/3eP;

.field private final i:LX/3eQ;

.field private final j:LX/3eR;

.field private final k:LX/3eS;

.field public final l:LX/3eT;

.field private final m:LX/3eV;

.field private final n:LX/3eW;

.field private final o:LX/0SG;

.field private final p:LX/3eU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 619971
    const-class v0, Lcom/facebook/stickers/service/StickerPacksHandler;

    .line 619972
    sput-object v0, Lcom/facebook/stickers/service/StickerPacksHandler;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/stickers/service/StickerPacksHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3dt;LX/3dx;LX/3e2;LX/18V;LX/3eP;LX/3eQ;LX/3eR;LX/3eS;LX/3eT;LX/3eU;LX/3eV;LX/3eW;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619955
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 619956
    iput-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->c:LX/0Ot;

    .line 619957
    iput-object p1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->d:LX/3dt;

    .line 619958
    iput-object p2, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    .line 619959
    iput-object p3, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->f:LX/3e2;

    .line 619960
    iput-object p4, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    .line 619961
    iput-object p5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->h:LX/3eP;

    .line 619962
    iput-object p6, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->i:LX/3eQ;

    .line 619963
    iput-object p7, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->j:LX/3eR;

    .line 619964
    iput-object p8, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->k:LX/3eS;

    .line 619965
    iput-object p9, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->l:LX/3eT;

    .line 619966
    iput-object p10, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->p:LX/3eU;

    .line 619967
    iput-object p11, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->m:LX/3eV;

    .line 619968
    iput-object p13, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->o:LX/0SG;

    .line 619969
    iput-object p12, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->n:LX/3eW;

    .line 619970
    return-void
.end method

.method private a(LX/3do;)J
    .locals 4

    .prologue
    .line 619952
    invoke-static {p1}, LX/8jd;->a(LX/3do;)LX/8je;

    move-result-object v0

    .line 619953
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->m:LX/3eV;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(LX/03R;)LX/8m2;
    .locals 1

    .prologue
    .line 619949
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619950
    sget-object v0, LX/8m2;->DOWNLOADED:LX/8m2;

    .line 619951
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/8m2;->IN_STORE:LX/8m2;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/stickers/service/StickerPacksHandler;Lcom/facebook/stickers/service/FetchStickerPacksParams;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 619857
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v7, v0

    .line 619858
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    move-object v0, v0

    .line 619859
    const-string v3, "StickerPacksHandler handleAddStickerPack"

    const v4, 0xee37f3

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V

    .line 619860
    :try_start_0
    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v0, v3, :cond_0

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq v0, v3, :cond_c

    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, v7}, LX/3dx;->b(LX/3do;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 619861
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 619862
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 619863
    const v1, 0x451b7c0b

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 619864
    :cond_1
    :try_start_1
    new-instance v0, LX/8lz;

    invoke-direct {v0, p1}, LX/8lz;-><init>(Lcom/facebook/stickers/service/FetchStickerPacksParams;)V

    .line 619865
    iget-boolean v3, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    move v3, v3

    .line 619866
    if-eqz v3, :cond_7

    invoke-direct {p0, v7}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/3do;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v3, v4, v8

    if-eqz v3, :cond_7

    move v3, v2

    .line 619867
    :goto_1
    if-eqz v3, :cond_2

    .line 619868
    invoke-direct {p0, v7}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/3do;)J

    move-result-wide v4

    .line 619869
    iput-wide v4, v0, LX/8lz;->d:J

    .line 619870
    :cond_2
    invoke-virtual {v0}, LX/8lz;->a()Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    move-result-object v0

    .line 619871
    iget-object v4, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    iget-object v5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->j:LX/3eR;

    invoke-virtual {v4, v5, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 619872
    iget-object v4, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v4

    .line 619873
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 619874
    invoke-direct {p0, v7}, Lcom/facebook/stickers/service/StickerPacksHandler;->c(LX/3do;)V

    .line 619875
    if-eqz v3, :cond_8

    .line 619876
    invoke-direct {p0, v7, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/3do;LX/0Px;)V

    .line 619877
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, v7}, LX/3dx;->a(LX/3do;)LX/0Px;

    move-result-object v3

    .line 619878
    :goto_2
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    move-object v0, v0

    .line 619879
    sget-object v4, LX/3eb;->DO_NOT_UPDATE:LX/3eb;

    if-eq v0, v4, :cond_4

    .line 619880
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    sget-object v4, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v0, v4}, LX/3dx;->b(LX/3do;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 619881
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    move-object v0, v0

    .line 619882
    sget-object v4, LX/3eb;->REPLACE_FROM_NETWORK:LX/3eb;

    if-ne v0, v4, :cond_9

    .line 619883
    :cond_3
    new-instance v0, LX/3ea;

    sget-object v4, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-direct {v0, v4, v5}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    .line 619884
    new-instance v4, LX/8lz;

    invoke-direct {v4, v0}, LX/8lz;-><init>(Lcom/facebook/stickers/service/FetchStickerPacksParams;)V

    invoke-virtual {v4}, LX/8lz;->a()Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    move-result-object v0

    .line 619885
    iget-object v4, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    iget-object v5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->j:LX/3eR;

    invoke-virtual {v4, v5, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 619886
    sget-object v4, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    .line 619887
    iget-object v5, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v5

    .line 619888
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-direct {p0, v4, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/3do;LX/0Px;)V

    .line 619889
    sget-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-direct {p0, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->c(LX/3do;)V

    .line 619890
    :cond_4
    :goto_3
    sget-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    if-ne v7, v0, :cond_10

    .line 619891
    invoke-static {v3}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v8

    .line 619892
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->n:LX/3eW;

    .line 619893
    iget-object v4, v0, LX/3eW;->a:LX/0Px;

    move-object v0, v4

    .line 619894
    invoke-interface {v8, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 619895
    new-instance v0, LX/3ea;

    sget-object v4, LX/3do;->OWNED_PACKS:LX/3do;

    sget-object v5, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v0, v4, v5}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    .line 619896
    iget-object v4, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 619897
    iput-object v4, v0, LX/3ea;->c:Ljava/lang/String;

    .line 619898
    move-object v0, v0

    .line 619899
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Lcom/facebook/stickers/service/StickerPacksHandler;Lcom/facebook/stickers/service/FetchStickerPacksParams;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 619900
    iget-boolean v4, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v4, v4

    .line 619901
    if-eqz v4, :cond_11

    .line 619902
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 619903
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 619904
    iget-object v5, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v5, v5

    .line 619905
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 619906
    iget-object v4, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v4

    .line 619907
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 619908
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 619909
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/model/StickerPack;

    .line 619910
    iget-object v9, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v9, v9

    .line 619911
    invoke-interface {v5, v9, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 619912
    :cond_5
    move-object v0, v5

    .line 619913
    move-object v6, v0

    .line 619914
    :goto_5
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 619915
    invoke-virtual {v9, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 619916
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->n:LX/3eW;

    .line 619917
    iget-object v4, v0, LX/3eW;->a:LX/0Px;

    move-object v10, v4

    .line 619918
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v4, v1

    move v5, v1

    :goto_6
    if-ge v4, v11, :cond_f

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 619919
    invoke-interface {v8, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_15

    .line 619920
    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_14

    .line 619921
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v9, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619922
    const/4 v12, 0x1

    .line 619923
    :goto_7
    move v0, v12

    .line 619924
    if-nez v0, :cond_6

    if-eqz v5, :cond_e

    :cond_6
    move v0, v2

    .line 619925
    :goto_8
    add-int/lit8 v4, v4, 0x1

    move v5, v0

    goto :goto_6

    :cond_7
    move v3, v1

    .line 619926
    goto/16 :goto_1

    .line 619927
    :cond_8
    invoke-direct {p0, v7, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/3do;LX/0Px;)V

    move-object v3, v0

    goto/16 :goto_2

    .line 619928
    :cond_9
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 619929
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v4, v1

    :goto_9
    if-ge v4, v6, :cond_b

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 619930
    iget-boolean v8, v0, Lcom/facebook/stickers/model/StickerPack;->o:Z

    move v8, v8

    .line 619931
    if-eqz v8, :cond_a

    .line 619932
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619933
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_9

    .line 619934
    :cond_b
    sget-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/3do;LX/0Px;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 619935
    :catchall_0
    move-exception v0

    const v1, 0x38749673

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 619936
    :cond_c
    :try_start_2
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, v7}, LX/3dx;->b(LX/3do;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 619937
    sget-object v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->a:Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 619938
    const v1, -0x43568e95

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 619939
    :cond_d
    :try_start_3
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, v7}, LX/3dx;->a(LX/3do;)LX/0Px;

    move-result-object v3

    goto/16 :goto_3

    :cond_e
    move v0, v1

    .line 619940
    goto :goto_8

    .line 619941
    :cond_f
    if-eqz v5, :cond_12

    .line 619942
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 619943
    invoke-direct {p0, v7, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/3do;LX/0Px;)V

    :goto_a
    move-object v3, v0

    .line 619944
    :cond_10
    :goto_b
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-direct {v0, v3}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 619945
    const v1, -0x16562413

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 619946
    :cond_11
    :try_start_4
    sget-object v0, Lcom/facebook/stickers/service/StickerPacksHandler;->a:Ljava/lang/Class;

    const-string v1, "Unable to include default packs in downloaded list!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_b

    :cond_12
    move-object v0, v3

    goto :goto_a

    :cond_13
    move-object v6, v4

    goto/16 :goto_5

    .line 619947
    :cond_14
    sget-object v12, Lcom/facebook/stickers/service/StickerPacksHandler;->a:Ljava/lang/Class;

    const-string p1, "Couldn\'t add Meep sticker pack!"

    invoke-static {v12, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 619948
    :cond_15
    const/4 v12, 0x0

    goto/16 :goto_7
.end method

.method private a(Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;)Lcom/facebook/stickers/service/FetchStickerPacksResult;
    .locals 2

    .prologue
    .line 619854
    const-string v0, "StickerPacksHandler fetchStickerPacksByIdFromServer"

    const v1, -0x7a4f4b1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 619855
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    iget-object v1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->k:LX/3eS;

    invoke-virtual {v0, v1, p1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619856
    const v1, -0x6405e7e8

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x468389d5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619849
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 619850
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 619851
    iget-object p0, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v0, p0

    .line 619852
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 619853
    :cond_0
    return-object v1
.end method

.method private a(LX/3do;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 619845
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, p1}, LX/3dx;->b(LX/3do;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619846
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, p1, p2}, LX/3dx;->b(LX/3do;Ljava/util/List;)V

    .line 619847
    :goto_0
    return-void

    .line 619848
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/stickers/service/StickerPacksHandler;->b(LX/3do;LX/0Px;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/stickers/service/StickerPacksHandler;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 619973
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->f:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 619974
    const v0, -0x1eb6846c

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619975
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    sget-object v2, LX/3do;->OWNED_PACKS:LX/3do;

    invoke-virtual {v0, v2}, LX/3dx;->b(LX/3do;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619976
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    sget-object v2, LX/3do;->OWNED_PACKS:LX/3do;

    invoke-virtual {v0, v2, p1}, LX/3dx;->b(LX/3do;Ljava/util/List;)V

    .line 619977
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v0, v2, p1}, LX/3dx;->b(LX/3do;Ljava/util/List;)V

    .line 619978
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619979
    const v0, -0x63071454

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619980
    return-void

    .line 619981
    :catchall_0
    move-exception v0

    const v2, 0x3a169fca

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private b(LX/3do;)J
    .locals 10

    .prologue
    .line 619838
    const-wide/16 v2, 0x0

    .line 619839
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, p1}, LX/3dx;->a(LX/3do;)LX/0Px;

    move-result-object v5

    .line 619840
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 619841
    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerPack;->j()J

    move-result-wide v8

    cmp-long v1, v8, v2

    if-lez v1, :cond_1

    .line 619842
    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerPack;->j()J

    move-result-wide v0

    .line 619843
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 619844
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/stickers/service/StickerPacksHandler;
    .locals 14

    .prologue
    .line 619834
    new-instance v0, Lcom/facebook/stickers/service/StickerPacksHandler;

    invoke-static {p0}, LX/3dt;->a(LX/0QB;)LX/3dt;

    move-result-object v1

    check-cast v1, LX/3dt;

    invoke-static {p0}, LX/3dx;->a(LX/0QB;)LX/3dx;

    move-result-object v2

    check-cast v2, LX/3dx;

    invoke-static {p0}, LX/3e2;->a(LX/0QB;)LX/3e2;

    move-result-object v3

    check-cast v3, LX/3e2;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {p0}, LX/3eP;->a(LX/0QB;)LX/3eP;

    move-result-object v5

    check-cast v5, LX/3eP;

    invoke-static {p0}, LX/3eQ;->a(LX/0QB;)LX/3eQ;

    move-result-object v6

    check-cast v6, LX/3eQ;

    invoke-static {p0}, LX/3eR;->a(LX/0QB;)LX/3eR;

    move-result-object v7

    check-cast v7, LX/3eR;

    invoke-static {p0}, LX/3eS;->a(LX/0QB;)LX/3eS;

    move-result-object v8

    check-cast v8, LX/3eS;

    invoke-static {p0}, LX/3eT;->a(LX/0QB;)LX/3eT;

    move-result-object v9

    check-cast v9, LX/3eT;

    invoke-static {p0}, LX/3eU;->a(LX/0QB;)LX/3eU;

    move-result-object v10

    check-cast v10, LX/3eU;

    invoke-static {p0}, LX/3eV;->a(LX/0QB;)LX/3eV;

    move-result-object v11

    check-cast v11, LX/3eV;

    invoke-static {p0}, LX/3eW;->a(LX/0QB;)LX/3eW;

    move-result-object v12

    check-cast v12, LX/3eW;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/stickers/service/StickerPacksHandler;-><init>(LX/3dt;LX/3dx;LX/3e2;LX/18V;LX/3eP;LX/3eQ;LX/3eR;LX/3eS;LX/3eT;LX/3eU;LX/3eV;LX/3eW;LX/0SG;)V

    .line 619835
    const/16 v1, 0x2ca

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 619836
    iput-object v1, v0, Lcom/facebook/stickers/service/StickerPacksHandler;->c:LX/0Ot;

    .line 619837
    return-object v0
.end method

.method private b(LX/3do;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 619832
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v0, p1, p2}, LX/3dx;->a(LX/3do;Ljava/util/List;)V

    .line 619833
    return-void
.end method

.method private c(LX/3do;)V
    .locals 4

    .prologue
    .line 619829
    invoke-static {p1}, LX/8jd;->a(LX/3do;)LX/8je;

    move-result-object v0

    .line 619830
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->m:LX/3eV;

    iget-object v2, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->o:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Iu;->b(LX/0To;J)V

    .line 619831
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 619822
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 619823
    const-string v1, "fetchStickerPackIdsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;

    .line 619824
    const-string v1, "StickerPacksHandler handleFetchStickerPackIds"

    const v2, -0x27359664

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 619825
    :try_start_0
    iget-object v1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    iget-object v2, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->i:LX/3eQ;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;

    .line 619826
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;->a:LX/0Px;

    move-object v0, v1

    .line 619827
    new-instance v1, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;

    invoke-direct {v1, v0}, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;-><init>(LX/0Px;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 619828
    const v1, 0x6f4791ba

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x1724376e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 619769
    if-nez p2, :cond_0

    .line 619770
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p2, v0

    .line 619771
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->d:LX/3dt;

    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v0, v1, p1}, LX/3dt;->a(LX/3do;Ljava/util/List;)V

    .line 619772
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->f:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 619773
    const v0, -0x2819f184

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619774
    :try_start_0
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v0, v2, p1}, LX/3dx;->a(LX/3do;Ljava/util/List;)V

    .line 619775
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    .line 619776
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 619777
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619778
    const v0, -0x6fdc53f1

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 619779
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->p:LX/3eU;

    invoke-virtual {v0}, LX/3eU;->a()V

    .line 619780
    iget-object v0, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->g:LX/18V;

    iget-object v1, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->h:LX/3eP;

    invoke-virtual {v0, v1, p1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619781
    return-void

    .line 619782
    :catchall_0
    move-exception v0

    const v2, 0x6ee49501

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 619783
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 619784
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/model/StickerPack;

    .line 619785
    iget-object p2, v2, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v2, p2

    .line 619786
    invoke-virtual {v3, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_1

    .line 619787
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 619788
    invoke-static {v0, v2}, LX/3dx;->d(LX/3dx;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 619792
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 619793
    const-string v1, "fetchStickerPacksByIdParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;

    .line 619794
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;->a:LX/0Px;

    move-object v0, v1

    .line 619795
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 619796
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 619797
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    .line 619798
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 619799
    iget-object v5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->d:LX/3dt;

    invoke-virtual {v5, v0}, LX/3dt;->c(Ljava/lang/String;)LX/03R;

    move-result-object v5

    .line 619800
    iget-object v6, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    .line 619801
    sget-object v7, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v6, v7}, LX/3dx;->b(LX/3do;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 619802
    sget-object v7, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-static {v6, v0, v7}, LX/3dx;->a(LX/3dx;Ljava/lang/String;LX/3do;)Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, LX/03R;->YES:LX/03R;

    .line 619803
    :goto_1
    move-object v6, v7

    .line 619804
    iget-object v7, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->d:LX/3dt;

    invoke-virtual {v7, v0}, LX/3dt;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5}, LX/03R;->isSet()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 619805
    iget-object v6, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->d:LX/3dt;

    invoke-virtual {v6, v0}, LX/3dt;->b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619806
    invoke-static {v5}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/03R;)LX/8m2;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 619807
    :cond_0
    iget-object v5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v5, v0}, LX/3dx;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v6}, LX/03R;->isSet()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 619808
    const-string v5, "StickerPacksHandler fetchStickerPacksById fetch from db"

    const v7, -0x72757cde

    invoke-static {v5, v7}, LX/02m;->a(Ljava/lang/String;I)V

    .line 619809
    :try_start_0
    iget-object v5, p0, Lcom/facebook/stickers/service/StickerPacksHandler;->e:LX/3dx;

    invoke-virtual {v5, v0}, LX/3dx;->b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619810
    invoke-static {v6}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(LX/03R;)LX/8m2;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619811
    const v0, 0x725bfdc3

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x39173a70

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 619812
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 619813
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 619814
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;)Lcom/facebook/stickers/service/FetchStickerPacksResult;

    move-result-object v1

    .line 619815
    iget-object v0, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v0

    .line 619816
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 619817
    iget-object v0, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    move-object v0, v0

    .line 619818
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v3, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 619819
    :cond_3
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;Ljava/util/Map;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 619820
    :cond_4
    sget-object v7, LX/03R;->NO:LX/03R;

    goto/16 :goto_1

    .line 619821
    :cond_5
    sget-object v7, LX/03R;->UNSET:LX/03R;

    goto/16 :goto_1
.end method

.method public final c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 619789
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 619790
    const-string v1, "fetchStickerPacksParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    .line 619791
    invoke-static {p0, v0}, Lcom/facebook/stickers/service/StickerPacksHandler;->a(Lcom/facebook/stickers/service/StickerPacksHandler;Lcom/facebook/stickers/service/FetchStickerPacksParams;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
