.class public Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3do;

.field public final b:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 617359
    new-instance v0, LX/3dn;

    invoke-direct {v0}, LX/3dn;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3do;LX/0rS;)V
    .locals 2

    .prologue
    .line 617360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617361
    iput-object p1, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    .line 617362
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "only supports local data fetch."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 617363
    iput-object p2, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    .line 617364
    return-void

    .line 617365
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 617366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617367
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3do;->valueOf(Ljava/lang/String;)LX/3do;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    .line 617368
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    .line 617369
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 617370
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 617371
    instance-of v1, p1, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;

    if-nez v1, :cond_1

    .line 617372
    :cond_0
    :goto_0
    return v0

    .line 617373
    :cond_1
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;

    .line 617374
    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 617375
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->hashCode()I

    move-result v0

    .line 617376
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    invoke-virtual {v1}, LX/0rS;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 617377
    return v0

    :cond_1
    move v0, v1

    .line 617378
    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 617379
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 617380
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 617381
    return-void
.end method
