.class public Lcom/facebook/stickers/service/FetchStickerPacksResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPacksResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/stickers/service/FetchStickerPacksResult;


# instance fields
.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/8m2;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 620476
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->a:Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 620477
    new-instance v0, LX/3ee;

    invoke-direct {v0}, LX/3ee;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 620458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620459
    const-class v0, Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 620460
    if-eqz v0, :cond_0

    .line 620461
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    .line 620462
    :goto_0
    const-class v0, LX/8m2;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    .line 620463
    if-eqz v0, :cond_1

    .line 620464
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 620465
    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 620466
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    .line 620467
    :goto_1
    return-void

    .line 620468
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    goto :goto_0

    .line 620469
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 620448
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;Ljava/util/Map;)V

    .line 620449
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8m2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 620470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620471
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    .line 620472
    if-eqz p2, :cond_1

    invoke-static {p2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    .line 620473
    return-void

    .line 620474
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 620475
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/model/StickerPack;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/stickers/model/StickerPack;",
            ")",
            "LX/0am",
            "<",
            "LX/8m2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 620454
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v0

    .line 620455
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620456
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 620457
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 620453
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 620450
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 620451
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 620452
    return-void
.end method
