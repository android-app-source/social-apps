.class public Lcom/facebook/stickers/service/FetchStickersParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickersParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3eg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620481
    new-instance v0, LX/3ef;

    invoke-direct {v0}, LX/3ef;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickersParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 620482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620483
    const-class v0, Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    .line 620484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3eg;->valueOf(Ljava/lang/String;)LX/3eg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickersParams;->b:LX/3eg;

    .line 620485
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;LX/3eg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3eg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 620486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620487
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    .line 620488
    iput-object p2, p0, Lcom/facebook/stickers/service/FetchStickersParams;->b:LX/3eg;

    .line 620489
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 620490
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 620491
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 620492
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickersParams;->b:LX/3eg;

    invoke-virtual {v0}, LX/3eg;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 620493
    return-void
.end method
