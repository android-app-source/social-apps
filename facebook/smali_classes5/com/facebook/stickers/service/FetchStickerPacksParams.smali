.class public Lcom/facebook/stickers/service/FetchStickerPacksParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPacksParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3do;

.field public final b:LX/0rS;

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/StickerInterfaces;
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:LX/3eb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620344
    new-instance v0, LX/3ec;

    invoke-direct {v0}, LX/3ec;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/3do;LX/0rS;Ljava/lang/String;ZZZZLX/3eb;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/StickerInterfaces;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 620345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620346
    iput-object p1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    .line 620347
    iput-object p2, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    .line 620348
    iput-object p3, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    .line 620349
    iput-boolean p4, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    .line 620350
    iput-boolean p5, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    .line 620351
    iput-boolean p6, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    .line 620352
    iput-boolean p7, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    .line 620353
    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    if-eq v1, v0, :cond_0

    sget-object v1, LX/3eb;->APPEND_TO_DB:LX/3eb;

    if-eq p8, v1, :cond_1

    :cond_0
    :goto_0
    const-string v1, "appending to db operation should only be used when performing a delta fetch"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 620354
    iput-object p8, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    .line 620355
    return-void

    .line 620356
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(LX/3do;LX/0rS;Ljava/lang/String;ZZZZLX/3eb;B)V
    .locals 0

    .prologue
    .line 620357
    invoke-direct/range {p0 .. p8}, Lcom/facebook/stickers/service/FetchStickerPacksParams;-><init>(LX/3do;LX/0rS;Ljava/lang/String;ZZZZLX/3eb;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 620358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620359
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3do;->valueOf(Ljava/lang/String;)LX/3do;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    .line 620360
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    .line 620361
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    .line 620362
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    .line 620363
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    .line 620364
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    .line 620365
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    .line 620366
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3eb;->valueOf(Ljava/lang/String;)LX/3eb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    .line 620367
    return-void

    :cond_0
    move v0, v2

    .line 620368
    goto :goto_0

    :cond_1
    move v0, v2

    .line 620369
    goto :goto_1

    :cond_2
    move v0, v2

    .line 620370
    goto :goto_2

    :cond_3
    move v1, v2

    .line 620371
    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 620372
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 620373
    instance-of v1, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    if-nez v1, :cond_1

    .line 620374
    :cond_0
    :goto_0
    return v0

    .line 620375
    :cond_1
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    .line 620376
    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    iget-boolean v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    iget-boolean v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    iget-boolean v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    iget-boolean v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 620377
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->hashCode()I

    move-result v0

    .line 620378
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 620379
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 620380
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 620381
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    add-int/2addr v0, v3

    .line 620382
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    add-int/2addr v0, v3

    .line 620383
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    if-eqz v3, :cond_7

    :goto_6
    add-int/2addr v0, v2

    .line 620384
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    invoke-virtual {v1}, LX/3eb;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 620385
    return v0

    :cond_1
    move v0, v1

    .line 620386
    goto :goto_0

    :cond_2
    move v0, v1

    .line 620387
    goto :goto_1

    :cond_3
    move v0, v1

    .line 620388
    goto :goto_2

    :cond_4
    move v0, v1

    .line 620389
    goto :goto_3

    :cond_5
    move v0, v1

    .line 620390
    goto :goto_4

    :cond_6
    move v0, v1

    .line 620391
    goto :goto_5

    :cond_7
    move v2, v1

    .line 620392
    goto :goto_6
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 620393
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    invoke-virtual {v0}, LX/3do;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 620394
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 620395
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 620396
    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 620397
    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 620398
    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 620399
    iget-boolean v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 620400
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->h:LX/3eb;

    invoke-virtual {v0}, LX/3eb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 620401
    return-void

    :cond_0
    move v0, v2

    .line 620402
    goto :goto_0

    :cond_1
    move v0, v2

    .line 620403
    goto :goto_1

    :cond_2
    move v0, v2

    .line 620404
    goto :goto_2

    :cond_3
    move v1, v2

    .line 620405
    goto :goto_3
.end method
