.class public Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 621005
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;

    invoke-direct {v0, v1, v1}, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;-><init>(Ljava/util/List;LX/0P1;)V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->a:Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;

    .line 621006
    new-instance v0, LX/3eq;

    invoke-direct {v0}, LX/3eq;-><init>()V

    sput-object v0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 621007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621008
    const-class v0, Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 621009
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->b:LX/0Px;

    .line 621010
    const-class v0, Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    .line 621011
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 621012
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 621013
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 621014
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->c:LX/0P1;

    .line 621015
    return-void
.end method

.method public constructor <init>(Ljava/util/List;LX/0P1;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 621016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621017
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->b:LX/0Px;

    .line 621018
    if-eqz p2, :cond_1

    invoke-static {p2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->c:LX/0P1;

    .line 621019
    return-void

    .line 621020
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 621021
    goto :goto_0

    .line 621022
    :cond_1
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 621023
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 621024
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 621025
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 621026
    iget-object v0, p0, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->c:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 621027
    return-void
.end method
