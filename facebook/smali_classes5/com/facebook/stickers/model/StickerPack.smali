.class public Lcom/facebook/stickers/model/StickerPack;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Landroid/net/Uri;

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/net/Uri;

.field public final h:Landroid/net/Uri;

.field public final i:I

.field private final j:Ljava/lang/Long;

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Lcom/facebook/stickers/model/StickerCapabilities;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 805358
    new-instance v0, LX/4m5;

    invoke-direct {v0}, LX/4m5;-><init>()V

    sput-object v0, Lcom/facebook/stickers/model/StickerPack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4m6;)V
    .locals 4

    .prologue
    .line 805320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805321
    iget-object v0, p1, LX/4m6;->a:Ljava/lang/String;

    move-object v0, v0

    .line 805322
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    .line 805323
    iget-object v0, p1, LX/4m6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 805324
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    .line 805325
    iget-object v0, p1, LX/4m6;->c:Ljava/lang/String;

    move-object v0, v0

    .line 805326
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    .line 805327
    iget-object v0, p1, LX/4m6;->d:Ljava/lang/String;

    move-object v0, v0

    .line 805328
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    .line 805329
    iget-object v0, p1, LX/4m6;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 805330
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    .line 805331
    iget-object v0, p1, LX/4m6;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 805332
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    .line 805333
    iget-object v0, p1, LX/4m6;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 805334
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    .line 805335
    iget-object v0, p1, LX/4m6;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 805336
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->h:Landroid/net/Uri;

    .line 805337
    iget v0, p1, LX/4m6;->i:I

    move v0, v0

    .line 805338
    iput v0, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    .line 805339
    iget-wide v2, p1, LX/4m6;->j:J

    move-wide v0, v2

    .line 805340
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->j:Ljava/lang/Long;

    .line 805341
    iget-boolean v0, p1, LX/4m6;->k:Z

    move v0, v0

    .line 805342
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->k:Z

    .line 805343
    iget-boolean v0, p1, LX/4m6;->l:Z

    move v0, v0

    .line 805344
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->l:Z

    .line 805345
    iget-boolean v0, p1, LX/4m6;->m:Z

    move v0, v0

    .line 805346
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->m:Z

    .line 805347
    iget-boolean v0, p1, LX/4m6;->n:Z

    move v0, v0

    .line 805348
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->n:Z

    .line 805349
    iget-boolean v0, p1, LX/4m6;->o:Z

    move v0, v0

    .line 805350
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->o:Z

    .line 805351
    iget-object v0, p1, LX/4m6;->p:Ljava/util/List;

    move-object v0, v0

    .line 805352
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    .line 805353
    iget-object v0, p1, LX/4m6;->q:Ljava/util/List;

    move-object v0, v0

    .line 805354
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    .line 805355
    iget-object v0, p1, LX/4m6;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v0, v0

    .line 805356
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805357
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 805293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805294
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    .line 805295
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    .line 805296
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    .line 805297
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    .line 805298
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    .line 805299
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 805300
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    .line 805301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    .line 805302
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->h:Landroid/net/Uri;

    .line 805303
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    .line 805304
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->j:Ljava/lang/Long;

    .line 805305
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->k:Z

    .line 805306
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->l:Z

    .line 805307
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->m:Z

    .line 805308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->n:Z

    .line 805309
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/stickers/model/StickerPack;->o:Z

    .line 805310
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    .line 805311
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    .line 805312
    const-class v0, Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerCapabilities;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805313
    return-void

    .line 805314
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 805315
    goto :goto_1

    :cond_2
    move v0, v2

    .line 805316
    goto :goto_2

    :cond_3
    move v0, v2

    .line 805317
    goto :goto_3

    :cond_4
    move v0, v2

    .line 805318
    goto :goto_4

    :cond_5
    move v1, v2

    .line 805319
    goto :goto_5
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805292
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805291
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805290
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 805289
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 805288
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 805287
    iget v0, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 805286
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 805261
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805262
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805263
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805264
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805265
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805266
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805267
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805268
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805269
    iget v0, p0, Lcom/facebook/stickers/model/StickerPack;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805270
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 805271
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805272
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805273
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805274
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->n:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805275
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerPack;->o:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 805276
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 805277
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 805278
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 805279
    return-void

    .line 805280
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 805281
    goto :goto_1

    :cond_2
    move v0, v2

    .line 805282
    goto :goto_2

    :cond_3
    move v0, v2

    .line 805283
    goto :goto_3

    :cond_4
    move v0, v2

    .line 805284
    goto :goto_4

    :cond_5
    move v1, v2

    .line 805285
    goto :goto_5
.end method
