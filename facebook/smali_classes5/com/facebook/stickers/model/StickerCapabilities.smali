.class public Lcom/facebook/stickers/model/StickerCapabilities;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/model/StickerCapabilities;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/03R;

.field public final b:LX/03R;

.field public final c:LX/03R;

.field public final d:LX/03R;

.field public final e:LX/03R;

.field public final f:LX/03R;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 805214
    new-instance v0, LX/4m1;

    invoke-direct {v0}, LX/4m1;-><init>()V

    sput-object v0, Lcom/facebook/stickers/model/StickerCapabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/03R;LX/03R;LX/03R;LX/03R;LX/03R;LX/03R;)V
    .locals 1

    .prologue
    .line 805223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805224
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    .line 805225
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    .line 805226
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    .line 805227
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    .line 805228
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    .line 805229
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    .line 805230
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 805215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805216
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    .line 805217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    .line 805218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    .line 805219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    .line 805220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    .line 805221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    .line 805222
    return-void
.end method

.method public static newBuilder()LX/4m3;
    .locals 1

    .prologue
    .line 805186
    new-instance v0, LX/4m3;

    invoke-direct {v0}, LX/4m3;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(LX/4m4;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 805205
    if-nez p1, :cond_0

    .line 805206
    :goto_0
    return v0

    .line 805207
    :cond_0
    sget-object v1, LX/4m2;->a:[I

    invoke-virtual {p1}, LX/4m4;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 805208
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 805209
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 805210
    :pswitch_2
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 805211
    :pswitch_3
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 805212
    :pswitch_4
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    .line 805213
    :pswitch_5
    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 805231
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 805204
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 805198
    if-ne p0, p1, :cond_1

    .line 805199
    :cond_0
    :goto_0
    return v0

    .line 805200
    :cond_1
    instance-of v2, p1, Lcom/facebook/stickers/model/StickerCapabilities;

    if-nez v2, :cond_2

    move v0, v1

    .line 805201
    goto :goto_0

    .line 805202
    :cond_2
    check-cast p1, Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805203
    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    iget-object v3, p1, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 805195
    iget v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->g:I

    if-nez v0, :cond_0

    .line 805196
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->g:I

    .line 805197
    :cond_0
    iget v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->g:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 805194
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StickerCapabilities{isCommentsCapable="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isComposerCapable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isMessengerCapable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSmsCapable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isPostsCapable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isMontageCapable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 805187
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805188
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805189
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805190
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805191
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805192
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805193
    return-void
.end method
