.class public Lcom/facebook/stickers/model/StickerTag;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:I

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 805400
    new-instance v0, LX/4m7;

    invoke-direct {v0}, LX/4m7;-><init>()V

    sput-object v0, Lcom/facebook/stickers/model/StickerTag;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 805401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    .line 805403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    .line 805404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->c:Ljava/lang/String;

    .line 805405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/stickers/model/StickerTag;->d:Z

    .line 805406
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/stickers/model/StickerTag;->e:I

    .line 805407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    .line 805408
    return-void

    .line 805409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V
    .locals 0

    .prologue
    .line 805410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805411
    iput-object p1, p0, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    .line 805412
    iput-object p2, p0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    .line 805413
    iput-object p3, p0, Lcom/facebook/stickers/model/StickerTag;->c:Ljava/lang/String;

    .line 805414
    iput-boolean p4, p0, Lcom/facebook/stickers/model/StickerTag;->d:Z

    .line 805415
    iput p5, p0, Lcom/facebook/stickers/model/StickerTag;->e:I

    .line 805416
    iput-object p6, p0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    .line 805417
    return-void
.end method

.method public static newBuilder()LX/4m8;
    .locals 1

    .prologue
    .line 805418
    new-instance v0, LX/4m8;

    invoke-direct {v0}, LX/4m8;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 805419
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 805420
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805421
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805422
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805423
    iget-boolean v0, p0, Lcom/facebook/stickers/model/StickerTag;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805424
    iget v0, p0, Lcom/facebook/stickers/model/StickerTag;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 805425
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 805426
    return-void

    .line 805427
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
