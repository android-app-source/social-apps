.class public final Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic d:LX/2GJ;


# direct methods
.method public constructor <init>(LX/2GJ;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 574775
    iput-object p1, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;->d:LX/2GJ;

    iput-object p4, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0, p2, p3}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 574776
    iget-object v0, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 574777
    iget-object v0, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;->d:LX/2GJ;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    const/4 v2, 0x1

    .line 574778
    const v8, 0x270001

    .line 574779
    invoke-static {v0}, LX/2GJ;->g(LX/2GJ;)V

    .line 574780
    invoke-static {v0}, LX/2GJ;->j(LX/2GJ;)LX/2VA;

    move-result-object v3

    .line 574781
    iget-object v4, v0, LX/2GJ;->j:LX/0Zm;

    invoke-virtual {v4}, LX/0Zm;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 574782
    invoke-static {v3}, LX/2GJ;->g(LX/2VA;)I

    move-result v5

    .line 574783
    iget-object v6, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v7, "exception"

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-interface {v6, v8, v5, v7, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 574784
    iget-object v4, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x3

    invoke-interface {v4, v8, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 574785
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/2VA;->b(Z)V

    .line 574786
    const-string v4, "start"

    invoke-static {v0, v4, v3, v1}, LX/2GJ;->a(LX/2GJ;Ljava/lang/String;LX/2VA;Ljava/lang/Throwable;)V

    .line 574787
    iget-object v4, v0, LX/2GJ;->c:LX/0SG;

    iget-wide v5, v0, LX/2GJ;->p:J

    iget-wide v7, v0, LX/2GJ;->q:J

    invoke-virtual/range {v3 .. v8}, LX/2VA;->a(LX/0SG;JJ)V

    .line 574788
    if-eqz v2, :cond_1

    .line 574789
    invoke-static {v0}, LX/2GJ;->h(LX/2GJ;)V

    .line 574790
    :cond_1
    return-void

    .line 574791
    :cond_2
    const-string v4, "null"

    goto :goto_0
.end method
