.class public abstract Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571329
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Ljava/util/concurrent/ExecutorService;)V
    .locals 3

    .prologue
    .line 571330
    iget-object v0, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571331
    const v0, -0x8aef134

    invoke-static {p1, p0, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 571332
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 571333
    iget-object v0, p0, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 571334
    invoke-virtual {p0}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a()V

    .line 571335
    return-void
.end method
