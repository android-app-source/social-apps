.class public Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JaI;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599203
    new-instance v0, LX/3ZF;

    invoke-direct {v0}, LX/3ZF;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599204
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599205
    iput-object p1, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599206
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;
    .locals 4

    .prologue
    .line 599207
    const-class v1, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    monitor-enter v1

    .line 599208
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599209
    sput-object v2, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599212
    new-instance p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 599213
    move-object v0, p0

    .line 599214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 599218
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 599219
    check-cast p2, LX/JaI;

    .line 599220
    iget-object v0, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedItemPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    .line 599221
    iget-object v2, p2, LX/JaI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 599222
    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5c940046

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599224
    check-cast p1, LX/JaI;

    .line 599225
    const v1, 0x7f0d1e04

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/FbImageView;

    iget v2, p1, LX/JaI;->a:I

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 599226
    const/16 v1, 0x1f

    const v2, -0x182add4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599227
    check-cast p1, LX/JaI;

    .line 599228
    iget v0, p1, LX/JaI;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 599229
    const v0, 0x7f0d1e04

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 599230
    return-void
.end method
