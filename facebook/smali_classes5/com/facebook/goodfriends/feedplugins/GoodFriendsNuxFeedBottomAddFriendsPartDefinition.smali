.class public Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNuxGoodFriendsFeedItemUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/22r;

.field public final c:LX/1b1;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599233
    new-instance v0, LX/3ZG;

    invoke-direct {v0}, LX/3ZG;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/22r;LX/1b1;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599244
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599245
    iput-object p2, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->c:LX/1b1;

    .line 599246
    iput-object p1, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->b:LX/22r;

    .line 599247
    iput-object p3, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599248
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;
    .locals 6

    .prologue
    .line 599249
    const-class v1, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    monitor-enter v1

    .line 599250
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599251
    sput-object v2, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599252
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599253
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599254
    new-instance p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;

    invoke-static {v0}, LX/22r;->b(LX/0QB;)LX/22r;

    move-result-object v3

    check-cast v3, LX/22r;

    invoke-static {v0}, LX/1b1;->b(LX/0QB;)LX/1b1;

    move-result-object v4

    check-cast v4, LX/1b1;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;-><init>(LX/22r;LX/1b1;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 599255
    move-object v0, p0

    .line 599256
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599257
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599258
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 599240
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 599241
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599242
    iget-object v0, p0, Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599243
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2d7cd09d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599237
    const v1, 0x7f0d1e03

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 599238
    new-instance v2, LX/JaH;

    invoke-direct {v2, p0}, LX/JaH;-><init>(Lcom/facebook/goodfriends/feedplugins/GoodFriendsNuxFeedBottomAddFriendsPartDefinition;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599239
    const/16 v1, 0x1f

    const v2, -0x442fd422

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599236
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 599234
    const v0, 0x7f0d1e03

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599235
    return-void
.end method
