.class public Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNoContentGoodFriendsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/17Y;

.field private final e:LX/9J2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599176
    new-instance v0, LX/3ZE;

    invoke-direct {v0}, LX/3ZE;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17Y;LX/9J2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599177
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599178
    iput-object p1, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    .line 599179
    iput-object p2, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 599180
    iput-object p3, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->d:LX/17Y;

    .line 599181
    iput-object p4, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->e:LX/9J2;

    .line 599182
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;
    .locals 7

    .prologue
    .line 599183
    const-class v1, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;

    monitor-enter v1

    .line 599184
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599185
    sput-object v2, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599186
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599187
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599188
    new-instance p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v6

    check-cast v6, LX/9J2;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17Y;LX/9J2;)V

    .line 599189
    move-object v0, p0

    .line 599190
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599191
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599192
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599193
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599194
    sget-object v0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 599195
    const v0, 0x7f0d1dc9

    iget-object v1, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 599196
    new-instance v2, LX/JaJ;

    invoke-direct {v2, p0}, LX/JaJ;-><init>(Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;)V

    move-object v2, v2

    .line 599197
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599198
    iget-object v0, p0, Lcom/facebook/goodfriends/feedplugins/NoContentFeedPartDefinition;->e:LX/9J2;

    const-string v1, "no_content_placeholder"

    invoke-virtual {v0, v1}, LX/9J2;->b(Ljava/lang/String;)V

    .line 599199
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599200
    const/4 v0, 0x1

    return v0
.end method
