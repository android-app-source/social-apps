.class public Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public j:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field public o:LX/27c;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 568776
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 568777
    return-void
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 568785
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 568778
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 568779
    const-string v1, "tos_acceptance"

    .line 568780
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 568781
    const-string v1, "sdk_dialog_reason"

    iget-object v2, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 568782
    iget-object v1, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->j:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 568783
    invoke-virtual {v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 568784
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v1, p1, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->j:LX/0Zb;

    iput-object v2, p1, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->k:LX/03V;

    iput-object p0, p1, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 568786
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 568787
    const v1, 0x7f082383

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const v3, 0x7f082384

    const-string v4, "https://m.facebook.com/terms.php"

    invoke-direct {p0, v3, v4}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x1

    const v4, 0x7f082385

    const-string v5, "https://m.facebook.com/about/privacy/"

    invoke-direct {p0, v4, v5}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const v4, 0x7f082386

    iget-object v5, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->m:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 568788
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f082382

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f082387

    new-instance v2, LX/8DX;

    invoke-direct {v2, p0}, LX/8DX;-><init>(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 568789
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 568790
    invoke-virtual {v0, v6}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 568791
    const-string v1, "tos_dialog_shown"

    invoke-static {p0, v1}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;Ljava/lang/String;)V

    .line 568792
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 568772
    const-string v0, "tos_dialog_back_clicked"

    invoke-static {p0, v0}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;Ljava/lang/String;)V

    .line 568773
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->o:LX/27c;

    if-eqz v0, :cond_0

    .line 568774
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->o:LX/27c;

    invoke-virtual {v0}, LX/27c;->a()V

    .line 568775
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1623e4c0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 568751
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 568752
    const-class v1, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 568753
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 568754
    const-string v2, "target_app"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 568755
    packed-switch v1, :pswitch_data_0

    .line 568756
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "target_app argument must be one of the TARGET_APP_ constants"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const v2, 0x3805c32c

    invoke-static {v2, v0}, LX/02F;->f(II)V

    throw v1

    .line 568757
    :pswitch_0
    const-string v1, "https://www.facebook.com/help/1710719789144492"

    iput-object v1, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->m:Ljava/lang/String;

    .line 568758
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 568759
    const-string v2, "sdk_dialog_reason"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->n:Ljava/lang/String;

    .line 568760
    const v1, 0x3fced166

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 568761
    :pswitch_1
    const-string v1, "https://www.facebook.com/help/messenger-app/1573402819647115/"

    iput-object v1, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->m:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3fd1227f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 568769
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 568770
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->o:LX/27c;

    .line 568771
    const/16 v1, 0x2b

    const v2, -0x2662f6dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6ffed255

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 568762
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 568763
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 568764
    const v2, 0x7f0d0578

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 568765
    if-eqz v0, :cond_0

    .line 568766
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 568767
    :goto_0
    const v0, -0x645c0db7

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 568768
    :cond_0
    iget-object v0, p0, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->k:LX/03V;

    const-string v2, "OXYGEN_TOS_DIALOG"

    const-string v3, "Unable to find view in order to enable support for clicking links"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
