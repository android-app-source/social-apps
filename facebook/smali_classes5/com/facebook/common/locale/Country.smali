.class public Lcom/facebook/common/locale/Country;
.super Lcom/facebook/common/locale/LocaleMember;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/common/locale/Country;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/common/locale/Country;

.field private static final c:LX/468;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 670831
    new-instance v0, LX/468;

    invoke-direct {v0}, LX/468;-><init>()V

    sput-object v0, Lcom/facebook/common/locale/Country;->c:LX/468;

    .line 670832
    const-string v0, "US"

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    .line 670833
    new-instance v0, LX/466;

    invoke-direct {v0}, LX/466;-><init>()V

    sput-object v0, Lcom/facebook/common/locale/Country;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 0

    .prologue
    .line 670834
    invoke-direct {p0, p1}, Lcom/facebook/common/locale/LocaleMember;-><init>(Ljava/util/Locale;)V

    .line 670835
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 670836
    sget-object v0, Lcom/facebook/common/locale/Country;->c:LX/468;

    invoke-virtual {v0, p0}, LX/467;->b(Ljava/lang/String;)Lcom/facebook/common/locale/LocaleMember;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    return-object v0
.end method

.method public static a(Ljava/util/Locale;)Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 670830
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670826
    iget-object v0, p0, Lcom/facebook/common/locale/LocaleMember;->b:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670829
    iget-object v0, p0, Lcom/facebook/common/locale/LocaleMember;->b:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 670828
    iget-object v0, p0, Lcom/facebook/common/locale/LocaleMember;->b:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670827
    iget-object v0, p0, Lcom/facebook/common/locale/LocaleMember;->b:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
