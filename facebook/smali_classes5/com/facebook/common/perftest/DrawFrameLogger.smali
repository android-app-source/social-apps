.class public Lcom/facebook/common/perftest/DrawFrameLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[J

.field private static final b:[J

.field private static final c:[J

.field private static d:I

.field private static e:Z

.field private static f:J

.field private static volatile l:Lcom/facebook/common/perftest/DrawFrameLogger;


# instance fields
.field private final g:Lcom/facebook/common/perftest/PerfTestConfig;

.field public h:LX/0wY;

.field public i:LX/0wa;

.field public j:J

.field private k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1770

    .line 635205
    new-array v0, v1, [J

    sput-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->a:[J

    .line 635206
    new-array v0, v1, [J

    sput-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->b:[J

    .line 635207
    new-array v0, v1, [J

    sput-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->c:[J

    .line 635208
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/facebook/common/perftest/DrawFrameLogger;->f:J

    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/perftest/PerfTestConfig;LX/0wY;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635210
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    .line 635211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->k:Z

    .line 635212
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 635213
    iput-object p1, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->g:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 635214
    iput-object p2, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->h:LX/0wY;

    .line 635215
    new-instance v0, LX/3lu;

    invoke-direct {v0, p0}, LX/3lu;-><init>(Lcom/facebook/common/perftest/DrawFrameLogger;)V

    iput-object v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->i:LX/0wa;

    .line 635216
    invoke-static {}, Lcom/facebook/common/perftest/DrawFrameLogger;->clearFrameRateLog()V

    .line 635217
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/perftest/DrawFrameLogger;
    .locals 5

    .prologue
    .line 635222
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->l:Lcom/facebook/common/perftest/DrawFrameLogger;

    if-nez v0, :cond_1

    .line 635223
    const-class v1, Lcom/facebook/common/perftest/DrawFrameLogger;

    monitor-enter v1

    .line 635224
    :try_start_0
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->l:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 635225
    if-eqz v2, :cond_0

    .line 635226
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 635227
    new-instance p0, Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v4

    check-cast v4, LX/0wY;

    invoke-direct {p0, v3, v4}, Lcom/facebook/common/perftest/DrawFrameLogger;-><init>(Lcom/facebook/common/perftest/PerfTestConfig;LX/0wY;)V

    .line 635228
    move-object v0, p0

    .line 635229
    sput-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->l:Lcom/facebook/common/perftest/DrawFrameLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635230
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 635231
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 635232
    :cond_1
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->l:Lcom/facebook/common/perftest/DrawFrameLogger;

    return-object v0

    .line 635233
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 635234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 635218
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->k:Z

    if-nez v0, :cond_0

    .line 635219
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    .line 635220
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->k:Z

    .line 635221
    return-void
.end method

.method public static b(JJ)V
    .locals 6

    .prologue
    .line 635191
    const-string v0, "BullyDrawFrameLogger.logFrameTime"

    const v1, -0x28894ff2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 635192
    :try_start_0
    sget v0, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x1770

    if-lt v0, v1, :cond_0

    .line 635193
    const v0, 0x327c1a55

    invoke-static {v0}, LX/02m;->a(I)V

    .line 635194
    :goto_0
    return-void

    .line 635195
    :cond_0
    :try_start_1
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->a:[J

    sget v1, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    aput-wide p0, v0, v1

    .line 635196
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->c:[J

    sget v1, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    aput-wide p2, v0, v1

    .line 635197
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->b:[J

    sget v1, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 635198
    sget-object v0, Lcom/facebook/common/perftest/DrawFrameLogger;->c:[J

    sget v1, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    aget-wide v0, v0, v1

    const-wide/16 v2, 0x22

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    sget-boolean v0, Lcom/facebook/common/perftest/DrawFrameLogger;->e:Z

    if-nez v0, :cond_1

    .line 635199
    const-wide/16 v4, 0x2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 635200
    :goto_1
    :try_start_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 635201
    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    sub-long/2addr v0, p0

    sput-wide v0, Lcom/facebook/common/perftest/DrawFrameLogger;->f:J

    .line 635202
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/common/perftest/DrawFrameLogger;->e:Z

    .line 635203
    :cond_1
    sget v0, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 635204
    const v0, 0x7f939f46

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x4ead0742

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :catch_0
    goto :goto_1
.end method

.method public static clearFrameRateLog()V
    .locals 6
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 635159
    sput v1, Lcom/facebook/common/perftest/DrawFrameLogger;->d:I

    move v0, v1

    .line 635160
    :goto_0
    const/16 v2, 0x1770

    if-ge v0, v2, :cond_0

    .line 635161
    sget-object v2, Lcom/facebook/common/perftest/DrawFrameLogger;->a:[J

    aput-wide v4, v2, v0

    .line 635162
    sget-object v2, Lcom/facebook/common/perftest/DrawFrameLogger;->b:[J

    aput-wide v4, v2, v0

    .line 635163
    sget-object v2, Lcom/facebook/common/perftest/DrawFrameLogger;->c:[J

    aput-wide v4, v2, v0

    .line 635164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 635165
    :cond_0
    sput-boolean v1, Lcom/facebook/common/perftest/DrawFrameLogger;->e:Z

    .line 635166
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/facebook/common/perftest/DrawFrameLogger;->f:J

    .line 635167
    return-void
.end method

.method public static getFrameRateLogJSON()Lorg/json/JSONObject;
    .locals 10
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 635176
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 635177
    const-string v0, "markerLag"

    sget-wide v2, Lcom/facebook/common/perftest/DrawFrameLogger;->f:J

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 635178
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 635179
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 635180
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 635181
    const/4 v0, 0x0

    :goto_0
    const/16 v5, 0x1770

    if-ge v0, v5, :cond_0

    .line 635182
    sget-object v5, Lcom/facebook/common/perftest/DrawFrameLogger;->a:[J

    aget-wide v6, v5, v0

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 635183
    sget-object v5, Lcom/facebook/common/perftest/DrawFrameLogger;->a:[J

    aget-wide v6, v5, v0

    invoke-virtual {v2, v0, v6, v7}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    .line 635184
    sget-object v5, Lcom/facebook/common/perftest/DrawFrameLogger;->b:[J

    aget-wide v6, v5, v0

    invoke-virtual {v3, v0, v6, v7}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    .line 635185
    sget-object v5, Lcom/facebook/common/perftest/DrawFrameLogger;->c:[J

    aget-wide v6, v5, v0

    invoke-virtual {v4, v0, v6, v7}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    .line 635186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 635187
    :cond_0
    const-string v0, "frameTimestampBuffer"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 635188
    const-string v0, "frameSystemTimeBuffer"

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 635189
    const-string v0, "frameElapsedMsBuffer"

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 635190
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 635172
    iget-boolean v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->k:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635173
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/common/perftest/DrawFrameLogger;->a(Z)V

    .line 635174
    iget-object v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->h:LX/0wY;

    iget-object v1, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->i:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 635175
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 635168
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635169
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/common/perftest/DrawFrameLogger;->a(Z)V

    .line 635170
    iget-object v0, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->h:LX/0wY;

    iget-object v1, p0, Lcom/facebook/common/perftest/DrawFrameLogger;->i:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->b(LX/0wa;)V

    .line 635171
    :cond_0
    return-void
.end method
