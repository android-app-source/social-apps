.class public abstract Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;
.super Landroid/app/job/JobService;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 670693
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 670694
    return-void
.end method


# virtual methods
.method public abstract a()LX/45y;
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x4a55fdb9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 670695
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;->a()LX/45y;

    .line 670696
    const/4 v1, 0x2

    move v1, v1

    .line 670697
    const/16 v2, 0x25

    const v3, 0x37243213

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 4

    .prologue
    .line 670691
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;->a()LX/45y;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v1

    new-instance v2, Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/PersistableBundle;)V

    new-instance v3, LX/45w;

    invoke-direct {v3, p0, p1}, LX/45w;-><init>(Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;Landroid/app/job/JobParameters;)V

    invoke-virtual {v0, v1, v2, v3}, LX/45y;->a(ILandroid/os/Bundle;LX/45o;)Z

    move-result v0

    return v0
.end method

.method public final onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 670692
    invoke-virtual {p0}, Lcom/facebook/common/jobscheduler/compat/JobServiceCompat;->a()LX/45y;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    invoke-virtual {v0}, LX/45y;->a()Z

    move-result v0

    return v0
.end method
