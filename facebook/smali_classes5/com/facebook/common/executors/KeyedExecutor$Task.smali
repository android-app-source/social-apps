.class public final Lcom/facebook/common/executors/KeyedExecutor$Task;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/lang/Object;

.field public final c:LX/0Va;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Va",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;LX/0So;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .prologue
    .line 669770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669771
    sget-object v0, LX/44Z;->a:Ljava/lang/Class;

    .line 669772
    new-instance v1, LX/44o;

    invoke-direct {v1, p3, v0, p5}, LX/44o;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Class;LX/0So;)V

    move-object v0, v1

    .line 669773
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 669774
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 669775
    if-eqz p4, :cond_0

    .line 669776
    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 669777
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 669778
    iput-object v1, v0, LX/44o;->f:Ljava/lang/String;

    .line 669779
    invoke-static {v0}, LX/0Va;->a(Ljava/util/concurrent/Callable;)LX/0Va;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/executors/KeyedExecutor$Task;->c:LX/0Va;

    .line 669780
    iput-object p1, p0, Lcom/facebook/common/executors/KeyedExecutor$Task;->a:Ljava/lang/Object;

    .line 669781
    iput-object p2, p0, Lcom/facebook/common/executors/KeyedExecutor$Task;->b:Ljava/lang/Object;

    .line 669782
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;LX/0So;B)V
    .locals 0

    .prologue
    .line 669783
    invoke-direct/range {p0 .. p5}, Lcom/facebook/common/executors/KeyedExecutor$Task;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/concurrent/Callable;Ljava/lang/String;LX/0So;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 669784
    iget-object v0, p0, Lcom/facebook/common/executors/KeyedExecutor$Task;->c:LX/0Va;

    invoke-virtual {v0}, LX/0Va;->run()V

    .line 669785
    return-void
.end method
