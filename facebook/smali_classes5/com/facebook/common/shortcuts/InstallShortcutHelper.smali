.class public Lcom/facebook/common/shortcuts/InstallShortcutHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Z

.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/res/Resources;

.field private final e:LX/1HI;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 671409
    const-class v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    const-string v1, "shortcut"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1HI;Ljava/util/concurrent/Executor;LX/0Zb;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671402
    iput-object p1, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->c:Landroid/content/Context;

    .line 671403
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->d:Landroid/content/res/Resources;

    .line 671404
    iput-object p2, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->e:LX/1HI;

    .line 671405
    iput-object p3, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->f:Ljava/util/concurrent/Executor;

    .line 671406
    iput-object p4, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->g:LX/0Zb;

    .line 671407
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b:Z

    .line 671408
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;
    .locals 1

    .prologue
    .line 671400
    invoke-static {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;IZZ)V
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 671392
    const-string v0, "URL cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671393
    const-string v0, "Icon name cannot be null"

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671394
    if-nez p4, :cond_0

    .line 671395
    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;LX/46b;IZZLandroid/os/Bundle;)V

    .line 671396
    :goto_0
    return-void

    .line 671397
    :cond_0
    invoke-static {p4}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 671398
    iget-object v1, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->e:LX/1HI;

    sget-object v2, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v9

    .line 671399
    new-instance v0, LX/46a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/46a;-><init>(Lcom/facebook/common/shortcuts/InstallShortcutHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/46b;IZZ)V

    iget-object v1, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->f:Ljava/util/concurrent/Executor;

    invoke-interface {v9, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;
    .locals 6

    .prologue
    .line 671390
    new-instance v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v2

    check-cast v2, LX/1HI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;-><init>(Landroid/content/Context;LX/1HI;Ljava/util/concurrent/Executor;LX/0Zb;Ljava/lang/Boolean;)V

    .line 671391
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 671386
    iget-object v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->c:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 671387
    if-eqz v0, :cond_0

    .line 671388
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v0

    .line 671389
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->d:Landroid/content/res/Resources;

    const/high16 v1, 0x1050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;LX/46b;Z)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 671349
    if-nez p1, :cond_0

    .line 671350
    iget-object v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->d:Landroid/content/res/Resources;

    const v1, 0x7f021126

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 671351
    :cond_0
    if-nez p1, :cond_1

    .line 671352
    const/4 v0, 0x0

    .line 671353
    :goto_0
    return-object v0

    .line 671354
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a()I

    move-result v1

    .line 671355
    if-eqz p3, :cond_4

    .line 671356
    sparse-switch v1, :sswitch_data_0

    .line 671357
    int-to-float v0, v1

    const/high16 v2, 0x3f600000    # 0.875f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_1
    move v0, v0

    .line 671358
    :goto_2
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 671359
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 671360
    invoke-virtual {v4, v8, v8, v8, v8}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 671361
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 671362
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 671363
    const/4 v2, -0x1

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 671364
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v6, v8, v8, v2, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 671365
    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v2, v7

    int-to-float v7, v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v2, v7}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 671366
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v2

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 671367
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v2, v8

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 671368
    sub-int v8, v1, v7

    int-to-float v8, v8

    div-float/2addr v8, v11

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 671369
    sub-int v9, v1, v2

    int-to-float v9, v9

    div-float/2addr v9, v11

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 671370
    new-instance v10, Landroid/graphics/Rect;

    add-int/2addr v7, v8

    add-int/2addr v2, v9

    invoke-direct {v10, v8, v9, v7, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 671371
    sget-object v2, LX/46b;->ROUNDED:LX/46b;

    if-eq p2, v2, :cond_2

    sget-object v2, LX/46b;->CIRCLE:LX/46b;

    if-ne p2, v2, :cond_3

    .line 671372
    :cond_2
    sget-object v2, LX/46b;->ROUNDED:LX/46b;

    if-ne p2, v2, :cond_5

    .line 671373
    int-to-float v2, v0

    const v7, 0x3dcccccd    # 0.1f

    mul-float/2addr v2, v7

    .line 671374
    :goto_3
    sub-int/2addr v1, v0

    int-to-float v1, v1

    div-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 671375
    new-instance v7, Landroid/graphics/RectF;

    int-to-float v8, v1

    int-to-float v9, v1

    add-int v11, v1, v0

    int-to-float v11, v11

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-direct {v7, v8, v9, v11, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 671376
    invoke-virtual {v4, v7, v2, v2, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 671377
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 671378
    :cond_3
    invoke-virtual {v4, p1, v6, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v0, v3

    .line 671379
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 671380
    goto/16 :goto_2

    .line 671381
    :cond_5
    int-to-float v2, v0

    div-float/2addr v2, v11

    goto :goto_3

    .line 671382
    :sswitch_0
    const/16 v0, 0x52

    goto/16 :goto_1

    .line 671383
    :sswitch_1
    const/16 v0, 0x3e

    goto/16 :goto_1

    .line 671384
    :sswitch_2
    const/16 v0, 0x2a

    goto/16 :goto_1

    .line 671385
    :sswitch_3
    const/16 v0, 0x22

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_3
        0x30 -> :sswitch_2
        0x48 -> :sswitch_1
        0x60 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V
    .locals 5
    .param p3    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 671331
    const-string v0, "Intent cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671332
    const-string v0, "Caption cannot be null"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671333
    invoke-virtual {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a()I

    move-result v1

    .line 671334
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Unexpected icon size. Use getLauncherIconSize() to get the proper size of an icon"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 671335
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 671336
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 671337
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 671338
    if-eqz p3, :cond_2

    .line 671339
    if-eqz p4, :cond_1

    .line 671340
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 671341
    int-to-float v3, v1

    const/high16 v4, 0x40300000    # 2.75f

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 671342
    sub-int v4, v1, v3

    sub-int v3, v1, v3

    invoke-virtual {p4, v4, v3, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 671343
    invoke-virtual {p4, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 671344
    :cond_1
    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 671345
    :cond_2
    const-string v1, "duplicate"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 671346
    iget-object v1, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->c:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 671347
    return-void

    .line 671348
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;)V
    .locals 9
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    .line 671325
    const-string v2, "com.facebook.katana.IntentUriHandler"

    .line 671326
    iget-boolean v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b:Z

    if-eqz v0, :cond_0

    const v0, 0x7f021afe

    :goto_0
    move v6, v0

    .line 671327
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;IZZ)V

    .line 671328
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "create_shortcut"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "uri"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 671329
    iget-object v1, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 671330
    return-void

    :cond_0
    const v0, 0x7f02069a

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;LX/46b;IZZLandroid/os/Bundle;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 671311
    const-string v0, "URL cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671312
    const-string v0, "Icon name cannot be null"

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671313
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 671314
    if-eqz p2, :cond_0

    .line 671315
    new-instance v0, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->c:Landroid/content/Context;

    invoke-direct {v0, v2, p2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 671316
    :cond_0
    if-eqz p9, :cond_1

    .line 671317
    invoke-virtual {v1, p9}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 671318
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 671319
    const-string v0, "shortcut_open"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 671320
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 671321
    const/4 v0, -0x1

    if-eq p6, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 671322
    :goto_0
    invoke-virtual {p0, p4, p5, p8}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/graphics/Bitmap;LX/46b;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v0, p0

    move-object v2, p3

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;Z)V

    .line 671323
    return-void

    .line 671324
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
