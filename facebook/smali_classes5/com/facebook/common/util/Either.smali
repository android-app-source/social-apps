.class public Lcom/facebook/common/util/Either;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0QR",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/common/util/Either;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 672338
    new-instance v0, LX/47b;

    invoke-direct {v0}, LX/47b;-><init>()V

    sput-object v0, Lcom/facebook/common/util/Either;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 672339
    invoke-static {p1}, LX/46R;->o(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1}, LX/46R;->o(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/common/util/Either;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 672340
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            "TR;Z)V"
        }
    .end annotation

    .prologue
    .line 672341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672342
    iput-object p1, p0, Lcom/facebook/common/util/Either;->a:Ljava/lang/Object;

    .line 672343
    iput-object p2, p0, Lcom/facebook/common/util/Either;->b:Ljava/lang/Object;

    .line 672344
    iput-boolean p3, p0, Lcom/facebook/common/util/Either;->c:Z

    .line 672345
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 672346
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 672347
    instance-of v0, p1, Lcom/facebook/common/util/Either;

    if-nez v0, :cond_0

    .line 672348
    const/4 v0, 0x0

    .line 672349
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/common/util/Either;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast p1, Lcom/facebook/common/util/Either;

    invoke-virtual {p1}, Lcom/facebook/common/util/Either;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 672350
    iget-boolean v0, p0, Lcom/facebook/common/util/Either;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/util/Either;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/util/Either;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 672351
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/common/util/Either;->get()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 672352
    const-string v1, "Either.%s(%s)"

    iget-boolean v0, p0, Lcom/facebook/common/util/Either;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "left"

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/common/util/Either;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "right"

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 672353
    iget-object v0, p0, Lcom/facebook/common/util/Either;->a:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 672354
    iget-object v0, p0, Lcom/facebook/common/util/Either;->b:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 672355
    iget-boolean v0, p0, Lcom/facebook/common/util/Either;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 672356
    return-void
.end method
