.class public Lcom/facebook/common/networkreachability/AndroidReachabilityListener;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/46K;

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mNetworkStateInfo:Lcom/facebook/common/networkreachability/NetworkStateInfo;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 671011
    const-class v0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;

    sput-object v0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->a:Ljava/lang/Class;

    .line 671012
    const-string v0, "android-reachability-announcer"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 671013
    return-void
.end method

.method public constructor <init>(LX/46K;)V
    .locals 2

    .prologue
    .line 671006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671007
    new-instance v0, LX/46P;

    invoke-direct {v0, p0}, LX/46P;-><init>(Lcom/facebook/common/networkreachability/AndroidReachabilityListener;)V

    iput-object v0, p0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->mNetworkStateInfo:Lcom/facebook/common/networkreachability/NetworkStateInfo;

    .line 671008
    iget-object v0, p0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->mNetworkStateInfo:Lcom/facebook/common/networkreachability/NetworkStateInfo;

    invoke-direct {p0, v0}, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->initHybrid(Lcom/facebook/common/networkreachability/NetworkStateInfo;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 671009
    iput-object p1, p0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->b:LX/46K;

    .line 671010
    return-void
.end method

.method private native initHybrid(Lcom/facebook/common/networkreachability/NetworkStateInfo;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native networkStateChanged(II)V
.end method
