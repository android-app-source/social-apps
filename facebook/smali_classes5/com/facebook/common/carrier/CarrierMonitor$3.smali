.class public final Lcom/facebook/common/carrier/CarrierMonitor$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/440;

.field public final synthetic b:LX/18b;


# direct methods
.method public constructor <init>(LX/18b;LX/440;)V
    .locals 0

    .prologue
    .line 669327
    iput-object p1, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iput-object p2, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->a:LX/440;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 669328
    iget-object v1, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    monitor-enter v1

    .line 669329
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v0, v0, LX/18b;->k:Landroid/telephony/PhoneStateListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 669330
    :try_start_1
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    const-class v2, Landroid/telephony/SignalStrength;

    const-string v3, "getLteRsrp"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 669331
    iput-object v2, v0, LX/18b;->l:Ljava/lang/reflect/Method;

    .line 669332
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v0, v0, LX/18b;->l:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669333
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    new-instance v2, LX/43z;

    invoke-direct {v2, p0}, LX/43z;-><init>(Lcom/facebook/common/carrier/CarrierMonitor$3;)V

    .line 669334
    iput-object v2, v0, LX/18b;->k:Landroid/telephony/PhoneStateListener;

    .line 669335
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v0, v0, LX/18b;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    .line 669336
    iget-object v2, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v2, v2, LX/18b;->m:Ljava/util/Map;

    iget-object v3, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->a:LX/440;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669337
    if-eqz v0, :cond_1

    .line 669338
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v0, v0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/facebook/common/carrier/CarrierMonitor$3;->b:LX/18b;

    iget-object v2, v2, LX/18b;->k:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x100

    invoke-virtual {v0, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 669339
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    goto :goto_0
.end method
