.class public final Lcom/facebook/common/carrier/CarrierMonitor$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/440;

.field public final synthetic b:LX/18b;


# direct methods
.method public constructor <init>(LX/18b;LX/440;)V
    .locals 0

    .prologue
    .line 669340
    iput-object p1, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    iput-object p2, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->a:LX/440;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 669341
    iget-object v1, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    monitor-enter v1

    .line 669342
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    iget-object v0, v0, LX/18b;->m:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->a:LX/440;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 669343
    const-string v0, "CarrierMonitor"

    const-string v2, "stopMonitoringSignalStrength for object not in list \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->a:LX/440;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 669344
    :cond_0
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    iget-object v0, v0, LX/18b;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 669345
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    iget-object v0, v0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    iget-object v2, v2, LX/18b;->k:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 669346
    iget-object v0, p0, Lcom/facebook/common/carrier/CarrierMonitor$4;->b:LX/18b;

    const/4 v2, 0x0

    .line 669347
    iput-object v2, v0, LX/18b;->n:Landroid/telephony/SignalStrength;

    .line 669348
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
