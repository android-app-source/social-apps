.class public final Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/2Hp;


# direct methods
.method public constructor <init>(LX/2Hp;)V
    .locals 0

    .prologue
    .line 571385
    iput-object p1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;->a:LX/2Hp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 571386
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 571387
    iget-object v0, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;->a:LX/2Hp;

    iget-object v0, v0, LX/2Hp;->i:LX/0V3;

    const-string v1, "daily"

    .line 571388
    :try_start_0
    invoke-static {v0, v1}, LX/0V3;->b(LX/0V3;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 571389
    :goto_0
    iget-object v0, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;->a:LX/2Hp;

    iget-object v1, p0, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;->a:LX/2Hp;

    iget-object v1, v1, LX/2Hp;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, LX/2Hp;->a$redex0(LX/2Hp;J)V

    .line 571390
    return-void

    .line 571391
    :catch_0
    move-exception v2

    .line 571392
    const-string v3, "MemoryDumper"

    const-string v4, "Error writing Hprof dump"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 571393
    iget-object v3, v0, LX/0V3;->e:LX/03V;

    const-string v4, "hprof"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
