.class public final Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0kg;


# direct methods
.method public constructor <init>(LX/0kg;)V
    .locals 0

    .prologue
    .line 670984
    iput-object p1, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 670985
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    iget-object v0, v0, LX/0kg;->a:LX/0kb;

    iget-object v1, v0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 670986
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    iget-object v0, v0, LX/0kg;->a:LX/0kb;

    iget-object v0, v0, LX/0kb;->C:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    iget-object v0, v0, LX/0kg;->a:LX/0kb;

    iget-object v0, v0, LX/0kb;->C:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v2, :cond_0

    .line 670987
    iget-object v0, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    iget-object v0, v0, LX/0kg;->a:LX/0kb;

    iget-object v2, p0, Lcom/facebook/common/network/FbNetworkManager$ActivityLifecycleListener$1;->a:LX/0kg;

    iget-object v2, v2, LX/0kg;->a:LX/0kb;

    iget-object v2, v2, LX/0kb;->C:Landroid/net/NetworkInfo;

    const-string v3, "checkNetworkOnResume"

    invoke-static {v0, v2, v3}, LX/0kb;->a$redex0(LX/0kb;Landroid/net/NetworkInfo;Ljava/lang/String;)V

    .line 670988
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
