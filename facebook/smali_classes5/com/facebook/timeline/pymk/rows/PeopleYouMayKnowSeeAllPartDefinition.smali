.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/DFo;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DFo;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613195
    new-instance v0, LX/3c9;

    invoke-direct {v0}, LX/3c9;-><init>()V

    sput-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613196
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 613197
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 613198
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->c:LX/17W;

    .line 613199
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;
    .locals 5

    .prologue
    .line 613200
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    monitor-enter v1

    .line 613201
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613202
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613203
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613204
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613205
    new-instance p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;)V

    .line 613206
    move-object v0, p0

    .line 613207
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613208
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613209
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DFo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613211
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 613212
    const v0, 0x7f0d27af

    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 613213
    new-instance v2, LX/G33;

    invoke-direct {v2, p0}, LX/G33;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;)V

    move-object v2, v2

    .line 613214
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613215
    const/4 v0, 0x0

    return-object v0
.end method
