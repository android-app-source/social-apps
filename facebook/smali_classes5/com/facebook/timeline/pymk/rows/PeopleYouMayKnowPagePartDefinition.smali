.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/G32;",
        "LX/DEo;",
        "TE;",
        "LX/2fB;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/2fB;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/2do;

.field private final c:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

.field private final d:Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613192
    new-instance v0, LX/3c8;

    invoke-direct {v0}, LX/3c8;-><init>()V

    sput-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/2do;Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613184
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 613185
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->b:LX/2do;

    .line 613186
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->c:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    .line 613187
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    .line 613188
    iput-object p4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    .line 613189
    iput-object p5, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    .line 613190
    iput-object p6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->g:Landroid/content/res/Resources;

    .line 613191
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;
    .locals 10

    .prologue
    .line 613173
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    monitor-enter v1

    .line 613174
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613175
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613178
    new-instance v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v4

    check-cast v4, LX/2do;

    invoke-static {v0}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;-><init>(LX/2do;Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;Landroid/content/res/Resources;)V

    .line 613179
    move-object v0, v3

    .line 613180
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/2fB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613172
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 613150
    check-cast p2, LX/G32;

    check-cast p3, LX/1Pc;

    .line 613151
    iget-object v0, p2, LX/G32;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 613152
    iget-object v0, p2, LX/G32;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    move-result-object v2

    .line 613153
    iget-object v0, p2, LX/G32;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v0

    .line 613154
    if-lez v0, :cond_0

    .line 613155
    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->g:Landroid/content/res/Resources;

    const v4, 0x7f0f005c

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 613156
    :goto_0
    move-object v3, v3

    .line 613157
    iget-object v0, p2, LX/G32;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v4

    .line 613158
    iget-object v0, p2, LX/G32;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 613159
    new-instance v5, LX/2en;

    invoke-direct {v5, v1, v7}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v0, p3

    .line 613160
    check-cast v0, LX/1Pr;

    iget-object v6, p2, LX/G32;->d:LX/0jW;

    invoke-interface {v0, v5, v6}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2ep;

    .line 613161
    const v8, 0x7f0d1145

    iget-object v9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->c:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowTextContainerPartDefinition;

    new-instance v0, LX/2er;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/2er;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ep;LX/162;)V

    invoke-interface {p1, v8, v9, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613162
    const v0, 0x7f0d1144

    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->d:Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;

    new-instance v5, LX/2ey;

    invoke-direct {v5, v4, v2}, LX/2ey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613163
    const v0, 0x7f0d1149

    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    new-instance v4, LX/G2q;

    iget-object v5, p2, LX/G32;->b:LX/G2x;

    iget-object v6, p2, LX/G32;->d:LX/0jW;

    invoke-direct {v4, v1, v7, v5, v6}, LX/G2q;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/G2x;LX/0jW;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613164
    const v0, 0x7f0d1148

    iget-object v3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowFriendingButtonPartDefinition;

    new-instance v4, LX/G2v;

    iget-object v5, p2, LX/G32;->d:LX/0jW;

    invoke-direct {v4, v1, v2, v7, v5}, LX/G2v;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/0jW;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613165
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->b:LX/2do;

    new-instance v1, LX/G31;

    check-cast p3, LX/1Pq;

    iget-object v2, p2, LX/G32;->d:LX/0jW;

    invoke-direct {v1, p3, v7, v2}, LX/G31;-><init>(LX/1Pq;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/0jW;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 613166
    new-instance v0, LX/DEo;

    iget-object v1, p2, LX/G32;->d:LX/0jW;

    invoke-interface {v1}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DEo;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string v3, ""

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7bc03d4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613167
    check-cast p1, LX/G32;

    check-cast p2, LX/DEo;

    check-cast p3, LX/1Pc;

    .line 613168
    move-object v1, p3

    check-cast v1, LX/1Pr;

    iget-object v2, p1, LX/G32;->d:LX/0jW;

    invoke-interface {v1, p2, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613169
    check-cast p3, LX/1Pr;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p3, p2, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 613170
    iget-object v1, p1, LX/G32;->c:LX/2dx;

    invoke-virtual {v1}, LX/2dx;->a()V

    .line 613171
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x6fcf35f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
