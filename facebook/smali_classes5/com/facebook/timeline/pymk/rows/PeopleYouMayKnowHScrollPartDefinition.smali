.class public Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G2o;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field public final c:LX/2dk;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

.field public final f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

.field public final h:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;

.field private final i:LX/2dq;

.field public final j:Landroid/content/res/Resources;

.field public final k:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613145
    new-instance v0, LX/3c7;

    invoke-direct {v0}, LX/3c7;-><init>()V

    sput-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a:LX/1Cz;

    .line 613146
    const-class v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2dk;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;LX/2dq;Landroid/content/res/Resources;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613134
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613135
    iput-object p1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->c:LX/2dk;

    .line 613136
    iput-object p2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 613137
    iput-object p3, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    .line 613138
    iput-object p4, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    .line 613139
    iput-object p5, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->g:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    .line 613140
    iput-object p6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->h:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;

    .line 613141
    iput-object p7, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->i:LX/2dq;

    .line 613142
    iput-object p8, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->j:Landroid/content/res/Resources;

    .line 613143
    iput-object p9, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->k:LX/1Ck;

    .line 613144
    return-void
.end method

.method private a(LX/G2o;LX/1Pc;LX/2dx;)LX/2eJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G2o;",
            "TE;",
            "LX/2dx;",
            ")",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 613112
    iget-object v0, p1, LX/G2o;->a:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 613113
    new-instance v0, LX/G2w;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/G2w;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;Ljava/util/List;LX/G2o;LX/1Pc;LX/2dx;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    .locals 13

    .prologue
    .line 613123
    const-class v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    monitor-enter v1

    .line 613124
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613125
    sput-object v2, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613126
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613127
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613128
    new-instance v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    invoke-static {v0}, LX/2dk;->b(LX/0QB;)LX/2dk;

    move-result-object v4

    check-cast v4, LX/2dk;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v10

    check-cast v10, LX/2dq;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;-><init>(LX/2dk;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;LX/2dq;Landroid/content/res/Resources;LX/1Ck;)V

    .line 613129
    move-object v0, v3

    .line 613130
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613131
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613132
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613147
    sget-object v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 613118
    check-cast p2, LX/G2o;

    check-cast p3, LX/1Pc;

    .line 613119
    new-instance v3, LX/2dx;

    invoke-direct {v3}, LX/2dx;-><init>()V

    .line 613120
    iget-object v0, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613121
    iget-object v6, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->i:LX/2dq;

    const/high16 v2, 0x43980000    # 304.0f

    sget-object v4, LX/2eF;->a:LX/1Ua;

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v4, v5}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p2, p3, v3}, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(LX/G2o;LX/1Pc;LX/2dx;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, LX/G2o;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613122
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x420b3693

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613115
    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 613116
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->j:Landroid/content/res/Resources;

    const p1, 0x7f0a00d5

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p4, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 613117
    const/16 v1, 0x1f

    const v2, -0x203fe57e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613114
    const/4 v0, 0x1

    return v0
.end method
