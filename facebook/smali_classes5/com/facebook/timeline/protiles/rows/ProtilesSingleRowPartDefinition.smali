.class public Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G1K;",
        "LX/G2H;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:LX/G2T;

.field private final d:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

.field private final e:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

.field public final f:Landroid/content/res/Resources;

.field private final g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612755
    const-class v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 612756
    new-instance v0, LX/3bz;

    invoke-direct {v0}, LX/3bz;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/G2T;Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612757
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612758
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->c:LX/G2T;

    .line 612759
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->d:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    .line 612760
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->e:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    .line 612761
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->f:Landroid/content/res/Resources;

    .line 612762
    iput-object p5, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 612763
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;
    .locals 9

    .prologue
    .line 612764
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    monitor-enter v1

    .line 612765
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612766
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612767
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612768
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612769
    new-instance v3, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;

    invoke-static {v0}, LX/G2T;->a(LX/0QB;)LX/G2T;

    move-result-object v4

    check-cast v4, LX/G2T;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;-><init>(LX/G2T;Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 612770
    move-object v0, v3

    .line 612771
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612775
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 612776
    check-cast p2, LX/G1K;

    .line 612777
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->g:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    const/4 v2, 0x0

    .line 612778
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->f:Landroid/content/res/Resources;

    const v3, 0x7f0b1493

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 612779
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->f:Landroid/content/res/Resources;

    const v4, 0x7f0b1492

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 612780
    iget-object v1, p2, LX/G1K;->b:LX/G1J;

    move-object v1, v1

    .line 612781
    sget-object v5, LX/G1J;->TOP_ROW:LX/G1J;

    if-ne v1, v5, :cond_1

    move v1, v2

    .line 612782
    :goto_0
    iget-object v5, p2, LX/G1K;->b:LX/G1J;

    move-object v5, v5

    .line 612783
    sget-object v6, LX/G1J;->BOTTOM_ROW:LX/G1J;

    if-ne v5, v6, :cond_2

    .line 612784
    :goto_1
    new-instance v3, LX/1ds;

    invoke-direct {v3, v4, v1, v4, v2}, LX/1ds;-><init>(IIII)V

    move-object v1, v3

    .line 612785
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612786
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b1493

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 612787
    iget-object v0, p2, LX/G1K;->c:Ljava/util/List;

    move-object v0, v0

    .line 612788
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 612789
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->c:LX/G2T;

    invoke-virtual {v0}, LX/G2T;->b()I

    move-result v4

    .line 612790
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v7, :cond_0

    .line 612791
    iget-object v8, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->d:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    new-instance v0, LX/G28;

    .line 612792
    iget-object v1, p2, LX/G1K;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object v1, v1

    .line 612793
    iget-object v2, p2, LX/G1K;->c:Ljava/util/List;

    move-object v2, v2

    .line 612794
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    sget-object v5, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct/range {v0 .. v5}, LX/G28;-><init>(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;IILcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612795
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesSingleRowPartDefinition;->e:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoFooterPartDefinition;

    new-instance v2, LX/G24;

    .line 612796
    iget-object v0, p2, LX/G1K;->c:Ljava/util/List;

    move-object v0, v0

    .line 612797
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-direct {v2, v0, v3}, LX/G24;-><init>(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612798
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 612799
    :cond_0
    new-instance v0, LX/G2H;

    invoke-direct {v0, v7, v6}, LX/G2H;-><init>(II)V

    return-object v0

    :cond_1
    move v1, v3

    .line 612800
    goto :goto_0

    :cond_2
    move v2, v3

    .line 612801
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3e3b147f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612802
    check-cast p2, LX/G2H;

    check-cast p4, Lcom/facebook/widget/CustomLinearLayout;

    .line 612803
    invoke-virtual {p4}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v1

    :goto_0
    iget v2, p2, LX/G2H;->a:I

    if-ge v1, v2, :cond_0

    .line 612804
    invoke-virtual {p4}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v4, p2, LX/G2H;->b:I

    const/4 p3, 0x0

    const/4 p1, -0x2

    .line 612805
    new-instance p0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, p1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 612806
    invoke-virtual {p0, v4, p3, v4, p3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 612807
    new-instance p1, LX/G2Z;

    invoke-direct {p1, v2}, LX/G2Z;-><init>(Landroid/content/Context;)V

    .line 612808
    invoke-virtual {p1, p0}, LX/G2Z;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 612809
    move-object v2, p1

    .line 612810
    invoke-virtual {p4, v2}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 612811
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 612812
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x530f9d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612813
    check-cast p1, LX/G1K;

    .line 612814
    iget-object v0, p1, LX/G1K;->c:Ljava/util/List;

    move-object v0, v0

    .line 612815
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
