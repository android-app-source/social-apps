.class public Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G1M;",
        "LX/1DI;",
        "LX/1PW;",
        "Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:LX/1K9;

.field public final d:LX/BQB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612820
    new-instance v0, LX/3c0;

    invoke-direct {v0}, LX/3c0;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1K9;LX/BQB;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612821
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612822
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->c:LX/1K9;

    .line 612823
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->d:LX/BQB;

    .line 612824
    const v0, 0x7f080061

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->b:Ljava/lang/String;

    .line 612825
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;
    .locals 6

    .prologue
    .line 612826
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;

    monitor-enter v1

    .line 612827
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612828
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612829
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612830
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612831
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v5

    check-cast v5, LX/BQB;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;-><init>(Landroid/content/res/Resources;LX/1K9;LX/BQB;)V

    .line 612832
    move-object v0, p0

    .line 612833
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612834
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612835
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612837
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 612838
    check-cast p2, LX/G1M;

    .line 612839
    invoke-virtual {p2}, LX/G1M;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612840
    new-instance v0, LX/G2D;

    invoke-direct {v0, p0}, LX/G2D;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;)V

    .line 612841
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x8a5bab5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612842
    check-cast p1, LX/G1M;

    check-cast p2, LX/1DI;

    check-cast p4, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 612843
    invoke-virtual {p1}, LX/G1M;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 612844
    invoke-virtual {p4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 612845
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->d:LX/BQB;

    const/4 v2, 0x1

    .line 612846
    iput-boolean v2, v1, LX/BQB;->w:Z

    .line 612847
    const/16 v1, 0x1f

    const v2, -0x168a6db4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 612848
    :cond_1
    invoke-virtual {p1}, LX/G1M;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 612849
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->b:Ljava/lang/String;

    invoke-virtual {p4, v1, p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612850
    check-cast p1, LX/G1M;

    .line 612851
    invoke-virtual {p1}, LX/G1M;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/G1M;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 612852
    check-cast p4, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 612853
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesLoadingIndicatorPartDefinition;->d:LX/BQB;

    const/4 v1, 0x0

    .line 612854
    iput-boolean v1, v0, LX/BQB;->w:Z

    .line 612855
    invoke-virtual {p4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e()V

    .line 612856
    return-void
.end method
