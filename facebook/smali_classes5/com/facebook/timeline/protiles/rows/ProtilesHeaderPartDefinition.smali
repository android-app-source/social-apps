.class public Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "LX/G2B;",
        "LX/1Pq;",
        "Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1K9;

.field private final c:LX/G1G;

.field public final d:LX/BQB;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/G2X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612862
    new-instance v0, LX/3c1;

    invoke-direct {v0}, LX/3c1;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1K9;LX/G1G;LX/BQB;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/G2X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612863
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612864
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->b:LX/1K9;

    .line 612865
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->c:LX/G1G;

    .line 612866
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->d:LX/BQB;

    .line 612867
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612868
    iput-object p5, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->f:LX/G2X;

    .line 612869
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;
    .locals 9

    .prologue
    .line 612870
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    monitor-enter v1

    .line 612871
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612872
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612873
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612874
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612875
    new-instance v3, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-static {v0}, LX/G1G;->a(LX/0QB;)LX/G1G;

    move-result-object v5

    check-cast v5, LX/G1G;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v6

    check-cast v6, LX/BQB;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/G2X;->a(LX/0QB;)LX/G2X;

    move-result-object v8

    check-cast v8, LX/G2X;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;-><init>(LX/1K9;LX/G1G;LX/BQB;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/G2X;)V

    .line 612876
    move-object v0, v3

    .line 612877
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612878
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612879
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612880
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612881
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612882
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p3, LX/1Pq;

    .line 612883
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/G2A;

    invoke-direct {v1, p0, p2, p3}, LX/G2A;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Pq;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612884
    new-instance v0, LX/G2B;

    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->c:LX/G1G;

    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/G1G;->a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->f:LX/G2X;

    const/4 p0, 0x0

    .line 612885
    invoke-static {v2, p2}, LX/G2X;->f(LX/G2X;Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 612886
    :cond_0
    :goto_0
    move v2, p0

    .line 612887
    invoke-direct {v0, v1, v2}, LX/G2B;-><init>(Landroid/graphics/drawable/Drawable;Z)V

    return-object v0

    :cond_1
    invoke-virtual {v2, p2}, LX/G2X;->c(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 612888
    iget-boolean p1, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->d:Z

    move p1, p1

    .line 612889
    if-nez p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4dead8e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612890
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p2, LX/G2B;

    check-cast p4, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    .line 612891
    iget-object v1, p2, LX/G2B;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 612892
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 612893
    iget-boolean v1, p2, LX/G2B;->b:Z

    invoke-virtual {p4, v1}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(Z)V

    .line 612894
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->d:LX/BQB;

    const/4 v2, 0x1

    .line 612895
    iput-boolean v2, v1, LX/BQB;->w:Z

    .line 612896
    const/16 v1, 0x1f

    const v2, 0x2d367116

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612897
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 612898
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 612899
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->d:LX/BQB;

    const/4 v1, 0x0

    .line 612900
    iput-boolean v1, v0, LX/BQB;->w:Z

    .line 612901
    return-void
.end method
