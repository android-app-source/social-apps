.class public Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G1N;",
        "Ljava/lang/String;",
        "LX/1PW;",
        "LX/G2Y;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612697
    new-instance v0, LX/3bx;

    invoke-direct {v0}, LX/3bx;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612691
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612692
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->b:LX/0Or;

    .line 612693
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->c:LX/0Or;

    .line 612694
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612695
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 612696
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;
    .locals 7

    .prologue
    .line 612668
    const-class v1, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;

    monitor-enter v1

    .line 612669
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612670
    sput-object v2, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612671
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612672
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612673
    new-instance v5, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    const/16 v6, 0x2eb

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x3646

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Or;LX/0Or;)V

    .line 612674
    move-object v0, v5

    .line 612675
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612676
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612677
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612678
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612690
    sget-object v0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612685
    check-cast p2, LX/G1N;

    .line 612686
    invoke-virtual {p2}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G1P;

    .line 612687
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/G2K;

    invoke-direct {v2, p0, v0}, LX/G2K;-><init>(Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;LX/G1P;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612688
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/TimelinePendingFriendRequestsPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, v0, LX/G1P;->d:Ljava/lang/String;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612689
    iget-object v1, v0, LX/G1P;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, LX/G1P;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x38c8438e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612681
    check-cast p2, Ljava/lang/String;

    check-cast p4, LX/G2Y;

    .line 612682
    if-eqz p2, :cond_0

    .line 612683
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 612684
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x5feb54e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612679
    check-cast p1, LX/G1N;

    .line 612680
    invoke-virtual {p1}, LX/BPB;->d()Z

    move-result v0

    return v0
.end method
