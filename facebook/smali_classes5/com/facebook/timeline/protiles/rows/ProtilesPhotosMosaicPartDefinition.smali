.class public Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/Integer;",
        "TE;",
        "Lcom/facebook/widget/mosaic/MosaicGridLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612743
    new-instance v0, LX/3by;

    invoke-direct {v0}, LX/3by;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612739
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612740
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    .line 612741
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->c:LX/0Or;

    .line 612742
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 4

    .prologue
    .line 612724
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 612725
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 612726
    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const v3, 0x7f0a054c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v1

    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 612727
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;
    .locals 5

    .prologue
    .line 612728
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    monitor-enter v1

    .line 612729
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612730
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612731
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612732
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612733
    new-instance v4, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;

    invoke-static {v0}, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    const/16 p0, 0x1488

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;LX/0Or;)V

    .line 612734
    move-object v0, v4

    .line 612735
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612736
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612737
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612700
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 612701
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p3, LX/1Pp;

    const/4 v0, 0x0

    .line 612702
    iget-object v1, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object v1, v1

    .line 612703
    invoke-interface {p3, v1}, LX/1Pp;->a(Ljava/lang/String;)V

    move v1, v0

    move v2, v0

    .line 612704
    :goto_0
    iget-object v0, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 612705
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 612706
    iget-object v0, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 612707
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 612708
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 612709
    iget-object v4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->b:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    new-instance v5, LX/G2F;

    add-int/lit8 v3, v2, 0x1

    invoke-direct {v5, v0, p2, v2}, LX/G2F;-><init>(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;Lcom/facebook/timeline/protiles/model/ProtileModel;I)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move v2, v3

    .line 612710
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 612711
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3fc9b944

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612714
    check-cast p2, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/widget/mosaic/MosaicGridLayout;

    const/4 p3, 0x0

    .line 612715
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 612716
    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 612717
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6d9ab8a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 612718
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a(Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    .line 612719
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 612720
    new-instance v2, LX/G6g;

    const/4 v4, 0x6

    const/4 p1, 0x3

    invoke-direct {v2, p3, p3, v4, p1}, LX/G6g;-><init>(IIII)V

    invoke-virtual {p4, v1, v2}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 612721
    :cond_2
    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getChildCount()I

    move-result v1

    :goto_1
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 612722
    invoke-virtual {p4}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/timeline/protiles/rows/ProtilesPhotosMosaicPartDefinition;->a(Landroid/content/Context;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;)V

    .line 612723
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612712
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 612713
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
