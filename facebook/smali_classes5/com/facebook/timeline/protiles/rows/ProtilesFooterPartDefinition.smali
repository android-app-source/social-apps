.class public Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/timeline/protiles/model/ProtileModel;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/timeline/protiles/views/ProtilesFooterView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1K9;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612968
    new-instance v0, LX/3c3;

    invoke-direct {v0}, LX/3c3;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1K9;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612969
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612970
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->b:LX/1K9;

    .line 612971
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612972
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 612973
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;
    .locals 6

    .prologue
    .line 612957
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    monitor-enter v1

    .line 612958
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612959
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612960
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612961
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612962
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v3

    check-cast v3, LX/1K9;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;-><init>(LX/1K9;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 612963
    move-object v0, p0

    .line 612964
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612965
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612966
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612940
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612952
    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 612953
    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->o()Ljava/lang/String;

    move-result-object v0

    .line 612954
    const v1, 0x7f0d279d

    iget-object v2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612955
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/G23;

    invoke-direct {v2, p0, p2}, LX/G23;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesFooterPartDefinition;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612956
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x626538a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612945
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    const/4 v2, 0x0

    .line 612946
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v1

    .line 612947
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v1, p0, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v1, p0, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 612948
    :goto_0
    iput-boolean v1, p4, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->d:Z

    .line 612949
    if-nez p2, :cond_1

    const/16 v2, 0x8

    :cond_1
    invoke-virtual {p4, v2}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->setFooterVisibility(I)V

    .line 612950
    const/16 v1, 0x1f

    const v2, -0x4228e53b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_2
    move v1, v2

    .line 612951
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612941
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 612942
    if-eqz p1, :cond_0

    .line 612943
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v0, v0

    .line 612944
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
