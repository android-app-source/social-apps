.class public Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G2J;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Pq;",
        "Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1K9;

.field public final c:LX/BQB;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612937
    new-instance v0, LX/3c2;

    invoke-direct {v0}, LX/3c2;-><init>()V

    sput-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1K9;LX/BQB;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612904
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612905
    iput-object p1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->b:LX/1K9;

    .line 612906
    iput-object p2, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->c:LX/BQB;

    .line 612907
    iput-object p3, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612908
    iput-object p4, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->e:LX/0wM;

    .line 612909
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;
    .locals 7

    .prologue
    .line 612926
    const-class v1, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    monitor-enter v1

    .line 612927
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612928
    sput-object v2, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612929
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612930
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612931
    new-instance p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v3

    check-cast v3, LX/1K9;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v4

    check-cast v4, LX/BQB;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;-><init>(LX/1K9;LX/BQB;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;)V

    .line 612932
    move-object v0, p0

    .line 612933
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612934
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612935
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612925
    sget-object v0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612922
    check-cast p2, LX/G2J;

    .line 612923
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/G2I;

    invoke-direct {v1, p0, p2}, LX/G2I;-><init>(Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;LX/G2J;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612924
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->e:LX/0wM;

    const v1, 0x7f02096e

    const v2, -0x25d6d7

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1584c2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612915
    check-cast p1, LX/G2J;

    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    .line 612916
    invoke-virtual {p4, p2}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 612917
    iget-object v1, p1, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 612918
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(Z)V

    .line 612919
    iget-object v1, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->c:LX/BQB;

    const/4 v2, 0x1

    .line 612920
    iput-boolean v2, v1, LX/BQB;->w:Z

    .line 612921
    const/16 v1, 0x1f

    const v2, 0xc2859d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612913
    check-cast p1, LX/G2J;

    .line 612914
    iget-object v0, p1, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/G2J;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 612910
    iget-object v0, p0, Lcom/facebook/timeline/protiles/rows/ProtilesVideoHeaderPartDefinition;->c:LX/BQB;

    const/4 v1, 0x0

    .line 612911
    iput-boolean v1, v0, LX/BQB;->w:Z

    .line 612912
    return-void
.end method
