.class public Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Fti;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/Ftx;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/Ftx;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612252
    new-instance v0, LX/3bq;

    invoke-direct {v0}, LX/3bq;-><init>()V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612214
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612215
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->b:LX/0Ot;

    .line 612216
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 612217
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 612218
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->e:Landroid/content/res/Resources;

    .line 612219
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;
    .locals 7

    .prologue
    .line 612241
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    monitor-enter v1

    .line 612242
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612243
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612244
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612245
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612246
    new-instance v6, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;

    const/16 v3, 0xe0f

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {v6, p0, v3, v4, v5}, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;-><init>(LX/0Ot;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 612247
    move-object v0, v6

    .line 612248
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612249
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612250
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612251
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Ftx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612240
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 612229
    check-cast p2, LX/Fti;

    .line 612230
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 612231
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b228e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 612232
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->e:Landroid/content/res/Resources;

    const v3, 0x7f0b228f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 612233
    iget-object v3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->e:Landroid/content/res/Resources;

    const p3, 0x7f0b2291

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 612234
    new-instance p3, LX/1ds;

    invoke-direct {p3, v1, v2, v1, v3}, LX/1ds;-><init>(IIII)V

    move-object v1, p3

    .line 612235
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612236
    const v0, 0x7f0d1065

    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/Fti;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612237
    iget-object v0, p2, LX/Fti;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 612238
    const v1, 0x7f0d1067

    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    iget-object v2, p2, LX/Fti;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v1, v0, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612239
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1b7d641d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612221
    check-cast p1, LX/Fti;

    check-cast p4, LX/Ftx;

    .line 612222
    iget-object v1, p1, LX/Fti;->b:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 612223
    :goto_0
    if-eqz v1, :cond_1

    .line 612224
    iget-object v2, p4, LX/Ftx;->b:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 612225
    iget-object v2, p4, LX/Ftx;->b:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p4, LX/Ftx;->a:LX/23P;

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 612226
    :goto_1
    const/16 v1, 0x1f

    const v2, -0xb8cf545

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 612227
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 612228
    :cond_1
    iget-object v2, p4, LX/Ftx;->b:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->c()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612220
    const/4 v0, 0x1

    return v0
.end method
