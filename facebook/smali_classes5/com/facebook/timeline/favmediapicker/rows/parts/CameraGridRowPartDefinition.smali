.class public Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/FtY;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final f:LX/Fu1;

.field public final g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612317
    const v0, 0x7f0305f6

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a:LX/1Cz;

    .line 612318
    const-class v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/Fu1;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612319
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612320
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 612321
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612322
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 612323
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->f:LX/Fu1;

    .line 612324
    iput-object p5, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->g:Landroid/content/res/Resources;

    .line 612325
    return-void
.end method

.method private static a(I)I
    .locals 2

    .prologue
    .line 612300
    rem-int/lit8 v0, p0, 0x3

    if-nez v0, :cond_0

    .line 612301
    const v0, 0x7f0d1060

    .line 612302
    :goto_0
    return v0

    .line 612303
    :cond_0
    rem-int/lit8 v0, p0, 0x3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 612304
    const v0, 0x7f0d1061

    goto :goto_0

    .line 612305
    :cond_1
    const v0, 0x7f0d1062

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;
    .locals 9

    .prologue
    .line 612306
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    monitor-enter v1

    .line 612307
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612308
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612309
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612310
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612311
    new-instance v3, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/Fu1;->a(LX/0QB;)LX/Fu1;

    move-result-object v7

    check-cast v7, LX/Fu1;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/Fu1;Landroid/content/res/Resources;)V

    .line 612312
    move-object v0, v3

    .line 612313
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612314
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612315
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612316
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612255
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 612256
    check-cast p2, LX/FtY;

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 612257
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->f:LX/Fu1;

    invoke-virtual {v0}, LX/Fu1;->a()I

    move-result v6

    .line 612258
    iget-object v0, p2, LX/FtY;->a:LX/FtE;

    .line 612259
    iget-object v2, v0, LX/FtE;->a:Ljava/util/List;

    move-object v0, v2

    .line 612260
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 612261
    iget v0, p2, LX/FtY;->b:I

    mul-int/lit8 v5, v0, 0x3

    .line 612262
    add-int/lit8 v0, v5, 0x3

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v4, v5

    .line 612263
    :goto_0
    add-int/lit8 v0, v5, 0x3

    if-ge v4, v0, :cond_4

    .line 612264
    if-ge v4, v8, :cond_0

    iget-object v0, p2, LX/FtY;->a:LX/FtE;

    .line 612265
    iget-object v2, v0, LX/FtE;->a:Ljava/util/List;

    move-object v0, v2

    .line 612266
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 612267
    iget-object v2, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v0, v2

    .line 612268
    :goto_1
    new-instance v2, LX/2f8;

    invoke-direct {v2}, LX/2f8;-><init>()V

    sget-object v9, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 612269
    iput-object v9, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 612270
    move-object v9, v2

    .line 612271
    if-nez v0, :cond_1

    move-object v2, v1

    .line 612272
    :goto_2
    iput-object v2, v9, LX/2f8;->a:LX/1bf;

    .line 612273
    move-object v9, v9

    .line 612274
    if-ge v4, v8, :cond_2

    const v2, 0x7f0a0168

    .line 612275
    :goto_3
    iput v2, v9, LX/2f8;->k:I

    .line 612276
    move-object v2, v9

    .line 612277
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    .line 612278
    invoke-static {v4}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a(I)I

    move-result v9

    iget-object v10, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v9, v10, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612279
    invoke-static {v4}, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a(I)I

    move-result v2

    iget-object v9, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    if-ge v4, v8, :cond_3

    .line 612280
    new-instance v10, LX/FtX;

    invoke-direct {v10, p0, v0}, LX/FtX;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;Landroid/net/Uri;)V

    move-object v0, v10

    .line 612281
    :goto_4
    invoke-interface {p1, v2, v9, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612282
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 612283
    goto :goto_1

    .line 612284
    :cond_1
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    new-instance v10, LX/1o9;

    invoke-direct {v10, v6, v6}, LX/1o9;-><init>(II)V

    .line 612285
    iput-object v10, v2, LX/1bX;->c:LX/1o9;

    .line 612286
    move-object v2, v2

    .line 612287
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    goto :goto_2

    :cond_2
    move v2, v3

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 612288
    goto :goto_4

    .line 612289
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    if-ne v8, v7, :cond_5

    const/4 v3, 0x1

    .line 612290
    :cond_5
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->g:Landroid/content/res/Resources;

    const v4, 0x7f0b2296

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 612291
    if-eqz v3, :cond_6

    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b2298

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 612292
    :goto_5
    new-instance v5, LX/1ds;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6, v4, v2}, LX/1ds;-><init>(IIII)V

    move-object v2, v5

    .line 612293
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612294
    return-object v1

    .line 612295
    :cond_6
    iget-object v2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b2297

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612296
    check-cast p1, LX/FtY;

    .line 612297
    iget-object v0, p1, LX/FtY;->a:LX/FtE;

    .line 612298
    iget-object v1, v0, LX/FtE;->a:Ljava/util/List;

    move-object v0, v1

    .line 612299
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p1, LX/FtY;->b:I

    mul-int/lit8 v1, v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
