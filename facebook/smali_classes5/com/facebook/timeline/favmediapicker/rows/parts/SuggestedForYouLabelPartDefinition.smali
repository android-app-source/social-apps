.class public Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/FtG;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/resources/ui/FbTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/23P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612326
    const v0, 0x7f03142c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612327
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612328
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->b:LX/23P;

    .line 612329
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;
    .locals 4

    .prologue
    .line 612330
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;

    monitor-enter v1

    .line 612331
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612332
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612333
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612334
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612335
    new-instance p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;-><init>(LX/23P;)V

    .line 612336
    move-object v0, p0

    .line 612337
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612338
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612339
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612340
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612341
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x20b012e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612342
    check-cast p4, Lcom/facebook/resources/ui/FbTextView;

    .line 612343
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->b:LX/23P;

    invoke-virtual {p4, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 612344
    const/16 v1, 0x1f

    const v2, -0x77e6545f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612345
    const/4 v0, 0x1

    return v0
.end method
