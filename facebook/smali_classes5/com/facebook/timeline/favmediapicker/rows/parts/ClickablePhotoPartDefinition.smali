.class public Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/Fte;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612183
    new-instance v0, LX/3bo;

    const v1, 0x7f0305f9

    invoke-direct {v0, v1}, LX/3bo;-><init>(I)V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612184
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 612185
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 612186
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612187
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;
    .locals 5

    .prologue
    .line 612172
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    monitor-enter v1

    .line 612173
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612174
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612177
    new-instance p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 612178
    move-object v0, p0

    .line 612179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612167
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612168
    check-cast p2, LX/Fte;

    .line 612169
    const v0, 0x7f0d1063

    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    iget-object v2, p2, LX/Fte;->a:LX/2f9;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612170
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/Fte;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612171
    const/4 v0, 0x0

    return-object v0
.end method
