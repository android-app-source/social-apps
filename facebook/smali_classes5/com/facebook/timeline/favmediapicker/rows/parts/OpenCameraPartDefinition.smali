.class public Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/1b0;

.field public final d:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612190
    new-instance v0, LX/3bp;

    const v1, 0x7f0305f8

    invoke-direct {v0, v1}, LX/3bp;-><init>(I)V

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1b0;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612191
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 612192
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612193
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->c:LX/1b0;

    .line 612194
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->d:LX/0ad;

    .line 612195
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;
    .locals 6

    .prologue
    .line 612196
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    monitor-enter v1

    .line 612197
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612198
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612199
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612200
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612201
    new-instance p0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v4

    check-cast v4, LX/1b0;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1b0;LX/0ad;)V

    .line 612202
    move-object v0, p0

    .line 612203
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612204
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612205
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612207
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 612208
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612209
    new-instance v1, LX/Fth;

    invoke-direct {v1, p0}, LX/Fth;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;)V

    move-object v1, v1

    .line 612210
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612211
    const/4 v0, 0x0

    return-object v0
.end method
