.class public Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Ftk;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field public final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612402
    const v0, 0x7f0305f6

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a:LX/1Cz;

    .line 612403
    const-class v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    const-string v1, "favorite_media_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612396
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612397
    iput-object p1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 612398
    iput-object p2, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612399
    iput-object p3, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 612400
    iput-object p4, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->f:Landroid/content/res/Resources;

    .line 612401
    return-void
.end method

.method private static a(I)I
    .locals 2

    .prologue
    .line 612390
    rem-int/lit8 v0, p0, 0x3

    if-nez v0, :cond_0

    .line 612391
    const v0, 0x7f0d1060

    .line 612392
    :goto_0
    return v0

    .line 612393
    :cond_0
    rem-int/lit8 v0, p0, 0x3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 612394
    const v0, 0x7f0d1061

    goto :goto_0

    .line 612395
    :cond_1
    const v0, 0x7f0d1062

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;
    .locals 7

    .prologue
    .line 612379
    const-class v1, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    monitor-enter v1

    .line 612380
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612381
    sput-object v2, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612382
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612383
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612384
    new-instance p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Landroid/content/res/Resources;)V

    .line 612385
    move-object v0, p0

    .line 612386
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612387
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612388
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612346
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 612349
    check-cast p2, LX/Ftk;

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 612350
    iget-object v0, p2, LX/Ftk;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    .line 612351
    iget v0, p2, LX/Ftk;->b:I

    mul-int/lit8 v5, v0, 0x3

    .line 612352
    add-int/lit8 v0, v5, 0x3

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v7

    move v4, v5

    .line 612353
    :goto_0
    add-int/lit8 v0, v5, 0x3

    if-ge v4, v0, :cond_5

    .line 612354
    if-ge v4, v7, :cond_0

    iget-object v0, p2, LX/Ftk;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 612355
    :goto_1
    if-nez v0, :cond_1

    move-object v1, v2

    .line 612356
    :goto_2
    new-instance v0, LX/2f8;

    invoke-direct {v0}, LX/2f8;-><init>()V

    sget-object v8, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 612357
    iput-object v8, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 612358
    move-object v8, v0

    .line 612359
    if-nez v1, :cond_2

    move-object v0, v2

    :goto_3
    invoke-virtual {v8, v0}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v8

    if-ge v4, v7, :cond_3

    const v0, 0x7f0a0168

    .line 612360
    :goto_4
    iput v0, v8, LX/2f8;->k:I

    .line 612361
    move-object v0, v8

    .line 612362
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    .line 612363
    invoke-static {v4}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a(I)I

    move-result v8

    iget-object v9, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v8, v9, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612364
    invoke-static {v4}, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a(I)I

    move-result v8

    iget-object v9, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    if-ge v4, v7, :cond_4

    iget-object v0, p2, LX/Ftk;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 612365
    new-instance p3, LX/Ftj;

    invoke-direct {p3, p0, v0, v1}, LX/Ftj;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p3

    .line 612366
    :goto_5
    invoke-interface {p1, v8, v9, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 612367
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 612368
    goto :goto_1

    .line 612369
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 612370
    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    :cond_3
    move v0, v3

    goto :goto_4

    :cond_4
    move-object v0, v2

    .line 612371
    goto :goto_5

    .line 612372
    :cond_5
    iget-object v0, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    if-ne v7, v6, :cond_6

    const/4 v3, 0x1

    .line 612373
    :cond_6
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->f:Landroid/content/res/Resources;

    const v4, 0x7f0b2296

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 612374
    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b2298

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 612375
    :goto_6
    new-instance v5, LX/1ds;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6, v4, v1}, LX/1ds;-><init>(IIII)V

    move-object v1, v5

    .line 612376
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612377
    return-object v2

    .line 612378
    :cond_7
    iget-object v1, p0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->f:Landroid/content/res/Resources;

    const v5, 0x7f0b2297

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612347
    check-cast p1, LX/Ftk;

    .line 612348
    iget-object v0, p1, LX/Ftk;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->k()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel$MediaListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget v1, p1, LX/Ftk;->b:I

    mul-int/lit8 v1, v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
