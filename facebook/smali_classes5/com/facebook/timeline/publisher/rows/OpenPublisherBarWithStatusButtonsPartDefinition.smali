.class public Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G2f;",
        "LX/G2g;",
        "LX/Ft9;",
        "Lcom/facebook/feed/inlinecomposer/InlineComposerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/inlinecomposer/InlineComposerView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/G2m;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:LX/0W9;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613092
    new-instance v0, LX/3c5;

    invoke-direct {v0}, LX/3c5;-><init>()V

    sput-object v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/G2m;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0W9;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/G2m;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0W9;",
            "LX/0Or",
            "<",
            "LX/1E8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613085
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613086
    iput-object p1, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->b:LX/0Or;

    .line 613087
    iput-object p2, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    .line 613088
    iput-object p3, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 613089
    iput-object p4, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->e:LX/0W9;

    .line 613090
    iput-object p5, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->f:LX/0Or;

    .line 613091
    return-void
.end method

.method private a(LX/1aD;LX/Ft9;)LX/G2g;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<",
            "LX/Ft9;",
            ">;",
            "LX/Ft9;",
            ")",
            "LX/G2g;"
        }
    .end annotation

    .prologue
    .line 613046
    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->MY_TIMELINE:LX/1Qt;

    if-ne v1, v2, :cond_1

    const/16 v18, 0x1

    .line 613047
    :goto_0
    const v1, 0x7f0d111b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    invoke-virtual {v3}, LX/G2m;->a()Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613048
    const v2, 0x7f0d111c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/G2m;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613049
    const v1, 0x7f0d1123

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    invoke-virtual {v3}, LX/G2m;->a()Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613050
    const v1, 0x7f0d1124

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    invoke-virtual {v3}, LX/G2m;->b()Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613051
    if-eqz v18, :cond_0

    .line 613052
    const v1, 0x7f0d1125

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->c:LX/G2m;

    invoke-virtual {v3}, LX/G2m;->c()Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 613053
    :cond_0
    if-eqz v18, :cond_2

    const v3, 0x7f081405

    .line 613054
    :goto_1
    if-eqz v18, :cond_3

    const v4, 0x7f0813f9

    .line 613055
    :goto_2
    if-eqz v18, :cond_4

    const v5, 0x7f08154d

    .line 613056
    :goto_3
    if-eqz v18, :cond_5

    const v6, 0x7f0814d8

    .line 613057
    :goto_4
    if-eqz v18, :cond_6

    const v7, 0x7f08154f

    .line 613058
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 613059
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 613060
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, LX/1E8;

    .line 613061
    invoke-virtual/range {v17 .. v17}, LX/1E8;->i()Z

    move-result v1

    .line 613062
    if-eqz v1, :cond_8

    const v8, 0x7f02080f

    .line 613063
    :goto_7
    if-eqz v1, :cond_9

    const v9, 0x7f0207b3

    .line 613064
    :goto_8
    if-eqz v1, :cond_a

    const v10, 0x7f02087a

    .line 613065
    :goto_9
    invoke-virtual/range {v17 .. v17}, LX/1E8;->b()Z

    move-result v11

    .line 613066
    if-eqz v11, :cond_b

    invoke-virtual/range {v17 .. v17}, LX/1E8;->d()I

    move-result v12

    .line 613067
    :goto_a
    if-eqz v11, :cond_c

    invoke-virtual/range {v17 .. v17}, LX/1E8;->e()I

    move-result v13

    .line 613068
    :goto_b
    if-eqz v11, :cond_d

    invoke-virtual/range {v17 .. v17}, LX/1E8;->g()I

    move-result v14

    .line 613069
    :goto_c
    invoke-virtual/range {v17 .. v17}, LX/1E8;->m()Z

    move-result v15

    .line 613070
    new-instance v1, LX/G2g;

    if-eqz v15, :cond_e

    invoke-virtual/range {v17 .. v17}, LX/1E8;->n()I

    move-result v16

    :goto_d
    invoke-virtual/range {v17 .. v17}, LX/1E8;->k()Z

    move-result v17

    invoke-interface/range {p2 .. p2}, LX/Ft4;->n()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v1 .. v19}, LX/G2g;-><init>(Landroid/net/Uri;IIIIIIIIZIIIZIZZLjava/lang/String;)V

    return-object v1

    .line 613071
    :cond_1
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 613072
    :cond_2
    const v3, 0x7f08140d

    goto/16 :goto_1

    .line 613073
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 613074
    :cond_4
    const v5, 0x7f08154a

    goto/16 :goto_3

    .line 613075
    :cond_5
    const v6, 0x7f0814da

    goto/16 :goto_4

    .line 613076
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 613077
    :cond_7
    const v1, 0x7f0203b2

    invoke-static {v1}, LX/1bX;->a(I)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v1}, LX/1bf;->b()Landroid/net/Uri;

    move-result-object v2

    goto :goto_6

    .line 613078
    :cond_8
    const/4 v8, 0x0

    goto :goto_7

    .line 613079
    :cond_9
    const/4 v9, 0x0

    goto :goto_8

    .line 613080
    :cond_a
    const v10, 0x7f020dfb

    goto :goto_9

    .line 613081
    :cond_b
    const/4 v12, 0x0

    goto :goto_a

    .line 613082
    :cond_c
    const/4 v13, 0x0

    goto :goto_b

    .line 613083
    :cond_d
    const/4 v14, 0x0

    goto :goto_c

    .line 613084
    :cond_e
    const/16 v16, 0x0

    goto :goto_d
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;
    .locals 9

    .prologue
    .line 613035
    const-class v1, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;

    monitor-enter v1

    .line 613036
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613037
    sput-object v2, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613038
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613039
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613040
    new-instance v3, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/G2m;->a(LX/0QB;)LX/G2m;

    move-result-object v5

    check-cast v5, LX/G2m;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    const/16 v8, 0x64b

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;-><init>(LX/0Or;LX/G2m;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0W9;LX/0Or;)V

    .line 613041
    move-object v0, v3

    .line 613042
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613043
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613044
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613045
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 613034
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->e:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/inlinecomposer/InlineComposerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613033
    sget-object v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 612991
    check-cast p3, LX/Ft9;

    invoke-direct {p0, p1, p3}, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a(LX/1aD;LX/Ft9;)LX/G2g;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70473ba0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612993
    check-cast p2, LX/G2g;

    check-cast p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;

    const/4 v6, 0x0

    .line 612994
    invoke-virtual {p4}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ddd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 612995
    const v2, 0x7f021178

    invoke-virtual {p4, v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->setBackgroundResource(I)V

    .line 612996
    invoke-virtual {p4, v6, v1, v6, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->setPadding(IIII)V

    .line 612997
    iget-object v1, p2, LX/G2g;->a:Landroid/net/Uri;

    .line 612998
    iget-object v2, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 612999
    invoke-virtual {p4}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 613000
    iget v1, p2, LX/G2g;->c:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p2, LX/G2g;->p:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 613001
    iget-object v4, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->g:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613002
    iget-boolean v1, p2, LX/G2g;->j:Z

    if-eqz v1, :cond_0

    .line 613003
    iget v1, p2, LX/G2g;->k:I

    iget v4, p2, LX/G2g;->l:I

    iget v5, p2, LX/G2g;->m:I

    .line 613004
    iget-object p1, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 613005
    iget-object p1, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p1, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 613006
    iget-object p1, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p1, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 613007
    :cond_0
    iget-boolean v1, p2, LX/G2g;->q:Z

    if-eqz v1, :cond_1

    .line 613008
    iget v1, p2, LX/G2g;->r:I

    .line 613009
    iget-object v5, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->e:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    array-length p1, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, p1, :cond_1

    aget-object p3, v5, v4

    .line 613010
    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTextColor(I)V

    .line 613011
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 613012
    :cond_1
    iget-boolean v1, p2, LX/G2g;->n:Z

    if-eqz v1, :cond_4

    iget v1, p2, LX/G2g;->d:I

    invoke-static {p0, v2, v1}, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a(Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    .line 613013
    :goto_1
    iget-object v4, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 613014
    iget v1, p2, LX/G2g;->g:I

    if-eqz v1, :cond_2

    .line 613015
    iget v1, p2, LX/G2g;->g:I

    .line 613016
    iget-object v4, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 613017
    :cond_2
    iget-boolean v1, p2, LX/G2g;->n:Z

    if-eqz v1, :cond_5

    iget v1, p2, LX/G2g;->e:I

    invoke-static {p0, v2, v1}, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a(Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    .line 613018
    :goto_2
    iget-object v4, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 613019
    iget v1, p2, LX/G2g;->h:I

    if-eqz v1, :cond_3

    .line 613020
    iget v1, p2, LX/G2g;->h:I

    .line 613021
    iget-object v4, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 613022
    :cond_3
    iget-boolean v1, p2, LX/G2g;->o:Z

    if-eqz v1, :cond_7

    .line 613023
    iget-boolean v1, p2, LX/G2g;->n:Z

    if-eqz v1, :cond_6

    iget v1, p2, LX/G2g;->f:I

    invoke-static {p0, v2, v1}, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a(Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    .line 613024
    :goto_3
    iget-object v2, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 613025
    invoke-virtual {p4, v6}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->setCheckInButtonVisibility(I)V

    .line 613026
    iget v1, p2, LX/G2g;->i:I

    .line 613027
    iget-object v2, p4, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 613028
    :goto_4
    const/16 v1, 0x1f

    const v2, -0x7d56f3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 613029
    :cond_4
    iget v1, p2, LX/G2g;->d:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 613030
    :cond_5
    iget v1, p2, LX/G2g;->e:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 613031
    :cond_6
    iget v1, p2, LX/G2g;->f:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 613032
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->setCheckInButtonVisibility(I)V

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612992
    const/4 v0, 0x1

    return v0
.end method
