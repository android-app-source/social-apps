.class public Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G52;",
        "LX/FuF;",
        "LX/1Po;",
        "LX/FuX;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/drawable/Drawable;

.field public final d:LX/1K9;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612498
    new-instance v0, LX/3bt;

    invoke-direct {v0}, LX/3bt;-><init>()V

    sput-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1K9;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612492
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612493
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->d:LX/1K9;

    .line 612494
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a0168

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->b:Landroid/graphics/drawable/Drawable;

    .line 612495
    const v0, 0x7f02195c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->c:Landroid/graphics/drawable/Drawable;

    .line 612496
    const v0, 0x7f08157b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->e:Ljava/lang/String;

    .line 612497
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;
    .locals 5

    .prologue
    .line 612481
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    monitor-enter v1

    .line 612482
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612483
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612484
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612485
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612486
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-direct {p0, v3, v4}, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;-><init>(Landroid/content/res/Resources;LX/1K9;)V

    .line 612487
    move-object v0, p0

    .line 612488
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612489
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612490
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612480
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 612468
    check-cast p2, LX/G52;

    check-cast p3, LX/1Po;

    .line 612469
    instance-of v0, p2, LX/G55;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, LX/G55;

    iget-object v0, v0, LX/G55;->c:Ljava/lang/String;

    .line 612470
    :goto_0
    invoke-virtual {p2}, LX/G52;->a()Z

    move-result v3

    .line 612471
    if-nez v3, :cond_1

    .line 612472
    new-instance v1, LX/FuE;

    invoke-direct {v1, p0, p2}, LX/FuE;-><init>(Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;LX/G52;)V

    move-object v1, v1

    .line 612473
    :goto_1
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    .line 612474
    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    sget-object p1, LX/1Qt;->PAGE_TIMELINE:LX/1Qt;

    if-eq v4, p1, :cond_3

    const/4 v4, 0x1

    :goto_2
    move v2, v4

    .line 612475
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->b:Landroid/graphics/drawable/Drawable;

    .line 612476
    :goto_3
    new-instance v4, LX/FuF;

    invoke-direct {v4, v0, v2, v3, v1}, LX/FuF;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V

    return-object v4

    .line 612477
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->e:Ljava/lang/String;

    goto :goto_0

    .line 612478
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 612479
    :cond_2
    iget-object v2, p0, Lcom/facebook/timeline/feed/parts/TimelineScrubberPartDefinition;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x15137341

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612457
    check-cast p2, LX/FuF;

    check-cast p4, LX/FuX;

    .line 612458
    iget-object v1, p2, LX/FuF;->a:Ljava/lang/String;

    .line 612459
    iget-object v2, p4, LX/FuX;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 612460
    iget-boolean v1, p2, LX/FuF;->c:Z

    .line 612461
    iget-object p0, p4, LX/FuX;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 612462
    iget-object v1, p2, LX/FuF;->b:Landroid/graphics/drawable/Drawable;

    .line 612463
    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 612464
    iget v2, p4, LX/FuX;->c:I

    iget p0, p4, LX/FuX;->c:I

    iget p1, p4, LX/FuX;->c:I

    iget p3, p4, LX/FuX;->c:I

    invoke-virtual {p4, v2, p0, p1, p3}, LX/FuX;->setPadding(IIII)V

    .line 612465
    iget-object v1, p2, LX/FuF;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/FuX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612466
    const/16 v1, 0x1f

    const v2, 0x2fc6bea5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 612467
    :cond_0
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612456
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 612453
    check-cast p4, LX/FuX;

    .line 612454
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/FuX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612455
    return-void
.end method
