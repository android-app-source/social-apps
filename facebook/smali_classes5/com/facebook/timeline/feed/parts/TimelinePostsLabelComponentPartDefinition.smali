.class public Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/G54;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static e:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612646
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612644
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 612645
    return-void
.end method

.method private static a(LX/1De;LX/G54;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/G54;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 612629
    iget-object v0, p1, LX/G54;->a:LX/G58;

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    if-ne v0, v1, :cond_1

    const v0, 0x7f081571

    .line 612630
    :goto_0
    const/4 v1, 0x0

    .line 612631
    new-instance v2, LX/FuB;

    invoke-direct {v2}, LX/FuB;-><init>()V

    .line 612632
    sget-object p1, LX/FuC;->b:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/FuA;

    .line 612633
    if-nez p1, :cond_0

    .line 612634
    new-instance p1, LX/FuA;

    invoke-direct {p1}, LX/FuA;-><init>()V

    .line 612635
    :cond_0
    invoke-static {p1, p0, v1, v1, v2}, LX/FuA;->a$redex0(LX/FuA;LX/1De;IILX/FuB;)V

    .line 612636
    move-object v2, p1

    .line 612637
    move-object v1, v2

    .line 612638
    move-object v1, v1

    .line 612639
    iget-object v2, v1, LX/FuA;->a:LX/FuB;

    invoke-virtual {v1, v0}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/FuB;->a:Ljava/lang/String;

    .line 612640
    iget-object v2, v1, LX/FuA;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 612641
    move-object v0, v1

    .line 612642
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 612643
    :cond_1
    const v0, 0x7f081572

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;
    .locals 4

    .prologue
    .line 612612
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;

    monitor-enter v1

    .line 612613
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612614
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612615
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612616
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612617
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 612618
    move-object v0, p0

    .line 612619
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612620
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612621
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 612628
    check-cast p2, LX/G54;

    invoke-static {p1, p2}, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->a(LX/1De;LX/G54;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 612627
    check-cast p2, LX/G54;

    invoke-static {p1, p2}, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->a(LX/1De;LX/G54;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612625
    check-cast p1, LX/G54;

    .line 612626
    iget-object v0, p1, LX/G54;->a:LX/G58;

    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/G54;->a:LX/G58;

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 612624
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 612623
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelinePostsLabelComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
