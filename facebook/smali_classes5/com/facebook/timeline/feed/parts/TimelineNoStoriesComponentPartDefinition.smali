.class public Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/G50;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static e:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612611
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612609
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 612610
    return-void
.end method

.method private static a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 612583
    const/4 v0, 0x0

    .line 612584
    new-instance v1, LX/Fu7;

    invoke-direct {v1}, LX/Fu7;-><init>()V

    .line 612585
    sget-object v2, LX/Fu8;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fu6;

    .line 612586
    if-nez v2, :cond_0

    .line 612587
    new-instance v2, LX/Fu6;

    invoke-direct {v2}, LX/Fu6;-><init>()V

    .line 612588
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Fu6;->a$redex0(LX/Fu6;LX/1De;IILX/Fu7;)V

    .line 612589
    move-object v1, v2

    .line 612590
    move-object v0, v1

    .line 612591
    move-object v0, v0

    .line 612592
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;
    .locals 4

    .prologue
    .line 612598
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;

    monitor-enter v1

    .line 612599
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612600
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612601
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612602
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612603
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 612604
    move-object v0, p0

    .line 612605
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612606
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612607
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 612597
    invoke-static {p1}, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 612596
    invoke-static {p1}, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612595
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 612594
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 612593
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineNoStoriesComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
