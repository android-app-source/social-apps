.class public Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G4z;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/BQB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612555
    new-instance v0, LX/3bv;

    invoke-direct {v0}, LX/3bv;-><init>()V

    sput-object v0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/BQB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612556
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612557
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->b:LX/BQB;

    .line 612558
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;
    .locals 4

    .prologue
    .line 612559
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    monitor-enter v1

    .line 612560
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612561
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612562
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612563
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612564
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v3

    check-cast v3, LX/BQB;

    invoke-direct {p0, v3}, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;-><init>(LX/BQB;)V

    .line 612565
    move-object v0, p0

    .line 612566
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612567
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612568
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612570
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x626c8bce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612571
    check-cast p1, LX/G4z;

    .line 612572
    iget-object v1, p1, LX/G4z;->a:LX/Fso;

    if-nez v1, :cond_0

    .line 612573
    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->b:LX/BQB;

    const/4 v2, 0x1

    sget-object p2, LX/BQA;->INVISIBLE:LX/BQA;

    invoke-virtual {v1, v2, p2}, LX/BQB;->a(ZLX/BQA;)V

    .line 612574
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x129b437e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612575
    check-cast p1, LX/G4z;

    .line 612576
    invoke-virtual {p1}, LX/G4z;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 612577
    check-cast p1, LX/G4z;

    .line 612578
    iget-object v0, p1, LX/G4z;->a:LX/Fso;

    if-nez v0, :cond_0

    .line 612579
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineInvisibleLoadingIndicatorPartDefinition;->b:LX/BQB;

    const/4 v1, 0x0

    sget-object v2, LX/BQA;->INVISIBLE:LX/BQA;

    invoke-virtual {v0, v1, v2}, LX/BQB;->a(ZLX/BQA;)V

    .line 612580
    :cond_0
    return-void
.end method
