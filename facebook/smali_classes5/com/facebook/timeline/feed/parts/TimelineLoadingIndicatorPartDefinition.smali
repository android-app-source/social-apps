.class public Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/G4z;",
        "LX/Fu5;",
        "LX/1Po;",
        "Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/BQB;

.field public final c:LX/1K9;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612501
    new-instance v0, LX/3bu;

    invoke-direct {v0}, LX/3bu;-><init>()V

    sput-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/BQB;LX/1K9;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612502
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612503
    iput-object p1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->b:LX/BQB;

    .line 612504
    iput-object p2, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->c:LX/1K9;

    .line 612505
    const v0, 0x7f0815b5

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->d:Ljava/lang/String;

    .line 612506
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;
    .locals 6

    .prologue
    .line 612507
    const-class v1, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    monitor-enter v1

    .line 612508
    :try_start_0
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612509
    sput-object v2, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612510
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612511
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612512
    new-instance p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v3

    check-cast v3, LX/BQB;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;-><init>(LX/BQB;LX/1K9;Landroid/content/res/Resources;)V

    .line 612513
    move-object v0, p0

    .line 612514
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612515
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612516
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612517
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 612518
    sget-object v0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 612519
    check-cast p2, LX/G4z;

    .line 612520
    new-instance v0, LX/Fu4;

    invoke-direct {v0, p0, p2}, LX/Fu4;-><init>(Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;LX/G4z;)V

    .line 612521
    new-instance v1, LX/Fu5;

    invoke-direct {v1, p2, v0}, LX/Fu5;-><init>(LX/G4z;LX/1DI;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2ee388ed    # -4.200079E10f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612522
    check-cast p2, LX/Fu5;

    check-cast p3, LX/1Po;

    check-cast p4, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 612523
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    .line 612524
    iget-object v2, p2, LX/Fu5;->a:LX/G4z;

    .line 612525
    iget-object p1, v2, LX/G4z;->c:LX/G5A;

    sget-object p3, LX/G5A;->LOADING:LX/G5A;

    if-ne p1, p3, :cond_5

    const/4 p1, 0x1

    :goto_0
    move v2, p1

    .line 612526
    if-eqz v2, :cond_4

    .line 612527
    invoke-virtual {p4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 612528
    iget-object v2, p2, LX/Fu5;->a:LX/G4z;

    iget-object v2, v2, LX/G4z;->a:LX/Fso;

    .line 612529
    if-nez v2, :cond_1

    .line 612530
    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->b:LX/BQB;

    const/4 v2, 0x1

    sget-object p1, LX/BQA;->VISIBLE:LX/BQA;

    invoke-virtual {v1, v2, p1}, LX/BQB;->a(ZLX/BQA;)V

    .line 612531
    :cond_0
    :goto_1
    const/16 v1, 0x1f

    const v2, -0x3a9cf0f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 612532
    :cond_1
    sget-object p1, LX/1Qt;->MY_TIMELINE:LX/1Qt;

    if-eq v1, p1, :cond_2

    sget-object p1, LX/1Qt;->OTHER_PERSON_TIMELINE:LX/1Qt;

    if-ne v1, p1, :cond_0

    .line 612533
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->b:LX/BQB;

    iget-object v2, v2, LX/Fso;->a:Ljava/lang/String;

    .line 612534
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 612535
    iget-object p1, v1, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 612536
    iget-object p1, v1, LX/BQB;->C:Ljava/util/Set;

    invoke-interface {p1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 612537
    iget-object p1, v1, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x1a0015

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p2

    invoke-interface {p1, p0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 612538
    :cond_3
    goto :goto_1

    .line 612539
    :cond_4
    iget-object v1, p2, LX/Fu5;->a:LX/G4z;

    .line 612540
    iget-object v2, v1, LX/G4z;->c:LX/G5A;

    sget-object p1, LX/G5A;->FAILED:LX/G5A;

    if-ne v2, p1, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 612541
    if-eqz v1, :cond_0

    .line 612542
    iget-object v1, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->d:Ljava/lang/String;

    iget-object v2, p2, LX/Fu5;->b:LX/1DI;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_1

    :cond_5
    const/4 p1, 0x0

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612543
    check-cast p1, LX/G4z;

    .line 612544
    invoke-virtual {p1}, LX/G4z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 612545
    check-cast p2, LX/Fu5;

    check-cast p4, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 612546
    invoke-virtual {p4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->e()V

    .line 612547
    iget-object v0, p2, LX/Fu5;->a:LX/G4z;

    iget-object v0, v0, LX/G4z;->a:LX/Fso;

    if-nez v0, :cond_0

    .line 612548
    iget-object v0, p0, Lcom/facebook/timeline/feed/parts/TimelineLoadingIndicatorPartDefinition;->b:LX/BQB;

    const/4 v1, 0x0

    sget-object v2, LX/BQA;->VISIBLE:LX/BQA;

    invoke-virtual {v0, v1, v2}, LX/BQB;->a(ZLX/BQA;)V

    .line 612549
    :cond_0
    return-void
.end method
