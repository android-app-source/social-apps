.class public final Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2945ff3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634148
    const-class v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634147
    const-class v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634145
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634146
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634142
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 634143
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 634144
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 634128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634129
    invoke-direct {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->m()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 634130
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 634131
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 634132
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 634133
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 634134
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 634135
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 634136
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 634137
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 634138
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 634139
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 634140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634141
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 634105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634106
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634107
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 634108
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 634109
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    .line 634110
    iput-object v0, v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 634111
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 634112
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634113
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 634114
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    .line 634115
    iput-object v0, v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634116
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 634117
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634118
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 634119
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    .line 634120
    iput-object v0, v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634121
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 634122
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634123
    invoke-virtual {p0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 634124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    .line 634125
    iput-object v0, v1, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634126
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634127
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 634101
    new-instance v0, LX/8pC;

    invoke-direct {v0, p1}, LX/8pC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634103
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 634104
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 634149
    invoke-virtual {p2}, LX/18L;->a()V

    .line 634150
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 634102
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 634098
    new-instance v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    invoke-direct {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;-><init>()V

    .line 634099
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 634100
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 634097
    const v0, 0x9a39a71

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 634096
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634094
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634095
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634092
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634093
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634090
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 634091
    iget-object v0, p0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
