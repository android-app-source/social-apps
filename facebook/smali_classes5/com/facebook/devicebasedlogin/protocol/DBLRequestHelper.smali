.class public final Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0fW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Di;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/284;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/10O;

.field public l:LX/10M;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568877
    const-class v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    sput-object v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->d:Ljava/lang/Class;

    .line 568878
    const-class v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/10M;LX/10O;)V
    .locals 0
    .param p1    # LX/10M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/10O;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 568873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568874
    iput-object p1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    .line 568875
    iput-object p2, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->k:LX/10O;

    .line 568876
    return-void
.end method

.method private a(ZLcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Ljava/lang/String;Z)LX/0TF;
    .locals 7
    .param p2    # Lcom/facebook/auth/credentials/DBLFacebookCredentials;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 568872
    new-instance v0, LX/GbN;

    move-object v1, p0

    move-object v2, p2

    move v3, p5

    move-object v4, p3

    move v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/GbN;-><init>(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Lcom/facebook/auth/credentials/DBLFacebookCredentials;ZLjava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Z)V
    .locals 6

    .prologue
    .line 568866
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 568867
    const-string v0, "account_id"

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568868
    const-string v0, "nonce"

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568869
    const-string v0, "end_persisted"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568870
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "remove_nonce"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x61a8bb7e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 568871
    return-void
.end method

.method public static a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 568798
    invoke-virtual {p0, p1}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/Boolean;)V

    .line 568799
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568800
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568801
    const-string v1, "has_pin"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568802
    :goto_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 568803
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568804
    :cond_0
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->k:LX/10O;

    const-string v2, "dbl_nux_save_placeholder"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 568805
    return-void

    .line 568806
    :cond_1
    const-string v1, "has_pin"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 568856
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/10M;->a(Ljava/lang/String;)V

    .line 568857
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a:LX/0fW;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fW;->c(Ljava/lang/String;)V

    .line 568858
    iget-object v10, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    new-instance v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mTime:I

    iget-object v3, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUsername:Ljava/lang/String;

    iget-object v6, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    const-string v7, "password_account"

    const/4 v8, 0x0

    iget-object v9, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v10, v0}, LX/10M;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 568859
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568860
    const-string v1, "switch_to"

    const-string v2, "password_account"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568861
    const-string v1, "fbid"

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568862
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 568863
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568864
    :cond_0
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->k:LX/10O;

    const-string v2, "switch_account"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 568865
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Z)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 568840
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 568841
    invoke-static {p0, p1, p3}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a$redex0(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Z)V

    .line 568842
    :cond_0
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v1, "password_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 568843
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/10M;->d(Ljava/lang/String;)V

    .line 568844
    :goto_0
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->j:LX/284;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/284;->a(Ljava/lang/String;)V

    .line 568845
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568846
    const-string v1, "fbid"

    iget-object p3, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568847
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 568848
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568849
    :cond_1
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string p3, "password_account"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568850
    const-string v1, "account_type"

    const-string p3, "password_account"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568851
    :goto_1
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->k:LX/10O;

    const-string p3, "dbl_settings_account_remove"

    invoke-virtual {v1, p3, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 568852
    return-void

    .line 568853
    :cond_2
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/10M;->a(Ljava/lang/String;)V

    .line 568854
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a:LX/0fW;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fW;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 568855
    :cond_3
    const-string v1, "account_type"

    const-string p3, "dbl"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    .line 568829
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 568830
    new-instance v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 568831
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 568832
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    .line 568833
    iget-object v5, v4, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v4, v5

    .line 568834
    if-nez v4, :cond_1

    const-string v4, ""

    :goto_0
    iget-object v5, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/User;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    invoke-virtual {v6}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    const-string v9, ""

    invoke-direct/range {v0 .. v9}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 568835
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->l:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 568836
    :cond_0
    return-void

    .line 568837
    :cond_1
    iget-object v4, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    .line 568838
    iget-object v5, v4, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v4, v5

    .line 568839
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 568822
    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a:LX/0fW;

    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 568823
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 568824
    invoke-virtual {v1, v0}, LX/0fW;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568825
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->c:LX/2Di;

    const/4 v1, 0x1

    .line 568826
    invoke-static {v0, v1, v1, v1}, LX/2Di;->a(LX/2Di;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 568827
    new-instance v1, LX/GbO;

    invoke-direct {v1, p0, p1}, LX/GbO;-><init>(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 568828
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V
    .locals 8
    .param p2    # Lcom/facebook/auth/credentials/DBLFacebookCredentials;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0TF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 568807
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 568808
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 568809
    const-string v0, "machine_id"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568810
    const-string v0, "pin"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568811
    const-string v1, "nonce_to_keep"

    if-nez p2, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568812
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "set_nonce"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x5ac8aa20

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 568813
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    .line 568814
    const/4 v1, 0x0

    .line 568815
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 568816
    const/4 v1, 0x1

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, v6

    move v5, p5

    .line 568817
    invoke-direct/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(ZLcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Ljava/lang/String;Z)LX/0TF;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 568818
    if-eqz p3, :cond_1

    .line 568819
    iget-object v0, p0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, p3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 568820
    :cond_1
    return-void

    .line 568821
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
