.class public Lcom/facebook/resources/ui/FbAutoFitTextView;
.super Landroid/widget/TextView;
.source ""


# instance fields
.field private a:Landroid/graphics/Rect;

.field private b:Landroid/text/TextPaint;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 804031
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 804032
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->a()V

    .line 804033
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 804028
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 804029
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->a()V

    .line 804030
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 804025
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 804026
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->a()V

    .line 804027
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 804020
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    .line 804021
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->b:Landroid/text/TextPaint;

    .line 804022
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->c:I

    .line 804023
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getTextSize()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->d:I

    .line 804024
    return-void
.end method

.method private a(I)Z
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 804011
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 804012
    :cond_0
    :goto_0
    return v7

    .line 804013
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 804014
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->b:Landroid/text/TextPaint;

    int-to-float v2, p1

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 804015
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->b:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getLineSpacingMultiplier()F

    move-result v5

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getLineSpacingExtra()F

    move-result v6

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 804016
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getMaxLines()I

    move-result v2

    if-le v1, v2, :cond_2

    move v7, v8

    .line 804017
    goto :goto_0

    .line 804018
    :cond_2
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-le v0, v1, :cond_0

    move v7, v8

    .line 804019
    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 803983
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 803984
    :cond_0
    :goto_0
    return-void

    .line 803985
    :cond_1
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getCompoundPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getCompoundPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 803986
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getCompoundPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getCompoundPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 803987
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_0

    .line 803988
    iget v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->d:I

    :goto_1
    iget v1, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->c:I

    if-lt v0, v1, :cond_3

    .line 803989
    invoke-direct {p0, v0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 803990
    int-to-float v0, v0

    invoke-super {p0, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 803991
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 803992
    :cond_3
    iget v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->c:I

    int-to-float v0, v0

    invoke-super {p0, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method


# virtual methods
.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x172b383e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 804007
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    .line 804008
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 804009
    :cond_0
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->b()V

    .line 804010
    :cond_1
    const/16 v1, 0x2d

    const v2, 0x15b850d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 804004
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 804005
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->b()V

    .line 804006
    return-void
.end method

.method public setMinTextSizeSp(F)V
    .locals 1

    .prologue
    .line 804002
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/0tP;->b(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->c:I

    .line 804003
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .prologue
    .line 803999
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 804000
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->b()V

    .line 804001
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 803996
    float-to-int v0, p1

    iput v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->d:I

    .line 803997
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->b()V

    .line 803998
    return-void
.end method

.method public final setTextSize(IF)V
    .locals 1

    .prologue
    .line 803993
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {p1, p2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/resources/ui/FbAutoFitTextView;->d:I

    .line 803994
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoFitTextView;->b()V

    .line 803995
    return-void
.end method
