.class public Lcom/facebook/resources/ui/FbAutoUnFocusEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 804055
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 804056
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 804052
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 804053
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;ZI)V

    .line 804054
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 804049
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 804050
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;ZI)V

    .line 804051
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;ZI)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 804057
    if-eqz p3, :cond_0

    sget-object v0, LX/03r;->FbAutoUnFocusEditText:[I

    invoke-virtual {p1, p2, v0, p4, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 804058
    :goto_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->b:Z

    .line 804059
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 804060
    return-void

    .line 804061
    :cond_0
    sget-object v0, LX/03r;->FbAutoUnFocusEditText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x796c80f4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 804046
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onAttachedToWindow()V

    .line 804047
    invoke-virtual {p0, p0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 804048
    const/16 v1, 0x2d

    const v2, 0x79987cbc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x52d138a7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 804043
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 804044
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onDetachedFromWindow()V

    .line 804045
    const/16 v1, 0x2d

    const v2, -0x2cca5aeb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 804034
    if-ne p1, p0, :cond_0

    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 804035
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->clearFocus()V

    .line 804036
    iget-boolean v0, p0, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->b:Z

    if-eqz v0, :cond_0

    .line 804037
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 804038
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 804039
    :cond_0
    return v2
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 804040
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    .line 804041
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbAutoUnFocusEditText;->clearFocus()V

    .line 804042
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
