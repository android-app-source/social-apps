.class public Lcom/facebook/resources/ui/DigitEditText;
.super LX/0zk;
.source ""


# instance fields
.field public a:LX/0jk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/4lG;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 803904
    invoke-direct {p0, p1}, LX/0zk;-><init>(Landroid/content/Context;)V

    .line 803905
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/resources/ui/DigitEditText;->c:I

    .line 803906
    invoke-direct {p0}, Lcom/facebook/resources/ui/DigitEditText;->a()V

    .line 803907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 803921
    invoke-direct {p0, p1, p2}, LX/0zk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 803922
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/resources/ui/DigitEditText;->c:I

    .line 803923
    invoke-direct {p0}, Lcom/facebook/resources/ui/DigitEditText;->a()V

    .line 803924
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 803917
    invoke-direct {p0, p1, p2, p3}, LX/0zk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 803918
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/resources/ui/DigitEditText;->c:I

    .line 803919
    invoke-direct {p0}, Lcom/facebook/resources/ui/DigitEditText;->a()V

    .line 803920
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 803913
    const-class v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-static {v0, p0}, Lcom/facebook/resources/ui/DigitEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 803914
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    .line 803915
    iget-object v0, p0, Lcom/facebook/resources/ui/DigitEditText;->a:LX/0jk;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/DigitEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 803916
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/resources/ui/DigitEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-static {v0}, LX/0jk;->a(LX/0QB;)LX/0jk;

    move-result-object v0

    check-cast v0, LX/0jk;

    iput-object v0, p0, Lcom/facebook/resources/ui/DigitEditText;->a:LX/0jk;

    return-void
.end method


# virtual methods
.method public final onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 803910
    new-instance v0, LX/4lH;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, LX/4lH;-><init>(Lcom/facebook/resources/ui/DigitEditText;Lcom/facebook/resources/ui/DigitEditText;Z)V

    .line 803911
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 803912
    return-object v0
.end method

.method public setDigitEditTextDeletionListener(LX/4lG;)V
    .locals 0

    .prologue
    .line 803908
    iput-object p1, p0, Lcom/facebook/resources/ui/DigitEditText;->b:LX/4lG;

    .line 803909
    return-void
.end method
