.class public Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;
.super Lcom/facebook/resources/ui/EllipsizingTextView;
.source ""


# instance fields
.field private a:I

.field public b:LX/4lJ;

.field private c:Z

.field private d:I

.field private e:I

.field private f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/4lK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 803976
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/EllipsizingTextView;-><init>(Landroid/content/Context;)V

    .line 803977
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 803978
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 803973
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/EllipsizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 803974
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 803975
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 803970
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/EllipsizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 803971
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 803972
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 803960
    sget-object v0, LX/03r;->ExpandingEllipsizingTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 803961
    const/16 v1, 0x1

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->d:I

    .line 803962
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->c:Z

    .line 803963
    const/16 v1, 0x2

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->e:I

    .line 803964
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 803965
    sget-object v0, LX/4lJ;->COLLAPSED:LX/4lJ;

    iput-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    .line 803966
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    .line 803967
    invoke-virtual {p0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->getMaxLines()I

    move-result v0

    iput v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a:I

    .line 803968
    new-instance v0, LX/4lI;

    invoke-direct {v0, p0}, LX/4lI;-><init>(Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;)V

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 803969
    return-void
.end method

.method public static a$redex0(Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;Z)V
    .locals 2

    .prologue
    .line 803954
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    sget-object v1, LX/4lJ;->EXPANDED:LX/4lJ;

    if-ne v0, v1, :cond_0

    .line 803955
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->c(Z)V

    .line 803956
    sget-object v0, LX/4lJ;->COLLAPSED:LX/4lJ;

    iput-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    .line 803957
    :goto_0
    return-void

    .line 803958
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b(Z)V

    .line 803959
    sget-object v0, LX/4lJ;->EXPANDED:LX/4lJ;

    iput-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 803979
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setMaxLines(I)V

    .line 803980
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 803981
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4lK;

    invoke-interface {v0}, LX/4lK;->a()V

    .line 803982
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 5

    .prologue
    .line 803942
    iget-boolean v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->c:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 803943
    :cond_0
    iget v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setMaxLines(I)V

    .line 803944
    :goto_0
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 803945
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4lK;

    invoke-interface {v0}, LX/4lK;->b()V

    .line 803946
    :cond_1
    return-void

    .line 803947
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->getLineCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setMaxLines(I)V

    .line 803948
    invoke-virtual {p0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a:I

    sub-int/2addr v0, v1

    .line 803949
    if-lez v0, :cond_3

    .line 803950
    const-string v1, "maxLines"

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a:I

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 803951
    iget v2, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->d:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->e:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 803952
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 803953
    :cond_3
    iget v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setMaxLines(I)V

    goto :goto_0
.end method


# virtual methods
.method public getExpandState()LX/4lJ;
    .locals 1

    .prologue
    .line 803941
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    return-object v0
.end method

.method public setExpandState(LX/4lJ;)V
    .locals 1

    .prologue
    .line 803938
    iget-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    if-ne v0, p1, :cond_0

    .line 803939
    :goto_0
    return-void

    .line 803940
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->a$redex0(Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;Z)V

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 803937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t override the onClickListener for this viewTry using EllipsizingTextView instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnExpandedStateChangeListener(LX/4lK;)V
    .locals 1
    .param p1    # LX/4lK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 803935
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->f:LX/0am;

    .line 803936
    return-void
.end method
