.class public Lcom/facebook/resources/ui/FbAutoCompleteTextView;
.super LX/2C8;
.source ""


# instance fields
.field public a:LX/0jk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 569586
    invoke-direct {p0, p1}, LX/2C8;-><init>(Landroid/content/Context;)V

    .line 569587
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 569580
    invoke-direct {p0, p1, p2}, LX/2C8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 569581
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a()V

    .line 569582
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 569583
    invoke-direct {p0, p1, p2, p3}, LX/2C8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 569584
    invoke-direct {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a()V

    .line 569585
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 569588
    const-class v0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-static {v0, p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 569589
    iget-object v0, p0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a:LX/0jk;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 569590
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-static {v0}, LX/0jk;->a(LX/0QB;)LX/0jk;

    move-result-object v0

    check-cast v0, LX/0jk;

    iput-object v0, p0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->a:LX/0jk;

    return-void
.end method
