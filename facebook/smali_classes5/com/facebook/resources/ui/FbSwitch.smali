.class public Lcom/facebook/resources/ui/FbSwitch;
.super Landroid/widget/Switch;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 804139
    invoke-direct {p0, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    .line 804140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 804141
    invoke-direct {p0, p1, p2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 804142
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbSwitch;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 804143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 804144
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 804145
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbSwitch;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 804146
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 804147
    sget-object v0, LX/03r;->FbSwitch:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 804148
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804149
    if-eqz v1, :cond_0

    .line 804150
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 804151
    :cond_0
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804152
    if-eqz v1, :cond_1

    .line 804153
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setHint(Ljava/lang/CharSequence;)V

    .line 804154
    :cond_1
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804155
    if-eqz v1, :cond_2

    .line 804156
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbSwitch;->getImeActionId()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/resources/ui/FbSwitch;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 804157
    :cond_2
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804158
    if-eqz v1, :cond_3

    .line 804159
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setText(Ljava/lang/CharSequence;)V

    .line 804160
    :cond_3
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804161
    if-eqz v1, :cond_4

    .line 804162
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setTextOff(Ljava/lang/CharSequence;)V

    .line 804163
    :cond_4
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 804164
    if-eqz v1, :cond_5

    .line 804165
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbSwitch;->setTextOn(Ljava/lang/CharSequence;)V

    .line 804166
    :cond_5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 804167
    return-void
.end method
