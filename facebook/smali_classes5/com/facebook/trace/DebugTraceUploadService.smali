.class public Lcom/facebook/trace/DebugTraceUploadService;
.super LX/1ZN;
.source ""


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0VP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/4mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0YI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0YC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0WV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 805571
    const-class v0, Lcom/facebook/trace/DebugTraceUploadService;

    sput-object v0, Lcom/facebook/trace/DebugTraceUploadService;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 805569
    const-string v0, "DebugTraceUploadService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 805570
    return-void
.end method

.method private a(Ljava/io/File;)LX/4mG;
    .locals 6

    .prologue
    .line 805568
    new-instance v0, LX/4mG;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/trace/DebugTraceUploadService;->g:LX/0WV;

    invoke-virtual {v1}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/trace/DebugTraceUploadService;->f:LX/0YC;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/4mG;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0YC;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/trace/DebugTraceUploadService;LX/0VP;LX/11H;LX/4mF;LX/0Ot;LX/0YI;LX/0YC;LX/0WV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/trace/DebugTraceUploadService;",
            "LX/0VP;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/4mF;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0YI;",
            "LX/0YC;",
            "LX/0WV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 805563
    iput-object p1, p0, Lcom/facebook/trace/DebugTraceUploadService;->a:LX/0VP;

    iput-object p2, p0, Lcom/facebook/trace/DebugTraceUploadService;->b:LX/11H;

    iput-object p3, p0, Lcom/facebook/trace/DebugTraceUploadService;->c:LX/4mF;

    iput-object p4, p0, Lcom/facebook/trace/DebugTraceUploadService;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/trace/DebugTraceUploadService;->e:LX/0YI;

    iput-object p6, p0, Lcom/facebook/trace/DebugTraceUploadService;->f:LX/0YC;

    iput-object p7, p0, Lcom/facebook/trace/DebugTraceUploadService;->g:LX/0WV;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/trace/DebugTraceUploadService;

    invoke-static {v7}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v1

    check-cast v1, LX/0VP;

    invoke-static {v7}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    new-instance v3, LX/4mF;

    invoke-direct {v3}, LX/4mF;-><init>()V

    move-object v3, v3

    move-object v3, v3

    check-cast v3, LX/4mF;

    const/16 v4, 0x2ca

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v7}, LX/0YI;->a(LX/0QB;)LX/0YI;

    move-result-object v5

    check-cast v5, LX/0YI;

    invoke-static {v7}, LX/0YC;->a(LX/0QB;)LX/0YC;

    move-result-object v6

    check-cast v6, LX/0YC;

    invoke-static {v7}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v7

    check-cast v7, LX/0WV;

    invoke-static/range {v0 .. v7}, Lcom/facebook/trace/DebugTraceUploadService;->a(Lcom/facebook/trace/DebugTraceUploadService;LX/0VP;LX/11H;LX/4mF;LX/0Ot;LX/0YI;LX/0YC;LX/0WV;)V

    return-void
.end method

.method private a([Ljava/io/File;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 805572
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, p1, v1

    .line 805573
    iget-object v0, p0, Lcom/facebook/trace/DebugTraceUploadService;->e:LX/0YI;

    invoke-virtual {v0, v4}, LX/0YI;->c(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 805574
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    .line 805575
    invoke-direct {p0, v4}, Lcom/facebook/trace/DebugTraceUploadService;->b(Ljava/io/File;)V

    .line 805576
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 805577
    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".gz"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 805578
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 805579
    invoke-static {v4, v0}, LX/0VP;->a(Ljava/io/File;Ljava/io/File;)V

    .line 805580
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 805581
    invoke-direct {p0, v4}, Lcom/facebook/trace/DebugTraceUploadService;->b(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 805582
    :catch_0
    move-exception v0

    .line 805583
    sget-object v5, Lcom/facebook/trace/DebugTraceUploadService;->h:Ljava/lang/Class;

    const-string v6, "Error: failed to compress %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v2

    invoke-static {v5, v0, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 805584
    :cond_2
    return-void
.end method

.method private b(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 805564
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 805565
    iget-object v0, p0, Lcom/facebook/trace/DebugTraceUploadService;->e:LX/0YI;

    invoke-virtual {v0, p1}, LX/0YI;->a(Ljava/io/File;)V

    .line 805566
    sget-object v0, Lcom/facebook/trace/DebugTraceUploadService;->h:Ljava/lang/Class;

    const-string v1, "Error: failed to delete traceFile %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 805567
    :cond_0
    return-void
.end method

.method private b([Ljava/io/File;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 805549
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p1, v1

    .line 805550
    iget-object v0, p0, Lcom/facebook/trace/DebugTraceUploadService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 805551
    :cond_0
    return-void

    .line 805552
    :cond_1
    iget-object v0, p0, Lcom/facebook/trace/DebugTraceUploadService;->e:LX/0YI;

    invoke-virtual {v0, v4}, LX/0YI;->c(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 805553
    invoke-direct {p0, v4}, Lcom/facebook/trace/DebugTraceUploadService;->a(Ljava/io/File;)LX/4mG;

    move-result-object v0

    .line 805554
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 805555
    iget-object v5, p0, Lcom/facebook/trace/DebugTraceUploadService;->b:LX/11H;

    iget-object v6, p0, Lcom/facebook/trace/DebugTraceUploadService;->c:LX/4mF;

    invoke-virtual {v5, v6, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 805556
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 805557
    if-eqz v0, :cond_3

    .line 805558
    invoke-direct {p0, v4}, Lcom/facebook/trace/DebugTraceUploadService;->b(Ljava/io/File;)V

    .line 805559
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 805560
    :cond_3
    sget-object v0, Lcom/facebook/trace/DebugTraceUploadService;->h:Ljava/lang/Class;

    const-string v5, "Error: failed to upload file %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 805561
    :catch_0
    move-exception v0

    .line 805562
    sget-object v5, Lcom/facebook/trace/DebugTraceUploadService;->h:Ljava/lang/Class;

    const-string v6, "Error: failed to upload file %s"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v2

    invoke-static {v5, v0, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x75c09af0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 805542
    iget-object v1, p0, Lcom/facebook/trace/DebugTraceUploadService;->f:LX/0YC;

    invoke-virtual {v1}, LX/0YC;->a()[Ljava/io/File;

    move-result-object v1

    .line 805543
    if-eqz v1, :cond_0

    .line 805544
    invoke-direct {p0, v1}, Lcom/facebook/trace/DebugTraceUploadService;->a([Ljava/io/File;)V

    .line 805545
    :cond_0
    iget-object v1, p0, Lcom/facebook/trace/DebugTraceUploadService;->f:LX/0YC;

    invoke-virtual {v1}, LX/0YC;->b()[Ljava/io/File;

    move-result-object v1

    .line 805546
    if-eqz v1, :cond_1

    .line 805547
    invoke-direct {p0, v1}, Lcom/facebook/trace/DebugTraceUploadService;->b([Ljava/io/File;)V

    .line 805548
    :cond_1
    const/16 v1, 0x25

    const v2, -0xa77dd7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5dc53fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 805539
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 805540
    invoke-static {p0, p0}, Lcom/facebook/trace/DebugTraceUploadService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 805541
    const/16 v1, 0x25

    const v2, 0x724a04c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
