.class public Lcom/facebook/widget/CustomProgressBar;
.super Landroid/widget/ProgressBar;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/LayerDrawable;

.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 807908
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 807909
    invoke-direct {p0}, Lcom/facebook/widget/CustomProgressBar;->b()V

    .line 807910
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 807902
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807903
    invoke-direct {p0}, Lcom/facebook/widget/CustomProgressBar;->b()V

    .line 807904
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 807905
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807906
    invoke-direct {p0}, Lcom/facebook/widget/CustomProgressBar;->b()V

    .line 807907
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 807911
    iget-object v0, p0, Lcom/facebook/widget/CustomProgressBar;->a:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/facebook/widget/CustomProgressBar;->a:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/LayerDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 807912
    invoke-virtual {p0}, Lcom/facebook/widget/CustomProgressBar;->getProgress()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/widget/CustomProgressBar;->getMax()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 807913
    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 807914
    iget-object v1, p0, Lcom/facebook/widget/CustomProgressBar;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 807915
    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 807916
    iget-object v0, p0, Lcom/facebook/widget/CustomProgressBar;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 807917
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 807890
    invoke-virtual {p0}, Lcom/facebook/widget/CustomProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 807891
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iput-object v0, p0, Lcom/facebook/widget/CustomProgressBar;->a:Landroid/graphics/drawable/LayerDrawable;

    .line 807892
    iget-object v0, p0, Lcom/facebook/widget/CustomProgressBar;->a:Landroid/graphics/drawable/LayerDrawable;

    const v1, 0x7f0d054d

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CustomProgressBar;->b:Landroid/graphics/drawable/Drawable;

    .line 807893
    return-void
.end method


# virtual methods
.method public final declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 807894
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/facebook/widget/CustomProgressBar;->a()V

    .line 807895
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807896
    monitor-exit p0

    return-void

    .line 807897
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProgress(I)V
    .locals 1

    .prologue
    .line 807898
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 807899
    invoke-virtual {p0}, Lcom/facebook/widget/CustomProgressBar;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807900
    monitor-exit p0

    return-void

    .line 807901
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
