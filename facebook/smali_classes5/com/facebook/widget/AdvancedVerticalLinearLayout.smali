.class public Lcom/facebook/widget/AdvancedVerticalLinearLayout;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 807357
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 807358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    .line 807359
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 807355
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807356
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 807348
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807349
    iput-boolean v2, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    .line 807350
    sget-object v0, LX/03r;->AdvancedVerticalLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 807351
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->a:I

    .line 807352
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->d:I

    .line 807353
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 807354
    return-void
.end method

.method private static a()LX/4ng;
    .locals 3

    .prologue
    .line 807347
    new-instance v0, LX/4ng;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/4ng;-><init>(II)V

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)LX/4ng;
    .locals 2

    .prologue
    .line 807346
    new-instance v0, LX/4ng;

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4ng;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/4ng;
    .locals 1

    .prologue
    .line 807261
    new-instance v0, LX/4ng;

    invoke-direct {v0, p0}, LX/4ng;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 807345
    instance-of v0, p1, LX/4ng;

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 807344
    invoke-static {}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->a()LX/4ng;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 807343
    invoke-direct {p0, p1}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->a(Landroid/util/AttributeSet;)LX/4ng;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 807342
    invoke-static {p1}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/4ng;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 807316
    iget v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->d:I

    and-int/lit8 v0, v0, 0x70

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 807317
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingTop()I

    move-result v0

    sub-int v1, p5, p3

    iget v2, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->c:I

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 807318
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingLeft()I

    move-result v5

    .line 807319
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingRight()I

    move-result v1

    .line 807320
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    .line 807321
    sub-int/2addr v2, v5

    sub-int v6, v2, v1

    .line 807322
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildCount()I

    move-result v7

    .line 807323
    const/4 v1, 0x0

    move v4, v1

    move v2, v0

    :goto_1
    if-ge v4, v7, :cond_4

    .line 807324
    invoke-virtual {p0, v4}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 807325
    if-eqz v8, :cond_0

    .line 807326
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 807327
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/4ng;

    .line 807328
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 807329
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 807330
    sub-int v3, v6, v9

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v5

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v10

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v10, v3, v10

    .line 807331
    iget-boolean v3, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, LX/4ng;->a:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    .line 807332
    :goto_2
    if-eqz v3, :cond_3

    .line 807333
    const/4 v1, 0x0

    .line 807334
    :goto_3
    add-int/2addr v9, v10

    add-int v11, v2, v1

    invoke-virtual {v8, v10, v2, v9, v11}, Landroid/view/View;->layout(IIII)V

    .line 807335
    if-nez v3, :cond_0

    .line 807336
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 807337
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 807338
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingTop()I

    move-result v0

    goto :goto_0

    .line 807339
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 807340
    :cond_3
    iget v11, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v12, v0, LX/4ng;->b:I

    sub-int/2addr v11, v12

    add-int/2addr v2, v11

    goto :goto_3

    .line 807341
    :cond_4
    return-void
.end method

.method public final onMeasure(II)V
    .locals 13

    .prologue
    .line 807262
    const/4 v8, 0x0

    .line 807263
    const/4 v7, 0x0

    .line 807264
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildCount()I

    move-result v10

    .line 807265
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 807266
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 807267
    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 807268
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 807269
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 807270
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 807271
    iget v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->a:I

    if-gt v11, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    .line 807272
    const/4 v0, 0x0

    move v9, v0

    :goto_3
    if-ge v9, v10, :cond_6

    .line 807273
    invoke-virtual {p0, v9}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 807274
    if-eqz v1, :cond_e

    .line 807275
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_e

    .line 807276
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/4ng;

    .line 807277
    iget-boolean v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    if-eqz v0, :cond_3

    iget-boolean v0, v6, LX/4ng;->a:Z

    if-eqz v0, :cond_3

    .line 807278
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    move v0, v7

    move v1, v8

    .line 807279
    :goto_4
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v7, v0

    move v8, v1

    goto :goto_3

    .line 807280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 807281
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 807282
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 807283
    :cond_3
    iget v0, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    add-float/2addr v8, v0

    .line 807284
    iget v0, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-nez v0, :cond_4

    iget v0, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    .line 807285
    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v7

    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v8

    goto :goto_4

    .line 807286
    :cond_4
    const/4 v3, 0x0

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_5

    iget v0, v6, LX/4ng;->b:I

    sub-int v5, v7, v0

    :goto_5
    move-object v0, p0

    move v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 807287
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 807288
    add-int/2addr v0, v7

    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    iget v1, v6, LX/4ng;->b:I

    sub-int/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v8

    goto :goto_4

    .line 807289
    :cond_5
    const/4 v5, 0x0

    goto :goto_5

    .line 807290
    :cond_6
    iput v7, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->c:I

    .line 807291
    iget v0, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->c:I

    sub-int v0, v11, v0

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingBottom()I

    move-result v1

    sub-int v2, v0, v1

    .line 807292
    if-eqz v2, :cond_c

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-lez v0, :cond_c

    .line 807293
    const/4 v4, 0x0

    .line 807294
    const/4 v0, 0x0

    move v7, v0

    move v5, v8

    :goto_6
    if-ge v7, v10, :cond_b

    .line 807295
    invoke-virtual {p0, v7}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 807296
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_d

    .line 807297
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/4ng;

    .line 807298
    iget-boolean v1, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->b:Z

    if-eqz v1, :cond_7

    iget-boolean v1, v0, LX/4ng;->a:Z

    if-nez v1, :cond_d

    .line 807299
    :cond_7
    iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 807300
    const/4 v1, 0x0

    cmpl-float v1, v6, v1

    if-lez v1, :cond_d

    .line 807301
    int-to-float v1, v2

    mul-float/2addr v1, v6

    div-float/2addr v1, v5

    float-to-int v1, v1

    .line 807302
    sub-float v6, v5, v6

    .line 807303
    sub-int v5, v2, v1

    .line 807304
    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v2, v8

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v8

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v8

    iget v8, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v2, v8}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->getChildMeasureSpec(III)I

    move-result v2

    .line 807305
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eqz v0, :cond_9

    .line 807306
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 807307
    if-gez v0, :cond_8

    .line 807308
    const/4 v0, 0x0

    :cond_8
    move v1, v2

    move-object v2, v3

    .line 807309
    :goto_7
    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 807310
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v4

    move v1, v5

    move v2, v6

    .line 807311
    :goto_8
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v4, v0

    move v5, v2

    move v2, v1

    goto :goto_6

    .line 807312
    :cond_9
    if-lez v1, :cond_a

    move v0, v1

    move v1, v2

    move-object v2, v3

    goto :goto_7

    :cond_a
    const/4 v0, 0x0

    move v1, v2

    move-object v2, v3

    goto :goto_7

    .line 807313
    :cond_b
    iput v4, p0, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->c:I

    .line 807314
    :cond_c
    invoke-virtual {p0, v12, v11}, Lcom/facebook/widget/AdvancedVerticalLinearLayout;->setMeasuredDimension(II)V

    .line 807315
    return-void

    :cond_d
    move v0, v4

    move v1, v2

    move v2, v5

    goto :goto_8

    :cond_e
    move v0, v7

    move v1, v8

    goto/16 :goto_4
.end method
