.class public Lcom/facebook/widget/SwitchCompat;
.super Landroid/widget/CompoundButton;
.source ""


# static fields
.field private static final G:[I

.field private static final a:[I


# instance fields
.field private A:Landroid/text/Layout;

.field private B:Landroid/text/Layout;

.field private C:Landroid/text/method/TransformationMethod;

.field private D:Landroid/view/animation/Animation;

.field private final E:Landroid/graphics/Rect;

.field private final F:LX/3wA;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:Z

.field private k:I

.field private l:I

.field private m:F

.field private n:F

.field private o:Landroid/view/VelocityTracker;

.field private p:I

.field private q:F

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Landroid/text/TextPaint;

.field private z:Landroid/content/res/ColorStateList;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 810035
    const/4 v0, 0x3

    new-array v0, v0, [I

    const v1, 0x1010098

    aput v1, v0, v3

    const v1, 0x1010095

    aput v1, v0, v4

    const/4 v1, 0x2

    const v2, 0x7f010089

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/widget/SwitchCompat;->a:[I

    .line 810036
    new-array v0, v4, [I

    const v1, 0x10100a0

    aput v1, v0, v3

    sput-object v0, Lcom/facebook/widget/SwitchCompat;->G:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810038
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 810039
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 810040
    const v0, 0x7f0103ad

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 810041
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 810042
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 810043
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->o:Landroid/view/VelocityTracker;

    .line 810044
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 810045
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v3}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    .line 810046
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 810047
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, v1, Landroid/text/TextPaint;->density:F

    .line 810048
    sget-object v0, LX/03r;->SwitchCompat:[I

    const v1, 0x7f0e02c7

    invoke-static {p1, p2, v0, p3, v1}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 810049
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    .line 810050
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    .line 810051
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, LX/3wC;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    .line 810052
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, LX/3wC;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    .line 810053
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v3}, LX/3wC;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    .line 810054
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v2}, LX/3wC;->d(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->d:I

    .line 810055
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, LX/3wC;->d(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->e:I

    .line 810056
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, LX/3wC;->d(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->f:I

    .line 810057
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, LX/3wC;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/SwitchCompat;->g:Z

    .line 810058
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, LX/3wC;->f(II)I

    move-result v1

    .line 810059
    if-eqz v1, :cond_0

    .line 810060
    invoke-direct {p0, p1, v1}, Lcom/facebook/widget/SwitchCompat;->a(Landroid/content/Context;I)V

    .line 810061
    :cond_0
    invoke-virtual {v0}, LX/3wC;->c()LX/3wA;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->F:LX/3wA;

    .line 810062
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 810063
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 810064
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    .line 810065
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->p:I

    .line 810066
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->refreshDrawableState()V

    .line 810067
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 810068
    invoke-virtual {p0, v3}, Lcom/facebook/widget/SwitchCompat;->setClickable(Z)V

    .line 810069
    return-void
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 810070
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8

    .prologue
    .line 810071
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->C:Landroid/text/method/TransformationMethod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->C:Landroid/text/method/TransformationMethod;

    invoke-interface {v0, p1, p0}, Landroid/text/method/TransformationMethod;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 810072
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0

    :cond_0
    move-object v1, p1

    .line 810073
    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 810074
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 810075
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->clearAnimation()V

    .line 810076
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    .line 810077
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 810078
    sget-object v0, Lcom/facebook/widget/SwitchCompat;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 810079
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 810080
    if-eqz v1, :cond_1

    .line 810081
    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->z:Landroid/content/res/ColorStateList;

    .line 810082
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 810083
    if-eqz v1, :cond_0

    .line 810084
    int-to-float v2, v1

    iget-object v3, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 810085
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 810086
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810087
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 810088
    if-eqz v1, :cond_2

    .line 810089
    new-instance v1, LX/3ua;

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3ua;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->C:Landroid/text/method/TransformationMethod;

    .line 810090
    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 810091
    return-void

    .line 810092
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->z:Landroid/content/res/ColorStateList;

    goto :goto_0

    .line 810093
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/SwitchCompat;->C:Landroid/text/method/TransformationMethod;

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 810094
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 810095
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 810096
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 810097
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 810098
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 810099
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    .line 810100
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 810101
    :goto_0
    sub-float/2addr v0, v1

    .line 810102
    new-instance v2, LX/4oY;

    invoke-direct {v2, p0, v1, v0}, LX/4oY;-><init>(Lcom/facebook/widget/SwitchCompat;FF)V

    iput-object v2, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    .line 810103
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 810104
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->startAnimation(Landroid/view/animation/Animation;)V

    .line 810105
    return-void

    .line 810106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(FF)Z
    .locals 5

    .prologue
    .line 810107
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getThumbOffset()I

    move-result v0

    .line 810108
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 810109
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->v:I

    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    sub-int/2addr v1, v2

    .line 810110
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->u:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    sub-int/2addr v0, v2

    .line 810111
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->t:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    add-int/2addr v2, v3

    .line 810112
    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->x:I

    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    add-int/2addr v3, v4

    .line 810113
    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    int-to-float v0, v2

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    int-to-float v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    int-to-float v0, v3

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 810114
    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    .line 810115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 810116
    :goto_0
    if-eqz v2, :cond_5

    .line 810117
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->o:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 810118
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    .line 810119
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->p:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 810120
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    cmpg-float v2, v2, v5

    if-gez v2, :cond_2

    .line 810121
    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 810122
    invoke-direct {p0, p1}, Lcom/facebook/widget/SwitchCompat;->a(Landroid/view/MotionEvent;)V

    .line 810123
    return-void

    :cond_1
    move v2, v1

    .line 810124
    goto :goto_0

    :cond_2
    move v0, v1

    .line 810125
    goto :goto_1

    :cond_3
    cmpl-float v2, v2, v5

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_1

    .line 810126
    :cond_4
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getTargetCheckedState()Z

    move-result v0

    goto :goto_1

    .line 810127
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    goto :goto_1
.end method

.method private getTargetCheckedState()Z
    .locals 2

    .prologue
    .line 810128
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getThumbOffset()I
    .locals 2

    .prologue
    .line 810129
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810130
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    sub-float/2addr v0, v1

    .line 810131
    :goto_0
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 810132
    :cond_0
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    goto :goto_0
.end method

.method private getThumbScrollRange()I
    .locals 3

    .prologue
    .line 810151
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 810152
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 810153
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 810154
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->t:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    .line 810155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setChecked$25decb5(Z)V
    .locals 2

    .prologue
    .line 810133
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    .line 810134
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 810135
    invoke-direct {p0, v0}, Lcom/facebook/widget/SwitchCompat;->a(Z)V

    .line 810136
    :goto_0
    return-void

    .line 810137
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->a()V

    .line 810138
    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setThumbPosition(Lcom/facebook/widget/SwitchCompat;F)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static setThumbPosition(Lcom/facebook/widget/SwitchCompat;F)V
    .locals 0

    .prologue
    .line 810156
    iput p1, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    .line 810157
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 810158
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 810159
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 810160
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->u:I

    .line 810161
    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->v:I

    .line 810162
    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->w:I

    .line 810163
    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->x:I

    .line 810164
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getThumbOffset()I

    move-result v0

    add-int/2addr v0, v2

    .line 810165
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    .line 810166
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 810167
    iget v6, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v6

    .line 810168
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 810169
    :cond_0
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 810170
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 810171
    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int v2, v0, v2

    .line 810172
    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->t:I

    add-int/2addr v0, v4

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    .line 810173
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v3, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 810174
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 810175
    if-eqz v1, :cond_1

    .line 810176
    invoke-static {v1, v2, v3, v0, v5}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 810177
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->draw(Landroid/graphics/Canvas;)V

    .line 810178
    return-void
.end method

.method public final drawableHotspotChanged(FF)V
    .locals 1

    .prologue
    .line 810193
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->drawableHotspotChanged(FF)V

    .line 810194
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 810195
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 810196
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 810197
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 810198
    :cond_1
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 810179
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    .line 810180
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getDrawableState()[I

    move-result-object v0

    .line 810181
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 810182
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 810183
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 810184
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 810185
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 810186
    return-void
.end method

.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 810145
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 810146
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    move-result v0

    .line 810147
    :cond_0
    :goto_0
    return v0

    .line 810148
    :cond_1
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    add-int/2addr v0, v1

    .line 810149
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 810150
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->f:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 810187
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 810188
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v0

    .line 810189
    :cond_0
    :goto_0
    return v0

    .line 810190
    :cond_1
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    add-int/2addr v0, v1

    .line 810191
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 810192
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->f:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getShowText()Z
    .locals 1

    .prologue
    .line 810144
    iget-boolean v0, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    return v0
.end method

.method public getSplitTrack()Z
    .locals 1

    .prologue
    .line 810143
    iget-boolean v0, p0, Lcom/facebook/widget/SwitchCompat;->g:Z

    return v0
.end method

.method public getSwitchMinWidth()I
    .locals 1

    .prologue
    .line 810142
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->e:I

    return v0
.end method

.method public getSwitchPadding()I
    .locals 1

    .prologue
    .line 810141
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->f:I

    return v0
.end method

.method public getTextOff()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 810140
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextOn()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 810139
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getThumbDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 810037
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getThumbTextPadding()I
    .locals 1

    .prologue
    .line 809821
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->d:I

    return v0
.end method

.method public getTrackDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 809989
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 2

    .prologue
    .line 809979
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 809980
    invoke-super {p0}, Landroid/widget/CompoundButton;->jumpDrawablesToCurrentState()V

    .line 809981
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809982
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 809983
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 809984
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 809985
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 809986
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->clearAnimation()V

    .line 809987
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->D:Landroid/view/animation/Animation;

    .line 809988
    :cond_2
    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 809975
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 809976
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 809977
    sget-object v1, Lcom/facebook/widget/SwitchCompat;->G:[I

    invoke-static {v0, v1}, Lcom/facebook/widget/SwitchCompat;->mergeDrawableStates([I[I)[I

    .line 809978
    :cond_0
    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 809942
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 809943
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 809944
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    .line 809945
    if-eqz v1, :cond_4

    .line 809946
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 809947
    :goto_0
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->v:I

    .line 809948
    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->x:I

    .line 809949
    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v4

    .line 809950
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v0

    .line 809951
    iget-object v4, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    .line 809952
    if-eqz v1, :cond_0

    .line 809953
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 809954
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v5

    .line 809955
    if-eqz v4, :cond_1

    .line 809956
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 809957
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getTargetCheckedState()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->A:Landroid/text/Layout;

    move-object v1, v0

    .line 809958
    :goto_1
    if-eqz v1, :cond_3

    .line 809959
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getDrawableState()[I

    move-result-object v0

    .line 809960
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->z:Landroid/content/res/ColorStateList;

    if-eqz v6, :cond_2

    .line 809961
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    iget-object v7, p0, Lcom/facebook/widget/SwitchCompat;->z:Landroid/content/res/ColorStateList;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 809962
    :cond_2
    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    iput-object v0, v6, Landroid/text/TextPaint;->drawableState:[I

    .line 809963
    if-eqz v4, :cond_6

    .line 809964
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 809965
    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v4

    .line 809966
    :goto_2
    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    .line 809967
    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 809968
    int-to-float v0, v0

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 809969
    invoke-virtual {v1, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 809970
    :cond_3
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 809971
    return-void

    .line 809972
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_0

    .line 809973
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->B:Landroid/text/Layout;

    move-object v1, v0

    goto :goto_1

    .line 809974
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getWidth()I

    move-result v0

    goto :goto_2
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 809939
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 809940
    const-class v0, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 809941
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4

    .prologue
    .line 809926
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 809927
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 809928
    const-class v0, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 809929
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    .line 809930
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 809931
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 809932
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 809933
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 809934
    :cond_0
    :goto_1
    return-void

    .line 809935
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    goto :goto_0

    .line 809936
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 809937
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 809938
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 809902
    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    .line 809903
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809904
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 809905
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 809906
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 809907
    :cond_0
    :goto_0
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 809908
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingLeft()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 809909
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x0

    add-int/lit8 v1, v1, 0x0

    .line 809910
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getGravity()I

    move-result v2

    and-int/lit8 v2, v2, 0x70

    sparse-switch v2, :sswitch_data_0

    .line 809911
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingTop()I

    move-result v3

    .line 809912
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->s:I

    add-int/2addr v2, v3

    .line 809913
    :goto_2
    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->u:I

    .line 809914
    iput v3, p0, Lcom/facebook/widget/SwitchCompat;->v:I

    .line 809915
    iput v2, p0, Lcom/facebook/widget/SwitchCompat;->x:I

    .line 809916
    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->w:I

    .line 809917
    return-void

    .line 809918
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_0

    .line 809919
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v1, v0, 0x0

    .line 809920
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x0

    add-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 809921
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->s:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v2, v3

    .line 809922
    iget v2, p0, Lcom/facebook/widget/SwitchCompat;->s:I

    add-int/2addr v2, v3

    .line 809923
    goto :goto_2

    .line 809924
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 809925
    iget v3, p0, Lcom/facebook/widget/SwitchCompat;->s:I

    sub-int v3, v2, v3

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, -0x7db6bcd5

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 809872
    iget-boolean v0, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    if-eqz v0, :cond_1

    .line 809873
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->A:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 809874
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/widget/SwitchCompat;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->A:Landroid/text/Layout;

    .line 809875
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->B:Landroid/text/Layout;

    if-nez v0, :cond_1

    .line 809876
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/facebook/widget/SwitchCompat;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/SwitchCompat;->B:Landroid/text/Layout;

    .line 809877
    :cond_1
    iget-object v5, p0, Lcom/facebook/widget/SwitchCompat;->E:Landroid/graphics/Rect;

    .line 809878
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 809879
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 809880
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v2, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    iget v2, v5, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v2

    .line 809881
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 809882
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    if-eqz v3, :cond_4

    .line 809883
    iget-object v3, p0, Lcom/facebook/widget/SwitchCompat;->A:Landroid/text/Layout;

    invoke-virtual {v3}, Landroid/text/Layout;->getWidth()I

    move-result v3

    iget-object v6, p0, Lcom/facebook/widget/SwitchCompat;->B:Landroid/text/Layout;

    invoke-virtual {v6}, Landroid/text/Layout;->getWidth()I

    move-result v6

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p0, Lcom/facebook/widget/SwitchCompat;->d:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    .line 809884
    :goto_1
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/widget/SwitchCompat;->t:I

    .line 809885
    iget-object v2, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    .line 809886
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 809887
    iget-object v1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 809888
    :goto_2
    iget v2, v5, Landroid/graphics/Rect;->left:I

    .line 809889
    iget v3, v5, Landroid/graphics/Rect;->right:I

    .line 809890
    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->e:I

    iget v6, p0, Lcom/facebook/widget/SwitchCompat;->t:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v2, v6

    add-int/2addr v2, v3

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 809891
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 809892
    iput v2, p0, Lcom/facebook/widget/SwitchCompat;->r:I

    .line 809893
    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->s:I

    .line 809894
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    .line 809895
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getMeasuredHeight()I

    move-result v1

    .line 809896
    if-ge v1, v0, :cond_2

    .line 809897
    invoke-static {p0}, LX/0vv;->k(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/SwitchCompat;->setMeasuredDimension(II)V

    .line 809898
    :cond_2
    const v0, -0x517a5bbe

    invoke-static {v0, v4}, LX/02F;->g(II)V

    return-void

    :cond_3
    move v0, v1

    move v2, v1

    .line 809899
    goto :goto_0

    :cond_4
    move v3, v1

    .line 809900
    goto :goto_1

    .line 809901
    :cond_5
    invoke-virtual {v5}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_2
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 809866
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 809867
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    .line 809868
    :goto_0
    if-eqz v0, :cond_0

    .line 809869
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809870
    :cond_0
    return-void

    .line 809871
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x1

    const v0, -0x2e4ba0de

    invoke-static {v6, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 809828
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 809829
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 809830
    packed-switch v0, :pswitch_data_0

    .line 809831
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x59e5a49f

    invoke-static {v1, v3}, LX/02F;->a(II)V

    :goto_1
    return v0

    .line 809832
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 809833
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 809834
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/SwitchCompat;->a(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 809835
    iput v2, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    .line 809836
    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->m:F

    .line 809837
    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->n:F

    goto :goto_0

    .line 809838
    :pswitch_2
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 809839
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 809840
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 809841
    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->m:F

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_1

    iget v4, p0, Lcom/facebook/widget/SwitchCompat;->n:F

    sub-float v4, v1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->l:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 809842
    :cond_1
    iput v6, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    .line 809843
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 809844
    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->m:F

    .line 809845
    iput v1, p0, Lcom/facebook/widget/SwitchCompat;->n:F

    .line 809846
    const v0, -0x615eb608

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto :goto_1

    .line 809847
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 809848
    invoke-direct {p0}, Lcom/facebook/widget/SwitchCompat;->getThumbScrollRange()I

    move-result v0

    .line 809849
    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->m:F

    sub-float v5, v4, v5

    .line 809850
    if-eqz v0, :cond_4

    .line 809851
    int-to-float v0, v0

    div-float v0, v5, v0

    .line 809852
    :goto_2
    invoke-static {p0}, LX/3wJ;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 809853
    neg-float v0, v0

    .line 809854
    :cond_2
    iget v5, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    add-float/2addr v0, v5

    invoke-static {v0, v7, v1}, Lcom/facebook/widget/SwitchCompat;->a(FFF)F

    move-result v0

    .line 809855
    iget v1, p0, Lcom/facebook/widget/SwitchCompat;->q:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 809856
    iput v4, p0, Lcom/facebook/widget/SwitchCompat;->m:F

    .line 809857
    invoke-static {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setThumbPosition(Lcom/facebook/widget/SwitchCompat;F)V

    .line 809858
    :cond_3
    const v0, -0x2a3f568a

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto/16 :goto_1

    .line 809859
    :cond_4
    cmpl-float v0, v5, v7

    if-lez v0, :cond_5

    move v0, v1

    goto :goto_2

    :cond_5
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_2

    .line 809860
    :pswitch_5
    iget v0, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    if-ne v0, v6, :cond_6

    .line 809861
    invoke-direct {p0, p1}, Lcom/facebook/widget/SwitchCompat;->b(Landroid/view/MotionEvent;)V

    .line 809862
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 809863
    const v0, -0x11482f5f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto/16 :goto_1

    .line 809864
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/SwitchCompat;->k:I

    .line 809865
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 809825
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 809826
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked$25decb5(Z)V

    .line 809827
    return-void
.end method

.method public setCheckedNoAnimation(Z)V
    .locals 1

    .prologue
    .line 809822
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 809823
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked$25decb5(Z)V

    .line 809824
    return-void
.end method

.method public setShowText(Z)V
    .locals 1

    .prologue
    .line 809817
    iget-boolean v0, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    if-eq v0, p1, :cond_0

    .line 809818
    iput-boolean p1, p0, Lcom/facebook/widget/SwitchCompat;->j:Z

    .line 809819
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 809820
    :cond_0
    return-void
.end method

.method public setSplitTrack(Z)V
    .locals 0

    .prologue
    .line 809990
    iput-boolean p1, p0, Lcom/facebook/widget/SwitchCompat;->g:Z

    .line 809991
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 809992
    return-void
.end method

.method public setSwitchMinWidth(I)V
    .locals 0

    .prologue
    .line 809993
    iput p1, p0, Lcom/facebook/widget/SwitchCompat;->e:I

    .line 809994
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 809995
    return-void
.end method

.method public setSwitchPadding(I)V
    .locals 0

    .prologue
    .line 809996
    iput p1, p0, Lcom/facebook/widget/SwitchCompat;->f:I

    .line 809997
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 809998
    return-void
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 809999
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 810000
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->y:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 810001
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810002
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 810003
    :cond_0
    return-void
.end method

.method public setTextOff(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 810004
    iput-object p1, p0, Lcom/facebook/widget/SwitchCompat;->i:Ljava/lang/CharSequence;

    .line 810005
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810006
    return-void
.end method

.method public setTextOn(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 810007
    iput-object p1, p0, Lcom/facebook/widget/SwitchCompat;->h:Ljava/lang/CharSequence;

    .line 810008
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810009
    return-void
.end method

.method public setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 810010
    iput-object p1, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    .line 810011
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810012
    return-void
.end method

.method public setThumbDrawableColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 810013
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 810014
    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 810015
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 810016
    return-void
.end method

.method public setThumbResource(I)V
    .locals 1

    .prologue
    .line 810017
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->F:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 810018
    return-void
.end method

.method public setThumbTextPadding(I)V
    .locals 0

    .prologue
    .line 810019
    iput p1, p0, Lcom/facebook/widget/SwitchCompat;->d:I

    .line 810020
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810021
    return-void
.end method

.method public setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 810022
    iput-object p1, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    .line 810023
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->requestLayout()V

    .line 810024
    return-void
.end method

.method public setTrackDrawableColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 810025
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 810026
    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 810027
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->invalidate()V

    .line 810028
    return-void
.end method

.method public setTrackResource(I)V
    .locals 1

    .prologue
    .line 810029
    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->F:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 810030
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 810031
    invoke-virtual {p0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 810032
    return-void

    .line 810033
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 810034
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->b:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/SwitchCompat;->c:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
