.class public Lcom/facebook/widget/CustomHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source ""


# instance fields
.field public a:LX/4np;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 807871
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 807872
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 807888
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807889
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 807886
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807887
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 807885
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 807881
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 807882
    iget-object v0, p0, Lcom/facebook/widget/CustomHorizontalScrollView;->a:LX/4np;

    if-eqz v0, :cond_0

    .line 807883
    iget-object v0, p0, Lcom/facebook/widget/CustomHorizontalScrollView;->a:LX/4np;

    invoke-interface {v0}, LX/4np;->a()V

    .line 807884
    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 2

    .prologue
    .line 807875
    const-string v0, "CustomFrameLayout.setContentView"

    const v1, -0x3b62092d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 807876
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/widget/CustomHorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 807877
    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807878
    const v0, 0x34641c70

    invoke-static {v0}, LX/02m;->a(I)V

    .line 807879
    return-void

    .line 807880
    :catchall_0
    move-exception v0

    const v1, -0x245a35ff

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setOnScrollListener(LX/4np;)V
    .locals 0

    .prologue
    .line 807873
    iput-object p1, p0, Lcom/facebook/widget/CustomHorizontalScrollView;->a:LX/4np;

    .line 807874
    return-void
.end method
