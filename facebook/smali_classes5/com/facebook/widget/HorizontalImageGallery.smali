.class public Lcom/facebook/widget/HorizontalImageGallery;
.super Landroid/widget/HorizontalScrollView;
.source ""


# instance fields
.field private a:I

.field public b:Landroid/widget/LinearLayout;

.field public c:I

.field private d:I

.field private e:I

.field public f:I

.field public g:I

.field public h:F

.field public i:F

.field public j:Landroid/view/View$OnTouchListener;

.field private k:LX/4o1;

.field public l:LX/4o2;

.field public m:I

.field public n:Z

.field private o:Z

.field private p:LX/4o3;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 808466
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 808467
    iput-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    .line 808468
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/HorizontalImageGallery;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808469
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808462
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808463
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    .line 808464
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/HorizontalImageGallery;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808465
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 808458
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    .line 808460
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/HorizontalImageGallery;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808461
    return-void
.end method

.method public static synthetic a(Lcom/facebook/widget/HorizontalImageGallery;F)F
    .locals 0

    .prologue
    .line 808457
    iput p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    return p1
.end method

.method private a(Landroid/view/MotionEvent;)I
    .locals 3

    .prologue
    .line 808449
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    if-eqz v0, :cond_0

    .line 808450
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    .line 808451
    :goto_0
    return v0

    .line 808452
    :cond_0
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 808453
    iget v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 808454
    sub-float v0, v1, v0

    iget v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->d:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 808455
    const/4 v0, 0x2

    goto :goto_0

    .line 808456
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v1, 0x5

    const/4 v6, -0x1

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 808429
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    .line 808430
    iget-object v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 808431
    iget-object v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v6, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-super {p0, v2, v6, v3}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 808432
    invoke-virtual {p0, v0}, Lcom/facebook/widget/HorizontalImageGallery;->setSmoothScrollingEnabled(Z)V

    .line 808433
    invoke-virtual {p0, v5}, Lcom/facebook/widget/HorizontalImageGallery;->setHorizontalFadingEdgeEnabled(Z)V

    .line 808434
    invoke-virtual {p0, v5}, Lcom/facebook/widget/HorizontalImageGallery;->setHorizontalScrollBarEnabled(Z)V

    .line 808435
    iput v5, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    .line 808436
    new-instance v2, LX/4o1;

    invoke-direct {v2, p0}, LX/4o1;-><init>(Lcom/facebook/widget/HorizontalImageGallery;)V

    iput-object v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->k:LX/4o1;

    .line 808437
    iget-object v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->k:LX/4o1;

    invoke-super {p0, v2}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 808438
    if-eqz p2, :cond_0

    .line 808439
    sget-object v2, LX/03r;->HorizontalImageGallery:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 808440
    const/16 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 808441
    const/16 v3, 0x1

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 808442
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 808443
    :cond_0
    invoke-virtual {p0, v1}, Lcom/facebook/widget/HorizontalImageGallery;->setLeftItemWidthPercentage(I)V

    .line 808444
    iput-boolean v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->o:Z

    .line 808445
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->c:I

    .line 808446
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->d:I

    .line 808447
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->e:I

    .line 808448
    return-void
.end method

.method public static synthetic a(Lcom/facebook/widget/HorizontalImageGallery;Z)Z
    .locals 0

    .prologue
    .line 808428
    iput-boolean p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->n:Z

    return p1
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 808426
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->a:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 808427
    iget v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v1, p1

    sub-int v0, v1, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 808421
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 808422
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    .line 808423
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    if-eqz v0, :cond_0

    .line 808424
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    invoke-virtual {v0}, LX/4o3;->a()V

    .line 808425
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 808412
    iget v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    .line 808413
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->getItemCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 808414
    add-int/lit8 v0, p1, -0x1

    .line 808415
    :goto_0
    if-gez v0, :cond_0

    move v0, v1

    .line 808416
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/widget/HorizontalImageGallery;->b(I)I

    move-result v3

    invoke-virtual {p0, v3, v1}, Lcom/facebook/widget/HorizontalImageGallery;->smoothScrollTo(II)V

    .line 808417
    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    .line 808418
    iget-object v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    if-eqz v1, :cond_1

    if-eq v2, v0, :cond_1

    .line 808419
    iget-object v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    invoke-virtual {v1, v0}, LX/4o3;->setCurrentPage(I)V

    .line 808420
    :cond_1
    return-void

    :cond_2
    move v0, p1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 808408
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->requestLayout()V

    .line 808409
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->invalidate()V

    .line 808410
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 808411
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 808470
    iget-boolean v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->o:Z

    if-nez v0, :cond_1

    .line 808471
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 808472
    :cond_0
    :goto_0
    return v0

    .line 808473
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 808474
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    .line 808475
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    .line 808476
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 808477
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/facebook/widget/HorizontalImageGallery;->e:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 808478
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/HorizontalImageGallery;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public getActiveItem()I
    .locals 1

    .prologue
    .line 808407
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 808406
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getItemIndicator()LX/4o3;
    .locals 1

    .prologue
    .line 808405
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    return-object v0
.end method

.method public getItemWidth()I
    .locals 1

    .prologue
    .line 808404
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    return v0
.end method

.method public getItemsContainer()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 808403
    iget-object v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 808373
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 808374
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    .line 808375
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    .line 808376
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    .line 808377
    iget-boolean v3, p0, Lcom/facebook/widget/HorizontalImageGallery;->n:Z

    if-nez v3, :cond_0

    .line 808378
    iput v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    .line 808379
    :cond_0
    :goto_0
    iget v3, p0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    if-ne v3, v4, :cond_2

    .line 808380
    :goto_1
    return v0

    .line 808381
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 808382
    invoke-direct {p0, p1}, Lcom/facebook/widget/HorizontalImageGallery;->a(Landroid/view/MotionEvent;)I

    goto :goto_0

    .line 808383
    :cond_2
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    if-ne v0, v1, :cond_3

    .line 808384
    iput-boolean v1, p0, Lcom/facebook/widget/HorizontalImageGallery;->n:Z

    move v0, v1

    .line 808385
    goto :goto_1

    :cond_3
    move v0, v2

    .line 808386
    goto :goto_1
.end method

.method public setActiveItem(I)V
    .locals 0

    .prologue
    .line 808401
    iput p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    .line 808402
    return-void
.end method

.method public setItemIndicator(LX/4o3;)V
    .locals 1

    .prologue
    .line 808397
    iput-object p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->p:LX/4o3;

    .line 808398
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->getItemCount()I

    move-result v0

    invoke-virtual {p1, v0}, LX/4o3;->setPageCount(I)V

    .line 808399
    iget v0, p0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    invoke-virtual {p1, v0}, LX/4o3;->setCurrentPage(I)V

    .line 808400
    return-void
.end method

.method public setItemWidth(I)V
    .locals 0

    .prologue
    .line 808394
    iput p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    .line 808395
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->requestLayout()V

    .line 808396
    return-void
.end method

.method public setLeftItemWidthPercentage(I)V
    .locals 0

    .prologue
    .line 808391
    iput p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->a:I

    .line 808392
    invoke-virtual {p0}, Lcom/facebook/widget/HorizontalImageGallery;->requestLayout()V

    .line 808393
    return-void
.end method

.method public setOnItemTouchListener(LX/4o2;)V
    .locals 0

    .prologue
    .line 808389
    iput-object p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->l:LX/4o2;

    .line 808390
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 808387
    iput-object p1, p0, Lcom/facebook/widget/HorizontalImageGallery;->j:Landroid/view/View$OnTouchListener;

    .line 808388
    return-void
.end method
