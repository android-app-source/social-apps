.class public Lcom/facebook/widget/BetterSwitch;
.super Landroid/widget/CompoundButton;
.source ""


# static fields
.field private static final J:[I


# instance fields
.field private A:F

.field private B:F

.field private C:Landroid/text/TextPaint;

.field private D:Landroid/content/res/ColorStateList;

.field private E:Landroid/content/res/ColorStateList;

.field private F:Landroid/content/res/ColorStateList;

.field private G:Landroid/text/Layout;

.field private H:Landroid/text/Layout;

.field private final I:Landroid/graphics/Rect;

.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Z

.field private l:I

.field private m:I

.field private n:F

.field private o:F

.field private p:Landroid/view/VelocityTracker;

.field private q:I

.field private r:F

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 807641
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/widget/BetterSwitch;->J:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 807642
    invoke-direct {p0, p1}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;)V

    .line 807643
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    .line 807644
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    .line 807645
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    .line 807646
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    .line 807647
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    .line 807648
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807649
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 807650
    invoke-direct {p0, p1, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807651
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    .line 807652
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    .line 807653
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    .line 807654
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    .line 807655
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    .line 807656
    invoke-direct {p0, p1, p2, v1}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807657
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 807658
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807659
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    .line 807660
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    .line 807661
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    .line 807662
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    .line 807663
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    .line 807664
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807665
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8

    .prologue
    .line 807666
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/BetterSwitch;->k:Z

    if-eqz v0, :cond_0

    .line 807667
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 807668
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0

    :cond_0
    move-object v1, p1

    goto :goto_0
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 807669
    const/4 v0, 0x0

    .line 807670
    packed-switch p1, :pswitch_data_0

    .line 807671
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/graphics/Typeface;I)V

    .line 807672
    return-void

    .line 807673
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 807674
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_0

    .line 807675
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 807676
    sget-object v0, LX/03r;->TextAppearanceBetterSwitch:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 807677
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 807678
    if-eqz v1, :cond_1

    .line 807679
    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->D:Landroid/content/res/ColorStateList;

    .line 807680
    :goto_0
    const/16 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 807681
    if-eqz v1, :cond_0

    .line 807682
    int-to-float v2, v1

    iget-object v3, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 807683
    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 807684
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807685
    :cond_0
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 807686
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 807687
    invoke-direct {p0, v1, v2}, Lcom/facebook/widget/BetterSwitch;->a(II)V

    .line 807688
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 807689
    return-void

    .line 807690
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->D:Landroid/content/res/ColorStateList;

    goto :goto_0
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 807691
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    .line 807692
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 807693
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, v1, Landroid/text/TextPaint;->density:F

    .line 807694
    if-eqz p2, :cond_7

    .line 807695
    sget-object v0, LX/03r;->BetterSwitch:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 807696
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    .line 807697
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    .line 807698
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    .line 807699
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 807700
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->A:F

    .line 807701
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->A:F

    invoke-virtual {p0, v1}, Lcom/facebook/widget/BetterSwitch;->setTrackOnAlpha(F)V

    .line 807702
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 807703
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->B:F

    .line 807704
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->B:F

    invoke-virtual {p0, v1}, Lcom/facebook/widget/BetterSwitch;->setTrackOffAlpha(F)V

    .line 807705
    :cond_1
    const/16 v1, 0x5

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    .line 807706
    const/16 v1, 0x6

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    .line 807707
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    .line 807708
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    .line 807709
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    .line 807710
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    .line 807711
    :cond_3
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/BetterSwitch;->k:Z

    .line 807712
    const/16 v1, 0xa

    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->v:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->v:I

    .line 807713
    const/16 v1, 0xb

    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    .line 807714
    const/16 v1, 0xd

    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    .line 807715
    const/16 v1, 0xe

    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    .line 807716
    const/16 v1, 0xf

    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    .line 807717
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 807718
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->F:Landroid/content/res/ColorStateList;

    .line 807719
    :cond_4
    const/16 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 807720
    const/16 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->E:Landroid/content/res/ColorStateList;

    .line 807721
    :cond_5
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 807722
    if-eqz v1, :cond_6

    .line 807723
    invoke-direct {p0, p1, v1}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/content/Context;I)V

    .line 807724
    :cond_6
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 807725
    :cond_7
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 807726
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->m:I

    .line 807727
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->q:I

    .line 807728
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->refreshDrawableState()V

    .line 807729
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 807730
    invoke-virtual {p0, v4}, Lcom/facebook/widget/BetterSwitch;->setClickable(Z)V

    .line 807731
    return-void
.end method

.method private a(Landroid/graphics/Typeface;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 807732
    if-lez p2, :cond_4

    .line 807733
    if-nez p1, :cond_1

    .line 807734
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 807735
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 807736
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 807737
    :goto_1
    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p2

    .line 807738
    iget-object v3, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 807739
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    const/high16 v0, -0x41800000    # -0.25f

    :goto_2
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 807740
    :goto_3
    return-void

    .line 807741
    :cond_1
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 807742
    goto :goto_1

    :cond_3
    move v0, v2

    .line 807743
    goto :goto_2

    .line 807744
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 807745
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 807746
    invoke-virtual {p0, p1}, Lcom/facebook/widget/BetterSwitch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_3
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 807747
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 807748
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 807749
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 807750
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 807751
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 807595
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    .line 807596
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 807597
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/BetterSwitch;->a(Landroid/view/MotionEvent;)V

    .line 807598
    if-eqz v0, :cond_3

    .line 807599
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 807600
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 807601
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/facebook/widget/BetterSwitch;->q:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 807602
    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 807603
    :goto_1
    invoke-virtual {p0, v1}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 807604
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 807605
    goto :goto_0

    :cond_1
    move v1, v2

    .line 807606
    goto :goto_1

    .line 807607
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getTargetCheckedState()Z

    move-result v1

    goto :goto_1

    .line 807608
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v0

    .line 807609
    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setChecked(Z)V

    .line 807610
    goto :goto_2
.end method

.method private getTargetCheckedState()Z
    .locals 3

    .prologue
    .line 807774
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getThumbScrollRange()I
    .locals 2

    .prologue
    .line 807752
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 807753
    const/4 v0, 0x0

    .line 807754
    :goto_0
    return v0

    .line 807755
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 807756
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->s:I

    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->u:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 807757
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    .line 807758
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getDrawableState()[I

    move-result-object v0

    .line 807759
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 807760
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 807761
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 807762
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->invalidate()V

    .line 807763
    return-void
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 807764
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->s:I

    add-int/2addr v0, v1

    .line 807765
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 807766
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    add-int/2addr v0, v1

    .line 807767
    :cond_0
    return v0
.end method

.method public getSwitchMinWidth()I
    .locals 1

    .prologue
    .line 807768
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    return v0
.end method

.method public getSwitchPadding()I
    .locals 1

    .prologue
    .line 807769
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    return v0
.end method

.method public getTextOff()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 807770
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextOn()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 807771
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getThumbDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 807772
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getThumbTextPadding()I
    .locals 1

    .prologue
    .line 807773
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x397631cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 807634
    invoke-super {p0}, Landroid/widget/CompoundButton;->onAttachedToWindow()V

    .line 807635
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    .line 807636
    const/16 v1, 0x2d

    const v2, 0x2bb45e96

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 807637
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 807638
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 807639
    sget-object v1, Lcom/facebook/widget/BetterSwitch;->J:[I

    invoke-static {v0, v1}, Lcom/facebook/widget/BetterSwitch;->mergeDrawableStates([I[I)[I

    .line 807640
    :cond_0
    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3fa4cb12

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 807458
    invoke-super {p0}, Landroid/widget/CompoundButton;->onDetachedFromWindow()V

    .line 807459
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 807460
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    .line 807461
    const/16 v1, 0x2d

    const v2, -0x584e4372

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 807462
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 807463
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->w:I

    .line 807464
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->x:I

    .line 807465
    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->y:I

    .line 807466
    iget v3, p0, Lcom/facebook/widget/BetterSwitch;->z:I

    .line 807467
    iget-object v4, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 807468
    iget-object v4, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 807469
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 807470
    iget-object v4, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 807471
    iget-object v4, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v4

    .line 807472
    iget-object v4, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v1

    .line 807473
    iget-object v5, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v5

    .line 807474
    iget-object v5, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v5, v3, v5

    .line 807475
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 807476
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 807477
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 807478
    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v2, v0, v2

    add-int/2addr v2, v1

    .line 807479
    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->u:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v0

    .line 807480
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 807481
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 807482
    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getTargetCheckedState()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->E:Landroid/content/res/ColorStateList;

    .line 807483
    :goto_0
    if-nez v0, :cond_0

    .line 807484
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->D:Landroid/content/res/ColorStateList;

    .line 807485
    :cond_0
    if-eqz v0, :cond_1

    .line 807486
    iget-object v3, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getDrawableState()[I

    move-result-object v6

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 807487
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getDrawableState()[I

    move-result-object v3

    iput-object v3, v0, Landroid/text/TextPaint;->drawableState:[I

    .line 807488
    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getTargetCheckedState()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->G:Landroid/text/Layout;

    .line 807489
    :goto_1
    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/text/Layout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-int v2, v4, v5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 807490
    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 807491
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 807492
    return-void

    .line 807493
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->F:Landroid/content/res/ColorStateList;

    goto :goto_0

    .line 807494
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->H:Landroid/text/Layout;

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 807495
    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    .line 807496
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getThumbScrollRange()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    .line 807497
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 807498
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->s:I

    sub-int v3, v2, v0

    .line 807499
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x70

    sparse-switch v0, :sswitch_data_0

    .line 807500
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getPaddingTop()I

    move-result v1

    .line 807501
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->t:I

    add-int/2addr v0, v1

    .line 807502
    :goto_1
    iput v3, p0, Lcom/facebook/widget/BetterSwitch;->w:I

    .line 807503
    iput v1, p0, Lcom/facebook/widget/BetterSwitch;->x:I

    .line 807504
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->z:I

    .line 807505
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->y:I

    .line 807506
    return-void

    .line 807507
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 807508
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->t:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 807509
    iget v0, p0, Lcom/facebook/widget/BetterSwitch;->t:I

    add-int/2addr v0, v1

    .line 807510
    goto :goto_1

    .line 807511
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 807512
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->t:I

    sub-int v1, v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x14bc0739

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 807513
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 807514
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 807515
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 807516
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 807517
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->G:Landroid/text/Layout;

    if-nez v1, :cond_0

    .line 807518
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/facebook/widget/BetterSwitch;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->G:Landroid/text/Layout;

    .line 807519
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->H:Landroid/text/Layout;

    if-nez v1, :cond_1

    .line 807520
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/facebook/widget/BetterSwitch;->a(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/BetterSwitch;->H:Landroid/text/Layout;

    .line 807521
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 807522
    iget-object v1, p0, Lcom/facebook/widget/BetterSwitch;->G:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->H:Landroid/text/Layout;

    invoke-virtual {v2}, Landroid/text/Layout;->getWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 807523
    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    mul-int/lit8 v1, v1, 0x2

    iget v7, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    mul-int/lit8 v7, v7, 0x4

    add-int/2addr v1, v7

    iget-object v7, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v7

    iget-object v7, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v7

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 807524
    iget v1, p0, Lcom/facebook/widget/BetterSwitch;->g:I

    iget-object v7, p0, Lcom/facebook/widget/BetterSwitch;->G:Landroid/text/Layout;

    invoke-virtual {v7}, Landroid/text/Layout;->getHeight()I

    move-result v7

    iget-object v8, p0, Lcom/facebook/widget/BetterSwitch;->H:Landroid/text/Layout;

    invoke-virtual {v8}, Landroid/text/Layout;->getHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 807525
    iget v7, p0, Lcom/facebook/widget/BetterSwitch;->v:I

    iget-object v8, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    sub-int v8, v2, v8

    iget-object v9, p0, Lcom/facebook/widget/BetterSwitch;->I:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/facebook/widget/BetterSwitch;->u:I

    .line 807526
    sparse-switch v5, :sswitch_data_0

    move v2, v3

    .line 807527
    :goto_0
    :sswitch_0
    sparse-switch v6, :sswitch_data_1

    .line 807528
    :goto_1
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->s:I

    .line 807529
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->t:I

    .line 807530
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    .line 807531
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getMeasuredHeight()I

    move-result v0

    .line 807532
    if-ge v0, v1, :cond_2

    .line 807533
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/BetterSwitch;->setMeasuredDimension(II)V

    .line 807534
    :cond_2
    const v0, 0x2a369a08

    invoke-static {v0, v4}, LX/02F;->g(II)V

    return-void

    .line 807535
    :sswitch_1
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0

    .line 807536
    :sswitch_2
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    :sswitch_3
    move v0, v1

    .line 807537
    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_3
    .end sparse-switch
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x18f81dd5

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 807538
    iget-object v2, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 807539
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 807540
    packed-switch v2, :pswitch_data_0

    .line 807541
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0xf9e4db4

    invoke-static {v2, v1}, LX/02F;->a(II)V

    :goto_1
    return v0

    .line 807542
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 807543
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 807544
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 807545
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    .line 807546
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->n:F

    .line 807547
    iput v3, p0, Lcom/facebook/widget/BetterSwitch;->o:F

    .line 807548
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 807549
    :pswitch_2
    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 807550
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 807551
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 807552
    iget v4, p0, Lcom/facebook/widget/BetterSwitch;->n:F

    sub-float v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/facebook/widget/BetterSwitch;->m:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_1

    iget v4, p0, Lcom/facebook/widget/BetterSwitch;->o:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/facebook/widget/BetterSwitch;->m:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 807553
    :cond_1
    iput v6, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    .line 807554
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 807555
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->n:F

    .line 807556
    iput v3, p0, Lcom/facebook/widget/BetterSwitch;->o:F

    .line 807557
    const v2, -0x766c3bc8

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    .line 807558
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 807559
    iget v3, p0, Lcom/facebook/widget/BetterSwitch;->n:F

    sub-float v3, v2, v3

    .line 807560
    const/4 v4, 0x0

    iget v5, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    add-float/2addr v3, v5

    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getThumbScrollRange()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 807561
    iget v4, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_2

    .line 807562
    iput v3, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    .line 807563
    iput v2, p0, Lcom/facebook/widget/BetterSwitch;->n:F

    .line 807564
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->invalidate()V

    .line 807565
    :cond_2
    const v2, -0x4bf246fb

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 807566
    :pswitch_5
    iget v2, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    if-ne v2, v6, :cond_3

    .line 807567
    invoke-direct {p0, p1}, Lcom/facebook/widget/BetterSwitch;->b(Landroid/view/MotionEvent;)V

    .line 807568
    const v2, 0x124d8c11

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 807569
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->l:I

    .line 807570
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 807571
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/BetterSwitch;->getThumbScrollRange()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    iput v0, p0, Lcom/facebook/widget/BetterSwitch;->r:F

    .line 807572
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    :goto_1
    iput-object v0, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    .line 807573
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 807574
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->invalidate()V

    .line 807575
    return-void

    .line 807576
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 807577
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public setSwitchMinWidth(I)V
    .locals 0

    .prologue
    .line 807578
    iput p1, p0, Lcom/facebook/widget/BetterSwitch;->f:I

    .line 807579
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807580
    return-void
.end method

.method public setSwitchPadding(I)V
    .locals 0

    .prologue
    .line 807581
    iput p1, p0, Lcom/facebook/widget/BetterSwitch;->h:I

    .line 807582
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807583
    return-void
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 807584
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 807585
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->C:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 807586
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807587
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->invalidate()V

    .line 807588
    :cond_0
    return-void
.end method

.method public setTextOff(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 807589
    iput-object p1, p0, Lcom/facebook/widget/BetterSwitch;->j:Ljava/lang/CharSequence;

    .line 807590
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807591
    return-void
.end method

.method public setTextOn(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 807592
    iput-object p1, p0, Lcom/facebook/widget/BetterSwitch;->i:Ljava/lang/CharSequence;

    .line 807593
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807594
    return-void
.end method

.method public setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 807611
    iput-object p1, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    .line 807612
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807613
    return-void
.end method

.method public setThumbResource(I)V
    .locals 1

    .prologue
    .line 807614
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 807615
    return-void
.end method

.method public setThumbTextPadding(I)V
    .locals 0

    .prologue
    .line 807616
    iput p1, p0, Lcom/facebook/widget/BetterSwitch;->e:I

    .line 807617
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807618
    return-void
.end method

.method public setTrackOffAlpha(F)V
    .locals 2

    .prologue
    .line 807619
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 807620
    return-void
.end method

.method public setTrackOffDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 807621
    iput-object p1, p0, Lcom/facebook/widget/BetterSwitch;->d:Landroid/graphics/drawable/Drawable;

    .line 807622
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807623
    return-void
.end method

.method public setTrackOffResource(I)V
    .locals 1

    .prologue
    .line 807624
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setTrackOffDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 807625
    return-void
.end method

.method public setTrackOnAlpha(F)V
    .locals 2

    .prologue
    .line 807626
    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 807627
    return-void
.end method

.method public setTrackOnDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 807628
    iput-object p1, p0, Lcom/facebook/widget/BetterSwitch;->c:Landroid/graphics/drawable/Drawable;

    .line 807629
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->requestLayout()V

    .line 807630
    return-void
.end method

.method public setTrackOnResource(I)V
    .locals 1

    .prologue
    .line 807631
    invoke-virtual {p0}, Lcom/facebook/widget/BetterSwitch;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/BetterSwitch;->setTrackOnDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 807632
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 807633
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->a:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/BetterSwitch;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
