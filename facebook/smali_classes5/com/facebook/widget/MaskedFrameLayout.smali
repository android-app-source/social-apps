.class public Lcom/facebook/widget/MaskedFrameLayout;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Landroid/graphics/PorterDuffXfermode;


# instance fields
.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Paint;

.field private g:Z

.field private h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 808858
    const-class v0, Lcom/facebook/widget/MaskedFrameLayout;

    sput-object v0, Lcom/facebook/widget/MaskedFrameLayout;->a:Ljava/lang/Class;

    .line 808859
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    sput-object v0, Lcom/facebook/widget/MaskedFrameLayout;->b:Landroid/graphics/PorterDuffXfermode;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 808855
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 808856
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/MaskedFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808857
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808852
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808853
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/MaskedFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808854
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 808849
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808850
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/MaskedFrameLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808851
    return-void
.end method

.method private a(II)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808841
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 808842
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    .line 808843
    :goto_0
    return-object v0

    .line 808844
    :cond_0
    invoke-static {p1, p2}, Lcom/facebook/widget/MaskedFrameLayout;->c(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    .line 808845
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 808846
    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 808847
    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 808848
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 808837
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 808838
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 808839
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->d:Landroid/graphics/Bitmap;

    .line 808840
    :cond_0
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 808821
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 808822
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->h:LX/03V;

    .line 808823
    if-eqz p2, :cond_1

    .line 808824
    sget-object v0, LX/03r;->MaskedFrameLayout:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 808825
    const/16 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 808826
    if-eqz v0, :cond_0

    .line 808827
    invoke-virtual {p0, v0}, Lcom/facebook/widget/MaskedFrameLayout;->setMaskDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 808828
    :cond_0
    const/16 v0, 0x2

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 808829
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 808830
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/MaskedFrameLayout;->setUsesFboToMask(Z)V

    .line 808831
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    .line 808832
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    sget-object v1, Lcom/facebook/widget/MaskedFrameLayout;->b:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 808833
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 808834
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 808835
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 808836
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 808817
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    .line 808818
    invoke-direct {p0, p1}, Lcom/facebook/widget/MaskedFrameLayout;->c(Landroid/graphics/Canvas;)V

    .line 808819
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 808820
    return-void
.end method

.method private b(II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 808802
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 808803
    :try_start_0
    invoke-static {p1, p2}, Lcom/facebook/widget/MaskedFrameLayout;->c(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 808804
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;

    return-object v0

    .line 808805
    :catch_0
    const-string v0, "MaskedFrameLayout failed to create working bitmap"

    .line 808806
    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->h:LX/03V;

    const-string v2, "T2335831:masked_frame_layout_oom"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 808807
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 808808
    const-string v0, " (width = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808809
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 808810
    const-string v0, ", height = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808811
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 808812
    const-string v0, ")\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808813
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 808814
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808815
    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808816
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 808860
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 808861
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 808862
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->e:Landroid/graphics/Bitmap;

    .line 808863
    :cond_0
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 808794
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/facebook/widget/MaskedFrameLayout;->b(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 808795
    if-nez v1, :cond_0

    .line 808796
    :goto_0
    return v0

    .line 808797
    :cond_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 808798
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 808799
    invoke-direct {p0, v2}, Lcom/facebook/widget/MaskedFrameLayout;->c(Landroid/graphics/Canvas;)V

    .line 808800
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v4, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 808801
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static c(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 808790
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 808791
    :goto_0
    return-object v0

    .line 808792
    :catch_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 808793
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 808777
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 808778
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 808779
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    .line 808780
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->getMaskDrawablePaint()Landroid/graphics/Paint;

    move-result-object v2

    .line 808781
    if-eqz v2, :cond_0

    .line 808782
    invoke-virtual {v2}, Landroid/graphics/Paint;->getXfermode()Landroid/graphics/Xfermode;

    move-result-object v3

    .line 808783
    sget-object v4, Lcom/facebook/widget/MaskedFrameLayout;->b:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 808784
    iget-object v4, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 808785
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 808786
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 808787
    :goto_0
    return-void

    .line 808788
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/MaskedFrameLayout;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 808789
    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private getMaskDrawablePaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 808769
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 808770
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 808771
    :goto_0
    return-object v0

    .line 808772
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v0, :cond_1

    .line 808773
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    goto :goto_0

    .line 808774
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/ShapeDrawable;

    if-eqz v0, :cond_2

    .line 808775
    iget-object v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    goto :goto_0

    .line 808776
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 808760
    const/4 v0, 0x0

    .line 808761
    iget-object v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 808762
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 808763
    const/4 v0, 0x1

    .line 808764
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 808765
    invoke-direct {p0, p1}, Lcom/facebook/widget/MaskedFrameLayout;->a(Landroid/graphics/Canvas;)V

    .line 808766
    :cond_1
    return-void

    .line 808767
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/widget/MaskedFrameLayout;->g:Z

    if-nez v1, :cond_0

    .line 808768
    invoke-direct {p0, p1}, Lcom/facebook/widget/MaskedFrameLayout;->b(Landroid/graphics/Canvas;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x50af6a84

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 808757
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->a()V

    .line 808758
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 808759
    const/16 v1, 0x2d

    const v2, -0x7e13f4ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x57b56e64

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 808753
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 808754
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->a()V

    .line 808755
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->b()V

    .line 808756
    const/16 v1, 0x2d

    const v2, 0x409f432f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setMaskDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 808749
    iput-object p1, p0, Lcom/facebook/widget/MaskedFrameLayout;->c:Landroid/graphics/drawable/Drawable;

    .line 808750
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->a()V

    .line 808751
    invoke-virtual {p0}, Lcom/facebook/widget/MaskedFrameLayout;->invalidate()V

    .line 808752
    return-void
.end method

.method public setUsesFboToMask(Z)V
    .locals 1

    .prologue
    .line 808744
    iput-boolean p1, p0, Lcom/facebook/widget/MaskedFrameLayout;->g:Z

    .line 808745
    iget-boolean v0, p0, Lcom/facebook/widget/MaskedFrameLayout;->g:Z

    if-eqz v0, :cond_0

    .line 808746
    invoke-direct {p0}, Lcom/facebook/widget/MaskedFrameLayout;->b()V

    .line 808747
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/MaskedFrameLayout;->invalidate()V

    .line 808748
    return-void
.end method
