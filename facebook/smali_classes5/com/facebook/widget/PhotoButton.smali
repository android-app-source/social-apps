.class public Lcom/facebook/widget/PhotoButton;
.super Landroid/widget/ImageButton;
.source ""


# instance fields
.field private a:Z

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 809092
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 809093
    invoke-direct {p0}, Lcom/facebook/widget/PhotoButton;->c()V

    .line 809094
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 809095
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809096
    invoke-direct {p0}, Lcom/facebook/widget/PhotoButton;->c()V

    .line 809097
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 809098
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809099
    invoke-direct {p0}, Lcom/facebook/widget/PhotoButton;->c()V

    .line 809100
    return-void
.end method

.method private final c()V
    .locals 1

    .prologue
    .line 809089
    const v0, 0x7f021446

    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoButton;->setBackgroundResource(I)V

    .line 809090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/PhotoButton;->b:Z

    .line 809091
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 809101
    const v0, -0x777778

    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoButton;->setColorFilter(I)V

    .line 809102
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 809087
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->clearColorFilter()V

    .line 809088
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 809086
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/high16 v5, 0x43480000    # 200.0f

    const/4 v4, 0x1

    const v0, -0x7516ddfb

    invoke-static {v2, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 809065
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 809066
    iget-boolean v1, p0, Lcom/facebook/widget/PhotoButton;->b:Z

    move v1, v1

    .line 809067
    if-eqz v1, :cond_0

    .line 809068
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 809069
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/widget/PhotoButton;->a:Z

    .line 809070
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->a()V

    .line 809071
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x602f48a2

    invoke-static {v2, v0}, LX/02F;->a(II)V

    return v1

    .line 809072
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-boolean v1, p0, Lcom/facebook/widget/PhotoButton;->a:Z

    if-nez v1, :cond_2

    .line 809073
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->b()V

    .line 809074
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->onClick()V

    goto :goto_0

    .line 809075
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/facebook/widget/PhotoButton;->a:Z

    if-nez v1, :cond_4

    .line 809076
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 809077
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 809078
    cmpl-float v1, v1, v5

    if-gtz v1, :cond_3

    cmpl-float v1, v2, v5

    if-lez v1, :cond_0

    .line 809079
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->b()V

    .line 809080
    iput-boolean v4, p0, Lcom/facebook/widget/PhotoButton;->a:Z

    goto :goto_0

    .line 809081
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 809082
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoButton;->b()V

    .line 809083
    iput-boolean v4, p0, Lcom/facebook/widget/PhotoButton;->a:Z

    goto :goto_0
.end method

.method public setShowPressState(Z)V
    .locals 0

    .prologue
    .line 809084
    iput-boolean p1, p0, Lcom/facebook/widget/PhotoButton;->b:Z

    .line 809085
    return-void
.end method
