.class public Lcom/facebook/widget/FlowLayout;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:[I

.field private k:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 808131
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 808132
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->c:I

    .line 808133
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->d:I

    .line 808134
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->e:I

    .line 808135
    const/16 v0, 0x33

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->f:I

    .line 808136
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->g:I

    .line 808137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/FlowLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 808295
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808296
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->c:I

    .line 808297
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->d:I

    .line 808298
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->e:I

    .line 808299
    const/16 v0, 0x33

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->f:I

    .line 808300
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->g:I

    .line 808301
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FlowLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808302
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 808287
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808288
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->c:I

    .line 808289
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->d:I

    .line 808290
    iput v1, p0, Lcom/facebook/widget/FlowLayout;->e:I

    .line 808291
    const/16 v0, 0x33

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->f:I

    .line 808292
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->g:I

    .line 808293
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FlowLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808294
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)LX/4o0;
    .locals 2

    .prologue
    .line 808286
    new-instance v0, LX/4o0;

    invoke-virtual {p0}, Lcom/facebook/widget/FlowLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4o0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/4o0;
    .locals 3

    .prologue
    .line 808285
    new-instance v0, LX/4o0;

    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v0, v1, v2}, LX/4o0;-><init>(II)V

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 808269
    iget v0, p0, Lcom/facebook/widget/FlowLayout;->g:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/widget/FlowLayout;->j:[I

    .line 808270
    iget v0, p0, Lcom/facebook/widget/FlowLayout;->g:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/facebook/widget/FlowLayout;->k:[I

    .line 808271
    if-eqz p2, :cond_0

    .line 808272
    sget-object v0, LX/03r;->FlowLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 808273
    :try_start_0
    const/16 v0, 0x3

    iget v2, p0, Lcom/facebook/widget/FlowLayout;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->d:I

    .line 808274
    const/16 v0, 0x4

    iget v2, p0, Lcom/facebook/widget/FlowLayout;->e:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->e:I

    .line 808275
    const/16 v0, 0x0

    iget-boolean v2, p0, Lcom/facebook/widget/FlowLayout;->a:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/FlowLayout;->a:Z

    .line 808276
    const/16 v0, 0x2

    iget v2, p0, Lcom/facebook/widget/FlowLayout;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->c:I

    .line 808277
    const/16 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/widget/FlowLayout;->b:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/FlowLayout;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808278
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 808279
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100af

    aput v1, v0, v3

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 808280
    const/4 v0, 0x0

    :try_start_1
    iget v2, p0, Lcom/facebook/widget/FlowLayout;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/FlowLayout;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 808281
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 808282
    :cond_0
    return-void

    .line 808283
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 808284
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 808268
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IIII)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 808257
    move v2, v1

    move v0, v1

    .line 808258
    :goto_0
    if-ge v2, p1, :cond_1

    .line 808259
    invoke-virtual {p0, v2}, Lcom/facebook/widget/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 808260
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 808261
    invoke-virtual {p0, v3, p3, p4}, Lcom/facebook/widget/FlowLayout;->measureChild(Landroid/view/View;II)V

    .line 808262
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 808263
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 808264
    if-lez v4, :cond_0

    if-lez v3, :cond_0

    .line 808265
    add-int/2addr v0, v4

    .line 808266
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 808267
    :cond_1
    if-le v0, p2, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private static b()LX/4o0;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 808256
    new-instance v0, LX/4o0;

    invoke-direct {v0, v1, v1}, LX/4o0;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 808255
    instance-of v0, p1, LX/4o0;

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 808254
    invoke-static {}, Lcom/facebook/widget/FlowLayout;->b()LX/4o0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 808253
    invoke-direct {p0, p1}, Lcom/facebook/widget/FlowLayout;->a(Landroid/util/AttributeSet;)LX/4o0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 808252
    invoke-static {p1}, Lcom/facebook/widget/FlowLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/4o0;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 808209
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingTop()I

    move-result v8

    .line 808210
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v2

    .line 808211
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingRight()I

    move-result v3

    .line 808212
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingBottom()I

    move-result v9

    .line 808213
    invoke-direct/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->a()Z

    move-result v10

    .line 808214
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getChildCount()I

    move-result v11

    .line 808215
    if-eqz v10, :cond_2

    move v1, v2

    .line 808216
    :goto_0
    const/4 v5, 0x0

    .line 808217
    const/4 v4, 0x0

    move v7, v4

    move v4, v5

    move v5, v1

    :goto_1
    if-ge v7, v11, :cond_6

    .line 808218
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/facebook/widget/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 808219
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v6, 0x8

    if-eq v1, v6, :cond_1

    .line 808220
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/4o0;

    .line 808221
    const/4 v6, 0x0

    .line 808222
    iget v13, v1, LX/4o0;->d:I

    if-eq v13, v4, :cond_0

    .line 808223
    iget v5, v1, LX/4o0;->d:I

    .line 808224
    if-eqz v10, :cond_3

    move v4, v2

    :goto_2
    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    .line 808225
    :cond_0
    iput v5, v1, LX/4o0;->a:I

    .line 808226
    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/widget/FlowLayout;->f:I

    and-int/lit8 v13, v13, 0x7

    packed-switch v13, :pswitch_data_0

    .line 808227
    :goto_3
    :pswitch_0
    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/widget/FlowLayout;->f:I

    and-int/lit8 v13, v13, 0x70

    sparse-switch v13, :sswitch_data_0

    .line 808228
    :goto_4
    :sswitch_0
    iget v13, v1, LX/4o0;->c:I

    and-int/lit8 v13, v13, 0x70

    sparse-switch v13, :sswitch_data_1

    .line 808229
    :goto_5
    :sswitch_1
    iget v13, v1, LX/4o0;->b:I

    const/4 v14, 0x0

    invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/2addr v6, v13

    iput v6, v1, LX/4o0;->b:I

    .line 808230
    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/FlowLayout;->d:I

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    add-int/2addr v6, v13

    add-int/2addr v5, v6

    .line 808231
    iget v6, v1, LX/4o0;->a:I

    iget v13, v1, LX/4o0;->b:I

    iget v14, v1, LX/4o0;->a:I

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int/2addr v14, v15

    iget v1, v1, LX/4o0;->b:I

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v1, v15

    invoke-virtual {v12, v6, v13, v14, v1}, Landroid/view/View;->layout(IIII)V

    .line 808232
    :cond_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    :cond_2
    move v1, v3

    .line 808233
    goto :goto_0

    :cond_3
    move v4, v3

    .line 808234
    goto :goto_2

    .line 808235
    :pswitch_1
    if-nez v10, :cond_5

    .line 808236
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredWidth()I

    move-result v13

    sub-int/2addr v13, v5

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iput v13, v1, LX/4o0;->a:I

    goto :goto_3

    .line 808237
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    iget v15, v1, LX/4o0;->d:I

    aget v14, v14, v15

    sub-int/2addr v13, v14

    div-int/lit8 v13, v13, 0x2

    .line 808238
    if-eqz v10, :cond_4

    .line 808239
    sub-int/2addr v13, v2

    add-int/2addr v13, v5

    iput v13, v1, LX/4o0;->a:I

    goto :goto_3

    .line 808240
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredWidth()I

    move-result v14

    sub-int v13, v14, v13

    add-int/2addr v13, v2

    sub-int/2addr v13, v5

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iput v13, v1, LX/4o0;->a:I

    goto :goto_3

    .line 808241
    :pswitch_3
    if-eqz v10, :cond_5

    .line 808242
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    iget v15, v1, LX/4o0;->d:I

    aget v14, v14, v15

    sub-int/2addr v13, v14

    sub-int/2addr v13, v3

    sub-int/2addr v13, v2

    add-int/2addr v13, v5

    iput v13, v1, LX/4o0;->a:I

    goto/16 :goto_3

    .line 808243
    :cond_5
    iput v5, v1, LX/4o0;->a:I

    goto/16 :goto_3

    .line 808244
    :sswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredHeight()I

    move-result v6

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/widget/FlowLayout;->i:I

    sub-int/2addr v6, v13

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v6, v8

    .line 808245
    goto/16 :goto_4

    .line 808246
    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getMeasuredHeight()I

    move-result v6

    move-object/from16 v0, p0

    iget v13, v0, Lcom/facebook/widget/FlowLayout;->i:I

    sub-int/2addr v6, v13

    sub-int/2addr v6, v8

    sub-int/2addr v6, v9

    goto/16 :goto_4

    .line 808247
    :sswitch_4
    const/4 v13, 0x0

    iput v13, v1, LX/4o0;->b:I

    .line 808248
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    iget v14, v1, LX/4o0;->d:I

    aget v13, v13, v14

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v13, v14

    div-int/lit8 v13, v13, 0x2

    add-int/2addr v6, v13

    .line 808249
    goto/16 :goto_5

    .line 808250
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    iget v14, v1, LX/4o0;->d:I

    aget v13, v13, v14

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v13, v14

    add-int/2addr v6, v13

    goto/16 :goto_5

    .line 808251
    :cond_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x30 -> :sswitch_0
        0x50 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_4
        0x30 -> :sswitch_1
        0x50 -> :sswitch_5
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 22

    .prologue
    .line 808153
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingRight()I

    move-result v5

    sub-int v12, v4, v5

    .line 808154
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 808155
    if-eqz v4, :cond_5

    const/4 v4, 0x1

    move v5, v4

    .line 808156
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v10

    .line 808157
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingTop()I

    move-result v8

    .line 808158
    const/4 v7, 0x0

    .line 808159
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/widget/FlowLayout;->h:I

    .line 808160
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/widget/FlowLayout;->i:I

    .line 808161
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getChildCount()I

    move-result v13

    .line 808162
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/widget/FlowLayout;->a:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v13, v12, v1, v2}, Lcom/facebook/widget/FlowLayout;->a(IIII)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    move v6, v4

    .line 808163
    :goto_1
    const/4 v9, 0x0

    .line 808164
    const/4 v4, 0x0

    move v11, v4

    move v4, v7

    move v7, v8

    move v8, v10

    :goto_2
    if-ge v11, v13, :cond_e

    .line 808165
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/facebook/widget/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 808166
    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v15, 0x8

    if-eq v10, v15, :cond_d

    .line 808167
    add-int/lit8 v10, v9, 0x1

    .line 808168
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/facebook/widget/FlowLayout;->a:Z

    if-nez v9, :cond_0

    .line 808169
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v14, v1, v2}, Lcom/facebook/widget/FlowLayout;->measureChild(Landroid/view/View;II)V

    .line 808170
    :cond_0
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 808171
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    .line 808172
    if-lez v15, :cond_c

    if-lez v16, :cond_c

    .line 808173
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->c:I

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v9, v0, :cond_8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->c:I

    add-int/lit8 v9, v9, -0x1

    if-lt v4, v9, :cond_8

    .line 808174
    if-nez v6, :cond_1

    const/4 v9, 0x2

    if-gt v10, v9, :cond_7

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/facebook/widget/FlowLayout;->b:Z

    if-eqz v9, :cond_7

    .line 808175
    :cond_1
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->c:I

    if-ne v4, v9, :cond_8

    .line 808176
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/FlowLayout;->c:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 808177
    sub-int v5, v13, v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v5}, Lcom/facebook/widget/FlowLayout;->removeViews(II)V

    .line 808178
    :cond_3
    if-nez v10, :cond_4

    .line 808179
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/util/Arrays;->fill([II)V

    .line 808180
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/util/Arrays;->fill([II)V

    .line 808181
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    aget v5, v5, v4

    add-int/2addr v5, v7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lcom/facebook/widget/FlowLayout;->i:I

    .line 808182
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingRight()I

    .line 808183
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    aget v4, v5, v4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, v7

    .line 808184
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/FlowLayout;->h:I

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    move/from16 v0, p1

    invoke-static {v5, v0}, Lcom/facebook/widget/FlowLayout;->resolveSize(II)I

    move-result v5

    move/from16 v0, p2

    invoke-static {v4, v0}, Lcom/facebook/widget/FlowLayout;->resolveSize(II)I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4}, Lcom/facebook/widget/FlowLayout;->setMeasuredDimension(II)V

    .line 808185
    return-void

    .line 808186
    :cond_5
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_0

    .line 808187
    :cond_6
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_1

    .line 808188
    :cond_7
    if-eqz v5, :cond_8

    add-int v9, v8, v15

    if-gt v9, v12, :cond_2

    .line 808189
    :cond_8
    if-nez v6, :cond_a

    const/4 v9, 0x2

    if-gt v10, v9, :cond_9

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/facebook/widget/FlowLayout;->b:Z

    if-nez v9, :cond_a

    :cond_9
    if-eqz v5, :cond_b

    add-int v9, v8, v15

    if-le v9, v12, :cond_b

    .line 808190
    :cond_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    aget v8, v8, v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->e:I

    add-int/2addr v8, v9

    add-int/2addr v7, v8

    .line 808191
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v8

    .line 808192
    add-int/lit8 v4, v4, 0x1

    .line 808193
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->g:I

    if-gt v9, v4, :cond_b

    .line 808194
    add-int/lit8 v9, v4, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lcom/facebook/widget/FlowLayout;->g:I

    .line 808195
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->g:I

    new-array v9, v9, [I

    .line 808196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 808197
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    .line 808198
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->g:I

    new-array v9, v9, [I

    .line 808199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 808200
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    :cond_b
    move v9, v8

    move v8, v7

    move v7, v4

    .line 808201
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, LX/4o0;

    .line 808202
    iput v8, v4, LX/4o0;->b:I

    .line 808203
    iput v7, v4, LX/4o0;->d:I

    .line 808204
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/FlowLayout;->d:I

    add-int/2addr v4, v15

    add-int/2addr v4, v9

    .line 808205
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/facebook/widget/FlowLayout;->d:I

    sub-int v14, v4, v14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v15

    sub-int/2addr v14, v15

    aput v14, v9, v7

    .line 808206
    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/FlowLayout;->h:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/widget/FlowLayout;->j:[I

    aget v14, v14, v7

    invoke-static {v9, v14}, Ljava/lang/Math;->max(II)I

    move-result v9

    move-object/from16 v0, p0

    iput v9, v0, Lcom/facebook/widget/FlowLayout;->h:I

    .line 808207
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/widget/FlowLayout;->k:[I

    aget v14, v14, v7

    move/from16 v0, v16

    invoke-static {v14, v0}, Ljava/lang/Math;->max(II)I

    move-result v14

    aput v14, v9, v7

    move v9, v4

    move v4, v10

    .line 808208
    :goto_4
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    move/from16 v21, v4

    move v4, v7

    move v7, v8

    move v8, v9

    move/from16 v9, v21

    goto/16 :goto_2

    :cond_c
    move v9, v8

    move v8, v7

    move v7, v4

    move v4, v10

    goto :goto_4

    :cond_d
    move/from16 v21, v9

    move v9, v8

    move v8, v7

    move v7, v4

    move/from16 v4, v21

    goto :goto_4

    :cond_e
    move v10, v9

    goto/16 :goto_3
.end method

.method public setForceFirstItemSeparateLine(Z)V
    .locals 0

    .prologue
    .line 808151
    iput-boolean p1, p0, Lcom/facebook/widget/FlowLayout;->b:Z

    .line 808152
    return-void
.end method

.method public setGravity(I)V
    .locals 2

    .prologue
    .line 808143
    iget v0, p0, Lcom/facebook/widget/FlowLayout;->f:I

    if-eq v0, p1, :cond_1

    .line 808144
    and-int/lit8 v0, p1, 0x7

    if-nez v0, :cond_2

    .line 808145
    or-int/lit8 v0, p1, 0x3

    .line 808146
    :goto_0
    and-int/lit8 v1, v0, 0x70

    if-nez v1, :cond_0

    .line 808147
    or-int/lit8 v0, v0, 0x30

    .line 808148
    :cond_0
    iput v0, p0, Lcom/facebook/widget/FlowLayout;->f:I

    .line 808149
    invoke-virtual {p0}, Lcom/facebook/widget/FlowLayout;->requestLayout()V

    .line 808150
    :cond_1
    return-void

    :cond_2
    move v0, p1

    goto :goto_0
.end method

.method public setHorizontalSpacing(I)V
    .locals 0

    .prologue
    .line 808141
    iput p1, p0, Lcom/facebook/widget/FlowLayout;->d:I

    .line 808142
    return-void
.end method

.method public setVerticalSpacing(I)V
    .locals 0

    .prologue
    .line 808139
    iput p1, p0, Lcom/facebook/widget/FlowLayout;->e:I

    .line 808140
    return-void
.end method
