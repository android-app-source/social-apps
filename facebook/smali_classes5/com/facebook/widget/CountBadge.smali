.class public Lcom/facebook/widget/CountBadge;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/ViewPropertyAnimator;

.field private b:Landroid/animation/ObjectAnimator;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/animation/AnimatorListenerAdapter;

.field private e:I

.field private f:LX/3Nk;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 807866
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 807867
    iput v0, p0, Lcom/facebook/widget/CountBadge;->g:I

    .line 807868
    iput v0, p0, Lcom/facebook/widget/CountBadge;->h:I

    .line 807869
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/CountBadge;->a(Landroid/util/AttributeSet;)V

    .line 807870
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 807861
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807862
    iput v0, p0, Lcom/facebook/widget/CountBadge;->g:I

    .line 807863
    iput v0, p0, Lcom/facebook/widget/CountBadge;->h:I

    .line 807864
    invoke-direct {p0, p2}, Lcom/facebook/widget/CountBadge;->a(Landroid/util/AttributeSet;)V

    .line 807865
    return-void
.end method

.method private final a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/high16 v0, -0x1000000

    .line 807844
    const v1, 0x7f030397

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 807845
    if-eqz p1, :cond_0

    .line 807846
    invoke-virtual {p0}, Lcom/facebook/widget/CountBadge;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->CountBadge:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 807847
    const/16 v2, 0x0

    iget v3, p0, Lcom/facebook/widget/CountBadge;->g:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/widget/CountBadge;->g:I

    .line 807848
    const/16 v2, 0x1

    iget v3, p0, Lcom/facebook/widget/CountBadge;->h:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/widget/CountBadge;->h:I

    .line 807849
    const/16 v2, 0x2

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 807850
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    move v1, v0

    .line 807851
    iget v0, p0, Lcom/facebook/widget/CountBadge;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setBackgroundResource(I)V

    .line 807852
    const v0, 0x7f0d0b81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/CountBadge;->c:Landroid/widget/TextView;

    .line 807853
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 807854
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    .line 807855
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setCount(I)V

    .line 807856
    invoke-virtual {p0}, Lcom/facebook/widget/CountBadge;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    .line 807857
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 807858
    new-instance v0, LX/4no;

    invoke-direct {v0, p0}, LX/4no;-><init>(Lcom/facebook/widget/CountBadge;)V

    iput-object v0, p0, Lcom/facebook/widget/CountBadge;->d:Landroid/animation/AnimatorListenerAdapter;

    .line 807859
    invoke-static {p0}, LX/4mJ;->a(Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/CountBadge;->b:Landroid/animation/ObjectAnimator;

    .line 807860
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 807836
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    if-nez v0, :cond_1

    .line 807837
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    .line 807838
    invoke-direct {p0}, Lcom/facebook/widget/CountBadge;->f()V

    .line 807839
    :cond_0
    :goto_0
    return-void

    .line 807840
    :cond_1
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    if-lez v0, :cond_0

    .line 807841
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setCount(I)V

    .line 807842
    if-eqz p1, :cond_0

    .line 807843
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 807828
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    if-ne v0, v1, :cond_1

    .line 807829
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    .line 807830
    invoke-direct {p0}, Lcom/facebook/widget/CountBadge;->h()V

    .line 807831
    :cond_0
    :goto_0
    return-void

    .line 807832
    :cond_1
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    if-le v0, v1, :cond_0

    .line 807833
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setCount(I)V

    .line 807834
    if-eqz p1, :cond_0

    .line 807835
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 807825
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    invoke-virtual {v0, v1}, LX/3Nk;->setScaleX(F)V

    .line 807826
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    invoke-virtual {v0, v1}, LX/3Nk;->setScaleY(F)V

    .line 807827
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 807821
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/facebook/widget/CountBadge;->d:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 807822
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 807823
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 807824
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 807790
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    invoke-virtual {v0, v1}, LX/3Nk;->setScaleX(F)V

    .line 807791
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    invoke-virtual {v0, v1}, LX/3Nk;->setScaleY(F)V

    .line 807792
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 807817
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Lcom/facebook/widget/CountBadge;->d:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 807818
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 807819
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->a:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 807820
    return-void
.end method

.method public static i(Lcom/facebook/widget/CountBadge;)V
    .locals 2

    .prologue
    .line 807812
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->f:LX/3Nk;

    .line 807813
    iget-object v1, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v1

    move v0, v1

    .line 807814
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 807815
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 807816
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 807810
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/CountBadge;->a(Z)V

    .line 807811
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 807808
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/CountBadge;->b(Z)V

    .line 807809
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 807807
    iget v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 807803
    if-eqz p1, :cond_0

    .line 807804
    iget v0, p0, Lcom/facebook/widget/CountBadge;->h:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setBackgroundResource(I)V

    .line 807805
    :goto_0
    return-void

    .line 807806
    :cond_0
    iget v0, p0, Lcom/facebook/widget/CountBadge;->g:I

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CountBadge;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setCount(I)V
    .locals 2

    .prologue
    .line 807795
    if-gtz p1, :cond_0

    .line 807796
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/CountBadge;->e:I

    .line 807797
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->c:Landroid/widget/TextView;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 807798
    invoke-direct {p0}, Lcom/facebook/widget/CountBadge;->g()V

    .line 807799
    :goto_0
    return-void

    .line 807800
    :cond_0
    iput p1, p0, Lcom/facebook/widget/CountBadge;->e:I

    .line 807801
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->c:Landroid/widget/TextView;

    iget v1, p0, Lcom/facebook/widget/CountBadge;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 807802
    invoke-direct {p0}, Lcom/facebook/widget/CountBadge;->e()V

    goto :goto_0
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 807793
    iget-object v0, p0, Lcom/facebook/widget/CountBadge;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 807794
    return-void
.end method
