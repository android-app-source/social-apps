.class public Lcom/facebook/widget/OverlayLayout;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 809034
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 809035
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 809032
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809033
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 809030
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809031
    return-void
.end method

.method private static a()LX/4oB;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 809029
    new-instance v0, LX/4oB;

    invoke-direct {v0, v1, v1}, LX/4oB;-><init>(II)V

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)LX/4oB;
    .locals 2

    .prologue
    .line 809028
    new-instance v0, LX/4oB;

    invoke-virtual {p0}, Lcom/facebook/widget/OverlayLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4oB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/4oB;
    .locals 1

    .prologue
    .line 808954
    new-instance v0, LX/4oB;

    invoke-direct {v0, p0}, LX/4oB;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 809019
    new-array v0, v1, [I

    .line 809020
    new-array v1, v1, [I

    .line 809021
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 809022
    invoke-virtual {p0, v1}, Lcom/facebook/widget/OverlayLayout;->getLocationInWindow([I)V

    .line 809023
    aget v2, v0, v3

    aget v3, v1, v3

    sub-int/2addr v2, v3

    .line 809024
    aget v0, v0, v4

    aget v1, v1, v4

    sub-int/2addr v0, v1

    .line 809025
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v1, v3

    .line 809026
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    .line 809027
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v1, v2

    add-int/2addr v3, v0

    invoke-direct {v4, v2, v0, v1, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 809018
    instance-of v0, p1, LX/4oB;

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 809013
    const-string v0, "Overlayout.dispatchDraw"

    const v1, -0x15d8079f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 809014
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809015
    const v0, -0x6070c4f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 809016
    return-void

    .line 809017
    :catchall_0
    move-exception v0

    const v1, 0x7890154f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 809012
    invoke-static {}, Lcom/facebook/widget/OverlayLayout;->a()LX/4oB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 809011
    invoke-direct {p0, p1}, Lcom/facebook/widget/OverlayLayout;->a(Landroid/util/AttributeSet;)LX/4oB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 809010
    invoke-static {p1}, Lcom/facebook/widget/OverlayLayout;->a(Landroid/view/ViewGroup$LayoutParams;)LX/4oB;

    move-result-object v0

    return-object v0
.end method

.method public final layoutChild(IIIILandroid/view/View;)V
    .locals 10

    .prologue
    .line 808965
    invoke-virtual {p5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/4oB;

    .line 808966
    iget-boolean v1, v0, LX/4oB;->a:Z

    if-nez v1, :cond_1

    .line 808967
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->layoutChild(IIIILandroid/view/View;)V

    .line 808968
    :cond_0
    :goto_0
    return-void

    .line 808969
    :cond_1
    iget-object v1, v0, LX/4oB;->c:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 808970
    iget-object v1, v0, LX/4oB;->c:Landroid/view/View;

    .line 808971
    :goto_1
    if-eqz v1, :cond_0

    .line 808972
    invoke-direct {p0, v1}, Lcom/facebook/widget/OverlayLayout;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 808973
    iget v4, v0, LX/4oB;->d:I

    .line 808974
    iget v5, v0, LX/4oB;->e:I

    .line 808975
    iget v6, v0, LX/4oB;->f:I

    .line 808976
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 808977
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 808978
    and-int/lit16 v1, v4, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_3

    .line 808979
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    .line 808980
    :goto_2
    and-int/lit8 v2, v4, 0x2

    const/4 v9, 0x2

    if-ne v2, v9, :cond_6

    .line 808981
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v7

    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v9

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    move v2, v1

    .line 808982
    :goto_3
    and-int/lit16 v1, v4, 0x800

    const/16 v9, 0x800

    if-ne v1, v9, :cond_8

    .line 808983
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    .line 808984
    :goto_4
    and-int/lit8 v3, v4, 0x10

    const/16 v9, 0x10

    if-ne v3, v9, :cond_b

    .line 808985
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, v8

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    .line 808986
    :goto_5
    add-int v1, v2, v5

    .line 808987
    add-int/2addr v0, v6

    .line 808988
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v1, v7

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/2addr v0, v8

    invoke-static {v0, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p5, v2, v3, v1, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 808989
    :cond_2
    iget v1, v0, LX/4oB;->b:I

    .line 808990
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 808991
    invoke-virtual {p0, v1}, Lcom/facebook/widget/OverlayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 808992
    :cond_3
    and-int/lit16 v1, v4, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_4

    .line 808993
    iget v1, v3, Landroid/graphics/Rect;->left:I

    goto :goto_2

    .line 808994
    :cond_4
    and-int/lit16 v1, v4, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_5

    .line 808995
    iget v1, v3, Landroid/graphics/Rect;->right:I

    goto :goto_2

    :cond_5
    move v1, p1

    .line 808996
    goto :goto_2

    .line 808997
    :cond_6
    and-int/lit8 v2, v4, 0x1

    const/4 v9, 0x1

    if-ne v2, v9, :cond_7

    .line 808998
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_3

    .line 808999
    :cond_7
    and-int/lit8 v2, v4, 0x4

    const/4 v9, 0x4

    if-ne v2, v9, :cond_e

    .line 809000
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v7

    sub-int/2addr v1, v2

    move v2, v1

    goto :goto_3

    .line 809001
    :cond_8
    and-int/lit16 v1, v4, 0x400

    const/16 v9, 0x400

    if-ne v1, v9, :cond_9

    .line 809002
    iget v1, v3, Landroid/graphics/Rect;->top:I

    goto :goto_4

    .line 809003
    :cond_9
    and-int/lit16 v1, v4, 0x1000

    const/16 v9, 0x1000

    if-ne v1, v9, :cond_a

    .line 809004
    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    goto :goto_4

    :cond_a
    move v1, p3

    .line 809005
    goto :goto_4

    .line 809006
    :cond_b
    and-int/lit8 v3, v4, 0x8

    const/16 v9, 0x8

    if-ne v3, v9, :cond_c

    .line 809007
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    goto :goto_5

    .line 809008
    :cond_c
    and-int/lit8 v3, v4, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_d

    .line 809009
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v8

    sub-int v0, v1, v0

    goto :goto_5

    :cond_d
    move v0, v1

    goto :goto_5

    :cond_e
    move v2, v1

    goto/16 :goto_3
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 808960
    const-string v0, "Overlayout.onLayout"

    const v1, 0x12b06124

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 808961
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808962
    const v0, -0x32ea9e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 808963
    return-void

    .line 808964
    :catchall_0
    move-exception v0

    const v1, 0x907fa79

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 808955
    const-string v0, "Overlayout.onMeasure"

    const v1, 0xb5e32de

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 808956
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808957
    const v0, 0x3b3cf1c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 808958
    return-void

    .line 808959
    :catchall_0
    move-exception v0

    const v1, -0x7e513eb7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
