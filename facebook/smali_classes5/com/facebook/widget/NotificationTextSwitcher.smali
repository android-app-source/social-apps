.class public Lcom/facebook/widget/NotificationTextSwitcher;
.super LX/4nr;
.source ""


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/view/animation/Animation$AnimationListener;

.field public c:J

.field public d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 808930
    invoke-direct {p0, p1}, LX/4nr;-><init>(Landroid/content/Context;)V

    .line 808931
    new-instance v0, LX/4o8;

    invoke-direct {v0, p0}, LX/4o8;-><init>(Lcom/facebook/widget/NotificationTextSwitcher;)V

    iput-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    .line 808932
    invoke-direct {p0}, Lcom/facebook/widget/NotificationTextSwitcher;->b()V

    .line 808933
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808926
    invoke-direct {p0, p1, p2}, LX/4nr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808927
    new-instance v0, LX/4o8;

    invoke-direct {v0, p0}, LX/4o8;-><init>(Lcom/facebook/widget/NotificationTextSwitcher;)V

    iput-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    .line 808928
    invoke-direct {p0}, Lcom/facebook/widget/NotificationTextSwitcher;->b()V

    .line 808929
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 808922
    new-instance v0, LX/4oA;

    invoke-direct {v0, p0}, LX/4oA;-><init>(Lcom/facebook/widget/NotificationTextSwitcher;)V

    iput-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->b:Landroid/view/animation/Animation$AnimationListener;

    .line 808923
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 808924
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 808925
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808905
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 808906
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 808907
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 808908
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 808909
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;J)V
    .locals 2

    .prologue
    .line 808934
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 808935
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    .line 808936
    invoke-virtual {p0, p1}, Lcom/facebook/widget/NotificationTextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 808937
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 808938
    :cond_0
    :goto_0
    return-void

    .line 808939
    :cond_1
    invoke-virtual {p0, p1}, Lcom/facebook/widget/NotificationTextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 808940
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;J)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 808918
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 808919
    iput-wide p2, p0, Lcom/facebook/widget/NotificationTextSwitcher;->c:J

    .line 808920
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/widget/NotificationTextSwitcher;->f:Landroid/os/Handler;

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/widget/NotificationTextSwitcher;->c:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 808921
    :cond_0
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 808917
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->a:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->a:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x48660cd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 808914
    invoke-virtual {p0}, Lcom/facebook/widget/NotificationTextSwitcher;->a()V

    .line 808915
    invoke-super {p0}, LX/4nr;->onDetachedFromWindow()V

    .line 808916
    const/16 v1, 0x2d

    const v2, -0x559e8237

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 808910
    iget-object v0, p0, Lcom/facebook/widget/NotificationTextSwitcher;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 808911
    iput-object p1, p0, Lcom/facebook/widget/NotificationTextSwitcher;->a:Ljava/lang/CharSequence;

    .line 808912
    :cond_0
    invoke-super {p0, p1}, LX/4nr;->setText(Ljava/lang/CharSequence;)V

    .line 808913
    return-void
.end method
