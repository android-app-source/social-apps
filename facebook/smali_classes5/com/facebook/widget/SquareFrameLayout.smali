.class public Lcom/facebook/widget/SquareFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 809785
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 809786
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    .line 809787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 809788
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809789
    sget-object v0, LX/03r;->SquareFrameLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 809790
    const/16 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    .line 809791
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 809792
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 809793
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809794
    sget-object v0, LX/03r;->SquareFrameLayout:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 809795
    const/16 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    .line 809796
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 809797
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 809798
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 809799
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 809800
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 809801
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 809802
    sparse-switch v0, :sswitch_data_0

    .line 809803
    iget v4, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    if-nez v4, :cond_2

    .line 809804
    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 809805
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 809806
    return-void

    .line 809807
    :sswitch_0
    iget v0, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    if-nez v0, :cond_0

    .line 809808
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_0

    .line 809809
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    .line 809810
    :sswitch_1
    iget v0, p0, Lcom/facebook/widget/SquareFrameLayout;->a:I

    if-nez v0, :cond_1

    move p2, p1

    .line 809811
    goto :goto_0

    :cond_1
    move p1, p2

    .line 809812
    goto :goto_0

    .line 809813
    :cond_2
    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method
