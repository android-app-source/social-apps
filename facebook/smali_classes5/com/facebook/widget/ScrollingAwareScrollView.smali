.class public Lcom/facebook/widget/ScrollingAwareScrollView;
.super Landroid/widget/ScrollView;
.source ""


# instance fields
.field public a:Z

.field private final b:Landroid/view/GestureDetector$OnGestureListener;

.field private final c:Landroid/view/GestureDetector;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4oV;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/4oU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 809772
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 809773
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 809774
    new-instance v0, LX/4oT;

    invoke-direct {v0, p0}, LX/4oT;-><init>(Lcom/facebook/widget/ScrollingAwareScrollView;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    .line 809775
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/ScrollingAwareScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->c:Landroid/view/GestureDetector;

    .line 809776
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    .line 809777
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 809740
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 809742
    new-instance v0, LX/4oT;

    invoke-direct {v0, p0}, LX/4oT;-><init>(Lcom/facebook/widget/ScrollingAwareScrollView;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    .line 809743
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/ScrollingAwareScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->c:Landroid/view/GestureDetector;

    .line 809744
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    .line 809745
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 809766
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809767
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 809768
    new-instance v0, LX/4oT;

    invoke-direct {v0, p0}, LX/4oT;-><init>(Lcom/facebook/widget/ScrollingAwareScrollView;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    .line 809769
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/widget/ScrollingAwareScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->b:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->c:Landroid/view/GestureDetector;

    .line 809770
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    .line 809771
    return-void
.end method


# virtual methods
.method public final a(LX/4oV;)V
    .locals 1

    .prologue
    .line 809764
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809765
    return-void
.end method

.method public final b(LX/4oV;)V
    .locals 1

    .prologue
    .line 809762
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 809763
    return-void
.end method

.method public final onScrollChanged(IIII)V
    .locals 2

    .prologue
    .line 809756
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 809757
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 809758
    :cond_0
    return-void

    .line 809759
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 809760
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4oV;

    invoke-interface {v0, p1, p2, p4}, LX/4oV;->a(III)V

    .line 809761
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x618b87c2

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 809750
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->c:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 809751
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->c:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 809752
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 809753
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->e:LX/4oU;

    if-eqz v0, :cond_2

    .line 809754
    iget-object v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->e:LX/4oU;

    invoke-interface {v0, p1}, LX/4oU;->a(Landroid/view/MotionEvent;)V

    .line 809755
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    if-eqz v0, :cond_3

    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    const v2, -0x439f2040

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGestureListener(LX/4oU;)V
    .locals 0

    .prologue
    .line 809748
    iput-object p1, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->e:LX/4oU;

    .line 809749
    return-void
.end method

.method public setScrollingEnabled(Z)V
    .locals 0

    .prologue
    .line 809746
    iput-boolean p1, p0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 809747
    return-void
.end method
