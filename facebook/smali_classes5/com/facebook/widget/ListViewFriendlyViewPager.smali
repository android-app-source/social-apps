.class public Lcom/facebook/widget/ListViewFriendlyViewPager;
.super Lcom/facebook/widget/CustomViewPager;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:J

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808738
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;)V

    .line 808739
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->f:J

    .line 808740
    iput-boolean v2, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->g:Z

    .line 808741
    iput-boolean v2, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->h:Z

    .line 808742
    invoke-direct {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->a(Landroid/content/Context;)V

    .line 808743
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808661
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808662
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->f:J

    .line 808663
    iput-boolean v2, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->g:Z

    .line 808664
    iput-boolean v2, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->h:Z

    .line 808665
    invoke-direct {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->a(Landroid/content/Context;)V

    .line 808666
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 808733
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 808734
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->a:I

    .line 808735
    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->b:I

    .line 808736
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->c:Z

    .line 808737
    return-void
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 808731
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 808732
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/31M;II)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 808721
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    .line 808722
    if-nez v2, :cond_1

    .line 808723
    :cond_0
    :goto_0
    return v0

    .line 808724
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->f:J

    sub-long/2addr v4, v6

    .line 808725
    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gtz v3, :cond_2

    move v0, v1

    .line 808726
    goto :goto_0

    .line 808727
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    .line 808728
    sget-object v4, LX/4o7;->a:[I

    invoke-virtual {p1}, LX/31M;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 808729
    :pswitch_0
    if-lez v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 808730
    :pswitch_1
    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 808719
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->g:Z

    .line 808720
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 808712
    invoke-direct {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 808713
    const/4 v0, 0x0

    .line 808714
    :cond_0
    :goto_0
    return v0

    .line 808715
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 808716
    iput-boolean v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->h:Z

    .line 808717
    if-eqz v0, :cond_0

    .line 808718
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, -0x479902b3

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 808672
    invoke-direct {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->h()Z

    move-result v3

    if-nez v3, :cond_0

    .line 808673
    const v1, 0x507406f1

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 808674
    :goto_0
    return v0

    .line 808675
    :cond_0
    iget-boolean v3, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->h:Z

    if-nez v3, :cond_1

    .line 808676
    invoke-virtual {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 808677
    const v0, -0x22a0ea7

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 808678
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 808679
    if-nez v3, :cond_2

    .line 808680
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808681
    const v1, 0x1d2745b0

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 808682
    :cond_2
    iget-boolean v3, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->g:Z

    if-eqz v3, :cond_3

    .line 808683
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 808684
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808685
    const v1, -0x33607cd2    # -8.3630448E7f

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 808686
    :pswitch_1
    const v0, -0xf4d28cd

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 808687
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808688
    const v0, 0x65045961

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 808689
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 808690
    :pswitch_3
    iput-boolean v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->c:Z

    .line 808691
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 808692
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808693
    :cond_4
    const v1, -0x3b1846ce

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 808694
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->d:I

    .line 808695
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->e:I

    .line 808696
    iput-boolean v1, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->c:Z

    .line 808697
    const v0, -0x211213a5

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    .line 808698
    :pswitch_5
    iget-boolean v3, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->c:Z

    if-nez v3, :cond_5

    .line 808699
    iput-boolean v1, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->c:Z

    .line 808700
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->d:I

    .line 808701
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->e:I

    .line 808702
    const v0, -0x18f9a502

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    .line 808703
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    iget v4, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->d:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 808704
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    iget v5, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->e:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 808705
    if-le v3, v4, :cond_6

    iget v5, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->b:I

    if-le v3, v5, :cond_6

    .line 808706
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808707
    const v0, 0x30e845e4    # 1.6900086E-9f

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    .line 808708
    :cond_6
    if-le v4, v3, :cond_7

    iget v3, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->a:I

    if-le v4, v3, :cond_7

    .line 808709
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 808710
    const v1, 0x361119e9

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 808711
    :cond_7
    const v0, 0x45bc02b0

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final scrollTo(II)V
    .locals 2

    .prologue
    .line 808667
    invoke-virtual {p0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getScrollX()I

    move-result v0

    .line 808668
    if-eq v0, p1, :cond_0

    .line 808669
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/widget/ListViewFriendlyViewPager;->f:J

    .line 808670
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;->scrollTo(II)V

    .line 808671
    return-void
.end method
