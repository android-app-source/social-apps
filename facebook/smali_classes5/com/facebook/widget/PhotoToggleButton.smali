.class public Lcom/facebook/widget/PhotoToggleButton;
.super Lcom/facebook/widget/PhotoButton;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field public e:LX/4oG;

.field public f:LX/4mU;

.field private g:LX/4mR;

.field private h:LX/4mR;

.field public i:Landroid/animation/ObjectAnimator;

.field public j:Z

.field private k:Z

.field public l:Z

.field private m:LX/3Nk;

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 809200
    invoke-direct {p0, p1}, Lcom/facebook/widget/PhotoButton;-><init>(Landroid/content/Context;)V

    .line 809201
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    .line 809202
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    .line 809203
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    .line 809204
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    .line 809205
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->n:Z

    .line 809206
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    .line 809207
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->a(Landroid/util/AttributeSet;)V

    .line 809208
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 809209
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/PhotoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809210
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    .line 809211
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    .line 809212
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    .line 809213
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    .line 809214
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->n:Z

    .line 809215
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    .line 809216
    invoke-direct {p0, p2}, Lcom/facebook/widget/PhotoToggleButton;->a(Landroid/util/AttributeSet;)V

    .line 809217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 809121
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/PhotoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809122
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    .line 809123
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    .line 809124
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    .line 809125
    iput v0, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    .line 809126
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->n:Z

    .line 809127
    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    .line 809128
    invoke-direct {p0, p2}, Lcom/facebook/widget/PhotoToggleButton;->a(Landroid/util/AttributeSet;)V

    .line 809129
    return-void
.end method

.method private final a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 809188
    if-eqz p1, :cond_0

    .line 809189
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->PhotoToggleButton:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 809190
    const/16 v1, 0x0

    iget v2, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    .line 809191
    const/16 v1, 0x1

    iget v2, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    .line 809192
    const/16 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/widget/PhotoToggleButton;->n:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/PhotoToggleButton;->n:Z

    .line 809193
    const/16 v1, 0x2

    iget v2, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    .line 809194
    const/16 v1, 0x3

    iget v2, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    .line 809195
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 809196
    :cond_0
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->m:LX/3Nk;

    .line 809197
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->setChecked(Z)V

    .line 809198
    invoke-static {p0}, LX/4mJ;->a(Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->i:Landroid/animation/ObjectAnimator;

    .line 809199
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 809185
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    if-nez v0, :cond_0

    .line 809186
    invoke-direct {p0}, Lcom/facebook/widget/PhotoToggleButton;->f()LX/4mU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    .line 809187
    :cond_0
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f400000    # 0.75f

    .line 809178
    invoke-direct {p0}, Lcom/facebook/widget/PhotoToggleButton;->c()V

    .line 809179
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0}, LX/4mU;->a()V

    .line 809180
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    iget-object v1, p0, Lcom/facebook/widget/PhotoToggleButton;->g:LX/4mR;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 809181
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 809182
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v4}, LX/4mU;->b(F)V

    .line 809183
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v4}, LX/4mU;->d(F)V

    .line 809184
    return-void
.end method

.method public static e(Lcom/facebook/widget/PhotoToggleButton;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 809171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    .line 809172
    invoke-direct {p0}, Lcom/facebook/widget/PhotoToggleButton;->c()V

    .line 809173
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    iget-object v1, p0, Lcom/facebook/widget/PhotoToggleButton;->h:LX/4mR;

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 809174
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 809175
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v4}, LX/4mU;->b(F)V

    .line 809176
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v4}, LX/4mU;->d(F)V

    .line 809177
    return-void
.end method

.method private f()LX/4mU;
    .locals 2

    .prologue
    .line 809165
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 809166
    invoke-static {v0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v0

    check-cast v0, LX/4mV;

    .line 809167
    invoke-virtual {v0, p0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    .line 809168
    new-instance v1, LX/4oE;

    invoke-direct {v1, p0}, LX/4oE;-><init>(Lcom/facebook/widget/PhotoToggleButton;)V

    iput-object v1, p0, Lcom/facebook/widget/PhotoToggleButton;->g:LX/4mR;

    .line 809169
    new-instance v1, LX/4oF;

    invoke-direct {v1, p0}, LX/4oF;-><init>(Lcom/facebook/widget/PhotoToggleButton;)V

    iput-object v1, p0, Lcom/facebook/widget/PhotoToggleButton;->h:LX/4mR;

    .line 809170
    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 809162
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->e:LX/4oG;

    if-eqz v0, :cond_0

    .line 809163
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->e:LX/4oG;

    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->isChecked()Z

    move-result v1

    invoke-interface {v0, v1}, LX/4oG;->a(Z)V

    .line 809164
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 809218
    invoke-direct {p0}, Lcom/facebook/widget/PhotoToggleButton;->d()V

    .line 809219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->j:Z

    .line 809220
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 809117
    iget-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    if-eqz v0, :cond_0

    .line 809118
    invoke-static {p0}, Lcom/facebook/widget/PhotoToggleButton;->e(Lcom/facebook/widget/PhotoToggleButton;)V

    .line 809119
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->j:Z

    .line 809120
    return-void
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 809130
    iget-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->k:Z

    return v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 809131
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->toggle()V

    .line 809132
    invoke-direct {p0}, Lcom/facebook/widget/PhotoToggleButton;->g()V

    .line 809133
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 809134
    invoke-super {p0}, Lcom/facebook/widget/PhotoButton;->onStartTemporaryDetach()V

    .line 809135
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    if-eqz v0, :cond_0

    .line 809136
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 809137
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->i:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 809138
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v2}, LX/4mU;->a(F)V

    .line 809139
    iget-object v0, p0, Lcom/facebook/widget/PhotoToggleButton;->f:LX/4mU;

    invoke-virtual {v0, v2}, LX/4mU;->c(F)V

    .line 809140
    iput-boolean v3, p0, Lcom/facebook/widget/PhotoToggleButton;->o:Z

    .line 809141
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 809142
    invoke-virtual {p0, v3}, Lcom/facebook/widget/PhotoToggleButton;->setHasTransientState(Z)V

    .line 809143
    :cond_1
    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 809144
    if-eqz p1, :cond_1

    .line 809145
    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    invoke-virtual {p0, v1}, Lcom/facebook/widget/PhotoToggleButton;->setImageResource(I)V

    .line 809146
    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 809147
    :goto_1
    iput-boolean p1, p0, Lcom/facebook/widget/PhotoToggleButton;->k:Z

    .line 809148
    iput-boolean p1, p0, Lcom/facebook/widget/PhotoToggleButton;->l:Z

    .line 809149
    return-void

    .line 809150
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->d:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 809151
    :cond_1
    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    invoke-virtual {p0, v1}, Lcom/facebook/widget/PhotoToggleButton;->setImageResource(I)V

    .line 809152
    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/PhotoToggleButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/PhotoToggleButton;->c:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public setCheckedImageResId(I)V
    .locals 0

    .prologue
    .line 809153
    iput p1, p0, Lcom/facebook/widget/PhotoToggleButton;->b:I

    .line 809154
    return-void
.end method

.method public setOnCheckedChangeListener(LX/4oG;)V
    .locals 0

    .prologue
    .line 809155
    iput-object p1, p0, Lcom/facebook/widget/PhotoToggleButton;->e:LX/4oG;

    .line 809156
    return-void
.end method

.method public setUncheckedImageResId(I)V
    .locals 0

    .prologue
    .line 809157
    iput p1, p0, Lcom/facebook/widget/PhotoToggleButton;->a:I

    .line 809158
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 809159
    iget-boolean v0, p0, Lcom/facebook/widget/PhotoToggleButton;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->setChecked(Z)V

    .line 809160
    return-void

    .line 809161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
