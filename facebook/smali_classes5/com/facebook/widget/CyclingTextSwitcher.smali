.class public Lcom/facebook/widget/CyclingTextSwitcher;
.super LX/4nr;
.source ""


# instance fields
.field public a:Z

.field public b:[Ljava/lang/CharSequence;

.field private final c:LX/4nx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 808051
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CyclingTextSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808054
    invoke-direct {p0, p1, p2}, LX/4nr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808055
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->a:Z

    .line 808056
    new-instance v0, LX/4nx;

    invoke-direct {v0, p0}, LX/4nx;-><init>(Lcom/facebook/widget/CyclingTextSwitcher;)V

    iput-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->c:LX/4nx;

    .line 808057
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 808053
    iget-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    rem-int/2addr p1, v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/widget/CyclingTextSwitcher;I)I
    .locals 1

    .prologue
    .line 808048
    invoke-direct {p0, p1}, Lcom/facebook/widget/CyclingTextSwitcher;->a(I)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 808049
    iget-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->c:LX/4nx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4nx;->removeMessages(I)V

    .line 808050
    return-void
.end method

.method private a([Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 808040
    invoke-direct {p0}, Lcom/facebook/widget/CyclingTextSwitcher;->a()V

    .line 808041
    iput-object p1, p0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    .line 808042
    iget-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 808043
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CyclingTextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 808044
    iget-object v0, p0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-le v0, v2, :cond_1

    .line 808045
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/widget/CyclingTextSwitcher;->a$redex0(Lcom/facebook/widget/CyclingTextSwitcher;IJ)V

    .line 808046
    :cond_0
    :goto_0
    return-void

    .line 808047
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/CyclingTextSwitcher;->a()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/widget/CyclingTextSwitcher;IJ)V
    .locals 2

    .prologue
    .line 808034
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 808035
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 808036
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 808037
    long-to-int v1, p2

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 808038
    iget-object v1, p0, Lcom/facebook/widget/CyclingTextSwitcher;->c:LX/4nx;

    invoke-virtual {v1, v0, p2, p3}, LX/4nx;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 808039
    return-void
.end method


# virtual methods
.method public final a([IJLjava/util/concurrent/TimeUnit;)V
    .locals 6

    .prologue
    .line 808028
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 808029
    const/4 v0, 0x0

    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 808030
    invoke-virtual {p0}, Lcom/facebook/widget/CyclingTextSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget v4, p1, v0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 808031
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 808032
    :cond_0
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/facebook/widget/CyclingTextSwitcher;->a([Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)V

    .line 808033
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2f9fd71d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 808025
    invoke-direct {p0}, Lcom/facebook/widget/CyclingTextSwitcher;->a()V

    .line 808026
    invoke-super {p0}, LX/4nr;->onDetachedFromWindow()V

    .line 808027
    const/16 v1, 0x2d

    const v2, -0x7d24ef61

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAnimated(Z)V
    .locals 0

    .prologue
    .line 808023
    iput-boolean p1, p0, Lcom/facebook/widget/CyclingTextSwitcher;->a:Z

    .line 808024
    return-void
.end method
