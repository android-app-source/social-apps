.class public Lcom/facebook/widget/FbScrollView;
.super Landroid/widget/ScrollView;
.source ""

# interfaces
.implements LX/0h0;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 808119
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 808120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    .line 808121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808098
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808099
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    .line 808100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 808116
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    .line 808118
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 808114
    invoke-virtual {p0}, Lcom/facebook/widget/FbScrollView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/FbScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 808115
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/FbScrollView;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final asViewGroup()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 808113
    return-object p0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 808110
    iget-boolean v0, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    if-eqz v0, :cond_0

    .line 808111
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 808112
    :cond_0
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 808107
    iget-boolean v0, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    if-eqz v0, :cond_0

    .line 808108
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 808109
    :cond_0
    return-void
.end method

.method public final restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 808105
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 808106
    return-void
.end method

.method public final saveHierarchyState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 808103
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    .line 808104
    return-void
.end method

.method public setSaveFromParentEnabledCompat(Z)V
    .locals 0

    .prologue
    .line 808101
    iput-boolean p1, p0, Lcom/facebook/widget/FbScrollView;->a:Z

    .line 808102
    return-void
.end method
