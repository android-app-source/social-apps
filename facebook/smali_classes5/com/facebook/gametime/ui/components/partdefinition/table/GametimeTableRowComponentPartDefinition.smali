.class public Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/Ghy;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/Ghx;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ghx;LX/2d1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598696
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 598697
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->d:LX/Ghx;

    .line 598698
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->e:LX/2d1;

    .line 598699
    return-void
.end method

.method private a(LX/1De;LX/Ghy;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/Ghy;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 598718
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->d:LX/Ghx;

    const/4 v1, 0x0

    .line 598719
    new-instance v2, LX/Ghw;

    invoke-direct {v2, v0}, LX/Ghw;-><init>(LX/Ghx;)V

    .line 598720
    sget-object p0, LX/Ghx;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ghv;

    .line 598721
    if-nez p0, :cond_0

    .line 598722
    new-instance p0, LX/Ghv;

    invoke-direct {p0}, LX/Ghv;-><init>()V

    .line 598723
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Ghv;->a$redex0(LX/Ghv;LX/1De;IILX/Ghw;)V

    .line 598724
    move-object v2, p0

    .line 598725
    move-object v1, v2

    .line 598726
    move-object v0, v1

    .line 598727
    iget-object v1, p2, LX/Ghy;->a:LX/0Px;

    .line 598728
    iget-object v2, v0, LX/Ghv;->a:LX/Ghw;

    iput-object v1, v2, LX/Ghw;->a:LX/0Px;

    .line 598729
    iget-object v2, v0, LX/Ghv;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 598730
    move-object v0, v0

    .line 598731
    iget-object v1, p2, LX/Ghy;->b:[I

    .line 598732
    iget-object v2, v0, LX/Ghv;->a:LX/Ghw;

    iput-object v1, v2, LX/Ghw;->b:[I

    .line 598733
    iget-object v2, v0, LX/Ghv;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 598734
    move-object v0, v0

    .line 598735
    iget v1, p2, LX/Ghy;->c:I

    .line 598736
    iget-object v2, v0, LX/Ghv;->a:LX/Ghw;

    iput v1, v2, LX/Ghw;->c:I

    .line 598737
    move-object v0, v0

    .line 598738
    iget v1, p2, LX/Ghy;->d:I

    .line 598739
    iget-object v2, v0, LX/Ghv;->a:LX/Ghw;

    iput v1, v2, LX/Ghw;->d:I

    .line 598740
    move-object v0, v0

    .line 598741
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;
    .locals 6

    .prologue
    .line 598707
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;

    monitor-enter v1

    .line 598708
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598709
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598710
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598711
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598712
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Ghx;->a(LX/0QB;)LX/Ghx;

    move-result-object v4

    check-cast v4, LX/Ghx;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v5

    check-cast v5, LX/2d1;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;-><init>(Landroid/content/Context;LX/Ghx;LX/2d1;)V

    .line 598713
    move-object v0, p0

    .line 598714
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598715
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598716
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 598706
    check-cast p2, LX/Ghy;

    invoke-direct {p0, p1, p2}, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->a(LX/1De;LX/Ghy;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 598705
    check-cast p2, LX/Ghy;

    invoke-direct {p0, p1, p2}, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->a(LX/1De;LX/Ghy;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 598704
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 598703
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableRowComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 598701
    check-cast p1, LX/Ghy;

    .line 598702
    iget-object v0, p1, LX/Ghy;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/Ghy;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, LX/Ghy;->b:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 598700
    const/4 v0, 0x0

    return-object v0
.end method
