.class public Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598742
    const v0, 0x7f030787

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->a:LX/1Cz;

    .line 598743
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    const-string v1, "gametime"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598744
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598745
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 598746
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598747
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->e:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    .line 598748
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;
    .locals 6

    .prologue
    .line 598749
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    monitor-enter v1

    .line 598750
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598751
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598754
    new-instance p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;)V

    .line 598755
    move-object v0, p0

    .line 598756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 598760
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 598761
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 598762
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598763
    invoke-interface {v0}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v4

    .line 598764
    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->m()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel$OfficialPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel$OfficialPageModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel$OfficialPageModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598765
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598766
    move-object v1, v1

    .line 598767
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    .line 598768
    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel$OfficialPageModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel$OfficialPageModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598769
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598770
    move-object v2, v2

    .line 598771
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    .line 598772
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->b()I

    move-result v3

    .line 598773
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->gI_()I

    move-result v5

    .line 598774
    mul-int/lit8 v6, v3, 0x64

    add-int/2addr v3, v5

    div-int v3, v6, v3

    .line 598775
    rsub-int/lit8 v5, v3, 0x64

    .line 598776
    const v6, 0x7f0d1414

    iget-object v7, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v6, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598777
    const v1, 0x7f0d1418

    iget-object v6, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v1, v6, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598778
    const v1, 0x7f0d1415

    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " %"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598779
    const v1, 0x7f0d1417

    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " %"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598780
    invoke-interface {v0}, LX/9uc;->dd()I

    move-result v1

    if-lez v1, :cond_0

    .line 598781
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v1

    invoke-interface {v0}, LX/9uc;->dd()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 598782
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f016b

    invoke-interface {v0}, LX/9uc;->dd()I

    move-result v0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v2, v3, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 598783
    const v1, 0x7f0d1416

    iget-object v2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598784
    :cond_0
    const v6, 0x7f0d1419

    iget-object v7, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePostVotePartDefinition;->e:Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    new-instance v0, LX/Ghd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->b()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->gI_()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x7f0b22d9

    invoke-direct/range {v0 .. v5}, LX/Ghd;-><init>(IIFFI)V

    invoke-interface {p1, v6, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598785
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 598786
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598787
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598788
    invoke-interface {v0}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v0

    .line 598789
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9oF;->b()I

    move-result v1

    invoke-interface {v0}, LX/9oF;->gI_()I

    move-result v0

    add-int/2addr v0, v1

    if-lez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
