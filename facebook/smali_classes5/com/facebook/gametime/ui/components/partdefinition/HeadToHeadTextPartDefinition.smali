.class public Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Ghc;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/Gi4;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599123
    new-instance v0, LX/3ZC;

    invoke-direct {v0}, LX/3ZC;-><init>()V

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599124
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599125
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;
    .locals 3

    .prologue
    .line 599126
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    monitor-enter v1

    .line 599127
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599128
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599129
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599130
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 599131
    new-instance v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;

    invoke-direct {v0}, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;-><init>()V

    .line 599132
    move-object v0, v0

    .line 599133
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599134
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599135
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599137
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/HeadToHeadTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7a315598

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599138
    check-cast p1, LX/Ghc;

    check-cast p4, LX/Gi4;

    .line 599139
    iget-object v1, p1, LX/Ghc;->a:Ljava/lang/String;

    .line 599140
    iget-object v2, p4, LX/Gi4;->a:LX/1nq;

    invoke-virtual {v2, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v2

    iget p0, p4, LX/Gi4;->e:I

    invoke-virtual {v2, p0}, LX/1nq;->c(I)LX/1nq;

    move-result-object v2

    invoke-virtual {v2}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v2

    iput-object v2, p4, LX/Gi4;->b:Landroid/text/Layout;

    .line 599141
    iget-object v2, p4, LX/Gi4;->b:Landroid/text/Layout;

    invoke-static {v2}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v2

    int-to-float v2, v2

    iput v2, p4, LX/Gi4;->f:F

    .line 599142
    iget-object v1, p1, LX/Ghc;->b:Ljava/lang/String;

    iget v2, p1, LX/Ghc;->d:I

    .line 599143
    iget-object p0, p4, LX/Gi4;->a:LX/1nq;

    invoke-virtual {p0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1nq;->c(I)LX/1nq;

    move-result-object p0

    invoke-virtual {p0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object p0

    iput-object p0, p4, LX/Gi4;->c:Landroid/text/Layout;

    .line 599144
    iget-object v1, p1, LX/Ghc;->c:Ljava/lang/String;

    iget v2, p1, LX/Ghc;->e:I

    .line 599145
    iget-object p0, p4, LX/Gi4;->a:LX/1nq;

    invoke-virtual {p0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1nq;->c(I)LX/1nq;

    move-result-object p0

    invoke-virtual {p0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object p0

    iput-object p0, p4, LX/Gi4;->d:Landroid/text/Layout;

    .line 599146
    iget-object p0, p4, LX/Gi4;->d:Landroid/text/Layout;

    invoke-static {p0}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result p0

    int-to-float p0, p0

    iput p0, p4, LX/Gi4;->g:F

    .line 599147
    const/16 v1, 0x1f

    const v2, -0x6d5cc4a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599148
    check-cast p1, LX/Ghc;

    .line 599149
    iget-object v0, p1, LX/Ghc;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/Ghc;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/Ghc;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
