.class public Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/GhZ;",
        "LX/Gha;",
        "TE;",
        "LX/Gi3;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

.field private final d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/39G;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field private final i:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

.field public final j:LX/0hy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598855
    new-instance v0, LX/3ZB;

    invoke-direct {v0}, LX/3ZB;-><init>()V

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a:LX/1Cz;

    .line 598856
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;LX/39G;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;LX/0hy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598857
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598858
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    .line 598859
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    .line 598860
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598861
    iput-object p4, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 598862
    iput-object p5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->g:LX/39G;

    .line 598863
    iput-object p6, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->h:Lcom/facebook/content/SecureContextHelper;

    .line 598864
    iput-object p7, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->i:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    .line 598865
    iput-object p8, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->j:LX/0hy;

    .line 598866
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;
    .locals 12

    .prologue
    .line 598867
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    monitor-enter v1

    .line 598868
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598869
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598870
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598871
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598872
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v8

    check-cast v8, LX/39G;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v11

    check-cast v11, LX/0hy;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;-><init>(Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;LX/39G;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;LX/0hy;)V

    .line 598873
    move-object v0, v3

    .line 598874
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598875
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598876
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/9o7;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 13

    .prologue
    .line 598878
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    invoke-interface {p0}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel$ShareableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 598879
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 598880
    move-object v0, v0

    .line 598881
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    const v2, 0x7794e0b2

    .line 598882
    iput v2, v1, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 598883
    move-object v1, v1

    .line 598884
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 598885
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 598886
    move-object v0, v0

    .line 598887
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 598888
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    invoke-interface {p0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 598889
    iput-object v2, v1, LX/2dc;->h:Ljava/lang/String;

    .line 598890
    move-object v1, v1

    .line 598891
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 598892
    new-instance v2, LX/3dL;

    invoke-direct {v2}, LX/3dL;-><init>()V

    invoke-interface {p0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 598893
    iput-object v3, v2, LX/3dL;->aK:Ljava/lang/String;

    .line 598894
    move-object v2, v2

    .line 598895
    iput-object v1, v2, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 598896
    move-object v1, v2

    .line 598897
    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 598898
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    invoke-interface {p0}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 598899
    iput-object v3, v2, LX/23u;->N:Ljava/lang/String;

    .line 598900
    move-object v2, v2

    .line 598901
    invoke-interface {p0}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 598902
    iput-object v3, v2, LX/23u;->m:Ljava/lang/String;

    .line 598903
    move-object v2, v2

    .line 598904
    invoke-interface {p0}, LX/9o7;->gH_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;

    move-result-object v3

    .line 598905
    if-nez v3, :cond_0

    .line 598906
    const/4 v4, 0x0

    .line 598907
    :goto_0
    move-object v3, v4

    .line 598908
    iput-object v3, v2, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 598909
    move-object v2, v2

    .line 598910
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 598911
    iput-object v1, v2, LX/23u;->d:LX/0Px;

    .line 598912
    move-object v1, v2

    .line 598913
    iput-object v0, v1, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 598914
    move-object v0, v1

    .line 598915
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    .line 598916
    :cond_0
    new-instance v6, LX/3dM;

    invoke-direct {v6}, LX/3dM;-><init>()V

    .line 598917
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->b()Z

    move-result v4

    .line 598918
    iput-boolean v4, v6, LX/3dM;->d:Z

    .line 598919
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->c()Z

    move-result v4

    .line 598920
    iput-boolean v4, v6, LX/3dM;->e:Z

    .line 598921
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->d()Z

    move-result v4

    invoke-virtual {v6, v4}, LX/3dM;->c(Z)LX/3dM;

    .line 598922
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->e()Z

    move-result v4

    .line 598923
    iput-boolean v4, v6, LX/3dM;->g:Z

    .line 598924
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->ac_()Z

    move-result v4

    .line 598925
    iput-boolean v4, v6, LX/3dM;->h:Z

    .line 598926
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->ad_()Z

    move-result v4

    .line 598927
    iput-boolean v4, v6, LX/3dM;->i:Z

    .line 598928
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->j()Z

    move-result v4

    invoke-virtual {v6, v4}, LX/3dM;->g(Z)LX/3dM;

    .line 598929
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->s()Z

    move-result v4

    .line 598930
    iput-boolean v4, v6, LX/3dM;->k:Z

    .line 598931
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->k()Z

    move-result v4

    .line 598932
    iput-boolean v4, v6, LX/3dM;->l:Z

    .line 598933
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->l()Ljava/lang/String;

    move-result-object v4

    .line 598934
    iput-object v4, v6, LX/3dM;->p:Ljava/lang/String;

    .line 598935
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->m()Z

    move-result v4

    invoke-virtual {v6, v4}, LX/3dM;->j(Z)LX/3dM;

    .line 598936
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    .line 598937
    iput-object v4, v6, LX/3dM;->y:Ljava/lang/String;

    .line 598938
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->t()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v4

    .line 598939
    if-nez v4, :cond_3

    .line 598940
    const/4 v5, 0x0

    .line 598941
    :goto_1
    move-object v4, v5

    .line 598942
    iput-object v4, v6, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 598943
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->o()Z

    move-result v4

    invoke-virtual {v6, v4}, LX/3dM;->l(Z)LX/3dM;

    .line 598944
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->p()Ljava/lang/String;

    move-result-object v4

    .line 598945
    iput-object v4, v6, LX/3dM;->D:Ljava/lang/String;

    .line 598946
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->u()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$LikersModel;

    move-result-object v4

    .line 598947
    if-nez v4, :cond_7

    .line 598948
    const/4 v5, 0x0

    .line 598949
    :goto_2
    move-object v4, v5

    .line 598950
    invoke-virtual {v6, v4}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 598951
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->v()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v4

    .line 598952
    if-nez v4, :cond_8

    .line 598953
    const/4 v5, 0x0

    .line 598954
    :goto_3
    move-object v4, v5

    .line 598955
    invoke-virtual {v6, v4}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 598956
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->q()Ljava/lang/String;

    move-result-object v4

    .line 598957
    iput-object v4, v6, LX/3dM;->J:Ljava/lang/String;

    .line 598958
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->w()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$ResharesModel;

    move-result-object v4

    .line 598959
    if-nez v4, :cond_9

    .line 598960
    const/4 v5, 0x0

    .line 598961
    :goto_4
    move-object v4, v5

    .line 598962
    iput-object v4, v6, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 598963
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->x()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 598964
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 598965
    const/4 v4, 0x0

    move v5, v4

    :goto_5
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_1

    .line 598966
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 598967
    if-nez v4, :cond_a

    .line 598968
    const/4 v8, 0x0

    .line 598969
    :goto_6
    move-object v4, v8

    .line 598970
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 598971
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 598972
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 598973
    iput-object v4, v6, LX/3dM;->N:LX/0Px;

    .line 598974
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->y()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$TopLevelCommentsModel;

    move-result-object v4

    .line 598975
    if-nez v4, :cond_b

    .line 598976
    const/4 v5, 0x0

    .line 598977
    :goto_7
    move-object v4, v5

    .line 598978
    invoke-virtual {v6, v4}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 598979
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v4

    .line 598980
    if-nez v4, :cond_c

    .line 598981
    const/4 v5, 0x0

    .line 598982
    :goto_8
    move-object v4, v5

    .line 598983
    invoke-virtual {v6, v4}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 598984
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->r()LX/59N;

    move-result-object v4

    .line 598985
    if-nez v4, :cond_11

    .line 598986
    const/4 v5, 0x0

    .line 598987
    :goto_9
    move-object v4, v5

    .line 598988
    iput-object v4, v6, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 598989
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->A()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v4

    .line 598990
    if-nez v4, :cond_12

    .line 598991
    const/4 v5, 0x0

    .line 598992
    :goto_a
    move-object v4, v5

    .line 598993
    iput-object v4, v6, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 598994
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;->B()I

    move-result v4

    invoke-virtual {v6, v4}, LX/3dM;->b(I)LX/3dM;

    .line 598995
    invoke-virtual {v6}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    goto/16 :goto_0

    .line 598996
    :cond_3
    new-instance v8, LX/4Wx;

    invoke-direct {v8}, LX/4Wx;-><init>()V

    .line 598997
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 598998
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 598999
    const/4 v5, 0x0

    move v7, v5

    :goto_b
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v7, v5, :cond_4

    .line 599000
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 599001
    if-nez v5, :cond_6

    .line 599002
    const/4 v10, 0x0

    .line 599003
    :goto_c
    move-object v5, v10

    .line 599004
    invoke-virtual {v9, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 599005
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_b

    .line 599006
    :cond_4
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 599007
    iput-object v5, v8, LX/4Wx;->b:LX/0Px;

    .line 599008
    :cond_5
    invoke-virtual {v8}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v5

    goto/16 :goto_1

    .line 599009
    :cond_6
    new-instance v10, LX/3dL;

    invoke-direct {v10}, LX/3dL;-><init>()V

    .line 599010
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 599011
    iput-object v11, v10, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 599012
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v11

    .line 599013
    iput-object v11, v10, LX/3dL;->ag:Ljava/lang/String;

    .line 599014
    invoke-virtual {v10}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v10

    goto :goto_c

    .line 599015
    :cond_7
    new-instance v5, LX/3dN;

    invoke-direct {v5}, LX/3dN;-><init>()V

    .line 599016
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$LikersModel;->a()I

    move-result v7

    .line 599017
    iput v7, v5, LX/3dN;->b:I

    .line 599018
    invoke-virtual {v5}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v5

    goto/16 :goto_2

    .line 599019
    :cond_8
    new-instance v5, LX/3dO;

    invoke-direct {v5}, LX/3dO;-><init>()V

    .line 599020
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v7

    .line 599021
    iput v7, v5, LX/3dO;->b:I

    .line 599022
    invoke-virtual {v5}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v5

    goto/16 :goto_3

    .line 599023
    :cond_9
    new-instance v5, LX/4Ya;

    invoke-direct {v5}, LX/4Ya;-><init>()V

    .line 599024
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$ResharesModel;->a()I

    move-result v7

    .line 599025
    iput v7, v5, LX/4Ya;->b:I

    .line 599026
    invoke-virtual {v5}, LX/4Ya;->a()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v5

    goto/16 :goto_4

    .line 599027
    :cond_a
    new-instance v8, LX/4WL;

    invoke-direct {v8}, LX/4WL;-><init>()V

    .line 599028
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v9

    .line 599029
    iput v9, v8, LX/4WL;->b:I

    .line 599030
    invoke-virtual {v8}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v8

    goto/16 :goto_6

    .line 599031
    :cond_b
    new-instance v5, LX/4ZH;

    invoke-direct {v5}, LX/4ZH;-><init>()V

    .line 599032
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel$TopLevelCommentsModel;->a()I

    move-result v7

    .line 599033
    iput v7, v5, LX/4ZH;->b:I

    .line 599034
    invoke-virtual {v5}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v5

    goto/16 :goto_7

    .line 599035
    :cond_c
    new-instance v8, LX/3dQ;

    invoke-direct {v8}, LX/3dQ;-><init>()V

    .line 599036
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_e

    .line 599037
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 599038
    const/4 v5, 0x0

    move v7, v5

    :goto_d
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v7, v5, :cond_d

    .line 599039
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 599040
    if-nez v5, :cond_f

    .line 599041
    const/4 v10, 0x0

    .line 599042
    :goto_e
    move-object v5, v10

    .line 599043
    invoke-virtual {v9, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 599044
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_d

    .line 599045
    :cond_d
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 599046
    iput-object v5, v8, LX/3dQ;->b:LX/0Px;

    .line 599047
    :cond_e
    invoke-virtual {v8}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v5

    goto/16 :goto_8

    .line 599048
    :cond_f
    new-instance v10, LX/4ZJ;

    invoke-direct {v10}, LX/4ZJ;-><init>()V

    .line 599049
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v11

    .line 599050
    if-nez v11, :cond_10

    .line 599051
    const/4 v12, 0x0

    .line 599052
    :goto_f
    move-object v11, v12

    .line 599053
    iput-object v11, v10, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 599054
    invoke-virtual {v5}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v11

    .line 599055
    iput v11, v10, LX/4ZJ;->c:I

    .line 599056
    invoke-virtual {v10}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v10

    goto :goto_e

    .line 599057
    :cond_10
    new-instance v12, LX/4WM;

    invoke-direct {v12}, LX/4WM;-><init>()V

    .line 599058
    invoke-virtual {v11}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result p0

    .line 599059
    iput p0, v12, LX/4WM;->f:I

    .line 599060
    invoke-virtual {v12}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v12

    goto :goto_f

    .line 599061
    :cond_11
    new-instance v5, LX/4XY;

    invoke-direct {v5}, LX/4XY;-><init>()V

    .line 599062
    invoke-interface {v4}, LX/59N;->b()Ljava/lang/String;

    move-result-object v7

    .line 599063
    iput-object v7, v5, LX/4XY;->ag:Ljava/lang/String;

    .line 599064
    invoke-interface {v4}, LX/59N;->c()Ljava/lang/String;

    move-result-object v7

    .line 599065
    iput-object v7, v5, LX/4XY;->aT:Ljava/lang/String;

    .line 599066
    invoke-interface {v4}, LX/59N;->d()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/9tr;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 599067
    iput-object v7, v5, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 599068
    invoke-virtual {v5}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    goto/16 :goto_9

    .line 599069
    :cond_12
    new-instance v5, LX/33O;

    invoke-direct {v5}, LX/33O;-><init>()V

    .line 599070
    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 599071
    iput-object v7, v5, LX/33O;->aI:Ljava/lang/String;

    .line 599072
    invoke-virtual {v5}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    goto/16 :goto_a
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599073
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 599074
    check-cast p2, LX/GhZ;

    check-cast p3, LX/1Pn;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 599075
    iget-object v2, p2, LX/GhZ;->a:LX/9o7;

    .line 599076
    invoke-static {v2}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->a(LX/9o7;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 599077
    const v4, 0x7f0d142f

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->i:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayContentViewComponentPartDefinition;

    invoke-interface {p1, v4, v5, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599078
    const v4, 0x7f0d1430

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutPartDefinition;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599079
    const v4, 0x7f0d1431

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    new-instance v6, LX/20y;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    invoke-direct {v6, v7, v0}, LX/20y;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599080
    const v4, 0x7f0d1431

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v6, LX/20d;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    sget-object v8, LX/1Wi;->HAS_FOLLOWUP_SECTION:LX/1Wi;

    invoke-direct {v6, v7, v8}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599081
    iget-object v4, p2, LX/GhZ;->b:LX/Ghb;

    sget-object v5, LX/Ghb;->SHOW_COMMENT:LX/Ghb;

    if-ne v4, v5, :cond_1

    invoke-interface {v2}, LX/9o7;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;

    move-result-object v4

    .line 599082
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->c()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    move v4, v5

    .line 599083
    if-eqz v4, :cond_1

    .line 599084
    :goto_1
    if-eqz v0, :cond_0

    .line 599085
    const v4, 0x7f0d1432

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v6, LX/GhY;

    invoke-direct {v6, p0, v2, p3}, LX/GhY;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;LX/9o7;LX/1Pn;)V

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599086
    :cond_0
    new-instance v2, LX/Gha;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/Gha;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V

    return-object v2

    :cond_1
    move v0, v1

    .line 599087
    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5898f1b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599088
    check-cast p1, LX/GhZ;

    check-cast p2, LX/Gha;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/Gi3;

    const/4 p0, 0x0

    .line 599089
    iget-object v1, p1, LX/GhZ;->a:LX/9o7;

    .line 599090
    iget-object v2, p2, LX/Gha;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 599091
    iget-object v5, p4, LX/Gi3;->b:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    move-object v5, v5

    .line 599092
    invoke-static {v2, v4, v5}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 599093
    iget-object v2, p4, LX/Gi3;->d:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    move-object v2, v2

    .line 599094
    iget-boolean v4, p2, LX/Gha;->b:Z

    if-eqz v4, :cond_1

    .line 599095
    invoke-interface {v1}, LX/9o7;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;

    move-result-object v1

    .line 599096
    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5, v6}, LX/3Wq;->a(Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 599097
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel$AuthorModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 599098
    iget-object v5, v2, LX/3Wq;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 599099
    iget-object v5, v2, LX/3Wq;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 599100
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$PreviewCommentModel;->c()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/3Wq;->setBody(Ljava/lang/CharSequence;)V

    .line 599101
    invoke-virtual {v2, p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->setVisibility(I)V

    .line 599102
    :goto_0
    iget-object v1, p4, LX/Gi3;->a:Landroid/widget/LinearLayout;

    move-object v2, v1

    .line 599103
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 599104
    iget-object v4, p1, LX/GhZ;->b:LX/Ghb;

    sget-object v5, LX/Ghb;->WITH_BORDER:LX/Ghb;

    if-eq v4, v5, :cond_0

    iget-object v4, p1, LX/GhZ;->b:LX/Ghb;

    sget-object v5, LX/Ghb;->WITH_INSET_BORDER:LX/Ghb;

    if-ne v4, v5, :cond_3

    .line 599105
    :cond_0
    iput p0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 599106
    iput p0, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 599107
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 599108
    iget-object v1, p1, LX/GhZ;->b:LX/Ghb;

    sget-object v4, LX/Ghb;->WITH_BORDER:LX/Ghb;

    if-ne v1, v4, :cond_2

    const v1, 0x7f020bf1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 599109
    :goto_2
    const/16 v1, 0x1f

    const v2, 0x67096be4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 599110
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->setVisibility(I)V

    goto :goto_0

    .line 599111
    :cond_2
    const v1, 0x7f020bef

    goto :goto_1

    .line 599112
    :cond_3
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b22dc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 599113
    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 599114
    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 599115
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 599116
    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599117
    check-cast p1, LX/GhZ;

    .line 599118
    iget-object v0, p1, LX/GhZ;->a:LX/9o7;

    .line 599119
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9o7;->n()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$StoryModel$ShareableModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9o7;->gH_()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFeedbackFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9o7;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9o7;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel;->d()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel$ActingTeamModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
