.class public Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:LX/0tX;

.field private final g:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598838
    const v0, 0x7f030788

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->a:LX/1Cz;

    .line 598839
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    const-string v1, "gametime"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598831
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598832
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 598833
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598834
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598835
    iput-object p4, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->g:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 598836
    iput-object p5, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->f:LX/0tX;

    .line 598837
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;
    .locals 9

    .prologue
    .line 598820
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    monitor-enter v1

    .line 598821
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598822
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598823
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598824
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598825
    new-instance v3, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/0tX;)V

    .line 598826
    move-object v0, v3

    .line 598827
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598828
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598829
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598830
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 598840
    new-instance v0, LX/GgU;

    invoke-direct {v0}, LX/GgU;-><init>()V

    .line 598841
    new-instance v1, LX/4J5;

    invoke-direct {v1}, LX/4J5;-><init>()V

    .line 598842
    const-string v2, "match_page_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 598843
    const-string v2, "voted_page_id"

    invoke-virtual {v1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 598844
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 598845
    const-string p1, "reaction_styles"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 598846
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 598847
    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->f:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, p4

    .line 598848
    check-cast v0, LX/1Pr;

    new-instance v1, LX/Gi2;

    invoke-direct {v1, p3}, LX/Gi2;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-interface {v0, v1, p3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gi1;

    .line 598849
    iput-boolean v3, v0, LX/Gi1;->a:Z

    .line 598850
    iget-object v1, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 598851
    invoke-interface {v1}, LX/9uc;->az()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 598852
    iput-boolean v1, v0, LX/Gi1;->b:Z

    .line 598853
    new-array v0, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    invoke-static {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p4, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 598854
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 598790
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 598794
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    const/4 v9, 0x0

    .line 598795
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598796
    invoke-interface {v0}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v0

    .line 598797
    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v1

    invoke-interface {v0}, LX/9oF;->c()LX/9oA;

    move-result-object v2

    invoke-interface {v2}, LX/9oA;->a()LX/9o9;

    move-result-object v2

    invoke-interface {v2}, LX/9o9;->b()LX/9o8;

    move-result-object v2

    invoke-interface {v2}, LX/9o8;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v2, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598798
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598799
    move-object v1, v1

    .line 598800
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    .line 598801
    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v2

    invoke-interface {v0}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v3

    invoke-interface {v3}, LX/9oE;->a()LX/9oD;

    move-result-object v3

    invoke-interface {v3}, LX/9oD;->b()LX/9oC;

    move-result-object v3

    invoke-interface {v3}, LX/9oC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598802
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598803
    move-object v2, v2

    .line 598804
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v4

    .line 598805
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598806
    invoke-interface {v2}, LX/9uc;->az()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 598807
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598808
    invoke-interface {v2}, LX/9uc;->cF()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 598809
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598810
    invoke-interface {v2}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;->q()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$MatchPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$MatchPageModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 598811
    const v5, 0x7f0d1414

    iget-object v7, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v5, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598812
    const v1, 0x7f0d1418

    iget-object v5, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v1, v5, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598813
    const v1, 0x7f0d141b

    iget-object v4, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/9oF;->c()LX/9oA;

    move-result-object v5

    invoke-interface {v5}, LX/9oA;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v1, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598814
    const v1, 0x7f0d141d

    iget-object v4, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/9oF;->gJ_()LX/9oE;

    move-result-object v0

    invoke-interface {v0}, LX/9oE;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598815
    const v7, 0x7f0d141a

    iget-object v8, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v0, LX/Ghe;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/Ghe;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)V

    invoke-interface {p1, v7, v8, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598816
    const v7, 0x7f0d141c

    iget-object v8, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v0, LX/Ghf;

    move-object v1, p0

    move-object v3, v6

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/Ghf;-><init>(Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)V

    invoke-interface {p1, v7, v8, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598817
    const v0, 0x7f0d141a

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->g:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v1, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598818
    const v0, 0x7f0d141c

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoritePreVotePartDefinition;->g:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v1, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598819
    return-object v9
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598791
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598792
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598793
    invoke-interface {v0}, LX/9uc;->bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/gametime/ui/components/partdefinition/fanfavorite/GametimeFanFavoriteUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
