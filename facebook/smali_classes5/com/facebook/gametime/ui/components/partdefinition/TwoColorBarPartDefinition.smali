.class public Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Ghd;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/gametime/ui/components/view/TwoColorBarView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599172
    new-instance v0, LX/3ZD;

    invoke-direct {v0}, LX/3ZD;-><init>()V

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599154
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599155
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;
    .locals 3

    .prologue
    .line 599161
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    monitor-enter v1

    .line 599162
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599163
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599164
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599165
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 599166
    new-instance v0, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;

    invoke-direct {v0}, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;-><init>()V

    .line 599167
    move-object v0, v0

    .line 599168
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599169
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599170
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599171
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599173
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/TwoColorBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x53ab9970

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599156
    check-cast p1, LX/Ghd;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;

    .line 599157
    iget v1, p1, LX/Ghd;->e:I

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a(ILandroid/content/Context;)V

    .line 599158
    iget v1, p1, LX/Ghd;->c:F

    iget v2, p1, LX/Ghd;->d:F

    invoke-virtual {p4, v1, v2}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a(FF)V

    .line 599159
    iget v1, p1, LX/Ghd;->a:I

    iget v2, p1, LX/Ghd;->b:I

    invoke-virtual {p4, v1, v2}, Lcom/facebook/gametime/ui/components/view/TwoColorBarView;->a(II)V

    .line 599160
    const/16 v1, 0x1f

    const v2, 0x7e3333b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599152
    check-cast p1, LX/Ghd;

    const/4 v1, 0x0

    .line 599153
    iget v0, p1, LX/Ghd;->c:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p1, LX/Ghd;->d:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
