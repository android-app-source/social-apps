.class public Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598688
    const-class v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;

    const-string v1, "gametime"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 598689
    const v0, 0x7f030786

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598690
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598691
    iput-object p1, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 598692
    iput-object p2, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 598693
    iput-object p3, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598694
    iput-object p4, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->f:LX/0Or;

    .line 598695
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 598637
    const-class v1, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;

    monitor-enter v1

    .line 598638
    :try_start_0
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598639
    sput-object v2, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598640
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598641
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598642
    new-instance v6, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Or;)V

    .line 598643
    move-object v0, v6

    .line 598644
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 598687
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 598660
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598661
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v1, LX/E1o;

    .line 598662
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598663
    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    .line 598664
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 598665
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 598666
    invoke-direct {v1, v2, v3, v4}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598667
    const v0, 0x7f0d0df9

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v2, LX/2f8;

    invoke-direct {v2}, LX/2f8;-><init>()V

    .line 598668
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 598669
    if-eqz v3, :cond_0

    .line 598670
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 598671
    invoke-interface {v3}, LX/9uc;->t()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 598672
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 598673
    invoke-interface {v3}, LX/9uc;->t()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;->d()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 598674
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 598675
    invoke-interface {v3}, LX/9uc;->t()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;->d()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 598676
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 598677
    invoke-interface {v3}, LX/9uc;->t()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;->d()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    .line 598678
    :goto_0
    move-object v3, v3

    .line 598679
    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 598680
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598681
    move-object v2, v2

    .line 598682
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598683
    const v0, 0x7f0d0dfa

    iget-object v1, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598684
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598685
    invoke-interface {v2}, LX/9uc;->cj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598686
    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 598648
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 598649
    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/gametime/ui/components/partdefinition/ComposerUnitComponentPartDefinition;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 598650
    :goto_0
    return v0

    .line 598651
    :cond_1
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598652
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 598653
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 598654
    goto :goto_0

    .line 598655
    :cond_2
    if-eqz v0, :cond_4

    .line 598656
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->d()LX/5LG;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 598657
    if-nez v3, :cond_3

    .line 598658
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->hc_()LX/2rX;

    move-result-object v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 598659
    if-eqz v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method
