.class public Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static b:LX/03R;


# instance fields
.field private final a:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 804929
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b:LX/03R;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804927
    iput-object p1, p0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a:LX/03V;

    .line 804928
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;
    .locals 2

    .prologue
    .line 804924
    new-instance v1, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;-><init>(LX/03V;)V

    .line 804925
    return-object v1
.end method

.method private declared-synchronized b(Ljavax/net/ssl/SSLContext;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 804908
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljavax/net/ssl/SSLContext;->getClientSessionContext()Ljavax/net/ssl/SSLSessionContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 804909
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 804910
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 804911
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "sslCtxNativePointer"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 804912
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 804913
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 804914
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 804915
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 804916
    :goto_1
    if-eqz v0, :cond_0

    .line 804917
    invoke-direct {p0, v0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->nativeApply(I)Z

    move-result v0

    .line 804918
    if-nez v0, :cond_0

    .line 804919
    iget-object v0, p0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a:LX/03V;

    const-string v1, "heartbleed"

    const-string v2, "could not init"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 804920
    :catch_0
    goto :goto_0

    .line 804921
    :cond_2
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 804922
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1

    .line 804923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()Z
    .locals 2

    .prologue
    .line 804899
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    .line 804900
    sget-object v0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 804901
    :goto_0
    monitor-exit p0

    return v0

    .line 804902
    :cond_0
    :try_start_1
    const-string v0, "heartbleed"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 804903
    sget-object v0, LX/03R;->YES:LX/03R;

    sput-object v0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b:LX/03R;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804904
    const/4 v0, 0x1

    goto :goto_0

    .line 804905
    :catch_0
    :try_start_2
    sget-object v0, LX/03R;->NO:LX/03R;

    sput-object v0, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b:LX/03R;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 804906
    const/4 v0, 0x0

    goto :goto_0

    .line 804907
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private native isHeartbeatActivated()Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeApply(I)Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final declared-synchronized a(Ljavax/net/ssl/SSLContext;)V
    .locals 1

    .prologue
    .line 804892
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804893
    invoke-direct {p0, p1}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b(Ljavax/net/ssl/SSLContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804894
    :cond_0
    monitor-exit p0

    return-void

    .line 804895
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 804896
    invoke-direct {p0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 804897
    const/4 v0, 0x0

    .line 804898
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/ssl/openssl/heartbleed/HeartbleedMitigation;->isHeartbeatActivated()Z

    move-result v0

    goto :goto_0
.end method

.method public native wasCallbackInvoked()Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end method
