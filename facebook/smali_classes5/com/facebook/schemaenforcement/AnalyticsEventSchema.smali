.class public Lcom/facebook/schemaenforcement/AnalyticsEventSchema;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/schemaenforcement/AnalyticsEventSchemaDeserializer;
.end annotation


# static fields
.field public static final a:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/schemaenforcement/AnalyticsEventSchema;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mClientEnforcement:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "clientEnforcement"
    .end annotation
.end field

.field private final mCollectionValueDataTypeMap:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "collectionValueDataType"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInheritsFrom:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "inherits_from"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOptionalColumnsMap:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optionalColumns"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPoc:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "poc"
    .end annotation
.end field

.field private final mRequiredColumnsMap:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "requiredColumns"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSubscribers:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subscribers"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 804247
    const-class v0, Lcom/facebook/schemaenforcement/AnalyticsEventSchemaDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 804246
    new-instance v0, LX/4lN;

    invoke-direct {v0}, LX/4lN;-><init>()V

    sput-object v0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->a:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 804213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804214
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mPoc:Ljava/lang/String;

    .line 804215
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mSubscribers:Ljava/lang/String;

    .line 804216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mClientEnforcement:Z

    .line 804217
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mRequiredColumnsMap:Ljava/util/Map;

    .line 804218
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mOptionalColumnsMap:Ljava/util/Map;

    .line 804219
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mCollectionValueDataTypeMap:Ljava/util/Map;

    .line 804220
    iput-object v1, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mInheritsFrom:Ljava/util/List;

    .line 804221
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 804236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mPoc:Ljava/lang/String;

    .line 804238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mSubscribers:Ljava/lang/String;

    .line 804239
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mClientEnforcement:Z

    .line 804240
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mRequiredColumnsMap:Ljava/util/Map;

    .line 804241
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mOptionalColumnsMap:Ljava/util/Map;

    .line 804242
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mCollectionValueDataTypeMap:Ljava/util/Map;

    .line 804243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mInheritsFrom:Ljava/util/List;

    .line 804244
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mInheritsFrom:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 804245
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 804235
    iget-boolean v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mClientEnforcement:Z

    return v0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 804234
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mRequiredColumnsMap:Ljava/util/Map;

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 804233
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mOptionalColumnsMap:Ljava/util/Map;

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 804232
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mCollectionValueDataTypeMap:Ljava/util/Map;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 804231
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 804230
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mInheritsFrom:Ljava/util/List;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 804222
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mPoc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 804223
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mSubscribers:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 804224
    iget-boolean v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mClientEnforcement:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 804225
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mRequiredColumnsMap:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 804226
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mOptionalColumnsMap:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 804227
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mCollectionValueDataTypeMap:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 804228
    iget-object v0, p0, Lcom/facebook/schemaenforcement/AnalyticsEventSchema;->mInheritsFrom:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 804229
    return-void
.end method
