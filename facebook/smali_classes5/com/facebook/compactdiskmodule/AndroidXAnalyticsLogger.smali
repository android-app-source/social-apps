.class public Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/compactdisk/XAnalyticsLogger;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/compactdisk/XAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571260
    const-class v0, Lcom/facebook/compactdisk/XAnalyticsLogger;

    sput-object v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 571232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571233
    iput-object p1, p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->b:LX/0Zb;

    .line 571234
    iput-object p2, p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->c:LX/0lC;

    .line 571235
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;
    .locals 5

    .prologue
    .line 571236
    sget-object v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->d:Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    if-nez v0, :cond_1

    .line 571237
    const-class v1, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    monitor-enter v1

    .line 571238
    :try_start_0
    sget-object v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->d:Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 571239
    if-eqz v2, :cond_0

    .line 571240
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 571241
    new-instance p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-direct {p0, v3, v4}, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;-><init>(LX/0Zb;LX/0lC;)V

    .line 571242
    move-object v0, p0

    .line 571243
    sput-object v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->d:Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571244
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 571245
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 571246
    :cond_1
    sget-object v0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->d:Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;

    return-object v0

    .line 571247
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 571248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public logEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 571249
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 571250
    iput-object p2, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 571251
    :try_start_0
    iget-object v0, p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->c:LX/0lC;

    invoke-virtual {v0, p3}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 571252
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    .line 571253
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571254
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 571255
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 571256
    :catch_0
    move-exception v0

    .line 571257
    sget-object v1, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->a:Ljava/lang/Class;

    const-string v2, "Could not deserialize JSON"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 571258
    :goto_1
    return-void

    .line 571259
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;->b:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
