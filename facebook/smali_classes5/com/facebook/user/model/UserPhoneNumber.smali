.class public Lcom/facebook/user/model/UserPhoneNumber;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/UserPhoneNumber;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 807105
    new-instance v0, LX/4nb;

    invoke-direct {v0}, LX/4nb;-><init>()V

    sput-object v0, Lcom/facebook/user/model/UserPhoneNumber;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 807106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    .line 807108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    .line 807109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    .line 807110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    .line 807111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03R;->valueOf(Ljava/lang/String;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    .line 807112
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 807115
    sget-object v5, LX/03R;->UNSET:LX/03R;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V

    .line 807116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 807113
    sget-object v5, LX/03R;->UNSET:LX/03R;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V

    .line 807114
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V
    .locals 0

    .prologue
    .line 807094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807095
    iput-object p1, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    .line 807096
    iput-object p2, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    .line 807097
    iput-object p3, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    .line 807098
    iput p4, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    .line 807099
    iput-object p5, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    .line 807100
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807103
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807104
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807102
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 807101
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 807090
    iget v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v0, v0

    .line 807091
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 807092
    iget v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    move v0, v0

    .line 807093
    const/16 v1, 0x11

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 807073
    if-ne p0, p1, :cond_1

    .line 807074
    :cond_0
    :goto_0
    return v0

    .line 807075
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 807076
    :cond_3
    check-cast p1, Lcom/facebook/user/model/UserPhoneNumber;

    .line 807077
    iget v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    iget v3, p1, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 807078
    goto :goto_0

    .line 807079
    :cond_4
    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 807080
    goto :goto_0

    .line 807081
    :cond_6
    iget-object v2, p1, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 807082
    :cond_7
    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    iget-object v3, p1, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 807083
    goto :goto_0

    .line 807084
    :cond_8
    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 807085
    goto :goto_0

    .line 807086
    :cond_a
    iget-object v2, p1, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 807087
    :cond_b
    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 807088
    goto :goto_0

    .line 807089
    :cond_c
    iget-object v2, p1, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 807064
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 807065
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 807066
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 807067
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    add-int/2addr v0, v2

    .line 807068
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    invoke-virtual {v1}, LX/03R;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 807069
    return v0

    :cond_1
    move v0, v1

    .line 807070
    goto :goto_0

    :cond_2
    move v0, v1

    .line 807071
    goto :goto_1

    :cond_3
    move v0, v1

    .line 807072
    goto :goto_2
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 807058
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807059
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807060
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807061
    iget v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 807062
    iget-object v0, p0, Lcom/facebook/user/model/UserPhoneNumber;->e:LX/03R;

    invoke-virtual {v0}, LX/03R;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807063
    return-void
.end method
