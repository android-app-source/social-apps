.class public Lcom/facebook/user/model/UserSmsIdentifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/user/model/UserIdentifier;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/user/model/UserSmsIdentifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 807145
    new-instance v0, LX/4nc;

    invoke-direct {v0}, LX/4nc;-><init>()V

    sput-object v0, Lcom/facebook/user/model/UserSmsIdentifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 807141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    .line 807143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    .line 807144
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 807137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807138
    iput-object p1, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    .line 807139
    iput-object p1, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    .line 807140
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 807133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807134
    iput-object p1, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    .line 807135
    iput-object p2, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    .line 807136
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807132
    iget-object v0, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 807131
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 807125
    if-ne p0, p1, :cond_1

    .line 807126
    :cond_0
    :goto_0
    return v0

    .line 807127
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 807128
    goto :goto_0

    .line 807129
    :cond_3
    check-cast p1, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 807130
    iget-object v2, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 807120
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/user/model/UserSmsIdentifier;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 807121
    invoke-virtual {p0}, Lcom/facebook/user/model/UserSmsIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807122
    iget-object v0, p0, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    move-object v0, v0

    .line 807123
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 807124
    return-void
.end method
