.class public Lcom/facebook/user/model/NameSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/user/model/Name;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 626620
    const-class v0, Lcom/facebook/user/model/Name;

    new-instance v1, Lcom/facebook/user/model/NameSerializer;

    invoke-direct {v1}, Lcom/facebook/user/model/NameSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 626621
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 626608
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/user/model/Name;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 626614
    if-nez p0, :cond_0

    .line 626615
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 626616
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 626617
    invoke-static {p0, p1, p2}, Lcom/facebook/user/model/NameSerializer;->b(Lcom/facebook/user/model/Name;LX/0nX;LX/0my;)V

    .line 626618
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 626619
    return-void
.end method

.method private static b(Lcom/facebook/user/model/Name;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 626610
    const-string v0, "firstName"

    iget-object v1, p0, Lcom/facebook/user/model/Name;->firstName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626611
    const-string v0, "lastName"

    iget-object v1, p0, Lcom/facebook/user/model/Name;->lastName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626612
    const-string v0, "displayName"

    iget-object v1, p0, Lcom/facebook/user/model/Name;->displayName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626613
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 626609
    check-cast p1, Lcom/facebook/user/model/Name;

    invoke-static {p1, p2, p3}, Lcom/facebook/user/model/NameSerializer;->a(Lcom/facebook/user/model/Name;LX/0nX;LX/0my;)V

    return-void
.end method
