.class public Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/BGN;",
        "TE;",
        "LX/AkC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final c:LX/24B;

.field private final d:LX/BGN;

.field private final e:LX/BGU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586801
    new-instance v0, LX/3Uh;

    invoke-direct {v0}, LX/3Uh;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/BGN;LX/BGU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586795
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586796
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586797
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->c:LX/24B;

    .line 586798
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->d:LX/BGN;

    .line 586799
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->e:LX/BGU;

    .line 586800
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;
    .locals 7

    .prologue
    .line 586784
    const-class v1, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;

    monitor-enter v1

    .line 586785
    :try_start_0
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586786
    sput-object v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586787
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586788
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586789
    new-instance p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v4

    check-cast v4, LX/24B;

    invoke-static {v0}, LX/BGN;->b(LX/0QB;)LX/BGN;

    move-result-object v5

    check-cast v5, LX/BGN;

    const-class v6, LX/BGU;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BGU;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/BGN;LX/BGU;)V

    .line 586790
    move-object v0, p0

    .line 586791
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586792
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586793
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586802
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586777
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    .line 586778
    iget-object v6, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->c:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586779
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->c:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->e:LX/BGU;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 586780
    new-instance p2, LX/BGT;

    invoke-static {v1}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    invoke-static {v1}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v7

    check-cast v7, LX/1Rg;

    invoke-direct {p2, v6, v7, v5}, LX/BGT;-><init>(LX/0hB;LX/1Rg;Landroid/content/Context;)V

    .line 586781
    move-object v5, p2

    .line 586782
    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 586783
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptsPromptPartDefinition;->d:LX/BGN;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x670378c3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586758
    check-cast p1, LX/1Ri;

    check-cast p2, LX/BGN;

    check-cast p4, LX/AkC;

    .line 586759
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/BGS;

    .line 586760
    iget-object v2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, LX/BGS;->a(LX/1RN;)V

    .line 586761
    iget-object v1, p1, LX/1Ri;->a:LX/1RN;

    .line 586762
    iput-object v1, p2, LX/BGN;->c:LX/1RN;

    .line 586763
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586764
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586765
    const/16 v1, 0x1f

    const v2, -0x2b1fd12e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 586771
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 586772
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 586773
    instance-of v2, v0, LX/1kW;

    if-nez v2, :cond_0

    move v0, v1

    .line 586774
    :goto_0
    return v0

    :cond_0
    check-cast v0, LX/1kW;

    .line 586775
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 586776
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586766
    check-cast p2, LX/BGN;

    check-cast p4, LX/AkC;

    const/4 v1, 0x0

    .line 586767
    iput-object v1, p2, LX/BGN;->c:LX/1RN;

    .line 586768
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586769
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586770
    return-void
.end method
