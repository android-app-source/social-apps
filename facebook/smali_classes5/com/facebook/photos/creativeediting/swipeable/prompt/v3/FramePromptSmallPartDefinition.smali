.class public Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Qa;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/BGZ;",
        "TE;",
        "LX/BMR;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/8GN;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

.field private final e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587138
    new-instance v0, LX/3Up;

    invoke-direct {v0}, LX/3Up;-><init>()V

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/8GN;Landroid/content/res/Resources;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587132
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587133
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->b:LX/8GN;

    .line 587134
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->c:Landroid/content/res/Resources;

    .line 587135
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    .line 587136
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587137
    return-void
.end method

.method private a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;Landroid/content/Context;)LX/BGY;
    .locals 7

    .prologue
    .line 587127
    new-instance v2, LX/5iG;

    const/4 v0, 0x0

    const-string v1, ""

    const-string v3, ""

    invoke-direct {v2, v0, v1, v3}, LX/5iG;-><init>(LX/1aX;Ljava/lang/String;Ljava/lang/String;)V

    .line 587128
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 587129
    iget-object v5, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->d:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, p2}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->a(Landroid/net/Uri;Landroid/content/Context;)LX/1aX;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/1aX;)V

    .line 587130
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 587131
    :cond_0
    new-instance v0, LX/BGY;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->c:Landroid/content/res/Resources;

    invoke-direct {v0, v2, v1}, LX/BGY;-><init>(LX/5iG;Landroid/content/res/Resources;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;
    .locals 7

    .prologue
    .line 587116
    const-class v1, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;

    monitor-enter v1

    .line 587117
    :try_start_0
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587118
    sput-object v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587119
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587120
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587121
    new-instance p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;

    invoke-static {v0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v3

    check-cast v3, LX/8GN;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->b(LX/0QB;)Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;-><init>(LX/8GN;Landroid/content/res/Resources;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V

    .line 587122
    move-object v0, p0

    .line 587123
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587124
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587125
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587126
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BMR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587115
    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 587108
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Ps;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 587109
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {p2}, LX/Ak0;->a(LX/1Ri;)LX/Ak0;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587110
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587111
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 587112
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    .line 587113
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->b:LX/8GN;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b116c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b116c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;II)LX/0Px;

    move-result-object v2

    .line 587114
    new-instance v3, LX/BGZ;

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;Landroid/content/Context;)LX/BGY;

    move-result-object v1

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-le v0, v6, :cond_0

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;Landroid/content/Context;)LX/BGY;

    move-result-object v0

    :goto_0
    invoke-direct {v3, v1, v0}, LX/BGZ;-><init>(LX/BGY;LX/BGY;)V

    return-object v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1610433c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587097
    check-cast p2, LX/BGZ;

    check-cast p4, LX/BMR;

    .line 587098
    iget-object v1, p2, LX/BGZ;->a:LX/BGY;

    iget-object v2, p2, LX/BGZ;->b:LX/BGY;

    invoke-virtual {p4, v1, v2}, LX/BMR;->a(LX/Alv;LX/Alv;)V

    .line 587099
    const/16 v1, 0x1f

    const v2, -0x3eeff870

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587100
    check-cast p1, LX/1Ri;

    .line 587101
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587102
    if-eqz v0, :cond_1

    .line 587103
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object p0, p0

    .line 587104
    if-eqz p0, :cond_1

    .line 587105
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object p0, p0

    .line 587106
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 587107
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
