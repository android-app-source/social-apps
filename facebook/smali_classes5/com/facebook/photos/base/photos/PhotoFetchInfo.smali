.class public Lcom/facebook/photos/base/photos/PhotoFetchInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/base/photos/PhotoFetchInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3WA;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589133
    new-instance v0, LX/3W9;

    invoke-direct {v0}, LX/3W9;-><init>()V

    sput-object v0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 589143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 589144
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3WA;

    iput-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->a:LX/3WA;

    .line 589145
    iput-object p2, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589146
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 589139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 589140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3WA;->getFetchCauseFromName(Ljava/lang/String;)LX/3WA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->a:LX/3WA;

    .line 589141
    const-class v0, Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589142
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 589138
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 589134
    iget-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->a:LX/3WA;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->a:LX/3WA;

    invoke-virtual {v0}, LX/3WA;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 589135
    iget-object v0, p0, Lcom/facebook/photos/base/photos/PhotoFetchInfo;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 589136
    return-void

    .line 589137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
