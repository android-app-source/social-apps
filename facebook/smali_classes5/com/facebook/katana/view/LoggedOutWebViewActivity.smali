.class public Lcom/facebook/katana/view/LoggedOutWebViewActivity;
.super Lcom/facebook/katana/activity/BaseFacebookActivity;
.source ""

# interfaces
.implements LX/0l6;


# static fields
.field public static final C:Ljava/lang/String;


# instance fields
.field public A:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Landroid/webkit/ValueCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Landroid/webkit/WebView;

.field public q:LX/Gwp;

.field private r:Landroid/view/View;

.field public s:Landroid/view/View;

.field public t:LX/44G;

.field public u:LX/03V;

.field public v:Landroid/content/ComponentName;

.field public w:Lcom/facebook/content/SecureContextHelper;

.field public x:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public y:LX/48V;

.field public z:Ljava/lang/Class;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 569498
    const-class v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 569496
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;-><init>()V

    .line 569497
    return-void
.end method

.method private a(LX/44G;LX/03V;Landroid/content/ComponentName;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/48V;Ljava/lang/Class;)V
    .locals 0
    .param p3    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Class;
        .annotation build Lcom/facebook/katana/view/FbLoginActivityClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 569446
    iput-object p1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->t:LX/44G;

    .line 569447
    iput-object p2, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    .line 569448
    iput-object p3, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->v:Landroid/content/ComponentName;

    .line 569449
    iput-object p4, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->w:Lcom/facebook/content/SecureContextHelper;

    .line 569450
    iput-object p5, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 569451
    iput-object p6, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->y:LX/48V;

    .line 569452
    iput-object p7, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->z:Ljava/lang/Class;

    .line 569453
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-static {v7}, LX/44G;->b(LX/0QB;)LX/44G;

    move-result-object v1

    check-cast v1, LX/44G;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v7}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-static {v7}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v7}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v6

    check-cast v6, LX/48V;

    const-class p0, Lcom/facebook/katana/UriAuthHandler;

    move-object p0, p0

    move-object v7, p0

    check-cast v7, Ljava/lang/Class;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->a(LX/44G;LX/03V;Landroid/content/ComponentName;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/48V;Ljava/lang/Class;)V

    return-void
.end method

.method public static l(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V
    .locals 4

    .prologue
    .line 569492
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->q:LX/Gwp;

    invoke-virtual {v0}, LX/Gwp;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 569493
    :goto_0
    return-void

    .line 569494
    :catch_0
    move-exception v0

    .line 569495
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    sget-object v2, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    const-string v3, "failed to hide spinner, bad state"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 569499
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 569500
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 569501
    :goto_0
    return-void

    .line 569502
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 569503
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 569504
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 569505
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 569506
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 569507
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 569464
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b(Landroid/os/Bundle;)V

    .line 569465
    invoke-static {p0, p0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 569466
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 569467
    new-instance v1, LX/Gwp;

    invoke-direct {v1, p0}, LX/Gwp;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->q:LX/Gwp;

    .line 569468
    new-instance v1, Lcom/facebook/webview/BasicWebView;

    invoke-direct {v1, p0}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    .line 569469
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    .line 569470
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    new-instance v2, LX/Gwl;

    invoke-direct {v2, p0}, LX/Gwl;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    const-string v3, "fbLoggedOutWebViewIsErrorPage"

    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 569471
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->r:Landroid/view/View;

    .line 569472
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->r:Landroid/view/View;

    const v2, 0x7f01027a

    invoke-static {p0, v2, v5}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 569473
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 569474
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    new-instance v2, LX/Gwo;

    invoke-direct {v2, p0}, LX/Gwo;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 569475
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    new-instance v2, LX/Gwn;

    invoke-direct {v2, p0}, LX/Gwn;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 569476
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-static {v1, v4, v7}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 569477
    if-nez p1, :cond_0

    .line 569478
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->y:LX/48V;

    iget-object v2, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 569479
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setClickable(Z)V

    .line 569480
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 569481
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 569482
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 569483
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 569484
    invoke-virtual {p0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0305f1

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    .line 569485
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 569486
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    new-instance v2, LX/Gwm;

    invoke-direct {v2, p0}, LX/Gwm;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 569487
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 569488
    invoke-virtual {p0, v0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->setContentView(Landroid/view/View;)V

    .line 569489
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->q:LX/Gwp;

    invoke-virtual {v0}, LX/Gwp;->show()V

    .line 569490
    return-void

    .line 569491
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v1, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ImprovedNewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 569454
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->A:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 569455
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 569456
    :goto_0
    iget-object v2, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->A:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 569457
    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->A:Landroid/webkit/ValueCallback;

    .line 569458
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 569459
    goto :goto_0

    .line 569460
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 569461
    invoke-static {p2, p3}, Landroid/webkit/WebChromeClient$FileChooserParams;->parseResult(ILandroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v0

    .line 569462
    iget-object v2, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 569463
    iput-object v1, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    goto :goto_1
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 569443
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 569444
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 569445
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 569440
    invoke-super {p0, p1}, Lcom/facebook/katana/activity/BaseFacebookActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 569441
    iget-object v0, p0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 569442
    return-void
.end method
