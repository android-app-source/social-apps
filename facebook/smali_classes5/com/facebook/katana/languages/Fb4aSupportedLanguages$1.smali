.class public final Lcom/facebook/katana/languages/Fb4aSupportedLanguages$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0WA;


# direct methods
.method public constructor <init>(LX/0WA;)V
    .locals 0

    .prologue
    .line 566984
    iput-object p1, p0, Lcom/facebook/katana/languages/Fb4aSupportedLanguages$1;->a:LX/0WA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 566977
    iget-object v0, p0, Lcom/facebook/katana/languages/Fb4aSupportedLanguages$1;->a:LX/0WA;

    .line 566978
    iget-boolean v1, v0, LX/0WA;->h:Z

    if-nez v1, :cond_0

    .line 566979
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0WA;->h:Z

    .line 566980
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fbresources_disabled_fb4a"

    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 566981
    const-string v1, "is_local_build"

    iget-object p0, v0, LX/0WA;->c:LX/0WV;

    invoke-virtual {p0}, LX/0WV;->d()Z

    move-result p0

    invoke-virtual {v2, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 566982
    iget-object v1, v0, LX/0WA;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 566983
    :cond_0
    return-void
.end method
