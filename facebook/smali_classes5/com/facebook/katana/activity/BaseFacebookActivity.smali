.class public Lcom/facebook/katana/activity/BaseFacebookActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f5;
.implements LX/10W;
.implements LX/2A2;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private p:LX/0kX;

.field private q:LX/03V;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 569508
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 569509
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->mp_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0T2;

    .line 569510
    instance-of v2, v0, LX/0kX;

    if-eqz v2, :cond_0

    .line 569511
    check-cast v0, LX/0kX;

    iput-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->p:LX/0kX;

    .line 569512
    return-void

    .line 569513
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to find a FbActivityListener of type FacebookActivityDelegate"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 569514
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 569515
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 569516
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->q:LX/03V;

    .line 569517
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 569518
    invoke-virtual {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->b()LX/0kX;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0kX;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 569519
    return-void
.end method

.method public final declared-synchronized b()LX/0kX;
    .locals 1

    .prologue
    .line 569520
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->p:LX/0kX;

    if-nez v0, :cond_0

    .line 569521
    invoke-direct {p0}, Lcom/facebook/katana/activity/BaseFacebookActivity;->a()V

    .line 569522
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->p:LX/0kX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 569523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569524
    iget-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->p:LX/0kX;

    if-eqz v0, :cond_0

    .line 569525
    iget-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->p:LX/0kX;

    invoke-virtual {v0}, LX/0kX;->g()LX/0P1;

    move-result-object v0

    .line 569526
    :goto_0
    return-object v0

    .line 569527
    :cond_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 569528
    goto :goto_0
.end method

.method public titleBarPrimaryActionClickHandler(Landroid/view/View;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 569529
    iget-object v0, p0, Lcom/facebook/katana/activity/BaseFacebookActivity;->q:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Either override titleBarPrimaryActionClickHandler or call FBAD.setPrimaryClickHandler for the click handling."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 569530
    return-void
.end method
