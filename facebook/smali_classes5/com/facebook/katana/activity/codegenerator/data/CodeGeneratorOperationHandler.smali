.class public Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/11H;

.field public final b:LX/2gf;

.field public final c:LX/2gg;

.field public final d:LX/2gh;

.field public final e:LX/2gi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 577915
    const-class v0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/2gf;LX/2gg;LX/2gh;LX/2gi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 577916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577917
    iput-object p1, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a:LX/11H;

    .line 577918
    iput-object p2, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->b:LX/2gf;

    .line 577919
    iput-object p3, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->c:LX/2gg;

    .line 577920
    iput-object p4, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->d:LX/2gh;

    .line 577921
    iput-object p5, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->e:LX/2gi;

    .line 577922
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;
    .locals 9

    .prologue
    .line 577923
    const-class v1, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;

    monitor-enter v1

    .line 577924
    :try_start_0
    sget-object v0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 577925
    sput-object v2, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 577926
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577927
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 577928
    new-instance v3, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    .line 577929
    new-instance v5, LX/2gf;

    invoke-direct {v5}, LX/2gf;-><init>()V

    .line 577930
    move-object v5, v5

    .line 577931
    move-object v5, v5

    .line 577932
    check-cast v5, LX/2gf;

    .line 577933
    new-instance v6, LX/2gg;

    invoke-direct {v6}, LX/2gg;-><init>()V

    .line 577934
    move-object v6, v6

    .line 577935
    move-object v6, v6

    .line 577936
    check-cast v6, LX/2gg;

    .line 577937
    new-instance v7, LX/2gh;

    invoke-direct {v7}, LX/2gh;-><init>()V

    .line 577938
    move-object v7, v7

    .line 577939
    move-object v7, v7

    .line 577940
    check-cast v7, LX/2gh;

    .line 577941
    new-instance v8, LX/2gi;

    invoke-direct {v8}, LX/2gi;-><init>()V

    .line 577942
    move-object v8, v8

    .line 577943
    move-object v8, v8

    .line 577944
    check-cast v8, LX/2gi;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;-><init>(LX/11H;LX/2gf;LX/2gg;LX/2gh;LX/2gi;)V

    .line 577945
    move-object v0, v3

    .line 577946
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 577947
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577948
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 577949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 577950
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 577951
    const-string v1, "legacy_check_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 577952
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 577953
    const-string v1, "checkCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeParams;

    .line 577954
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->d:LX/2gh;

    sget-object v3, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/CheckCodeResult;

    .line 577955
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 577956
    :goto_0
    return-object v0

    .line 577957
    :cond_0
    const-string v1, "fetch_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577958
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 577959
    const-string v1, "checkCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeParams;

    .line 577960
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->b:LX/2gf;

    sget-object v3, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 577961
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 577962
    goto :goto_0

    .line 577963
    :cond_1
    const-string v1, "legacy_fetch_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 577964
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 577965
    const-string v1, "checkCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/LegacyFetchCodeParams;

    .line 577966
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->c:LX/2gg;

    sget-object v3, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 577967
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 577968
    goto :goto_0

    .line 577969
    :cond_2
    const-string v1, "activation_code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 577970
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 577971
    const-string v1, "checkCodeParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/ActivationCodeParams;

    .line 577972
    iget-object v1, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->a:LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->e:LX/2gi;

    sget-object v3, Lcom/facebook/katana/activity/codegenerator/data/CodeGeneratorOperationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 577973
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 577974
    goto :goto_0

    .line 577975
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
