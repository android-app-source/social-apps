.class public final Lcom/facebook/katana/notification/impl/AppBadgingInitializer$AppBadgingListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2LY;


# direct methods
.method public constructor <init>(LX/2LY;)V
    .locals 0

    .prologue
    .line 571522
    iput-object p1, p0, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$AppBadgingListener$1;->a:LX/2LY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 571523
    iget-object v0, p0, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$AppBadgingListener$1;->a:LX/2LY;

    iget-object v0, v0, LX/2LY;->a:LX/2LL;

    iget-object v1, v0, LX/2LL;->f:LX/2LM;

    iget-object v0, p0, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$AppBadgingListener$1;->a:LX/2LY;

    iget-object v0, v0, LX/2LY;->a:LX/2LL;

    .line 571524
    iget-object v2, v0, LX/2LL;->l:LX/0ad;

    sget-short v3, LX/15r;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 571525
    if-nez v2, :cond_0

    const/4 v3, 0x0

    .line 571526
    iget-boolean v2, v0, LX/2LL;->n:Z

    if-eqz v2, :cond_8

    move v2, v3

    .line 571527
    :goto_0
    move v2, v2

    .line 571528
    if-eqz v2, :cond_0

    iget-object v2, v0, LX/2LL;->h:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/2LL;->h:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 571529
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, LX/2LM;->a(I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 571530
    return-void

    .line 571531
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/notification/impl/AppBadgingInitializer$AppBadgingListener$1;->a:LX/2LY;

    .line 571532
    iget-object v2, v0, LX/2LY;->a:LX/2LL;

    iget-object v2, v2, LX/2LL;->e:LX/0xB;

    sget-object v3, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v2, v3}, LX/0xB;->a(LX/12j;)I

    move-result v3

    .line 571533
    iget-object v2, v0, LX/2LY;->a:LX/2LL;

    iget-object v2, v2, LX/2LL;->e:LX/0xB;

    sget-object v4, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v2, v4}, LX/0xB;->a(LX/12j;)I

    move-result v2

    .line 571534
    iget-object v4, v0, LX/2LY;->a:LX/2LL;

    iget-object v4, v4, LX/2LL;->g:LX/0xW;

    invoke-virtual {v4}, LX/0xW;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/2LY;->a:LX/2LL;

    iget-object v4, v4, LX/2LL;->g:LX/0xW;

    invoke-virtual {v4}, LX/0xW;->o()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 571535
    :cond_2
    add-int/2addr v2, v3

    .line 571536
    :cond_3
    :goto_3
    move v2, v2

    .line 571537
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-object v3, v3, LX/2LL;->j:LX/23i;

    sget-object v4, LX/03R;->UNSET:LX/03R;

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, LX/23i;->a(LX/03R;Z)Z

    move-result v3

    if-nez v3, :cond_4

    .line 571538
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-object v3, v3, LX/2LL;->e:LX/0xB;

    sget-object v4, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v3, v4}, LX/0xB;->a(LX/12j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 571539
    :cond_4
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-boolean v3, v3, LX/2LL;->m:Z

    if-eqz v3, :cond_5

    .line 571540
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-object v3, v3, LX/2LL;->e:LX/0xB;

    sget-object v4, LX/12j;->VIDEO_HOME:LX/12j;

    invoke-virtual {v3, v4}, LX/0xB;->a(LX/12j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 571541
    :cond_5
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-object v3, v3, LX/2LL;->o:LX/0fO;

    invoke-virtual {v3}, LX/0fO;->p()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 571542
    iget-object v3, v0, LX/2LY;->a:LX/2LL;

    iget-object v3, v3, LX/2LL;->e:LX/0xB;

    sget-object v4, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v3, v4}, LX/0xB;->a(LX/12j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 571543
    :cond_6
    move v0, v2

    .line 571544
    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_1

    .line 571545
    :cond_8
    iget-object v2, v0, LX/2LL;->d:LX/00H;

    .line 571546
    iget-object v4, v2, LX/00H;->j:LX/01T;

    move-object v2, v4

    .line 571547
    sget-object v4, LX/01T;->FB4A:LX/01T;

    if-eq v2, v4, :cond_9

    move v2, v3

    .line 571548
    goto/16 :goto_0

    .line 571549
    :cond_9
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 571550
    sget-object v4, LX/2LZ;->HTC:LX/2LZ;

    invoke-virtual {v4}, LX/2LZ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 571551
    sget-object v2, LX/2LZ;->HTC:LX/2LZ;

    .line 571552
    :goto_4
    move-object v2, v2

    .line 571553
    sget-object v4, LX/2La;->a:[I

    invoke-virtual {v2}, LX/2LZ;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 571554
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 571555
    :pswitch_0
    iget-object v2, v0, LX/2LL;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    goto/16 :goto_0

    .line 571556
    :pswitch_1
    iget-object v2, v0, LX/2LL;->a:LX/0Uh;

    const/16 v4, 0x377

    invoke-virtual {v2, v4, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    goto/16 :goto_0

    .line 571557
    :cond_a
    sget-object v4, LX/2LZ;->SONY:LX/2LZ;

    invoke-virtual {v4}, LX/2LZ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 571558
    sget-object v2, LX/2LZ;->SONY:LX/2LZ;

    goto :goto_4

    .line 571559
    :cond_b
    sget-object v4, LX/2LZ;->LG:LX/2LZ;

    invoke-virtual {v4}, LX/2LZ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 571560
    sget-object v2, LX/2LZ;->LG:LX/2LZ;

    goto :goto_4

    .line 571561
    :cond_c
    sget-object v4, LX/2LZ;->OPPO:LX/2LZ;

    invoke-virtual {v4}, LX/2LZ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 571562
    sget-object v2, LX/2LZ;->OPPO:LX/2LZ;

    goto :goto_4

    .line 571563
    :cond_d
    sget-object v2, LX/2LZ;->UNKNOWN:LX/2LZ;

    goto :goto_4

    .line 571564
    :cond_e
    iget-object v4, v0, LX/2LY;->a:LX/2LL;

    iget-object v4, v4, LX/2LL;->g:LX/0xW;

    invoke-virtual {v4}, LX/0xW;->p()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 571565
    if-gt v2, v3, :cond_3

    move v2, v3

    goto/16 :goto_3

    :cond_f
    move v2, v3

    .line 571566
    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
