.class public Lcom/facebook/katana/platform/FacebookAuthenticationService;
.super LX/0te;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/Gvz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568956
    const-class v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;

    .line 568957
    sput-object v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 568954
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 568955
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 568952
    invoke-static {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 568953
    array-length v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 568918
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 568919
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 568920
    invoke-static {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .line 568921
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 568922
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 568923
    :goto_1
    return-object v0

    .line 568924
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 568925
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 568947
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 568948
    if-nez v0, :cond_0

    .line 568949
    sget-object v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a:Ljava/lang/Class;

    const-string v1, "No callback IBinder"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 568950
    :goto_0
    return-void

    .line 568951
    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 568939
    const-string v0, "accountAuthenticatorResponse"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    .line 568940
    if-nez v0, :cond_0

    .line 568941
    sget-object v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a:Ljava/lang/Class;

    const-string v1, "No callback IBinder"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 568942
    :goto_0
    return-void

    .line 568943
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568944
    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568945
    const-string v2, "accountType"

    const-string v3, "com.facebook.auth.login"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568946
    invoke-virtual {v0, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 568933
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 568934
    const-string v1, "com.facebook.auth.login"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 568935
    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Landroid/accounts/Account;

    :cond_0
    return-object v0

    .line 568936
    :catch_0
    move-exception v0

    .line 568937
    sget-object v1, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a:Ljava/lang/Class;

    const-string v2, "Unexpected error"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 568938
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 568928
    const-string v0, "android.accounts.AccountAuthenticator"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568929
    iget-object v0, p0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->c:LX/Gvz;

    invoke-virtual {v0}, LX/Gvz;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 568930
    :goto_0
    return-object v0

    .line 568931
    :cond_0
    sget-object v0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bound with unknown intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 568932
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x7542c74d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 568926
    new-instance v1, LX/Gvz;

    invoke-direct {v1, p0}, LX/Gvz;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/katana/platform/FacebookAuthenticationService;->c:LX/Gvz;

    .line 568927
    const/16 v1, 0x25

    const v2, 0x7fb256ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
