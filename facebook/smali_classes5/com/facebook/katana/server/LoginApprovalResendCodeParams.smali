.class public Lcom/facebook/katana/server/LoginApprovalResendCodeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/katana/server/LoginApprovalResendCodeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568745
    new-instance v0, LX/Gwe;

    invoke-direct {v0}, LX/Gwe;-><init>()V

    sput-object v0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 568741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568742
    iput-wide p1, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->a:J

    .line 568743
    iput-object p3, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->b:Ljava/lang/String;

    .line 568744
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 568733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568734
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->a:J

    .line 568735
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->b:Ljava/lang/String;

    .line 568736
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 568740
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 568737
    iget-wide v0, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 568738
    iget-object v0, p0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568739
    return-void
.end method
