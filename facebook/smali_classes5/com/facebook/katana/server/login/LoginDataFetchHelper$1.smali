.class public final Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/2Xs;


# direct methods
.method public constructor <init>(LX/2Xs;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 577760
    iput-object p1, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iput-object p2, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const v5, 0x23000a

    .line 577761
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x23000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 577762
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 577763
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/335;

    .line 577764
    invoke-interface {v0}, LX/335;->a()LX/2ZE;

    move-result-object v0

    .line 577765
    if-eqz v0, :cond_0

    .line 577766
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 577767
    :catch_0
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x57

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 577768
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->c:LX/2A0;

    .line 577769
    iget-object v1, v0, LX/2A0;->a:LX/0Xp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 577770
    return-void

    .line 577771
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->f:LX/28W;

    const-string v2, "fetchLoginData-batch"

    iget-object v3, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v4, v4, LX/2Xs;->e:LX/11Q;

    invoke-virtual {v4}, LX/11Q;->a()LX/14U;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 577772
    iget-object v0, p0, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;->b:LX/2Xs;

    iget-object v0, v0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x23000a

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
