.class public Lcom/facebook/katana/server/handler/Fb4aAuthHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:LX/2Xk;

.field private final e:LX/2Xl;

.field public final f:LX/2Xn;

.field public final g:LX/28S;

.field public final h:LX/28T;

.field private final i:LX/28U;

.field public final j:LX/11H;

.field private final k:LX/2Xo;

.field private final l:LX/0WJ;

.field private final m:LX/03V;

.field public final n:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final o:LX/2Xr;

.field public final p:LX/2Xs;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/11Q;

.field private final s:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final t:LX/0y2;

.field private final u:LX/0Uh;

.field private final v:Ljava/util/concurrent/ExecutorService;

.field private final w:Lcom/facebook/auth/login/AuthOperations;

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0fW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 569826
    const-class v0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/2Xk;LX/2Xl;LX/2Xn;LX/28S;LX/28T;LX/28U;LX/11H;LX/2Xo;LX/0WJ;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Xr;LX/2Xs;LX/0Ot;LX/11Q;Lcom/facebook/performancelogger/PerformanceLogger;LX/0y2;LX/0Uh;Ljava/util/concurrent/ExecutorService;Lcom/facebook/auth/login/AuthOperations;LX/0Ot;LX/0fW;)V
    .locals 1
    .param p20    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p21    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;",
            "LX/2Xk;",
            "LX/2Xl;",
            "LX/2Xn;",
            "LX/28S;",
            "LX/28T;",
            "LX/28U;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/2Xo;",
            "LX/0WJ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2Xr;",
            "LX/2Xs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;",
            "LX/11Q;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0y2;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/auth/login/AuthOperations;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/0fW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 569800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569801
    iput-object p1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b:LX/0Ot;

    .line 569802
    iput-object p2, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->c:LX/0Ot;

    .line 569803
    iput-object p3, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->d:LX/2Xk;

    .line 569804
    iput-object p4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->e:LX/2Xl;

    .line 569805
    iput-object p5, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->f:LX/2Xn;

    .line 569806
    iput-object p6, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->g:LX/28S;

    .line 569807
    iput-object p7, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->h:LX/28T;

    .line 569808
    iput-object p8, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->i:LX/28U;

    .line 569809
    iput-object p9, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->j:LX/11H;

    .line 569810
    iput-object p10, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->k:LX/2Xo;

    .line 569811
    iput-object p11, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->l:LX/0WJ;

    .line 569812
    iput-object p12, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->m:LX/03V;

    .line 569813
    iput-object p13, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 569814
    iput-object p14, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->o:LX/2Xr;

    .line 569815
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->p:LX/2Xs;

    .line 569816
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->q:LX/0Ot;

    .line 569817
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->r:LX/11Q;

    .line 569818
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->s:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 569819
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->t:LX/0y2;

    .line 569820
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->u:LX/0Uh;

    .line 569821
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->v:Ljava/util/concurrent/ExecutorService;

    .line 569822
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->w:Lcom/facebook/auth/login/AuthOperations;

    .line 569823
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    .line 569824
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->y:LX/0fW;

    .line 569825
    return-void
.end method

.method public static a(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 4
    .param p1    # LX/0e6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;",
            "Lcom/facebook/auth/component/AuthenticationResult;",
            ">;TPARAMS;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/auth/component/AuthenticationResult;"
        }
    .end annotation

    .prologue
    .line 569797
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    .line 569798
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->v:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/katana/server/handler/Fb4aAuthHandler$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler$1;-><init>(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Lcom/facebook/auth/component/AuthenticationResult;)V

    const v3, 0x34dba773

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569799
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 569777
    :try_start_0
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 569778
    iget-object v2, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->j:LX/11H;

    iget-object v3, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->e:LX/2Xl;

    new-instance v4, LX/41y;

    const/4 v5, 0x1

    invoke-direct {v4, p1, v1, v5}, LX/41y;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    sget-object v1, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    move-object v9, v0

    .line 569779
    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v6

    .line 569780
    new-instance v10, Lcom/facebook/auth/protocol/AuthenticationResultImpl;

    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->a()Ljava/lang/String;

    move-result-object v11

    new-instance v1, Lcom/facebook/auth/credentials/FacebookCredentials;

    .line 569781
    iget-object v0, v6, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    move-object v2, v0

    .line 569782
    iget-object v0, v6, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v3, v0

    .line 569783
    iget-object v0, v6, Lcom/facebook/auth/credentials/FacebookCredentials;->c:Ljava/lang/String;

    move-object v4, v0

    .line 569784
    iget-object v0, v6, Lcom/facebook/auth/credentials/FacebookCredentials;->d:Ljava/lang/String;

    move-object v5, v0

    .line 569785
    iget-object v0, v6, Lcom/facebook/auth/credentials/FacebookCredentials;->e:Ljava/lang/String;

    move-object v6, v0

    .line 569786
    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/auth/credentials/FacebookCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->d()LX/03R;

    move-result-object v6

    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->e()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v9}, Lcom/facebook/auth/component/AuthenticationResult;->f()Ljava/lang/String;

    move-result-object v8

    move-object v2, v10

    move-object v3, v11

    move-object v4, v1

    invoke-direct/range {v2 .. v8}, Lcom/facebook/auth/protocol/AuthenticationResultImpl;-><init>(Ljava/lang/String;Lcom/facebook/auth/credentials/FacebookCredentials;Ljava/lang/String;LX/03R;Ljava/lang/String;Ljava/lang/String;)V

    .line 569787
    invoke-interface {v10}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 569788
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 569789
    sget-object v2, LX/26p;->f:LX/0Tn;

    invoke-interface {v10}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 569790
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 569791
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->l:LX/0WJ;

    invoke-interface {v10}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0WJ;->a(Lcom/facebook/auth/credentials/FacebookCredentials;)V

    .line 569792
    invoke-static {p0, v10}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a$redex0(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Lcom/facebook/auth/component/AuthenticationResult;)V

    .line 569793
    invoke-static {v9}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 569794
    :catch_0
    move-exception v1

    .line 569795
    invoke-static {p0, v1}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Ljava/lang/Exception;)V

    .line 569796
    throw v1
.end method

.method public static a$redex0(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 5

    .prologue
    const v4, 0x4e0003

    .line 569766
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569767
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16B;

    .line 569768
    :try_start_0
    invoke-virtual {v0, p1}, LX/16B;->a(Lcom/facebook/auth/component/AuthenticationResult;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 569769
    :catch_0
    move-exception v1

    .line 569770
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "AuthComponent.authComplete failure"

    invoke-static {v0, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 569771
    :catch_1
    move-exception v3

    .line 569772
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->m:LX/03V;

    const-string v1, "AuthComponent.authComplete failure"

    invoke-virtual {v0, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 569773
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 569774
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-nez v1, :cond_0

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 569775
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569776
    return-void
.end method

.method private b(LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PARAMS:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0e6",
            "<TPARAMS;",
            "Lcom/facebook/auth/component/AuthenticationResult;",
            ">;TPARAMS;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/auth/component/AuthenticationResult;"
        }
    .end annotation

    .prologue
    .line 569722
    :try_start_0
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->l:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 569723
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->w:Lcom/facebook/auth/login/AuthOperations;

    invoke-virtual {v1}, Lcom/facebook/auth/login/AuthOperations;->a()V

    .line 569724
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->m:LX/03V;

    const-string v2, "LogoutDidNotComplete"

    const-string v3, "Trying to login, but logout did not complete."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 569725
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569726
    invoke-virtual {v1}, LX/16B;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 569727
    :catch_0
    move-exception v1

    .line 569728
    invoke-static {p0, v1}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Ljava/lang/Exception;)V

    .line 569729
    throw v1

    .line 569730
    :cond_1
    const/4 v2, 0x0

    .line 569731
    :try_start_1
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230014

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569732
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230014

    invoke-interface {v1, v3, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 569733
    :try_start_2
    instance-of v1, p2, LX/29g;

    if-eqz v1, :cond_8

    .line 569734
    move-object v0, p2

    check-cast v0, LX/29g;

    move-object v1, v0

    .line 569735
    iget-object v2, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->y:LX/0fW;

    .line 569736
    iget-object v0, v1, LX/29g;->a:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    move-object v1, v0

    .line 569737
    iget-object v0, v1, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    move-object v1, v0

    .line 569738
    invoke-virtual {v2, v1}, LX/0fW;->e(Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    .line 569739
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x230014

    if-nez v2, :cond_4

    const-string v3, "dbl_nonce"

    :goto_1
    invoke-interface {v1, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    move-object v1, v2

    .line 569740
    :goto_2
    if-nez v1, :cond_7

    .line 569741
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->j:LX/11H;

    sget-object v2, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p1, p2, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/component/AuthenticationResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v2, v1

    .line 569742
    :goto_3
    :try_start_3
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230014

    const/4 v4, 0x2

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569743
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    .line 569744
    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 569745
    sget-object v1, LX/26p;->f:LX/0Tn;

    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v4}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 569746
    :cond_2
    instance-of v1, p2, LX/29g;

    if-eqz v1, :cond_3

    .line 569747
    check-cast p2, LX/29g;

    .line 569748
    iget-object v0, p2, LX/29g;->a:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    move-object v1, v0

    .line 569749
    if-eqz v1, :cond_3

    .line 569750
    sget-object v1, LX/26p;->q:LX/0Tn;

    .line 569751
    iget-object v0, p2, LX/29g;->a:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    move-object v4, v0

    .line 569752
    iget-object v0, v4, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    move-object v4, v0

    .line 569753
    invoke-virtual {v1, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 569754
    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->d()LX/03R;

    move-result-object v4

    invoke-virtual {v4}, LX/03R;->getDbValue()I

    move-result v4

    invoke-interface {v3, v1, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 569755
    :cond_3
    invoke-interface {v3}, LX/0hN;->commit()V

    .line 569756
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->l:LX/0WJ;

    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0WJ;->a(Lcom/facebook/auth/credentials/FacebookCredentials;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 569757
    return-object v2

    .line 569758
    :cond_4
    :try_start_4
    const-string v3, "dbl_localauth"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 569759
    :catch_1
    move-exception v1

    move-object v2, v1

    .line 569760
    :try_start_5
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230013

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 569761
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 569762
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x230014

    if-nez v3, :cond_5

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_5
    invoke-interface {v1, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569763
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230014

    const/4 v4, 0x3

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569764
    :goto_4
    throw v2

    .line 569765
    :cond_6
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230014

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_4

    :cond_7
    move-object v2, v1

    goto/16 :goto_3

    :cond_8
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public static b(LX/0QB;)Lcom/facebook/katana/server/handler/Fb4aAuthHandler;
    .locals 27

    .prologue
    .line 569614
    new-instance v2, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;

    invoke-static/range {p0 .. p0}, LX/2Xi;->a(LX/0QB;)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/2Xj;->a(LX/0QB;)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/2Xk;->a(LX/0QB;)LX/2Xk;

    move-result-object v5

    check-cast v5, LX/2Xk;

    invoke-static/range {p0 .. p0}, LX/2Xl;->a(LX/0QB;)LX/2Xl;

    move-result-object v6

    check-cast v6, LX/2Xl;

    invoke-static/range {p0 .. p0}, LX/2Xn;->a(LX/0QB;)LX/2Xn;

    move-result-object v7

    check-cast v7, LX/2Xn;

    invoke-static/range {p0 .. p0}, LX/28S;->a(LX/0QB;)LX/28S;

    move-result-object v8

    check-cast v8, LX/28S;

    invoke-static/range {p0 .. p0}, LX/28T;->a(LX/0QB;)LX/28T;

    move-result-object v9

    check-cast v9, LX/28T;

    invoke-static/range {p0 .. p0}, LX/28U;->a(LX/0QB;)LX/28U;

    move-result-object v10

    check-cast v10, LX/28U;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v11

    check-cast v11, LX/11H;

    invoke-static/range {p0 .. p0}, LX/2Xo;->a(LX/0QB;)LX/2Xo;

    move-result-object v12

    check-cast v12, LX/2Xo;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v13

    check-cast v13, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2Xr;->a(LX/0QB;)LX/2Xr;

    move-result-object v16

    check-cast v16, LX/2Xr;

    invoke-static/range {p0 .. p0}, LX/2Xs;->a(LX/0QB;)LX/2Xs;

    move-result-object v17

    check-cast v17, LX/2Xs;

    const/16 v18, 0xb79

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/11Q;->a(LX/0QB;)LX/11Q;

    move-result-object v19

    check-cast v19, LX/11Q;

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v20

    check-cast v20, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0y2;->a(LX/0QB;)LX/0y2;

    move-result-object v21

    check-cast v21, LX/0y2;

    invoke-static/range {p0 .. p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v23

    check-cast v23, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, Lcom/facebook/auth/login/AuthOperations;->a(LX/0QB;)Lcom/facebook/auth/login/AuthOperations;

    move-result-object v24

    check-cast v24, Lcom/facebook/auth/login/AuthOperations;

    const/16 v25, 0x103d

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v26

    check-cast v26, LX/0fW;

    invoke-direct/range {v2 .. v26}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;-><init>(LX/0Ot;LX/0Ot;LX/2Xk;LX/2Xl;LX/2Xn;LX/28S;LX/28T;LX/28U;LX/11H;LX/2Xo;LX/0WJ;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Xr;LX/2Xs;LX/0Ot;LX/11Q;Lcom/facebook/performancelogger/PerformanceLogger;LX/0y2;LX/0Uh;Ljava/util/concurrent/ExecutorService;Lcom/facebook/auth/login/AuthOperations;LX/0Ot;LX/0fW;)V

    .line 569615
    return-object v2
.end method

.method public static b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;)Lcom/facebook/location/ImmutableLocation;
    .locals 3

    .prologue
    .line 569721
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->u:LX/0Uh;

    const/16 v1, 0x26

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->t:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 569717
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16B;

    .line 569718
    invoke-virtual {v0}, LX/16B;->d()V

    goto :goto_0

    .line 569719
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->l:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->f()V

    .line 569720
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 569616
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 569617
    const-string v1, "auth_reauth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 569618
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->w:Lcom/facebook/auth/login/AuthOperations;

    .line 569619
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569620
    const-string v2, "password"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 569621
    iget-object v2, v0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    iget-object v3, v0, Lcom/facebook/auth/login/AuthOperations;->d:LX/2E3;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    const-string p1, "AuthOperations"

    invoke-static {p0, p1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-virtual {v2, v3, v1, p0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/component/ReauthResult;

    move-object v0, v2

    .line 569622
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 569623
    :goto_0
    return-object v0

    .line 569624
    :cond_0
    const-string v1, "get_sso_user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 569625
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569626
    const-string v1, "get_sso_user_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569627
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->j:LX/11H;

    iget-object v2, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->d:LX/2Xk;

    sget-object v3, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/katana/server/protocol/GetSsoUserMethod$Result;

    .line 569628
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v0, v1

    .line 569629
    goto :goto_0

    .line 569630
    :cond_1
    const-string v1, "sso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 569631
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569632
    const-string v1, "sso_auth_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569633
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569634
    const-string v2, "sso_username"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 569635
    :cond_2
    const-string v1, "auth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 569636
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569637
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 569638
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569639
    const-string v2, "error_detail_type_param"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x0

    .line 569640
    iget-object v4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    invoke-interface {v4, v5, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 569641
    invoke-static {p0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    .line 569642
    iget-object v11, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->f:LX/2Xn;

    new-instance v4, LX/28d;

    if-nez v5, :cond_9

    move-object v7, v10

    :goto_1
    const/4 v8, 0x1

    move-object v5, v0

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, LX/28d;-><init>(Lcom/facebook/auth/credentials/PasswordCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;Ljava/lang/String;)V

    const-string v5, "password"

    invoke-static {p0, v11, v4, v5}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v4

    .line 569643
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    move-object v0, v4

    .line 569644
    goto/16 :goto_0

    .line 569645
    :cond_3
    const-string v1, "login_data_fetch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 569646
    iget-object v0, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->p:LX/2Xs;

    sget-object v1, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v4, 0x230004

    .line 569647
    iget-object v2, v0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569648
    iget-object v2, v0, LX/2Xs;->d:LX/2Bu;

    invoke-virtual {v2, v1}, LX/2Bu;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 569649
    iget-object v2, v0, LX/2Xs;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/katana/server/login/LoginDataFetchHelper$1;-><init>(LX/2Xs;Lcom/facebook/common/callercontext/CallerContext;)V

    const p0, -0x1b65f707

    invoke-static {v2, v3, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569650
    iget-object v2, v0, LX/2Xs;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x2

    invoke-interface {v2, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569651
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 569652
    move-object v0, v0

    .line 569653
    goto/16 :goto_0

    .line 569654
    :cond_4
    const-string v1, "logout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 569655
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569656
    const-string v1, "retain_session_for_dbl"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 569657
    iget-object v1, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->w:Lcom/facebook/auth/login/AuthOperations;

    .line 569658
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 569659
    const-string v3, "logout_reason_param"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/facebook/auth/login/AuthOperations;->a(Ljava/lang/String;Z)V

    .line 569660
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 569661
    goto/16 :goto_0

    .line 569662
    :cond_5
    const-string v1, "logged_out_set_nonce"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 569663
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569664
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 569665
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569666
    const-string v2, "error_detail_type_param"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x0

    .line 569667
    iget-object v4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    invoke-interface {v4, v5, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 569668
    invoke-static {p0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    .line 569669
    new-instance v11, LX/42Q;

    .line 569670
    iget-object v4, v0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    move-object v4, v4

    .line 569671
    invoke-direct {v11, v6, v10, v10, v4}, LX/42Q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569672
    new-instance v4, LX/28d;

    if-nez v5, :cond_a

    move-object v7, v10

    :goto_2
    const/4 v8, 0x1

    move-object v5, v0

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, LX/28d;-><init>(Lcom/facebook/auth/credentials/PasswordCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;Ljava/lang/String;)V

    .line 569673
    iget-object v5, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->q:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/18V;

    invoke-virtual {v5}, LX/18V;->a()LX/2VK;

    move-result-object v5

    .line 569674
    iget-object v6, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->f:LX/2Xn;

    invoke-static {v6, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v6, "authenticate"

    .line 569675
    iput-object v6, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 569676
    move-object v4, v4

    .line 569677
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v5, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 569678
    iget-object v4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->o:LX/2Xr;

    invoke-static {v4, v11}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v6, "set_nonce"

    .line 569679
    iput-object v6, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 569680
    move-object v4, v4

    .line 569681
    const-string v6, "authenticate"

    .line 569682
    iput-object v6, v4, LX/2Vk;->d:Ljava/lang/String;

    .line 569683
    move-object v4, v4

    .line 569684
    const-string v6, "?access_token={result=authenticate:$.access_token}"

    .line 569685
    iput-object v6, v4, LX/2Vk;->g:Ljava/lang/String;

    .line 569686
    move-object v4, v4

    .line 569687
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v5, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 569688
    const-string v4, "logged_out_set_nonce"

    sget-object v6, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v7, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->r:LX/11Q;

    invoke-virtual {v7}, LX/11Q;->a()LX/14U;

    move-result-object v7

    invoke-interface {v5, v4, v6, v7}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 569689
    const-string v4, "set_nonce"

    invoke-interface {v5, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 569690
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    move-object v0, v4

    .line 569691
    goto/16 :goto_0

    .line 569692
    :cond_6
    const-string v1, "device_based_login"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 569693
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569694
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 569695
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569696
    const-string v2, "error_detail_type_param"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    .line 569697
    iget-object v4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    invoke-interface {v4, v5, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 569698
    invoke-static {p0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    .line 569699
    iget-object v10, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->g:LX/28S;

    new-instance v4, LX/29g;

    if-nez v5, :cond_b

    :goto_3
    const/4 v8, 0x1

    move-object v5, v0

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, LX/29g;-><init>(Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;)V

    const-string v5, "dbl"

    invoke-static {p0, v10, v4, v5}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v4

    .line 569700
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    move-object v0, v4

    .line 569701
    goto/16 :goto_0

    .line 569702
    :cond_7
    const-string v1, "openid_login"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 569703
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 569704
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;

    .line 569705
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 569706
    const-string v2, "error_detail_type_param"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    .line 569707
    iget-object v4, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    invoke-interface {v4, v5, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 569708
    invoke-static {p0}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->b(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    .line 569709
    iget-object v10, p0, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->h:LX/28T;

    new-instance v4, LX/41x;

    if-nez v5, :cond_c

    :goto_4
    const/4 v8, 0x1

    move-object v5, v0

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, LX/41x;-><init>(Lcom/facebook/auth/credentials/OpenIDLoginCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;)V

    const-string v5, "openid"

    invoke-static {p0, v10, v4, v5}, Lcom/facebook/katana/server/handler/Fb4aAuthHandler;->a(Lcom/facebook/katana/server/handler/Fb4aAuthHandler;LX/0e6;Ljava/lang/Object;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v4

    .line 569710
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    move-object v0, v4

    .line 569711
    goto/16 :goto_0

    .line 569712
    :cond_8
    sget-object v0, LX/1nY;->ORCA_SERVICE_UNKNOWN_OPERATION:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 569713
    :cond_9
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v7

    goto/16 :goto_1

    .line 569714
    :cond_a
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v7

    goto/16 :goto_2

    .line 569715
    :cond_b
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v7

    goto :goto_3

    .line 569716
    :cond_c
    invoke-virtual {v5}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v7

    goto :goto_4
.end method
