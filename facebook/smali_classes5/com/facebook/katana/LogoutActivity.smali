.class public Lcom/facebook/katana/LogoutActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;
.implements LX/274;


# instance fields
.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/275;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/276;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:Z

.field public x:Z

.field private y:LX/278;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 566992
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 566993
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/LogoutActivity;->x:Z

    .line 566994
    new-instance v0, LX/277;

    invoke-direct {v0, p0}, LX/277;-><init>(Lcom/facebook/katana/LogoutActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/LogoutActivity;->y:LX/278;

    return-void
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 566989
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566990
    :goto_0
    return-void

    .line 566991
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/katana/LogoutActivity;LX/275;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0Ot;LX/10N;LX/276;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/LogoutActivity;",
            "LX/275;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/10N;",
            "LX/276;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .prologue
    .line 567038
    iput-object p1, p0, Lcom/facebook/katana/LogoutActivity;->q:LX/275;

    iput-object p2, p0, Lcom/facebook/katana/LogoutActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p3, p0, Lcom/facebook/katana/LogoutActivity;->s:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/katana/LogoutActivity;->p:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/katana/LogoutActivity;->t:LX/10N;

    iput-object p6, p0, Lcom/facebook/katana/LogoutActivity;->u:LX/276;

    iput-object p7, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/LogoutActivity;

    invoke-static {v7}, LX/275;->b(LX/0QB;)LX/275;

    move-result-object v1

    check-cast v1, LX/275;

    invoke-static {v7}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v2

    check-cast v2, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v3, 0x259

    invoke-static {v7, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xb99

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v7}, LX/10N;->b(LX/0QB;)LX/10N;

    move-result-object v5

    check-cast v5, LX/10N;

    invoke-static {v7}, LX/276;->a(LX/0QB;)LX/276;

    move-result-object v6

    check-cast v6, LX/276;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v7}, Lcom/facebook/katana/LogoutActivity;->a(Lcom/facebook/katana/LogoutActivity;LX/275;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0Ot;LX/10N;LX/276;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method public static d(Lcom/facebook/katana/LogoutActivity;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 567023
    iget-object v0, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->S:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 567024
    iget-object v1, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->T:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567025
    iget-object v2, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0hM;->U:LX/0Tn;

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 567026
    iget-object v3, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->V:LX/0Tn;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 567027
    iget-object v4, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2ss;->d:LX/0Tn;

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 567028
    iget-object v5, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0hM;->R:LX/0Tn;

    invoke-interface {v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v5

    .line 567029
    iget-object v6, p0, Lcom/facebook/katana/LogoutActivity;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/0hM;->Q:LX/0Tn;

    invoke-interface {v6, v7}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v6

    sget-object v7, LX/2ss;->d:LX/0Tn;

    invoke-interface {v6, v7}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 567030
    const-string v6, "uid"

    invoke-static {p1, v6, v0}, Lcom/facebook/katana/LogoutActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 567031
    const-string v0, "ndid"

    invoke-static {p1, v0, v1}, Lcom/facebook/katana/LogoutActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 567032
    const-string v0, "landing_experience"

    invoke-static {p1, v0, v2}, Lcom/facebook/katana/LogoutActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 567033
    const-string v0, "logged_in_user_id"

    invoke-static {p1, v0, v3}, Lcom/facebook/katana/LogoutActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 567034
    const-string v0, "user_id_to_log_in"

    invoke-static {p1, v0, v4}, Lcom/facebook/katana/LogoutActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 567035
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq v5, v0, :cond_0

    .line 567036
    const-string v0, "after_login_redirect_to_notifications_extra"

    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567037
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 567022
    const-string v0, "logout"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 567013
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 567014
    invoke-virtual {p0}, Lcom/facebook/katana/LogoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 567015
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 567016
    invoke-virtual {p0}, Lcom/facebook/katana/LogoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0421

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 567017
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 567018
    invoke-static {p0, p0}, Lcom/facebook/katana/LogoutActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 567019
    const v0, 0x7f030609

    invoke-virtual {p0, v0}, Lcom/facebook/katana/LogoutActivity;->setContentView(I)V

    .line 567020
    invoke-virtual {p0}, Lcom/facebook/katana/LogoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 567021
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7bd78966

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567010
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 567011
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/katana/LogoutActivity;->x:Z

    .line 567012
    const/16 v1, 0x23

    const v2, -0x243940ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x22

    const v1, 0x4b68b6d4    # 1.5251156E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 566995
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 566996
    iput-boolean v4, p0, Lcom/facebook/katana/LogoutActivity;->x:Z

    .line 566997
    iget-boolean v1, p0, Lcom/facebook/katana/LogoutActivity;->w:Z

    if-eqz v1, :cond_0

    .line 566998
    const/16 v1, 0x23

    const v2, -0x6f93f57a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 566999
    :goto_0
    return-void

    .line 567000
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 567001
    if-eqz v1, :cond_1

    .line 567002
    iget-object v2, p0, Lcom/facebook/katana/LogoutActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230012

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567003
    iget-object v2, p0, Lcom/facebook/katana/LogoutActivity;->y:LX/278;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/service/AppSession;->a(LX/278;)V

    .line 567004
    sget-object v2, LX/279;->USER_INITIATED:LX/279;

    invoke-virtual {v1, p0, v2}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;LX/279;)V

    .line 567005
    iput-boolean v4, p0, Lcom/facebook/katana/LogoutActivity;->w:Z

    .line 567006
    :goto_1
    const v1, -0x24d54ba4

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0

    .line 567007
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/LogoutActivity;->q:LX/275;

    .line 567008
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, LX/275;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 567009
    goto :goto_1
.end method
