.class public Lcom/facebook/katana/FacebookAccountReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576152
    const-class v0, Lcom/facebook/katana/FacebookAccountReceiver;

    sput-object v0, Lcom/facebook/katana/FacebookAccountReceiver;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 576153
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 576154
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 576134
    invoke-static {p0}, LX/2Bb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 576135
    if-nez v0, :cond_1

    .line 576136
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 576137
    :cond_1
    invoke-static {p0, v0}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 576138
    if-eqz v1, :cond_2

    .line 576139
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account still exists: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 576140
    :cond_2
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 576141
    if-eqz v1, :cond_0

    .line 576142
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 576143
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 576144
    iget-object v3, v1, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v3, v3

    .line 576145
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 576146
    :cond_3
    sget-object v2, LX/Gsn;->a:[I

    .line 576147
    iget-object v3, v1, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v3, v3

    .line 576148
    invoke-virtual {v3}, LX/2A1;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 576149
    :pswitch_1
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 576150
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Logging out: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576151
    :cond_4
    sget-object v0, LX/279;->ACCOUNT_REMOVED:LX/279;

    invoke-virtual {v1, p0, v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;LX/279;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x7aa4891e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 576129
    invoke-virtual {p2}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 576130
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 576131
    invoke-static {v1}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v1

    check-cast v1, LX/0Uq;

    .line 576132
    new-instance v3, LX/2XX;

    invoke-direct {v3, p0, p1, v0}, LX/2XX;-><init>(Lcom/facebook/katana/FacebookAccountReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v1, v3}, LX/0Uq;->a(LX/0V0;)V

    .line 576133
    const/16 v0, 0x27

    const v1, -0x71791e1d

    invoke-static {p2, v4, v0, v1, v2}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
