.class public Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/2Ad;
.implements LX/2A4;
.implements LX/2JL;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final af:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/2J4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/294;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ne;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/2Di;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0fW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/8D3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:Landroid/os/Bundle;

.field private N:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private O:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private P:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field public Q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public R:Z

.field public S:Z

.field private final T:Landroid/os/Handler;

.field private U:Lcom/facebook/katana/service/AppSession;

.field public V:Z

.field private W:J

.field private X:LX/IYX;

.field private Y:LX/0Yb;

.field public Z:Z

.field private aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

.field private ab:I

.field public ac:Ljava/lang/Boolean;

.field private ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field private ae:I

.field private ag:LX/IYY;

.field private final ah:Ljava/lang/Runnable;

.field public p:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2DR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2CQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation runtime Lcom/facebook/katana/annotations/DBLFragment;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/10M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/2CR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/2Cz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/10O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Landroid/content/ComponentName;
    .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 569351
    const-class v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    sput-object v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->af:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 569188
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 569189
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->T:Landroid/os/Handler;

    .line 569190
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Z:Z

    .line 569191
    iput v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    .line 569192
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ac:Ljava/lang/Boolean;

    .line 569193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 569194
    iput v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ae:I

    .line 569195
    new-instance v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity$1;-><init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ah:Ljava/lang/Runnable;

    .line 569196
    return-void
.end method

.method private a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 569197
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569198
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->c()V

    .line 569199
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->q()V

    .line 569200
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    if-eqz v0, :cond_1

    .line 569201
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    invoke-virtual {v0, v2}, Lcom/facebook/katana/service/AppSession;->b(LX/278;)V

    .line 569202
    :cond_1
    instance-of v0, p1, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    if-eqz v0, :cond_2

    const-string v2, ""

    move-object v0, p1

    check-cast v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 569203
    iget-object v3, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    move-object v0, v3

    .line 569204
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p1

    .line 569205
    check-cast v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 569206
    :goto_0
    new-instance v2, LX/IYX;

    invoke-direct {v2, p0, v0, p3, p4}, LX/IYX;-><init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    iput-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    .line 569207
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    invoke-static {p1, p2, v0, v2, v1}, LX/2CQ;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V

    .line 569208
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2DR;LX/2CQ;Lcom/facebook/content/SecureContextHelper;Ljava/lang/String;LX/10M;LX/0Or;LX/2CR;LX/2Cz;LX/10O;Landroid/content/ComponentName;LX/0Xl;Landroid/content/Context;LX/2hU;LX/10N;LX/2J4;LX/0Ot;LX/0Ot;LX/0Uh;LX/0Ot;LX/0Tf;LX/2Di;LX/0fW;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8D3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/2DR;",
            "LX/2CQ;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/lang/String;",
            "LX/10M;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2CR;",
            "LX/2Cz;",
            "LX/10O;",
            "Landroid/content/ComponentName;",
            "LX/0Xl;",
            "Landroid/content/Context;",
            "LX/2hU;",
            "LX/10N;",
            "LX/2J4;",
            "LX/0Ot",
            "<",
            "LX/294;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/2Ne;",
            ">;",
            "LX/0Tf;",
            "LX/2Di;",
            "LX/0fW;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/8D3;",
            ")V"
        }
    .end annotation

    .prologue
    .line 569209
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->q:LX/2DR;

    iput-object p3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r:LX/2CQ;

    iput-object p4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->t:Ljava/lang/String;

    iput-object p6, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    iput-object p7, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    iput-object p8, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    iput-object p9, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->x:LX/2Cz;

    iput-object p10, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y:LX/10O;

    iput-object p11, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->z:Landroid/content/ComponentName;

    iput-object p12, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->A:LX/0Xl;

    iput-object p13, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->B:Landroid/content/Context;

    iput-object p14, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->N:LX/2hU;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->O:LX/10N;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->C:LX/2J4;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->D:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->E:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->F:LX/0Uh;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->G:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->H:LX/0Tf;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->I:LX/2Di;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->J:LX/0fW;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->K:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->L:LX/8D3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 28

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v27

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static/range {v27 .. v27}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v27 .. v27}, LX/2DR;->a(LX/0QB;)LX/2DR;

    move-result-object v4

    check-cast v4, LX/2DR;

    invoke-static/range {v27 .. v27}, LX/2CQ;->a(LX/0QB;)LX/2CQ;

    move-result-object v5

    check-cast v5, LX/2CQ;

    invoke-static/range {v27 .. v27}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v27 .. v27}, LX/Gcn;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static/range {v27 .. v27}, LX/10M;->a(LX/0QB;)LX/10M;

    move-result-object v8

    check-cast v8, LX/10M;

    const/16 v9, 0x12cb

    move-object/from16 v0, v27

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {v27 .. v27}, LX/2CR;->a(LX/0QB;)LX/2CR;

    move-result-object v10

    check-cast v10, LX/2CR;

    invoke-static/range {v27 .. v27}, LX/2Cz;->a(LX/0QB;)LX/2Cz;

    move-result-object v11

    check-cast v11, LX/2Cz;

    invoke-static/range {v27 .. v27}, LX/10O;->a(LX/0QB;)LX/10O;

    move-result-object v12

    check-cast v12, LX/10O;

    invoke-static/range {v27 .. v27}, LX/27F;->a(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v13

    check-cast v13, Landroid/content/ComponentName;

    invoke-static/range {v27 .. v27}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v14

    check-cast v14, LX/0Xl;

    const-class v15, Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-interface {v0, v15}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/Context;

    invoke-static/range {v27 .. v27}, LX/2hU;->a(LX/0QB;)LX/2hU;

    move-result-object v16

    check-cast v16, LX/2hU;

    invoke-static/range {v27 .. v27}, LX/10N;->a(LX/0QB;)LX/10N;

    move-result-object v17

    check-cast v17, LX/10N;

    const-class v18, LX/2J4;

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/2J4;

    const/16 v19, 0xa3

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x2e3

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {v27 .. v27}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    const/16 v22, 0x69

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v27 .. v27}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v23

    check-cast v23, LX/0Tf;

    invoke-static/range {v27 .. v27}, LX/2Di;->a(LX/0QB;)LX/2Di;

    move-result-object v24

    check-cast v24, LX/2Di;

    invoke-static/range {v27 .. v27}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v25

    check-cast v25, LX/0fW;

    invoke-static/range {v27 .. v27}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v26

    check-cast v26, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v27 .. v27}, LX/8D3;->a(LX/0QB;)LX/8D3;

    move-result-object v27

    check-cast v27, LX/8D3;

    invoke-static/range {v2 .. v27}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/2DR;LX/2CQ;Lcom/facebook/content/SecureContextHelper;Ljava/lang/String;LX/10M;LX/0Or;LX/2CR;LX/2Cz;LX/10O;Landroid/content/ComponentName;LX/0Xl;Landroid/content/Context;LX/2hU;LX/10N;LX/2J4;LX/0Ot;LX/0Ot;LX/0Uh;LX/0Ot;LX/0Tf;LX/2Di;LX/0fW;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8D3;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/Throwable;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 569210
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 569211
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    .line 569212
    iput-object p0, v0, LX/2CR;->h:LX/2A4;

    .line 569213
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569214
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(Ljava/lang/Throwable;)V

    .line 569215
    :goto_0
    return-void

    .line 569216
    :cond_0
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_7

    .line 569217
    check-cast p1, LX/2Oo;

    .line 569218
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 569219
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2CR;->a(Ljava/lang/String;)LX/28P;

    move-result-object v1

    .line 569220
    sparse-switch v0, :sswitch_data_0

    .line 569221
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v2, v0, v1}, LX/2CR;->a(ILX/28P;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 569222
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(LX/2Oo;)V

    .line 569223
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    if-ne v0, v1, :cond_a

    iget v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    if-lt v0, v3, :cond_a

    .line 569224
    sget-object v0, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569225
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_0

    .line 569226
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->NO_PASSCODE_SET:LX/IYY;

    if-ne v0, v1, :cond_2

    .line 569227
    sget-object v0, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569228
    invoke-direct {p0, p2, v3}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_1

    .line 569229
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    if-ne v0, v1, :cond_4

    .line 569230
    iget v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    .line 569231
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    .line 569232
    if-eqz v0, :cond_1

    .line 569233
    const v1, 0x7f083490

    .line 569234
    iput v1, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->h:I

    .line 569235
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 569236
    if-eqz v2, :cond_3

    .line 569237
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 569238
    const p1, 0x7f0d02c4

    invoke-virtual {v2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 569239
    iget p1, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->h:I

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 569240
    :cond_3
    goto :goto_1

    .line 569241
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    if-ne v0, v1, :cond_6

    .line 569242
    :cond_5
    const v0, 0x7f0800d8

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getString(I)Ljava/lang/String;

    .line 569243
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    .line 569244
    if-eqz v0, :cond_1

    .line 569245
    const v1, 0x7f083473

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a(I)V

    goto :goto_1

    .line 569246
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

    if-ne v0, v1, :cond_1

    goto :goto_1

    .line 569247
    :sswitch_1
    sget-object v0, LX/IYY;->LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569248
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v0, v1}, LX/2CR;->a(LX/28P;)V

    .line 569249
    invoke-direct {p0, p2, p3}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto/16 :goto_1

    .line 569250
    :cond_7
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_9

    .line 569251
    check-cast p1, Ljava/io/IOException;

    .line 569252
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ac:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    if-eqz p1, :cond_8

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->x:LX/2Cz;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/2Cz;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 569253
    :cond_8
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(Ljava/io/IOException;)V

    goto/16 :goto_1

    .line 569254
    :cond_9
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 569255
    :cond_a
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    const v1, 0x7f08348f

    invoke-virtual {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gb4;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x196 -> :sswitch_1
    .end sparse-switch
.end method

.method private b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 8

    .prologue
    .line 569256
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 569257
    if-eqz p2, :cond_0

    .line 569258
    const-string v1, "dbl_flag"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 569259
    :cond_0
    if-eqz p1, :cond_2

    .line 569260
    const-string v1, "dbl_account_details"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569261
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v2, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    if-ne v1, v2, :cond_4

    .line 569262
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;-><init>()V

    .line 569263
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569264
    const v0, 0x7f083476

    invoke-virtual {v1, v0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a(I)V

    .line 569265
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    .line 569266
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 569267
    :cond_1
    :goto_0
    return-void

    .line 569268
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->l_(Z)V

    goto :goto_0

    .line 569269
    :cond_3
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 569270
    :cond_4
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v2, LX/IYY;->LOGIN_APPROVALS_CODE_ENTRY:LX/IYY;

    if-ne v1, v2, :cond_5

    .line 569271
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;-><init>()V

    .line 569272
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569273
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/LoginApprovalsFragment;->b:LX/2Ad;

    .line 569274
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 569275
    :cond_5
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v2, LX/IYY;->PASSWORD_ENTRY:LX/IYY;

    if-ne v1, v2, :cond_8

    .line 569276
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;-><init>()V

    .line 569277
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569278
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    .line 569279
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6

    .line 569280
    const v0, 0x7f083475

    invoke-virtual {v1, v0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a(I)V

    .line 569281
    :cond_6
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569282
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    .line 569283
    instance-of v2, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    if-nez v2, :cond_7

    .line 569284
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 569285
    :cond_7
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    .line 569286
    const/4 p2, 0x0

    const/4 p1, 0x1

    const/4 v7, 0x0

    .line 569287
    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v3, 0x7f0d02c4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 569288
    const-string v3, "alpha"

    new-array v4, p1, [F

    aput p2, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 569289
    iget-object v3, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->g:Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    const-string v4, "alpha"

    new-array v5, p1, [F

    aput p2, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 569290
    iget-object v4, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    const-string v5, "alpha"

    new-array v6, p1, [F

    aput p2, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 569291
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 569292
    const/4 v6, 0x3

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object v3, v6, v7

    aput-object v4, v6, p1

    const/4 v3, 0x2

    aput-object v2, v6, v3

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 569293
    move-object v0, v5

    .line 569294
    new-instance v2, LX/IYU;

    invoke-direct {v2, p0, v1}, LX/IYU;-><init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 569295
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    .line 569296
    :cond_8
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v2, LX/IYY;->NO_NONCE_EXISTS:LX/IYY;

    if-ne v1, v2, :cond_1

    .line 569297
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;-><init>()V

    .line 569298
    const v2, 0x7f083475

    invoke-virtual {v1, v2}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->a(I)V

    .line 569299
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569300
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->e:LX/2Ad;

    .line 569301
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_0
.end method

.method public static b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 569302
    instance-of v0, p1, LX/Gb4;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 569303
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 569304
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 569305
    return-void
.end method

.method public static b$redex0(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 569306
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 569307
    if-nez v0, :cond_1

    .line 569308
    sget-object v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->af:Ljava/lang/Class;

    const-string v1, "Current user not logged in."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 569309
    :cond_0
    :goto_0
    return-void

    .line 569310
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    .line 569311
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 569312
    invoke-virtual {v1, v0}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v2

    .line 569313
    if-eqz v2, :cond_0

    const-string v0, "password_account"

    iget-object v1, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569314
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->P:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static c(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 569315
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 569316
    iget-object v1, p0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 569317
    const-string v1, "login_id"

    iget-object v2, p0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569318
    :cond_0
    return-object v0
.end method

.method public static c(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 569319
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v1, "uid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569320
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 6

    .prologue
    .line 569354
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->N:LX/2hU;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->B:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->B:Landroid/content/Context;

    const v3, 0x7f0838a8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xfa0

    .line 569355
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03029b

    const/4 p0, 0x0

    invoke-virtual {v4, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 569356
    const v4, 0x7f0d0951

    invoke-static {v5, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 569357
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569358
    invoke-virtual {v0, v5, v3}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v4

    move-object v0, v4

    .line 569359
    invoke-virtual {v0}, LX/4nS;->a()V

    .line 569360
    return-void
.end method

.method private o()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 569321
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->O:LX/10N;

    .line 569322
    iget-object v2, v1, LX/10N;->c:LX/0Uh;

    const/16 v3, 0x15

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 569323
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->K:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->p:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static p(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V
    .locals 1

    .prologue
    .line 569352
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    .line 569353
    return-void
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 569342
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Y:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 569343
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->W:J

    .line 569344
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Z:Z

    .line 569345
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->S:Z

    .line 569346
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->R:Z

    .line 569347
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    if-nez v0, :cond_0

    .line 569348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    .line 569349
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->T:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ah:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    const v4, 0x30b3a806

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 569350
    :cond_0
    return-void
.end method

.method private r()J
    .locals 4

    .prologue
    .line 569341
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->W:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V
    .locals 3

    .prologue
    .line 569338
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->q:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->b()Landroid/content/Intent;

    move-result-object v0

    .line 569339
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r:LX/2CQ;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v0}, LX/2CQ;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 569340
    return-void
.end method

.method public static t(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V
    .locals 5

    .prologue
    .line 569326
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 569327
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 569328
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 569329
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 569330
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 569331
    invoke-virtual {v3, v0}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    .line 569332
    if-eqz v0, :cond_0

    .line 569333
    const-string v3, "dbl_account_details"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569334
    :cond_0
    const-string v0, "operation_type"

    const-string v3, "change_passcode_from_login_flow"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569335
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 569336
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 569337
    :cond_1
    return-void
.end method

.method public static u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;
    .locals 2

    .prologue
    .line 569325
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/Gb4;

    return-object v0
.end method

.method private v()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569324
    new-instance v0, LX/IYV;

    invoke-direct {v0, p0}, LX/IYV;-><init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    return-object v0
.end method

.method public static w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z
    .locals 1

    .prologue
    .line 569186
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z
    .locals 3

    .prologue
    .line 569187
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->F:LX/0Uh;

    const/16 v1, 0x3e0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568974
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->O:LX/10N;

    invoke-virtual {v0}, LX/10N;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568975
    const-class v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListVerticalFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 568976
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->t:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 568977
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->l_(Z)V

    .line 568978
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 568979
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 568980
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 568981
    iget-object v0, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->c(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568982
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v1, "landing_experience"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v0

    .line 568983
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->ACCOUNT_SWITCHER:Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    if-ne v0, v1, :cond_0

    .line 568984
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->L:LX/8D3;

    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v3, "ndid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v4, "landing_experience"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v5, "logged_in_user_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "account_switcher"

    invoke-virtual/range {v0 .. v5}, LX/8D3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568985
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568986
    const-string v1, "fbid"

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568987
    const-string v1, "password_account"

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568988
    sget-object v1, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 568989
    const-string v1, "account_type"

    const-string v2, "password_account"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568990
    invoke-direct {p0, p1, v6}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 568991
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y:LX/10O;

    const-string v2, "dbl_user_chooser_selected_user"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 568992
    return-void

    .line 568993
    :cond_1
    const-string v1, ""

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568994
    sget-object v1, LX/IYY;->NO_NONCE_EXISTS:LX/IYY;

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 568995
    const-string v1, "placeholder"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568996
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 568997
    const-string v1, "pin"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568998
    invoke-direct {p0, p1, v7}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_0

    .line 568999
    :cond_2
    const-string v1, "pin"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569000
    const/4 v1, 0x2

    invoke-direct {p0, p1, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_0

    .line 569001
    :cond_3
    const-string v1, "placeholder"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569002
    iget-object v1, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 569003
    const-string v1, "pin"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569004
    sget-object v1, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569005
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;-><init>()V

    .line 569006
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 569007
    const-string v3, "dbl_account_details"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569008
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569009
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->c:LX/2Ad;

    .line 569010
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 569011
    :cond_4
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 569012
    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    invoke-direct {v1}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;-><init>()V

    .line 569013
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 569014
    const-string v3, "dbl_account_details"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569015
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569016
    iput-object p0, v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->f:LX/2Ad;

    .line 569017
    invoke-static {p0, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_0

    .line 569018
    :cond_5
    const-string v1, "pin"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569019
    sget-object v1, LX/IYY;->NO_PASSCODE_SET:LX/IYY;

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569020
    const-string v1, ""

    .line 569021
    new-instance v2, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    iget-object v3, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    sget-object v5, LX/41B;->DEVICE_BASED_LOGIN_TYPE:LX/41B;

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41B;)V

    invoke-virtual {p0, v2, p1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 3

    .prologue
    .line 569022
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    invoke-virtual {v0}, LX/2Ne;->a()V

    .line 569023
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    invoke-virtual {v0}, LX/2Ne;->e()V

    .line 569024
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 569025
    iput p2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ae:I

    .line 569026
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/account/recovery/AccountRecoveryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 569027
    invoke-static {p1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->c(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 569028
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 569029
    return-void
.end method

.method public final a(Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 3

    .prologue
    const v2, 0x230013

    .line 569030
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569031
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v0, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "dbl_pin"

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569032
    const-string v0, "device_based_login"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 569033
    return-void

    .line 569034
    :cond_0
    const-string v0, "dbl_nonce"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/credentials/PasswordCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V
    .locals 7

    .prologue
    const v6, 0x230013

    .line 569035
    iget-object v0, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->c(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569036
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->L:LX/8D3;

    iget-object v1, p2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v3, "ndid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v4, "landing_experience"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v5, "logged_in_user_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "password"

    invoke-virtual/range {v0 .. v5}, LX/8D3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569037
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569038
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "dbl_password"

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569039
    const-string v0, "auth"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    .line 569040
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 569182
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0, p1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    .line 569183
    if-eqz v0, :cond_0

    .line 569184
    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 569185
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 569041
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->e()Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 569042
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->e()Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    .line 569043
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 569044
    const-string v2, "fbid"

    iget-object v3, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569045
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v3, LX/IYY;->PASSWORD_ACCOUNT:LX/IYY;

    if-ne v2, v3, :cond_0

    .line 569046
    const-string v2, "account_type"

    const-string v3, "password_account"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569047
    :goto_0
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y:LX/10O;

    const-string v3, "dbl_settings_displayed"

    invoke-virtual {v2, v3, v1}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 569048
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/devicebasedlogin/settings/DBLAccountSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 569049
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 569050
    const-string v3, "dbl_account_details"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569051
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 569052
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 569053
    :goto_1
    return-void

    .line 569054
    :cond_0
    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 569055
    const-string v2, "view"

    const-string v3, "pin"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 569056
    :cond_1
    const-string v2, "view"

    const-string v3, "no_pin"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 569057
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y:LX/10O;

    const-string v1, "dbl_settings_list_displayed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 569058
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/devicebasedlogin/settings/DBLLoginSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 569059
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 569060
    invoke-static {p0, p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 569061
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    .line 569062
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 569063
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    .line 569064
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 569065
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->l()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569066
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->t(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    .line 569067
    :goto_0
    return-void

    .line 569068
    :cond_1
    invoke-virtual {p0, v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->l_(Z)V

    goto :goto_0

    .line 569069
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v1, "logout_snackbar_enabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 569070
    if-eqz v0, :cond_3

    .line 569071
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->n()V

    .line 569072
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->C:LX/2J4;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y:LX/10O;

    invoke-virtual {v0, v1, v2}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->P:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 569073
    iput-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->C:LX/2J4;

    .line 569074
    sget-object v0, LX/IYY;->DEFAULT:LX/IYY;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    .line 569075
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 569076
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 569077
    const-string v2, "user_id_to_log_in"

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v4, "user_id_to_log_in"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569078
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 569079
    const v0, 0x7f0303e0

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->setContentView(I)V

    move-object v0, v1

    .line 569080
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569081
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569082
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    .line 569083
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 569084
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 569085
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    .line 569086
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->A:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    new-instance v2, LX/IYT;

    invoke-direct {v2, p0}, LX/IYT;-><init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Y:LX/0Yb;

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 569087
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->c()V

    .line 569088
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->P:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v1, "dbl_login"

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Z)V

    .line 569089
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0834b5

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 569090
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    invoke-virtual {v0}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569091
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->d()V

    .line 569092
    :goto_0
    return-void

    .line 569093
    :cond_0
    invoke-virtual {p0, v3}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->l_(Z)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 569094
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ac:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 569095
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->P:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->v()LX/0TF;

    move-result-object v3

    const-string v4, "fb4a_login"

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    .line 569096
    return-void
.end method

.method public final l_(Z)V
    .locals 4

    .prologue
    .line 569097
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->z:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 569098
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v2, "redirected_from_dbl"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569099
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    const-string v2, "redirected_from_adduser"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 569100
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->M:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 569101
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x8a2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 569102
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 569103
    const/16 v0, 0x8a2

    if-ne p1, v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 569104
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    .line 569105
    :cond_0
    :goto_0
    return-void

    .line 569106
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_2

    .line 569107
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u:LX/10M;

    .line 569108
    iget-object v1, v0, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 569109
    sget-object v2, LX/26p;->r:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 569110
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 569111
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    goto :goto_0

    .line 569112
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 569113
    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-eqz v0, :cond_0

    .line 569114
    const-string v0, "account_password"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569115
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 569116
    new-instance v1, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v2, v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ae:I

    invoke-virtual {p0, v1, v0, v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->a(Lcom/facebook/auth/credentials/PasswordCredentials;Lcom/facebook/auth/credentials/DBLFacebookCredentials;I)V

    goto :goto_0

    .line 569117
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->finish()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 569118
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->DEFAULT:LX/IYY;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v1, LX/IYY;->NO_PASSCODE_SET:LX/IYY;

    invoke-virtual {v0, v1}, LX/IYY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569119
    :cond_0
    invoke-virtual {p0, v5}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->setResult(I)V

    .line 569120
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->finish()V

    .line 569121
    :goto_0
    iput v5, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    .line 569122
    return-void

    .line 569123
    :cond_1
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v1

    .line 569124
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->y()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 569125
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 569126
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    sget-object v4, LX/IYY;->PASSCODE_ENTRY:LX/IYY;

    if-ne v0, v4, :cond_2

    .line 569127
    if-eqz v1, :cond_2

    instance-of v0, v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    if-eqz v0, :cond_2

    .line 569128
    const-string v0, "previous_login_state"

    const-string v4, "login_state_passcode_entry"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 569129
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    .line 569130
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 569131
    const-wide/16 v8, 0x258

    invoke-virtual {v6, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 569132
    iget-object v7, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->f:Landroid/view/View;

    const v8, 0x7f0d02c4

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 569133
    iget-object v6, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->d:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-virtual {v6}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->c()V

    .line 569134
    check-cast v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;

    .line 569135
    iget-object v0, v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFragment;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-object v0, v0

    .line 569136
    const-string v1, "dbl_account_details"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569137
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    :cond_2
    move-object v0, v2

    .line 569138
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569139
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569140
    iput-object p0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    .line 569141
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->b(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;Landroid/support/v4/app/Fragment;)V

    .line 569142
    sget-object v0, LX/IYY;->DEFAULT:LX/IYY;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ag:LX/IYY;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x22

    const v1, -0x232ca982

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 569143
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    if-eqz v1, :cond_0

    .line 569144
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/service/AppSession;->b(LX/278;)V

    .line 569145
    iput-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->X:LX/IYX;

    .line 569146
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Y:LX/0Yb;

    if-eqz v1, :cond_1

    .line 569147
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Y:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 569148
    iput-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Y:LX/0Yb;

    .line 569149
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    if-eqz v1, :cond_2

    .line 569150
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569151
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    .line 569152
    iput-object v3, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->aa:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 569153
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 569154
    const/16 v1, 0x23

    const v2, 0x64a1fb56

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4c6d25bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 569155
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 569156
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ac:Ljava/lang/Boolean;

    .line 569157
    const/16 v1, 0x23

    const v2, -0x2d5d71b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 569158
    const-string v0, "save_ar_creds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 569159
    const-string v0, "save_ar_flag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ae:I

    .line 569160
    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const-wide/32 v6, 0xea60

    const/4 v4, 0x1

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x2cbfd4d0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 569161
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 569162
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ab:I

    .line 569163
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ac:Ljava/lang/Boolean;

    .line 569164
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    if-eqz v1, :cond_0

    .line 569165
    sget-object v1, LX/IYW;->a:[I

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->U:Lcom/facebook/katana/service/AppSession;

    .line 569166
    iget-object v3, v2, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v2, v3

    .line 569167
    invoke-virtual {v2}, LX/2A1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 569168
    :cond_0
    :goto_0
    const v1, 0x63e5cac7

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 569169
    :pswitch_0
    iput-boolean v4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->R:Z

    .line 569170
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r()J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->Z:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    if-nez v1, :cond_1

    .line 569171
    iput-boolean v4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    .line 569172
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->T:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ah:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r()J

    move-result-wide v4

    sub-long v4, v6, v4

    const v3, 0x122c78b8

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 569173
    :cond_1
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    goto :goto_0

    .line 569174
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r()J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    if-nez v1, :cond_0

    .line 569175
    iput-boolean v4, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->V:Z

    .line 569176
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->T:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ah:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->r()J

    move-result-wide v4

    sub-long v4, v6, v4

    const v3, 0x691a2f3f

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 569177
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 569178
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    if-eqz v0, :cond_0

    .line 569179
    const-string v0, "save_ar_creds"

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ad:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 569180
    :cond_0
    const-string v0, "save_ar_flag"

    iget v1, p0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->ae:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 569181
    return-void
.end method
