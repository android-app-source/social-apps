.class public Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/0f2;
.implements LX/10X;
.implements LX/0l6;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2A4;
.implements LX/2JL;


# static fields
.field public static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final q:LX/0Tn;


# instance fields
.field private A:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27d;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private E:LX/2Cz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:LX/10N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/2CQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private H:LX/10M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private I:LX/2CR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/10O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:LX/2CS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private N:LX/0Or;
    .annotation runtime Lcom/facebook/katana/annotations/IsFastPwErrorArEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private O:LX/0Or;
    .annotation runtime Lcom/facebook/katana/annotations/IsFullWidthRegButtonEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private P:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/katana/annotations/IsLoginWithPhoneNumberSupported;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Q:LX/2Lx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private R:LX/0dz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private S:LX/2JA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27j;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private U:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private V:LX/0dC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private W:LX/295;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private X:LX/0Ot;
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsBounceFromMSiteEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Y:LX/0Ot;
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsGkUnsetBounceFromMSiteEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private Z:LX/0Or;
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsParallelSearchEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aA:LX/2NP;

.field public aB:Lcom/facebook/katana/service/AppSession;

.field public aC:LX/2NU;

.field private aD:Ljava/lang/String;

.field private aE:Z

.field private aF:Landroid/view/View;

.field private aG:J

.field private aH:I

.field public aI:Z

.field public aJ:Z

.field public aK:Z

.field private aL:Z

.field public aM:Ljava/lang/String;

.field public aN:Ljava/lang/String;

.field public final aO:Landroid/os/Handler;

.field private aP:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aQ:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aR:Z

.field private aS:Z

.field public aT:Landroid/os/Handler;

.field private aU:Ljava/lang/Runnable;

.field private aV:Ljava/lang/String;

.field private aW:J

.field private aX:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private aY:Z

.field public aZ:Z

.field public aa:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ne;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ab:LX/27w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ac:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2EH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ad:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ag;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ae:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private af:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ah:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ai:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27g;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aj:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ak:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27t;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private al:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private am:LX/2J4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private an:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field public ao:LX/2NM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ap:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aq:LX/2NN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ar:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/294;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private as:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private at:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public au:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public av:LX/276;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aw:LX/0Or;
    .annotation runtime Lcom/facebook/katana/annotations/IsReuseLoginFieldsForRegEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ax:LX/0Or;
    .annotation runtime Lcom/facebook/katana/annotations/IsWhiteLoginPageEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ay:LX/0Or;
    .annotation runtime Lcom/facebook/katana/annotations/IsHideRegisterButtonEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private az:LX/2NO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public bA:Landroid/widget/ImageView;

.field private bB:Landroid/widget/ImageView;

.field private bC:Landroid/widget/ImageView;

.field private bD:Landroid/view/View;

.field public bE:Landroid/view/View;

.field public bF:Landroid/view/View;

.field private bG:Landroid/view/View;

.field private bH:Landroid/widget/TextView;

.field private bI:Landroid/widget/TextView;

.field private bJ:Landroid/widget/TextView;

.field private bK:Lcom/facebook/resources/ui/FbTextView;

.field private bL:Lcom/facebook/resources/ui/FbTextView;

.field private bM:Lcom/facebook/resources/ui/FbButton;

.field private bN:Lcom/facebook/resources/ui/FbButton;

.field public bO:Landroid/widget/TextView;

.field private bP:Landroid/widget/TextView;

.field private bQ:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bR:Landroid/widget/FrameLayout;

.field private final bS:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final bT:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final bU:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public final bV:Landroid/view/View$OnClickListener;

.field public final bW:Ljava/lang/Runnable;

.field private final bX:Ljava/lang/Runnable;

.field private final bY:Ljava/lang/Runnable;

.field private ba:Z

.field private bb:Ljava/lang/String;

.field private bc:Ljava/lang/String;

.field private bd:Z

.field private be:Z

.field public bf:Z

.field private bg:Z

.field private bh:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public bi:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public bj:Landroid/widget/AutoCompleteTextView;

.field public bk:Landroid/widget/EditText;

.field public bl:Landroid/widget/EditText;

.field public bm:Landroid/widget/LinearLayout;

.field private bn:Landroid/widget/RelativeLayout;

.field private bo:Landroid/widget/LinearLayout;

.field private bp:Landroid/widget/LinearLayout;

.field private bq:Landroid/widget/LinearLayout;

.field public br:Landroid/widget/LinearLayout;

.field private bs:Landroid/widget/LinearLayout;

.field public bt:Lcom/facebook/resources/ui/FbButton;

.field public bu:Lcom/facebook/resources/ui/FbButton;

.field public bv:Lcom/facebook/resources/ui/FbButton;

.field public bw:Lcom/facebook/resources/ui/FbButton;

.field public bx:Landroid/view/View;

.field private by:Landroid/widget/ProgressBar;

.field private bz:Landroid/widget/RelativeLayout;

.field public r:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0pW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2DR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/2Cy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 567970
    const-class v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sput-object v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->p:Ljava/lang/Class;

    .line 567971
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "ar_bounce_from_msite"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->q:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 567949
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 567950
    sget-object v0, LX/2NP;->SPLASHSCREEN:LX/2NP;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aA:LX/2NP;

    .line 567951
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    .line 567952
    iput v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aH:I

    .line 567953
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aI:Z

    .line 567954
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aL:Z

    .line 567955
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    .line 567956
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    .line 567957
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aY:Z

    .line 567958
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aZ:Z

    .line 567959
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ba:Z

    .line 567960
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 567961
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bg:Z

    .line 567962
    new-instance v0, LX/2NQ;

    invoke-direct {v0, p0}, LX/2NQ;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bS:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 567963
    new-instance v0, LX/2NR;

    invoke-direct {v0, p0}, LX/2NR;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bT:LX/0TF;

    .line 567964
    new-instance v0, LX/2Cq;

    invoke-direct {v0, p0}, LX/2Cq;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bU:LX/0TF;

    .line 567965
    new-instance v0, LX/2NS;

    invoke-direct {v0, p0}, LX/2NS;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bV:Landroid/view/View$OnClickListener;

    .line 567966
    new-instance v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$5;

    invoke-direct {v0, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$5;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    .line 567967
    new-instance v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$6;

    invoke-direct {v0, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$6;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bX:Ljava/lang/Runnable;

    .line 567968
    new-instance v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;

    invoke-direct {v0, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bY:Ljava/lang/Runnable;

    .line 567969
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    const/16 v2, 0x17

    .line 567938
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_0

    .line 567939
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_2

    .line 567940
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0e0bd7

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(I)V

    .line 567941
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f020e77

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 567942
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_1

    .line 567943
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_3

    .line 567944
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0e0bd8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(I)V

    .line 567945
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f020e79

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 567946
    :cond_1
    return-void

    .line 567947
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0e0bd7

    invoke-virtual {v0, p0, v1}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 567948
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0e0bd8

    invoke-virtual {v0, p0, v1}, Lcom/facebook/resources/ui/FbButton;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private B()LX/28M;
    .locals 4

    .prologue
    .line 567931
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reg_login_nonce"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567932
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "username"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 567933
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 567934
    new-instance v0, LX/28M;

    sget-object v3, LX/28K;->APP_REGISTRATION_LOGIN_NONCE:LX/28K;

    invoke-direct {v0, p0, v3}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 567935
    iput-object v2, v0, LX/28M;->b:Ljava/lang/String;

    .line 567936
    iput-object v1, v0, LX/28M;->d:Ljava/lang/String;

    .line 567937
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()LX/28M;
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 567908
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "calling_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 567909
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 567910
    :goto_0
    return-object v0

    .line 567911
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1CA;->n:LX/0Tn;

    invoke-interface {v1, v3, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 567912
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0x1b7740

    cmp-long v1, v4, v6

    if-ltz v1, :cond_2

    move-object v0, v2

    .line 567913
    goto :goto_0

    .line 567914
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/1CA;->n:LX/0Tn;

    invoke-interface {v1, v3, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 567915
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "cptoken"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567916
    if-nez v1, :cond_3

    move-object v0, v2

    .line 567917
    goto :goto_0

    .line 567918
    :cond_3
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "cpuid"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 567919
    new-instance v3, LX/28M;

    sget-object v6, LX/28K;->TRANSIENT_TOKEN:LX/28K;

    invoke-direct {v3, p0, v6}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 567920
    iput-wide v4, v3, LX/28M;->c:J

    .line 567921
    iput-object v1, v3, LX/28M;->d:Ljava/lang/String;

    .line 567922
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ne;

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->V:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    .line 567923
    iget-object v5, v1, LX/2Ne;->a:LX/0Zb;

    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v7, LX/GB4;->LOGIN_FROM_MSITE:LX/GB4;

    invoke-virtual {v7}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "account_recovery"

    .line 567924
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567925
    move-object v6, v6

    .line 567926
    const-string v7, "device_id"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567927
    move-object v0, v3

    .line 567928
    goto :goto_0

    .line 567929
    :catch_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    const-string v3, "LoginCheckpointCorruptLink"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Checkpoint login redirect expected uid but got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v5, "cpuid"

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 567930
    goto/16 :goto_0
.end method

.method private D()LX/28M;
    .locals 4

    .prologue
    .line 567901
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_uid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567902
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_pwd"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 567903
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 567904
    new-instance v0, LX/28M;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v0, p0, v3}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 567905
    iput-object v1, v0, LX/28M;->b:Ljava/lang/String;

    .line 567906
    iput-object v2, v0, LX/28M;->d:Ljava/lang/String;

    .line 567907
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()LX/28M;
    .locals 4

    .prologue
    .line 567894
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "username"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567895
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "otp"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 567896
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 567897
    new-instance v0, LX/28M;

    sget-object v3, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v0, p0, v3}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 567898
    iput-object v1, v0, LX/28M;->b:Ljava/lang/String;

    .line 567899
    iput-object v2, v0, LX/28M;->d:Ljava/lang/String;

    .line 567900
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static F(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 2

    .prologue
    .line 567888
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 567889
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eq v1, v0, :cond_1

    .line 567890
    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 567891
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 567892
    :cond_1
    return-void

    .line 567893
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static G$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 567860
    invoke-direct {p0, v6}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Z)V

    .line 567861
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 567862
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v0, :cond_0

    .line 567863
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 567864
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567865
    :cond_0
    invoke-direct {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(I)V

    .line 567866
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 567867
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v0, :cond_4

    .line 567868
    invoke-static {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 567869
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 567870
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567871
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->an()Z

    move-result v0

    if-nez v0, :cond_2

    .line 567872
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567873
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 567874
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 567875
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v1, v3, v4, v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    move-object v0, v1

    .line 567876
    :goto_0
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 567877
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567878
    :cond_3
    return-void

    .line 567879
    :cond_4
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 567880
    invoke-static {p0, v6}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 567881
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567882
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567883
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 567884
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v4, 0x0

    invoke-direct {v3, v5, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 567885
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->O:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    .line 567886
    :goto_1
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v3, v2, v4, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    move-object v0, v3

    goto :goto_0

    .line 567887
    :cond_5
    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->f(I)I

    move-result v1

    goto :goto_1
.end method

.method private H()V
    .locals 2

    .prologue
    .line 567854
    const v0, 0x7f0d108e

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 567855
    if-eqz v0, :cond_0

    .line 567856
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 567857
    if-eqz v0, :cond_0

    .line 567858
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bS:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 567859
    :cond_0
    return-void
.end method

.method private I()V
    .locals 3

    .prologue
    const v2, 0x230013

    .line 567844
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567845
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "password"

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 567846
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230003

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567847
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567848
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->M:LX/2CS;

    .line 567849
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->e:LX/0ih;

    sget-object p0, LX/2Wl;->LOGIN_START:LX/2Wl;

    invoke-virtual {p0}, LX/2Wl;->getEventName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 567850
    sget-object v1, LX/0ig;->f:LX/0ih;

    const/16 v2, 0x258

    .line 567851
    iput v2, v1, LX/0ih;->c:I

    .line 567852
    iget-object v1, v0, LX/2CS;->b:LX/0if;

    sget-object v2, LX/0ig;->f:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 567853
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    .line 567841
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230001

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 567842
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->K()V

    .line 567843
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 567837
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->M:LX/2CS;

    invoke-virtual {v0}, LX/2CS;->d()V

    .line 567838
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230003

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 567839
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 567840
    return-void
.end method

.method public static L(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 567828
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aG:J

    .line 567829
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aI:Z

    .line 567830
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    .line 567831
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aK:Z

    .line 567832
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    if-nez v0, :cond_0

    .line 567833
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    .line 567834
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    const v4, -0x7628cd1f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 567835
    :cond_0
    sget-object v0, LX/2NP;->LOGGING_IN:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 567836
    return-void
.end method

.method private M()V
    .locals 1

    .prologue
    .line 567722
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->K:LX/0hx;

    invoke-virtual {v0}, LX/0hx;->a()V

    .line 567723
    return-void
.end method

.method private N()V
    .locals 5

    .prologue
    .line 567817
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 567818
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj()Ljava/lang/String;

    move-result-object v2

    .line 567819
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;

    invoke-direct {v4, p0, v2, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;J)V

    const v0, -0x486a79bc

    invoke-static {v3, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 567820
    return-void
.end method

.method private O()V
    .locals 5

    .prologue
    .line 567806
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 567807
    if-nez v0, :cond_1

    .line 567808
    :cond_0
    :goto_0
    return-void

    .line 567809
    :cond_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ab:LX/27w;

    invoke-virtual {v1}, LX/27w;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 567810
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ab:LX/27w;

    invoke-virtual {v1}, LX/27w;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567811
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->v:LX/0Sh;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    .line 567812
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v4

    .line 567813
    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v0, v4}, LX/2NZ;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Ljava/lang/String;Z)V

    goto :goto_0

    .line 567814
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->v:LX/0Sh;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    .line 567815
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v4

    .line 567816
    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v0, v4}, LX/2NZ;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private P()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const v8, 0x23000e

    const/4 v7, 0x2

    const/4 v3, 0x0

    const v6, 0x230015

    .line 567786
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567787
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "redirected_from_adduser"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567788
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->an:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/Boolean;)V

    .line 567789
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->an:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v1, ""

    const-string v4, "add_user"

    const/4 v5, 0x1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;LX/0TF;Ljava/lang/String;Z)V

    .line 567790
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->b()Landroid/content/Intent;

    move-result-object v0

    .line 567791
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    .line 567792
    iget-object v2, v1, LX/2DR;->g:Ljava/lang/String;

    move-object v1, v2

    .line 567793
    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    .line 567794
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 567795
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0x33

    invoke-interface {v1, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 567796
    :goto_0
    new-instance v1, LX/27o;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/27o;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/content/Intent;)V

    .line 567797
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x230003

    invoke-interface {v2, v3, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 567798
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->M:LX/2CS;

    invoke-virtual {v2}, LX/2CS;->d()V

    .line 567799
    if-eqz v0, :cond_1

    .line 567800
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 567801
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    invoke-interface {v2, v8, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 567802
    :cond_1
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G:LX/2CQ;

    invoke-virtual {v1}, LX/27o;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/2CQ;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 567803
    return-void

    .line 567804
    :cond_2
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    invoke-interface {v1, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 567805
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method

.method private Q()V
    .locals 3

    .prologue
    .line 567784
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    const v1, 0x7f0800fb

    invoke-virtual {p0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0800fd

    invoke-virtual {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567785
    return-void
.end method

.method private R()Z
    .locals 2

    .prologue
    .line 567783
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private S()V
    .locals 6

    .prologue
    .line 567757
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    invoke-virtual {v0}, LX/2Ne;->a()V

    .line 567758
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567759
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/facebook/account/recovery/AccountRecoveryActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567760
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->U()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 567761
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    invoke-virtual {v0}, LX/2Ne;->e()V

    .line 567762
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 567763
    :goto_0
    return-void

    .line 567764
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Q:LX/2Lx;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "login/identify?ctx=recover&allow_token_login=1"

    invoke-virtual {v0, v1, v2}, LX/2Lx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 567765
    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 567766
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v0, v2, :cond_1

    .line 567767
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ac:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2EH;

    .line 567768
    iget-object v2, v0, LX/2EH;->f:LX/2Ne;

    .line 567769
    iget-object v3, v2, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->BACKGROUND_FETCH_DEVICE_DATA:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 567770
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567771
    move-object v4, v4

    .line 567772
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567773
    iget-object v2, v0, LX/2EH;->b:LX/F9o;

    invoke-virtual {v2}, LX/F9o;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 567774
    new-instance v3, LX/GB3;

    invoke-direct {v3, v0}, LX/GB3;-><init>(LX/2EH;)V

    iget-object v4, v0, LX/2EH;->d:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 567775
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567776
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->MSITE_ACCOUNT_RECOVERY:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 567777
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567778
    move-object v3, v3

    .line 567779
    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567780
    iget-object v2, v0, LX/2Ne;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Cy;

    sget-object v3, LX/GB4;->FB4A_ACCOUNT_RECOVERY:LX/GB4;

    invoke-virtual {v3}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/2Cy;->a(Ljava/lang/String;Z)V

    .line 567781
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v0}, LX/2CR;->a()V

    .line 567782
    invoke-direct {p0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method private T()V
    .locals 4

    .prologue
    .line 567749
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567750
    iget-object v1, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/GB4;->HELP_CENTER_CLICK:LX/GB4;

    invoke-virtual {v3}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "account_recovery"

    .line 567751
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567752
    move-object v2, v2

    .line 567753
    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567754
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Q:LX/2Lx;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "help/android-app?ref=Android"

    invoke-virtual {v0, v1, v2}, LX/2Lx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 567755
    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/net/Uri;)V

    .line 567756
    return-void
.end method

.method private U()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 567744
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 567745
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 567746
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 567747
    const-string v2, "login_id"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567748
    :cond_0
    return-object v0
.end method

.method private V()V
    .locals 1

    .prologue
    .line 567742
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aX:LX/0Or;

    if-nez v0, :cond_0

    .line 567743
    :cond_0
    return-void
.end method

.method public static X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 567741
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y()Z
    .locals 4

    .prologue
    .line 567740
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Z()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v1, 0x321

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Z()D
    .locals 4

    .prologue
    .line 567737
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v0, v0

    .line 567738
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v2, v2

    .line 567739
    div-double/2addr v0, v2

    return-wide v0
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 567731
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567732
    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {p0, p2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 567733
    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 567734
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567735
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 567736
    return-void
.end method

.method private static a(LX/0Rf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 567728
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 567729
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 567730
    :cond_0
    return-void
.end method

.method public static a(LX/0Rf;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<+",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 567724
    invoke-virtual {p0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 567725
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;)V

    .line 567726
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 567727
    return-void
.end method

.method private a(LX/28M;)V
    .locals 4

    .prologue
    .line 568004
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->s:LX/0pW;

    .line 568005
    invoke-static {v0}, LX/0pW;->k(LX/0pW;)V

    .line 568006
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0pW;->d:Z

    .line 568007
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I()V

    .line 568008
    if-eqz p1, :cond_0

    .line 568009
    iget-object v0, p1, LX/28M;->b:Ljava/lang/String;

    move-object v0, v0

    .line 568010
    if-nez v0, :cond_0

    .line 568011
    iget-wide v2, p1, LX/28M;->c:J

    move-wide v0, v2

    .line 568012
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 568013
    iput-object v0, p1, LX/28M;->b:Ljava/lang/String;

    .line 568014
    :cond_0
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 568015
    return-void
.end method

.method private a(LX/28P;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const v2, 0x1080027

    .line 567972
    iget v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aH:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aH:I

    .line 567973
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567974
    iget-boolean v1, v0, LX/276;->j:Z

    move v0, v1

    .line 567975
    if-eqz v0, :cond_0

    .line 567976
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567977
    invoke-static {v0}, LX/276;->j(LX/276;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 567978
    :cond_0
    :goto_0
    const/4 v0, 0x3

    .line 567979
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 567980
    const/4 v0, 0x1

    .line 567981
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 567982
    iget v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aH:I

    if-lt v1, v0, :cond_3

    .line 567983
    const v0, 0x7f0800d7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0800d9

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0800db

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/28P;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    const v0, 0x7f0800de

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/2EI;

    invoke-direct {v7, p0}, LX/2EI;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    .line 567984
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 567985
    :goto_2
    return-void

    .line 567986
    :cond_2
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ab()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 567987
    const/4 v0, 0x5

    goto :goto_1

    .line 567988
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ao:LX/2NM;

    .line 567989
    iget-object v1, v0, LX/2NM;->l:LX/10N;

    invoke-virtual {v1}, LX/10N;->o()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 567990
    iget-object v1, v0, LX/2NM;->i:LX/10M;

    invoke-virtual {v1}, LX/10M;->c()I

    move-result v1

    const/16 v3, 0x9

    if-le v1, v3, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 567991
    if-nez v1, :cond_7

    invoke-static {v0, v5}, LX/2NM;->d(LX/2NM;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 567992
    invoke-static {v0, v5}, LX/2NM;->e(LX/2NM;Ljava/lang/String;)Lcom/facebook/openidconnect/model/OpenIDCredential;

    move-result-object v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 567993
    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_5
    move v0, v1

    .line 567994
    if-eqz v0, :cond_4

    .line 567995
    const v0, 0x7f0800d7

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0800da

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0800dc

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v5}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v5

    const v0, 0x7f0800de

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/2On;

    invoke-direct {v7, p0}, LX/2On;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    .line 567996
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_2

    .line 567997
    :cond_4
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ac()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 567998
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800d7

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0800d8

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    invoke-virtual {v0, v1, v8}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0800dd

    invoke-direct {p0, p1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/28P;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_2

    .line 567999
    :cond_5
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    const/16 v1, 0x191

    invoke-virtual {v0, v1, p1}, LX/2CR;->a(ILX/28P;)Z

    goto/16 :goto_2

    .line 568000
    :cond_6
    iget-object v1, v0, LX/276;->d:LX/2wX;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/276;->d:LX/2wX;

    invoke-virtual {v1}, LX/2wX;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/276;->g:Lcom/google/android/gms/auth/api/credentials/Credential;

    if-eqz v1, :cond_0

    .line 568001
    sget-object v1, LX/G7R;->i:LX/G7V;

    iget-object v3, v0, LX/276;->d:LX/2wX;

    iget-object v4, v0, LX/276;->g:Lcom/google/android/gms/auth/api/credentials/Credential;

    invoke-interface {v1, v3, v4}, LX/G7V;->a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/Credential;)LX/2wg;

    .line 568002
    const/4 v1, 0x0

    iput-object v1, v0, LX/276;->g:Lcom/google/android/gms/auth/api/credentials/Credential;

    .line 568003
    iget-object v1, v0, LX/276;->c:LX/0if;

    sget-object v3, LX/0ig;->be:LX/0ih;

    sget-object v4, LX/27W;->SMARTLOCK_DELETE:LX/27W;

    invoke-virtual {v4}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_4
.end method

.method private a(LX/2NX;)V
    .locals 2

    .prologue
    .line 568405
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bR:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 568406
    :goto_0
    return-void

    .line 568407
    :cond_0
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    if-ne p1, v0, :cond_1

    const v0, 0x7f0a00d5

    .line 568408
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bR:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 568409
    :cond_1
    const v0, 0x7f0a00d1

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 568400
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 568401
    :try_start_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 568402
    :goto_0
    return-void

    .line 568403
    :catch_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ae:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080109

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 568404
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    const-string v1, "LoginPageBrowserMissing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ActivityNotFoundException when attempting to open web view to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;LX/28M;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 568258
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    invoke-virtual {v0}, LX/2DR;->h()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_7

    move v0, v1

    .line 568259
    :goto_0
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1CA;->A:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 568260
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    .line 568261
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->az:LX/2NO;

    sget-object v3, LX/27f;->FB4A_LOGIN_FIRST_BOOT_PAGE:LX/27f;

    invoke-virtual {v0, v3}, LX/2NO;->a(LX/27f;)I

    move-result v0

    .line 568262
    if-ltz v0, :cond_0

    .line 568263
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v4, "FB4A_LOGIN_FIRST_BOOT"

    .line 568264
    packed-switch v0, :pswitch_data_0

    .line 568265
    const-string v5, "unset"

    :goto_1
    move-object v5, v5

    .line 568266
    invoke-virtual {v3, v4, v5}, LX/2Cy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 568267
    const/4 v3, 0x3

    if-ge v0, v3, :cond_0

    .line 568268
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/facebook/katana/dbl/activity/FirstBootActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 568269
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 568270
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->finish()V

    .line 568271
    :cond_0
    const v0, 0x7f030604

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->setContentView(I)V

    .line 568272
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->O:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 568273
    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 568274
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->o()V

    .line 568275
    :cond_1
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v4, "FULL_WIDTH_REG_BUTTON"

    invoke-virtual {v3, v4, v0}, LX/2Cy;->a(Ljava/lang/String;LX/03R;)V

    .line 568276
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->am()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bd:Z

    .line 568277
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->be:Z

    .line 568278
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 568279
    const v0, 0x7f0d108d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bR:Landroid/widget/FrameLayout;

    .line 568280
    const v0, 0x7f0d10a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    .line 568281
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 568282
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->p()Landroid/text/TextWatcher;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 568283
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0800d0

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setHint(I)V

    .line 568284
    const v0, 0x7f0d10aa

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    .line 568285
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 568286
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->p()Landroid/text/TextWatcher;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 568287
    const v0, 0x7f0d0955

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    .line 568288
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->q()Landroid/text/TextWatcher;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 568289
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bd:Z

    if-eqz v0, :cond_2

    .line 568290
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x()V

    .line 568291
    :cond_2
    const v0, 0x7f0d10ac

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    .line 568292
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 568293
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568294
    const v0, 0x7f0d10af

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bu:Lcom/facebook/resources/ui/FbButton;

    .line 568295
    const v0, 0x7f0d10b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bv:Lcom/facebook/resources/ui/FbButton;

    .line 568296
    const v0, 0x7f0d10b1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bP:Landroid/widget/TextView;

    .line 568297
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bP:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568298
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bu:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568299
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bu:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 568300
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bv:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568301
    const v0, 0x7f0d10a4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bw:Lcom/facebook/resources/ui/FbButton;

    .line 568302
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bw:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568303
    const v0, 0x7f0d10a5

    const v3, 0x7f080158

    invoke-direct {p0, v0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(II)V

    .line 568304
    const v0, 0x7f0d10b4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    .line 568305
    const v0, 0x7f0d10b5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    .line 568306
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568307
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568308
    const v0, 0x7f0d0c1f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    .line 568309
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568310
    const v0, 0x7f0d10ab

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    .line 568311
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af:LX/23P;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 568312
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568313
    const v0, 0x7f0d1096

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    .line 568314
    const v0, 0x7f0d1097

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bG:Landroid/view/View;

    .line 568315
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bG:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568316
    const v0, 0x7f0d109b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bs:Landroid/widget/LinearLayout;

    .line 568317
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->S:LX/2JA;

    invoke-virtual {v0}, LX/2JA;->a()V

    .line 568318
    const v0, 0x7f0d1098

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 568319
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->R:LX/0dz;

    invoke-virtual {v3}, LX/0dz;->a()Ljava/lang/String;

    move-result-object v3

    .line 568320
    if-nez v3, :cond_3

    .line 568321
    const-string v3, "en"

    .line 568322
    :cond_3
    invoke-static {v3}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 568323
    invoke-static {v3}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568324
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 568325
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 568326
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 568327
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x109000a

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    iget-object v5, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 568328
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 568329
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "redirected_from_dbl"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 568330
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 568331
    :cond_5
    if-eqz p1, :cond_6

    .line 568332
    const-string v0, "nux_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    .line 568333
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/EditText;)V

    .line 568334
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(Landroid/widget/EditText;)V

    .line 568335
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/EditText;)V

    .line 568336
    const v0, 0x7f0d10a7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bx:Landroid/view/View;

    .line 568337
    new-instance v0, LX/27b;

    invoke-direct {v0, p0}, LX/27b;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 568338
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 568339
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 568340
    new-instance v0, LX/2NU;

    invoke-direct {v0, p0}, LX/2NU;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aC:LX/2NU;

    .line 568341
    const v0, 0x7f0d10bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->by:Landroid/widget/ProgressBar;

    .line 568342
    const v0, 0x7f0d108e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    .line 568343
    const v0, 0x7f0d10b6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bn:Landroid/widget/RelativeLayout;

    .line 568344
    const v0, 0x7f0d10bc

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aF:Landroid/view/View;

    .line 568345
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bn:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aF:Landroid/view/View;

    invoke-static {v0, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    .line 568346
    const v0, 0x7f0d10a2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bo:Landroid/widget/LinearLayout;

    .line 568347
    const v0, 0x7f0d10a6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bp:Landroid/widget/LinearLayout;

    .line 568348
    const v0, 0x7f0d10ad

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bq:Landroid/widget/LinearLayout;

    .line 568349
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bo:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bp:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bq:Landroid/widget/LinearLayout;

    invoke-static {v0, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    .line 568350
    const v0, 0x7f0d10b8

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    .line 568351
    const v0, 0x7f0d1095

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    .line 568352
    const v0, 0x7f0d1090

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bz:Landroid/widget/RelativeLayout;

    .line 568353
    invoke-direct {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Z)V

    .line 568354
    const v0, 0x7f0d108f

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    .line 568355
    const v0, 0x7f0d10a1

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bE:Landroid/view/View;

    .line 568356
    const v0, 0x7f0d10b2

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bF:Landroid/view/View;

    .line 568357
    const v0, 0x7f0d10b3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    .line 568358
    if-eqz p2, :cond_a

    .line 568359
    invoke-direct {p0, p2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/28M;)V

    .line 568360
    :goto_4
    return-void

    :cond_7
    move v0, v2

    .line 568361
    goto/16 :goto_0

    .line 568362
    :cond_8
    const v0, 0x7f0800cf

    goto/16 :goto_2

    .line 568363
    :cond_9
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 568364
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->n()Z

    move-result v0

    if-nez v0, :cond_4

    .line 568365
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 568366
    :cond_a
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aq:LX/2NN;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    new-instance v3, LX/27c;

    invoke-direct {v3, p0}, LX/27c;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    const/4 v5, 0x0

    .line 568367
    const/4 v4, 0x0

    .line 568368
    iget-object v6, v0, LX/2NN;->e:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, v0, LX/2NN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/29H;->b:LX/0Tn;

    invoke-interface {v6, v7, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 568369
    :cond_b
    sget-object v4, LX/2NN;->a:Ljava/lang/Class;

    const-string v6, "Tos Acceptance Dialog enabled, running checks."

    invoke-static {v4, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 568370
    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 568371
    iget-object v8, v0, LX/2NN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/29H;->c:LX/0Tn;

    invoke-interface {v8, v9, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v8

    .line 568372
    if-eq v8, v6, :cond_c

    const/4 v9, 0x2

    if-ne v8, v9, :cond_10

    .line 568373
    :cond_c
    sget-object v4, LX/2NN;->a:Ljava/lang/Class;

    const-string v6, "Tos Acceptance state cached, dialog not required."

    invoke-static {v4, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move v4, v7

    .line 568374
    :goto_5
    move v4, v4

    .line 568375
    :cond_d
    move v4, v4

    .line 568376
    if-eqz v4, :cond_f

    .line 568377
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 568378
    const-string v4, "target_app"

    invoke-virtual {v6, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 568379
    const-string v4, "sdk_dialog_reason"

    iget-object v7, v0, LX/2NN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/29H;->d:LX/0Tn;

    invoke-interface {v7, v8, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568380
    new-instance v4, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;

    invoke-direct {v4}, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;-><init>()V

    .line 568381
    invoke-virtual {v4, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568382
    iput-object v3, v4, Lcom/facebook/oxygen/preloads/integration/tosacceptance/TosAcceptanceDialogFragment;->o:LX/27c;

    .line 568383
    invoke-virtual {v4, v2, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 568384
    :goto_6
    invoke-static {}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 568385
    sget-object v0, LX/2NP;->FETCHING_SSO_DATA:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 568386
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27d;

    invoke-virtual {v0}, LX/27d;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 568387
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bT:LX/0TF;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->B:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_4

    .line 568388
    :cond_e
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$11;

    invoke-direct {v1, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$11;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    const-wide/16 v2, 0x3e8

    const v4, -0x2a75e20f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_4

    .line 568389
    :pswitch_0
    const-string v5, "normal_button"

    goto/16 :goto_1

    .line 568390
    :pswitch_1
    const-string v5, "radio_button"

    goto/16 :goto_1

    .line 568391
    :pswitch_2
    const-string v5, "radio_button_with_default"

    goto/16 :goto_1

    .line 568392
    :pswitch_3
    const-string v5, "control"

    goto/16 :goto_1

    :cond_f
    goto :goto_6

    .line 568393
    :cond_10
    if-ne v8, v4, :cond_11

    .line 568394
    sget-object v6, LX/2NN;->a:Ljava/lang/Class;

    const-string v7, "Tos Acceptance state cached, dialog required."

    invoke-static {v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_5

    .line 568395
    :cond_11
    iget-object v8, v0, LX/2NN;->b:Landroid/content/Context;

    invoke-static {v8}, LX/29I;->a(Landroid/content/Context;)LX/2BP;

    move-result-object v8

    .line 568396
    sget-object v9, LX/2NN;->a:Ljava/lang/Class;

    const-string v10, "Tos Acceptance state queried, dialog required = %b"

    new-array p1, v4, [Ljava/lang/Object;

    iget-boolean p2, v8, LX/2BP;->a:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    aput-object p2, p1, v7

    invoke-static {v9, v10, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 568397
    iget-object v7, v0, LX/2NN;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v9, LX/29H;->c:LX/0Tn;

    iget-boolean v10, v8, LX/2BP;->a:Z

    if-eqz v10, :cond_12

    :goto_7
    invoke-interface {v7, v9, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v4

    sget-object v6, LX/29H;->d:LX/0Tn;

    iget-object v7, v8, LX/2BP;->c:LX/29k;

    invoke-virtual {v7}, LX/29k;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 568398
    iget-boolean v4, v8, LX/2BP;->a:Z

    goto/16 :goto_5

    :cond_12
    move v4, v6

    .line 568399
    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/view/View;I)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 568224
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    .line 568225
    if-nez v0, :cond_0

    .line 568226
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 568227
    :cond_0
    invoke-direct {p0, p2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->f(I)I

    move-result v3

    .line 568228
    sub-int v3, v0, v3

    .line 568229
    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v4, v3, 0x5

    .line 568230
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v3, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 568231
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 568232
    iget-object v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 568233
    :cond_1
    invoke-direct {p0, v5}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(I)V

    .line 568234
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    .line 568235
    if-nez v3, :cond_2

    .line 568236
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 568237
    :cond_2
    sub-int/2addr v0, v3

    div-int/lit8 v6, v0, 0x2

    .line 568238
    sub-int v0, v6, v4

    .line 568239
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 568240
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bz:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    .line 568241
    if-nez v0, :cond_3

    .line 568242
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 568243
    :cond_3
    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 568244
    sub-int v0, v6, v0

    move v3, v0

    .line 568245
    :goto_0
    new-instance v11, Landroid/view/animation/AnimationSet;

    invoke-direct {v11, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 568246
    const-wide/16 v6, 0x1f4

    invoke-virtual {v11, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 568247
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 568248
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 568249
    new-instance v0, LX/27n;

    invoke-direct {v0, p0, p1}, LX/27n;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/view/View;)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 568250
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    if-nez v0, :cond_4

    .line 568251
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020702

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    .line 568252
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    .line 568253
    div-float v3, v0, v2

    .line 568254
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    const/high16 v8, 0x3f000000    # 0.5f

    move v4, v12

    move v5, v3

    move v6, v12

    move v7, v1

    move v9, v1

    move v10, v12

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 568255
    invoke-virtual {v11, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 568256
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bz:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568257
    return-void

    :cond_5
    move v3, v0

    goto :goto_0
.end method

.method private a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 568222
    new-instance v0, LX/2sT;

    invoke-direct {v0, p0}, LX/2sT;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 568223
    return-void
.end method

.method private a(Landroid/widget/EditText;I)V
    .locals 1

    .prologue
    .line 568220
    invoke-virtual {p0, p2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 568221
    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 1

    .prologue
    .line 568217
    invoke-virtual {p0, p2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568218
    return-void
.end method

.method private static a(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0pW;LX/2DR;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Lcom/facebook/content/SecureContextHelper;LX/2Cy;LX/0iA;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Ot;LX/2Cz;LX/10N;LX/2CQ;LX/10M;LX/2CR;Landroid/content/Context;LX/0hx;LX/10O;LX/2CS;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/2Lx;LX/0dz;LX/2JA;LX/0Ot;LX/0yc;LX/0dC;LX/295;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/27w;LX/0Ot;LX/0Ot;LX/0Ot;LX/23P;LX/0Ot;LX/0W9;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2J4;LX/2NM;LX/0Ot;LX/2NN;LX/0Ot;LX/0Ot;LX/0Uh;LX/0Uh;LX/276;LX/0Or;LX/0Or;LX/0Or;LX/2NO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0pW;",
            "LX/2DR;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0aG;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2Cy;",
            "LX/0iA;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/27d;",
            ">;",
            "LX/2Cz;",
            "LX/10N;",
            "LX/2CQ;",
            "LX/10M;",
            "LX/2CR;",
            "Landroid/content/Context;",
            "LX/0hx;",
            "LX/10O;",
            "LX/2CS;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/2Lx;",
            "LX/0dz;",
            "LX/2JA;",
            "LX/0Ot",
            "<",
            "LX/27j;",
            ">;",
            "LX/0yc;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/295;",
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ne;",
            ">;",
            "LX/27w;",
            "LX/0Ot",
            "<",
            "LX/2EH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ag;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/23P;",
            "LX/0Ot",
            "<",
            "LX/0hw;",
            ">;",
            "LX/0W9;",
            "LX/0Ot",
            "<",
            "LX/27g;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/27t;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2CZ;",
            ">;",
            "LX/2J4;",
            "LX/2NM;",
            "LX/0Ot",
            "<",
            "LX/0hv;",
            ">;",
            "LX/2NN;",
            "LX/0Ot",
            "<",
            "LX/294;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/276;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2NO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 568216
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->s:LX/0pW;

    iput-object p3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    iput-object p4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->v:LX/0Sh;

    iput-object p6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    iput-object p7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    iput-object p8, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    iput-object p9, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->z:LX/0iA;

    iput-object p10, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A:Ljava/util/concurrent/ExecutorService;

    iput-object p11, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->B:Ljava/util/concurrent/ExecutorService;

    iput-object p12, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    iput-object p13, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->D:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->E:LX/2Cz;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G:LX/2CQ;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->J:Landroid/content/Context;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->K:LX/0hx;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L:LX/10O;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->M:LX/2CS;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->N:LX/0Or;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->O:LX/0Or;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->P:Ljava/lang/Boolean;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Q:LX/2Lx;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->R:LX/0dz;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->S:LX/2JA;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->U:LX/0yc;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->V:LX/0dC;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->W:LX/295;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X:LX/0Ot;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Y:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Z:LX/0Or;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ab:LX/27w;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ac:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ad:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ae:LX/0Ot;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af:LX/23P;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ag:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ah:LX/0W9;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ai:LX/0Ot;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj:LX/0Ot;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ak:LX/0Ot;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->al:LX/0Ot;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->am:LX/2J4;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ao:LX/2NM;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ap:LX/0Ot;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aq:LX/2NN;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ar:LX/0Ot;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->as:LX/0Ot;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->at:LX/0Uh;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aw:LX/0Or;

    move-object/from16 v0, p58

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ax:LX/0Or;

    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ay:LX/0Or;

    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->az:LX/2NO;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 63

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v62

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static/range {v62 .. v62}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v62 .. v62}, LX/0pW;->a(LX/0QB;)LX/0pW;

    move-result-object v4

    check-cast v4, LX/0pW;

    invoke-static/range {v62 .. v62}, LX/2DR;->a(LX/0QB;)LX/2DR;

    move-result-object v5

    check-cast v5, LX/2DR;

    invoke-static/range {v62 .. v62}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v62 .. v62}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static/range {v62 .. v62}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static/range {v62 .. v62}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v62 .. v62}, LX/2Cy;->a(LX/0QB;)LX/2Cy;

    move-result-object v10

    check-cast v10, LX/2Cy;

    invoke-static/range {v62 .. v62}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v11

    check-cast v11, LX/0iA;

    invoke-static/range {v62 .. v62}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v62 .. v62}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v62 .. v62}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    const/16 v15, 0x187

    move-object/from16 v0, v62

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v62 .. v62}, LX/2Cz;->a(LX/0QB;)LX/2Cz;

    move-result-object v16

    check-cast v16, LX/2Cz;

    invoke-static/range {v62 .. v62}, LX/10N;->a(LX/0QB;)LX/10N;

    move-result-object v17

    check-cast v17, LX/10N;

    invoke-static/range {v62 .. v62}, LX/2CQ;->a(LX/0QB;)LX/2CQ;

    move-result-object v18

    check-cast v18, LX/2CQ;

    invoke-static/range {v62 .. v62}, LX/10M;->a(LX/0QB;)LX/10M;

    move-result-object v19

    check-cast v19, LX/10M;

    invoke-static/range {v62 .. v62}, LX/2CR;->a(LX/0QB;)LX/2CR;

    move-result-object v20

    check-cast v20, LX/2CR;

    const-class v21, Landroid/content/Context;

    move-object/from16 v0, v62

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/content/Context;

    invoke-static/range {v62 .. v62}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v22

    check-cast v22, LX/0hx;

    invoke-static/range {v62 .. v62}, LX/10O;->a(LX/0QB;)LX/10O;

    move-result-object v23

    check-cast v23, LX/10O;

    invoke-static/range {v62 .. v62}, LX/2CS;->a(LX/0QB;)LX/2CS;

    move-result-object v24

    check-cast v24, LX/2CS;

    const/16 v25, 0x325

    move-object/from16 v0, v62

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    const/16 v26, 0x326

    move-object/from16 v0, v62

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    invoke-static/range {v62 .. v62}, LX/2NT;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v27

    check-cast v27, Ljava/lang/Boolean;

    invoke-static/range {v62 .. v62}, LX/2Lx;->a(LX/0QB;)LX/2Lx;

    move-result-object v28

    check-cast v28, LX/2Lx;

    invoke-static/range {v62 .. v62}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v29

    check-cast v29, LX/0dz;

    invoke-static/range {v62 .. v62}, LX/2JA;->a(LX/0QB;)LX/2JA;

    move-result-object v30

    check-cast v30, LX/2JA;

    const/16 v31, 0xc65

    move-object/from16 v0, v62

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    invoke-static/range {v62 .. v62}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v32

    check-cast v32, LX/0yc;

    invoke-static/range {v62 .. v62}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v33

    check-cast v33, LX/0dC;

    invoke-static/range {v62 .. v62}, LX/295;->a(LX/0QB;)LX/295;

    move-result-object v34

    check-cast v34, LX/295;

    const/16 v35, 0x2f1

    move-object/from16 v0, v62

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v35

    const/16 v36, 0x2f4

    move-object/from16 v0, v62

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x2f5

    move-object/from16 v0, v62

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v37

    const/16 v38, 0x69

    move-object/from16 v0, v62

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v38

    invoke-static/range {v62 .. v62}, LX/27w;->a(LX/0QB;)LX/27w;

    move-result-object v39

    check-cast v39, LX/27w;

    const/16 v40, 0x66

    move-object/from16 v0, v62

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v41, 0xb3a

    move-object/from16 v0, v62

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v41

    const/16 v42, 0x12c4

    move-object/from16 v0, v62

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v42

    invoke-static/range {v62 .. v62}, LX/23P;->a(LX/0QB;)LX/23P;

    move-result-object v43

    check-cast v43, LX/23P;

    const/16 v44, 0xb3e

    move-object/from16 v0, v62

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v44

    invoke-static/range {v62 .. v62}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v45

    check-cast v45, LX/0W9;

    const/16 v46, 0xc61

    move-object/from16 v0, v62

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v47, 0x17d

    move-object/from16 v0, v62

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v47

    const/16 v48, 0x4cc

    move-object/from16 v0, v62

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v48

    const/16 v49, 0x8a

    move-object/from16 v0, v62

    move/from16 v1, v49

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v49

    const-class v50, LX/2J4;

    move-object/from16 v0, v62

    move-object/from16 v1, v50

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v50

    check-cast v50, LX/2J4;

    invoke-static/range {v62 .. v62}, LX/2NM;->a(LX/0QB;)LX/2NM;

    move-result-object v51

    check-cast v51, LX/2NM;

    const/16 v52, 0x5ee

    move-object/from16 v0, v62

    move/from16 v1, v52

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v52

    invoke-static/range {v62 .. v62}, LX/2NN;->a(LX/0QB;)LX/2NN;

    move-result-object v53

    check-cast v53, LX/2NN;

    const/16 v54, 0xa3

    move-object/from16 v0, v62

    move/from16 v1, v54

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v54

    const/16 v55, 0x2e3

    move-object/from16 v0, v62

    move/from16 v1, v55

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v55

    invoke-static/range {v62 .. v62}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v56

    check-cast v56, LX/0Uh;

    invoke-static/range {v62 .. v62}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v57

    check-cast v57, LX/0Uh;

    invoke-static/range {v62 .. v62}, LX/276;->a(LX/0QB;)LX/276;

    move-result-object v58

    check-cast v58, LX/276;

    const/16 v59, 0x32b

    move-object/from16 v0, v62

    move/from16 v1, v59

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v59

    const/16 v60, 0x32c

    move-object/from16 v0, v62

    move/from16 v1, v60

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v60

    const/16 v61, 0x327

    move-object/from16 v0, v62

    move/from16 v1, v61

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v61

    invoke-static/range {v62 .. v62}, LX/2NO;->a(LX/0QB;)LX/2NO;

    move-result-object v62

    check-cast v62, LX/2NO;

    invoke-static/range {v2 .. v62}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0pW;LX/2DR;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0aG;Lcom/facebook/content/SecureContextHelper;LX/2Cy;LX/0iA;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Ot;LX/2Cz;LX/10N;LX/2CQ;LX/10M;LX/2CR;Landroid/content/Context;LX/0hx;LX/10O;LX/2CS;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/2Lx;LX/0dz;LX/2JA;LX/0Ot;LX/0yc;LX/0dC;LX/295;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/27w;LX/0Ot;LX/0Ot;LX/0Ot;LX/23P;LX/0Ot;LX/0W9;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2J4;LX/2NM;LX/0Ot;LX/2NN;LX/0Ot;LX/0Ot;LX/0Uh;LX/0Uh;LX/276;LX/0Or;LX/0Or;LX/0Or;LX/2NO;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;S)V
    .locals 3

    .prologue
    const v2, 0x230001

    .line 568209
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 568210
    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568211
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 568212
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 568213
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->K()V

    .line 568214
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    const-string v1, "FacebookLogin failure"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 568215
    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 568162
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 568163
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->be:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    if-eq p1, v0, :cond_0

    .line 568164
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t()V

    .line 568165
    :cond_0
    sget-object v0, LX/2Nb;->a:[I

    invoke-virtual {p1}, LX/2NP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 568166
    :cond_1
    :goto_0
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/2NP;->SSO_AUTH_UI:LX/2NP;

    if-ne p1, v0, :cond_4

    .line 568167
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aA:LX/2NP;

    sget-object v1, LX/2NP;->SPLASHSCREEN:LX/2NP;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aA:LX/2NP;

    sget-object v1, LX/2NP;->FETCHING_SSO_DATA:LX/2NP;

    if-ne v0, v1, :cond_6

    .line 568168
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;)V

    .line 568169
    invoke-static {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568170
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 568171
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    if-ne p1, v0, :cond_5

    .line 568172
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bp:Landroid/widget/LinearLayout;

    .line 568173
    const/16 v1, 0xe4

    .line 568174
    :goto_1
    add-int/lit8 v1, v1, 0x70

    .line 568175
    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/view/View;I)V

    .line 568176
    :cond_4
    :goto_2
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aA:LX/2NP;

    .line 568177
    return-void

    .line 568178
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bn:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568179
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->by:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 568180
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bn:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568181
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->by:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 568182
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568183
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bo:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568184
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568185
    invoke-direct {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(Z)V

    goto :goto_0

    .line 568186
    :pswitch_3
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->be:Z

    .line 568187
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568188
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bp:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568189
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568190
    invoke-static {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568191
    invoke-direct {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(Z)V

    .line 568192
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bd:Z

    if-eqz v0, :cond_1

    .line 568193
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u()V

    goto/16 :goto_0

    .line 568194
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568195
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bq:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568196
    invoke-static {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568197
    invoke-direct {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(Z)V

    goto/16 :goto_0

    .line 568198
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bh:LX/0Rf;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aF:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;Landroid/view/View;)V

    .line 568199
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bi:LX/0Rf;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/0Rf;)V

    .line 568200
    invoke-static {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V

    .line 568201
    invoke-direct {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(Z)V

    goto/16 :goto_0

    .line 568202
    :cond_5
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bo:Landroid/widget/LinearLayout;

    .line 568203
    const/16 v1, 0x8c

    goto/16 :goto_1

    .line 568204
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 568205
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 568206
    invoke-direct {p0, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(I)V

    .line 568207
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bE:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 568208
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bF:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NV;)V
    .locals 6

    .prologue
    .line 568138
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568139
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568140
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 568141
    const v0, 0x7f0800f3

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 568142
    :cond_0
    :goto_0
    return-void

    .line 568143
    :cond_1
    new-instance v1, LX/28M;

    sget-object v2, LX/28K;->PASSWORD:LX/28K;

    invoke-direct {v1, p0, v2}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 568144
    iput-object v0, v1, LX/28M;->b:Ljava/lang/String;

    .line 568145
    sget-object v0, LX/2NV;->PASSWORD_ENTRY:LX/2NV;

    if-ne p1, v0, :cond_2

    .line 568146
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568147
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 568148
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 568149
    const v0, 0x7f0800eb

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 568150
    :cond_2
    sget-object v0, LX/2NV;->TRANSIENT_AUTH_TOKEN_ENTRY:LX/2NV;

    if-ne p1, v0, :cond_4

    .line 568151
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aV:Ljava/lang/String;

    .line 568152
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aW:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 568153
    new-instance v1, LX/28M;

    sget-object v2, LX/28K;->TRANSIENT_TOKEN:LX/28K;

    invoke-direct {v1, p0, v2}, LX/28M;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28K;)V

    .line 568154
    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aW:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 568155
    iput-object v2, v1, LX/28M;->b:Ljava/lang/String;

    .line 568156
    :cond_3
    iput-object v0, v1, LX/28M;->d:Ljava/lang/String;

    .line 568157
    invoke-direct {p0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/28M;)V

    goto :goto_0

    .line 568158
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568159
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 568160
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 568161
    const v0, 0x7f0800f8

    invoke-static {p0, v0}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 568410
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 568411
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 568412
    new-instance v1, LX/28L;

    invoke-direct {v1, p0, p1}, LX/28L;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 568413
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568414
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568415
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bE:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568416
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bF:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568417
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bs:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568418
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 568419
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 568420
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/facebook/auth/credentials/OpenIDLoginCredentials;)V
    .locals 5

    .prologue
    .line 568135
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/28M;)V

    .line 568136
    const-string v0, "openid_login"

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aC:LX/2NU;

    new-instance v3, LX/27m;

    invoke-direct {v3, p0}, LX/27m;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    invoke-static {p1, v0, v1, v2, v3}, LX/2CQ;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V

    .line 568137
    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/google/android/gms/auth/api/credentials/Credential;)V
    .locals 2

    .prologue
    .line 568124
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 568125
    iput-object p1, v0, LX/276;->g:Lcom/google/android/gms/auth/api/credentials/Credential;

    .line 568126
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p1, Lcom/google/android/gms/auth/api/credentials/Credential;->b:Ljava/lang/String;

    move-object v1, v1

    .line 568127
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 568128
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/auth/api/credentials/Credential;->f:Ljava/lang/String;

    move-object v1, v1

    .line 568129
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 568130
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    const/4 v1, 0x1

    .line 568131
    iput-boolean v1, v0, LX/276;->j:Z

    .line 568132
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    invoke-virtual {v0}, LX/276;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    invoke-virtual {v0}, LX/276;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 568133
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->performClick()Z

    .line 568134
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/google/android/gms/common/api/Status;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 568113
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 568114
    iget-boolean v1, v0, LX/276;->i:Z

    move v0, v1

    .line 568115
    if-eqz v0, :cond_1

    .line 568116
    :cond_0
    :goto_0
    return-void

    .line 568117
    :cond_1
    iget v0, p1, Lcom/google/android/gms/common/api/Status;->i:I

    move v0, v0

    .line 568118
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 568119
    :try_start_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    .line 568120
    const/4 v0, 0x4

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/api/Status;->a(Landroid/app/Activity;I)V

    .line 568121
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    const/4 v1, 0x1

    .line 568122
    iput-boolean v1, v0, LX/276;->i:Z
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 568123
    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 568111
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 568112
    return-void
.end method

.method private aa()Z
    .locals 3

    .prologue
    .line 568108
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->N:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 568109
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v2, "fast_pw_error_ar"

    invoke-virtual {v1, v2, v0}, LX/2Cy;->a(Ljava/lang/String;LX/03R;)V

    .line 568110
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()Z
    .locals 3

    .prologue
    .line 568107
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v1, 0x20

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private ac()Z
    .locals 3

    .prologue
    .line 568106
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v1, 0x1e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private ad()V
    .locals 5

    .prologue
    .line 568093
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27g;

    invoke-virtual {v0}, LX/27g;->a()LX/27h;

    move-result-object v0

    .line 568094
    invoke-virtual {v0}, LX/27h;->a()[Ljava/lang/String;

    move-result-object v1

    .line 568095
    new-instance v2, LX/0ju;

    invoke-direct {v2, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 568096
    invoke-virtual {v0}, LX/27h;->b()[Ljava/lang/String;

    move-result-object v3

    .line 568097
    iget v4, v0, LX/27h;->b:I

    move v0, v4

    .line 568098
    new-instance v4, LX/27i;

    invoke-direct {v4, p0, v1}, LX/27i;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;[Ljava/lang/String;)V

    invoke-virtual {v2, v3, v0, v4}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 568099
    const v0, 0x7f083205

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 568100
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 568101
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 568102
    invoke-virtual {v0}, LX/2EJ;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 568103
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    .line 568104
    iget-object v1, v0, LX/27j;->a:LX/0Zb;

    const-string v2, "language_switcher_login_more_clicked"

    invoke-static {v2}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 568105
    return-void
.end method

.method private ae()V
    .locals 2

    .prologue
    .line 568079
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0800f0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 568080
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0800f0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 568081
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0800f1

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/TextView;I)V

    .line 568082
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080103

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/TextView;I)V

    .line 568083
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aY:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0800ee

    .line 568084
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/TextView;I)V

    .line 568085
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bJ:Landroid/widget/TextView;

    const v1, 0x7f08010a

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/TextView;I)V

    .line 568086
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0800d0

    .line 568087
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/EditText;I)V

    .line 568088
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const v1, 0x7f08348e

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/EditText;I)V

    .line 568089
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f0800ec

    invoke-direct {p0, v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/widget/TextView;I)V

    .line 568090
    return-void

    .line 568091
    :cond_0
    const v0, 0x7f0800ed

    goto :goto_0

    .line 568092
    :cond_1
    const v0, 0x7f0800cf

    goto :goto_1
.end method

.method private af()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 568057
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ah:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    .line 568058
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 568059
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->S:LX/2JA;

    invoke-virtual {v0}, LX/2JA;->c()LX/0Px;

    move-result-object v2

    .line 568060
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 568061
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v8, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 568062
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 568063
    :cond_2
    invoke-interface {v1, v6, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    .line 568064
    const v0, 0x7f0d109c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bH:Landroid/widget/TextView;

    .line 568065
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bH:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568066
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bH:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568067
    const v0, 0x7f0d109e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bI:Landroid/widget/TextView;

    .line 568068
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bI:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568069
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bI:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568070
    const v0, 0x7f0d10a0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bJ:Landroid/widget/TextView;

    .line 568071
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bJ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568072
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ba:Z

    if-nez v0, :cond_3

    .line 568073
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 568074
    const-string v4, "language_switcher_login_displayed"

    invoke-static {v4}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 568075
    const-string v5, "current_device_locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "network_country"

    iget-object v8, v0, LX/27j;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "sim_country"

    iget-object v8, v0, LX/27j;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "first_language"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "second_language"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "current_app_locale"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 568076
    iget-object v5, v0, LX/27j;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 568077
    iput-boolean v7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ba:Z

    .line 568078
    :cond_3
    return-void
.end method

.method public static ag(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 2

    .prologue
    .line 568050
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aY:Z

    .line 568051
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    const v1, 0x7f0800ee

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 568052
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af:LX/23P;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 568053
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const/16 v1, 0x91

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 568054
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 568055
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 568056
    return-void
.end method

.method public static ah(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 568018
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    if-eqz v0, :cond_1

    .line 568019
    :cond_0
    iput-boolean v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    .line 568020
    :goto_0
    return-void

    .line 568021
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aG:J

    sub-long/2addr v0, v2

    .line 568022
    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aI:Z

    if-nez v0, :cond_2

    .line 568023
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    const v4, -0x3c52549

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 568024
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aK:Z

    if-eqz v0, :cond_7

    .line 568025
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->N()V

    .line 568026
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ao:LX/2NM;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 568027
    if-nez v0, :cond_9

    .line 568028
    sget-object v2, LX/2NM;->a:Ljava/lang/Class;

    const-string v3, "Could not register user emails, \'user\' was null"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 568029
    :cond_3
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj()Ljava/lang/String;

    move-result-object v2

    .line 568030
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ak()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 568031
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/294;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->as:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5, v2}, LX/294;->a(JLjava/lang/String;)V

    .line 568032
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CZ;

    invoke-virtual {v0}, LX/2CZ;->a()V

    .line 568033
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 568034
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 568035
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v0, v2

    .line 568036
    invoke-static {v1, v0}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 568037
    :cond_5
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->O()V

    .line 568038
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->P()V

    .line 568039
    :goto_2
    iput-boolean v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    goto/16 :goto_0

    .line 568040
    :cond_6
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 568041
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {v2}, LX/0uQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1

    .line 568042
    :cond_7
    iput-boolean v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    .line 568043
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    if-eqz v0, :cond_8

    .line 568044
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    const v1, 0x7f0800e5

    invoke-virtual {p0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0800e8

    invoke-virtual {p0, v2}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 568045
    :cond_8
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    goto :goto_2

    .line 568046
    :cond_9
    iget-object v2, v0, Lcom/facebook/user/model/User;->c:LX/0Px;

    move-object v2, v2

    .line 568047
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/UserEmailAddress;

    .line 568048
    iget-object v5, v2, Lcom/facebook/user/model/UserEmailAddress;->a:Ljava/lang/String;

    move-object v2, v5

    .line 568049
    iget-object v5, v1, LX/2NM;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v7, LX/26p;->s:LX/0Tn;

    invoke-virtual {v7, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    const/4 v7, 0x1

    invoke-interface {v5, v2, v7}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_3
.end method

.method private ai()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 568016
    new-instance v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    const-string v1, ""

    const-string v2, ""

    sget-object v3, LX/28K;->UNSET:LX/28K;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    const-string v1, "auth"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    invoke-static {v0, v1, v2, v4, v4}, LX/2CQ;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V

    .line 568017
    return-void
.end method

.method private aj()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 567821
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    .line 567822
    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 567823
    if-eqz v0, :cond_0

    .line 567824
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 567825
    :goto_0
    return-object v0

    .line 567826
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C:LX/03V;

    const-string v1, "ViewerContextNullAfterLogin"

    const-string v2, "LoggedInUserAuthDataStore returned null ViewerContext after login."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567827
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ak()Z
    .locals 3

    .prologue
    .line 568219
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->at:LX/0Uh;

    const/16 v1, 0x3e2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private al()LX/03R;
    .locals 1

    .prologue
    .line 567155
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aw:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    return-object v0
.end method

.method private am()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 567289
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v2, 0x3e

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 567290
    :goto_0
    return v0

    .line 567291
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ax:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 567292
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v3, "FB4A_WHITE_LOGIN_PAGE"

    invoke-virtual {v2, v3, v0}, LX/2Cy;->a(Ljava/lang/String;LX/03R;)V

    .line 567293
    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0
.end method

.method private an()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 567284
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v2, 0x2d

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 567285
    :goto_0
    return v0

    .line 567286
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ay:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 567287
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v3, "fb4a_hide_register_button"

    invoke-virtual {v2, v3, v0}, LX/2Cy;->a(Ljava/lang/String;LX/03R;)V

    .line 567288
    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0
.end method

.method private b(LX/28P;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 567283
    new-instance v0, LX/2DQ;

    invoke-direct {v0, p0, p1}, LX/2DQ;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/28P;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 567282
    new-instance v0, LX/28O;

    invoke-direct {v0, p0, p1}, LX/28O;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method private b(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8

    .prologue
    .line 567243
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "device_id"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->V:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 567244
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->W:LX/295;

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    sget-object v4, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->q:LX/0Tn;

    const/4 v6, 0x1

    const/4 p1, 0x0

    .line 567245
    iget-object v5, v3, LX/295;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 567246
    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-ne v1, v5, :cond_3

    .line 567247
    invoke-static {v3, v0, v6}, LX/295;->a(LX/295;LX/03R;Z)V

    .line 567248
    :goto_0
    move-object v1, v0

    .line 567249
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->W:LX/295;

    .line 567250
    iget-object v3, v0, LX/295;->b:LX/03R;

    move-object v0, v3

    .line 567251
    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_1

    .line 567252
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567253
    iget-object v3, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->TEST_BOUNCE_FROM_MSITE:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 567254
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567255
    move-object v4, v4

    .line 567256
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567257
    :cond_0
    :goto_1
    sget-object v0, LX/03R;->YES:LX/03R;

    if-eq v1, v0, :cond_2

    move-object v0, v2

    .line 567258
    :goto_2
    return-object v0

    .line 567259
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->W:LX/295;

    .line 567260
    iget-object v3, v0, LX/295;->b:LX/03R;

    move-object v0, v3

    .line 567261
    sget-object v3, LX/03R;->NO:LX/03R;

    if-ne v0, v3, :cond_0

    .line 567262
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567263
    iget-object v3, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->UNSET_BOUNCE_FROM_MSITE:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 567264
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567265
    move-object v4, v4

    .line 567266
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567267
    goto :goto_1

    .line 567268
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567269
    iget-object v1, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->BOUNCE_FROM_MSITE:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 567270
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567271
    move-object v3, v3

    .line 567272
    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567273
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "btp"

    const-string v2, "app"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 567274
    :cond_3
    const-class v5, LX/03R;

    iget-object v6, v3, LX/295;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v7}, LX/03R;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v5

    check-cast v5, LX/03R;

    .line 567275
    invoke-static {v3, v5, p1}, LX/295;->a(LX/295;LX/03R;Z)V

    move-object v0, v5

    .line 567276
    goto/16 :goto_0

    .line 567277
    :cond_4
    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-eq v0, v5, :cond_5

    .line 567278
    invoke-static {v3, v0, v6}, LX/295;->a(LX/295;LX/03R;Z)V

    goto/16 :goto_0

    .line 567279
    :cond_5
    iget-object v5, v3, LX/295;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    invoke-virtual {v1}, LX/03R;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 567280
    invoke-static {v3, v1, p1}, LX/295;->a(LX/295;LX/03R;Z)V

    move-object v0, v1

    .line 567281
    goto/16 :goto_0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 567236
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    .line 567237
    if-nez v1, :cond_0

    .line 567238
    :goto_0
    return-void

    .line 567239
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 567240
    instance-of v2, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v2, :cond_1

    .line 567241
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 567242
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method

.method private b(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 567230
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 567231
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 567232
    :cond_0
    new-instance v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$24;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$24;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;JLjava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    .line 567233
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 567234
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    const v4, 0x236bdf66

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 567235
    :cond_1
    return-void
.end method

.method private b(LX/28M;)V
    .locals 6

    .prologue
    .line 567223
    invoke-direct {p0, p1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/28M;)V

    .line 567224
    new-instance v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 567225
    iget-object v1, p1, LX/28M;->b:Ljava/lang/String;

    move-object v1, v1

    .line 567226
    iget-object v2, p1, LX/28M;->d:Ljava/lang/String;

    move-object v2, v2

    .line 567227
    iget-object v3, p1, LX/28M;->e:LX/28K;

    move-object v3, v3

    .line 567228
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    const-string v1, "auth"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aC:LX/2NU;

    new-instance v4, LX/27m;

    invoke-direct {v4, p0}, LX/27m;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    invoke-static {v0, v1, v2, v3, v4}, LX/2CQ;->a(Lcom/facebook/auth/credentials/LoginCredentials;Ljava/lang/String;Lcom/facebook/katana/service/AppSession;LX/278;LX/0TF;)V

    .line 567229
    return-void
.end method

.method private b(LX/2NX;)V
    .locals 2

    .prologue
    .line 567218
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_0

    .line 567219
    :goto_0
    return-void

    .line 567220
    :cond_0
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    if-ne p1, v0, :cond_1

    const v0, 0x7f0a00a8

    .line 567221
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 567222
    :cond_1
    const v0, 0x7f0a00d5

    goto :goto_1
.end method

.method private b(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 567216
    new-instance v0, LX/27e;

    invoke-direct {v0, p0}, LX/27e;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 567217
    return-void
.end method

.method public static b(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 567172
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    .line 567173
    iput-object p0, v0, LX/2CR;->h:LX/2A4;

    .line 567174
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 567175
    check-cast v0, LX/2Oo;

    .line 567176
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    .line 567177
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2CR;->a(Ljava/lang/String;)LX/28P;

    move-result-object v4

    .line 567178
    sparse-switch v3, :sswitch_data_0

    .line 567179
    const/16 v5, 0x57

    invoke-direct {p0, p1, v5}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Ljava/lang/Throwable;S)V

    .line 567180
    iget-object v5, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v5, v3, v4}, LX/2CR;->a(ILX/28P;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 567181
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v3, v0}, LX/2CR;->a(LX/2Oo;)V

    :cond_0
    move v0, v2

    .line 567182
    :goto_0
    if-eqz v0, :cond_7

    .line 567183
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 567184
    sget-object v0, LX/2NP;->LOGIN_APPPROVAL_UI:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 567185
    :goto_1
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    .line 567186
    return-void

    .line 567187
    :sswitch_0
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->J()V

    .line 567188
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bg:Z

    if-eqz v0, :cond_1

    .line 567189
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Q()V

    move v0, v1

    goto :goto_0

    .line 567190
    :cond_1
    invoke-direct {p0, v4}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/28P;)V

    move v0, v2

    .line 567191
    goto :goto_0

    .line 567192
    :sswitch_1
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->J()V

    .line 567193
    invoke-direct {p0, v4}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c(LX/28P;)V

    move v0, v2

    .line 567194
    goto :goto_0

    .line 567195
    :sswitch_2
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->J()V

    .line 567196
    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bg:Z

    .line 567197
    invoke-direct {p0, v4}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d(LX/28P;)V

    .line 567198
    iget-object v0, v4, LX/28P;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bc:Ljava/lang/String;

    .line 567199
    iget-object v0, v4, LX/28P;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 567200
    iget-object v0, v4, LX/28P;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bb:Ljava/lang/String;

    .line 567201
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bv:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567202
    :goto_2
    iget-wide v2, v4, LX/28P;->a:J

    iput-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aW:J

    .line 567203
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v0, v4}, LX/2CR;->a(LX/28P;)V

    move v0, v1

    .line 567204
    goto :goto_0

    .line 567205
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bv:Lcom/facebook/resources/ui/FbButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_2

    .line 567206
    :cond_3
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_6

    .line 567207
    const/16 v0, 0x61

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Ljava/lang/Throwable;S)V

    .line 567208
    check-cast p1, Ljava/io/IOException;

    .line 567209
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->E:LX/2Cz;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p0, v3}, LX/2Cz;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 567210
    :cond_4
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(Ljava/io/IOException;)V

    :cond_5
    move v0, v2

    .line 567211
    goto :goto_0

    .line 567212
    :cond_6
    const/16 v0, 0x69

    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Ljava/lang/Throwable;S)V

    .line 567213
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    invoke-virtual {v0, p1}, LX/2CR;->a(Ljava/lang/Throwable;)V

    move v0, v2

    goto/16 :goto_0

    .line 567214
    :cond_7
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 567215
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_1
        0x191 -> :sswitch_0
        0x196 -> :sswitch_2
    .end sparse-switch
.end method

.method public static b$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 567164
    if-eqz p1, :cond_0

    move v0, v1

    .line 567165
    :goto_0
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->br:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 567166
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bN:Lcom/facebook/resources/ui/FbButton;

    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567167
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bM:Lcom/facebook/resources/ui/FbButton;

    iget-boolean v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 567168
    return-void

    :cond_0
    move v0, v2

    .line 567169
    goto :goto_0

    :cond_1
    move v0, v2

    .line 567170
    goto :goto_1

    :cond_2
    move v2, v1

    .line 567171
    goto :goto_2
.end method

.method public static synthetic c(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 567163
    invoke-direct {p0, p1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 567156
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->U()Landroid/os/Bundle;

    move-result-object v0

    .line 567157
    const-string v1, "from_login_pw_error"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567158
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 567159
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "cuid"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567160
    if-eqz v1, :cond_0

    .line 567161
    const-string v2, "cuid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567162
    :cond_0
    return-object v0
.end method

.method private c(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 567039
    new-instance v0, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;-><init>(JLjava/lang/String;)V

    .line 567040
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 567041
    const-string v1, "loginApprovalsResendCodeParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 567042
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    const-string v1, "login_approval_resend_code"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x41e28ea3

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 567043
    new-instance v1, LX/27V;

    invoke-direct {v1, p0}, LX/27V;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 567044
    return-void
.end method

.method private c(LX/28P;)V
    .locals 12

    .prologue
    .line 567046
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 567047
    iget-object v1, p1, LX/28P;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 567048
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p1, LX/28P;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 567049
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->I:LX/2CR;

    const/4 v9, 0x0

    .line 567050
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 567051
    iget-object v5, v1, LX/2CR;->o:LX/0Uh;

    const/16 v6, 0x2e

    invoke-virtual {v5, v6, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-nez v5, :cond_3

    .line 567052
    :cond_1
    :goto_0
    move v3, v3

    .line 567053
    if-nez v3, :cond_2

    .line 567054
    const/16 v3, 0x190

    invoke-virtual {v1, v3, p1}, LX/2CR;->a(ILX/28P;)Z

    .line 567055
    :goto_1
    return-void

    .line 567056
    :cond_2
    iget-object v3, v1, LX/2CR;->f:Landroid/content/res/Resources;

    const v4, 0x7f0800d4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 567057
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 567058
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 567059
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v6, 0x1

    invoke-direct {v3, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v4

    const/16 v7, 0x12

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 567060
    iget-object v3, v1, LX/2CR;->f:Landroid/content/res/Resources;

    const v4, 0x7f0800d3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0ax;->z:Ljava/lang/String;

    iget-object v3, v1, LX/2CR;->f:Landroid/content/res/Resources;

    const v7, 0x7f0800d6

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v3, v1, LX/2CR;->f:Landroid/content/res/Resources;

    const v8, 0x7f0800d5

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v3, v1

    move v10, v9

    move v11, v9

    invoke-static/range {v3 .. v11}, LX/2CR;->a(LX/2CR;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_1

    .line 567061
    :cond_3
    iget-object v5, p1, LX/28P;->g:Ljava/lang/String;

    const-string v6, "email"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_4

    iget-object v5, p1, LX/28P;->g:Ljava/lang/String;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v7, :cond_6

    :cond_4
    move v5, v4

    .line 567062
    :goto_2
    if-eqz v5, :cond_1

    .line 567063
    iget-object v5, p1, LX/28P;->k:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 567064
    iget v5, v1, LX/2CR;->n:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v1, LX/2CR;->n:I

    .line 567065
    iget-object v5, v1, LX/2CR;->m:LX/2NO;

    sget-object v6, LX/27f;->FB4A_LOGIN_PREFILL_AND_REDIRECT:LX/27f;

    invoke-virtual {v5, v6}, LX/2NO;->a(LX/27f;)I

    move-result v5

    .line 567066
    iget-object v6, v1, LX/2CR;->i:LX/2Cy;

    const-string v7, "LOGIN_PREFILL_AND_REDIRECT_SOURCE_REDIRECT"

    invoke-static {v5}, LX/2DR;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/2Cy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567067
    if-eq v5, v4, :cond_5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_7

    :cond_5
    move v3, v4

    .line 567068
    goto/16 :goto_0

    :cond_6
    move v5, v3

    .line 567069
    goto :goto_2

    .line 567070
    :cond_7
    if-eq v5, v10, :cond_8

    const/4 v6, 0x5

    if-ne v5, v6, :cond_9

    .line 567071
    :cond_8
    iget v5, v1, LX/2CR;->n:I

    if-lt v5, v10, :cond_1

    move v3, v4

    .line 567072
    goto/16 :goto_0

    .line 567073
    :cond_9
    if-eq v5, v11, :cond_a

    const/4 v6, 0x6

    if-ne v5, v6, :cond_1

    .line 567074
    :cond_a
    iget v5, v1, LX/2CR;->n:I

    if-lt v5, v11, :cond_1

    move v3, v4

    .line 567075
    goto/16 :goto_0
.end method

.method private c(LX/2NX;)V
    .locals 2

    .prologue
    .line 567076
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 567077
    :goto_0
    return-void

    .line 567078
    :cond_0
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    if-ne p1, v0, :cond_1

    const v0, 0x7f020703

    .line 567079
    :goto_1
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-static {p0, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 567080
    :cond_1
    const v0, 0x7f020702

    goto :goto_1
.end method

.method public static c$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Z)V
    .locals 2

    .prologue
    .line 567081
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 567082
    return-void

    .line 567083
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 567084
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 p1, 0x8

    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 567085
    return-void
.end method

.method private d(LX/28P;)V
    .locals 5

    .prologue
    .line 567086
    iget-wide v2, p1, LX/28P;->a:J

    .line 567087
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 567088
    iget-object v0, p1, LX/28P;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/28P;->i:Ljava/lang/String;

    .line 567089
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, LX/28P;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 567090
    iget-object v1, p1, LX/28P;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aV:Ljava/lang/String;

    .line 567091
    iput-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aW:J

    .line 567092
    invoke-direct {p0, v2, v3, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(JLjava/lang/String;)V

    .line 567093
    :cond_0
    return-void

    .line 567094
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Z)V
    .locals 1

    .prologue
    .line 567095
    iput-boolean p1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aR:Z

    .line 567096
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V

    .line 567097
    return-void

    .line 567098
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static d$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 567294
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ah:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 567295
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->R:LX/0dz;

    invoke-virtual {v0, p1}, LX/0dz;->a(Ljava/lang/String;)V

    .line 567296
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    .line 567297
    const-string v2, "language_switcher_login_language_selected"

    invoke-static {v2}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 567298
    const-string v3, "old_language"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "new_language"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 567299
    iget-object v3, v0, LX/27j;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 567300
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ae()V

    .line 567301
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af()V

    .line 567302
    return-void
.end method

.method public static e(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;I)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 567099
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aR:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->S:LX/2JA;

    .line 567100
    iget-object v4, v0, LX/2JA;->g:LX/0Px;

    if-eqz v4, :cond_7

    iget-object v4, v0, LX/2JA;->g:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_7

    const/4 v4, 0x1

    :goto_0
    move v0, v4

    .line 567101
    if-eqz v0, :cond_3

    move v0, v1

    .line 567102
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aR:Z

    if-eqz v4, :cond_4

    if-nez v0, :cond_4

    .line 567103
    :goto_2
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bs:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 567104
    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 567105
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af()V

    .line 567106
    iget-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bd:Z

    if-eqz v2, :cond_0

    .line 567107
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w()V

    .line 567108
    :cond_0
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bs:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    move v0, p1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 567109
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bG:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 567110
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bG:Landroid/view/View;

    if-eqz v1, :cond_6

    :goto_4
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 567111
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 567112
    goto :goto_1

    :cond_4
    move v1, v2

    .line 567113
    goto :goto_2

    :cond_5
    move v0, v3

    .line 567114
    goto :goto_3

    :cond_6
    move p1, v3

    .line 567115
    goto :goto_4

    :cond_7
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 567116
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 567117
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567118
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567119
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567120
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->X(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 567121
    const v0, 0x7f0d1095

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    .line 567122
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bd:Z

    if-eqz v0, :cond_1

    .line 567123
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c(LX/2NX;)V

    .line 567124
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567125
    const v0, 0x7f0d10b8

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    .line 567126
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567127
    :goto_0
    return-void

    .line 567128
    :cond_2
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->Y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 567129
    const v0, 0x7f0d1093

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    .line 567130
    const v0, 0x7f0d1093

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    .line 567131
    const v0, 0x7f0d1091

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    .line 567132
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567133
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 567134
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 567135
    :cond_3
    const v0, 0x7f0d1094

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bB:Landroid/widget/ImageView;

    .line 567136
    const v0, 0x7f0d1094

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bC:Landroid/widget/ImageView;

    .line 567137
    const v0, 0x7f0d1092

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bA:Landroid/widget/ImageView;

    goto :goto_1

    .line 567138
    :cond_4
    const/4 v0, 0x4

    goto :goto_2
.end method

.method private f(I)I
    .locals 2

    .prologue
    .line 567139
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->J:Landroid/content/Context;

    int-to-float v1, p1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method

.method private m()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 567140
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "redirected_from_dbl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567141
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 567142
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v1}, LX/10N;->d()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v1}, LX/10N;->e()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v1}, LX/10N;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private n()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 567143
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->au:LX/0Uh;

    const/16 v3, 0x2e

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 567144
    :cond_0
    :goto_0
    return v0

    .line 567145
    :cond_1
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->az:LX/2NO;

    sget-object v3, LX/27f;->FB4A_LOGIN_PREFILL_AND_REDIRECT:LX/27f;

    invoke-virtual {v2, v3}, LX/2NO;->a(LX/27f;)I

    move-result v2

    .line 567146
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v4, "FB4A_LOGIN_PREFILL_AND_REDIRECT_SOURCE_PREFILL"

    invoke-static {v2}, LX/2DR;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/2Cy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567147
    if-eqz v2, :cond_2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 567148
    const v0, 0x7f0d10b3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 567149
    instance-of v1, v0, Lcom/facebook/widget/MaxWidthLinearLayout;

    if-eqz v1, :cond_0

    .line 567150
    check-cast v0, Lcom/facebook/widget/MaxWidthLinearLayout;

    const v1, 0x7fffffff

    .line 567151
    iput v1, v0, Lcom/facebook/widget/MaxWidthLinearLayout;->a:I

    .line 567152
    :cond_0
    const v0, 0x7f0d10b3

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(I)V

    .line 567153
    const v0, 0x7f0d10b4

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(I)V

    .line 567154
    return-void
.end method

.method private p()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 567045
    new-instance v0, LX/2Nc;

    invoke-direct {v0, p0}, LX/2Nc;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    return-object v0
.end method

.method private q()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 567507
    new-instance v0, LX/2Nd;

    invoke-direct {v0, p0}, LX/2Nd;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    return-object v0
.end method

.method public static r(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 6

    .prologue
    .line 567712
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    new-instance v1, LX/2P8;

    invoke-direct {v1, p0}, LX/2P8;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    const/4 p0, 0x1

    .line 567713
    invoke-static {v0}, LX/276;->j(LX/276;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 567714
    :cond_0
    :goto_0
    return-void

    .line 567715
    :cond_1
    iget-object v2, v0, LX/276;->d:LX/2wX;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/276;->d:LX/2wX;

    invoke-virtual {v2}, LX/2wX;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/276;->h:Z

    if-nez v2, :cond_0

    .line 567716
    new-instance v2, LX/G7T;

    invoke-direct {v2}, LX/G7T;-><init>()V

    iput-boolean p0, v2, LX/G7T;->a:Z

    move-object v2, v2

    .line 567717
    new-array v3, p0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "https://www.facebook.com"

    aput-object v5, v3, v4

    if-nez v3, :cond_2

    const/4 v4, 0x0

    new-array v3, v4, [Ljava/lang/String;

    :cond_2
    iput-object v3, v2, LX/G7T;->b:[Ljava/lang/String;

    move-object v2, v2

    .line 567718
    invoke-virtual {v2}, LX/G7T;->a()Lcom/google/android/gms/auth/api/credentials/CredentialRequest;

    move-result-object v2

    .line 567719
    sget-object v3, LX/G7R;->i:LX/G7V;

    iget-object v4, v0, LX/276;->d:LX/2wX;

    invoke-interface {v3, v4, v2}, LX/G7V;->a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/CredentialRequest;)LX/2wg;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2wg;->a(LX/27U;)V

    .line 567720
    iput-boolean p0, v0, LX/276;->h:Z

    .line 567721
    iget-object v2, v0, LX/276;->c:LX/0if;

    sget-object v3, LX/0ig;->be:LX/0ih;

    sget-object v4, LX/27W;->SMARTLOCK_REQUEST:LX/27W;

    invoke-virtual {v4}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static s()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 567707
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "Kindle Fire"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "KF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 567708
    :goto_0
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    .line 567709
    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v5, "Amazon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    if-nez v0, :cond_1

    if-eqz v3, :cond_3

    :cond_1
    :goto_1
    return v2

    :cond_2
    move v0, v1

    .line 567710
    goto :goto_0

    :cond_3
    move v2, v1

    .line 567711
    goto :goto_1
.end method

.method private t()V
    .locals 1

    .prologue
    .line 567703
    sget-object v0, LX/2NX;->BLUE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/2NX;)V

    .line 567704
    sget-object v0, LX/2NX;->BLUE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/2NX;)V

    .line 567705
    sget-object v0, LX/2NX;->BLUE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c(LX/2NX;)V

    .line 567706
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 567695
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bR:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 567696
    :goto_0
    return-void

    .line 567697
    :cond_0
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(LX/2NX;)V

    .line 567698
    sget-object v0, LX/2NX;->WHITE:LX/2NX;

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->b(LX/2NX;)V

    .line 567699
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->v()V

    .line 567700
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y()V

    .line 567701
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->z()V

    .line 567702
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A()V

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 567684
    const v0, 0x7f0a00a4

    invoke-static {p0, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 567685
    const v0, 0x7f0d1098

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567686
    if-eqz v0, :cond_0

    .line 567687
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567688
    :cond_0
    const v0, 0x7f0d1099

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567689
    if-eqz v0, :cond_1

    .line 567690
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567691
    :cond_1
    const v0, 0x7f0d109a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567692
    if-eqz v0, :cond_2

    .line 567693
    const v1, 0x7f0a008b

    invoke-static {p0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567694
    :cond_2
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 567670
    const v0, 0x7f0a00a4

    invoke-static {p0, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 567671
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bH:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 567672
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bH:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567673
    :cond_0
    const v0, 0x7f0d109d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567674
    if-eqz v0, :cond_1

    .line 567675
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567676
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bI:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 567677
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bI:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567678
    :cond_2
    const v0, 0x7f0d109f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 567679
    if-eqz v0, :cond_3

    .line 567680
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567681
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bJ:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 567682
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bJ:Landroid/widget/TextView;

    const v1, 0x7f0a008b

    invoke-static {p0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567683
    :cond_4
    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    const/16 v2, 0x17

    .line 567659
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 567660
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_2

    .line 567661
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    const v1, 0x7f0e023a

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setTextAppearance(I)V

    .line 567662
    :goto_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    const v1, 0x7f020847

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setBackgroundResource(I)V

    .line 567663
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 567664
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_3

    .line 567665
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const v1, 0x7f0e023c

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextAppearance(I)V

    .line 567666
    :goto_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const v1, 0x7f020847

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 567667
    :cond_1
    return-void

    .line 567668
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    const v1, 0x7f0e023a

    invoke-virtual {v0, p0, v1}, Landroid/widget/AutoCompleteTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 567669
    :cond_3
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const v1, 0x7f0e023c

    invoke-virtual {v0, p0, v1}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private y()V
    .locals 2

    .prologue
    .line 567656
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 567657
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    const v1, 0x7f0a00a8

    invoke-static {p0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 567658
    :cond_0
    return-void
.end method

.method private z()V
    .locals 2

    .prologue
    .line 567651
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 567652
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 567653
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0e0238

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(I)V

    .line 567654
    :cond_0
    :goto_0
    return-void

    .line 567655
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bK:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0e0238

    invoke-virtual {v0, p0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 567650
    const-string v0, "login_screen"

    return-object v0
.end method

.method public final a(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 567644
    new-instance v0, Lcom/facebook/auth/login/CheckApprovedMachineParams;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/auth/login/CheckApprovedMachineParams;-><init>(JLjava/lang/String;)V

    .line 567645
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 567646
    const-string v1, "checkApprovedMachineParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 567647
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->w:LX/0aG;

    const-string v1, "check_approved_machine"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x7618b981

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 567648
    new-instance v1, LX/39z;

    invoke-direct {v1, p0}, LX/39z;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 567649
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 567643
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/16 v3, 0x258

    .line 567509
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 567510
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 567511
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 567512
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0421

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 567513
    :cond_0
    invoke-static {p0, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 567514
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 567515
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->s:LX/0pW;

    .line 567516
    iget-object v4, v0, LX/0pW;->a:LX/0Yi;

    .line 567517
    iget-object v5, v4, LX/0Yi;->k:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, LX/0Yi;->c(LX/0Yi;J)V

    .line 567518
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->A:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$8;

    invoke-direct {v1, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$8;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    const v2, -0x39ff896f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 567519
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->U:LX/0yc;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/0yc;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 567520
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    .line 567521
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G:LX/2CQ;

    const/4 v2, 0x0

    .line 567522
    iget-boolean v1, v0, LX/2CQ;->f:Z

    if-eqz v1, :cond_12

    iget-object v1, v0, LX/2CQ;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    sget-object v4, LX/03R;->YES:LX/03R;

    invoke-virtual {v1, v4}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    move v1, v2

    .line 567523
    :goto_0
    move v0, v1

    .line 567524
    if-eqz v0, :cond_1

    .line 567525
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->M()V

    .line 567526
    :goto_1
    return-void

    .line 567527
    :cond_1
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ai()V

    .line 567528
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ad:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ag;

    .line 567529
    iget-object v1, v0, LX/2Ag;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1nR;->j:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 567530
    if-eqz v1, :cond_16

    .line 567531
    :goto_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Af;->a:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 567532
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->am:LX/2J4;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L:LX/10O;

    invoke-virtual {v0, v1, v2}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->an:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 567533
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->B()LX/28M;

    move-result-object v0

    .line 567534
    if-nez v0, :cond_2

    .line 567535
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->C()LX/28M;

    move-result-object v0

    .line 567536
    :cond_2
    if-nez v0, :cond_3

    .line 567537
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->D()LX/28M;

    move-result-object v0

    .line 567538
    :cond_3
    if-nez v0, :cond_4

    .line 567539
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->E()LX/28M;

    move-result-object v0

    .line 567540
    :cond_4
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    .line 567541
    if-eqz v0, :cond_e

    .line 567542
    iget-object v1, v0, LX/28M;->b:Ljava/lang/String;

    move-object v1, v1

    .line 567543
    if-eqz v1, :cond_e

    .line 567544
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    .line 567545
    iget-object v2, v0, LX/28M;->b:Ljava/lang/String;

    move-object v2, v2

    .line 567546
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 567547
    :cond_5
    :goto_3
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    .line 567548
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    .line 567549
    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-virtual {p0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    .line 567550
    if-nez v2, :cond_17

    iget-object v2, v1, LX/2DR;->e:Landroid/telephony/TelephonyManager;

    if-eqz v2, :cond_17

    iget-object v2, v1, LX/2DR;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_17

    sget-object v2, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    iget-object v4, v1, LX/2DR;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 567551
    iget-object v2, v1, LX/2DR;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    .line 567552
    :goto_4
    move-object v1, v2

    .line 567553
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 567554
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 567555
    :cond_6
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {v1}, LX/2DR;->j()Ljava/util/Set;

    move-result-object v1

    .line 567556
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aQ:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 567557
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ao:LX/2NM;

    .line 567558
    iget-object v4, v1, LX/2NM;->l:LX/10N;

    invoke-virtual {v4}, LX/10N;->o()Z

    move-result v4

    if-nez v4, :cond_18

    .line 567559
    :cond_7
    :goto_5
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    .line 567560
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->V()V

    .line 567561
    if-nez v0, :cond_10

    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->m()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 567562
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 567563
    const-string v1, "accounts"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    invoke-virtual {v2}, LX/10M;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567564
    const-string v1, "pin"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    .line 567565
    const/4 v4, 0x0

    .line 567566
    iget-object v5, v2, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/26p;->k:LX/0Tn;

    invoke-interface {v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v4

    :cond_8
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 567567
    if-eqz v4, :cond_8

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 567568
    const/4 v7, 0x0

    .line 567569
    :try_start_0
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    move-object v3, v5

    check-cast v3, Ljava/lang/String;

    move-object v4, v3
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567570
    :goto_7
    if-eqz v4, :cond_8

    .line 567571
    :try_start_1
    iget-object v5, v2, LX/10M;->c:LX/0lC;

    const-class v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v5, v4, v7}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 567572
    iget-object v4, v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    if-eqz v4, :cond_1d

    .line 567573
    add-int/lit8 v4, v6, 0x1

    :goto_8
    move v6, v4

    .line 567574
    goto :goto_6

    .line 567575
    :catch_0
    move-exception v5

    .line 567576
    iget-object v9, v2, LX/10M;->d:LX/03V;

    const-string v10, "FB4ADBLStoreManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "getNumberOfAccountsWithPin: Error encountered in casting one tap login credential value from Object of type "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " to String"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v10, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v7

    goto :goto_7

    .line 567577
    :catch_1
    move-exception v4

    .line 567578
    iget-object v5, v2, LX/10M;->d:LX/03V;

    const-string v7, "FB4ADBLStoreManager"

    const-string v9, "Error encountered in reading the DBLcredentials from FbSharedPreferences"

    invoke-virtual {v5, v7, v9, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 567579
    :cond_9
    move v2, v6

    .line 567580
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567581
    const-string v1, "placeholder"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    .line 567582
    const/4 v4, 0x0

    .line 567583
    iget-object v5, v2, LX/10M;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/26p;->k:LX/0Tn;

    invoke-interface {v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v4

    :cond_a
    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 567584
    if-eqz v4, :cond_a

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 567585
    const/4 v7, 0x0

    .line 567586
    :try_start_2
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    move-object v3, v5

    check-cast v3, Ljava/lang/String;

    move-object v4, v3
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_2

    .line 567587
    :goto_a
    if-eqz v4, :cond_a

    .line 567588
    :try_start_3
    iget-object v5, v2, LX/10M;->c:LX/0lC;

    const-class v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v5, v4, v7}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 567589
    const-string v5, ""

    iget-object v4, v4, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v4

    if-eqz v4, :cond_1e

    .line 567590
    add-int/lit8 v4, v6, 0x1

    :goto_b
    move v6, v4

    .line 567591
    goto :goto_9

    .line 567592
    :catch_2
    move-exception v5

    .line 567593
    iget-object v9, v2, LX/10M;->d:LX/03V;

    const-string v10, "FB4ADBLStoreManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "getNumberOfPlaceholderAccounts: Error encountered in casting one tap login credential value from Object of type "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " to String"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v10, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v7

    goto :goto_a

    .line 567594
    :catch_3
    move-exception v4

    .line 567595
    iget-object v5, v2, LX/10M;->d:LX/03V;

    const-string v7, "FB4ADBLStoreManager"

    const-string v9, "Error encountered in reading the DBLcredentials from FbSharedPreferences"

    invoke-virtual {v5, v7, v9, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 567596
    :cond_b
    move v2, v6

    .line 567597
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567598
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v1}, LX/10N;->b()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v1}, LX/10N;->j()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 567599
    :cond_c
    const-string v1, "password_accounts"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    invoke-virtual {v2}, LX/10M;->i()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567600
    :cond_d
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L:LX/10O;

    const-string v2, "dbl_user_chooser_displayed"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 567601
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567602
    new-instance v1, LX/27o;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/27o;-><init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Landroid/content/Intent;)V

    .line 567603
    invoke-virtual {v1}, LX/27o;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 567604
    invoke-virtual {v1}, LX/27o;->b()Z

    move-result v1

    if-nez v1, :cond_f

    .line 567605
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 567606
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->finish()V

    .line 567607
    :goto_c
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->s:LX/0pW;

    invoke-virtual {v0}, LX/0pW;->b()V

    goto/16 :goto_1

    .line 567608
    :cond_e
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getApplicationContext()Landroid/content/Context;

    const/4 v2, 0x0

    .line 567609
    :try_start_4
    iget-object v4, v1, LX/2DR;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1CA;->p:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v2

    .line 567610
    :goto_d
    move-object v1, v2

    .line 567611
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 567612
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aP:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 567613
    :cond_f
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x3

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_c

    .line 567614
    :cond_10
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    invoke-virtual {v1}, LX/276;->e()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 567615
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bY:Ljava/lang/Runnable;

    invoke-virtual {v1, p0, v2, v3}, LX/276;->a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Runnable;I)V

    .line 567616
    :goto_e
    invoke-direct {p0, p1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a(Landroid/os/Bundle;LX/28M;)V

    goto :goto_c

    .line 567617
    :cond_11
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bX:Ljava/lang/Runnable;

    invoke-virtual {v1, p0, v2, v3}, LX/276;->a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Runnable;I)V

    goto :goto_e

    .line 567618
    :cond_12
    iget-object v1, v0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 567619
    iget-object v4, v0, LX/2CQ;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 567620
    iget-object v4, v1, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v1, v4

    .line 567621
    sget-object v4, LX/2A1;->STATUS_LOGGED_IN:LX/2A1;

    if-ne v1, v4, :cond_15

    .line 567622
    iget-object v1, v0, LX/2CQ;->e:LX/10M;

    invoke-virtual {v1}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_13

    iget-object v1, v0, LX/2CQ;->e:LX/10M;

    invoke-virtual {v1}, LX/10M;->l()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_14

    :cond_13
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_14
    move v1, v2

    goto/16 :goto_0

    :cond_15
    move v1, v2

    .line 567623
    goto/16 :goto_0

    .line 567624
    :cond_16
    iget-object v1, v0, LX/2Ag;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/growth/campaign/AdvertisingIdLogger$1;

    invoke-direct {v2, v0}, Lcom/facebook/growth/campaign/AdvertisingIdLogger$1;-><init>(LX/2Ag;)V

    const v4, 0x1533f2ae

    invoke-static {v1, v2, v4}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_2

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 567625
    :cond_18
    const-wide/16 v12, 0x0

    .line 567626
    iget-object v10, v1, LX/2NM;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/26p;->t:LX/0Tn;

    invoke-interface {v10, v11, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    .line 567627
    cmp-long v12, v10, v12

    if-eqz v12, :cond_19

    iget-object v12, v1, LX/2NM;->m:LX/0SG;

    invoke-interface {v12}, LX/0SG;->a()J

    move-result-wide v12

    sub-long v10, v12, v10

    const-wide/32 v12, 0x5265c00

    cmp-long v10, v10, v12

    if-lez v10, :cond_1c

    :cond_19
    const/4 v10, 0x1

    :goto_f
    move v4, v10

    .line 567628
    if-eqz v4, :cond_7

    .line 567629
    iget-object v4, v1, LX/2NM;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/26p;->t:LX/0Tn;

    iget-object v6, v1, LX/2NM;->m:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 567630
    iget-object v4, v1, LX/2NM;->g:LX/2Du;

    invoke-virtual {v4}, LX/2Du;->a()Ljava/util/List;

    move-result-object v4

    .line 567631
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 567632
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1a
    :goto_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/accounts/Account;

    .line 567633
    iget-object v7, v1, LX/2NM;->g:LX/2Du;

    iget-object v8, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/2Du;->b(Ljava/lang/String;)LX/4gy;

    move-result-object v7

    .line 567634
    if-eqz v7, :cond_1a

    .line 567635
    iget-object v8, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v8}, LX/2NM;->d(LX/2NM;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1a

    .line 567636
    iget-object v8, v1, LX/2NM;->g:LX/2Du;

    invoke-virtual {v8, v4, v7}, LX/2Du;->b(Landroid/accounts/Account;LX/4gy;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 567637
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 567638
    new-instance v9, LX/Hwn;

    invoke-direct {v9, v1, v4, v7}, LX/Hwn;-><init>(LX/2NM;Landroid/accounts/Account;LX/4gy;)V

    iget-object v4, v1, LX/2NM;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8, v9, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_10

    .line 567639
    :cond_1b
    invoke-static {v5}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 567640
    new-instance v5, LX/Hwo;

    invoke-direct {v5, v1}, LX/Hwo;-><init>(LX/2NM;)V

    move-object v5, v5

    .line 567641
    iget-object v6, v1, LX/2NM;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_5

    :cond_1c
    const/4 v10, 0x0

    goto :goto_f

    :cond_1d
    move v4, v6

    goto/16 :goto_8

    :cond_1e
    move v4, v6

    goto/16 :goto_b

    .line 567642
    :catch_4
    goto/16 :goto_d
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 567508
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v5, 0x0

    .line 567303
    if-ne p1, v3, :cond_3

    .line 567304
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {v0}, LX/2DR;->d()Landroid/content/Intent;

    move-result-object v2

    .line 567305
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 567306
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 567307
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->z:LX/0iA;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    const-class v4, LX/13G;

    invoke-virtual {v0, v3, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 567308
    if-eqz v0, :cond_9

    .line 567309
    invoke-interface {v0, p2, p3}, LX/13G;->a(ILandroid/content/Intent;)LX/0am;

    move-result-object v0

    .line 567310
    :goto_0
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 567311
    const/high16 v1, 0x10000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 567312
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 567313
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 567314
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "redirected_from_adduser"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 567315
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H:LX/10M;

    invoke-virtual {v0}, LX/10M;->m()V

    .line 567316
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x23000e

    invoke-interface {v0, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 567317
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->finish()V

    .line 567318
    :cond_1
    :goto_2
    return-void

    .line 567319
    :cond_2
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 567320
    :cond_3
    if-ne p1, v6, :cond_4

    .line 567321
    if-ne p2, v1, :cond_1

    .line 567322
    const-string v0, "account_user_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 567323
    const-string v1, "account_password"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 567324
    new-array v2, v6, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aput-object v1, v2, v3

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 567325
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 567326
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 567327
    iput-boolean v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aL:Z

    goto :goto_2

    .line 567328
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    .line 567329
    if-nez p2, :cond_1

    .line 567330
    invoke-virtual {p0, v5}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->setResult(I)V

    .line 567331
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->finish()V

    goto :goto_2

    .line 567332
    :cond_5
    const/4 v0, 0x4

    if-ne p1, v0, :cond_7

    .line 567333
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567334
    iput-boolean v5, v0, LX/276;->i:Z

    .line 567335
    if-ne p2, v1, :cond_6

    .line 567336
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE_SUCCESS:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    .line 567337
    const-string v0, "com.google.android.gms.credentials.Credential"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/api/credentials/Credential;

    .line 567338
    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Lcom/google/android/gms/auth/api/credentials/Credential;)V

    goto :goto_2

    .line 567339
    :cond_6
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE_FAIL:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 567340
    :cond_7
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 567341
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567342
    iput-boolean v5, v0, LX/276;->h:Z

    .line 567343
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567344
    iput-boolean v5, v0, LX/276;->i:Z

    .line 567345
    if-ne p2, v1, :cond_8

    .line 567346
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_HINT_SUCCESS:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    .line 567347
    const-string v0, "com.google.android.gms.credentials.Credential"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/api/credentials/Credential;

    .line 567348
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    iget-object v2, v0, Lcom/google/android/gms/auth/api/credentials/Credential;->b:Ljava/lang/String;

    move-object v0, v2

    .line 567349
    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 567350
    :cond_8
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    sget-object v1, LX/27W;->SMARTLOCK_HINT_FAIL:LX/27W;

    invoke-virtual {v1}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/276;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 567504
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onAttachedToWindow()V

    .line 567505
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 567506
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 567490
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 567491
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 567492
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aA:LX/2NP;

    sget-object v1, LX/2NP;->LOGGING_IN:LX/2NP;

    if-eq v0, v1, :cond_3

    .line 567493
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bg:Z

    if-eqz v0, :cond_1

    .line 567494
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 567495
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 567496
    iput-boolean v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bg:Z

    .line 567497
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aJ:Z

    .line 567498
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    .line 567499
    :goto_0
    return-void

    .line 567500
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "redirected_from_dbl"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v0}, LX/10N;->e()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v0}, LX/10N;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->F:LX/10N;

    invoke-virtual {v0}, LX/10N;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 567501
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567502
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 567503
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->finish()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    const v1, 0x4378bc98

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 567428
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 567429
    const v2, 0x7f0d10ac

    if-ne v0, v2, :cond_1

    .line 567430
    sget-object v0, LX/2NV;->PASSWORD_ENTRY:LX/2NV;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NV;)V

    .line 567431
    :cond_0
    :goto_0
    const v0, 0x7f36334e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 567432
    :cond_1
    const v2, 0x7f0d10af

    if-ne v0, v2, :cond_2

    .line 567433
    sget-object v0, LX/2NV;->LOGIN_APPROVALS_CODE_ENTRY:LX/2NV;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NV;)V

    goto :goto_0

    .line 567434
    :cond_2
    const v2, 0x7f0d10b4

    if-eq v0, v2, :cond_3

    const v2, 0x7f0d10b5

    if-ne v0, v2, :cond_5

    .line 567435
    :cond_3
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 567436
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 567437
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->al()LX/03R;

    move-result-object v2

    .line 567438
    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->y:LX/2Cy;

    const-string v4, "PREFILL_REG_WITH_LOGIN_INFO"

    invoke-virtual {v3, v4, v2}, LX/2Cy;->a(Ljava/lang/String;LX/03R;)V

    .line 567439
    invoke-virtual {v2, v5}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 567440
    const-string v2, "login_field_username"

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bj:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567441
    const-string v2, "login_field_password"

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567442
    :cond_4
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 567443
    :cond_5
    const v2, 0x7f0d0c1f

    if-ne v0, v2, :cond_6

    .line 567444
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->S()V

    goto :goto_0

    .line 567445
    :cond_6
    const v2, 0x7f0d10a4

    if-eq v0, v2, :cond_0

    .line 567446
    const v2, 0x7f0d10a5

    if-ne v0, v2, :cond_7

    .line 567447
    sget-object v0, LX/2NP;->PASSWORD_AUTH_UI:LX/2NP;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NP;)V

    goto :goto_0

    .line 567448
    :cond_7
    const v2, 0x7f0d1097

    if-ne v0, v2, :cond_8

    .line 567449
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 567450
    const-string v2, "calling_intent"

    sget-object v3, LX/2Ab;->LOGIN:LX/2Ab;

    invoke-virtual {v3}, LX/2Ab;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567451
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 567452
    :cond_8
    const v2, 0x7f0d109c

    if-ne v0, v2, :cond_9

    .line 567453
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    invoke-virtual {v0}, LX/27j;->c()V

    .line 567454
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 567455
    :cond_9
    const v2, 0x7f0d109e

    if-ne v0, v2, :cond_a

    .line 567456
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27j;

    invoke-virtual {v0}, LX/27j;->c()V

    .line 567457
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bQ:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->d$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 567458
    :cond_a
    const v2, 0x7f0d10a0

    if-ne v0, v2, :cond_b

    .line 567459
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ad()V

    goto/16 :goto_0

    .line 567460
    :cond_b
    const v2, 0x7f0d10b1

    if-ne v0, v2, :cond_d

    .line 567461
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bc:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 567462
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bc:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 567463
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 567464
    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->x:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 567465
    :cond_c
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->T()V

    goto/16 :goto_0

    .line 567466
    :cond_d
    const v2, 0x7f0d10ab

    if-ne v0, v2, :cond_f

    .line 567467
    iget-boolean v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aY:Z

    if-nez v0, :cond_e

    .line 567468
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ag(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 567469
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567470
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->SHOW_PASSWORD_CHECKED:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 567471
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567472
    move-object v3, v3

    .line 567473
    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567474
    goto/16 :goto_0

    .line 567475
    :cond_e
    iput-boolean v5, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aY:Z

    .line 567476
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    const v2, 0x7f0800ed

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 567477
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bO:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->af:LX/23P;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 567478
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    const/16 v2, 0x81

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 567479
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 567480
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 567481
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aa:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ne;

    .line 567482
    iget-object v2, v0, LX/2Ne;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/GB4;->SHOW_PASSWORD_UNCHECKED:LX/GB4;

    invoke-virtual {v4}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "account_recovery"

    .line 567483
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 567484
    move-object v3, v3

    .line 567485
    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 567486
    goto/16 :goto_0

    .line 567487
    :cond_f
    const v2, 0x7f0d10b0

    if-ne v0, v2, :cond_0

    .line 567488
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bv:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 567489
    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aW:J

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bb:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->c(JLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 567421
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 567422
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 567423
    :cond_0
    :goto_0
    return-void

    .line 567424
    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 567425
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 567426
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bD:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567427
    :cond_2
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->G$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    goto :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 567418
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aF:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 567419
    const/4 v0, 0x1

    .line 567420
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x28d1193e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567412
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    invoke-virtual {v1}, LX/2DR;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    .line 567413
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x190

    const-string v3, "User canceled"

    invoke-static {v1, v2, v3}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Intent;ILjava/lang/String;)V

    .line 567414
    :cond_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    if-eqz v1, :cond_1

    .line 567415
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aC:LX/2NU;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/service/AppSession;->b(LX/278;)V

    .line 567416
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 567417
    const/16 v1, 0x23

    const v2, -0x5ba4ec8d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3c255b9a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567407
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 567408
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    .line 567409
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 567410
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 567411
    :cond_0
    const/16 v1, 0x23

    const v2, 0x2e80b4bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 567404
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aF:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 567405
    const/4 v0, 0x1

    .line 567406
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 567398
    const-string v0, "nux_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    .line 567399
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    const-string v1, "sl_restore"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 567400
    iput-boolean v1, v0, LX/276;->i:Z

    .line 567401
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    const-string v1, "sl_request"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 567402
    iput-boolean v1, v0, LX/276;->h:Z

    .line 567403
    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x12bfad4f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567369
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 567370
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_0

    .line 567371
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->U:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 567372
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 567373
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 567374
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aT:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aU:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    const v3, -0x4c249d2e

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 567375
    :cond_1
    iput-boolean v7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aE:Z

    .line 567376
    iput v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aH:I

    .line 567377
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bx:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 567378
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bx:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 567379
    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v1, :cond_2

    .line 567380
    iput-boolean v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 567381
    :cond_2
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 567382
    :cond_3
    sget-object v1, LX/2Nb;->b:[I

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    .line 567383
    iget-object v3, v2, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v2, v3

    .line 567384
    invoke-virtual {v2}, LX/2A1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 567385
    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aL:Z

    if-eqz v1, :cond_4

    .line 567386
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bt:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbButton;->performClick()Z

    .line 567387
    iput-boolean v6, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aL:Z

    .line 567388
    :cond_4
    :goto_1
    const v1, 0x297ec1c2

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 567389
    :cond_5
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bL:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 567390
    :pswitch_0
    iput-boolean v7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aK:Z

    .line 567391
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aG:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v1, v2, v4

    if-gez v1, :cond_6

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aI:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    if-nez v1, :cond_6

    .line 567392
    iput-boolean v7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    .line 567393
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    const v3, -0x2b31f1b8

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    .line 567394
    :cond_6
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->P()V

    goto :goto_1

    .line 567395
    :pswitch_1
    iget-boolean v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    if-nez v1, :cond_4

    .line 567396
    iput-boolean v7, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aS:Z

    .line 567397
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aO:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bW:Ljava/lang/Runnable;

    const v3, 0x5634ff68

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 567359
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 567360
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 567361
    const-string v0, "nux_id"

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567362
    :cond_0
    const-string v0, "sl_restore"

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567363
    iget-boolean v2, v1, LX/276;->i:Z

    move v1, v2

    .line 567364
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567365
    const-string v0, "sl_request"

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 567366
    iget-boolean v2, v1, LX/276;->h:Z

    move v1, v2

    .line 567367
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567368
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 567358
    const/4 v0, 0x1

    return v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1bebf58

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567355
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 567356
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->H()V

    .line 567357
    const/16 v1, 0x23

    const v2, -0x51632c40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x422da751

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 567351
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 567352
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bm:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bS:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 567353
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 567354
    const/16 v1, 0x23

    const v2, -0x72eaadd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
