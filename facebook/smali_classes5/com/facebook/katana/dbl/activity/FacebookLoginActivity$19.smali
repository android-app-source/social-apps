.class public final Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 568710
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iput-object p2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->a:Ljava/lang/String;

    iput-wide p3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 568711
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v1

    .line 568712
    const-string v0, "prev_uid_hashed"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    .line 568713
    const/4 v3, 0x0

    .line 568714
    :try_start_0
    iget-object v4, v2, LX/2DR;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1CA;->q:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 568715
    :goto_0
    move-object v2, v3

    .line 568716
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568717
    const-string v2, "curr_uid_hashed"

    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568718
    const-string v0, "prev_user_login_time"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 568719
    invoke-virtual {v2}, LX/2DR;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568720
    const-string v0, "prev_user_logout_time"

    iget-object v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->t:LX/2DR;

    iget-object v3, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 568721
    const-wide/16 v4, 0x0

    .line 568722
    :try_start_1
    iget-object v6, v2, LX/2DR;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/1CA;->r:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    .line 568723
    :goto_2
    move-wide v2, v4

    .line 568724
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568725
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hw;

    invoke-virtual {v0}, LX/0hw;->a()V

    .line 568726
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->K:LX/0hx;

    .line 568727
    iget-object v2, v0, LX/0hx;->c:LX/0gh;

    invoke-virtual {v2, v1}, LX/0gh;->a(Ljava/util/Map;)V

    .line 568728
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->c:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1CA;->o:LX/0Tn;

    iget-wide v2, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->b:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 568729
    return-void

    .line 568730
    :cond_0
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$19;->a:Ljava/lang/String;

    invoke-static {v0}, LX/03l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 568731
    :catch_0
    goto :goto_0

    .line 568732
    :catch_1
    goto :goto_2
.end method
