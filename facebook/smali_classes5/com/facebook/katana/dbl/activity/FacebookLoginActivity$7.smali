.class public final Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 568430
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 568431
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    .line 568432
    iget-boolean v1, v0, LX/276;->i:Z

    move v0, v1

    .line 568433
    if-nez v0, :cond_0

    .line 568434
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->av:LX/276;

    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$7;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 568435
    invoke-static {v0}, LX/276;->j(LX/276;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 568436
    :cond_0
    :goto_0
    return-void

    .line 568437
    :cond_1
    iget-object v2, v0, LX/276;->d:LX/2wX;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/276;->d:LX/2wX;

    invoke-virtual {v2}, LX/2wX;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/276;->h:Z

    if-nez v2, :cond_0

    .line 568438
    new-instance v2, LX/G7W;

    invoke-direct {v2}, LX/G7W;-><init>()V

    new-instance v3, LX/G7S;

    invoke-direct {v3}, LX/G7S;-><init>()V

    iput-boolean v6, v3, LX/G7S;->b:Z

    move-object v3, v3

    .line 568439
    invoke-virtual {v3}, LX/G7S;->a()Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;

    move-result-object v3

    invoke-static {v3}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;

    iput-object v4, v2, LX/G7W;->d:Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;

    move-object v2, v2

    .line 568440
    iput-boolean v6, v2, LX/G7W;->a:Z

    move-object v2, v2

    .line 568441
    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "https://www.facebook.com"

    aput-object v4, v3, v5

    if-nez v3, :cond_2

    const/4 v4, 0x0

    new-array v3, v4, [Ljava/lang/String;

    :cond_2
    iput-object v3, v2, LX/G7W;->c:[Ljava/lang/String;

    move-object v2, v2

    .line 568442
    invoke-virtual {v2}, LX/G7W;->a()Lcom/google/android/gms/auth/api/credentials/HintRequest;

    move-result-object v2

    .line 568443
    iget-object v3, v0, LX/276;->c:LX/0if;

    sget-object v4, LX/0ig;->be:LX/0ih;

    sget-object v5, LX/27W;->SMARTLOCK_HINT_REQUEST:LX/27W;

    invoke-virtual {v5}, LX/27W;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 568444
    sget-object v3, LX/G7R;->i:LX/G7V;

    iget-object v4, v0, LX/276;->d:LX/2wX;

    invoke-interface {v3, v4, v2}, LX/G7V;->a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/HintRequest;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 568445
    iput-boolean v6, v0, LX/276;->h:Z

    .line 568446
    iput-boolean v6, v0, LX/276;->i:Z

    .line 568447
    :try_start_0
    invoke-virtual {v2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v3

    const/4 v4, 0x5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v1

    invoke-virtual/range {v2 .. v8}, Landroid/support/v4/app/FragmentActivity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method
