.class public final Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd1d3843
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578713
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578726
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 578724
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 578725
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 578714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578715
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 578716
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 578717
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578718
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->f:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578719
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->g:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578720
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->h:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 578721
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 578722
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578723
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 578705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578706
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578707
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 578708
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 578709
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;

    .line 578710
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->i:Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 578711
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578712
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 578699
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 578700
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->e:Z

    .line 578701
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->f:Z

    .line 578702
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->g:Z

    .line 578703
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->h:Z

    .line 578704
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 578727
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578728
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 578696
    new-instance v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;-><init>()V

    .line 578697
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 578698
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 578695
    const v0, 0x2a2e9abb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 578694
    const v0, -0x5d378b0e

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 578686
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578687
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->f:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 578692
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578693
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->g:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 578690
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578691
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->h:Z

    return v0
.end method

.method public final m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 578688
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->i:Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->i:Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 578689
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->i:Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    return-object v0
.end method
