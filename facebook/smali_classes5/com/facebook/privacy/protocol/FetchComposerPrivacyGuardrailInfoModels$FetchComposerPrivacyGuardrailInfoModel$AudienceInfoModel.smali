.class public final Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3394148a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634552
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634553
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634554
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634555
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 634556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634557
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x6ae018e4

    invoke-static {v1, v0, v2}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 634558
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 634559
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 634560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634561
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 634562
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634563
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 634564
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6ae018e4

    invoke-static {v2, v0, v3}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634565
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 634566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;

    .line 634567
    iput v3, v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->e:I

    .line 634568
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634569
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 634570
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 634571
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getComposerPrivacyGuardrailInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 634572
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634573
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 634574
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 634575
    const/4 v0, 0x0

    const v1, 0x6ae018e4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;->e:I

    .line 634576
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 634577
    new-instance v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel$AudienceInfoModel;-><init>()V

    .line 634578
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 634579
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 634580
    const v0, 0x79a07672

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 634581
    const v0, -0x5d378b0e

    return v0
.end method
