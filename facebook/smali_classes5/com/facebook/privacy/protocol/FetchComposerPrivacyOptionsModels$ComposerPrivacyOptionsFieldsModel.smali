.class public final Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x36d48e6f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578255
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578254
    const-class v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 578252
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 578253
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 578241
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578242
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 578243
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 578244
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 578245
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 578246
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 578247
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->g:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 578248
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 578249
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 578250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578251
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 578233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 578234
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578235
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578236
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 578237
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;

    .line 578238
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578239
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 578240
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 578228
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 578229
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->e:Z

    .line 578230
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->f:Z

    .line 578231
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->g:Z

    .line 578232
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 578226
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578227
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 578223
    new-instance v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;-><init>()V

    .line 578224
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 578225
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 578213
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578214
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 578221
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 578222
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->g:Z

    return v0
.end method

.method public final d()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 578219
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578220
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 578218
    const v0, -0x66b7cf54

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 578216
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 578217
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ComposerPrivacyOptionsFieldsModel;->i:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 578215
    const v0, -0x595cbcd

    return v0
.end method
