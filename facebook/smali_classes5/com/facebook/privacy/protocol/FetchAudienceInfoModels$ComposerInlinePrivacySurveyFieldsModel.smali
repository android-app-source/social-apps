.class public final Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3fcd6e87
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634297
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634282
    const-class v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 634298
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 634299
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 634318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634319
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 634320
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 634321
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 634322
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 634323
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 634324
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 634325
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 634326
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 634327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634328
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 634300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 634301
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634302
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634303
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 634304
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 634305
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634306
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 634307
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634308
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 634309
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 634310
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634311
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 634312
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634313
    invoke-virtual {p0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 634314
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    .line 634315
    iput-object v0, v1, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634316
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 634317
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 634292
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 634293
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->e:Z

    .line 634294
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 634295
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 634296
    iget-boolean v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 634289
    new-instance v0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;-><init>()V

    .line 634290
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 634291
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 634288
    const v0, 0x6ca0a08a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 634287
    const v0, 0x62dc6282

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634285
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634286
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634283
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634284
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 634280
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634281
    iget-object v0, p0, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->h:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method
