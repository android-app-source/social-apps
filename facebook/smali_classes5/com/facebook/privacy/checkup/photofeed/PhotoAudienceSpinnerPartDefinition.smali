.class public Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/J4O;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1U6;",
        "LX/J4d;",
        "TE;",
        "Lcom/facebook/privacy/spinner/AudienceSpinner;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/J4p;

.field private final c:LX/8SQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602685
    new-instance v0, LX/3a4;

    invoke-direct {v0}, LX/3a4;-><init>()V

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/J4p;LX/8SQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602729
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602730
    iput-object p1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->b:LX/J4p;

    .line 602731
    iput-object p2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->c:LX/8SQ;

    .line 602732
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;
    .locals 5

    .prologue
    .line 602718
    const-class v1, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    monitor-enter v1

    .line 602719
    :try_start_0
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602720
    sput-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602721
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602722
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602723
    new-instance p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    invoke-static {v0}, LX/J4p;->a(LX/0QB;)LX/J4p;

    move-result-object v3

    check-cast v3, LX/J4p;

    const-class v4, LX/8SQ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8SQ;

    invoke-direct {p0, v3, v4}, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;-><init>(LX/J4p;LX/8SQ;)V

    .line 602724
    move-object v0, p0

    .line 602725
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602726
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602727
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602728
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602717
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 602699
    check-cast p2, LX/1U6;

    check-cast p3, LX/1Ps;

    const/4 v4, 0x0

    .line 602700
    invoke-interface {p2}, LX/1U6;->d()Ljava/lang/String;

    move-result-object v1

    move-object v0, p3

    .line 602701
    check-cast v0, LX/J4O;

    invoke-interface {v0}, LX/J4O;->mN_()LX/1oT;

    move-result-object v0

    .line 602702
    if-nez v0, :cond_1

    .line 602703
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->b:LX/J4p;

    .line 602704
    iget-object v2, v0, LX/J4p;->e:LX/3R3;

    invoke-interface {p2}, LX/1U6;->l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/3R3;->a(LX/0Px;Z)LX/8QJ;

    move-result-object v2

    move-object v0, v2

    .line 602705
    invoke-static {v0}, LX/8SR;->a(LX/8QJ;)LX/8SR;

    move-result-object v2

    move-object v0, p3

    .line 602706
    check-cast v0, LX/J4O;

    invoke-interface {p2}, LX/1U6;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, LX/J4O;->g_(Ljava/lang/String;)LX/1oT;

    move-result-object v0

    .line 602707
    if-eqz v0, :cond_0

    .line 602708
    invoke-virtual {v2, v0}, LX/8SR;->b(LX/1oT;)V

    .line 602709
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->c:LX/8SQ;

    sget-object v3, LX/8SM;->AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;

    invoke-virtual {v0, v2, v3, v1}, LX/8SQ;->a(LX/8SR;LX/8SM;Ljava/lang/String;)LX/8SP;

    move-result-object v1

    .line 602710
    new-instance v2, LX/J4b;

    invoke-direct {v2, p0, p3}, LX/J4b;-><init>(Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;LX/1Ps;)V

    .line 602711
    new-instance v3, LX/J4c;

    invoke-direct {v3, p0, p3}, LX/J4c;-><init>(Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;LX/1Ps;)V

    .line 602712
    new-instance v0, LX/J4d;

    invoke-direct {v0, v1, v2, v3}, LX/J4d;-><init>(LX/8SP;LX/8SN;LX/8SO;)V

    .line 602713
    :goto_0
    return-object v0

    .line 602714
    :cond_1
    new-instance v2, LX/8SR;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/8SR;-><init>(LX/0Px;LX/1oT;)V

    .line 602715
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->c:LX/8SQ;

    sget-object v3, LX/8SM;->AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;

    invoke-virtual {v0, v2, v3, v1}, LX/8SQ;->a(LX/8SR;LX/8SM;Ljava/lang/String;)LX/8SP;

    move-result-object v1

    .line 602716
    new-instance v0, LX/J4d;

    invoke-direct {v0, v1, v4, v4}, LX/J4d;-><init>(LX/8SP;LX/8SN;LX/8SO;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x751e4848

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602693
    check-cast p2, LX/J4d;

    check-cast p4, Lcom/facebook/privacy/spinner/AudienceSpinner;

    const/4 v1, 0x1

    .line 602694
    iget-object v2, p2, LX/J4d;->a:LX/8SP;

    iget-object p0, p2, LX/J4d;->a:LX/8SP;

    invoke-virtual {p0}, LX/8SP;->getCount()I

    move-result p0

    if-le p0, v1, :cond_0

    :goto_0
    invoke-virtual {p4, v2, v1}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(LX/8SP;Z)V

    .line 602695
    iget-object v1, p2, LX/J4d;->b:LX/8SN;

    invoke-virtual {p4, v1}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setPrivacyChangeListener(LX/8SN;)V

    .line 602696
    iget-object v1, p2, LX/J4d;->c:LX/8SO;

    invoke-virtual {p4, v1}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setSelectorOpenedListener(LX/8SO;)V

    .line 602697
    const/16 v1, 0x1f

    const v2, 0x1fef31e6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 602698
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602691
    check-cast p1, LX/1U6;

    .line 602692
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1U6;->l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1U6;->l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1U6;->l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1U6;->l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 602686
    check-cast p4, Lcom/facebook/privacy/spinner/AudienceSpinner;

    const/4 v0, 0x0

    .line 602687
    invoke-virtual {p4, v0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setPrivacyChangeListener(LX/8SN;)V

    .line 602688
    invoke-virtual {p4, v0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setSelectorOpenedListener(LX/8SO;)V

    .line 602689
    invoke-virtual {p4}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a()V

    .line 602690
    return-void
.end method
