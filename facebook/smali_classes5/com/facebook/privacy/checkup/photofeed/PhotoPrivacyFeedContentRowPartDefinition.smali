.class public Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1U6;",
        "Ljava/lang/Void;",
        "LX/J4e;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field public final d:Landroid/content/res/Resources;

.field public final e:LX/11R;

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final h:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final i:Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x40c00000    # 6.0f

    const/high16 v1, 0x40800000    # 4.0f

    .line 602774
    const v0, 0x7f030f48

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->a:LX/1Cz;

    .line 602775
    const-class v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 602776
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 602777
    iput v2, v0, LX/1UY;->b:F

    .line 602778
    move-object v0, v0

    .line 602779
    iput v2, v0, LX/1UY;->c:F

    .line 602780
    move-object v0, v0

    .line 602781
    iput v1, v0, LX/1UY;->d:F

    .line 602782
    move-object v0, v0

    .line 602783
    iput v1, v0, LX/1UY;->e:F

    .line 602784
    move-object v0, v0

    .line 602785
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/11R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602786
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602787
    iput-object p1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->d:Landroid/content/res/Resources;

    .line 602788
    iput-object p2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->e:LX/11R;

    .line 602789
    iput-object p3, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 602790
    iput-object p4, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 602791
    iput-object p5, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->h:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 602792
    iput-object p6, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->i:Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    .line 602793
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;
    .locals 10

    .prologue
    .line 602794
    const-class v1, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;

    monitor-enter v1

    .line 602795
    :try_start_0
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602796
    sput-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602797
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602798
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602799
    new-instance v3, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v5

    check-cast v5, LX/11R;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;->a(LX/0QB;)Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;-><init>(Landroid/content/res/Resources;LX/11R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;)V

    .line 602800
    move-object v0, v3

    .line 602801
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602802
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602803
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602805
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 602806
    check-cast p2, LX/1U6;

    const/4 v5, 0x0

    .line 602807
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->c:LX/1Ua;

    const v3, 0x7f021449

    const/4 v4, -0x1

    invoke-direct {v1, v5, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 602808
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b247d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 602809
    const v1, 0x7f0d24ff

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-interface {p2}, LX/1U6;->ai_()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 602810
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 602811
    move-object v3, v3

    .line 602812
    invoke-interface {p2}, LX/1U6;->c()LX/1f8;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 602813
    invoke-interface {p2}, LX/1U6;->c()LX/1f8;

    move-result-object v7

    .line 602814
    new-instance v6, Landroid/graphics/PointF;

    invoke-interface {v7}, LX/1f8;->a()D

    move-result-wide v8

    double-to-float v8, v8

    invoke-interface {v7}, LX/1f8;->b()D

    move-result-wide v10

    double-to-float v7, v10

    invoke-direct {v6, v8, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 602815
    :goto_0
    move-object v4, v6

    .line 602816
    iput-object v4, v3, LX/2f8;->h:Landroid/graphics/PointF;

    .line 602817
    move-object v3, v3

    .line 602818
    invoke-virtual {v3, v0, v0}, LX/2f8;->a(II)LX/2f8;

    move-result-object v0

    const/4 v3, 0x1

    .line 602819
    iput-boolean v3, v0, LX/2f8;->g:Z

    .line 602820
    move-object v0, v0

    .line 602821
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602822
    const v0, 0x7f0d2501

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->h:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p2}, LX/1U6;->k()J

    move-result-wide v2

    .line 602823
    iget-object v6, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->e:LX/11R;

    sget-object v7, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v2

    invoke-virtual {v6, v7, v8, v9}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    .line 602824
    iget-object v7, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->d:Landroid/content/res/Resources;

    const v8, 0x7f0838fa

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 602825
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602826
    const v0, 0x7f0d2500

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;->i:Lcom/facebook/privacy/checkup/photofeed/PhotoAudienceSpinnerPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602827
    return-object v5

    :cond_0
    sget-object v6, LX/J4f;->a:Landroid/graphics/PointF;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602828
    check-cast p1, LX/1U6;

    .line 602829
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1U6;->e()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
