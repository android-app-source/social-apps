.class public Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/J4j;",
        "Ljava/lang/Void;",
        "LX/J4e;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final d:Landroid/content/res/Resources;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 602767
    const v0, 0x7f030f49

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->a:LX/1Cz;

    .line 602768
    const-class v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 602769
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 602770
    iput v1, v0, LX/1UY;->c:F

    .line 602771
    move-object v0, v0

    .line 602772
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602736
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602737
    iput-object p1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->d:Landroid/content/res/Resources;

    .line 602738
    iput-object p2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 602739
    iput-object p3, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 602740
    iput-object p4, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 602741
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;
    .locals 7

    .prologue
    .line 602756
    const-class v1, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;

    monitor-enter v1

    .line 602757
    :try_start_0
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602758
    sput-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602759
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602760
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602761
    new-instance p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 602762
    move-object v0, p0

    .line 602763
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602764
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602765
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602773
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 602742
    check-cast p2, LX/J4j;

    .line 602743
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->c:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 602744
    iget-object v0, p2, LX/J4j;->f:LX/1Fb;

    move-object v0, v0

    .line 602745
    if-eqz v0, :cond_0

    .line 602746
    const v0, 0x7f0d14ab

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v2, LX/2f8;

    invoke-direct {v2}, LX/2f8;-><init>()V

    .line 602747
    iget-object v3, p2, LX/J4j;->f:LX/1Fb;

    move-object v3, v3

    .line 602748
    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 602749
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 602750
    move-object v2, v2

    .line 602751
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602752
    :cond_0
    const v0, 0x7f0d02c4

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->d:Landroid/content/res/Resources;

    const v3, 0x7f0838f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602753
    const v0, 0x7f0d0550

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->d:Landroid/content/res/Resources;

    const v3, 0x7f0838f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602754
    const v0, 0x7f0d2503

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;->d:Landroid/content/res/Resources;

    const v3, 0x7f0838f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602755
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602735
    const/4 v0, 0x1

    return v0
.end method
