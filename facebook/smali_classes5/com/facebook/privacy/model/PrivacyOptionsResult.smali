.class public final Lcom/facebook/privacy/model/PrivacyOptionsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/model/PrivacyOptionsResultDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final basicPrivacyOptions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "basic_privacy_options"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final expandablePrivacyOptionIndices:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "expandable_privacy_option_indices"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final friendListPrivacyOptions:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friend_list_privacy_options"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final isResultFromServer:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_result_from_server"
    .end annotation
.end field

.field public final isSelectedOptionExternal:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_selected_option_external"
    .end annotation
.end field

.field public final primaryOptionIndices:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_option_indices"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "recent_privacy_option"
    .end annotation
.end field

.field public final recentPrivacyOptionIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "recent_privacy_option_index"
    .end annotation
.end field

.field public final selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_privacy_option"
    .end annotation
.end field

.field public final selectedPrivacyOptionIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_privacy_option_index"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578651
    const-class v0, Lcom/facebook/privacy/model/PrivacyOptionsResultDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 578540
    const-class v0, Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 578650
    new-instance v0, LX/2sM;

    invoke-direct {v0}, LX/2sM;-><init>()V

    sput-object v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 578638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578639
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    .line 578640
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    .line 578641
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    .line 578642
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 578643
    iput v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    .line 578644
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578645
    iput v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    .line 578646
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578647
    iput-boolean v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    .line 578648
    iput-boolean v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    .line 578649
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ZZ)V
    .locals 1
    .param p8    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "I",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 578626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578627
    iput-object p1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    .line 578628
    iput-object p2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    .line 578629
    iput-object p3, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    .line 578630
    iput-object p4, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 578631
    iput p5, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    .line 578632
    invoke-virtual {p0, p6}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578633
    iput p7, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    .line 578634
    invoke-virtual {p0, p8}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578635
    iput-boolean p9, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    .line 578636
    iput-boolean p10, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    .line 578637
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 578604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578605
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 578606
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    .line 578607
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 578608
    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    .line 578609
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 578610
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    .line 578611
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 578612
    if-nez v0, :cond_3

    :goto_3
    iput-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 578613
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    .line 578614
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578615
    invoke-virtual {p0, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578616
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    .line 578617
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578618
    invoke-virtual {p0, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578619
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    .line 578620
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    .line 578621
    return-void

    .line 578622
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 578623
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 578624
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 578625
    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_3
.end method

.method public static a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)LX/2br;
    .locals 1

    .prologue
    .line 578603
    new-instance v0, LX/2br;

    invoke-direct {v0, p0}, LX/2br;-><init>(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 6
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 578596
    const/4 v1, 0x0

    .line 578597
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578598
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v4, v5, :cond_0

    .line 578599
    :goto_1
    return-object v0

    .line 578600
    :cond_0
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v4, v5, :cond_2

    .line 578601
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 578602
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(I)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 578652
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 578653
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578654
    :goto_0
    return-object v0

    .line 578655
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_1

    .line 578656
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto :goto_0

    .line 578657
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 578588
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578589
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v4

    if-ne v4, p1, :cond_1

    .line 578590
    :cond_0
    :goto_1
    return-object v0

    .line 578591
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 578592
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578593
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    if-eq v3, p1, :cond_0

    .line 578594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 578595
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 578580
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578581
    invoke-static {v0, p1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 578582
    :cond_0
    :goto_1
    return-object v0

    .line 578583
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 578584
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578585
    invoke-static {v0, p1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 578586
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 578587
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 578573
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-static {v0, p1}, LX/2cA;->a(Ljava/util/List;LX/1oS;)I

    move-result v0

    .line 578574
    if-ltz v0, :cond_0

    .line 578575
    :goto_0
    return v0

    .line 578576
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-static {v0, p1}, LX/2cA;->a(Ljava/util/List;LX/1oS;)I

    move-result v0

    .line 578577
    if-ltz v0, :cond_1

    .line 578578
    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 578579
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 578572
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 578543
    if-nez p1, :cond_1

    .line 578544
    :cond_0
    :goto_0
    return v3

    .line 578545
    :cond_1
    if-ne p1, p0, :cond_2

    move v3, v4

    .line 578546
    goto :goto_0

    .line 578547
    :cond_2
    instance-of v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_0

    .line 578548
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 578549
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 578550
    :goto_1
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 578551
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1oS;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    .line 578552
    if-eqz v0, :cond_0

    .line 578553
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 578554
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 578555
    :goto_2
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 578556
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1oS;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    .line 578557
    if-eqz v0, :cond_0

    .line 578558
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 578559
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    .line 578560
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    .line 578561
    xor-int v2, v0, v1

    if-nez v2, :cond_0

    .line 578562
    if-nez v0, :cond_5

    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578563
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    .line 578564
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    .line 578565
    xor-int v2, v0, v1

    if-nez v2, :cond_0

    .line 578566
    if-nez v0, :cond_6

    if-nez v1, :cond_6

    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578567
    :cond_6
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    iget v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    if-ne v0, v1, :cond_0

    .line 578568
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578569
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    iget v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    if-ne v0, v1, :cond_0

    .line 578570
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578571
    iget-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    iget-boolean v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    iget-boolean v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    if-ne v0, v1, :cond_0

    move v3, v4

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 578542
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 578541
    const-class v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "basicPrivacyOptions"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friendListOptions"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "primaryOptionIndices"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "expandablePrivacyOptionIndices"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "selectedPrivacyOptionIndex"

    iget v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "selectedPrivacyOption"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "recentPrivacyOptionIndex"

    iget v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "recentPrivacyOption"

    iget-object v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isSelectedOptionExternal"

    iget-boolean v2, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 578529
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 578530
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 578531
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 578532
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 578533
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 578534
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 578535
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 578536
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 578537
    iget-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 578538
    iget-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 578539
    return-void
.end method
