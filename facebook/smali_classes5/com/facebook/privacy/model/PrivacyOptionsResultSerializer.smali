.class public Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 578665
    const-class v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    new-instance v1, Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 578666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 578685
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/model/PrivacyOptionsResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 578679
    if-nez p0, :cond_0

    .line 578680
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 578681
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 578682
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;->b(Lcom/facebook/privacy/model/PrivacyOptionsResult;LX/0nX;LX/0my;)V

    .line 578683
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 578684
    return-void
.end method

.method private static b(Lcom/facebook/privacy/model/PrivacyOptionsResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 578668
    const-string v0, "basic_privacy_options"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 578669
    const-string v0, "friend_list_privacy_options"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 578670
    const-string v0, "primary_option_indices"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 578671
    const-string v0, "expandable_privacy_option_indices"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 578672
    const-string v0, "selected_privacy_option_index"

    iget v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 578673
    const-string v0, "selected_privacy_option"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 578674
    const-string v0, "recent_privacy_option_index"

    iget v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 578675
    const-string v0, "recent_privacy_option"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 578676
    const-string v0, "is_selected_option_external"

    iget-boolean v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 578677
    const-string v0, "is_result_from_server"

    iget-boolean v1, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 578678
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 578667
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/model/PrivacyOptionsResultSerializer;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;LX/0nX;LX/0my;)V

    return-void
.end method
