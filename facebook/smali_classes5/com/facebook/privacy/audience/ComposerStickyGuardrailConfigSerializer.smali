.class public Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 634789
    const-class v0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    new-instance v1, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 634790
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634791
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 634792
    if-nez p0, :cond_0

    .line 634793
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 634794
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 634795
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;->b(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;LX/0nX;LX/0my;)V

    .line 634796
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 634797
    return-void
.end method

.method private static b(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 634798
    const-string v0, "eligible"

    iget-boolean v1, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mEligible:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 634799
    const-string v0, "current_privacy_option"

    iget-object v1, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mCurrentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 634800
    const-string v0, "suggested_privacy_option"

    iget-object v1, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 634801
    const-string v0, "suggestion_timestamp"

    iget-wide v2, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 634802
    const-string v0, "config_updated_time"

    iget-wide v2, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mUpdatedTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 634803
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 634804
    check-cast p1, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;->a(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;LX/0nX;LX/0my;)V

    return-void
.end method
