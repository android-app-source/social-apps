.class public Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;
.end annotation


# instance fields
.field public final mEligible:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "eligible"
    .end annotation
.end field

.field public final mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "first_option"
    .end annotation
.end field

.field public final mSecondSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "second_option"
    .end annotation
.end field

.field public final mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "trigger_option"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634336
    const-class v0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634337
    const-class v0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 634338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634339
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mEligible:Z

    .line 634340
    iput-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634341
    iput-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634342
    iput-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mSecondSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634343
    return-void
.end method

.method public constructor <init>(LX/3lY;)V
    .locals 1

    .prologue
    .line 634344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634345
    iget-boolean v0, p1, LX/3lY;->a:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mEligible:Z

    .line 634346
    iget-object v0, p1, LX/3lY;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634347
    iget-object v0, p1, LX/3lY;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634348
    iget-object v0, p1, LX/3lY;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mSecondSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634349
    return-void
.end method
