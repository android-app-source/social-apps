.class public Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;
.end annotation


# instance fields
.field public final mCurrentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "current_privacy_option"
    .end annotation
.end field

.field public final mEligible:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "eligible"
    .end annotation
.end field

.field public final mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggested_privacy_option"
    .end annotation
.end field

.field public final mSuggestedTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestion_timestamp"
    .end annotation
.end field

.field public final mUpdatedTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "config_updated_time"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634787
    const-class v0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 634786
    const-class v0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfigSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 634771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mEligible:Z

    .line 634773
    iput-object v1, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mCurrentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634774
    iput-object v1, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634775
    iput-wide v2, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedTime:J

    .line 634776
    iput-wide v2, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mUpdatedTime:J

    .line 634777
    return-void
.end method

.method public constructor <init>(LX/3le;)V
    .locals 2

    .prologue
    .line 634779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634780
    iget-boolean v0, p1, LX/3le;->a:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mEligible:Z

    .line 634781
    iget-object v0, p1, LX/3le;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mCurrentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634782
    iget-object v0, p1, LX/3le;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634783
    iget-wide v0, p1, LX/3le;->d:J

    iput-wide v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedTime:J

    .line 634784
    iget-wide v0, p1, LX/3le;->e:J

    iput-wide v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mUpdatedTime:J

    .line 634785
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 634788
    iget-boolean v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mEligible:Z

    return v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1

    .prologue
    .line 634778
    iget-object v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mCurrentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1

    .prologue
    .line 634770
    iget-object v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 634769
    iget-wide v0, p0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedTime:J

    return-wide v0
.end method
