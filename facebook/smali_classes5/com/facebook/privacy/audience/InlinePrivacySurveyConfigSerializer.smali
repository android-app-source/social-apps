.class public Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 634363
    const-class v0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    new-instance v1, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 634364
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634362
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 634356
    if-nez p0, :cond_0

    .line 634357
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 634358
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 634359
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;->b(Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;LX/0nX;LX/0my;)V

    .line 634360
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 634361
    return-void
.end method

.method private static b(Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 634351
    const-string v0, "eligible"

    iget-boolean v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mEligible:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 634352
    const-string v0, "trigger_option"

    iget-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 634353
    const-string v0, "first_option"

    iget-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 634354
    const-string v0, "second_option"

    iget-object v1, p0, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mSecondSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 634355
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 634350
    check-cast p1, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfigSerializer;->a(Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;LX/0nX;LX/0my;)V

    return-void
.end method
