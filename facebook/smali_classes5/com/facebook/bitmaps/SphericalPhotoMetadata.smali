.class public Lcom/facebook/bitmaps/SphericalPhotoMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bitmaps/SphericalPhotoMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mCroppedAreaImageHeightPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mCroppedAreaImageWidthPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mCroppedAreaLeftPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mCroppedAreaTopPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mFullPanoHeightPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mFullPanoWidthPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mInitialViewHeadingDegrees:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mInitialViewPitchDegrees:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mPoseHeadingDegrees:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mPosePitchDegrees:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mPoseRollDegrees:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mPreProcessCropLeftPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mPreProcessCropRightPixels:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mProjectionType:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668875
    const-string v0, "fb_imgproc"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V

    .line 668876
    new-instance v0, LX/43K;

    invoke-direct {v0}, LX/43K;-><init>()V

    sput-object v0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 668873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668874
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 668857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668858
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    .line 668859
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    .line 668860
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    .line 668861
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaLeftPixels:I

    .line 668862
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaTopPixels:I

    .line 668863
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    .line 668864
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    .line 668865
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseHeadingDegrees:D

    .line 668866
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPosePitchDegrees:D

    .line 668867
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseRollDegrees:D

    .line 668868
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewHeadingDegrees:D

    .line 668869
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewPitchDegrees:D

    .line 668870
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPreProcessCropLeftPixels:I

    .line 668871
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPreProcessCropRightPixels:I

    .line 668872
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 668855
    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    int-to-double v2, v2

    cmpl-double v2, v2, v0

    if-nez v2, :cond_0

    .line 668856
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x4076800000000000L    # 360.0

    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 668831
    iget-object v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 668854
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 668853
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 668852
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 668877
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaLeftPixels:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 668851
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaTopPixels:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 668850
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 668849
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    return v0
.end method

.method public final i()D
    .locals 2

    .prologue
    .line 668848
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPosePitchDegrees:D

    return-wide v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 668847
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseRollDegrees:D

    return-wide v0
.end method

.method public native serializeToJson()Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 668832
    iget-object v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 668833
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoWidthPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668834
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mFullPanoHeightPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668835
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaLeftPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668836
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaTopPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668837
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageWidthPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668838
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mCroppedAreaImageHeightPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668839
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseHeadingDegrees:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 668840
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPosePitchDegrees:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 668841
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPoseRollDegrees:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 668842
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewHeadingDegrees:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 668843
    iget-wide v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mInitialViewPitchDegrees:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 668844
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPreProcessCropLeftPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668845
    iget v0, p0, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mPreProcessCropRightPixels:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668846
    return-void
.end method
