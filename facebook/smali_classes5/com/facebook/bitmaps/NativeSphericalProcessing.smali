.class public Lcom/facebook/bitmaps/NativeSphericalProcessing;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668826
    const-string v0, "fb_imgproc"

    invoke-static {v0}, LX/02C;->a(Ljava/lang/String;)V

    .line 668827
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 668825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/facebook/bitmaps/SphericalPhotoMetadata;)Z
    .locals 1

    .prologue
    .line 668820
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668821
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668822
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668823
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668824
    invoke-static/range {p0 .. p8}, Lcom/facebook/bitmaps/NativeSphericalProcessing;->nativeTryDeriveSphericalMetadata(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/facebook/bitmaps/SphericalPhotoMetadata;)Z

    move-result v0

    return v0
.end method

.method private static native nativeTryDeriveSphericalMetadata(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/facebook/bitmaps/SphericalPhotoMetadata;)Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
