.class public Lcom/facebook/bitmaps/NativeImageProcessor;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/bitmaps/NativeImageLibraries;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 668677
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x10e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/bitmaps/NativeImageProcessor;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/bitmaps/NativeImageLibraries;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/bitmaps/NativeImageLibraries;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668679
    iput-object p2, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->b:Lcom/facebook/bitmaps/NativeImageLibraries;

    .line 668680
    iget-object v0, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->b:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0}, LX/03m;->T_()Z

    .line 668681
    iput-object p1, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->c:LX/0Ot;

    .line 668682
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageProcessor;
    .locals 1

    .prologue
    .line 668683
    invoke-static {p0}, Lcom/facebook/bitmaps/NativeImageProcessor;->b(LX/0QB;)Lcom/facebook/bitmaps/NativeImageProcessor;

    move-result-object v0

    return-object v0
.end method

.method private static a(III)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 668697
    if-le p0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 668698
    if-le p1, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 668699
    if-lez p2, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 668700
    const/16 v0, 0x64

    if-gt p2, v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 668701
    return-void

    :cond_0
    move v0, v2

    .line 668702
    goto :goto_0

    :cond_1
    move v0, v2

    .line 668703
    goto :goto_1

    :cond_2
    move v0, v2

    .line 668704
    goto :goto_2

    :cond_3
    move v1, v2

    .line 668705
    goto :goto_3
.end method

.method private static a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/io/OutputStream;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 668684
    iget v0, p2, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "All rect bounds must be between 0 and 1."

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668685
    iget v0, p2, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_1

    iget v0, p2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_1

    iget v0, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_1

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "All rect bounds must be between 0 and 1."

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668686
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "All rect bounds must have left < right and top < bottom"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668687
    invoke-static {p0}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    sget-object v3, LX/1ld;->a:LX/1lW;

    if-ne v0, v3, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 668688
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668689
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/bitmaps/NativeImageProcessor;->cropJpeg(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingBadParamException; {:try_start_0 .. :try_end_0} :catch_0

    .line 668690
    return-void

    :cond_0
    move v0, v2

    .line 668691
    goto :goto_0

    :cond_1
    move v0, v2

    .line 668692
    goto :goto_1

    :cond_2
    move v0, v2

    .line 668693
    goto :goto_2

    :cond_3
    move v1, v2

    .line 668694
    goto :goto_3

    .line 668695
    :catch_0
    move-exception v0

    .line 668696
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 668708
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 668709
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 668710
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 668711
    :cond_0
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 668712
    :try_start_0
    invoke-static {p0, p1, p2, v1}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668713
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 668714
    return-void

    .line 668715
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/bitmaps/NativeImageProcessor;
    .locals 3

    .prologue
    .line 668706
    new-instance v1, Lcom/facebook/bitmaps/NativeImageProcessor;

    const/16 v0, 0x259

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/bitmaps/NativeImageLibraries;->a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;

    move-result-object v0

    check-cast v0, Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-direct {v1, v2, v0}, Lcom/facebook/bitmaps/NativeImageProcessor;-><init>(LX/0Ot;Lcom/facebook/bitmaps/NativeImageLibraries;)V

    .line 668707
    return-object v1
.end method

.method private native cropFaceIntoBitmap(Ljava/lang/String;ILandroid/graphics/RectF;IZZLandroid/graphics/Bitmap;[F[I)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native cropFaceIntoBitmapFromBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/RectF;IZZLandroid/graphics/Bitmap;[F[I)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private static native cropJpeg(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/io/OutputStream;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeRotateJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native transcodeJpeg(Ljava/lang/String;IIIIIIZ)[B
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 2

    .prologue
    .line 668622
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668623
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668624
    sget-object v0, Lcom/facebook/bitmaps/NativeImageProcessor;->a:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 668625
    if-nez p3, :cond_0

    .line 668626
    invoke-static {p1, p2}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 668627
    :goto_0
    return-void

    .line 668628
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/bitmaps/NativeImageProcessor;->nativeRotateJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingBadParamException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/facebook/bitmaps/NativeImageProcessor$NativeImageProcessorException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 668629
    :catch_0
    move-exception v0

    .line 668630
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 668631
    :catch_1
    move-exception v0

    .line 668632
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;IIILandroid/graphics/RectF;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 668633
    iget v0, p5, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p5, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p5, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    iget v0, p5, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "All rect bounds must be between 0 and 1."

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668634
    iget v0, p5, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p5, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p5, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    iget v0, p5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "All rect bounds must be between 0 and 1."

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668635
    iget v0, p5, Landroid/graphics/RectF;->left:F

    iget v3, p5, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    iget v0, p5, Landroid/graphics/RectF;->top:F

    iget v3, p5, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    move v0, v1

    :goto_2
    const-string v3, "All rect bounds must have left < right and top < bottom"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668636
    invoke-static {p1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    sget-object v3, LX/1ld;->a:LX/1lW;

    if-eq v0, v3, :cond_4

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 668637
    invoke-static {p1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v2

    .line 668638
    new-instance v1, Landroid/graphics/Rect;

    iget v0, v2, LX/434;->b:I

    int-to-float v0, v0

    iget v3, p5, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, v2, LX/434;->a:I

    int-to-float v3, v3

    iget v4, p5, Landroid/graphics/RectF;->top:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, v2, LX/434;->b:I

    int-to-float v4, v4

    iget v5, p5, Landroid/graphics/RectF;->right:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, v2, LX/434;->a:I

    int-to-float v5, v5

    iget v6, p5, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v0, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 668639
    const/4 v6, 0x0

    .line 668640
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_5

    const/16 v0, 0x1e0

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-le v0, v3, :cond_5

    const/16 v0, 0x1e0

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v0, v3, :cond_5

    .line 668641
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget v3, v2, LX/434;->b:I

    iget v4, v2, LX/434;->a:I

    const/4 v5, 0x1

    move v2, p4

    invoke-static/range {v0 .. v5}, LX/2Qx;->a(Landroid/net/Uri;Landroid/graphics/Rect;IIIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 668642
    if-eqz v1, :cond_7

    .line 668643
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 668644
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x55

    invoke-static {v1, v2, v3, v0}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 668645
    if-eqz v1, :cond_0

    .line 668646
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    :goto_4
    return-void

    :cond_1
    move v0, v2

    .line 668647
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 668648
    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 668649
    goto :goto_2

    :cond_4
    move v1, v2

    .line 668650
    goto :goto_3

    .line 668651
    :cond_5
    if-nez p3, :cond_6

    if-nez p2, :cond_6

    .line 668652
    :try_start_2
    iget v0, v2, LX/434;->b:I

    iget v2, v2, LX/434;->a:I

    invoke-static {p1, v0, v2}, LX/2Qx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 668653
    :goto_5
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 668654
    invoke-static {v0, v2, v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object v1, v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 668655
    if-eqz v1, :cond_7

    .line 668656
    :try_start_3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 668657
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x55

    invoke-static {v1, v2, v3, v0}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 668658
    if-eqz v1, :cond_0

    .line 668659
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_4

    .line 668660
    :cond_6
    :try_start_4
    invoke-static {p1, p2, p3}, LX/2Qx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto :goto_5

    .line 668661
    :cond_7
    :try_start_5
    iget-object v0, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "failed to crop photo to file"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 668662
    if-eqz v1, :cond_0

    .line 668663
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_4

    .line 668664
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    .line 668665
    :goto_6
    :try_start_6
    iget-object v0, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "failed to crop photo to file"

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 668666
    if-eqz v2, :cond_0

    .line 668667
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_4

    .line 668668
    :catchall_0
    move-exception v0

    :goto_7
    if-eqz v6, :cond_8

    .line 668669
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    throw v0

    .line 668670
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v6, v2

    goto :goto_7

    .line 668671
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_6
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 668672
    iget-object v0, p0, Lcom/facebook/bitmaps/NativeImageProcessor;->b:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0}, LX/03m;->T_()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;IIILX/437;LX/43I;IZ)[B
    .locals 10

    .prologue
    .line 668673
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 668674
    new-instance v1, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "N/missing file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException;-><init>(Ljava/lang/String;Z)V

    throw v1

    .line 668675
    :cond_0
    move/from16 v0, p7

    invoke-static {p3, p4, v0}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(III)V

    .line 668676
    invoke-virtual {p5}, LX/437;->ordinal()I

    move-result v6

    invoke-virtual/range {p6 .. p6}, LX/43I;->ordinal()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/facebook/bitmaps/NativeImageProcessor;->transcodeJpeg(Ljava/lang/String;IIIIIIZ)[B

    move-result-object v1

    return-object v1
.end method

.method public native startProfiler()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public native stopProfiler()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
