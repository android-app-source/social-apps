.class public Lcom/facebook/work/auth/request/model/WorkCommunity;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/work/auth/request/model/WorkCommunity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 810496
    new-instance v0, LX/4ow;

    invoke-direct {v0}, LX/4ow;-><init>()V

    sput-object v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 810504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810505
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    .line 810506
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    .line 810507
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->c:Ljava/lang/String;

    .line 810508
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->d:Ljava/lang/String;

    .line 810509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->e:Ljava/lang/String;

    .line 810510
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 810511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810512
    iput-object p1, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    .line 810513
    iput-object p2, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    .line 810514
    iput-object p3, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->c:Ljava/lang/String;

    .line 810515
    iput-object p4, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->d:Ljava/lang/String;

    .line 810516
    iput-object p5, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->e:Ljava/lang/String;

    .line 810517
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 810503
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 810497
    iget-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810498
    iget-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810499
    iget-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810500
    iget-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810501
    iget-object v0, p0, Lcom/facebook/work/auth/request/model/WorkCommunity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810502
    return-void
.end method
