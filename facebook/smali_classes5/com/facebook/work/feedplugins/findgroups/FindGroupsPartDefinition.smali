.class public Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "LX/KAW;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Ljava/lang/String;

.field public final f:LX/KAO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 614630
    new-instance v0, LX/3cU;

    invoke-direct {v0}, LX/3cU;-><init>()V

    sput-object v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/KAO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 614631
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 614632
    iput-object p2, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 614633
    iput-object p1, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 614634
    iput-object p3, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 614635
    iput-object p4, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->f:LX/KAO;

    .line 614636
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->e:Ljava/lang/String;

    .line 614637
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;
    .locals 7

    .prologue
    .line 614638
    const-class v1, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    monitor-enter v1

    .line 614639
    :try_start_0
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 614640
    sput-object v2, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 614641
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614642
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 614643
    new-instance p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/KAO;->b(LX/0QB;)LX/KAO;

    move-result-object v6

    check-cast v6, LX/KAO;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/KAO;)V

    .line 614644
    move-object v0, p0

    .line 614645
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 614646
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614647
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 614648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 614649
    sget-object v0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 614650
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 614651
    new-instance v2, LX/KAU;

    invoke-direct {v2, p0}, LX/KAU;-><init>(Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;)V

    .line 614652
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v3, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614653
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 614654
    new-instance v3, LX/KAV;

    .line 614655
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 614656
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFindGroupsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, LX/KAV;-><init>(Ljava/lang/String;Z)V

    move-object v0, p3

    .line 614657
    check-cast v0, LX/1Pr;

    .line 614658
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 614659
    check-cast v1, LX/0jW;

    invoke-interface {v0, v3, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 614660
    iget-object v0, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->f:LX/KAO;

    iget-object v1, p0, Lcom/facebook/work/feedplugins/findgroups/FindGroupsPartDefinition;->e:Ljava/lang/String;

    const-string v4, "find_groups"

    invoke-virtual {v0, v1, v4}, LX/KAO;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 614661
    check-cast p3, LX/1Pr;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p3, v3, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 614662
    :cond_0
    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x209c7f70

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 614663
    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/KAW;

    .line 614664
    invoke-virtual {p4, p2}, LX/KAW;->setButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 614665
    const/16 v1, 0x1f

    const v2, 0x17385689

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 614666
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 614667
    check-cast p4, LX/KAW;

    .line 614668
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/KAW;->setButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 614669
    return-void
.end method
