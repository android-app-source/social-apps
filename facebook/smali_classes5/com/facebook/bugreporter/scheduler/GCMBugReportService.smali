.class public Lcom/facebook/bugreporter/scheduler/GCMBugReportService;
.super LX/2fD;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6H2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 570782
    invoke-direct {p0}, LX/2fD;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;

    const/16 v1, 0x1876

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 1

    .prologue
    .line 570783
    iget-object v0, p0, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6H2;

    .line 570784
    invoke-virtual {v0}, LX/6H2;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 570785
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x72e9e16f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 570786
    invoke-super {p0}, LX/2fD;->onCreate()V

    .line 570787
    invoke-static {p0, p0}, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 570788
    const/16 v1, 0x25

    const v2, -0x444c99cd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x26a5d7b3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 570789
    if-nez p1, :cond_0

    .line 570790
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, 0x43d423ad

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570791
    :catch_0
    move-exception v1

    .line 570792
    const-string v3, "GCMBugReportService"

    const-string v4, "Unexpected service start parameters"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570793
    invoke-virtual {p0, p3}, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;->stopSelf(I)V

    .line 570794
    const v1, -0x6b0f0d82

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 570795
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    const v1, 0x51907332

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
