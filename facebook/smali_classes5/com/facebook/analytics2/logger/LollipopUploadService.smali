.class public Lcom/facebook/analytics2/logger/LollipopUploadService;
.super Landroid/app/job/JobService;
.source ""


# instance fields
.field public a:LX/2Xc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 662965
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 662966
    return-void
.end method


# virtual methods
.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x3f92fb31

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 662967
    new-instance v1, LX/2Xc;

    invoke-direct {v1, p0}, LX/2Xc;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/analytics2/logger/LollipopUploadService;->a:LX/2Xc;

    .line 662968
    const/16 v1, 0x25

    const v2, 0x31ea0d61

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x39d39c6d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 662969
    iget-object v0, p0, Lcom/facebook/analytics2/logger/LollipopUploadService;->a:LX/2Xc;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Xc;

    new-instance v2, LX/2WW;

    invoke-direct {v2, p0, p3}, LX/2WW;-><init>(Landroid/app/Service;I)V

    invoke-virtual {v0, p1, v2}, LX/2Xc;->a(Landroid/content/Intent;LX/2WW;)I

    move-result v0

    const/16 v2, 0x25

    const v3, 0x6f8c15ed

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v0
.end method

.method public final onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5

    .prologue
    .line 662970
    :try_start_0
    iget-object v0, p0, Lcom/facebook/analytics2/logger/LollipopUploadService;->a:LX/2Xc;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Xc;

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v1

    new-instance v2, LX/2DI;

    new-instance v3, Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(Landroid/os/PersistableBundle;)V

    invoke-direct {v2, v3}, LX/2DI;-><init>(Landroid/os/Bundle;)V

    new-instance v3, LX/409;

    invoke-direct {v3, p0, p1}, LX/409;-><init>(Lcom/facebook/analytics2/logger/LollipopUploadService;Landroid/app/job/JobParameters;)V

    invoke-virtual {v0, v1, v2, v3}, LX/2Xc;->a(ILX/2DI;LX/2fJ;)V
    :try_end_0
    .catch LX/403; {:try_start_0 .. :try_end_0} :catch_0

    .line 662971
    const/4 v0, 0x1

    .line 662972
    :goto_0
    return v0

    .line 662973
    :catch_0
    move-exception v0

    .line 662974
    const-string v1, "PostLolliopUploadService"

    const-string v2, "Misunderstood job service extras: %s"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 662975
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    .line 662976
    iget-object v0, p0, Lcom/facebook/analytics2/logger/LollipopUploadService;->a:LX/2Xc;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Xc;

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Xc;->a(I)V

    .line 662977
    const/4 v0, 0x1

    return v0
.end method
