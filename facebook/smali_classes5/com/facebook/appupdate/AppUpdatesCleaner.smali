.class public Lcom/facebook/appupdate/AppUpdatesCleaner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LX/1sZ;

.field private final b:LX/1wh;

.field private final c:LX/1wo;


# direct methods
.method public constructor <init>(LX/1sZ;LX/1wh;LX/1wo;)V
    .locals 0

    .prologue
    .line 571261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571262
    iput-object p1, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->a:LX/1sZ;

    .line 571263
    iput-object p2, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->b:LX/1wh;

    .line 571264
    iput-object p3, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->c:LX/1wo;

    .line 571265
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 571266
    invoke-static {}, LX/1wm;->a()V

    .line 571267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 571268
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->c:LX/1wo;

    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 571269
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    iget-object v0, v0, LX/EeX;->localFile:Ljava/io/File;

    .line 571270
    if-eqz v0, :cond_0

    .line 571271
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 571272
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->a:LX/1sZ;

    const/4 v4, 0x0

    .line 571273
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 571274
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 571275
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571276
    :cond_2
    invoke-static {v0}, LX/1sZ;->a(LX/1sZ;)Ljava/io/File;

    move-result-object v2

    .line 571277
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 571278
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    invoke-static {v3}, LX/1wm;->a(Z)V

    .line 571279
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 571280
    if-eqz v6, :cond_4

    .line 571281
    array-length v7, v6

    move v3, v4

    :goto_2
    if-ge v3, v7, :cond_4

    aget-object v8, v6, v3

    .line 571282
    const/4 v2, 0x0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 571283
    :try_start_1
    invoke-virtual {v8}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    move-result-object v2

    .line 571284
    :goto_3
    if-eqz v2, :cond_3

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 571285
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 571286
    sget-boolean v2, LX/EeM;->a:Z

    if-eqz v2, :cond_3

    .line 571287
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "Removing orphaned file: "

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v8, v4, [Ljava/lang/Object;

    invoke-static {v2, v8}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 571288
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 571289
    :catch_0
    move-exception v9

    .line 571290
    const-string v10, "AppUpdateLib"

    const-string v11, "Could not get canonical file for %s, skipping."

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v8, v12, v4

    invoke-static {v10, v9, v11, v12}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 571291
    :cond_4
    :goto_4
    return-void

    .line 571292
    :catch_1
    move-exception v0

    .line 571293
    iget-object v1, p0, Lcom/facebook/appupdate/AppUpdatesCleaner;->b:LX/1wh;

    const-string v2, "appupdate_error_ioexception_cleaning_files"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/1wh;->b(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Throwable;)V

    goto :goto_4
.end method
