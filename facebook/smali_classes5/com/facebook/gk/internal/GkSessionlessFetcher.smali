.class public Lcom/facebook/gk/internal/GkSessionlessFetcher;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:Lcom/facebook/gk/internal/GkSessionlessFetcher;


# instance fields
.field private final b:LX/11H;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2K5;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2K2;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2K2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571473
    const-class v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;

    sput-object v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/0Ot;Ljava/util/Set;)V
    .locals 1
    .param p3    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/gk/internal/SessionlessGkListener;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0Ot",
            "<",
            "LX/2K5;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/2K2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 571474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571475
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->e:Ljava/util/List;

    .line 571476
    iput-object p1, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->b:LX/11H;

    .line 571477
    iput-object p2, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->c:LX/0Ot;

    .line 571478
    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571479
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->d:Ljava/util/List;

    .line 571480
    :goto_0
    return-void

    .line 571481
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->d:Ljava/util/List;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/gk/internal/GkSessionlessFetcher;
    .locals 8

    .prologue
    .line 571482
    sget-object v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->f:Lcom/facebook/gk/internal/GkSessionlessFetcher;

    if-nez v0, :cond_1

    .line 571483
    const-class v1, Lcom/facebook/gk/internal/GkSessionlessFetcher;

    monitor-enter v1

    .line 571484
    :try_start_0
    sget-object v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->f:Lcom/facebook/gk/internal/GkSessionlessFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 571485
    if-eqz v2, :cond_0

    .line 571486
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 571487
    new-instance v4, Lcom/facebook/gk/internal/GkSessionlessFetcher;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    const/16 v5, 0xaaf

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 571488
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance p0, LX/2K0;

    invoke-direct {p0, v0}, LX/2K0;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 571489
    invoke-direct {v4, v3, v5, v6}, Lcom/facebook/gk/internal/GkSessionlessFetcher;-><init>(LX/11H;LX/0Ot;Ljava/util/Set;)V

    .line 571490
    move-object v0, v4

    .line 571491
    sput-object v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->f:Lcom/facebook/gk/internal/GkSessionlessFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571492
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 571493
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 571494
    :cond_1
    sget-object v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->f:Lcom/facebook/gk/internal/GkSessionlessFetcher;

    return-object v0

    .line 571495
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 571496
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 571497
    new-instance v1, LX/2K3;

    .line 571498
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 571499
    sget-object v2, LX/2K4;->IS_SESSIONLESS:LX/2K4;

    invoke-direct {v1, v0, v2}, LX/2K3;-><init>(LX/0Rf;LX/2K4;)V

    .line 571500
    :try_start_0
    iget-object v2, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->b:LX/11H;

    iget-object v0, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v2, v0, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 571501
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gatekeepers"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 571502
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 571503
    :catch_0
    move-exception v0

    .line 571504
    sget-object v1, Lcom/facebook/gk/internal/GkSessionlessFetcher;->a:Ljava/lang/Class;

    const-string v2, "Sessionless gatekeeper fetch with SingleMethodRunner failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 571505
    const/4 v0, 0x0

    goto :goto_0

    .line 571506
    :cond_1
    iget-object v1, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 571507
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    .line 571508
    iget-object v1, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2K2;

    invoke-interface {v1, v0}, LX/2K2;->a(Landroid/os/Bundle;)V

    .line 571509
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 571510
    :cond_2
    iget-object v1, p0, Lcom/facebook/gk/internal/GkSessionlessFetcher;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2K2;

    .line 571511
    invoke-interface {v1, v0}, LX/2K2;->a(Landroid/os/Bundle;)V

    goto :goto_2
.end method
