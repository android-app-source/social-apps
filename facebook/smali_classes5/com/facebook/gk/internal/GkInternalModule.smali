.class public Lcom/facebook/gk/internal/GkInternalModule;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation

.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 571518
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 571519
    return-void
.end method

.method public static a(Lcom/facebook/gk/store/GatekeeperWriter;LX/0Uh;)LX/87d;
    .locals 3
    .param p0    # Lcom/facebook/gk/store/GatekeeperWriter;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 571517
    new-instance v0, LX/87d;

    sget-object v1, LX/2Jx;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, LX/87d;-><init>(Lcom/facebook/gk/store/GatekeeperWriter;LX/0Uh;LX/0Tn;I)V

    return-object v0
.end method

.method public static a()Ljava/lang/Long;
    .locals 2
    .annotation runtime Lcom/facebook/gk/internal/GkConfigurationFetchPeriodMillis;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 571515
    const-wide/32 v0, 0x6ddd00

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0dC;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/gk/internal/DeviceIdForGKs;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 571516
    invoke-virtual {p0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/gk/store/GatekeeperWriter;LX/0Uh;)LX/87d;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 571514
    new-instance v0, LX/87d;

    sget-object v1, LX/2ai;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, LX/87d;-><init>(Lcom/facebook/gk/store/GatekeeperWriter;LX/0Uh;LX/0Tn;I)V

    return-object v0
.end method

.method public static getInstanceForTest_GkSessionlessFetcher(LX/0QA;)Lcom/facebook/gk/internal/GkSessionlessFetcher;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 571512
    invoke-static {p0}, Lcom/facebook/gk/internal/GkSessionlessFetcher;->a(LX/0QB;)Lcom/facebook/gk/internal/GkSessionlessFetcher;

    move-result-object v0

    check-cast v0, Lcom/facebook/gk/internal/GkSessionlessFetcher;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 571513
    return-void
.end method
