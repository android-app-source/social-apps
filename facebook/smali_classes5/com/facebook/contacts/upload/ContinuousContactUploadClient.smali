.class public Lcom/facebook/contacts/upload/ContinuousContactUploadClient;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/0aG;

.field private final d:LX/0SG;

.field public final e:LX/2UP;

.field public final f:LX/2UQ;

.field public final g:LX/30I;

.field public final h:Ljava/util/concurrent/Executor;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/0tX;

.field private final k:LX/2UR;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field public o:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574537
    const-class v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 574538
    const-class v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    sput-object v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0SG;LX/2UP;LX/2UQ;LX/30I;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/2UR;LX/0Or;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadLimitEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0SG;",
            "LX/2UP;",
            "LX/2UQ;",
            "LX/30I;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0tX;",
            "LX/2UR;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574525
    iput-object p1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->c:LX/0aG;

    .line 574526
    iput-object p2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->d:LX/0SG;

    .line 574527
    iput-object p3, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    .line 574528
    iput-object p4, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->f:LX/2UQ;

    .line 574529
    iput-object p5, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->g:LX/30I;

    .line 574530
    iput-object p6, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->h:Ljava/util/concurrent/Executor;

    .line 574531
    iput-object p7, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574532
    iput-object p8, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->j:LX/0tX;

    .line 574533
    iput-object p9, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->k:LX/2UR;

    .line 574534
    iput-object p10, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->l:LX/0Or;

    .line 574535
    const/16 v0, 0x2710

    iput v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->n:I

    .line 574536
    return-void
.end method

.method private static a(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)LX/1ML;
    .locals 6

    .prologue
    .line 574516
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->START_UPLOAD_CONTACTS:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 574517
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 574518
    const-string v0, "forceFullUploadAndTurnOffGlobalKillSwitch"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 574519
    const-string v0, "contactsUploadPhonebookMaxLimit"

    iget v1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->n:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 574520
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->c:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x7a869c52

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 574521
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 574522
    new-instance v1, LX/951;

    invoke-direct {v1, p0}, LX/951;-><init>(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574523
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/contacts/upload/ContinuousContactUploadClient;
    .locals 1

    .prologue
    .line 574515
    invoke-static {p0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(LX/0QB;)Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)V
    .locals 8

    .prologue
    .line 574505
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 574506
    invoke-static {p0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 574507
    iget-object v4, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    .line 574508
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "ccu_upload"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 574509
    const-string v6, "data_type"

    const-string v7, "ccu_upload_age"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "ccu_last_uploaded_addressbook_age_in_seconds"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 574510
    const-string v7, "contacts_upload"

    move-object v7, v7

    .line 574511
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 574512
    iget-object v6, v4, LX/2UP;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 574513
    iget-object v2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/32V;->f:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 574514
    return-void
.end method

.method public static b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)J
    .locals 4

    .prologue
    .line 574504
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->f:LX/0Tn;

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/contacts/upload/ContinuousContactUploadClient;
    .locals 11

    .prologue
    .line 574539
    new-instance v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/2UP;->a(LX/0QB;)LX/2UP;

    move-result-object v3

    check-cast v3, LX/2UP;

    invoke-static {p0}, LX/2UQ;->b(LX/0QB;)LX/2UQ;

    move-result-object v4

    check-cast v4, LX/2UQ;

    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v5

    check-cast v5, LX/30I;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {p0}, LX/2UR;->a(LX/0QB;)LX/2UR;

    move-result-object v9

    check-cast v9, LX/2UR;

    const/16 v10, 0x30e

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;-><init>(LX/0aG;LX/0SG;LX/2UP;LX/2UQ;LX/30I;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/2UR;LX/0Or;)V

    .line 574540
    return-object v0
.end method

.method private static b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/15i;I)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 574488
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v3, LX/95K;->UPDATE_SNAPSHOT_DB_WITH_SERVER_ENTRIES:LX/95K;

    invoke-virtual {v0, v3}, LX/2UP;->a(LX/95K;)V

    .line 574489
    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 574490
    :goto_1
    return-void

    .line 574491
    :cond_0
    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 574492
    :cond_2
    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v0

    const v3, 0x57e83eec

    invoke-static {p1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 574493
    :goto_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 574494
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->m:Ljava/util/Set;

    .line 574495
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v0

    :cond_3
    :goto_3
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 574496
    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->c(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 574497
    if-eqz v6, :cond_3

    .line 574498
    invoke-virtual {v5, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 574499
    new-instance v5, LX/95A;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v5, v8, v9, v4}, LX/95A;-><init>(JLjava/lang/String;)V

    .line 574500
    new-instance v4, LX/95C;

    sget-object v7, LX/95B;->ADD:LX/95B;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v4, v7, v8, v9, v5}, LX/95C;-><init>(LX/95B;JLX/95A;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 574501
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 574502
    :cond_5
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->f:LX/2UQ;

    invoke-virtual {v0}, LX/2UQ;->a()V

    .line 574503
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->f:LX/2UQ;

    invoke-virtual {v0, v3}, LX/2UQ;->a(Ljava/util/List;)V

    goto :goto_1
.end method

.method private b(Z)V
    .locals 18

    .prologue
    .line 574461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->k:LX/2UR;

    invoke-virtual {v2}, LX/2UR;->d()Landroid/database/Cursor;

    move-result-object v6

    .line 574462
    if-nez v6, :cond_1

    .line 574463
    :cond_0
    :goto_0
    return-void

    .line 574464
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/32V;->j:LX/0Tn;

    const-wide/16 v4, 0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 574465
    const-wide/16 v4, 0x1

    .line 574466
    const-string v2, "_id"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 574467
    const-string v2, "version"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 574468
    const/4 v3, 0x0

    .line 574469
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 574470
    if-eqz p1, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    const/4 v11, 0x0

    invoke-virtual {v2, v11}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_5

    add-int/lit8 v2, v3, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->n:I

    if-gt v2, v3, :cond_2

    .line 574471
    :goto_2
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 574472
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v14

    .line 574473
    const-wide/16 v16, 0x1f

    mul-long v4, v4, v16

    add-long/2addr v4, v12

    .line 574474
    const-wide/16 v12, 0x1f

    mul-long/2addr v4, v12

    add-long/2addr v4, v14

    move v3, v2

    .line 574475
    goto :goto_1

    .line 574476
    :cond_2
    cmp-long v2, v8, v4

    if-nez v2, :cond_3

    .line 574477
    if-eqz v6, :cond_0

    .line 574478
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 574479
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/32V;->i:LX/0Tn;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->d:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-interface {v2, v3, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/32V;->j:LX/0Tn;

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574480
    if-eqz v6, :cond_0

    .line 574481
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 574482
    :catch_0
    move-exception v2

    .line 574483
    :try_start_2
    sget-object v3, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b:Ljava/lang/Class;

    const-string v4, "Got exception when check contact id and phonebook version"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 574484
    if-eqz v6, :cond_0

    .line 574485
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 574486
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_4

    .line 574487
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private static c(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)Ljava/lang/Long;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 574453
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574454
    :goto_0
    return-object v0

    .line 574455
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 574456
    iget-object v2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->m:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 574457
    iget-object v1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, LX/2UP;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 574458
    :catch_0
    iget-object v1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/2UP;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 574459
    :cond_1
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->m:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 574460
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)LX/1ML;
    .locals 3

    .prologue
    .line 574430
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    .line 574431
    sget-object v1, LX/0ig;->p:LX/0ih;

    const/4 v2, 0x0

    .line 574432
    iput-boolean v2, v1, LX/0ih;->d:Z

    .line 574433
    move-object v1, v1

    .line 574434
    sget v2, LX/95L;->a:I

    .line 574435
    iput v2, v1, LX/0ih;->c:I

    .line 574436
    iget-object v1, v0, LX/2UP;->b:LX/0if;

    sget-object v2, LX/0ig;->p:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 574437
    const-string v0, "contacts_upload_friend_finder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574438
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95L;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2UP;->a(Ljava/lang/String;)V

    .line 574439
    :goto_0
    if-eqz p2, :cond_1

    .line 574440
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95L;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2UP;->a(Ljava/lang/String;)V

    .line 574441
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->k:LX/0Tn;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 574442
    new-instance v0, LX/95D;

    invoke-direct {v0}, LX/95D;-><init>()V

    move-object v0, v0

    .line 574443
    const-string v2, "client_hash"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/95D;

    .line 574444
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 574445
    iget-object v2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object p2, LX/95K;->SEND_ROOTHASH_TO_SERVER:LX/95K;

    invoke-virtual {v2, p2}, LX/2UP;->a(LX/95K;)V

    .line 574446
    iget-object v2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->j:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 574447
    new-instance v2, LX/952;

    invoke-direct {v2, p0, p1, v1}, LX/952;-><init>(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->h:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574448
    const/4 v0, 0x0

    .line 574449
    :goto_1
    return-object v0

    .line 574450
    :cond_0
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95L;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2UP;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 574451
    :cond_1
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95L;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2UP;->a(Ljava/lang/String;)V

    .line 574452
    invoke-static {p0, p1}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)LX/1ML;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 574393
    const v5, 0xae5d6da

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 574394
    if-eqz p1, :cond_0

    .line 574395
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 574396
    if-nez v0, :cond_6

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_a

    .line 574397
    const/4 v0, 0x0

    invoke-static {v0, v3}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 574398
    :goto_2
    move-object v0, v0

    .line 574399
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574400
    if-eqz v0, :cond_1

    .line 574401
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->j(II)I

    move-result v3

    iput v3, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->n:I

    .line 574402
    :cond_1
    const/4 v3, 0x1

    .line 574403
    if-eqz v0, :cond_c

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v4

    if-nez v4, :cond_c

    :goto_3
    move v3, v3

    .line 574404
    if-eqz v3, :cond_2

    .line 574405
    iget-object v4, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v5, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    sget-object v6, LX/95L;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/2UP;->a(LX/95K;Ljava/lang/String;)V

    .line 574406
    invoke-static {p0, v1, v0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;LX/15i;I)V

    .line 574407
    invoke-static {p0, p2}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)LX/1ML;

    move v4, v2

    .line 574408
    :goto_4
    iget-object v1, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    iget-wide v6, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->o:J

    invoke-static {p0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)J

    move-result-wide v8

    move-object v5, p3

    invoke-virtual/range {v1 .. v9}, LX/2UP;->a(ZZZLjava/lang/String;JJ)V

    .line 574409
    return-void

    .line 574410
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 574411
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 574412
    const/4 v4, 0x0

    .line 574413
    if-nez v0, :cond_3

    .line 574414
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    sget-object v5, LX/95L;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, LX/2UP;->a(LX/95K;Ljava/lang/String;)V

    .line 574415
    :goto_5
    invoke-static {p0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a$redex0(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)V

    .line 574416
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    invoke-virtual {v0}, LX/2UP;->b()V

    goto :goto_4

    .line 574417
    :cond_3
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    sget-object v5, LX/95L;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, LX/2UP;->a(LX/95K;Ljava/lang/String;)V

    goto :goto_5

    .line 574418
    :cond_4
    if-nez v0, :cond_5

    .line 574419
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    sget-object v4, LX/95L;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/2UP;->a(LX/95K;Ljava/lang/String;)V

    .line 574420
    :goto_6
    invoke-static {p0, p2}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;)LX/1ML;

    move v4, v2

    goto :goto_4

    .line 574421
    :cond_5
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    sget-object v4, LX/95L;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/2UP;->a(LX/95K;Ljava/lang/String;)V

    goto :goto_6

    .line 574422
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 574423
    check-cast v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 574424
    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto/16 :goto_0

    .line 574425
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 574426
    check-cast v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 574427
    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_7
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto/16 :goto_1

    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_7

    .line 574428
    :cond_a
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 574429
    check-cast v0, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v3, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_8
    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto/16 :goto_2

    :cond_b
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_8

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_3
.end method

.method public final a(Z)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 574389
    invoke-direct {p0, p1}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(Z)V

    .line 574390
    iget-object v0, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->i:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 574391
    iget-object v2, p0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/32V;->f:LX/0Tn;

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 574392
    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
