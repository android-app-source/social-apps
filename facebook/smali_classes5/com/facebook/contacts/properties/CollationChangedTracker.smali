.class public Lcom/facebook/contacts/properties/CollationChangedTracker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2It;

.field private final c:LX/2JB;

.field private final d:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571459
    const-class v0, Lcom/facebook/contacts/properties/CollationChangedTracker;

    sput-object v0, Lcom/facebook/contacts/properties/CollationChangedTracker;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2It;LX/2JB;LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 571460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571461
    iput-object p1, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->b:LX/2It;

    .line 571462
    iput-object p2, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->c:LX/2JB;

    .line 571463
    iput-object p3, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->d:LX/0aG;

    .line 571464
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    .line 571465
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 571466
    :cond_0
    :goto_0
    return-void

    .line 571467
    :cond_1
    iget-object v0, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->c:LX/2JB;

    invoke-virtual {v0}, LX/2JB;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 571468
    iget-object v0, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->b:LX/2It;

    sget-object v1, LX/3Fg;->g:LX/3OK;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/2Iu;->a(LX/0To;I)I

    move-result v0

    .line 571469
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-eq v1, v0, :cond_0

    .line 571470
    iget-object v0, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->d:LX/0aG;

    const-string v1, "reindex_omnistore_contacts"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const v3, -0x6e5587b6

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 571471
    iget-object v0, p0, Lcom/facebook/contacts/properties/CollationChangedTracker;->d:LX/0aG;

    const-string v1, "reindex_contacts_names"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x3557234d    # -5533273.5f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 571472
    new-instance v1, LX/6Ns;

    invoke-direct {v1, p0}, LX/6Ns;-><init>(Lcom/facebook/contacts/properties/CollationChangedTracker;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
