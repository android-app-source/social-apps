.class public Lcom/facebook/contacts/service/ContactsServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/contacts/service/ContactsServiceHandler;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final w:Ljava/lang/Object;


# instance fields
.field public final c:LX/3fX;

.field public final d:LX/3Ku;

.field public final e:LX/3fY;

.field public final f:LX/3fZ;

.field public final g:LX/3fa;

.field public final h:LX/3ff;

.field public final i:LX/2It;

.field public final j:LX/3fg;

.field public final k:LX/3fk;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/11H;

.field public final n:LX/0Xl;

.field public final o:LX/0SG;

.field private final p:LX/3fl;

.field private final q:LX/2JB;

.field public final r:LX/3fq;

.field public final s:LX/3ft;

.field public final t:LX/3fv;

.field public final u:LX/3fy;

.field public final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622325
    const-class v0, Lcom/facebook/contacts/service/ContactsServiceHandler;

    .line 622326
    sput-object v0, Lcom/facebook/contacts/service/ContactsServiceHandler;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contacts/service/ContactsServiceHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 622327
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/contacts/service/ContactsServiceHandler;->w:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fX;LX/3Ku;LX/3fY;LX/3fZ;LX/3fa;LX/3ff;LX/2It;LX/3fg;LX/3fk;LX/0Ot;LX/11H;LX/0Xl;LX/0SG;LX/3fl;LX/2JB;LX/3fq;LX/3ft;LX/3fv;LX/3fy;LX/0Or;)V
    .locals 1
    .param p12    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/service/IsFavoritePollingFrequencyReduced;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fX;",
            "LX/3Ku;",
            "LX/3fY;",
            "LX/3fZ;",
            "LX/3fa;",
            "LX/3ff;",
            "LX/2It;",
            "LX/3fg;",
            "LX/3fk;",
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0Xl;",
            "LX/0SG;",
            "LX/3fl;",
            "LX/2JB;",
            "LX/3fq;",
            "LX/3ft;",
            "LX/3fv;",
            "LX/3fy;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622329
    iput-object p1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->c:LX/3fX;

    .line 622330
    iput-object p2, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->d:LX/3Ku;

    .line 622331
    iput-object p3, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->e:LX/3fY;

    .line 622332
    iput-object p4, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->f:LX/3fZ;

    .line 622333
    iput-object p5, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->g:LX/3fa;

    .line 622334
    iput-object p6, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->h:LX/3ff;

    .line 622335
    iput-object p7, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->i:LX/2It;

    .line 622336
    iput-object p8, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->j:LX/3fg;

    .line 622337
    iput-object p9, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->k:LX/3fk;

    .line 622338
    iput-object p10, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->l:LX/0Ot;

    .line 622339
    iput-object p11, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->m:LX/11H;

    .line 622340
    iput-object p12, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->n:LX/0Xl;

    .line 622341
    iput-object p13, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->o:LX/0SG;

    .line 622342
    iput-object p14, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->p:LX/3fl;

    .line 622343
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->q:LX/2JB;

    .line 622344
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->r:LX/3fq;

    .line 622345
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->s:LX/3ft;

    .line 622346
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->t:LX/3fv;

    .line 622347
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->u:LX/3fy;

    .line 622348
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->v:LX/0Or;

    .line 622349
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;
    .locals 7

    .prologue
    .line 622350
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 622351
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 622352
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 622353
    if-nez v1, :cond_0

    .line 622354
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622355
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 622356
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 622357
    sget-object v1, Lcom/facebook/contacts/service/ContactsServiceHandler;->w:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 622358
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 622359
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 622360
    :cond_1
    if-nez v1, :cond_4

    .line 622361
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 622362
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 622363
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/contacts/service/ContactsServiceHandler;->b(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 622364
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 622365
    if-nez v1, :cond_2

    .line 622366
    sget-object v0, Lcom/facebook/contacts/service/ContactsServiceHandler;->w:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/service/ContactsServiceHandler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 622367
    :goto_1
    if-eqz v0, :cond_3

    .line 622368
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 622369
    :goto_3
    check-cast v0, Lcom/facebook/contacts/service/ContactsServiceHandler;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 622370
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 622371
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 622372
    :catchall_1
    move-exception v0

    .line 622373
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 622374
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 622375
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 622376
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/contacts/service/ContactsServiceHandler;->w:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/service/ContactsServiceHandler;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)Lcom/facebook/contacts/service/ContactsServiceHandler;
    .locals 23

    .prologue
    .line 622377
    new-instance v2, Lcom/facebook/contacts/service/ContactsServiceHandler;

    invoke-static/range {p0 .. p0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v3

    check-cast v3, LX/3fX;

    invoke-static/range {p0 .. p0}, LX/3Ku;->a(LX/0QB;)LX/3Ku;

    move-result-object v4

    check-cast v4, LX/3Ku;

    invoke-static/range {p0 .. p0}, LX/3fY;->a(LX/0QB;)LX/3fY;

    move-result-object v5

    check-cast v5, LX/3fY;

    invoke-static/range {p0 .. p0}, LX/3fZ;->a(LX/0QB;)LX/3fZ;

    move-result-object v6

    check-cast v6, LX/3fZ;

    invoke-static/range {p0 .. p0}, LX/3fa;->a(LX/0QB;)LX/3fa;

    move-result-object v7

    check-cast v7, LX/3fa;

    invoke-static/range {p0 .. p0}, LX/3ff;->a(LX/0QB;)LX/3ff;

    move-result-object v8

    check-cast v8, LX/3ff;

    invoke-static/range {p0 .. p0}, LX/2It;->a(LX/0QB;)LX/2It;

    move-result-object v9

    check-cast v9, LX/2It;

    invoke-static/range {p0 .. p0}, LX/3fg;->a(LX/0QB;)LX/3fg;

    move-result-object v10

    check-cast v10, LX/3fg;

    invoke-static/range {p0 .. p0}, LX/3fk;->a(LX/0QB;)LX/3fk;

    move-result-object v11

    check-cast v11, LX/3fk;

    const/16 v12, 0x1a18

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v13

    check-cast v13, LX/11H;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v14

    check-cast v14, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v15

    check-cast v15, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/3fl;->a(LX/0QB;)LX/3fl;

    move-result-object v16

    check-cast v16, LX/3fl;

    invoke-static/range {p0 .. p0}, LX/2JB;->a(LX/0QB;)LX/2JB;

    move-result-object v17

    check-cast v17, LX/2JB;

    invoke-static/range {p0 .. p0}, LX/3fq;->a(LX/0QB;)LX/3fq;

    move-result-object v18

    check-cast v18, LX/3fq;

    invoke-static/range {p0 .. p0}, LX/3ft;->a(LX/0QB;)LX/3ft;

    move-result-object v19

    check-cast v19, LX/3ft;

    invoke-static/range {p0 .. p0}, LX/3fv;->a(LX/0QB;)LX/3fv;

    move-result-object v20

    check-cast v20, LX/3fv;

    invoke-static/range {p0 .. p0}, LX/3fy;->a(LX/0QB;)LX/3fy;

    move-result-object v21

    check-cast v21, LX/3fy;

    const/16 v22, 0x147b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, Lcom/facebook/contacts/service/ContactsServiceHandler;-><init>(LX/3fX;LX/3Ku;LX/3fY;LX/3fZ;LX/3fa;LX/3ff;LX/2It;LX/3fg;LX/3fk;LX/0Ot;LX/11H;LX/0Xl;LX/0SG;LX/3fl;LX/2JB;LX/3fq;LX/3ft;LX/3fv;LX/3fy;LX/0Or;)V

    .line 622378
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 622379
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 622380
    const-string v1, "fetch_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 622381
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622382
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 622383
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->r:LX/3fq;

    const-string v2, "fetchMultipleContactsParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;

    invoke-virtual {v1, v0}, LX/3fq;->a(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 622384
    :goto_0
    return-object v0

    .line 622385
    :cond_0
    const-string v1, "sync_contacts_partial"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 622386
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->p:LX/3fl;

    .line 622387
    iget-object v1, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 622388
    invoke-virtual {v0, v1}, LX/3fl;->a(Lcom/facebook/common/callercontext/CallerContext;)V

    .line 622389
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622390
    goto :goto_0

    .line 622391
    :cond_1
    const-string v1, "delete_contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 622392
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->s:LX/3ft;

    .line 622393
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622394
    const-string v2, "deleteContactParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/DeleteContactParams;

    .line 622395
    iget-object v2, v1, LX/3ft;->a:LX/3fX;

    iget-object v3, v0, Lcom/facebook/contacts/server/DeleteContactParams;->a:Lcom/facebook/contacts/graphql/Contact;

    invoke-static {v3}, LX/6Ms;->a(Lcom/facebook/contacts/graphql/Contact;)LX/0Py;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3fX;->a(Ljava/lang/Iterable;)V

    .line 622396
    iget-object v2, v0, Lcom/facebook/contacts/server/DeleteContactParams;->a:Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v3

    .line 622397
    sget-object p0, LX/6Mu;->a:[I

    iget-object v2, v1, LX/3ft;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Jp;

    invoke-virtual {v2}, LX/2Jp;->ordinal()I

    move-result v2

    aget v2, p0, v2

    packed-switch v2, :pswitch_data_0

    .line 622398
    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.contacts.ACTION_CONTACT_DELETED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 622399
    iget-object v3, v1, LX/3ft;->b:LX/0Xl;

    invoke-interface {v3, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 622400
    iget-object v2, v1, LX/3ft;->c:LX/11H;

    iget-object v3, v1, LX/3ft;->d:LX/3fu;

    invoke-virtual {v2, v3, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622401
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622402
    goto :goto_0

    .line 622403
    :cond_2
    const-string v1, "sync_favorite_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 622404
    const-wide/16 v8, -0x1

    .line 622405
    iget-object v4, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->i:LX/2It;

    sget-object v5, LX/3Fg;->f:LX/3OK;

    invoke-virtual {v4, v5, v8, v9}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v6

    .line 622406
    iget-object v4, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->v:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_10

    const-wide/32 v4, 0x5265c00

    .line 622407
    :goto_2
    cmp-long v8, v6, v8

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->o:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    sub-long v6, v8, v6

    cmp-long v4, v6, v4

    if-gez v4, :cond_11

    .line 622408
    :goto_3
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622409
    goto/16 :goto_0

    .line 622410
    :cond_3
    const-string v1, "update_favorite_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 622411
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622412
    const-string v1, "favorites"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    .line 622413
    iget-object v1, v0, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;->a:LX/0Px;

    move-object v1, v1

    .line 622414
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 622415
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 622416
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->b()Z

    move-result v4

    if-nez v4, :cond_4

    .line 622417
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 622418
    :cond_5
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->m:LX/11H;

    iget-object v3, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->f:LX/3fZ;

    new-instance v4, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/facebook/contacts/server/UpdateFavoriteContactsParams;-><init>(Ljava/util/List;)V

    sget-object v2, Lcom/facebook/contacts/service/ContactsServiceHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3, v4, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 622419
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->k:LX/3fk;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/3fk;->a(Ljava/util/Collection;Z)V

    .line 622420
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->d:LX/3Ku;

    invoke-virtual {v0}, LX/3Ku;->a()V

    .line 622421
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622422
    goto/16 :goto_0

    .line 622423
    :cond_6
    const-string v1, "add_contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 622424
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622425
    const-string v1, "addContactParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/AddContactParams;

    .line 622426
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->t:LX/3fv;

    invoke-virtual {v1, v0}, LX/3fv;->a(Lcom/facebook/contacts/server/AddContactParams;)Lcom/facebook/contacts/server/AddContactResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 622427
    goto/16 :goto_0

    .line 622428
    :cond_7
    const-string v1, "update_contact_is_messenger_user"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 622429
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622430
    const-string v1, "updateIsMessengerUserParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;

    .line 622431
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->u:LX/3fy;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 622432
    iget-object v2, v1, LX/3fy;->d:LX/3fq;

    new-instance v3, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;

    iget-object p0, v0, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;->a:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object p0

    invoke-static {p0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object p0

    sget-object p1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v3, p0, p1}, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;-><init>(LX/0Rf;LX/0rS;)V

    invoke-virtual {v2, v3}, LX/3fq;->a(Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;)Lcom/facebook/contacts/server/FetchContactsResult;

    move-result-object v3

    .line 622433
    iget-object v2, v3, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v2, v2

    .line 622434
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_12

    move v2, v4

    :goto_5
    const-string p0, "Tried to update isMessengerUser on user with unavailable Contact data"

    invoke-static {v2, p0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 622435
    iget-object v2, v3, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v2, v2

    .line 622436
    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/contacts/graphql/Contact;

    .line 622437
    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result p0

    iget-boolean p1, v0, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;->b:Z

    if-eq p0, p1, :cond_8

    .line 622438
    iget-object p0, v3, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, p0

    .line 622439
    sget-object p0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v3, p0, :cond_13

    .line 622440
    :cond_8
    :goto_6
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622441
    goto/16 :goto_0

    .line 622442
    :cond_9
    const-string v1, "mark_full_contact_sync_required"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 622443
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->q:LX/2JB;

    const-wide/16 v6, -0x1

    .line 622444
    iget-object v4, v0, LX/2JB;->b:LX/2It;

    sget-object v5, LX/3Fg;->b:LX/3OK;

    invoke-virtual {v4, v5, v6, v7}, LX/2Iu;->b(LX/0To;J)V

    .line 622445
    iget-object v4, v0, LX/2JB;->b:LX/2It;

    sget-object v5, LX/3Fg;->a:LX/3OK;

    invoke-virtual {v4, v5, v6, v7}, LX/2Iu;->b(LX/0To;J)V

    .line 622446
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622447
    goto/16 :goto_0

    .line 622448
    :cond_a
    const-string v1, "reindex_contacts_names"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 622449
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->j:LX/3fg;

    invoke-virtual {v0}, LX/3fg;->a()V

    .line 622450
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->c:LX/3fX;

    invoke-virtual {v0}, LX/3fX;->a()V

    .line 622451
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622452
    goto/16 :goto_0

    .line 622453
    :cond_b
    const-string v1, "update_contacts_coefficient"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 622454
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->p:LX/3fl;

    invoke-virtual {v0}, LX/3fl;->a()LX/0Px;

    .line 622455
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622456
    goto/16 :goto_0

    .line 622457
    :cond_c
    const-string v1, "fetch_payment_eligible_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 622458
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622459
    const-string v1, "fetchPaymentEligibleContactsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchPaymentEligibleContactsParams;

    .line 622460
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->m:LX/11H;

    iget-object v2, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->g:LX/3fa;

    sget-object v3, Lcom/facebook/contacts/service/ContactsServiceHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 622461
    goto/16 :goto_0

    .line 622462
    :cond_d
    const-string v1, "fetch_top_contacts_by_cfphat_coefficient"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 622463
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 622464
    const-string v1, "fetchTopContactsByCfphatParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 622465
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->m:LX/11H;

    iget-object v2, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->h:LX/3ff;

    sget-object v3, Lcom/facebook/contacts/service/ContactsServiceHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchContactsResult;

    .line 622466
    iget-object v1, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->j:LX/3fg;

    .line 622467
    iget-object v2, v0, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v2, v2

    .line 622468
    sget-object v3, LX/3gm;->INSERT:LX/3gm;

    .line 622469
    iget-object v4, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 622470
    invoke-virtual {v1, v2, v3, v4}, LX/3fg;->a(LX/0Py;LX/3gm;LX/0ta;)V

    .line 622471
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 622472
    goto/16 :goto_0

    .line 622473
    :cond_e
    const-string v1, "reindex_omnistore_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 622474
    iget-object v0, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Nd;

    .line 622475
    iget-object v1, v0, LX/6Nd;->a:LX/6NX;

    sget-object v2, LX/6Nb;->REINDEX_COLLECION:LX/6Nb;

    invoke-virtual {v1, v2}, LX/6NX;->a(LX/6Nb;)Lcom/facebook/omnistore/Collection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/omnistore/Collection;->reindexAllObjects()V

    .line 622476
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 622477
    goto/16 :goto_0

    .line 622478
    :cond_f
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 622479
    :pswitch_0
    iget-object v2, v1, LX/3ft;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3fg;

    .line 622480
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/3fg;->a(LX/0Py;)V

    .line 622481
    goto/16 :goto_1

    .line 622482
    :pswitch_1
    iget-object v2, v1, LX/3ft;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Nd;

    .line 622483
    iget-object p0, v2, LX/6Nd;->a:LX/6NX;

    sget-object p1, LX/6Nb;->DELETE_CONTACT:LX/6Nb;

    invoke-virtual {p0, p1}, LX/6NX;->a(LX/6Nb;)Lcom/facebook/omnistore/Collection;

    move-result-object p0

    invoke-virtual {p0, v3}, Lcom/facebook/omnistore/Collection;->deleteObject(Ljava/lang/String;)V

    .line 622484
    goto/16 :goto_1

    .line 622485
    :cond_10
    const-wide/32 v4, 0xa4cb80

    goto/16 :goto_2

    .line 622486
    :cond_11
    iget-object v4, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->m:LX/11H;

    iget-object v5, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->e:LX/3fY;

    const/4 v6, 0x0

    sget-object v7, Lcom/facebook/contacts/service/ContactsServiceHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v6, v7}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/contacts/server/FetchMessagingFavoritesResult;

    .line 622487
    iget-object v5, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->k:LX/3fk;

    .line 622488
    iget-object v6, v4, Lcom/facebook/contacts/server/FetchMessagingFavoritesResult;->a:LX/0Px;

    move-object v4, v6

    .line 622489
    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, LX/3fk;->a(Ljava/util/Collection;Z)V

    .line 622490
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.facebook.contacts.FAVORITE_CONTACT_SYNC_PROGRESS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 622491
    iget-object v5, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->n:LX/0Xl;

    invoke-interface {v5, v4}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 622492
    iget-object v4, p0, Lcom/facebook/contacts/service/ContactsServiceHandler;->d:LX/3Ku;

    invoke-virtual {v4}, LX/3Ku;->a()V

    goto/16 :goto_3

    :cond_12
    move v2, v5

    .line 622493
    goto/16 :goto_5

    .line 622494
    :cond_13
    new-instance v3, LX/3hB;

    invoke-direct {v3, v2}, LX/3hB;-><init>(Lcom/facebook/contacts/graphql/Contact;)V

    iget-boolean p0, v0, Lcom/facebook/contacts/server/UpdateContactIsMessengerUserParams;->b:Z

    .line 622495
    iput-boolean p0, v3, LX/3hB;->w:Z

    .line 622496
    move-object v3, v3

    .line 622497
    invoke-virtual {v3}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object p0

    .line 622498
    sget-object p1, LX/6Mx;->a:[I

    iget-object v3, v1, LX/3fy;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Jp;

    invoke-virtual {v3}, LX/2Jp;->ordinal()I

    move-result v3

    aget v3, p1, v3

    packed-switch v3, :pswitch_data_1

    .line 622499
    :goto_7
    iget-object v3, v1, LX/3fy;->c:LX/3fX;

    invoke-virtual {v3, p0}, LX/3fX;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 622500
    iget-object v3, v1, LX/3fy;->b:LX/3fz;

    new-instance p1, Lcom/facebook/contacts/protocol/push/ContactsMessengerUserMap;

    new-array v4, v4, [Lcom/facebook/contacts/graphql/Contact;

    aput-object p0, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p1, v4}, Lcom/facebook/contacts/protocol/push/ContactsMessengerUserMap;-><init>(Ljava/util/List;)V

    .line 622501
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.facebook.presence.ACTION_PUSH_STATE_RECEIVED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 622502
    const-string v5, "extra_on_messenger_map"

    invoke-virtual {v4, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 622503
    iget-object v5, v3, LX/3fz;->a:LX/0Xl;

    invoke-interface {v5, v4}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 622504
    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto/16 :goto_6

    .line 622505
    :pswitch_2
    iget-object v3, v1, LX/3fy;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3fg;

    sget-object p1, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    invoke-virtual {v3, p0, p1}, LX/3fg;->a(Lcom/facebook/contacts/graphql/Contact;LX/0ta;)J

    goto :goto_7

    .line 622506
    :pswitch_3
    iget-object v3, v1, LX/3fy;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6Nd;

    invoke-virtual {v3, p0}, LX/6Nd;->a(Lcom/facebook/contacts/graphql/Contact;)V

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
