.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xfd60e68
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 624908
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 624919
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 624917
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 624918
    return-void
.end method

.method private j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 624915
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    .line 624916
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 624909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 624910
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 624911
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 624912
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 624913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 624914
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 624920
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 624921
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 624922
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    .line 624923
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 624924
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;

    .line 624925
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->e:Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    .line 624926
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 624927
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 624902
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;->j()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel$MessengerContactsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 624903
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsFullQueryModel;-><init>()V

    .line 624904
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 624905
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 624906
    const v0, -0x38573e42

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 624907
    const v0, -0x6747e1ce

    return v0
.end method
