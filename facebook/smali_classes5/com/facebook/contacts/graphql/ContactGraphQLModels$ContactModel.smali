.class public final Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x357d22c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 625617
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 625618
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 625619
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 625620
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 625621
    iput-boolean p1, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k:Z

    .line 625622
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 625623
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 625624
    if-eqz v0, :cond_0

    .line 625625
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 625626
    :cond_0
    return-void
.end method

.method private q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneticName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625627
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->m:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->m:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625628
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->m:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    return-object v0
.end method

.method private r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRepresentedProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625629
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->n:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->n:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    .line 625630
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->n:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    return-object v0
.end method

.method private s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625721
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->p:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->p:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625722
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->p:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 625631
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 625632
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x4309df98

    invoke-static {v3, v2, v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 625633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 625634
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x4309df98

    invoke-static {v3, v2, v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 625635
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 625636
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k()LX/2uF;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v12

    .line 625637
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 625638
    invoke-direct/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 625639
    invoke-direct/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 625640
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x4309df98

    invoke-static {v3, v2, v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 625641
    invoke-direct/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 625642
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dA_()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 625643
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 625644
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->e:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 625645
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 625646
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 625647
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 625648
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 625649
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 625650
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 625651
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 625652
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 625653
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 625654
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625655
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625656
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625657
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 625658
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const v5, -0x4309df98

    .line 625659
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 625660
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 625661
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v2, v0, v5}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 625662
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 625663
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625664
    iput v3, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->f:I

    move-object v1, v0

    .line 625665
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 625666
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v2, v0, v5}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 625667
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 625668
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625669
    iput v3, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->h:I

    move-object v1, v0

    .line 625670
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 625671
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 625672
    if-eqz v2, :cond_2

    .line 625673
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625674
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j:LX/3Sb;

    move-object v1, v0

    .line 625675
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 625676
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 625677
    if-eqz v2, :cond_3

    .line 625678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625679
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l:Ljava/util/List;

    move-object v1, v0

    .line 625680
    :cond_3
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 625681
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625682
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 625683
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625684
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->m:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625685
    :cond_4
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 625686
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    .line 625687
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 625688
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625689
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->n:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    .line 625690
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 625691
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v2, v0, v5}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/graphql/ContactGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 625692
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 625693
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625694
    iput v3, v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o:I

    move-object v1, v0

    .line 625695
    :cond_6
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 625696
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625697
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 625698
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    .line 625699
    iput-object v0, v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->p:Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    .line 625700
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 625701
    if-nez v1, :cond_8

    :goto_0
    return-object p0

    .line 625702
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 625703
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 625704
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_8
    move-object p0, v1

    .line 625705
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 625706
    new-instance v0, LX/6MN;

    invoke-direct {v0, p1}, LX/6MN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625707
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const v4, -0x4309df98

    .line 625708
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 625709
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->e:J

    .line 625710
    const/4 v0, 0x1

    invoke-static {p1, p2, v0, v4}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->f:I

    .line 625711
    const/4 v0, 0x3

    invoke-static {p1, p2, v0, v4}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->h:I

    .line 625712
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k:Z

    .line 625713
    const/16 v0, 0xa

    invoke-static {p1, p2, v0, v4}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o:I

    .line 625714
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 625715
    const-string v0, "is_on_viewer_contact_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625716
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->e()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 625717
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 625718
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    .line 625719
    :goto_0
    return-void

    .line 625720
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 625612
    const-string v0, "is_on_viewer_contact_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625613
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->a(Z)V

    .line 625614
    :cond_0
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 625615
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 625616
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->e:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 625586
    new-instance v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    invoke-direct {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;-><init>()V

    .line 625587
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 625588
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625589
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->g:Ljava/lang/String;

    .line 625590
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625591
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->i:Ljava/lang/String;

    .line 625592
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final dA_()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625593
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 625594
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 625595
    const v0, -0x44500ed8

    return v0
.end method

.method public final dz_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBigPictureUrl"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625596
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 625597
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 625598
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 625599
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 625600
    const v0, -0x64104400

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHugePictureUrl"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625601
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 625602
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImportedPhoneEntries"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 625603
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x5

    const v4, -0x20741a65

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j:LX/3Sb;

    .line 625604
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNameEntries"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 625605
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l:Ljava/util/List;

    .line 625606
    iget-object v0, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneticName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625607
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->q()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRepresentedProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625608
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->r()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSmallPictureUrl"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625609
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 625610
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic p()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625611
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->s()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v0

    return-object v0
.end method
