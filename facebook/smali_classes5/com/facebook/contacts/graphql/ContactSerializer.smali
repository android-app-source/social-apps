.class public Lcom/facebook/contacts/graphql/ContactSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 626558
    const-class v0, Lcom/facebook/contacts/graphql/Contact;

    new-instance v1, Lcom/facebook/contacts/graphql/ContactSerializer;

    invoke-direct {v1}, Lcom/facebook/contacts/graphql/ContactSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 626559
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 626560
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/graphql/Contact;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 626561
    if-nez p0, :cond_0

    .line 626562
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 626563
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 626564
    invoke-static {p0, p1, p2}, Lcom/facebook/contacts/graphql/ContactSerializer;->b(Lcom/facebook/contacts/graphql/Contact;LX/0nX;LX/0my;)V

    .line 626565
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 626566
    return-void
.end method

.method private static b(Lcom/facebook/contacts/graphql/Contact;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 626567
    const-string v0, "contactId"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626568
    const-string v0, "profileFbid"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626569
    const-string v0, "graphApiWriteId"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626570
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626571
    const-string v0, "phoneticName"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626572
    const-string v0, "smallPictureUrl"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626573
    const-string v0, "bigPictureUrl"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626574
    const-string v0, "hugePictureUrl"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626575
    const-string v0, "smallPictureSize"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626576
    const-string v0, "bigPictureSize"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626577
    const-string v0, "hugePictureSize"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626578
    const-string v0, "communicationRank"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 626579
    const-string v0, "withTaggingRank"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 626580
    const-string v0, "phones"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 626581
    const-string v0, "nameSearchTokens"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 626582
    const-string v0, "isMessageBlockedByViewer"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626583
    const-string v0, "canMessage"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626584
    const-string v0, "isMobilePushable"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626585
    const-string v0, "isMessengerUser"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626586
    const-string v0, "messengerInstallTime"

    iget-wide v2, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 626587
    const-string v0, "isMemorialized"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626588
    const-string v0, "isOnViewerContactList"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626589
    const-string v0, "addedTime"

    iget-wide v2, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 626590
    const-string v0, "friendshipStatus"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626591
    const-string v0, "subscribeStatus"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626592
    const-string v0, "contactType"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626593
    const-string v0, "nameEntries"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 626594
    const-string v0, "birthdayDay"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626595
    const-string v0, "birthdayMonth"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626596
    const-string v0, "cityName"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626597
    const-string v0, "isPartial"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626598
    const-string v0, "lastFetchTime"

    iget-wide v2, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 626599
    const-string v0, "montageThreadFBID"

    iget-wide v2, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 626600
    const-string v0, "canSeeViewerMontageThread"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626601
    const-string v0, "phatRank"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 626602
    const-string v0, "username"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 626603
    const-string v0, "messengerInvitePriority"

    iget v1, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 626604
    const-string v0, "canViewerSendMoney"

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 626605
    const-string v0, "viewerConnectionStatus"

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 626606
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 626607
    check-cast p1, Lcom/facebook/contacts/graphql/Contact;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/graphql/ContactSerializer;->a(Lcom/facebook/contacts/graphql/Contact;LX/0nX;LX/0my;)V

    return-void
.end method
