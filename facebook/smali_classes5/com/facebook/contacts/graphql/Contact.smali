.class public Lcom/facebook/contacts/graphql/Contact;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/0Pm;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/graphql/ContactDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/graphql/ContactSerializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mAddedTimeInMS:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "addedTime"
    .end annotation
.end field

.field public final mBigPictureSize:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bigPictureSize"
    .end annotation
.end field

.field public final mBigPictureUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bigPictureUrl"
    .end annotation
.end field

.field public final mBirthdayDay:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthdayDay"
    .end annotation
.end field

.field public final mBirthdayMonth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "birthdayMonth"
    .end annotation
.end field

.field public final mCanMessage:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canMessage"
    .end annotation
.end field

.field public final mCanSeeViewerMontageThread:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canSeeViewerMontageThread"
    .end annotation
.end field

.field public final mCanViewerSendMoney:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "canViewerSendMoney"
    .end annotation
.end field

.field public final mCityName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cityName"
    .end annotation
.end field

.field public final mCommunicationRank:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "communicationRank"
    .end annotation
.end field

.field public final mContactId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contactId"
    .end annotation
.end field

.field public final mContactProfileType:LX/2RU;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "contactType"
    .end annotation
.end field

.field public final mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendshipStatus"
    .end annotation
.end field

.field public final mGraphApiWriteId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "graphApiWriteId"
    .end annotation
.end field

.field public final mHugePictureSize:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hugePictureSize"
    .end annotation
.end field

.field public final mHugePictureUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hugePictureUrl"
    .end annotation
.end field

.field public final mIsMemorialized:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isMemorialized"
    .end annotation
.end field

.field public final mIsMessageBlockedByViewer:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isMessageBlockedByViewer"
    .end annotation
.end field

.field public final mIsMessengerUser:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isMessengerUser"
    .end annotation
.end field

.field public final mIsMobilePushable:LX/03R;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isMobilePushable"
    .end annotation
.end field

.field public final mIsOnViewerContactList:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isOnViewerContactList"
    .end annotation
.end field

.field public final mIsPartial:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isPartial"
    .end annotation
.end field

.field public final mLastFetchTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "lastFetchTime"
    .end annotation
.end field

.field public final mMessengerInstallTimeInMS:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "messengerInstallTime"
    .end annotation
.end field

.field public final mMessengerInvitePriority:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "messengerInvitePriority"
    .end annotation
.end field

.field public final mMontageThreadFBID:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "montageThreadFBID"
    .end annotation
.end field

.field public final mName:Lcom/facebook/user/model/Name;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final mNameEntries:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nameEntries"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;"
        }
    .end annotation
.end field

.field public final mNameSearchTokens:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nameSearchTokens"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mPhatRank:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phatRank"
    .end annotation
.end field

.field public final mPhones:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phones"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;"
        }
    .end annotation
.end field

.field public final mPhoneticName:Lcom/facebook/user/model/Name;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "phoneticName"
    .end annotation
.end field

.field public final mProfileFbid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profileFbid"
    .end annotation
.end field

.field public final mSmallPictureSize:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "smallPictureSize"
    .end annotation
.end field

.field public final mSmallPictureUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "smallPictureUrl"
    .end annotation
.end field

.field public final mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subscribeStatus"
    .end annotation
.end field

.field public final mUsername:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "username"
    .end annotation
.end field

.field public final mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewerConnectionStatus"
    .end annotation
.end field

.field public final mWithTaggingRank:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "withTaggingRank"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 625818
    const-class v0, Lcom/facebook/contacts/graphql/ContactDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 625819
    const-class v0, Lcom/facebook/contacts/graphql/ContactSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 625820
    new-instance v0, LX/3hA;

    invoke-direct {v0}, LX/3hA;-><init>()V

    sput-object v0, Lcom/facebook/contacts/graphql/Contact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v0, -0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 625821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625822
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    .line 625823
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    .line 625824
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    .line 625825
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    .line 625826
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    .line 625827
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    .line 625828
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    .line 625829
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    .line 625830
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    .line 625831
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    .line 625832
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    .line 625833
    iput v3, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    .line 625834
    iput v3, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    .line 625835
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    .line 625836
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    .line 625837
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    .line 625838
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    .line 625839
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    .line 625840
    iput-wide v4, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    .line 625841
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    .line 625842
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    .line 625843
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 625844
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 625845
    sget-object v0, LX/2RU;->UNMATCHED:LX/2RU;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    .line 625846
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    .line 625847
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    .line 625848
    iput-wide v4, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    .line 625849
    iput v2, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    .line 625850
    iput v2, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    .line 625851
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    .line 625852
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    .line 625853
    iput-wide v4, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    .line 625854
    iput-wide v4, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    .line 625855
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    .line 625856
    iput v3, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    .line 625857
    iput-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    .line 625858
    iput v3, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    .line 625859
    iput-boolean v2, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    .line 625860
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 625861
    return-void
.end method

.method public constructor <init>(LX/3hB;)V
    .locals 4

    .prologue
    .line 625862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625863
    iget-object v0, p1, LX/3hB;->a:Ljava/lang/String;

    move-object v0, v0

    .line 625864
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    .line 625865
    iget-object v0, p1, LX/3hB;->b:Ljava/lang/String;

    move-object v0, v0

    .line 625866
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    .line 625867
    iget-object v0, p1, LX/3hB;->c:Ljava/lang/String;

    move-object v0, v0

    .line 625868
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    .line 625869
    invoke-static {p1}, Lcom/facebook/contacts/graphql/Contact;->a(LX/3hB;)Lcom/facebook/user/model/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    .line 625870
    iget-object v0, p1, LX/3hB;->h:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 625871
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    .line 625872
    iget-object v0, p1, LX/3hB;->i:Ljava/lang/String;

    move-object v0, v0

    .line 625873
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    .line 625874
    iget-object v0, p1, LX/3hB;->j:Ljava/lang/String;

    move-object v0, v0

    .line 625875
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    .line 625876
    iget-object v0, p1, LX/3hB;->k:Ljava/lang/String;

    move-object v0, v0

    .line 625877
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    .line 625878
    iget v0, p1, LX/3hB;->l:I

    move v0, v0

    .line 625879
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    .line 625880
    iget v0, p1, LX/3hB;->m:I

    move v0, v0

    .line 625881
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    .line 625882
    iget v0, p1, LX/3hB;->n:I

    move v0, v0

    .line 625883
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    .line 625884
    iget v0, p1, LX/3hB;->o:F

    move v0, v0

    .line 625885
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    .line 625886
    iget v0, p1, LX/3hB;->p:F

    move v0, v0

    .line 625887
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    .line 625888
    iget-object v0, p1, LX/3hB;->q:LX/0Px;

    move-object v0, v0

    .line 625889
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    .line 625890
    iget-boolean v0, p1, LX/3hB;->r:Z

    move v0, v0

    .line 625891
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    .line 625892
    iget-boolean v0, p1, LX/3hB;->s:Z

    move v0, v0

    .line 625893
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    .line 625894
    iget-object v0, p1, LX/3hB;->v:LX/03R;

    move-object v0, v0

    .line 625895
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    .line 625896
    iget-boolean v0, p1, LX/3hB;->w:Z

    move v0, v0

    .line 625897
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    .line 625898
    iget-wide v2, p1, LX/3hB;->x:J

    move-wide v0, v2

    .line 625899
    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    .line 625900
    iget-boolean v0, p1, LX/3hB;->y:Z

    move v0, v0

    .line 625901
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    .line 625902
    iget-boolean v0, p1, LX/3hB;->z:Z

    move v0, v0

    .line 625903
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    .line 625904
    iget-object v0, p1, LX/3hB;->t:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 625905
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 625906
    iget-object v0, p1, LX/3hB;->u:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v0, v0

    .line 625907
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 625908
    iget-object v0, p1, LX/3hB;->A:LX/2RU;

    move-object v0, v0

    .line 625909
    if-eqz v0, :cond_0

    .line 625910
    iget-object v0, p1, LX/3hB;->A:LX/2RU;

    move-object v0, v0

    .line 625911
    :goto_0
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    .line 625912
    iget-object v0, p1, LX/3hB;->B:LX/0Px;

    move-object v0, v0

    .line 625913
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    .line 625914
    iget-object v0, p1, LX/3hB;->C:LX/0Px;

    move-object v0, v0

    .line 625915
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    .line 625916
    iget-wide v2, p1, LX/3hB;->D:J

    move-wide v0, v2

    .line 625917
    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    .line 625918
    iget v0, p1, LX/3hB;->F:I

    move v0, v0

    .line 625919
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    .line 625920
    iget v0, p1, LX/3hB;->E:I

    move v0, v0

    .line 625921
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    .line 625922
    iget-object v0, p1, LX/3hB;->G:Ljava/lang/String;

    move-object v0, v0

    .line 625923
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    .line 625924
    iget-boolean v0, p1, LX/3hB;->H:Z

    move v0, v0

    .line 625925
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    .line 625926
    iget-wide v2, p1, LX/3hB;->I:J

    move-wide v0, v2

    .line 625927
    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    .line 625928
    iget-wide v2, p1, LX/3hB;->J:J

    move-wide v0, v2

    .line 625929
    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    .line 625930
    iget-boolean v0, p1, LX/3hB;->K:Z

    move v0, v0

    .line 625931
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    .line 625932
    iget v0, p1, LX/3hB;->L:F

    move v0, v0

    .line 625933
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    .line 625934
    iget-object v0, p1, LX/3hB;->M:Ljava/lang/String;

    move-object v0, v0

    .line 625935
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    .line 625936
    iget v0, p1, LX/3hB;->N:F

    move v0, v0

    .line 625937
    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    .line 625938
    iget-boolean v0, p1, LX/3hB;->O:Z

    move v0, v0

    .line 625939
    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    .line 625940
    iget-object v0, p1, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-object v0, v0

    .line 625941
    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 625942
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/Contact;->P()V

    .line 625943
    return-void

    .line 625944
    :cond_0
    sget-object v0, LX/2RU;->UNMATCHED:LX/2RU;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 625945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625946
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    .line 625947
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    .line 625948
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    .line 625949
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    .line 625950
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    .line 625951
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    .line 625952
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    .line 625953
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    .line 625954
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    .line 625955
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    .line 625956
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    .line 625957
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    .line 625958
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    .line 625959
    const-class v0, Lcom/facebook/contacts/graphql/ContactPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    .line 625960
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    .line 625961
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    .line 625962
    const-class v0, LX/03R;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    .line 625963
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    .line 625964
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    .line 625965
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    .line 625966
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    .line 625967
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 625968
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 625969
    const-class v0, LX/2RU;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2RU;

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    .line 625970
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    .line 625971
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    .line 625972
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    .line 625973
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    .line 625974
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    .line 625975
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    .line 625976
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    .line 625977
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    .line 625978
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    .line 625979
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    .line 625980
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    .line 625981
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    .line 625982
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    .line 625983
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    .line 625984
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 625985
    return-void
.end method

.method private P()V
    .locals 2

    .prologue
    .line 625986
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 625987
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    if-eqz v0, :cond_0

    .line 625988
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    const-string v1, "if contact.canMessage, fbid cannot be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625989
    :cond_0
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 625990
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    sget-object v1, LX/2RU;->UNMATCHED:LX/2RU;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "If contact has not fbid its profile type must be UNMATCHED"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 625991
    :cond_1
    return-void

    .line 625992
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/3hB;)Lcom/facebook/user/model/Name;
    .locals 4

    .prologue
    .line 625993
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 625994
    if-eqz v0, :cond_0

    .line 625995
    iget-object v0, p0, LX/3hB;->d:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 625996
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/user/model/Name;

    .line 625997
    iget-object v1, p0, LX/3hB;->f:Ljava/lang/String;

    move-object v1, v1

    .line 625998
    iget-object v2, p0, LX/3hB;->g:Ljava/lang/String;

    move-object v2, v2

    .line 625999
    iget-object v3, p0, LX/3hB;->e:Ljava/lang/String;

    move-object v3, v3

    .line 626000
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static newBuilder()LX/3hB;
    .locals 1

    .prologue
    .line 626001
    new-instance v0, LX/3hB;

    invoke-direct {v0}, LX/3hB;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final A()LX/2RU;
    .locals 1

    .prologue
    .line 626002
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    return-object v0
.end method

.method public final B()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$NameEntriesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626003
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    return-object v0
.end method

.method public final C()I
    .locals 1

    .prologue
    .line 626004
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    return v0
.end method

.method public final D()I
    .locals 1

    .prologue
    .line 626005
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    return v0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 626017
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    return v0
.end method

.method public final F()J
    .locals 2

    .prologue
    .line 626006
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    return-wide v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626007
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    return-object v0
.end method

.method public final H()J
    .locals 2

    .prologue
    .line 626008
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    return-wide v0
.end method

.method public final I()Z
    .locals 1

    .prologue
    .line 626009
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    return v0
.end method

.method public final J()F
    .locals 1

    .prologue
    .line 626010
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626011
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    return-object v0
.end method

.method public final L()F
    .locals 1

    .prologue
    .line 626012
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    return v0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 626013
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    return v0
.end method

.method public final N()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
    .locals 1

    .prologue
    .line 626014
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    return-object v0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 626015
    invoke-direct {p0}, Lcom/facebook/contacts/graphql/Contact;->P()V

    .line 626016
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625816
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625817
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626018
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 625764
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/user/model/Name;
    .locals 1

    .prologue
    .line 625763
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    return-object v0
.end method

.method public final f()Lcom/facebook/user/model/Name;
    .locals 1

    .prologue
    .line 625762
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625761
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625760
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625759
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 625758
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 625757
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 625756
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    return v0
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 625755
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    return v0
.end method

.method public final n()F
    .locals 1

    .prologue
    .line 625754
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    return v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/ContactPhone;",
            ">;"
        }
    .end annotation

    .prologue
    .line 625753
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 625752
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 625765
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    return v0
.end method

.method public final r()LX/03R;
    .locals 1

    .prologue
    .line 625766
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 625767
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    return v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 625768
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 625769
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (phonetic name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") <contactId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> <profileFbid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> <commRank:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> <canMessage:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> <isMemorialized:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "><contactType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 625770
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 625771
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    return v0
.end method

.method public final w()J
    .locals 2

    .prologue
    .line 625772
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    return-wide v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 625773
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625774
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mProfileFbid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625775
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mGraphApiWriteId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625776
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mName:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 625777
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhoneticName:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 625778
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625779
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625780
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625781
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSmallPictureSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625782
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBigPictureSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625783
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mHugePictureSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625784
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCommunicationRank:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 625785
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mWithTaggingRank:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 625786
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhones:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 625787
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessageBlockedByViewer:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625788
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanMessage:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625789
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMobilePushable:LX/03R;

    invoke-virtual {v0}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625790
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMessengerUser:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625791
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInstallTimeInMS:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 625792
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsMemorialized:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625793
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsOnViewerContactList:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625794
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 625795
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 625796
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mContactProfileType:LX/2RU;

    invoke-virtual {v0}, LX/2RU;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625797
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameEntries:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 625798
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 625799
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mAddedTimeInMS:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 625800
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625801
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mBirthdayDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625802
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCityName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625803
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mIsPartial:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625804
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mLastFetchTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 625805
    iget-wide v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMontageThreadFBID:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 625806
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanSeeViewerMontageThread:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625807
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mPhatRank:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 625808
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mUsername:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625809
    iget v0, p0, Lcom/facebook/contacts/graphql/Contact;->mMessengerInvitePriority:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 625810
    iget-boolean v0, p0, Lcom/facebook/contacts/graphql/Contact;->mCanViewerSendMoney:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 625811
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mViewerConnectionStatus:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 625812
    return-void
.end method

.method public final x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 625813
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mFriendshipStatus:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final y()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 625814
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mSubscribeStatus:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final z()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 625815
    iget-object v0, p0, Lcom/facebook/contacts/graphql/Contact;->mNameSearchTokens:LX/0Px;

    return-object v0
.end method
