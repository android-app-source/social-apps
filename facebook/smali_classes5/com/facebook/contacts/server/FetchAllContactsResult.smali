.class public Lcom/facebook/contacts/server/FetchAllContactsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchAllContactsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 626363
    new-instance v0, LX/3hC;

    invoke-direct {v0}, LX/3hC;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchAllContactsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;LX/0Px;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 626364
    invoke-direct {p0, p1, p6, p7}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 626365
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 626366
    iput-object p2, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->a:LX/0Px;

    .line 626367
    iput-object p3, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->b:Ljava/lang/String;

    .line 626368
    iput-boolean p4, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->c:Z

    .line 626369
    iput-object p5, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->d:Ljava/lang/String;

    .line 626370
    iput-object p8, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->e:Ljava/lang/String;

    .line 626371
    return-void
.end method

.method public constructor <init>(LX/6O8;)V
    .locals 11

    .prologue
    .line 626372
    iget-object v0, p1, LX/6O8;->a:LX/0ta;

    move-object v1, v0

    .line 626373
    iget-object v0, p1, LX/6O8;->c:LX/0Px;

    move-object v2, v0

    .line 626374
    iget-object v0, p1, LX/6O8;->d:Ljava/lang/String;

    move-object v3, v0

    .line 626375
    iget-boolean v0, p1, LX/6O8;->e:Z

    move v4, v0

    .line 626376
    iget-object v0, p1, LX/6O8;->f:Ljava/lang/String;

    move-object v5, v0

    .line 626377
    iget-wide v9, p1, LX/6O8;->b:J

    move-wide v6, v9

    .line 626378
    iget-object v0, p1, LX/6O8;->g:Ljava/lang/String;

    move-object v8, v0

    .line 626379
    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/contacts/server/FetchAllContactsResult;-><init>(LX/0ta;LX/0Px;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;)V

    .line 626380
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 626381
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 626382
    const-class v1, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->a:LX/0Px;

    .line 626383
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->b:Ljava/lang/String;

    .line 626384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->c:Z

    .line 626385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->d:Ljava/lang/String;

    .line 626386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->e:Ljava/lang/String;

    .line 626387
    return-void

    .line 626388
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 626389
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 626390
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 626391
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 626392
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 626393
    iget-boolean v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 626394
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 626395
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsResult;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 626396
    return-void

    .line 626397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
