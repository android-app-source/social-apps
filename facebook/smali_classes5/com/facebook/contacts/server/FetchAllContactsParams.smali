.class public Lcom/facebook/contacts/server/FetchAllContactsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/contacts/server/FetchAllContactsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field private final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 624857
    new-instance v0, LX/3gp;

    invoke-direct {v0}, LX/3gp;-><init>()V

    sput-object v0, Lcom/facebook/contacts/server/FetchAllContactsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;J)V
    .locals 1

    .prologue
    .line 624858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624859
    iput p1, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->a:I

    .line 624860
    iput-object p2, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->b:Ljava/lang/String;

    .line 624861
    iput-wide p3, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->c:J

    .line 624862
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 624863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624864
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->a:I

    .line 624865
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->b:Ljava/lang/String;

    .line 624866
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->c:J

    .line 624867
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 624868
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 624869
    iget v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 624870
    iget-object v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 624871
    iget-wide v0, p0, Lcom/facebook/contacts/server/FetchAllContactsParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 624872
    return-void
.end method
