.class public Lcom/facebook/account/recovery/common/model/AccountRecoveryData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/account/recovery/common/model/AccountRecoveryData;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/openidconnect/model/OpenIDCredential;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 569531
    new-instance v0, LX/27q;

    invoke-direct {v0}, LX/27q;-><init>()V

    sput-object v0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 569557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569558
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    .line 569559
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 569553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569554
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    .line 569555
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    const-class v1, Lcom/facebook/openidconnect/model/OpenIDCredential;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 569556
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/account/recovery/common/model/AccountRecoveryData;
    .locals 3

    .prologue
    .line 569542
    const-class v1, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    monitor-enter v1

    .line 569543
    :try_start_0
    sget-object v0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 569544
    sput-object v2, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 569545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 569547
    new-instance v0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-direct {v0}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;-><init>()V

    .line 569548
    move-object v0, v0

    .line 569549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 569550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 569551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 569552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/openidconnect/model/OpenIDCredential;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569539
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 569540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    .line 569541
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/openidconnect/model/OpenIDCredential;)V
    .locals 1

    .prologue
    .line 569535
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 569536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    .line 569537
    :cond_0
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569538
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 569534
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 569532
    iget-object v0, p0, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 569533
    return-void
.end method
