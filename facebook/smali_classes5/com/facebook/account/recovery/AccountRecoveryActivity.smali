.class public Lcom/facebook/account/recovery/AccountRecoveryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/2Ng;
.implements LX/2Nh;
.implements LX/2Ni;
.implements LX/2Nj;
.implements LX/2Nk;
.implements LX/2Bt;
.implements LX/0l6;
.implements LX/1ZF;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final u:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final v:LX/0Tn;


# instance fields
.field private A:Landroid/view/View;

.field public p:LX/2Ne;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0lB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/2Dt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/GB2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/0h5;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 568558
    const-class v0, Lcom/facebook/account/recovery/AccountRecoveryActivity;

    sput-object v0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->u:Ljava/lang/Class;

    .line 568559
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "ar_logout"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->v:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 568560
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 568561
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->z:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/facebook/account/recovery/AccountRecoveryActivity;LX/2Ne;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/2Dt;LX/GB2;)V
    .locals 0

    .prologue
    .line 568562
    iput-object p1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->p:LX/2Ne;

    iput-object p2, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->r:LX/0lB;

    iput-object p4, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->s:LX/2Dt;

    iput-object p5, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->t:LX/GB2;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/account/recovery/AccountRecoveryActivity;

    invoke-static {v5}, LX/2Ne;->a(LX/0QB;)LX/2Ne;

    move-result-object v1

    check-cast v1, LX/2Ne;

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v5}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-static {v5}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object v4

    check-cast v4, LX/2Dt;

    invoke-static {v5}, LX/GB2;->b(LX/0QB;)LX/GB2;

    move-result-object v5

    check-cast v5, LX/GB2;

    invoke-static/range {v0 .. v5}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->a(Lcom/facebook/account/recovery/AccountRecoveryActivity;LX/2Ne;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/2Dt;LX/GB2;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 568563
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 568564
    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v1

    if-lez v1, :cond_0

    .line 568565
    invoke-virtual {v0}, LX/0gc;->e()Z

    .line 568566
    :cond_0
    const-string v1, "account_search"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    .line 568567
    if-eqz v0, :cond_1

    .line 568568
    iput-object p1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    .line 568569
    iput-object p2, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    .line 568570
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 568571
    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->d(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 568572
    :goto_0
    return-void

    .line 568573
    :cond_1
    new-instance v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;-><init>()V

    .line 568574
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568575
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568576
    const-string v2, "friend_name"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568577
    const-string v2, "account_search_use_query_now"

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568578
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568579
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    const-string v3, "account_search"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0

    .line 568580
    :cond_2
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 568581
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    const/4 v3, 0x3

    const/4 p0, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lcom/facebook/ui/search/SearchEditText;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public static b(Lcom/facebook/account/recovery/AccountRecoveryActivity;)V
    .locals 3

    .prologue
    .line 568582
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08354a    # 1.810517E38f

    invoke-virtual {p0, v1}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08354b

    invoke-virtual {p0, v1}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08354d

    invoke-virtual {p0, v1}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/GAn;

    invoke-direct {v2, p0}, LX/GAn;-><init>(Lcom/facebook/account/recovery/AccountRecoveryActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08354c

    invoke-virtual {p0, v1}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 568583
    return-void
.end method

.method private l()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 568642
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 568643
    const-string v1, "from_login_pw_error"

    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "from_login_pw_error"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568644
    const-string v1, "login_id"

    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "login_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568645
    const-string v1, "cuid"

    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "cuid"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568646
    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 568584
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 568585
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/GAw;->a:LX/0Tn;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 568586
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/GAw;->a:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568587
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 568588
    :try_start_0
    iget-object v3, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->r:LX/0lB;

    new-instance v4, LX/GAo;

    invoke-direct {v4, p0}, LX/GAo;-><init>(Lcom/facebook/account/recovery/AccountRecoveryActivity;)V

    invoke-virtual {v3, v0, v4}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 568589
    :try_start_1
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->p:LX/2Ne;

    .line 568590
    iget-object v3, v1, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->DEVICE_DATA_READY:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 568591
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 568592
    move-object v4, v4

    .line 568593
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 568594
    :goto_0
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/GAw;->a:LX/0Tn;

    invoke-interface {v1, v3}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 568595
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "cuids"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 568596
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 568597
    :try_start_2
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 568598
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 568599
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 568600
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 568601
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 568602
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 568603
    :goto_3
    sget-object v3, Lcom/facebook/account/recovery/AccountRecoveryActivity;->u:Ljava/lang/Class;

    const-string v4, "Fetching DeviceData from SharedPreferences failed"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 568604
    :cond_0
    :try_start_3
    const-string v1, "cuid"

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568605
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->p:LX/2Ne;

    .line 568606
    iget-object v3, v1, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->CUID_READY:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 568607
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 568608
    move-object v4, v4

    .line 568609
    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 568610
    :cond_1
    :goto_4
    :try_start_4
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z
    :try_end_4
    .catch LX/28F; {:try_start_4 .. :try_end_4} :catch_2

    move-result v1

    if-eqz v1, :cond_2

    .line 568611
    :goto_5
    return-object v2

    .line 568612
    :catch_1
    move-exception v1

    .line 568613
    sget-object v3, Lcom/facebook/account/recovery/AccountRecoveryActivity;->u:Ljava/lang/Class;

    const-string v4, "Parsing encrypted user IDs failed"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 568614
    :cond_2
    :try_start_5
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->r:LX/0lB;

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_5
    .catch LX/28F; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v0

    :goto_6
    move-object v2, v0

    .line 568615
    goto :goto_5

    .line 568616
    :catch_2
    move-exception v0

    .line 568617
    sget-object v1, Lcom/facebook/account/recovery/AccountRecoveryActivity;->u:Ljava/lang/Class;

    const-string v3, "JsonCode Account Search Assisted Data failed"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_6

    .line 568618
    :catch_3
    move-exception v1

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 568619
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->z:Ljava/lang/String;

    const-string v1, ""

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 568620
    return-void
.end method

.method public final a(LX/63W;)V
    .locals 0

    .prologue
    .line 568621
    return-void
.end method

.method public final a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 568622
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->s:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 568623
    new-instance v0, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/BypassAccountConfirmationFragment;-><init>()V

    .line 568624
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568625
    const-string v2, "account_profile"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 568626
    const-string v2, "auto_identify"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568627
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568628
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 568629
    :goto_0
    return-void

    .line 568630
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0368

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 568631
    if-eqz v0, :cond_1

    .line 568632
    invoke-virtual {v0, p1, p2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V

    goto :goto_0

    .line 568633
    :cond_1
    new-instance v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;-><init>()V

    .line 568634
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568635
    const-string v2, "account_profile"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 568636
    const-string v2, "auto_identify"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568637
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568638
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 568639
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 568556
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 568557
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 568640
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 568641
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 568526
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 568527
    invoke-static {p0, p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 568528
    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->setContentView(I)V

    .line 568529
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 568530
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 568531
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    .line 568532
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->s:LX/2Dt;

    .line 568533
    iget-object v2, v0, LX/2Dt;->a:LX/0Uh;

    const/16 v3, 0x21

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 568534
    if-eqz v0, :cond_2

    .line 568535
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    new-instance v2, LX/GAl;

    invoke-direct {v2, p0}, LX/GAl;-><init>(Lcom/facebook/account/recovery/AccountRecoveryActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 568536
    :goto_0
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->s:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 568537
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->t:LX/GB2;

    .line 568538
    iget-object v2, v0, LX/GB2;->b:LX/2Du;

    invoke-virtual {v2}, LX/2Du;->a()Ljava/util/List;

    move-result-object v2

    .line 568539
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 568540
    iget-object v4, v0, LX/GB2;->b:LX/2Du;

    iget-object v5, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2Du;->b(Ljava/lang/String;)LX/4gy;

    move-result-object v4

    .line 568541
    if-eqz v4, :cond_0

    .line 568542
    iget-object v5, v0, LX/GB2;->b:LX/2Du;

    invoke-virtual {v5, v2, v4}, LX/2Du;->b(Landroid/accounts/Account;LX/4gy;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 568543
    new-instance p1, LX/GB1;

    invoke-direct {p1, v0, v2, v4}, LX/GB1;-><init>(LX/GB2;Landroid/accounts/Account;LX/4gy;)V

    iget-object v2, v0, LX/GB2;->c:LX/0TD;

    invoke-static {v5, p1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 568544
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account_profile"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 568545
    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_profile"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 568546
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V

    .line 568547
    :goto_2
    return-void

    .line 568548
    :cond_2
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    new-instance v2, LX/GAm;

    invoke-direct {v2, p0, v1}, LX/GAm;-><init>(Lcom/facebook/account/recovery/AccountRecoveryActivity;LX/0gc;)V

    invoke-interface {v0, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 568549
    :cond_3
    new-instance v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;-><init>()V

    .line 568550
    invoke-direct {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->l()Landroid/os/Bundle;

    move-result-object v2

    .line 568551
    invoke-direct {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->m()Ljava/lang/String;

    move-result-object v3

    .line 568552
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 568553
    const-string v4, "parallel_search"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568554
    :cond_4
    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568555
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    const-string v3, "account_search"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_2
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 568525
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 568511
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "friend_search"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    .line 568512
    if-eqz v0, :cond_0

    .line 568513
    iput-object p1, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->f:Ljava/lang/String;

    .line 568514
    const/16 v2, 0x8

    .line 568515
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 568516
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 568517
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 568518
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 568519
    :goto_0
    return-void

    .line 568520
    :cond_0
    new-instance v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;-><init>()V

    .line 568521
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568522
    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568523
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568524
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    const-string v3, "friend_search"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 568499
    iput-object p1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->x:Ljava/lang/String;

    .line 568500
    iput-object p2, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->y:Ljava/lang/String;

    .line 568501
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "logout"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/fragment/LogoutFragment;

    .line 568502
    if-eqz v0, :cond_0

    .line 568503
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->x:Ljava/lang/String;

    .line 568504
    iput-object v1, v0, Lcom/facebook/account/recovery/fragment/LogoutFragment;->f:Ljava/lang/String;

    .line 568505
    :goto_0
    return-void

    .line 568506
    :cond_0
    new-instance v0, Lcom/facebook/account/recovery/fragment/LogoutFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/LogoutFragment;-><init>()V

    .line 568507
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568508
    const-string v2, "account_secret_id"

    iget-object v3, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568509
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568510
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    const-string v3, "logout"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 568464
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d29e0

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    .line 568465
    if-eqz v0, :cond_0

    .line 568466
    iget-object v1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->y:Ljava/lang/String;

    .line 568467
    iput-object v1, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->i:Ljava/lang/String;

    .line 568468
    iput-object v2, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->j:Ljava/lang/String;

    .line 568469
    :goto_0
    return-void

    .line 568470
    :cond_0
    new-instance v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-direct {v0}, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;-><init>()V

    .line 568471
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568472
    const-string v2, "account_secret_id"

    iget-object v3, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568473
    const-string v2, "account_confirmation_code"

    iget-object v3, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568474
    const-string v2, "account_logout"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568475
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 568476
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 568495
    iput-object p1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->z:Ljava/lang/String;

    .line 568496
    new-instance v0, Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-direct {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;-><init>()V

    .line 568497
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0379

    const-string v3, "captcha"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 568498
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 568489
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 568490
    const-string v1, "account_user_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568491
    const-string v1, "account_password"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568492
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->setResult(ILandroid/content/Intent;)V

    .line 568493
    invoke-virtual {p0}, Lcom/facebook/account/recovery/AccountRecoveryActivity;->finish()V

    .line 568494
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 568488
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->A:Landroid/view/View;

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 568487
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 568486
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 568482
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0379

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 568483
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/GAv;

    if-eqz v1, :cond_0

    check-cast v0, LX/GAv;

    invoke-interface {v0}, LX/GAv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568484
    :goto_0
    return-void

    .line 568485
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 568479
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 568480
    iput-object p1, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->A:Landroid/view/View;

    .line 568481
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 568477
    iget-object v0, p0, Lcom/facebook/account/recovery/AccountRecoveryActivity;->w:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 568478
    return-void
.end method
