.class public final Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LX/2t2;


# direct methods
.method public constructor <init>(LX/2t2;)V
    .locals 0

    .prologue
    .line 578916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578917
    iput-object p1, p0, Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;->a:LX/2t2;

    .line 578918
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 578919
    iget-object v0, p0, Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;->a:LX/2t2;

    .line 578920
    iget-object v1, v0, LX/2t2;->h:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 578921
    :cond_0
    :goto_0
    return-void

    .line 578922
    :cond_1
    iget-object v1, v0, LX/2t2;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 578923
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 578924
    sget-object v2, LX/GAw;->b:LX/0Tn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "_"

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 578925
    iget-object v2, v0, LX/2t2;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 578926
    if-nez v2, :cond_0

    .line 578927
    iget-object v2, v0, LX/2t2;->b:LX/2Dr;

    const-string v3, "app_activations"

    invoke-virtual {v2, v3}, LX/2Dr;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/account/recovery/common/helper/AccountRecoveryActivationsHelper$1;-><init>(LX/2t2;LX/0Tn;)V

    iget-object v1, v0, LX/2t2;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
