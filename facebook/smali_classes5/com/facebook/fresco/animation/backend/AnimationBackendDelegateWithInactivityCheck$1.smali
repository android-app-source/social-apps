.class public final Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4C8;


# direct methods
.method public constructor <init>(LX/4C8;)V
    .locals 0

    .prologue
    .line 678410
    iput-object p1, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 678411
    iget-object v1, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    monitor-enter v1

    .line 678412
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    const/4 v2, 0x0

    .line 678413
    iput-boolean v2, v0, LX/4C8;->c:Z

    .line 678414
    iget-object v0, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    .line 678415
    iget-object v3, v0, LX/4C8;->a:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iget-wide v5, v0, LX/4C8;->d:J

    sub-long/2addr v3, v5

    iget-wide v5, v0, LX/4C8;->e:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 678416
    if-eqz v0, :cond_1

    .line 678417
    iget-object v0, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    iget-object v0, v0, LX/4C8;->g:LX/4C7;

    if-eqz v0, :cond_0

    .line 678418
    iget-object v0, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    iget-object v0, v0, LX/4C8;->g:LX/4C7;

    invoke-interface {v0}, LX/4C7;->f()V

    .line 678419
    :cond_0
    :goto_1
    monitor-exit v1

    return-void

    .line 678420
    :cond_1
    iget-object v0, p0, Lcom/facebook/fresco/animation/backend/AnimationBackendDelegateWithInactivityCheck$1;->a:LX/4C8;

    invoke-static {v0}, LX/4C8;->g(LX/4C8;)V

    goto :goto_1

    .line 678421
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
