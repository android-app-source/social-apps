.class public final Lcom/facebook/ipc/media/data/MimeType$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/ipc/media/data/MimeType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799576
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    return-void
.end method

.method private static a(LX/15w;)Lcom/facebook/ipc/media/data/MimeType;
    .locals 3

    .prologue
    .line 799577
    const/4 v0, 0x0

    .line 799578
    :goto_0
    invoke-static {p0}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 799579
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_0

    .line 799580
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v0

    .line 799581
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 799582
    :cond_1
    if-nez v0, :cond_2

    .line 799583
    new-instance v0, LX/2aQ;

    const-string v1, "MimeType: missing raw type string"

    invoke-virtual {p0}, LX/15w;->l()LX/28G;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v0

    .line 799584
    :cond_2
    invoke-static {v0}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 799585
    invoke-static {p1}, Lcom/facebook/ipc/media/data/MimeType$Deserializer;->a(LX/15w;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v0

    return-object v0
.end method
