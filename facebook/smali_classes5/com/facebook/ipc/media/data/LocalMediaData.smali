.class public Lcom/facebook/ipc/media/data/LocalMediaData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/4gK;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/media/data/LocalMediaData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mDateTaken:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "date_taken"
    .end annotation
.end field

.field public final mDisplayName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "display_name"
    .end annotation
.end field

.field public final mMediaData:Lcom/facebook/ipc/media/data/MediaData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "media_data"
    .end annotation
.end field

.field public final mMediaStoreId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_store_id"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799439
    new-instance v0, LX/4gM;

    invoke-direct {v0}, LX/4gM;-><init>()V

    sput-object v0, Lcom/facebook/ipc/media/data/LocalMediaData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 799427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799428
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    .line 799429
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    .line 799430
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    .line 799431
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    .line 799432
    return-void
.end method

.method public constructor <init>(LX/4gN;)V
    .locals 2

    .prologue
    .line 799404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799405
    iget-object v0, p1, LX/4gN;->a:Lcom/facebook/ipc/media/data/MediaData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    .line 799406
    iget-wide v0, p1, LX/4gN;->b:J

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    .line 799407
    iget-object v0, p1, LX/4gN;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    .line 799408
    iget-wide v0, p1, LX/4gN;->d:J

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    .line 799409
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 799433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799434
    const-class v0, Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    .line 799435
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    .line 799436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    .line 799437
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    .line 799438
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 799425
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    return-wide v0
.end method

.method public final b()Lcom/facebook/ipc/media/data/MediaData;
    .locals 1

    .prologue
    .line 799426
    iget-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 799424
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 799423
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/4gN;
    .locals 5

    .prologue
    .line 799416
    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    iget-object v1, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {v0, v1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    .line 799417
    iput-wide v2, v0, LX/4gN;->b:J

    .line 799418
    move-object v0, v0

    .line 799419
    iget-object v1, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4gN;->a(Ljava/lang/String;)LX/4gN;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    .line 799420
    iput-wide v2, v0, LX/4gN;->d:J

    .line 799421
    move-object v0, v0

    .line 799422
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 799415
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{LocalMediaData: %s, %d}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 799410
    iget-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799411
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 799412
    iget-object v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799413
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 799414
    return-void
.end method
