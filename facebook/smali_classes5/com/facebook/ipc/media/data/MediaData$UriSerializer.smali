.class public final Lcom/facebook/ipc/media/data/MediaData$UriSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799485
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 799486
    if-nez p0, :cond_0

    .line 799487
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "UriSerializer.serialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799488
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 799489
    const-string v0, "uri"

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 799490
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 799491
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 799492
    check-cast p1, Landroid/net/Uri;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/media/data/MediaData$UriSerializer;->a(Landroid/net/Uri;LX/0nX;LX/0my;)V

    return-void
.end method
