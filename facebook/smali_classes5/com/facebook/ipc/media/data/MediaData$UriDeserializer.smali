.class public final Lcom/facebook/ipc/media/data/MediaData$UriDeserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799477
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    return-void
.end method

.method private static a(LX/15w;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 799478
    const/4 v0, 0x0

    .line 799479
    :goto_0
    invoke-static {p0}, LX/1Xh;->a(LX/15w;)LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 799480
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v1, v2, :cond_0

    .line 799481
    invoke-virtual {p0}, LX/15w;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 799482
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 799483
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 799484
    invoke-static {p1}, Lcom/facebook/ipc/media/data/MediaData$UriDeserializer;->a(LX/15w;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
