.class public final Lcom/facebook/ipc/media/data/MediaData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/media/data/MediaData;",
            ">;"
        }
    .end annotation
.end field

.field public static a:F

.field public static b:D


# instance fields
.field public final mAspectRatio:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aspect_ratio"
    .end annotation
.end field

.field public final mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "focus_point"
    .end annotation
.end field

.field public final mHeight:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field public final mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "id"
    .end annotation
.end field

.field public final mLatitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "latitude"
    .end annotation
.end field

.field public final mLongitude:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "longitude"
    .end annotation
.end field

.field public final mMimeType:Lcom/facebook/ipc/media/data/MimeType;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mime_type"
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
        using = Lcom/facebook/ipc/media/data/MimeType$Deserializer;
    .end annotation
.end field

.field public final mOrientation:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "orientation"
    .end annotation
.end field

.field public final mType:LX/4gQ;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "type"
    .end annotation
.end field

.field public final mUri:Landroid/net/Uri;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "uri"
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
        using = Lcom/facebook/ipc/media/data/MediaData$UriDeserializer;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
        using = Lcom/facebook/ipc/media/data/MediaData$UriSerializer;
    .end annotation
.end field

.field public final mWidth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 799569
    const/high16 v0, 0x7fc00000    # NaNf

    sput v0, Lcom/facebook/ipc/media/data/MediaData;->a:F

    .line 799570
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    sput-wide v0, Lcom/facebook/ipc/media/data/MediaData;->b:D

    .line 799571
    new-instance v0, LX/4gO;

    invoke-direct {v0}, LX/4gO;-><init>()V

    sput-object v0, Lcom/facebook/ipc/media/data/MediaData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 799493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799494
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    .line 799495
    sget-object v0, LX/4gQ;->Photo:LX/4gQ;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    .line 799496
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    .line 799497
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->e:Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    .line 799498
    iput v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    .line 799499
    iput v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    .line 799500
    iput v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    .line 799501
    sget v0, Lcom/facebook/ipc/media/data/MediaData;->a:F

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    .line 799502
    sget-object v0, Lcom/facebook/ipc/media/data/FocusPoint;->a:Lcom/facebook/ipc/media/data/FocusPoint;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799503
    sget-wide v0, Lcom/facebook/ipc/media/data/MediaData;->b:D

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    .line 799504
    sget-wide v0, Lcom/facebook/ipc/media/data/MediaData;->b:D

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    .line 799505
    return-void
.end method

.method public constructor <init>(LX/4gP;)V
    .locals 2

    .prologue
    .line 799556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799557
    iget-object v0, p1, LX/4gP;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    .line 799558
    iget-object v0, p1, LX/4gP;->b:LX/4gQ;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4gQ;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    .line 799559
    iget-object v0, p1, LX/4gP;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    .line 799560
    iget-object v0, p1, LX/4gP;->d:Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    .line 799561
    iget v0, p1, LX/4gP;->e:I

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    .line 799562
    iget v0, p1, LX/4gP;->f:I

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    .line 799563
    iget v0, p1, LX/4gP;->g:I

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    .line 799564
    iget v0, p1, LX/4gP;->h:F

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    .line 799565
    iget-object v0, p1, LX/4gP;->i:Lcom/facebook/ipc/media/data/FocusPoint;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799566
    iget-wide v0, p1, LX/4gP;->j:D

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    .line 799567
    iget-wide v0, p1, LX/4gP;->k:D

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    .line 799568
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 799542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799543
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    .line 799544
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4gQ;->valueOf(Ljava/lang/String;)LX/4gQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    .line 799545
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    .line 799546
    const-class v0, Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MimeType;

    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    .line 799547
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    .line 799548
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    .line 799549
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    .line 799550
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    .line 799551
    const-class v0, Lcom/facebook/ipc/media/data/FocusPoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799552
    sget-object v1, Lcom/facebook/ipc/media/data/FocusPoint;->a:Lcom/facebook/ipc/media/data/FocusPoint;

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/media/data/FocusPoint;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/facebook/ipc/media/data/FocusPoint;->a:Lcom/facebook/ipc/media/data/FocusPoint;

    :cond_0
    iput-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799553
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    .line 799554
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    .line 799555
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 799541
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/4gQ;
    .locals 1

    .prologue
    .line 799540
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 799539
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 799572
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 799538
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 799537
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    return v0
.end method

.method public final i()D
    .locals 2

    .prologue
    .line 799536
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    return-wide v0
.end method

.method public final j()D
    .locals 2

    .prologue
    .line 799535
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    return-wide v0
.end method

.method public final k()LX/4gP;
    .locals 4

    .prologue
    .line 799519
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    iget-object v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v0

    iget v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    .line 799520
    iput v1, v0, LX/4gP;->e:I

    .line 799521
    move-object v0, v0

    .line 799522
    iget v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    .line 799523
    iput v1, v0, LX/4gP;->f:I

    .line 799524
    move-object v0, v0

    .line 799525
    iget v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    .line 799526
    iput v1, v0, LX/4gP;->g:I

    .line 799527
    move-object v0, v0

    .line 799528
    iget v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    .line 799529
    iput v1, v0, LX/4gP;->h:F

    .line 799530
    move-object v0, v0

    .line 799531
    iget-object v1, p0, Lcom/facebook/ipc/media/data/MediaData;->mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799532
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/data/FocusPoint;

    iput-object v2, v0, LX/4gP;->i:Lcom/facebook/ipc/media/data/FocusPoint;

    .line 799533
    move-object v0, v0

    .line 799534
    iget-wide v2, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    invoke-virtual {v0, v2, v3}, LX/4gP;->a(D)LX/4gP;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    invoke-virtual {v0, v2, v3}, LX/4gP;->b(D)LX/4gP;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 799518
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{MediaData: %s %s %s}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 799506
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799507
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    invoke-virtual {v0}, LX/4gQ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799508
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799509
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799510
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 799511
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 799512
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 799513
    iget v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 799514
    iget-object v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mFocusPoint:Lcom/facebook/ipc/media/data/FocusPoint;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799515
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 799516
    iget-wide v0, p0, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 799517
    return-void
.end method
