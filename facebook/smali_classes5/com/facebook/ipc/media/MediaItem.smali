.class public abstract Lcom/facebook/ipc/media/MediaItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final a:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Lcom/facebook/ipc/media/data/LocalMediaData;

.field public final d:J

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 799305
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    .line 799306
    new-instance v0, LX/4gE;

    invoke-direct {v0}, LX/4gE;-><init>()V

    sput-object v0, Lcom/facebook/ipc/media/MediaItem;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 799299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799300
    const-class v0, Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/LocalMediaData;

    iput-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 799301
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->d:J

    .line 799302
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->e:J

    .line 799303
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    .line 799304
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/media/data/LocalMediaData;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 799293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799294
    iput-object p1, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 799295
    iput-wide p2, p0, Lcom/facebook/ipc/media/MediaItem;->d:J

    .line 799296
    iput-wide p4, p0, Lcom/facebook/ipc/media/MediaItem;->e:J

    .line 799297
    iput-object p6, p0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    .line 799298
    return-void
.end method

.method public static b(Ljava/lang/String;)LX/4gF;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799284
    if-nez p0, :cond_0

    .line 799285
    sget-object v0, LX/4gF;->UNKNOWN:LX/4gF;

    .line 799286
    :goto_0
    return-object v0

    .line 799287
    :cond_0
    invoke-static {p0}, LX/46I;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799288
    sget-object v0, LX/4gF;->PHOTO:LX/4gF;

    goto :goto_0

    .line 799289
    :cond_1
    invoke-static {p0}, LX/46I;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 799290
    sget-object v0, LX/4gF;->VIDEO:LX/4gF;

    goto :goto_0

    .line 799291
    :cond_2
    const-string v0, "MediaItem"

    const-string v1, "Unsupported mimeType %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 799292
    sget-object v0, LX/4gF;->UNKNOWN:LX/4gF;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 1

    .prologue
    .line 799283
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    return-object v0
.end method

.method public final b()Lcom/facebook/ipc/media/data/MediaData;
    .locals 1

    .prologue
    .line 799223
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799280
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v0

    .line 799281
    iget-wide v2, v0, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v0, v2

    .line 799282
    return-wide v0
.end method

.method public final d()Lcom/facebook/ipc/media/MediaIdKey;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799276
    new-instance v0, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    .line 799277
    iget-object v2, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v2

    .line 799278
    iget-wide v4, v2, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v2, v4

    .line 799279
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 799275
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 799267
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799268
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v1

    .line 799269
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 799270
    const/4 v0, 0x0

    .line 799271
    :goto_0
    return-object v0

    .line 799272
    :cond_0
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 799273
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 799274
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 799266
    instance-of v0, p1, Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 799263
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799264
    iget-object p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, p0

    .line 799265
    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 799260
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799261
    iget p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v0, p0

    .line 799262
    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 799257
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v0

    .line 799258
    iget-object p0, v0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDisplayName:Ljava/lang/String;

    move-object v0, p0

    .line 799259
    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 799256
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799253
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799254
    iget-object p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v0, p0

    .line 799255
    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()J
    .locals 4

    .prologue
    .line 799250
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v0

    .line 799251
    iget-wide v2, v0, Lcom/facebook/ipc/media/data/LocalMediaData;->mDateTaken:J

    move-wide v0, v2

    .line 799252
    return-wide v0
.end method

.method public final k()F
    .locals 1

    .prologue
    .line 799247
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799248
    iget p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mAspectRatio:F

    move v0, p0

    .line 799249
    return v0
.end method

.method public final l()LX/4gF;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799244
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 799245
    iget-object p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v0, p0

    .line 799246
    invoke-virtual {v0}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/media/MediaItem;->b(Ljava/lang/String;)LX/4gF;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/4gF;
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799235
    sget-object v0, LX/4gD;->a:[I

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 799236
    iget-object v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v1, v2

    .line 799237
    invoke-virtual {v1}, LX/4gQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 799238
    const-string v0, "MediaItem"

    const-string v1, "MediaItem.getType: unexpected source type %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 799239
    iget-object p0, v4, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v4, p0

    .line 799240
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 799241
    sget-object v0, LX/4gF;->UNKNOWN:LX/4gF;

    :goto_0
    return-object v0

    .line 799242
    :pswitch_0
    sget-object v0, LX/4gF;->PHOTO:LX/4gF;

    goto :goto_0

    .line 799243
    :pswitch_1
    sget-object v0, LX/4gF;->VIDEO:LX/4gF;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final o()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799234
    iget-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->e:J

    return-wide v0
.end method

.method public final p()Z
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 799233
    iget-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 799232
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 799229
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "MediaItem(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 799230
    iget-object v4, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v4, v4

    .line 799231
    invoke-virtual {v4}, Lcom/facebook/ipc/media/data/LocalMediaData;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 799224
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799225
    iget-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 799226
    iget-wide v0, p0, Lcom/facebook/ipc/media/MediaItem;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 799227
    iget-object v0, p0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799228
    return-void
.end method
