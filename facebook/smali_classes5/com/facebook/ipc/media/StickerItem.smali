.class public Lcom/facebook/ipc/media/StickerItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/media/StickerItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/stickers/model/Sticker;

.field public final b:J

.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799315
    new-instance v0, LX/4gG;

    invoke-direct {v0}, LX/4gG;-><init>()V

    sput-object v0, Lcom/facebook/ipc/media/StickerItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 799316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799317
    iput-wide p1, p0, Lcom/facebook/ipc/media/StickerItem;->b:J

    .line 799318
    iput-object p3, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    .line 799319
    iput-object p4, p0, Lcom/facebook/ipc/media/StickerItem;->d:Ljava/lang/String;

    .line 799320
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 799321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799322
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/media/StickerItem;->b:J

    .line 799323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    .line 799324
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->d:Ljava/lang/String;

    .line 799325
    const-class v0, Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    iput-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->a:Lcom/facebook/stickers/model/Sticker;

    .line 799326
    return-void
.end method

.method public static b()LX/4gH;
    .locals 1

    .prologue
    .line 799327
    new-instance v0, LX/4gH;

    invoke-direct {v0}, LX/4gH;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 799328
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 799329
    instance-of v0, p1, Lcom/facebook/ipc/media/StickerItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/ipc/media/StickerItem;

    .line 799330
    iget-object v0, p1, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    move-object v0, v0

    .line 799331
    iget-object v1, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    move-object v1, v1

    .line 799332
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 799333
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 799334
    iget-object v2, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    move-object v2, v2

    .line 799335
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 799336
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "StickerItem(%d, %s, %s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/facebook/ipc/media/StickerItem;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/facebook/ipc/media/StickerItem;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 799337
    iget-wide v0, p0, Lcom/facebook/ipc/media/StickerItem;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 799338
    iget-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799339
    iget-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799340
    iget-object v0, p0, Lcom/facebook/ipc/media/StickerItem;->a:Lcom/facebook/stickers/model/Sticker;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799341
    return-void
.end method
