.class public Lcom/facebook/offlinemode/db/FlattenableTags;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/Flattenable;


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 800269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800270
    return-void
.end method

.method public constructor <init>(LX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 800271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800272
    invoke-virtual {p1}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/offlinemode/db/FlattenableTags;->a:LX/0Px;

    .line 800273
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 800274
    iget-object v0, p0, Lcom/facebook/offlinemode/db/FlattenableTags;->a:LX/0Px;

    invoke-virtual {p1, v0, v2}, LX/186;->b(Ljava/util/List;Z)I

    move-result v0

    .line 800275
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 800276
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 800277
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 800278
    iget-object v0, p0, Lcom/facebook/offlinemode/db/FlattenableTags;->a:LX/0Px;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;I)V
    .locals 2

    .prologue
    .line 800279
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "initFromMutableFlatBuffer is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    .line 800280
    const/4 v0, 0x0

    const-class v1, Ljava/util/ArrayList;

    invoke-static {p1, p2, v0, v1}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 800281
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/offlinemode/db/FlattenableTags;->a:LX/0Px;

    .line 800282
    return-void

    .line 800283
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
