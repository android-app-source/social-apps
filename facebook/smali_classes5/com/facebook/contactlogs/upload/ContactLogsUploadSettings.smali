.class public Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0aG;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2UL;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574372
    const-class v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;LX/2UL;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/contactlogs/annotation/IsContactLogsUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2UL;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574383
    iput-object p1, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574384
    iput-object p2, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->c:LX/0aG;

    .line 574385
    iput-object p3, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->d:LX/0Or;

    .line 574386
    iput-object p4, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->e:LX/2UL;

    .line 574387
    iput-object p5, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->f:LX/0Or;

    .line 574388
    return-void
.end method

.method public static b(Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;)LX/0Tn;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 574377
    iget-object v0, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574378
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 574379
    const/4 p0, 0x0

    .line 574380
    :goto_0
    move-object v0, p0

    .line 574381
    return-object v0

    :cond_0
    sget-object p0, LX/DAD;->c:LX/0Tn;

    invoke-virtual {p0, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object p0

    check-cast p0, LX/0Tn;

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;
    .locals 6

    .prologue
    .line 574375
    new-instance v0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/2UL;->a(LX/0QB;)LX/2UL;

    move-result-object v4

    check-cast v4, LX/2UL;

    const/16 v5, 0x308

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;LX/2UL;LX/0Or;)V

    .line 574376
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 574373
    invoke-static {p0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;)LX/0Tn;

    move-result-object v1

    .line 574374
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
