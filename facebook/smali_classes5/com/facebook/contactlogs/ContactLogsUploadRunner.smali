.class public Lcom/facebook/contactlogs/ContactLogsUploadRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile n:Lcom/facebook/contactlogs/ContactLogsUploadRunner;


# instance fields
.field public final b:LX/0aG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0SG;

.field public final f:LX/1Ml;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:Landroid/net/ConnectivityManager;

.field private final i:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

.field public final j:LX/2UL;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/1ML;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574314
    const-class v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;LX/1Ml;Ljava/util/concurrent/ExecutorService;Landroid/net/ConnectivityManager;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;LX/2UL;LX/0Or;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/contactlogs/annotation/IsContactLogsUploadEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/contactlogs/annotation/IsContactLogsFrequentUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/1Ml;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/net/ConnectivityManager;",
            "Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;",
            "LX/2UL;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574316
    iput-object p1, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->b:LX/0aG;

    .line 574317
    iput-object p2, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574318
    iput-object p3, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->d:LX/0Or;

    .line 574319
    iput-object p4, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->e:LX/0SG;

    .line 574320
    iput-object p5, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->f:LX/1Ml;

    .line 574321
    iput-object p6, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->g:Ljava/util/concurrent/ExecutorService;

    .line 574322
    iput-object p7, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->h:Landroid/net/ConnectivityManager;

    .line 574323
    iput-object p8, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->i:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    .line 574324
    iput-object p9, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->j:LX/2UL;

    .line 574325
    iput-object p10, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->k:LX/0Or;

    .line 574326
    iput-object p11, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->l:LX/0Or;

    .line 574327
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/contactlogs/ContactLogsUploadRunner;
    .locals 15

    .prologue
    .line 574328
    sget-object v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->n:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    if-nez v0, :cond_1

    .line 574329
    const-class v1, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    monitor-enter v1

    .line 574330
    :try_start_0
    sget-object v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->n:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574331
    if-eqz v2, :cond_0

    .line 574332
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 574333
    new-instance v3, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v10

    check-cast v10, Landroid/net/ConnectivityManager;

    invoke-static {v0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(LX/0QB;)Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    move-result-object v11

    check-cast v11, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {v0}, LX/2UL;->a(LX/0QB;)LX/2UL;

    move-result-object v12

    check-cast v12, LX/2UL;

    const/16 v13, 0x308

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x1475

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;-><init>(LX/0aG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;LX/1Ml;Ljava/util/concurrent/ExecutorService;Landroid/net/ConnectivityManager;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;LX/2UL;LX/0Or;LX/0Or;)V

    .line 574334
    move-object v0, v3

    .line 574335
    sput-object v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->n:Lcom/facebook/contactlogs/ContactLogsUploadRunner;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574336
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574337
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574338
    :cond_1
    sget-object v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->n:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    return-object v0

    .line 574339
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574340
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized e(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)LX/1ML;
    .locals 10

    .prologue
    .line 574341
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->j:LX/2UL;

    sget-object v1, LX/DA8;->UPLOAD_STARTED:LX/DA8;

    invoke-virtual {v0, v1}, LX/2UL;->a(LX/DA8;)V

    .line 574342
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/DAD;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 574343
    iget-object v4, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->b:LX/0aG;

    const-string v5, "upload_contact_logs"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v8, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x231ddf10

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    move-object v0, v4

    .line 574344
    iput-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->m:LX/1ML;

    .line 574345
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->m:LX/1ML;

    .line 574346
    new-instance v1, LX/D9z;

    invoke-direct {v1, p0}, LX/D9z;-><init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)V

    iget-object v2, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574347
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->m:LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 574348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/1ML;
    .locals 1

    .prologue
    .line 574349
    invoke-virtual {p0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574350
    const/4 v0, 0x0

    .line 574351
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->e(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)LX/1ML;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 574352
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->i:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-virtual {v0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574353
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->m:LX/1ML;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 574354
    if-nez v0, :cond_0

    .line 574355
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->f:LX/1Ml;

    const-string v2, "android.permission.READ_CALL_LOG"

    invoke-virtual {v0, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 574356
    if-eqz v0, :cond_0

    .line 574357
    iget-object v0, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->f:LX/1Ml;

    const-string v2, "android.permission.READ_SMS"

    invoke-virtual {v0, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 574358
    if-eqz v0, :cond_0

    .line 574359
    iget-object v2, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/DAD;->b:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 574360
    iget-object v4, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    const/4 v6, 0x1

    .line 574361
    iget-object v7, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->h:Landroid/net/ConnectivityManager;

    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v7

    .line 574362
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    if-ne v7, v6, :cond_3

    move v7, v6

    .line 574363
    :goto_1
    iget-object v6, p0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->l:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 574364
    const-wide/32 v6, 0x36ee80

    .line 574365
    :goto_2
    move-wide v4, v6

    .line 574366
    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    :goto_3
    move v0, v2

    .line 574367
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_4
    return v0

    :cond_0
    move v0, v1

    goto :goto_4

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 574368
    :cond_3
    const/4 v6, 0x0

    move v7, v6

    goto :goto_1

    .line 574369
    :cond_4
    if-eqz v7, :cond_5

    .line 574370
    const-wide/32 v6, 0x5265c00

    goto :goto_2

    .line 574371
    :cond_5
    const-wide/32 v6, 0x6c258c00

    goto :goto_2
.end method
