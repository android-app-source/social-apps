.class public Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/BMS;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587048
    new-instance v0, LX/3Un;

    invoke-direct {v0}, LX/3Un;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->a:LX/1Cz;

    .line 587049
    const-class v0, Lcom/facebook/productionprompts/v3/ProductionPromptSmallPartDefintion;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587045
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587046
    iput-object p1, p0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587047
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;
    .locals 4

    .prologue
    .line 587034
    const-class v1, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;

    monitor-enter v1

    .line 587035
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587036
    sput-object v2, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587037
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587038
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587039
    new-instance p0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V

    .line 587040
    move-object v0, p0

    .line 587041
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587042
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587043
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BMS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587033
    sget-object v0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 587025
    check-cast p2, LX/1Ri;

    .line 587026
    iget-object v0, p0, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {p2}, LX/Ak0;->a(LX/1Ri;)LX/Ak0;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587027
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x789b5d87

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587030
    check-cast p1, LX/1Ri;

    check-cast p4, LX/BMS;

    .line 587031
    iget-object v1, p1, LX/1Ri;->c:LX/AkL;

    invoke-interface {v1}, LX/AkL;->f()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/BMS;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 587032
    const/16 v1, 0x1f

    const v2, 0x1709a59a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 587028
    check-cast p1, LX/1Ri;

    .line 587029
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kV;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
