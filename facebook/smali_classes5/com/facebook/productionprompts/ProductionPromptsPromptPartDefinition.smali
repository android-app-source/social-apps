.class public Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Landroid/view/View$OnClickListener;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final b:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/BMP;

.field public final c:LX/B5l;

.field private final d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final e:LX/24B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586674
    new-instance v0, LX/3Ue;

    invoke-direct {v0}, LX/3Ue;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/BMP;LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586668
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586669
    iput-object p1, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a:LX/BMP;

    .line 586670
    iput-object p2, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->c:LX/B5l;

    .line 586671
    iput-object p3, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586672
    iput-object p4, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->e:LX/24B;

    .line 586673
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;
    .locals 7

    .prologue
    .line 586657
    const-class v1, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;

    monitor-enter v1

    .line 586658
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586659
    sput-object v2, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586662
    new-instance p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v3

    check-cast v3, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v4

    check-cast v4, LX/B5l;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v6

    check-cast v6, LX/24B;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;-><init>(LX/BMP;LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;)V

    .line 586663
    move-object v0, p0

    .line 586664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public a()LX/1Cz;
    .locals 1

    .prologue
    .line 586656
    sget-object v0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586652
    check-cast p2, LX/1Ri;

    .line 586653
    iget-object v6, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->e:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586654
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    .line 586655
    new-instance v1, LX/BMF;

    invoke-direct {v1, p0, v0}, LX/BMF;-><init>(Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;LX/1RN;)V

    return-object v1
.end method

.method public a(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ri;",
            "Landroid/view/View$OnClickListener;",
            "TE;TV;)V"
        }
    .end annotation

    .prologue
    .line 586650
    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586651
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x19e5d3e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586649
    check-cast p1, LX/1Ri;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p3, LX/1Pr;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, 0x584625a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public a(LX/1Ri;)Z
    .locals 1

    .prologue
    .line 586644
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->c:LX/AkL;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586648
    check-cast p1, LX/1Ri;

    invoke-virtual {p0, p1}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a(LX/1Ri;)Z

    move-result v0

    return v0
.end method

.method public b(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ri;",
            "Landroid/view/View$OnClickListener;",
            "TE;TV;)V"
        }
    .end annotation

    .prologue
    .line 586646
    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586647
    return-void
.end method

.method public bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 586645
    check-cast p1, LX/1Ri;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p3, LX/1Pr;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    return-void
.end method
