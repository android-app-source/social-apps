.class public Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;
.super Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition",
        "<TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586708
    new-instance v0, LX/3Uf;

    invoke-direct {v0}, LX/3Uf;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/BMP;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/B5l;LX/24B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586706
    invoke-direct {p0, p1, p3, p2, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;-><init>(LX/BMP;LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;)V

    .line 586707
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;
    .locals 7

    .prologue
    .line 586695
    const-class v1, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;

    monitor-enter v1

    .line 586696
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586697
    sput-object v2, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586698
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586699
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586700
    new-instance p0, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v3

    check-cast v3, LX/BMP;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v5

    check-cast v5, LX/B5l;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v6

    check-cast v6, LX/24B;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;-><init>(LX/BMP;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/B5l;LX/24B;)V

    .line 586701
    move-object v0, p0

    .line 586702
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586703
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586704
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586694
    sget-object v0, Lcom/facebook/productionprompts/ClipboardPromptsPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ri;",
            "Landroid/view/View$OnClickListener;",
            "TE;TV;)V"
        }
    .end annotation

    .prologue
    .line 586686
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    .line 586687
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kV;

    move-object v1, p4

    .line 586688
    check-cast v1, LX/1aQ;

    invoke-interface {v1}, LX/1aQ;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 586689
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object p0, p0

    .line 586690
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 586691
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586692
    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586693
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4ddb9652    # 4.60507712E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586677
    check-cast p1, LX/1Ri;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p3, LX/1Pr;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    const/16 v1, 0x1f

    const v2, -0x174cba79

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1Ri;)Z
    .locals 2

    .prologue
    .line 586685
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586684
    check-cast p1, LX/1Ri;

    invoke-virtual {p0, p1}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->a(LX/1Ri;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ri;",
            "Landroid/view/View$OnClickListener;",
            "TE;TV;)V"
        }
    .end annotation

    .prologue
    .line 586679
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    move-object v0, p4

    .line 586680
    check-cast v0, LX/1aQ;

    invoke-interface {v0}, LX/1aQ;->getV2AttachmentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 586681
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586682
    check-cast p4, LX/1aQ;

    invoke-interface {p4}, LX/1aQ;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586683
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 586678
    check-cast p1, LX/1Ri;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p3, LX/1Pr;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/facebook/productionprompts/ProductionPromptsPromptPartDefinition;->b(LX/1Ri;Landroid/view/View$OnClickListener;LX/1Pr;Landroid/view/View;)V

    return-void
.end method
