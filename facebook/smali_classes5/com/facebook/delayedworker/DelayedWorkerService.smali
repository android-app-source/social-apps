.class public Lcom/facebook/delayedworker/DelayedWorkerService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/1ET;

.field private d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 573722
    const-class v0, Lcom/facebook/delayedworker/DelayedWorkerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/delayedworker/DelayedWorkerService;->a:Ljava/lang/String;

    .line 573723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/facebook/delayedworker/DelayedWorkerService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".facebook.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/delayedworker/DelayedWorkerService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 573730
    const-class v0, Lcom/facebook/delayedworker/DelayedWorkerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 573731
    return-void
.end method

.method public static a(Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 573732
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 573733
    sget-object v1, Lcom/facebook/delayedworker/DelayedWorkerService;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "class"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 573734
    if-eqz p1, :cond_0

    .line 573735
    const-string v1, "last"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 573736
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/delayedworker/AbstractDelayedWorker;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 573737
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 573738
    :goto_0
    return-object v0

    .line 573739
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 573740
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 573741
    instance-of v2, v0, Lcom/facebook/delayedworker/AbstractDelayedWorker;

    if-nez v2, :cond_1

    .line 573742
    iget-object v0, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->d:LX/03V;

    sget-object v2, Lcom/facebook/delayedworker/DelayedWorkerService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "It\'s not a DelayedWorker instance - DelayedWorkerClassName: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 573743
    goto :goto_0

    .line 573744
    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    move-object v0, v1

    .line 573745
    goto :goto_0

    .line 573746
    :catch_1
    move-exception v0

    .line 573747
    iget-object v2, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->d:LX/03V;

    sget-object v3, Lcom/facebook/delayedworker/DelayedWorkerService;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DelayedWorkerClassName: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 573748
    goto :goto_0

    .line 573749
    :catch_2
    move-exception v0

    .line 573750
    iget-object v2, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->d:LX/03V;

    sget-object v3, Lcom/facebook/delayedworker/DelayedWorkerService;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DelayedWorkerClassName: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 573751
    goto :goto_0

    .line 573752
    :cond_1
    check-cast v0, Lcom/facebook/delayedworker/AbstractDelayedWorker;

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 573724
    sget-object v0, Lcom/facebook/delayedworker/DelayedWorkerService;->b:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573725
    const/4 v0, 0x0

    .line 573726
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "class"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/1ET;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573727
    iput-object p1, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->c:LX/1ET;

    .line 573728
    iput-object p2, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->d:LX/03V;

    .line 573729
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/delayedworker/DelayedWorker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573697
    const-string v0, "last"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573698
    iget-object v0, p0, Lcom/facebook/delayedworker/DelayedWorkerService;->c:LX/1ET;

    .line 573699
    sget-object p0, LX/1ET;->a:LX/0Tn;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object p0

    check-cast p0, LX/0Tn;

    .line 573700
    iget-object p1, v0, LX/1ET;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    invoke-interface {p1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 573701
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/delayedworker/DelayedWorkerService;

    invoke-static {v1}, LX/1ET;->a(LX/0QB;)LX/1ET;

    move-result-object v0

    check-cast v0, LX/1ET;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {p0, v0, v1}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(LX/1ET;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x37c0745c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 573702
    const/16 v1, 0xa

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 573703
    if-nez p1, :cond_0

    .line 573704
    const/16 v1, 0x25

    const v2, -0x12a293ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 573705
    :goto_0
    return-void

    .line 573706
    :cond_0
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 573707
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 573708
    if-nez v1, :cond_1

    .line 573709
    const v1, 0x66936e4b

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0

    .line 573710
    :cond_1
    invoke-static {v1}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(Ljava/lang/String;)Lcom/facebook/delayedworker/AbstractDelayedWorker;

    move-result-object v2

    .line 573711
    if-nez v2, :cond_2

    .line 573712
    const v1, 0xafdedb6

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0

    .line 573713
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/delayedworker/DelayedWorkerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 573714
    iput-object v3, v2, Lcom/facebook/delayedworker/AbstractDelayedWorker;->a:Landroid/content/Context;

    .line 573715
    invoke-virtual {v2}, Lcom/facebook/delayedworker/AbstractDelayedWorker;->a()V

    .line 573716
    invoke-virtual {v2}, Lcom/facebook/delayedworker/AbstractDelayedWorker;->b()V

    .line 573717
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 573718
    const v1, 0x4ee395f1

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x27850184

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 573719
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 573720
    invoke-static {p0, p0}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 573721
    const/16 v1, 0x25

    const v2, 0x7e04d5ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
