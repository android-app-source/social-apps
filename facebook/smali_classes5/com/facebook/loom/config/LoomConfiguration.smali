.class public Lcom/facebook/loom/config/LoomConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pd;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/loom/config/LoomConfigurationDeserializer;
.end annotation


# instance fields
.field private final mID:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mSystemControl:Lcom/facebook/loom/config/SystemControlConfiguration;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "system_control"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mTraceControl:Lcom/facebook/loom/config/TraceControlConfiguration;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "trace_control"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 582231
    const-class v0, Lcom/facebook/loom/config/LoomConfigurationDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 582272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582273
    iput-object v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mSystemControl:Lcom/facebook/loom/config/SystemControlConfiguration;

    .line 582274
    iput-object v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mTraceControl:Lcom/facebook/loom/config/TraceControlConfiguration;

    .line 582275
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mID:J

    .line 582276
    return-void
.end method

.method public static a(Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 582240
    if-nez p0, :cond_0

    .line 582241
    :goto_0
    return v2

    .line 582242
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 582243
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    move v0, v3

    :goto_2
    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_3
    move v1, v0

    .line 582244
    goto :goto_1

    .line 582245
    :sswitch_0
    const-string v5, "async"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_2

    :sswitch_1
    const-string v5, "lifecycle"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "other"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v5, "qpl"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v5, "fbtrace"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v5, "user_counters"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v5, "system_counters"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v5, "stack_trace"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v5, "liger"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    goto :goto_2

    :sswitch_9
    const-string v5, "major_faults"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    goto :goto_2

    :sswitch_a
    const-string v5, "thread_schedule"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    goto :goto_2

    :sswitch_b
    const-string v5, "class_load"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v5, "native_stack_trace"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_2

    .line 582246
    :pswitch_0
    or-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 582247
    goto/16 :goto_1

    .line 582248
    :pswitch_1
    or-int/lit8 v0, v1, 0x2

    move v1, v0

    .line 582249
    goto/16 :goto_1

    .line 582250
    :pswitch_2
    or-int/lit8 v0, v1, 0x8

    move v1, v0

    .line 582251
    goto/16 :goto_1

    .line 582252
    :pswitch_3
    or-int/lit8 v0, v1, 0x4

    move v1, v0

    .line 582253
    goto/16 :goto_1

    .line 582254
    :pswitch_4
    or-int/lit8 v0, v1, 0x10

    move v1, v0

    .line 582255
    goto/16 :goto_1

    .line 582256
    :pswitch_5
    or-int/lit8 v0, v1, 0x20

    move v1, v0

    .line 582257
    goto/16 :goto_1

    .line 582258
    :pswitch_6
    or-int/lit8 v0, v1, 0x40

    move v1, v0

    .line 582259
    goto/16 :goto_1

    .line 582260
    :pswitch_7
    or-int/lit16 v0, v1, 0x80

    move v1, v0

    .line 582261
    goto/16 :goto_1

    .line 582262
    :pswitch_8
    or-int/lit16 v0, v1, 0x100

    move v1, v0

    .line 582263
    goto/16 :goto_1

    .line 582264
    :pswitch_9
    or-int/lit16 v0, v1, 0x200

    move v1, v0

    .line 582265
    goto/16 :goto_1

    .line 582266
    :pswitch_a
    or-int/lit16 v0, v1, 0x400

    move v1, v0

    .line 582267
    goto/16 :goto_1

    .line 582268
    :pswitch_b
    or-int/lit16 v0, v1, 0x800

    move v1, v0

    .line 582269
    goto/16 :goto_1

    .line 582270
    :pswitch_c
    or-int/lit16 v0, v1, 0x1000

    goto/16 :goto_3

    :cond_2
    move v2, v1

    .line 582271
    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4d0aef32 -> :sswitch_7
        -0x474de029 -> :sswitch_9
        -0x3e7185f7 -> :sswitch_4
        -0x240eab5a -> :sswitch_c
        -0x13293c15 -> :sswitch_5
        -0x12051896 -> :sswitch_1
        -0x115100b3 -> :sswitch_b
        0x1b62d -> :sswitch_3
        0x58d027c -> :sswitch_0
        0x62334b7 -> :sswitch_8
        0x6527f10 -> :sswitch_2
        0x6c18e12c -> :sswitch_a
        0x71782e07 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0Pg;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 582277
    iget-object v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mTraceControl:Lcom/facebook/loom/config/TraceControlConfiguration;

    return-object v0
.end method

.method public final b()Lcom/facebook/loom/config/SystemControlConfiguration;
    .locals 1

    .prologue
    .line 582239
    iget-object v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mSystemControl:Lcom/facebook/loom/config/SystemControlConfiguration;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 582233
    iget-object v1, p0, Lcom/facebook/loom/config/LoomConfiguration;->mTraceControl:Lcom/facebook/loom/config/TraceControlConfiguration;

    .line 582234
    if-nez v1, :cond_1

    .line 582235
    :cond_0
    :goto_0
    return v0

    .line 582236
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/loom/config/TraceControlConfiguration;->d()Lcom/facebook/loom/config/HTTPTraceControlConfiguration;

    move-result-object v1

    .line 582237
    if-eqz v1, :cond_0

    .line 582238
    invoke-virtual {v1}, Lcom/facebook/loom/config/HTTPTraceControlConfiguration;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 582232
    iget-wide v0, p0, Lcom/facebook/loom/config/LoomConfiguration;->mID:J

    return-wide v0
.end method
