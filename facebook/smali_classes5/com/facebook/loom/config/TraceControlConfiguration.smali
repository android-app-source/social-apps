.class public Lcom/facebook/loom/config/TraceControlConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pm;
.implements LX/0Pg;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/loom/config/TraceControlConfigurationDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Pm",
        "<",
        "Lcom/facebook/loom/config/TraceControlConfiguration;",
        ">;",
        "LX/0Pg;"
    }
.end annotation


# instance fields
.field private a:LX/0Pl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mColdStartTraceControl:Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cold_start"
    .end annotation
.end field

.field private final mHTTPTraceControl:Lcom/facebook/loom/config/HTTPTraceControlConfiguration;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "http"
    .end annotation
.end field

.field private mQPLTraceControl:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "qpl"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/loom/config/QPLTraceControlConfiguration;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mTimedOutUploadSampleRate:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timed_out_upload_sample_rate"
    .end annotation
.end field

.field private mTraceTimeousMs:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_trace_timeout_ms"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 582324
    const-class v0, Lcom/facebook/loom/config/TraceControlConfigurationDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 582325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582326
    iput-object v1, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mQPLTraceControl:LX/0P1;

    .line 582327
    iput-object v1, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mHTTPTraceControl:Lcom/facebook/loom/config/HTTPTraceControlConfiguration;

    .line 582328
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mTraceTimeousMs:I

    .line 582329
    iput-object v1, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mColdStartTraceControl:Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;

    .line 582330
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mTimedOutUploadSampleRate:I

    .line 582331
    return-void
.end method


# virtual methods
.method public final a(I)LX/0Pk;
    .locals 1

    .prologue
    .line 582332
    sparse-switch p1, :sswitch_data_0

    .line 582333
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 582334
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->a:LX/0Pl;

    goto :goto_0

    .line 582335
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mHTTPTraceControl:Lcom/facebook/loom/config/HTTPTraceControlConfiguration;

    goto :goto_0

    .line 582336
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mColdStartTraceControl:Lcom/facebook/loom/config/ColdStartTraceControlConfiguration;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
        0x10 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 582337
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mQPLTraceControl:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mQPLTraceControl:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 582338
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 582339
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mQPLTraceControl:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 582340
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 582341
    :cond_0
    new-instance v0, LX/0Pl;

    invoke-direct {v0, v2}, LX/0Pl;-><init>(Landroid/util/SparseArray;)V

    iput-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->a:LX/0Pl;

    .line 582342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mQPLTraceControl:LX/0P1;

    .line 582343
    :cond_1
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 582344
    iget v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mTraceTimeousMs:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 582345
    iget v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mTimedOutUploadSampleRate:I

    return v0
.end method

.method public final d()Lcom/facebook/loom/config/HTTPTraceControlConfiguration;
    .locals 1

    .prologue
    .line 582346
    iget-object v0, p0, Lcom/facebook/loom/config/TraceControlConfiguration;->mHTTPTraceControl:Lcom/facebook/loom/config/HTTPTraceControlConfiguration;

    return-object v0
.end method
