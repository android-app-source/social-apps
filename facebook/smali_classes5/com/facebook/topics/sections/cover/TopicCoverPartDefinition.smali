.class public Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/Ban;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/Ban;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613403
    const-class v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 613404
    new-instance v0, LX/3cG;

    invoke-direct {v0}, LX/3cG;-><init>()V

    sput-object v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613416
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613417
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;
    .locals 3

    .prologue
    .line 613405
    const-class v1, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;

    monitor-enter v1

    .line 613406
    :try_start_0
    sget-object v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613407
    sput-object v2, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613408
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613409
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 613410
    new-instance v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;

    invoke-direct {v0}, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;-><init>()V

    .line 613411
    move-object v0, v0

    .line 613412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/net/Uri;LX/Ban;)V
    .locals 15

    .prologue
    .line 613418
    invoke-virtual/range {p1 .. p1}, LX/Ban;->getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, LX/Ban;->getScreenWidth()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, LX/Ban;->getCoverPhotoHeight()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 613419
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Ban;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613400
    sget-object v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5527b9ca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613401
    check-cast p1, Landroid/net/Uri;

    check-cast p4, LX/Ban;

    invoke-static {p1, p4}, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->a(Landroid/net/Uri;LX/Ban;)V

    const/16 v1, 0x1f

    const v2, 0xe470cd1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613402
    const/4 v0, 0x1

    return v0
.end method
