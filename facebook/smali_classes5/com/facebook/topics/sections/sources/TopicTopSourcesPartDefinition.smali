.class public Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;


# instance fields
.field private final e:LX/Hdb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613450
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Hdb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613447
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 613448
    iput-object p2, p0, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->e:LX/Hdb;

    .line 613449
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;LX/1Pm;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 613431
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->e:LX/Hdb;

    const/4 v1, 0x0

    .line 613432
    new-instance v2, LX/Hda;

    invoke-direct {v2, v0}, LX/Hda;-><init>(LX/Hdb;)V

    .line 613433
    sget-object p0, LX/Hdb;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HdZ;

    .line 613434
    if-nez p0, :cond_0

    .line 613435
    new-instance p0, LX/HdZ;

    invoke-direct {p0}, LX/HdZ;-><init>()V

    .line 613436
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HdZ;->a$redex0(LX/HdZ;LX/1De;IILX/Hda;)V

    .line 613437
    move-object v2, p0

    .line 613438
    move-object v1, v2

    .line 613439
    move-object v0, v1

    .line 613440
    iget-object v1, v0, LX/HdZ;->a:LX/Hda;

    iput-object p2, v1, LX/Hda;->a:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    .line 613441
    iget-object v1, v0, LX/HdZ;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 613442
    move-object v0, v0

    .line 613443
    iget-object v1, v0, LX/HdZ;->a:LX/Hda;

    iput-object p3, v1, LX/Hda;->b:LX/1Pm;

    .line 613444
    iget-object v1, v0, LX/HdZ;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 613445
    move-object v0, v0

    .line 613446
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 613430
    check-cast p2, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 613429
    check-cast p2, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613428
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 613427
    sget-object v0, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->d:LX/1Cz;

    return-object v0
.end method

.method public final iV_()Z
    .locals 1

    .prologue
    .line 613426
    const/4 v0, 0x0

    return v0
.end method
