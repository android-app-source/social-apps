.class public final Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public extra:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "daily_dialogue_lightweight_extra"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "daily_dialogue_lightweight_unit_id"
    .end annotation
.end field

.field public type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "daily_dialogue_lightweight_unit_type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632690
    const-class v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632692
    const-class v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
