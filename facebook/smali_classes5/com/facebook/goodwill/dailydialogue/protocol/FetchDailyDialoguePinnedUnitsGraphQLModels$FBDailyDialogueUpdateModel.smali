.class public final Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x45d1e474
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632749
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632748
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 632725
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 632726
    return-void
.end method

.method private a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 632746
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    .line 632747
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 632740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 632741
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 632742
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 632743
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 632744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 632745
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 632732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 632733
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 632734
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    .line 632735
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 632736
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;

    .line 632737
    iput-object v0, v1, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel$ViewerModel;

    .line 632738
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 632739
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 632729
    new-instance v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;-><init>()V

    .line 632730
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 632731
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 632728
    const v0, 0x5921fce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 632727
    const v0, 0x76141b93

    return v0
.end method
