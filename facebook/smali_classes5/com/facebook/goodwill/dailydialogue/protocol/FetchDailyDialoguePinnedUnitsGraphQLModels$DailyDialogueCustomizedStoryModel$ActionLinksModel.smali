.class public final Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x653b52f9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 631594
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 631625
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 631623
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 631624
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 631601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 631602
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 631603
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 631604
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 631605
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 631606
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 631607
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->kq_()LX/0Px;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 631608
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->kr_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 631609
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 631610
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 631611
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 631612
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 631613
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 631614
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 631615
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 631616
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 631617
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 631618
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 631619
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 631620
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 631621
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 631622
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 631598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 631599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 631600
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631595
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 631596
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 631597
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 631591
    new-instance v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;-><init>()V

    .line 631592
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 631593
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631589
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->f:Ljava/lang/String;

    .line 631590
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631626
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->g:Ljava/lang/String;

    .line 631627
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 631587
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->h:Ljava/util/List;

    .line 631588
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 631586
    const v0, -0x39453e25

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631575
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->i:Ljava/lang/String;

    .line 631576
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 631585
    const v0, -0x6829c9fb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631583
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->l:Ljava/lang/String;

    .line 631584
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631581
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->m:Ljava/lang/String;

    .line 631582
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final kq_()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 631579
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->j:Ljava/util/List;

    .line 631580
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final kr_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631577
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->k:Ljava/lang/String;

    .line 631578
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel$ActionLinksModel;->k:Ljava/lang/String;

    return-object v0
.end method
