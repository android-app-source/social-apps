.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLUser;",
        "LX/F1H;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/17W;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0wM;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599386
    new-instance v0, LX/3ZM;

    invoke-direct {v0}, LX/3ZM;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/17W;Landroid/content/res/Resources;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599387
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599388
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->b:LX/17W;

    .line 599389
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->c:Landroid/content/res/Resources;

    .line 599390
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->d:LX/0wM;

    .line 599391
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 599392
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;
    .locals 7

    .prologue
    .line 599393
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    monitor-enter v1

    .line 599394
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599395
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599398
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;-><init>(LX/17W;Landroid/content/res/Resources;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 599399
    move-object v0, p0

    .line 599400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599404
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 599405
    check-cast p2, Lcom/facebook/graphql/model/GraphQLUser;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 599406
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v1

    .line 599407
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 599408
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    .line 599409
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f083169

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 599410
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f083167

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 599411
    :goto_3
    new-instance v6, LX/F1F;

    invoke-direct {v6, p0, p2}, LX/F1F;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;)V

    .line 599412
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v7, LX/F1G;

    invoke-direct {v7, p0, p2, v5}, LX/F1G;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;Ljava/lang/String;)V

    invoke-interface {p1, v0, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599413
    new-instance v0, LX/F1H;

    invoke-direct/range {v0 .. v6}, LX/F1H;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    move-object v2, v0

    .line 599414
    goto :goto_0

    :cond_1
    move-object v5, v0

    .line 599415
    goto :goto_1

    :cond_2
    move-object v3, v0

    .line 599416
    goto :goto_2

    :cond_3
    move-object v4, v0

    .line 599417
    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1cfe58c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599418
    check-cast p2, LX/F1H;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599419
    iget-object v1, p2, LX/F1H;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 599420
    iget-object v1, p2, LX/F1H;->e:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 599421
    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 599422
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->d:LX/0wM;

    const v2, 0x7f020740

    const p1, -0x4d4d4e

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 599423
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->c:Landroid/content/res/Resources;

    const v2, 0x7f0207fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 599424
    iget-object v1, p2, LX/F1H;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 599425
    iget-object v1, p2, LX/F1H;->d:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 599426
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setFocusable(Z)V

    .line 599427
    iget-object v1, p2, LX/F1H;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 599428
    const v1, 0x7f0e0aee

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 599429
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 599430
    iget-object v1, p2, LX/F1H;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599431
    const/16 v1, 0x1f

    const v2, 0x16cadfe3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599432
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 599433
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599434
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599435
    return-void
.end method
