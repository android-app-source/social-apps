.class public Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1XH;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/1XH;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final f:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:LX/1Uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 599579
    const v0, 0x7f0314a4

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->a:LX/1Cz;

    .line 599580
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40800000    # -1.0f

    .line 599581
    iput v1, v0, LX/1UY;->b:F

    .line 599582
    move-object v0, v0

    .line 599583
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->f:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599573
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599574
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599575
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599576
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 599577
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->e:LX/1Uf;

    .line 599578
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;
    .locals 7

    .prologue
    .line 599562
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;

    monitor-enter v1

    .line 599563
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599564
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599565
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599566
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599567
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 599568
    move-object v0, p0

    .line 599569
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599570
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599571
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 599584
    if-eqz p0, :cond_0

    .line 599585
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599586
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 599587
    :goto_0
    return v0

    .line 599588
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599589
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 599590
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 599591
    goto :goto_0

    .line 599592
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 599593
    goto :goto_0

    .line 599594
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_6

    move v0, v1

    .line 599595
    goto :goto_0

    .line 599596
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599561
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 599547
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 599548
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599549
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 599550
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->f:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599551
    const v1, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599552
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->e:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v5}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v1

    .line 599553
    const v2, 0x7f0d02c3

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599554
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "goodwill_throwback"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 599555
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 599556
    const v2, 0x7f0d0bdf

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v4

    invoke-static {v0}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v0

    .line 599557
    iput-object v1, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 599558
    move-object v0, v0

    .line 599559
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599560
    return-object v5
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 599546
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599545
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
