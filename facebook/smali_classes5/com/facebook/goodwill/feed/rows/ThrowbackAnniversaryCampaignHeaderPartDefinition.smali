.class public Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;",
        ">;",
        "LX/F0j;",
        "LX/1Ps;",
        "LX/C3y;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final e:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/1Uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 599438
    new-instance v0, LX/3ZN;

    invoke-direct {v0}, LX/3ZN;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->a:LX/1Cz;

    .line 599439
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 599440
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->e:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599441
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599442
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599443
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->d:LX/1Uf;

    .line 599444
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;
    .locals 5

    .prologue
    .line 599445
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    monitor-enter v1

    .line 599446
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599447
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599448
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599449
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599450
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-direct {p0, v3, v4}, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;)V

    .line 599451
    move-object v0, p0

    .line 599452
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599453
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599454
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599455
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599456
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 599457
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 599458
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599459
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    .line 599460
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    sget-object v4, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->e:LX/1Ua;

    sget-object v5, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v3, p2, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599461
    const/high16 v1, -0x1000000

    .line 599462
    if-eqz v0, :cond_1

    .line 599463
    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->d:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {v4}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v2}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v3

    .line 599464
    iget-object v4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->d:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {v5}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v5

    invoke-virtual {v4, v5, v6, v2}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    .line 599465
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 599466
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;->a()Ljava/lang/String;

    move-result-object v0

    .line 599467
    const/16 v7, 0x10

    invoke-static {v0, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v7

    long-to-int v7, v7

    move v0, v7

    .line 599468
    move-object v1, v2

    move-object v2, v3

    .line 599469
    :goto_0
    new-instance v3, LX/F0j;

    invoke-direct {v3, v2, v1, v0}, LX/F0j;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    return-object v3

    :cond_0
    move v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :cond_1
    move v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5ff60d3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599470
    check-cast p2, LX/F0j;

    check-cast p4, LX/C3y;

    const/4 p1, 0x0

    const/4 p0, 0x0

    .line 599471
    iget-object v1, p2, LX/F0j;->a:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/F0j;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1, v2, p1}, LX/C3y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 599472
    iget v1, p2, LX/F0j;->c:I

    invoke-virtual {p4, v1}, LX/C3y;->setContentSummaryColor(I)V

    .line 599473
    invoke-virtual {p4, p0}, LX/C3y;->setMenuButtonActive(Z)V

    .line 599474
    sget-object v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, p1, p0, v1}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 599475
    invoke-virtual {p4, p0}, LX/C3y;->setOverlapMode(Z)V

    .line 599476
    sget-object v1, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, p1, v1}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 599477
    const/16 v1, 0x1f

    const v2, -0x1c79a330

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599478
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599479
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599480
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    .line 599481
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
