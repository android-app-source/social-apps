.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;",
        ">;",
        "LX/F1C;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/17W;

.field public final e:Landroid/content/res/Resources;

.field public final f:LX/0wM;

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final h:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 599664
    const v0, 0x7f0314ab

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->a:LX/1Cz;

    .line 599665
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 599666
    iput v1, v0, LX/1UY;->b:F

    .line 599667
    move-object v0, v0

    .line 599668
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/17W;Landroid/content/res/Resources;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/17W;",
            "Landroid/content/res/Resources;",
            "LX/0wM;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599655
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599656
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->c:LX/0Or;

    .line 599657
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->d:LX/17W;

    .line 599658
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->e:Landroid/content/res/Resources;

    .line 599659
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->f:LX/0wM;

    .line 599660
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 599661
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->h:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599662
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->i:LX/0Or;

    .line 599663
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;
    .locals 11

    .prologue
    .line 599644
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    monitor-enter v1

    .line 599645
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599646
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599647
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599648
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599649
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const/16 v10, 0x1399

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;-><init>(LX/0Or;LX/17W;Landroid/content/res/Resources;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Or;)V

    .line 599650
    move-object v0, v3

    .line 599651
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599652
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599653
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599654
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599643
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 599627
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 599628
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599629
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 599630
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    .line 599631
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v1

    .line 599632
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 599633
    :goto_0
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    .line 599634
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->e:Landroid/content/res/Resources;

    const v3, 0x7f083169

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 599635
    :goto_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->e:Landroid/content/res/Resources;

    const v4, 0x7f083168

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 599636
    :cond_0
    new-instance v6, LX/F19;

    invoke-direct {v6, p0, v7}, LX/F19;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;)V

    .line 599637
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v8, LX/F1A;

    invoke-direct {v8, p0, v7, v5}, LX/F1A;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;Ljava/lang/String;)V

    invoke-interface {p1, v0, v8}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599638
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->h:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v7, LX/1X6;

    sget-object v8, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->b:LX/1Ua;

    invoke-direct {v7, p2, v8}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599639
    new-instance v0, LX/F1C;

    invoke-direct/range {v0 .. v6}, LX/F1C;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_1
    move-object v2, v4

    .line 599640
    goto :goto_0

    :cond_2
    move-object v5, v4

    .line 599641
    goto :goto_1

    :cond_3
    move-object v3, v4

    .line 599642
    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6627140c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599608
    check-cast p2, LX/F1C;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599609
    iget-object v1, p2, LX/F1C;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 599610
    iget-object v1, p2, LX/F1C;->e:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 599611
    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 599612
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->f:LX/0wM;

    const v2, 0x7f020725

    const p1, -0x4d4d4e

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 599613
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->e:Landroid/content/res/Resources;

    const v2, 0x7f0207fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 599614
    iget-object v1, p2, LX/F1C;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 599615
    iget-object v1, p2, LX/F1C;->d:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 599616
    iget-object v1, p2, LX/F1C;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 599617
    const v1, 0x7f0e0aee

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 599618
    iget-object v1, p2, LX/F1C;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599619
    const/16 v1, 0x1f

    const v2, -0x73d6e999

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599623
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599624
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599625
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 599626
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 599620
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599621
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599622
    return-void
.end method
