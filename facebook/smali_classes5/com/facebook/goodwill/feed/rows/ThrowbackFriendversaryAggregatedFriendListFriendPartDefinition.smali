.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/F14;",
        "LX/F10;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/17W;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599722
    new-instance v0, LX/3ZP;

    invoke-direct {v0}, LX/3ZP;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/17W;Landroid/content/res/Resources;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599754
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599755
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->b:LX/17W;

    .line 599756
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->c:Landroid/content/res/Resources;

    .line 599757
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->d:LX/0wM;

    .line 599758
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;
    .locals 6

    .prologue
    .line 599743
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    monitor-enter v1

    .line 599744
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599745
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599746
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599747
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599748
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;-><init>(LX/17W;Landroid/content/res/Resources;LX/0wM;)V

    .line 599749
    move-object v0, p0

    .line 599750
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599751
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599752
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599742
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 599759
    check-cast p2, LX/F14;

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    .line 599760
    iget-object v1, p2, LX/F14;->b:Lcom/facebook/graphql/model/GraphQLUser;

    move-object v7, v1

    .line 599761
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v1

    .line 599762
    iget-object v2, p2, LX/F14;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v2, v2

    .line 599763
    if-eqz v2, :cond_0

    .line 599764
    iget-object v2, p2, LX/F14;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v2, v2

    .line 599765
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 599766
    :goto_0
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    .line 599767
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f083169

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v9

    invoke-virtual {v3, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 599768
    :goto_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f083167

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v9

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 599769
    :goto_3
    new-instance v6, LX/F0z;

    invoke-direct {v6, p0, v7}, LX/F0z;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;)V

    .line 599770
    new-instance v0, LX/F10;

    invoke-direct/range {v0 .. v6}, LX/F10;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    move-object v2, v0

    .line 599771
    goto :goto_0

    :cond_1
    move-object v5, v0

    .line 599772
    goto :goto_1

    :cond_2
    move-object v3, v0

    .line 599773
    goto :goto_2

    :cond_3
    move-object v4, v0

    .line 599774
    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7ce4b460

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599730
    check-cast p2, LX/F10;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599731
    iget-object v1, p2, LX/F10;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 599732
    iget-object v1, p2, LX/F10;->e:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 599733
    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 599734
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->d:LX/0wM;

    const v2, 0x7f020740

    const p1, -0x4d4d4e

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 599735
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->c:Landroid/content/res/Resources;

    const v2, 0x7f0207fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 599736
    iget-object v1, p2, LX/F10;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 599737
    iget-object v1, p2, LX/F10;->d:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 599738
    iget-object v1, p2, LX/F10;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 599739
    const v1, 0x7f0e0aee

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 599740
    iget-object v1, p2, LX/F10;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599741
    const/16 v1, 0x1f

    const v2, 0x2693b79b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599727
    check-cast p1, LX/F14;

    .line 599728
    iget-object v0, p1, LX/F14;->b:Lcom/facebook/graphql/model/GraphQLUser;

    move-object v0, v0

    .line 599729
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 599723
    check-cast p1, LX/F14;

    check-cast p2, LX/F10;

    check-cast p3, LX/1Ps;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 599724
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 599725
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599726
    return-void
.end method
