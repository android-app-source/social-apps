.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599356
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 599357
    new-instance v0, LX/3ZJ;

    invoke-direct {v0}, LX/3ZJ;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 599358
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 599359
    const v0, 0x7f0314ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 599360
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 599361
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 599362
    const v0, 0x7f0d0738

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 599363
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 599364
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 599365
    return-void
.end method
