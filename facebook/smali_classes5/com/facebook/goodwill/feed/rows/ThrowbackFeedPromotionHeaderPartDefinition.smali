.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final f:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40a00000    # 5.0f

    .line 599484
    const v0, 0x7f0314b5

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->a:LX/1Cz;

    .line 599485
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 599486
    iput v1, v0, LX/1UY;->b:F

    .line 599487
    move-object v0, v0

    .line 599488
    iput v1, v0, LX/1UY;->c:F

    .line 599489
    move-object v0, v0

    .line 599490
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->f:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599539
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599540
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 599541
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599542
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 599543
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 599544
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;
    .locals 7

    .prologue
    .line 599528
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    monitor-enter v1

    .line 599529
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599530
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599531
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599532
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599533
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 599534
    move-object v0, p0

    .line 599535
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599536
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599537
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599538
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599527
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 599500
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599501
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599502
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 599503
    const v1, 0x7f0d2f02

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 599504
    iget-object v3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->b:Ljava/lang/String;

    move-object v3, v3

    .line 599505
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599506
    sget-object v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->f:LX/1Ua;

    .line 599507
    iget-object v2, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 599508
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 599509
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    const/high16 v2, -0x3f000000    # -8.0f

    .line 599510
    iput v2, v1, LX/1UY;->b:F

    .line 599511
    move-object v1, v1

    .line 599512
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    .line 599513
    :cond_0
    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    .line 599514
    iget-object v4, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v4, v4

    .line 599515
    invoke-virtual {p2, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    sget-object v5, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v3, v4, v1, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599516
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->e:LX/1Fb;

    move-object v1, v1

    .line 599517
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "goodwill_throwback"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 599518
    iget-object v3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->b:Ljava/lang/String;

    move-object v0, v3

    .line 599519
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    invoke-static {v1}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 599520
    const v0, 0x7f0d14b1

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599521
    const v0, 0x7f0d14b1

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v4

    invoke-static {v1}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v1

    .line 599522
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 599523
    move-object v1, v1

    .line 599524
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v0, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599525
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 599526
    :cond_1
    const v0, 0x7f0d14b1

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->e:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599491
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 599492
    if-eqz p1, :cond_0

    .line 599493
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599494
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 599495
    :goto_0
    return v0

    .line 599496
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599497
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;

    .line 599498
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackPromotionFeedUnitEdge$ThrowbackPromotionFeedUnit;->c:Lcom/facebook/graphql/model/FeedUnit;

    move-object v0, p0

    .line 599499
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
