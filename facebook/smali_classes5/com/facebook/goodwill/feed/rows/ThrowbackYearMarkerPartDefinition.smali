.class public Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599262
    new-instance v0, LX/3ZH;

    invoke-direct {v0}, LX/3ZH;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599263
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599264
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599265
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 599266
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->d:Landroid/content/res/Resources;

    .line 599267
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;
    .locals 6

    .prologue
    .line 599268
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;

    monitor-enter v1

    .line 599269
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599270
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599271
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599272
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599273
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Landroid/content/res/Resources;)V

    .line 599274
    move-object v0, p0

    .line 599275
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599276
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599277
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599278
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599279
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 599280
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/high16 v4, -0x80000000

    .line 599281
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599282
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;

    .line 599283
    const v1, 0x7f0d2f09

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599284
    iget-object v3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;->c:Ljava/lang/String;

    move-object v0, v3

    .line 599285
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599286
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b209e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 599287
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599288
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;

    .line 599289
    iget-boolean v2, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;->b:Z

    move v0, v2

    .line 599290
    if-eqz v0, :cond_0

    .line 599291
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b209d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 599292
    :goto_0
    const v1, 0x7f0d2f09

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v3, LX/1ds;

    invoke-direct {v3, v4, v0, v4, v4}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599293
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599294
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599295
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599296
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;

    .line 599297
    iget-boolean v0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
