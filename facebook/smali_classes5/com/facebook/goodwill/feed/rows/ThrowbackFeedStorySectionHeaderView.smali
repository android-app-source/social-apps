.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599597
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 599598
    new-instance v0, LX/3ZO;

    invoke-direct {v0}, LX/3ZO;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 599599
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 599600
    const v0, 0x7f0314a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 599601
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 599602
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 599603
    const v0, 0x7f0d0738

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 599604
    const v0, 0x7f0d2ee9

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->f:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 599605
    return-void
.end method
