.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/F14;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/17W;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

.field public final g:LX/0wM;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599669
    const v0, 0x7f0314ac

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/17W;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;LX/0wM;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/17W;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;",
            "LX/0wM;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599670
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599671
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->b:LX/0Or;

    .line 599672
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->c:LX/17W;

    .line 599673
    iput-object p3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 599674
    iput-object p4, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 599675
    iput-object p5, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    .line 599676
    iput-object p6, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->g:LX/0wM;

    .line 599677
    iput-object p7, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->h:LX/0Or;

    .line 599678
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;
    .locals 11

    .prologue
    .line 599679
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    monitor-enter v1

    .line 599680
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599681
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599682
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599683
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599684
    new-instance v3, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    const/16 v10, 0x1399

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;-><init>(LX/0Or;LX/17W;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;LX/0wM;LX/0Or;)V

    .line 599685
    move-object v0, v3

    .line 599686
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599687
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599688
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599690
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 599691
    check-cast p2, LX/F14;

    const/4 v2, 0x0

    .line 599692
    invoke-virtual {p2}, LX/F14;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    .line 599693
    iget-object v1, p2, LX/F14;->b:Lcom/facebook/graphql/model/GraphQLUser;

    move-object v3, v1

    .line 599694
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 599695
    :goto_0
    new-instance v4, LX/F11;

    invoke-direct {v4, p0, v3}, LX/F11;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;)V

    .line 599696
    new-instance v5, LX/F12;

    invoke-direct {v5, p0, v3, v1}, LX/F12;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;Ljava/lang/String;)V

    .line 599697
    const v1, 0x7f0d2eef

    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599698
    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599699
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    const/high16 v3, -0x3ed00000    # -11.0f

    .line 599700
    iput v3, v1, LX/1UY;->d:F

    .line 599701
    move-object v3, v1

    .line 599702
    iget-boolean v1, p2, LX/F14;->d:Z

    move v1, v1

    .line 599703
    if-eqz v1, :cond_1

    const/high16 v1, -0x3f600000    # -5.0f

    .line 599704
    :goto_1
    iput v1, v3, LX/1UY;->c:F

    .line 599705
    move-object v1, v3

    .line 599706
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    .line 599707
    iget-object v3, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v4, v0, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599708
    const v0, 0x7f0d2eee

    iget-object v1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599709
    return-object v2

    :cond_0
    move-object v1, v2

    .line 599710
    goto :goto_0

    .line 599711
    :cond_1
    const/high16 v1, -0x3ee00000    # -10.0f

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6eefdce4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599712
    check-cast p1, LX/F14;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    check-cast p4, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 599713
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 599714
    iget-boolean v1, p1, LX/F14;->d:Z

    move v1, v1

    .line 599715
    if-eqz v1, :cond_0

    .line 599716
    const/4 v1, 0x4

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 599717
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x54c8ccc0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 599718
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599719
    check-cast p1, LX/F14;

    .line 599720
    iget-object v0, p1, LX/F14;->b:Lcom/facebook/graphql/model/GraphQLUser;

    move-object v0, v0

    .line 599721
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
