.class public Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;",
        ">;",
        "Ljava/lang/Integer;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599300
    new-instance v0, LX/3ZI;

    invoke-direct {v0}, LX/3ZI;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599301
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599302
    iput-object p2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->c:Landroid/content/res/Resources;

    .line 599303
    iput-object p1, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599304
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;
    .locals 5

    .prologue
    .line 599305
    const-class v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;

    monitor-enter v1

    .line 599306
    :try_start_0
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599307
    sput-object v2, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599310
    new-instance p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 599311
    move-object v0, p0

    .line 599312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599316
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 599317
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599318
    const v1, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599319
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599320
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    .line 599321
    iget-object p3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;->a:Ljava/lang/String;

    move-object v0, p3

    .line 599322
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599323
    const v1, 0x7f0d02c3

    iget-object v2, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599324
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599325
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    .line 599326
    iget-object p3, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;->b:Ljava/lang/String;

    move-object v0, p3

    .line 599327
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 599328
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599329
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    .line 599330
    iget-boolean v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;->c:Z

    move v0, v1

    .line 599331
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b20de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b20dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6c323b73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599332
    check-cast p2, Ljava/lang/Integer;

    const/4 p1, 0x0

    .line 599333
    const v1, 0x7f0d2ee8

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 599334
    if-eqz v2, :cond_0

    .line 599335
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 599336
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v1, p1, p0, p1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 599337
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 599338
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x5c578bef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 599339
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 599340
    if-eqz p1, :cond_0

    .line 599341
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599342
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 599343
    :goto_0
    return v0

    .line 599344
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599345
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    .line 599346
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;->a:Ljava/lang/String;

    move-object v0, p0

    .line 599347
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 599348
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599349
    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;

    .line 599350
    iget-object p0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackSectionHeaderFeedUnitEdge$ThrowbackSectionHeaderFeedUnit;->b:Ljava/lang/String;

    move-object v0, p0

    .line 599351
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 599352
    goto :goto_0

    .line 599353
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
