.class public Lcom/facebook/compactdisk/StalePruningConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571176
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571177
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571179
    invoke-direct {p0}, Lcom/facebook/compactdisk/StalePruningConfig;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/StalePruningConfig;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571180
    return-void
.end method

.method private native initHybrid()Lcom/facebook/jni/HybridData;
.end method

.method private native staleAgeNative(J)V
.end method


# virtual methods
.method public final a(J)Lcom/facebook/compactdisk/StalePruningConfig;
    .locals 1

    .prologue
    .line 571181
    invoke-direct {p0, p1, p2}, Lcom/facebook/compactdisk/StalePruningConfig;->staleAgeNative(J)V

    .line 571182
    return-object p0
.end method
