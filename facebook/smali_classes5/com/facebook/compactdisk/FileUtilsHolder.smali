.class public Lcom/facebook/compactdisk/FileUtilsHolder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571072
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571073
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571075
    invoke-direct {p0}, Lcom/facebook/compactdisk/FileUtilsHolder;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/FileUtilsHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571076
    return-void
.end method

.method private native initHybrid()Lcom/facebook/jni/HybridData;
.end method
