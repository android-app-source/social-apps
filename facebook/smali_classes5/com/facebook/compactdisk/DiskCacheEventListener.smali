.class public interface abstract Lcom/facebook/compactdisk/DiskCacheEventListener;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# virtual methods
.method public abstract onEviction(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public abstract onHit(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public abstract onMiss(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public abstract onReadException(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public abstract onWrite(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method public abstract onWriteException(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
