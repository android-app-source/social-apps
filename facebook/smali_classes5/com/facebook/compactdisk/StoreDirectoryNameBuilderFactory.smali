.class public Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571183
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571184
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 571185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571186
    const-string v0, "ijxLJi1yGs1JpL-X1SExmchvork"

    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;->initHybrid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571187
    return-void
.end method

.method private native initHybrid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/jni/HybridData;
.end method
