.class public Lcom/facebook/compactdisk/UnmanagedStore;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571227
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571228
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 571229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571230
    iput-object p1, p0, Lcom/facebook/compactdisk/UnmanagedStore;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571231
    return-void
.end method


# virtual methods
.method public native getDirectoryPath()Ljava/lang/String;
.end method

.method public native getMaxSize()J
.end method
