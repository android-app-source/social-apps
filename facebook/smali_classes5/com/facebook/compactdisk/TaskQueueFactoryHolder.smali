.class public Lcom/facebook/compactdisk/TaskQueueFactoryHolder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571217
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571218
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .prologue
    .line 571219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571220
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;->initHybrid(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571221
    return-void
.end method

.method private native initHybrid(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/facebook/jni/HybridData;
.end method
