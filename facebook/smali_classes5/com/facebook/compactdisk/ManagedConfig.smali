.class public Lcom/facebook/compactdisk/ManagedConfig;
.super Lcom/facebook/compactdisk/SubConfig;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571155
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571156
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571153
    invoke-static {}, Lcom/facebook/compactdisk/ManagedConfig;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/compactdisk/SubConfig;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 571154
    return-void
.end method

.method private native eventListenerPairsNative([Lcom/facebook/compactdisk/DiskCacheEventListener;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;)V
.end method

.method private native evictionNative(Lcom/facebook/compactdisk/EvictionConfig;)V
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method

.method private native inlineDataNative(Z)V
.end method

.method private native stalePruningNative(Lcom/facebook/compactdisk/StalePruningConfig;)V
.end method


# virtual methods
.method public final a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;
    .locals 0

    .prologue
    .line 571151
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/ManagedConfig;->evictionNative(Lcom/facebook/compactdisk/EvictionConfig;)V

    .line 571152
    return-object p0
.end method

.method public final a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;
    .locals 0

    .prologue
    .line 571149
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/ManagedConfig;->stalePruningNative(Lcom/facebook/compactdisk/StalePruningConfig;)V

    .line 571150
    return-object p0
.end method

.method public final a(Z)Lcom/facebook/compactdisk/ManagedConfig;
    .locals 0

    .prologue
    .line 571147
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/ManagedConfig;->inlineDataNative(Z)V

    .line 571148
    return-object p0
.end method

.method public final a([Lcom/facebook/compactdisk/DiskCacheEventListener;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/compactdisk/ManagedConfig;
    .locals 1

    .prologue
    .line 571145
    new-instance v0, Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    invoke-direct {v0, p2}, Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    invoke-direct {p0, p1, v0}, Lcom/facebook/compactdisk/ManagedConfig;->eventListenerPairsNative([Lcom/facebook/compactdisk/DiskCacheEventListener;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;)V

    .line 571146
    return-object p0
.end method
