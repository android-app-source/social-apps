.class public Lcom/facebook/compactdisk/DiskSizeCalculator;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571122
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571123
    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)V
    .locals 1

    .prologue
    .line 571127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571128
    invoke-static {p1}, Lcom/facebook/compactdisk/DiskSizeCalculator;->initHybrid(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/DiskSizeCalculator;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571129
    return-void
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 571124
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 571125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "On UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571126
    :cond_0
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)Lcom/facebook/jni/HybridData;
.end method

.method private native startNativeDiskSizeCalculation()V
.end method

.method private native startNativeRandomDiskSizeCalculation()V
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 571119
    invoke-static {}, Lcom/facebook/compactdisk/DiskSizeCalculator;->b()V

    .line 571120
    invoke-direct {p0}, Lcom/facebook/compactdisk/DiskSizeCalculator;->startNativeRandomDiskSizeCalculation()V

    .line 571121
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 571117
    const-wide/32 v0, 0x15180

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/compactdisk/DiskSizeCalculator;->addDirectory(Ljava/lang/String;J)V

    .line 571118
    return-void
.end method

.method public native addDirectory(Ljava/lang/String;J)V
.end method

.method public native fetchDirectorySize(Ljava/lang/String;)J
.end method

.method public native setAnalytics(Z)V
.end method
