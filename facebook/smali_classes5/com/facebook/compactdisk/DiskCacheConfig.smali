.class public Lcom/facebook/compactdisk/DiskCacheConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571100
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571101
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571098
    invoke-direct {p0}, Lcom/facebook/compactdisk/DiskCacheConfig;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/DiskCacheConfig;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571099
    return-void
.end method

.method private native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native getName()Ljava/lang/String;
.end method

.method public native getSessionScoped()Z
.end method

.method public native localeSensitive(Z)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native maxCapacity(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method

.method public native version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;
.end method
