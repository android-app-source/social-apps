.class public final enum Lcom/facebook/compactdisk/DiskArea;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/compactdisk/DiskArea;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/compactdisk/DiskArea;

.field public static final enum CACHES:Lcom/facebook/compactdisk/DiskArea;

.field public static final enum DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 572323
    new-instance v0, Lcom/facebook/compactdisk/DiskArea;

    const-string v1, "CACHES"

    invoke-direct {v0, v1, v2}, Lcom/facebook/compactdisk/DiskArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    new-instance v0, Lcom/facebook/compactdisk/DiskArea;

    const-string v1, "DOCUMENTS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/compactdisk/DiskArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/compactdisk/DiskArea;->DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;

    .line 572324
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/compactdisk/DiskArea;

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/compactdisk/DiskArea;->$VALUES:[Lcom/facebook/compactdisk/DiskArea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 572326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskArea;
    .locals 1

    .prologue
    .line 572327
    const-class v0, Lcom/facebook/compactdisk/DiskArea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskArea;

    return-object v0
.end method

.method public static values()[Lcom/facebook/compactdisk/DiskArea;
    .locals 1

    .prologue
    .line 572325
    sget-object v0, Lcom/facebook/compactdisk/DiskArea;->$VALUES:[Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/compactdisk/DiskArea;

    return-object v0
.end method
