.class public Lcom/facebook/compactdisk/EvictionConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571130
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571131
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 571132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571133
    invoke-direct {p0}, Lcom/facebook/compactdisk/EvictionConfig;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/compactdisk/EvictionConfig;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571134
    return-void
.end method

.method private native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native evictionComparator(Lcom/facebook/compactdisk/EvictionComparator;)Lcom/facebook/compactdisk/EvictionConfig;
.end method

.method public native lowSpaceMaxSize(J)Lcom/facebook/compactdisk/EvictionConfig;
.end method

.method public native maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;
.end method

.method public native strictEnforcement(Z)Lcom/facebook/compactdisk/EvictionConfig;
.end method
