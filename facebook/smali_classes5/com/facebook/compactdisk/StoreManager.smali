.class public Lcom/facebook/compactdisk/StoreManager;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/2O7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2O7",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/compactdisk/UnmanagedStore;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/2O7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2O7",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/compactdisk/PersistentKeyValueStore;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/2OA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2OA",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/compactdisk/DiskCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571215
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571216
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 571209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571210
    iput-object p1, p0, Lcom/facebook/compactdisk/StoreManager;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571211
    new-instance v0, LX/2O7;

    new-instance v1, LX/2OE;

    invoke-direct {v1, p0}, LX/2OE;-><init>(Lcom/facebook/compactdisk/StoreManager;)V

    invoke-direct {v0, v1}, LX/2O7;-><init>(LX/2O9;)V

    iput-object v0, p0, Lcom/facebook/compactdisk/StoreManager;->a:LX/2O7;

    .line 571212
    new-instance v0, LX/2O7;

    new-instance v1, LX/2OF;

    invoke-direct {v1, p0}, LX/2OF;-><init>(Lcom/facebook/compactdisk/StoreManager;)V

    invoke-direct {v0, v1}, LX/2O7;-><init>(LX/2O9;)V

    iput-object v0, p0, Lcom/facebook/compactdisk/StoreManager;->b:LX/2O7;

    .line 571213
    new-instance v0, LX/2OA;

    invoke-direct {v0}, LX/2OA;-><init>()V

    iput-object v0, p0, Lcom/facebook/compactdisk/StoreManager;->c:LX/2OA;

    .line 571214
    return-void
.end method

.method public static synthetic a(Lcom/facebook/compactdisk/StoreManager;Ljava/lang/String;Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;
    .locals 1

    .prologue
    .line 571208
    invoke-direct {p0, p1, p2}, Lcom/facebook/compactdisk/StoreManager;->createDiskCache(Ljava/lang/String;Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/compactdisk/StoreManager;Ljava/lang/String;)Lcom/facebook/compactdisk/UnmanagedStore;
    .locals 1

    .prologue
    .line 571207
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/StoreManager;->createUnmanagedStore(Ljava/lang/String;)Lcom/facebook/compactdisk/UnmanagedStore;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/facebook/compactdisk/StoreManager;Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;
    .locals 1

    .prologue
    .line 571206
    invoke-direct {p0, p1}, Lcom/facebook/compactdisk/StoreManager;->createPersistentKeyValueStore(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    return-object v0
.end method

.method private native createDiskCache(Ljava/lang/String;Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;
.end method

.method private native createPersistentKeyValueStore(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;
.end method

.method private native createUnmanagedStore(Ljava/lang/String;)Lcom/facebook/compactdisk/UnmanagedStore;
.end method


# virtual methods
.method public final a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;
    .locals 3

    .prologue
    .line 571204
    invoke-virtual {p1}, Lcom/facebook/compactdisk/DiskCacheConfig;->getName()Ljava/lang/String;

    move-result-object v0

    .line 571205
    iget-object v1, p0, Lcom/facebook/compactdisk/StoreManager;->c:LX/2OA;

    new-instance v2, LX/2OG;

    invoke-direct {v2, p0, v0, p1}, LX/2OG;-><init>(Lcom/facebook/compactdisk/StoreManager;Ljava/lang/String;Lcom/facebook/compactdisk/DiskCacheConfig;)V

    invoke-virtual {v1, v0, v2}, LX/2OA;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/DiskCache;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;
    .locals 1

    .prologue
    .line 571203
    iget-object v0, p0, Lcom/facebook/compactdisk/StoreManager;->b:LX/2O7;

    invoke-virtual {v0, p1}, LX/2O7;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/PersistentKeyValueStore;

    return-object v0
.end method
