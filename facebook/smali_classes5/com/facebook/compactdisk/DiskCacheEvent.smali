.class public Lcom/facebook/compactdisk/DiskCacheEvent;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571102
    const-string v0, "compactdisk-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571103
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 571104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571105
    iput-object p1, p0, Lcom/facebook/compactdisk/DiskCacheEvent;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 571106
    return-void
.end method


# virtual methods
.method public native getAge()Ljava/lang/Long;
.end method

.method public native getCacheSize()Ljava/lang/Long;
.end method

.method public native getEvictionReason()Lcom/facebook/compactdisk/EvictionReason;
.end method

.method public native getException()Ljava/io/IOException;
.end method

.method public native getKey()Ljava/lang/String;
.end method

.method public native getName()Ljava/lang/String;
.end method

.method public native getSize()Ljava/lang/Long;
.end method

.method public native getTag()Ljava/lang/String;
.end method
