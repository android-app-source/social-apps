.class public final Lcom/facebook/fbservice/ops/BlueServiceOperation$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/4Al;

.field public final synthetic val$result:Lcom/facebook/fbservice/service/OperationResult;


# direct methods
.method public constructor <init>(LX/4Al;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 0

    .prologue
    .line 676891
    iput-object p1, p0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;->this$0:LX/4Al;

    iput-object p2, p0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;->val$result:Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 676892
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;->this$0:LX/4Al;

    iget-boolean v0, v0, LX/4Al;->disposed:Z

    if-nez v0, :cond_2

    .line 676893
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;->this$0:LX/4Al;

    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceOperation$2;->val$result:Lcom/facebook/fbservice/service/OperationResult;

    .line 676894
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 676895
    if-eqz v2, :cond_3

    .line 676896
    sget-object v2, LX/4Ak;->COMPLETED:LX/4Ak;

    iput-object v2, v0, LX/4Al;->operationState:LX/4Ak;

    .line 676897
    const/4 v2, 0x0

    iput-object v2, v0, LX/4Al;->operationId:Ljava/lang/String;

    .line 676898
    invoke-static {v0}, LX/4Al;->stopShowingProgress(LX/4Al;)V

    .line 676899
    iget-boolean v2, v0, LX/4Al;->autoReset:Z

    if-eqz v2, :cond_0

    .line 676900
    invoke-virtual {v0}, LX/4Al;->reset()V

    .line 676901
    :cond_0
    iget-object v2, v0, LX/4Al;->onCompletedListener:LX/4Ae;

    if-eqz v2, :cond_1

    .line 676902
    iget-object v2, v0, LX/4Al;->onCompletedListener:LX/4Ae;

    invoke-virtual {v2, v1}, LX/4Ae;->onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 676903
    :cond_1
    iget-boolean v2, v0, LX/4Al;->autoDispose:Z

    if-eqz v2, :cond_2

    .line 676904
    invoke-virtual {v0}, LX/4Al;->dispose()V

    .line 676905
    :cond_2
    :goto_0
    return-void

    .line 676906
    :cond_3
    new-instance v2, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {v2, v1}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 676907
    sget-object v3, LX/4Ak;->COMPLETED:LX/4Ak;

    iput-object v3, v0, LX/4Al;->operationState:LX/4Ak;

    .line 676908
    const/4 v3, 0x0

    iput-object v3, v0, LX/4Al;->operationId:Ljava/lang/String;

    .line 676909
    invoke-static {v0}, LX/4Al;->stopShowingProgress(LX/4Al;)V

    .line 676910
    const/4 p0, 0x0

    .line 676911
    iget-object v3, v0, LX/4Al;->context:Landroid/content/Context;

    const-class v1, LX/0ev;

    invoke-static {v3, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ev;

    .line 676912
    if-eqz v3, :cond_7

    .line 676913
    invoke-interface {v3, v2}, LX/0ev;->a(Ljava/lang/Throwable;)Z

    move-result v3

    .line 676914
    :goto_1
    iget-boolean p0, v0, LX/4Al;->autoReset:Z

    if-eqz p0, :cond_4

    .line 676915
    invoke-virtual {v0}, LX/4Al;->reset()V

    .line 676916
    :cond_4
    if-nez v3, :cond_5

    iget-object v3, v0, LX/4Al;->onCompletedListener:LX/4Ae;

    if-eqz v3, :cond_5

    .line 676917
    iget-object v3, v0, LX/4Al;->onCompletedListener:LX/4Ae;

    invoke-virtual {v3, v2}, LX/4Ae;->onFailed(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 676918
    :cond_5
    iget-boolean v3, v0, LX/4Al;->autoDispose:Z

    if-eqz v3, :cond_6

    .line 676919
    invoke-virtual {v0}, LX/4Al;->dispose()V

    .line 676920
    :cond_6
    goto :goto_0

    :cond_7
    move v3, p0

    goto :goto_1
.end method
