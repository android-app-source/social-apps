.class public Lcom/facebook/fbservice/ops/BlueServiceFragment;
.super Landroid/support/v4/app/Fragment;
.source ""


# instance fields
.field public initialized:Z

.field public mBlueServiceOperation:LX/4Al;

.field public onCompletedListener:LX/4Ae;

.field public onProgressListener:LX/4Ag;

.field private operationProgressIndicator:LX/4At;

.field public queuedCallerContext:Lcom/facebook/common/callercontext/CallerContext;

.field public queuedOperationType:Ljava/lang/String;

.field public queuedParam:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 676845
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;
    .locals 2

    .prologue
    .line 676838
    invoke-virtual {p0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 676839
    if-nez v0, :cond_0

    .line 676840
    new-instance v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;

    invoke-direct {v0}, Lcom/facebook/fbservice/ops/BlueServiceFragment;-><init>()V

    .line 676841
    invoke-virtual {p0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 676842
    invoke-virtual {v1, v0, p1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 676843
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 676844
    :cond_0
    return-object v0
.end method

.method public static create(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;
    .locals 1

    .prologue
    .line 676809
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(LX/0gc;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getOnCompletedListener()LX/4Ae;
    .locals 1

    .prologue
    .line 676837
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    return-object v0
.end method

.method public getOnProgressListener()LX/4Ag;
    .locals 1

    .prologue
    .line 676836
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onProgressListener:LX/4Ag;

    return-object v0
.end method

.method public getOperationProgressIndicator()LX/4At;
    .locals 1

    .prologue
    .line 676832
    iget-boolean v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->initialized:Z

    if-eqz v0, :cond_0

    .line 676833
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676834
    iget-object p0, v0, LX/4Al;->operationProgressIndicator:LX/4At;

    move-object v0, p0

    .line 676835
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->operationProgressIndicator:LX/4At;

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 2

    .prologue
    .line 676829
    iget-boolean v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->initialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676830
    iget-object v1, v0, LX/4Al;->operationState:LX/4Ak;

    sget-object p0, LX/4Ak;->INIT:LX/4Ak;

    if-eq v1, p0, :cond_1

    iget-object v1, v0, LX/4Al;->operationState:LX/4Ak;

    sget-object p0, LX/4Ak;->COMPLETED:LX/4Ak;

    if-eq v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 676831
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, 0x2b501f93

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 676846
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 676847
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676848
    iput-boolean v3, v1, LX/4Al;->autoReset:Z

    .line 676849
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    new-instance v2, LX/4Af;

    invoke-direct {v2, p0}, LX/4Af;-><init>(Lcom/facebook/fbservice/ops/BlueServiceFragment;)V

    .line 676850
    iput-object v2, v1, LX/4Al;->onCompletedListener:LX/4Ae;

    .line 676851
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    new-instance v2, LX/4Ah;

    invoke-direct {v2, p0}, LX/4Ah;-><init>(Lcom/facebook/fbservice/ops/BlueServiceFragment;)V

    .line 676852
    iput-object v2, v1, LX/4Al;->onProgressListener:LX/4Ag;

    .line 676853
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedOperationType:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 676854
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676855
    const-string v2, "operationState"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/4Ak;

    iput-object v2, v1, LX/4Al;->operationState:LX/4Ak;

    .line 676856
    const-string v2, "type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/4Al;->operationType:Ljava/lang/String;

    .line 676857
    const-string v2, "param"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    iput-object v2, v1, LX/4Al;->param:Landroid/os/Bundle;

    .line 676858
    const-string v2, "callerContext"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v2, v1, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 676859
    const-string v2, "operationId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/4Al;->operationId:Ljava/lang/String;

    .line 676860
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 676861
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, v1, LX/4Al;->handler:Landroid/os/Handler;

    .line 676862
    :cond_0
    iget-object v2, v1, LX/4Al;->operationState:LX/4Ak;

    sget-object v4, LX/4Ak;->INIT:LX/4Ak;

    if-eq v2, v4, :cond_1

    .line 676863
    iget-object v2, v1, LX/4Al;->operationState:LX/4Ak;

    sget-object v4, LX/4Ak;->READY_TO_QUEUE:LX/4Ak;

    if-ne v2, v4, :cond_3

    .line 676864
    invoke-static {v1}, LX/4Al;->beginShowingProgress(LX/4Al;)V

    .line 676865
    invoke-static {v1}, LX/4Al;->bindToService(LX/4Al;)V

    .line 676866
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    iget-object v2, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->operationProgressIndicator:LX/4At;

    invoke-virtual {v1, v2}, LX/4Al;->setOperationProgressIndicator(LX/4At;)V

    .line 676867
    iput-object v5, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->operationProgressIndicator:LX/4At;

    .line 676868
    iput-boolean v3, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->initialized:Z

    .line 676869
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedOperationType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 676870
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    iget-object v2, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedOperationType:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedParam:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3, v4}, LX/4Al;->start(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 676871
    iput-object v5, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedOperationType:Ljava/lang/String;

    .line 676872
    iput-object v5, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedParam:Landroid/os/Bundle;

    .line 676873
    iput-object v5, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 676874
    :cond_2
    const/16 v1, 0x2b

    const v2, -0xdaf510e

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 676875
    :cond_3
    iget-object v2, v1, LX/4Al;->operationState:LX/4Ak;

    sget-object v4, LX/4Ak;->OPERATION_QUEUED:LX/4Ak;

    if-ne v2, v4, :cond_4

    .line 676876
    invoke-static {v1}, LX/4Al;->beginShowingProgress(LX/4Al;)V

    .line 676877
    invoke-static {v1}, LX/4Al;->bindToService(LX/4Al;)V

    goto :goto_0

    .line 676878
    :cond_4
    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 676825
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 676826
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v0, v0

    .line 676827
    invoke-static {v0}, LX/4Al;->createInstance__com_facebook_fbservice_ops_BlueServiceOperation__INJECTED_BY_TemplateInjector(LX/0QB;)LX/4Al;

    move-result-object v0

    check-cast v0, LX/4Al;

    iput-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676828
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x33f4be5d    # -3.6505228E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 676820
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 676821
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    invoke-virtual {v1}, LX/4Al;->dispose()V

    .line 676822
    iput-object v2, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 676823
    iput-object v2, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onProgressListener:LX/4Ag;

    .line 676824
    const/16 v1, 0x2b

    const v2, 0x1a9f698f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 676812
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 676813
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    .line 676814
    const-string v1, "operationState"

    iget-object p0, v0, LX/4Al;->operationState:LX/4Ak;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 676815
    const-string v1, "type"

    iget-object p0, v0, LX/4Al;->operationType:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 676816
    const-string v1, "param"

    iget-object p0, v0, LX/4Al;->param:Landroid/os/Bundle;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 676817
    const-string v1, "callerContext"

    iget-object p0, v0, LX/4Al;->callerContext:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 676818
    const-string v1, "operationId"

    iget-object p0, v0, LX/4Al;->operationId:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 676819
    return-void
.end method

.method public setOnProgressListener(LX/4Ag;)V
    .locals 0

    .prologue
    .line 676810
    iput-object p1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onProgressListener:LX/4Ag;

    .line 676811
    return-void
.end method

.method public setOperationProgressIndicator(LX/4At;)V
    .locals 1

    .prologue
    .line 676805
    iget-boolean v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->initialized:Z

    if-eqz v0, :cond_0

    .line 676806
    iget-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    invoke-virtual {v0, p1}, LX/4Al;->setOperationProgressIndicator(LX/4At;)V

    .line 676807
    :goto_0
    return-void

    .line 676808
    :cond_0
    iput-object p1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->operationProgressIndicator:LX/4At;

    goto :goto_0
.end method

.method public start(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676798
    const/4 v0, 0x0

    .line 676799
    iget-boolean v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->initialized:Z

    if-eqz v1, :cond_0

    .line 676800
    iget-object v1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->mBlueServiceOperation:LX/4Al;

    invoke-virtual {v1, p1, p2, v0}, LX/4Al;->start(Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 676801
    :goto_0
    return-void

    .line 676802
    :cond_0
    iput-object p1, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedOperationType:Ljava/lang/String;

    .line 676803
    iput-object p2, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedParam:Landroid/os/Bundle;

    .line 676804
    iput-object v0, p0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->queuedCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method
