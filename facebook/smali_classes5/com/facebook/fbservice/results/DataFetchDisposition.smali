.class public Lcom/facebook/fbservice/results/DataFetchDisposition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbservice/results/DataFetchDisposition;",
            ">;"
        }
    .end annotation
.end field

.field public static final FROM_CACHE_HAD_SERVER_ERROR:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_CACHE_INCOMPLETE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_IN_MEMORY_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_IN_MEMORY_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_LOCAL_DISK_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public static final NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;


# instance fields
.field public final dataSource:LX/4B1;

.field public final fellbackToCachedDataAfterFailedToHitServer:LX/03R;

.field public final fromAuthoritativeData:LX/03R;

.field public final hasData:Z

.field public final isIncompleteData:LX/03R;

.field public final isStaleData:LX/03R;

.field public final needsInitialFetch:LX/03R;

.field public final wasFetchSynchronous:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 677234
    new-instance v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-direct {v0}, Lcom/facebook/fbservice/results/DataFetchDisposition;-><init>()V

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677235
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->SERVER:LX/4B1;

    .line 677236
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677237
    move-object v0, v0

    .line 677238
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677239
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677240
    move-object v0, v0

    .line 677241
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677242
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677243
    move-object v0, v0

    .line 677244
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677245
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677246
    move-object v0, v0

    .line 677247
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677248
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->IN_MEMORY_CACHE:LX/4B1;

    .line 677249
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677250
    move-object v0, v0

    .line 677251
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677252
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677253
    move-object v0, v0

    .line 677254
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677255
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677256
    move-object v0, v0

    .line 677257
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677258
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677259
    move-object v0, v0

    .line 677260
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677261
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->IN_MEMORY_CACHE:LX/4B1;

    .line 677262
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677263
    move-object v0, v0

    .line 677264
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677265
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677266
    move-object v0, v0

    .line 677267
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677268
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677269
    move-object v0, v0

    .line 677270
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677271
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677272
    move-object v0, v0

    .line 677273
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677274
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    .line 677275
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677276
    move-object v0, v0

    .line 677277
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677278
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677279
    move-object v0, v0

    .line 677280
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677281
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677282
    move-object v0, v0

    .line 677283
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677284
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677285
    move-object v0, v0

    .line 677286
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677287
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    .line 677288
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677289
    move-object v0, v0

    .line 677290
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677291
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677292
    move-object v0, v0

    .line 677293
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677294
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677295
    move-object v0, v0

    .line 677296
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677297
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677298
    move-object v0, v0

    .line 677299
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677300
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    .line 677301
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677302
    move-object v0, v0

    .line 677303
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677304
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677305
    move-object v0, v0

    .line 677306
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677307
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677308
    move-object v0, v0

    .line 677309
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677310
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677311
    move-object v0, v0

    .line 677312
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677313
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    .line 677314
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677315
    move-object v0, v0

    .line 677316
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677317
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677318
    move-object v0, v0

    .line 677319
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677320
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677321
    move-object v0, v0

    .line 677322
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677323
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677324
    move-object v0, v0

    .line 677325
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677326
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    .line 677327
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677328
    move-object v0, v0

    .line 677329
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677330
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677331
    move-object v0, v0

    .line 677332
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677333
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677334
    move-object v0, v0

    .line 677335
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677336
    iput-object v1, v0, LX/4B2;->mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677337
    move-object v0, v0

    .line 677338
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677339
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677340
    move-object v0, v0

    .line 677341
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_CACHE_HAD_SERVER_ERROR:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677342
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_UNSPECIFIED_CACHE:LX/4B1;

    .line 677343
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677344
    move-object v0, v0

    .line 677345
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677346
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677347
    move-object v0, v0

    .line 677348
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677349
    iput-object v1, v0, LX/4B2;->mIsIncompleteData:LX/03R;

    .line 677350
    move-object v0, v0

    .line 677351
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677352
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677353
    move-object v0, v0

    .line 677354
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_CACHE_INCOMPLETE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677355
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->SMS:LX/4B1;

    .line 677356
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677357
    move-object v0, v0

    .line 677358
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 677359
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677360
    move-object v0, v0

    .line 677361
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677362
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677363
    move-object v0, v0

    .line 677364
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 677365
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677366
    move-object v0, v0

    .line 677367
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677368
    new-instance v0, LX/4B0;

    invoke-direct {v0}, LX/4B0;-><init>()V

    sput-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 677224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    .line 677226
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    .line 677227
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    .line 677228
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    .line 677229
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    .line 677230
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677231
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    .line 677232
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    .line 677233
    return-void
.end method

.method public constructor <init>(LX/4B2;)V
    .locals 1

    .prologue
    .line 677207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    .line 677209
    iget-object v0, p1, LX/4B2;->mDataSource:LX/4B1;

    move-object v0, v0

    .line 677210
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4B1;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    .line 677211
    iget-object v0, p1, LX/4B2;->mFromAuthoritativeData:LX/03R;

    move-object v0, v0

    .line 677212
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    .line 677213
    iget-object v0, p1, LX/4B2;->mIsStaleData:LX/03R;

    move-object v0, v0

    .line 677214
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    .line 677215
    iget-object v0, p1, LX/4B2;->mIsIncompleteData:LX/03R;

    move-object v0, v0

    .line 677216
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    .line 677217
    iget-object v0, p1, LX/4B2;->mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

    move-object v0, v0

    .line 677218
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677219
    iget-object v0, p1, LX/4B2;->mNeedsInitialFetch:LX/03R;

    move-object v0, v0

    .line 677220
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    .line 677221
    iget-object v0, p1, LX/4B2;->mWasFetchSynchronous:LX/03R;

    move-object v0, v0

    .line 677222
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    .line 677223
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 677197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677198
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    .line 677199
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/4B1;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    .line 677200
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    .line 677201
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    .line 677202
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    .line 677203
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677204
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    .line 677205
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    .line 677206
    return-void
.end method

.method public static buildCombinedDisposition(Ljava/util/List;)Lcom/facebook/fbservice/results/DataFetchDisposition;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/fbservice/results/DataFetchDisposition;",
            ">;)",
            "Lcom/facebook/fbservice/results/DataFetchDisposition;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 677369
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677370
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677371
    :goto_0
    return-object v0

    .line 677372
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 677373
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    goto :goto_0

    .line 677374
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 677375
    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    if-eq v0, v4, :cond_2

    move v0, v1

    .line 677376
    :goto_1
    if-eqz v0, :cond_3

    .line 677377
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    goto :goto_0

    .line 677378
    :cond_3
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    .line 677379
    sget-object v1, LX/4B1;->COMPOSED:LX/4B1;

    .line 677380
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 677381
    new-instance v1, LX/4Au;

    invoke-direct {v1}, LX/4Au;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->a:LX/47a;

    sget-object v3, LX/03R;->NO:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677382
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677383
    new-instance v1, LX/4Av;

    invoke-direct {v1}, LX/4Av;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->b:LX/47a;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677384
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677385
    new-instance v1, LX/4Aw;

    invoke-direct {v1}, LX/4Aw;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->b:LX/47a;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677386
    iput-object v1, v0, LX/4B2;->mIsIncompleteData:LX/03R;

    .line 677387
    new-instance v1, LX/4Ax;

    invoke-direct {v1}, LX/4Ax;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->b:LX/47a;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677388
    iput-object v1, v0, LX/4B2;->mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677389
    new-instance v1, LX/4Ay;

    invoke-direct {v1}, LX/4Ay;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->b:LX/47a;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677390
    iput-object v1, v0, LX/4B2;->mNeedsInitialFetch:LX/03R;

    .line 677391
    new-instance v1, LX/4Az;

    invoke-direct {v1}, LX/4Az;-><init>()V

    invoke-static {p0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    sget-object v2, LX/480;->b:LX/47a;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-static {v1, v2, v3}, LX/480;->a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;

    move-result-object v1

    .line 677392
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677393
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1
.end method

.method public static newBuilder()LX/4B2;
    .locals 1

    .prologue
    .line 677185
    new-instance v0, LX/4B2;

    invoke-direct {v0}, LX/4B2;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 677196
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 677195
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "dataSource"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fromAuthoritativeData"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isStaleData"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isIncompleteData"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fellbackToCachedDataAfterFailedToHitServer"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "needsInitialFetch"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "wasFetchSynchronous"

    iget-object v2, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 677186
    iget-boolean v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 677187
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677188
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677189
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677190
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677191
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677192
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677193
    iget-object v0, p0, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 677194
    return-void
.end method
