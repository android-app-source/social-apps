.class public Lcom/facebook/fbservice/service/ServiceException;
.super Ljava/lang/Exception;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbservice/service/ServiceException;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final errorCode:LX/1nY;

.field public final result:Lcom/facebook/fbservice/service/OperationResult;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 570641
    new-instance v0, LX/3d8;

    invoke-direct {v0}, LX/3d8;-><init>()V

    sput-object v0, Lcom/facebook/fbservice/service/ServiceException;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 570664
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 570665
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/1nY;

    iput-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    .line 570666
    const-class v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    iput-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    .line 570667
    return-void
.end method

.method public constructor <init>(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 570653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 570654
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v1, v1

    .line 570655
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 570656
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v1, v1

    .line 570657
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 570658
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v1, v1

    .line 570659
    invoke-direct {p0, v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 570660
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v0, v0

    .line 570661
    iput-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    .line 570662
    iput-object p1, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    .line 570663
    return-void
.end method

.method public static forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;
    .locals 2

    .prologue
    .line 570648
    instance-of v0, p0, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 570649
    check-cast p0, Lcom/facebook/fbservice/service/ServiceException;

    .line 570650
    :goto_0
    return-object p0

    .line 570651
    :cond_0
    invoke-static {p0}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    invoke-static {p0}, LX/3d6;->bundleForException(Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Landroid/os/Bundle;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 570652
    new-instance p0, Lcom/facebook/fbservice/service/ServiceException;

    invoke-direct {p0, v0}, Lcom/facebook/fbservice/service/ServiceException;-><init>(Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 570647
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorCode()LX/1nY;
    .locals 1

    .prologue
    .line 570646
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    return-object v0
.end method

.method public getResult()Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 570645
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 570642
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 570643
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 570644
    return-void
.end method
