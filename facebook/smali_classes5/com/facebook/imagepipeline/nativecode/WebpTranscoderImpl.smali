.class public Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 797422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native nativeTranscodeWebpToJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private static native nativeTranscodeWebpToPng(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 797423
    invoke-static {}, LX/4ej;->a()V

    .line 797424
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;

    invoke-static {v0, v1}, Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;->nativeTranscodeWebpToPng(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 797425
    return-void
.end method

.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 2

    .prologue
    .line 797426
    invoke-static {}, LX/4ej;->a()V

    .line 797427
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;

    invoke-static {v0, v1, p3}, Lcom/facebook/imagepipeline/nativecode/WebpTranscoderImpl;->nativeTranscodeWebpToJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 797428
    return-void
.end method

.method public final a(LX/1lW;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 797429
    sget-object v1, LX/1ld;->e:LX/1lW;

    if-ne p1, v1, :cond_1

    .line 797430
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 797431
    :cond_0
    :goto_0
    return v0

    .line 797432
    :cond_1
    sget-object v1, LX/1ld;->f:LX/1lW;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/1ld;->g:LX/1lW;

    if-eq p1, v1, :cond_2

    sget-object v1, LX/1ld;->h:LX/1lW;

    if-ne p1, v1, :cond_3

    .line 797433
    :cond_2
    sget-boolean v0, LX/1cG;->c:Z

    goto :goto_0

    .line 797434
    :cond_3
    sget-object v1, LX/1ld;->i:LX/1lW;

    if-eq p1, v1, :cond_0

    .line 797435
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Image format is not a WebP."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
