.class public final Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1FL;

.field public final synthetic c:LX/1bh;

.field public final synthetic d:LX/23L;


# direct methods
.method public constructor <init>(LX/23L;Ljava/lang/String;LX/1FL;LX/1bh;)V
    .locals 0

    .prologue
    .line 797979
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->d:LX/23L;

    iput-object p2, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->b:LX/1FL;

    iput-object p4, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->c:LX/1bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 797980
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->d:LX/23L;

    iget-object v0, v0, LX/23L;->c:LX/23M;

    invoke-virtual {v0}, LX/23M;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 797981
    const v0, 0x7ef37ad5

    :try_start_0
    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 797982
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 797983
    const-string v2, "media_id"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 797984
    const-string v2, "width"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->b:LX/1FL;

    .line 797985
    iget v4, v3, LX/1FL;->e:I

    move v3, v4

    .line 797986
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 797987
    const-string v2, "height"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->b:LX/1FL;

    .line 797988
    iget v4, v3, LX/1FL;->f:I

    move v3, v4

    .line 797989
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 797990
    const-string v2, "cache_key"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->c:LX/1bh;

    invoke-interface {v3}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 797991
    const-string v2, "resource_id"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->c:LX/1bh;

    invoke-static {v3}, LX/1gD;->b(LX/1bh;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 797992
    const-string v2, "media_variations_index"

    const/4 v3, 0x0

    const v4, -0x46d78b89

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x495dc5a5

    invoke-static {v0}, LX/03h;->a(I)V

    .line 797993
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797994
    const v0, -0x5dbc2862

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 797995
    :goto_0
    return-void

    .line 797996
    :catch_0
    move-exception v0

    .line 797997
    :try_start_1
    sget-object v2, LX/23L;->a:Ljava/lang/String;

    const-string v3, "Error writing for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/03J;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797998
    const v0, 0x79dc817c

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, 0x3abd23db

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
