.class public final Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;
.super Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1bf;

.field public final synthetic c:LX/4f7;


# direct methods
.method public constructor <init>(LX/4f7;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V
    .locals 0

    .prologue
    .line 797928
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;->c:LX/4f7;

    iput-object p6, p0, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;->b:LX/1bf;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;-><init>(LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 797929
    check-cast p1, LX/1FJ;

    .line 797930
    invoke-static {p1}, LX/1FJ;->c(LX/1FJ;)V

    .line 797931
    return-void
.end method

.method public final c()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 797932
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;->b:LX/1bf;

    invoke-virtual {v0}, LX/1bf;->n()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/LocalVideoThumbnailProducer$1;->b:LX/1bf;

    const/16 v3, 0x60

    .line 797933
    iget-object v2, v1, LX/1bf;->h:LX/1o9;

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/1bf;->h:LX/1o9;

    iget v2, v2, LX/1o9;->a:I

    :goto_0
    move v2, v2

    .line 797934
    if-gt v2, v3, :cond_0

    .line 797935
    iget-object v2, v1, LX/1bf;->h:LX/1o9;

    if-eqz v2, :cond_4

    iget-object v2, v1, LX/1bf;->h:LX/1o9;

    iget v2, v2, LX/1o9;->b:I

    :goto_1
    move v2, v2

    .line 797936
    if-le v2, v3, :cond_2

    .line 797937
    :cond_0
    const/4 v2, 0x1

    .line 797938
    :goto_2
    move v1, v2

    .line 797939
    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 797940
    if-nez v0, :cond_1

    .line 797941
    const/4 v0, 0x0

    .line 797942
    :goto_3
    return-object v0

    :cond_1
    new-instance v1, LX/1ll;

    invoke-static {}, LX/4dn;->a()LX/4dn;

    move-result-object v2

    sget-object v3, LX/1lk;->a:LX/1lk;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, LX/1ll;-><init>(Landroid/graphics/Bitmap;LX/1FN;LX/1lk;I)V

    invoke-static {v1}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v0

    goto :goto_3

    :cond_2
    const/4 v2, 0x3

    goto :goto_2

    :cond_3
    const/16 v2, 0x800

    goto :goto_0

    :cond_4
    const/16 v2, 0x800

    goto :goto_1
.end method

.method public final c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 2

    .prologue
    .line 797943
    check-cast p1, LX/1FJ;

    .line 797944
    const-string v1, "createdThumbnail"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
