.class public final Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;
.super Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1FL;

.field public final synthetic c:LX/1cH;


# direct methods
.method public constructor <init>(LX/1cH;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1FL;)V
    .locals 0

    .prologue
    .line 798405
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->c:LX/1cH;

    iput-object p6, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;-><init>(LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 798406
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 798407
    invoke-super {p0, p1}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->a(Ljava/lang/Exception;)V

    .line 798408
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 798386
    check-cast p1, LX/1FL;

    .line 798387
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 798388
    invoke-super {p0, p1}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->a(Ljava/lang/Object;)V

    .line 798389
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 798402
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-static {v0}, LX/1FL;->d(LX/1FL;)V

    .line 798403
    invoke-super {p0}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;->b()V

    .line 798404
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 798399
    check-cast p1, LX/1FL;

    .line 798400
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    .line 798401
    return-void
.end method

.method public final c()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 798390
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->c:LX/1cH;

    iget-object v0, v0, LX/1cH;->b:LX/1Fj;

    invoke-virtual {v0}, LX/1Fj;->a()LX/1gO;

    move-result-object v1

    .line 798391
    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-static {v0, v1}, LX/1cH;->b(LX/1FL;LX/1gO;)V

    .line 798392
    invoke-virtual {v1}, LX/1gO;->a()LX/1FK;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 798393
    :try_start_1
    new-instance v0, LX/1FL;

    invoke-direct {v0, v2}, LX/1FL;-><init>(LX/1FJ;)V

    .line 798394
    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/WebpTranscodeProducer$1;->b:LX/1FL;

    invoke-virtual {v0, v3}, LX/1FL;->b(LX/1FL;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 798395
    :try_start_2
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 798396
    invoke-virtual {v1}, LX/1gO;->close()V

    return-object v0

    .line 798397
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798398
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, LX/1gO;->close()V

    throw v0
.end method
