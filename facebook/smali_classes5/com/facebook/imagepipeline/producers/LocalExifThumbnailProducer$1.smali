.class public final Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;
.super Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1bf;

.field public final synthetic c:LX/4f1;


# direct methods
.method public constructor <init>(LX/4f1;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V
    .locals 0

    .prologue
    .line 797830
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;->c:LX/4f1;

    iput-object p6, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;->b:LX/1bf;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;-><init>(LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 797831
    check-cast p1, LX/1FL;

    .line 797832
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    .line 797833
    return-void
.end method

.method public final c()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 797834
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;->b:LX/1bf;

    .line 797835
    iget-object v1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 797836
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;->c:LX/4f1;

    .line 797837
    iget-object v2, v1, LX/4f1;->c:Landroid/content/ContentResolver;

    invoke-static {v2, v0}, LX/1be;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 797838
    const/4 v2, 0x0

    .line 797839
    if-nez v3, :cond_4

    .line 797840
    :cond_0
    :goto_0
    move v2, v2

    .line 797841
    if-eqz v2, :cond_3

    .line 797842
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 797843
    :goto_1
    move-object v0, v2

    .line 797844
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/ExifInterface;->hasThumbnail()Z

    move-result v1

    if-nez v1, :cond_2

    .line 797845
    :cond_1
    const/4 v0, 0x0

    .line 797846
    :goto_2
    return-object v0

    .line 797847
    :cond_2
    invoke-virtual {v0}, Landroid/media/ExifInterface;->getThumbnail()[B

    move-result-object v1

    .line 797848
    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;->c:LX/4f1;

    iget-object v2, v2, LX/4f1;->b:LX/1Fj;

    invoke-virtual {v2, v1}, LX/1Fj;->a([B)LX/1FK;

    move-result-object v1

    .line 797849
    invoke-static {v1, v0}, LX/4f1;->a(LX/1FK;Landroid/media/ExifInterface;)LX/1FL;

    move-result-object v3

    move-object v0, v3

    .line 797850
    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 797851
    :cond_4
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 797852
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 2

    .prologue
    .line 797853
    check-cast p1, LX/1FL;

    .line 797854
    const-string v1, "createdThumbnail"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
