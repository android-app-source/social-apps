.class public Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static volatile j:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;


# instance fields
.field private final d:LX/4e8;

.field private final e:LX/03V;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1GD;

.field private h:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final i:LX/0SG;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 796921
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/4e9;->a:LX/0U1;

    .line 796922
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796923
    aput-object v1, v0, v3

    sget-object v1, LX/4e9;->b:LX/0U1;

    .line 796924
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796925
    aput-object v1, v0, v4

    sget-object v1, LX/4e9;->c:LX/0U1;

    .line 796926
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796927
    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, LX/4e9;->d:LX/0U1;

    .line 796928
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 796929
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/4e9;->e:LX/0U1;

    .line 796930
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 796931
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/4e9;->f:LX/0U1;

    .line 796932
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 796933
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/4e9;->g:LX/0U1;

    .line 796934
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 796935
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/4e9;->h:LX/0U1;

    .line 796936
    iget-object v6, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v6

    .line 796937
    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a:[Ljava/lang/String;

    .line 796938
    new-array v0, v4, [Ljava/lang/String;

    sget-object v1, LX/4e9;->e:LX/0U1;

    .line 796939
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796940
    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b:[Ljava/lang/String;

    .line 796941
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, LX/4e9;->e:LX/0U1;

    .line 796942
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796943
    aput-object v1, v0, v3

    sget-object v1, LX/4e9;->a:LX/0U1;

    .line 796944
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 796945
    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/4e8;LX/0SG;LX/03V;LX/1GD;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 796914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796915
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->i:LX/0SG;

    .line 796916
    iput-object p3, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    .line 796917
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->f:Ljava/util/Map;

    .line 796918
    iput-object p4, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->g:LX/1GD;

    .line 796919
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    .line 796920
    return-void
.end method

.method private static a(LX/0U1;JLandroid/database/Cursor;)J
    .locals 5

    .prologue
    .line 796736
    invoke-virtual {p0, p3}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    .line 796737
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 796738
    sub-long v0, p1, v0

    .line 796739
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;
    .locals 7

    .prologue
    .line 796901
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->j:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    if-nez v0, :cond_1

    .line 796902
    const-class v1, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    monitor-enter v1

    .line 796903
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->j:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 796904
    if-eqz v2, :cond_0

    .line 796905
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 796906
    new-instance p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    invoke-static {v0}, LX/4e8;->a(LX/0QB;)LX/4e8;

    move-result-object v3

    check-cast v3, LX/4e8;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/1GD;->a(LX/0QB;)LX/1GD;

    move-result-object v6

    check-cast v6, LX/1GD;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;-><init>(LX/4e8;LX/0SG;LX/03V;LX/1GD;)V

    .line 796907
    move-object v0, p0

    .line 796908
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->j:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796909
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 796910
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 796911
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->j:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    return-object v0

    .line 796912
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 796913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;Landroid/database/Cursor;LX/0oG;)V
    .locals 6

    .prologue
    .line 796892
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 796893
    const-string v2, "hit_count"

    sget-object v3, LX/4e9;->e:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v3

    invoke-virtual {p2, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 796894
    const-string v2, "bytes"

    sget-object v3, LX/4e9;->b:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    invoke-virtual {p2, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 796895
    const-string v2, "age_in_cache"

    sget-object v3, LX/4e9;->c:LX/0U1;

    invoke-static {v3, v0, v1, p1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/0U1;JLandroid/database/Cursor;)J

    move-result-wide v4

    invoke-virtual {p2, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 796896
    const-string v2, "original_analytics_tag"

    sget-object v3, LX/4e9;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796897
    const-string v2, "original_calling_class"

    sget-object v3, LX/4e9;->g:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796898
    const-string v2, "original_feature_tag"

    sget-object v3, LX/4e9;->h:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796899
    const-string v2, "age_since_last_hit"

    sget-object v3, LX/4e9;->d:LX/0U1;

    invoke-static {v3, v0, v1, p1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/0U1;JLandroid/database/Cursor;)J

    move-result-wide v0

    invoke-virtual {p2, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 796900
    return-void
.end method

.method public static a(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 796883
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 796884
    const v0, 0x27dc67d1

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796885
    :try_start_0
    const-string v0, "disk_cache_image_histories"

    invoke-virtual {v1, v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 796886
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796887
    const v0, -0x6a09cf78

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796888
    :goto_0
    return-void

    .line 796889
    :catch_0
    move-exception v0

    .line 796890
    :try_start_1
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v3, "DiskCacheImageHistoriesDbMarshaller"

    const-string v4, "delete"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796891
    const v0, 0x714a07e8    # 1.0004085E30f

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, 0x205da688

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static b(Ljava/lang/String;)LX/0ux;
    .locals 2

    .prologue
    .line 796880
    sget-object v0, LX/4e9;->a:LX/0U1;

    .line 796881
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 796882
    invoke-static {v0, p0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/1gC;I)V
    .locals 6

    .prologue
    .line 796861
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 796862
    const v0, -0x673cd6e2

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796863
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 796864
    sget-object v2, LX/4e9;->a:LX/0U1;

    .line 796865
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796866
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796867
    sget-object v2, LX/4e9;->b:LX/0U1;

    .line 796868
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796869
    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 796870
    sget-object v2, LX/4e9;->e:LX/0U1;

    .line 796871
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796872
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 796873
    const-string v2, "disk_cache_image_histories"

    const/4 v3, 0x0

    const v4, -0x5461c75b

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0xbb6ddb9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 796874
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796875
    const v0, -0x19fe96ea

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796876
    :goto_0
    return-void

    .line 796877
    :catch_0
    move-exception v0

    .line 796878
    :try_start_1
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v3, "DiskCacheImageHistoriesDbMarshaller"

    const-string v4, "insert incomplete"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796879
    const v0, 0x264f36a8

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, -0xb2d51b6

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static declared-synchronized c(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;)V
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 796835
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 796836
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 796837
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->h:J

    .line 796838
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 796839
    invoke-static {p0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;)V

    .line 796840
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 796841
    const-string v1, "disk_cache_image_histories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 796842
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 796843
    const-string v1, "disk_cache_image_histories"

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    .line 796844
    const-wide/16 v4, 0x1f4

    const-wide/16 v6, 0x2

    mul-long/2addr v2, v6

    const-wide/16 v6, 0x3

    div-long/2addr v2, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    .line 796845
    :try_start_2
    const-string v1, "disk_cache_image_histories"

    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/4e9;->e:LX/0U1;

    .line 796846
    iget-object v12, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v12

    .line 796847
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ASC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 796848
    :goto_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 796849
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->f:Ljava/util/Map;

    sget-object v2, LX/4e9;->a:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/4e9;->e:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 796850
    :catch_0
    move-exception v0

    .line 796851
    :goto_2
    :try_start_4
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v3, "DiskCacheImageHistoriesDbMarshaller"

    const-string v4, "get"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 796852
    if-eqz v1, :cond_0

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 796853
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 796854
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 796855
    :cond_2
    if-eqz v1, :cond_0

    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 796856
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 796857
    :catchall_1
    move-exception v0

    move-object v1, v9

    :goto_3
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 796858
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 796859
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 796860
    :catch_1
    move-exception v0

    move-object v1, v9

    goto :goto_2
.end method

.method private static declared-synchronized d(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;)V
    .locals 4

    .prologue
    .line 796827
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->g:LX/1GD;

    invoke-virtual {v0}, LX/1GD;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 796828
    invoke-static {}, LX/1gB;->h()LX/1gB;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 796829
    iput-object v1, v3, LX/1gB;->e:Ljava/lang/String;

    .line 796830
    move-object v1, v3

    .line 796831
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/1gC;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 796832
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 796833
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->g:LX/1GD;

    invoke-virtual {v0}, LX/1GD;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796834
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(LX/1gC;I)V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 796794
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v11

    .line 796795
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 796796
    const-string v1, "disk_cache_image_histories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 796797
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 796798
    const v2, -0x15b7b5dc

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796799
    :try_start_0
    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b:[Ljava/lang/String;

    invoke-virtual {v11}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 796800
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 796801
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 796802
    sget-object v0, LX/4e9;->e:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v0

    .line 796803
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796804
    if-eqz v9, :cond_2

    .line 796805
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b(LX/1gC;I)V

    .line 796806
    :goto_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 796807
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 796808
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796809
    :cond_0
    const v0, -0x52c571b7

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796810
    :goto_2
    return-void

    :cond_1
    move v0, v9

    move v9, v10

    .line 796811
    goto :goto_0

    .line 796812
    :cond_2
    :try_start_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 796813
    sget-object v4, LX/4e9;->e:LX/0U1;

    .line 796814
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 796815
    add-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 796816
    const-string v0, "disk_cache_image_histories"

    invoke-virtual {v11}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 796817
    :catch_0
    move-exception v0

    .line 796818
    :goto_3
    :try_start_3
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v4, "DiskCacheImageHistoriesDbMarshaller"

    const-string v5, "update"

    invoke-virtual {v3, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 796819
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 796820
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796821
    :cond_3
    const v0, -0x57d0b6fa

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_2

    .line 796822
    :catchall_0
    move-exception v0

    move-object v2, v8

    :goto_4
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_4

    .line 796823
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796824
    :cond_4
    const v2, -0x6012bc9d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 796825
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 796826
    :catch_1
    move-exception v0

    move-object v2, v8

    goto :goto_3
.end method

.method public final a(LX/1gC;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 796762
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 796763
    const v0, 0x6c59fb72

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796764
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 796765
    sget-object v2, LX/4e9;->a:LX/0U1;

    .line 796766
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796767
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796768
    sget-object v2, LX/4e9;->b:LX/0U1;

    .line 796769
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796770
    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 796771
    sget-object v2, LX/4e9;->c:LX/0U1;

    .line 796772
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796773
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->i:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 796774
    sget-object v2, LX/4e9;->e:LX/0U1;

    .line 796775
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796776
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 796777
    sget-object v2, LX/4e9;->f:LX/0U1;

    .line 796778
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796779
    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796780
    sget-object v2, LX/4e9;->g:LX/0U1;

    .line 796781
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796782
    iget-object v3, p2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 796783
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796784
    sget-object v2, LX/4e9;->h:LX/0U1;

    .line 796785
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 796786
    invoke-virtual {p2}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796787
    const-string v2, "disk_cache_image_histories"

    const/4 v3, 0x0

    const v4, -0x23f9205d

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x6e2fbac0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 796788
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796789
    const v0, -0x6b8d9796

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796790
    :goto_0
    return-void

    .line 796791
    :catch_0
    move-exception v0

    .line 796792
    :try_start_1
    iget-object v2, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v3, "DiskCacheImageHistoriesDbMarshaller"

    const-string v4, "insert"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796793
    const v0, -0x28608e1f

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, -0x56a2d125

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/0oG;)Z
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 796740
    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 796741
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 796742
    const-string v1, "disk_cache_image_histories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 796743
    iget-object v1, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->d:LX/4e8;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 796744
    const v2, 0x4a343034    # 2952205.0f

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796745
    sget-object v2, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 796746
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 796747
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 796748
    invoke-static {p0, v2, p2}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;Landroid/database/Cursor;LX/0oG;)V

    .line 796749
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796750
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796751
    const v0, 0x32841ae3

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    const/4 v0, 0x1

    .line 796752
    :goto_0
    return v0

    .line 796753
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796754
    const v0, 0x7bc7c68a

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 796755
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 796756
    :catch_0
    move-exception v0

    .line 796757
    :try_start_1
    iget-object v3, p0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->e:LX/03V;

    const-string v4, "DiskCacheImageHistoriesDbMarshaller"

    const-string v5, "marshall"

    invoke-virtual {v3, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796758
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796759
    const v0, -0x47755b76    # -6.6110006E-5f

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_1

    .line 796760
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 796761
    const v2, 0xfa6ef3f

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
