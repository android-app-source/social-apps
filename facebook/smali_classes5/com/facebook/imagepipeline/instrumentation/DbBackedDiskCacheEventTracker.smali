.class public Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;


# instance fields
.field private final a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

.field private final b:LX/0Zb;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 796710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796711
    iput-object p1, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    .line 796712
    iput-object p2, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->b:LX/0Zb;

    .line 796713
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;
    .locals 5

    .prologue
    .line 796697
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->d:Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;

    if-nez v0, :cond_1

    .line 796698
    const-class v1, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;

    monitor-enter v1

    .line 796699
    :try_start_0
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->d:Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 796700
    if-eqz v2, :cond_0

    .line 796701
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 796702
    new-instance p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;

    invoke-static {v0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    move-result-object v3

    check-cast v3, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;-><init>(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;LX/0Zb;)V

    .line 796703
    move-object v0, p0

    .line 796704
    sput-object v0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->d:Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 796706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 796707
    :cond_1
    sget-object v0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->d:Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;

    return-object v0

    .line 796708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 796709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0oG;Ljava/lang/String;LX/1gC;)V
    .locals 4

    .prologue
    .line 796686
    const-string v0, "event"

    invoke-virtual {p1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "cache_size"

    iget-wide v2, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 796687
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    invoke-interface {p3}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(Ljava/lang/String;LX/0oG;)Z

    move-result v0

    .line 796688
    if-nez v0, :cond_0

    .line 796689
    const-string v0, "bytes"

    invoke-interface {p3}, LX/1gC;->c()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 796690
    :cond_0
    invoke-static {p3}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 796691
    const-string v1, "latest_analytics_tag"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796692
    const-string v1, "latest_calling_class"

    .line 796693
    iget-object v2, v0, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v2

    .line 796694
    invoke-virtual {p1, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796695
    const-string v1, "latest_feature_tag"

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 796696
    return-void
.end method

.method private a(Ljava/lang/String;LX/1gC;)V
    .locals 2

    .prologue
    .line 796681
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v0

    .line 796682
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 796683
    :goto_0
    return-void

    .line 796684
    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/1gC;)V

    .line 796685
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/io/IOException;LX/1gC;)V
    .locals 2

    .prologue
    .line 796675
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v0

    .line 796676
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 796677
    :goto_0
    return-void

    .line 796678
    :cond_0
    invoke-direct {p0, v0, p1, p3}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/1gC;)V

    .line 796679
    const-string v1, "exception"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 796680
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method private b()LX/0oG;
    .locals 3

    .prologue
    .line 796674
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->b:LX/0Zb;

    const-string v1, "fresco_disk_cache_event"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method

.method private static h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;
    .locals 2

    .prologue
    .line 796714
    invoke-interface {p0}, LX/1gC;->a()LX/1bh;

    move-result-object v0

    .line 796715
    instance-of v1, v0, LX/1ec;

    if-eqz v1, :cond_0

    .line 796716
    check-cast v0, LX/1ec;

    .line 796717
    iget-object v1, v0, LX/1ec;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 796718
    instance-of v1, v0, Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v1, :cond_0

    .line 796719
    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    .line 796720
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/common/callercontext/CallerContext;->a:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 796671
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    const/4 p0, 0x0

    .line 796672
    invoke-static {v0, p0, p0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;Ljava/lang/String;[Ljava/lang/String;)V

    .line 796673
    return-void
.end method

.method public final a(LX/1gC;)V
    .locals 2

    .prologue
    .line 796668
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/1gC;I)V

    .line 796669
    const-string v0, "hit"

    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/1gC;)V

    .line 796670
    return-void
.end method

.method public final b(LX/1gC;)V
    .locals 0

    .prologue
    .line 796667
    return-void
.end method

.method public final c(LX/1gC;)V
    .locals 0

    .prologue
    .line 796666
    return-void
.end method

.method public final d(LX/1gC;)V
    .locals 2

    .prologue
    .line 796662
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    invoke-static {p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->h(LX/1gC;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(LX/1gC;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 796663
    invoke-interface {p1}, LX/1gC;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->c:J

    .line 796664
    const-string v0, "write"

    invoke-direct {p0, v0, p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(Ljava/lang/String;LX/1gC;)V

    .line 796665
    return-void
.end method

.method public final e(LX/1gC;)V
    .locals 2

    .prologue
    .line 796660
    const-string v0, "read_exception"

    invoke-interface {p1}, LX/1gC;->f()Ljava/io/IOException;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(Ljava/lang/String;Ljava/io/IOException;LX/1gC;)V

    .line 796661
    return-void
.end method

.method public final f(LX/1gC;)V
    .locals 2

    .prologue
    .line 796658
    const-string v0, "write_exception"

    invoke-interface {p1}, LX/1gC;->f()Ljava/io/IOException;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(Ljava/lang/String;Ljava/io/IOException;LX/1gC;)V

    .line 796659
    return-void
.end method

.method public final g(LX/1gC;)V
    .locals 6

    .prologue
    .line 796647
    invoke-interface {p1}, LX/1gC;->g()LX/37E;

    move-result-object v0

    .line 796648
    invoke-interface {p1}, LX/1gC;->d()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->c:J

    .line 796649
    invoke-direct {p0}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->b()LX/0oG;

    move-result-object v1

    .line 796650
    invoke-interface {p1}, LX/1gC;->b()Ljava/lang/String;

    move-result-object v2

    .line 796651
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 796652
    const-string v3, "eviction"

    invoke-direct {p0, v1, v3, p1}, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a(LX/0oG;Ljava/lang/String;LX/1gC;)V

    .line 796653
    const-string v3, "cache_limit"

    invoke-interface {p1}, LX/1gC;->e()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v3, "eviction_reason"

    invoke-virtual {v1, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 796654
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/instrumentation/DbBackedDiskCacheEventTracker;->a:Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    .line 796655
    invoke-static {v2}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 796656
    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->a(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;Ljava/lang/String;[Ljava/lang/String;)V

    .line 796657
    return-void
.end method
