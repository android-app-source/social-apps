.class public final Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4dC;


# direct methods
.method public constructor <init>(LX/4dC;)V
    .locals 0

    .prologue
    .line 795491
    iput-object p1, p0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$4;->a:LX/4dC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 795492
    iget-object v0, p0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$4;->a:LX/4dC;

    const-wide/16 v11, 0x3e8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 795493
    iput-boolean v3, v0, LX/4dC;->x:Z

    .line 795494
    iget-boolean v1, v0, LX/4dC;->w:Z

    if-nez v1, :cond_0

    .line 795495
    :goto_0
    return-void

    .line 795496
    :cond_0
    iget-object v1, v0, LX/4dC;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v5

    .line 795497
    iget-boolean v1, v0, LX/4dC;->u:Z

    if-eqz v1, :cond_3

    iget-wide v7, v0, LX/4dC;->v:J

    sub-long v7, v5, v7

    cmp-long v1, v7, v11

    if-lez v1, :cond_3

    move v1, v2

    .line 795498
    :goto_1
    iget-wide v7, v0, LX/4dC;->C:J

    const-wide/16 v9, -0x1

    cmp-long v4, v7, v9

    if-eqz v4, :cond_1

    iget-wide v7, v0, LX/4dC;->C:J

    sub-long/2addr v5, v7

    cmp-long v4, v5, v11

    if-lez v4, :cond_1

    move v3, v2

    .line 795499
    :cond_1
    if-nez v1, :cond_2

    if-eqz v3, :cond_4

    .line 795500
    :cond_2
    invoke-virtual {v0}, LX/4dC;->a()V

    .line 795501
    invoke-static {v0}, LX/4dC;->i(LX/4dC;)V

    goto :goto_0

    :cond_3
    move v1, v3

    .line 795502
    goto :goto_1

    .line 795503
    :cond_4
    iget-object v1, v0, LX/4dC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, v0, LX/4dC;->H:Ljava/lang/Runnable;

    const-wide/16 v5, 0x7d0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v3, v5, v6, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 795504
    iput-boolean v2, v0, LX/4dC;->x:Z

    goto :goto_0
.end method
