.class public final Lcom/facebook/imagepipeline/internal/PriorityOverridingExecutor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:LX/4eJ;


# direct methods
.method public constructor <init>(LX/4eJ;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 797188
    iput-object p1, p0, Lcom/facebook/imagepipeline/internal/PriorityOverridingExecutor$1;->b:LX/4eJ;

    iput-object p2, p0, Lcom/facebook/imagepipeline/internal/PriorityOverridingExecutor$1;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 797189
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    .line 797190
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iget-object v2, p0, Lcom/facebook/imagepipeline/internal/PriorityOverridingExecutor$1;->b:LX/4eJ;

    iget v2, v2, LX/4eJ;->b:I

    invoke-static {v0, v2}, Landroid/os/Process;->setThreadPriority(II)V

    .line 797191
    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/internal/PriorityOverridingExecutor$1;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797192
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadPriority(II)V

    .line 797193
    return-void

    .line 797194
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    invoke-static {v2, v1}, Landroid/os/Process;->setThreadPriority(II)V

    throw v0
.end method
