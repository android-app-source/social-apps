.class public Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0aG;

.field public final c:LX/28W;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2ZK;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/48N;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0SG;

.field public final h:LX/14U;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 624007
    const-class v0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/28W;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/28W;",
            "Ljava/util/Set",
            "<",
            "LX/2ZK;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/48N;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623961
    iput-object p1, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->b:LX/0aG;

    .line 623962
    iput-object p2, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->c:LX/28W;

    .line 623963
    iput-object p3, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->d:Ljava/util/Set;

    .line 623964
    iput-object p4, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->e:Ljava/util/Set;

    .line 623965
    iput-object p5, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 623966
    iput-object p6, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->g:LX/0SG;

    .line 623967
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->h:LX/14U;

    .line 623968
    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->h:LX/14U;

    const/4 v1, 0x0

    .line 623969
    iput-boolean v1, v0, LX/14U;->h:Z

    .line 623970
    return-void
.end method

.method public static a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;)V
    .locals 10

    .prologue
    .line 623971
    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/48N;

    .line 623972
    if-eqz v7, :cond_0

    invoke-interface {v7}, LX/48N;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623973
    sget-object v0, LX/3g1;->d:LX/0Tn;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/0Tn;

    .line 623974
    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v8, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 623975
    invoke-interface {v7}, LX/48N;->c()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;JJZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623976
    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->b:LX/0aG;

    invoke-interface {v7}, LX/48N;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, LX/48N;->b()Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    .line 623977
    sget-object v4, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 623978
    const v5, 0x457c1f66

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 623979
    invoke-static {p0, v8}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;LX/0Tn;)V

    goto :goto_0

    .line 623980
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;LX/0Tn;)V
    .locals 4

    .prologue
    .line 623981
    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 623982
    return-void
.end method

.method public static a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;JJZ)Z
    .locals 3

    .prologue
    .line 623983
    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    cmp-long v0, v0, p3

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    .line 623984
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 623985
    const-string v1, "configuration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 623986
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 623987
    const-string v5, "forceFetch"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 623988
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v12

    .line 623989
    iget-object v4, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->d:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2ZK;

    .line 623990
    sget-object v5, LX/3g1;->c:LX/0Tn;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    move-object v11, v5

    check-cast v11, LX/0Tn;

    .line 623991
    iget-object v5, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v6, 0x0

    invoke-interface {v5, v11, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 623992
    invoke-interface {v4}, LX/2ZK;->dr_()J

    move-result-wide v8

    move-object v5, p0

    invoke-static/range {v5 .. v10}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;JJZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 623993
    invoke-static {p0, v11}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;LX/0Tn;)V

    .line 623994
    invoke-interface {v4}, LX/2ZK;->b()LX/2ZE;

    move-result-object v4

    .line 623995
    if-eqz v4, :cond_0

    .line 623996
    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 623997
    :cond_1
    if-eqz v10, :cond_3

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 623998
    :goto_1
    iget-object v5, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->h:LX/14U;

    .line 623999
    iput-object v4, v5, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 624000
    iget-object v4, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->c:LX/28W;

    const-string v5, "handleFetchConfiguration"

    sget-object v6, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v7, p0, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->h:LX/14U;

    invoke-virtual {v4, v5, v6, v12, v7}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 624001
    invoke-static {p0}, Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;->a(Lcom/facebook/config/background/impl/ConfigBackgroundServiceHandler;)V

    .line 624002
    sget-object v4, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 624003
    move-object v0, v4

    .line 624004
    return-object v0

    .line 624005
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 624006
    :cond_3
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_1
.end method
