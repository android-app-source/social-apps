.class public final Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 667340
    const-class v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    new-instance v1, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 667341
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 667313
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 667315
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 667316
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 667317
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 667318
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 667319
    if-eqz v2, :cond_0

    .line 667320
    const-string p0, "is_work_user"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667321
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 667322
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 667323
    if-eqz v2, :cond_1

    .line 667324
    const-string p0, "work_community"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667325
    invoke-static {v1, v2, p1, p2}, LX/4p6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 667326
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 667327
    if-eqz v2, :cond_2

    .line 667328
    const-string p0, "work_subdomain"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667329
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667330
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 667331
    if-eqz v2, :cond_4

    .line 667332
    const-string p0, "work_users"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667333
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 667334
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_3

    .line 667335
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/42X;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 667336
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 667337
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 667338
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 667339
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 667314
    check-cast p1, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Serializer;->a(Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
