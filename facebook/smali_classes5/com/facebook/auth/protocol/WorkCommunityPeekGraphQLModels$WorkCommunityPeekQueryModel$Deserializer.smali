.class public final Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 667277
    const-class v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    new-instance v1, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 667278
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 667279
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 667280
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 667281
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 667282
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 667283
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 667284
    :goto_0
    move v1, v2

    .line 667285
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 667286
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 667287
    new-instance v1, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$WorkCommunityPeekQueryModel;-><init>()V

    .line 667288
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 667289
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 667290
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 667291
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 667292
    :cond_0
    return-object v1

    .line 667293
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_6

    .line 667294
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 667295
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 667296
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v8, :cond_1

    .line 667297
    const-string p0, "is_work_user"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 667298
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_1

    .line 667299
    :cond_2
    const-string p0, "work_community"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 667300
    invoke-static {p1, v0}, LX/4p6;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 667301
    :cond_3
    const-string p0, "work_subdomain"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 667302
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 667303
    :cond_4
    const-string p0, "work_users"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 667304
    invoke-static {p1, v0}, LX/42X;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 667305
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 667306
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 667307
    if-eqz v1, :cond_7

    .line 667308
    invoke-virtual {v0, v2, v7}, LX/186;->a(IZ)V

    .line 667309
    :cond_7
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 667310
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 667311
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 667312
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1
.end method
