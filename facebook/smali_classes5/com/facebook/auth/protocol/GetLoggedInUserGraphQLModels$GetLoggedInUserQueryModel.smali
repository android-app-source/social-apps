.class public final Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52bced33
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 576601
    const-class v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 576613
    const-class v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 576611
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 576612
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 576602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 576603
    invoke-virtual {p0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 576604
    invoke-virtual {p0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 576605
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 576606
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 576607
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 576608
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 576609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 576610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 576582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 576583
    invoke-virtual {p0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 576584
    invoke-virtual {p0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    .line 576585
    invoke-virtual {p0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 576586
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;

    .line 576587
    iput-object v0, v1, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->e:Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    .line 576588
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 576589
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActor"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576599
    iget-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->e:Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    iput-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->e:Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    .line 576600
    iget-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->e:Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 576614
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 576615
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->f:Z

    .line 576616
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 576596
    new-instance v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;

    invoke-direct {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;-><init>()V

    .line 576597
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 576598
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 576595
    const v0, 0x21807552

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 576594
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 576592
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 576593
    iget-boolean v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->f:Z

    return v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 576590
    iget-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    iput-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    .line 576591
    iget-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->g:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    return-object v0
.end method
