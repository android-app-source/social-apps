.class public final Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 667209
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 667210
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 667211
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 667212
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 667213
    if-nez p1, :cond_0

    .line 667214
    :goto_0
    return v0

    .line 667215
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 667216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 667217
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 667218
    const v2, -0x5e66c474

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 667219
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 667220
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 667221
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 667222
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 667223
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 667224
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 667225
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 667226
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 667227
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 667228
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 667229
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 667230
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v2

    .line 667231
    const v3, 0x48f8d642

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 667232
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 667233
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 667234
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 667235
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 667236
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 667237
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 667238
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 667239
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 667240
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 667241
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 667242
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 667243
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 667244
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 667245
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 667246
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5e66c474 -> :sswitch_1
        0x2e7f96ff -> :sswitch_0
        0x48f8d642 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 667247
    if-nez p0, :cond_0

    move v0, v1

    .line 667248
    :goto_0
    return v0

    .line 667249
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 667250
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 667251
    :goto_1
    if-ge v1, v2, :cond_2

    .line 667252
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 667253
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 667254
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 667255
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 667270
    const/4 v7, 0x0

    .line 667271
    const/4 v1, 0x0

    .line 667272
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 667273
    invoke-static {v2, v3, v0}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667274
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 667275
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 667276
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 667256
    new-instance v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 667257
    sparse-switch p2, :sswitch_data_0

    .line 667258
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 667259
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 667260
    const v1, -0x5e66c474

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 667261
    :goto_0
    :sswitch_1
    return-void

    .line 667262
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 667263
    const v1, 0x48f8d642

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5e66c474 -> :sswitch_2
        0x2e7f96ff -> :sswitch_0
        0x48f8d642 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 667264
    if-eqz p1, :cond_0

    .line 667265
    invoke-static {p0, p1, p2}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 667266
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    .line 667267
    if-eq v0, v1, :cond_0

    .line 667268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 667269
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 667206
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 667207
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 667208
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 667201
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 667202
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 667203
    :cond_0
    iput-object p1, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 667204
    iput p2, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->b:I

    .line 667205
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 667200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 667199
    new-instance v0, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 667196
    iget v0, p0, LX/1vt;->c:I

    .line 667197
    move v0, v0

    .line 667198
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 667193
    iget v0, p0, LX/1vt;->c:I

    .line 667194
    move v0, v0

    .line 667195
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 667190
    iget v0, p0, LX/1vt;->b:I

    .line 667191
    move v0, v0

    .line 667192
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 667187
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 667188
    move-object v0, v0

    .line 667189
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 667178
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 667179
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 667180
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 667181
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 667182
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 667183
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 667184
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 667185
    invoke-static {v3, v9, v2}, Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/auth/protocol/WorkCommunityPeekGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 667186
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 667175
    iget v0, p0, LX/1vt;->c:I

    .line 667176
    move v0, v0

    .line 667177
    return v0
.end method
