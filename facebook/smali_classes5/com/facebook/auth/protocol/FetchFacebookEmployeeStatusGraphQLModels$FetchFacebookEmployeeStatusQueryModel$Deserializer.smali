.class public final Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 632884
    const-class v0, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;

    new-instance v1, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 632885
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632910
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 632886
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 632887
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 632888
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_5

    .line 632889
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 632890
    :goto_0
    move v1, v2

    .line 632891
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 632892
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 632893
    new-instance v1, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;-><init>()V

    .line 632894
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 632895
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 632896
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 632897
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 632898
    :cond_0
    return-object v1

    .line 632899
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_3

    .line 632900
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 632901
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 632902
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v5, :cond_1

    .line 632903
    const-string p0, "is_fb_employee"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 632904
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    goto :goto_1

    .line 632905
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 632906
    :cond_3
    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 632907
    if-eqz v1, :cond_4

    .line 632908
    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 632909
    :cond_4
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v4, v2

    goto :goto_1
.end method
