.class public Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667149
    new-instance v0, LX/42S;

    invoke-direct {v0}, LX/42S;-><init>()V

    sput-object v0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 667150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 667151
    iput-object p1, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->a:Ljava/lang/String;

    .line 667152
    iput-object p2, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->b:Ljava/lang/String;

    .line 667153
    iput-object p3, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->c:Ljava/lang/String;

    .line 667154
    iput-object p4, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->d:Ljava/lang/String;

    .line 667155
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 667156
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 667157
    iget-object v0, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 667158
    iget-object v0, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 667159
    iget-object v0, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 667160
    iget-object v0, p0, Lcom/facebook/auth/protocol/SuggestedFacebookAccountInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 667161
    return-void
.end method
