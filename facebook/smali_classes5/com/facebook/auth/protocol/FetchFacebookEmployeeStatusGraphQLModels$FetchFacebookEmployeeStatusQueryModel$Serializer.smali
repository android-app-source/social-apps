.class public final Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 666849
    const-class v0, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;

    new-instance v1, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 666850
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666838
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 666840
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 666841
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 666842
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 666843
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 666844
    if-eqz p0, :cond_0

    .line 666845
    const-string p2, "is_fb_employee"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 666846
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 666847
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 666848
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 666839
    check-cast p1, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel$Serializer;->a(Lcom/facebook/auth/protocol/FetchFacebookEmployeeStatusGraphQLModels$FetchFacebookEmployeeStatusQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
