.class public Lcom/facebook/auth/protocol/WorkCommunityPeekResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/protocol/WorkCommunityPeekResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/work/auth/request/model/WorkCommunity;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/work/auth/request/model/WorkCommunity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667492
    new-instance v0, LX/42Z;

    invoke-direct {v0}, LX/42Z;-><init>()V

    sput-object v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;JZLcom/facebook/work/auth/request/model/WorkCommunity;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ta;",
            "JZ",
            "Lcom/facebook/work/auth/request/model/WorkCommunity;",
            "LX/0Px",
            "<",
            "Lcom/facebook/work/auth/request/model/WorkCommunity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 667493
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 667494
    iput-boolean p4, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->a:Z

    .line 667495
    iput-object p5, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->b:Lcom/facebook/work/auth/request/model/WorkCommunity;

    .line 667496
    iput-object p6, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->c:LX/0Px;

    .line 667497
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 667498
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 667499
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->a:Z

    .line 667500
    const-class v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    iput-object v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->b:Lcom/facebook/work/auth/request/model/WorkCommunity;

    .line 667501
    const-class v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->c:LX/0Px;

    .line 667502
    return-void

    .line 667503
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 667504
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 667505
    iget-boolean v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 667506
    iget-object v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->b:Lcom/facebook/work/auth/request/model/WorkCommunity;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 667507
    iget-object v0, p0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 667508
    return-void

    .line 667509
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
