.class public final Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 667095
    const-class v0, Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;

    new-instance v1, Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 667096
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 667097
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 667098
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 667099
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 667100
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 667101
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 667102
    if-eqz p0, :cond_0

    .line 667103
    const-string p2, "is_approved"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667104
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 667105
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 667106
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 667107
    check-cast p1, Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel$Serializer;->a(Lcom/facebook/auth/protocol/LoginApprovalMutationModels$LoginApprovalMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
