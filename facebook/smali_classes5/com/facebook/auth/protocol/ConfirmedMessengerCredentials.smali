.class public Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666648
    new-instance v0, LX/41z;

    invoke-direct {v0}, LX/41z;-><init>()V

    sput-object v0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 666649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666650
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->a:Ljava/lang/String;

    .line 666651
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->b:Ljava/lang/String;

    .line 666652
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->c:Ljava/lang/String;

    .line 666653
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->d:Ljava/lang/String;

    .line 666654
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->e:Ljava/lang/String;

    .line 666655
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->f:Ljava/util/Calendar;

    .line 666656
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 666657
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 666658
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666659
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666660
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666661
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666662
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666663
    iget-object v0, p0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->f:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 666664
    return-void
.end method
