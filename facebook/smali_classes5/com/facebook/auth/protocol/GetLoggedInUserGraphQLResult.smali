.class public Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/User;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 577691
    new-instance v0, LX/2u2;

    invoke-direct {v0}, LX/2u2;-><init>()V

    sput-object v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;Lcom/facebook/user/model/User;J)V
    .locals 1

    .prologue
    .line 577692
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 577693
    iput-object p2, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->a:Lcom/facebook/user/model/User;

    .line 577694
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 577695
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 577696
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->a:Lcom/facebook/user/model/User;

    .line 577697
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 577698
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 577699
    iget-object v0, p0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;->a:Lcom/facebook/user/model/User;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 577700
    return-void
.end method
