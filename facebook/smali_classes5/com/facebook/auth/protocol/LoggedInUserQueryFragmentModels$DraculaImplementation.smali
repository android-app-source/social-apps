.class public final Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 666880
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 666881
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 666939
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 666940
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 666919
    if-nez p1, :cond_0

    .line 666920
    :goto_0
    return v0

    .line 666921
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 666922
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 666923
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 666924
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 666925
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 666926
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 666927
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 666928
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 666929
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 666930
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 666931
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 666932
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 666933
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 666934
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 666935
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 666936
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 666937
    invoke-virtual {p3, v3, v2}, LX/186;->b(II)V

    .line 666938
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5c6f7173 -> :sswitch_0
        0x2cabb705 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 666918
    new-instance v0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 666915
    sparse-switch p0, :sswitch_data_0

    .line 666916
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 666917
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5c6f7173 -> :sswitch_0
        0x2cabb705 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 666914
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 666912
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->b(I)V

    .line 666913
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 666907
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 666908
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 666909
    :cond_0
    iput-object p1, p0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->a:LX/15i;

    .line 666910
    iput p2, p0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->b:I

    .line 666911
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 666941
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 666906
    new-instance v0, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 666903
    iget v0, p0, LX/1vt;->c:I

    .line 666904
    move v0, v0

    .line 666905
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 666900
    iget v0, p0, LX/1vt;->c:I

    .line 666901
    move v0, v0

    .line 666902
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 666897
    iget v0, p0, LX/1vt;->b:I

    .line 666898
    move v0, v0

    .line 666899
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 666894
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 666895
    move-object v0, v0

    .line 666896
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 666885
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 666886
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 666887
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 666888
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 666889
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 666890
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 666891
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 666892
    invoke-static {v3, v9, v2}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 666893
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 666882
    iget v0, p0, LX/1vt;->c:I

    .line 666883
    move v0, v0

    .line 666884
    return v0
.end method
