.class public Lcom/facebook/auth/protocol/AuthenticationResultImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/auth/component/AuthenticationResult;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/component/AuthenticationResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/auth/credentials/FacebookCredentials;

.field private final c:Ljava/lang/String;

.field private final d:LX/03R;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576054
    new-instance v0, LX/2Wo;

    invoke-direct {v0}, LX/2Wo;-><init>()V

    sput-object v0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 576046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576047
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->a:Ljava/lang/String;

    .line 576048
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/FacebookCredentials;

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->b:Lcom/facebook/auth/credentials/FacebookCredentials;

    .line 576049
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->c:Ljava/lang/String;

    .line 576050
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->d:LX/03R;

    .line 576051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->e:Ljava/lang/String;

    .line 576052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->f:Ljava/lang/String;

    .line 576053
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/auth/credentials/FacebookCredentials;Ljava/lang/String;LX/03R;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 576038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576039
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->a:Ljava/lang/String;

    .line 576040
    iput-object p2, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->b:Lcom/facebook/auth/credentials/FacebookCredentials;

    .line 576041
    iput-object p3, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->c:Ljava/lang/String;

    .line 576042
    if-nez p4, :cond_0

    sget-object p4, LX/03R;->UNSET:LX/03R;

    :cond_0
    iput-object p4, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->d:LX/03R;

    .line 576043
    iput-object p5, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->e:Ljava/lang/String;

    .line 576044
    iput-object p6, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->f:Ljava/lang/String;

    .line 576045
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576037
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/facebook/auth/credentials/FacebookCredentials;
    .locals 1

    .prologue
    .line 576036
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->b:Lcom/facebook/auth/credentials/FacebookCredentials;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576024
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/03R;
    .locals 1

    .prologue
    .line 576035
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->d:LX/03R;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 576034
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576033
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576032
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 576025
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576026
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->b:Lcom/facebook/auth/credentials/FacebookCredentials;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 576027
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576028
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576029
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576030
    iget-object v0, p0, Lcom/facebook/auth/protocol/AuthenticationResultImpl;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576031
    return-void
.end method
