.class public Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/41Q;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/41Q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/41Q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 666333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666334
    iput-object p1, p0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->b:LX/0Or;

    .line 666335
    iput-object p2, p0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->a:LX/41Q;

    .line 666336
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;
    .locals 5

    .prologue
    .line 666337
    const-class v1, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;

    monitor-enter v1

    .line 666338
    :try_start_0
    sget-object v0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 666339
    sput-object v2, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 666340
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666341
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 666342
    new-instance v4, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 666343
    new-instance v3, LX/41Q;

    invoke-direct {v3}, LX/41Q;-><init>()V

    .line 666344
    move-object v3, v3

    .line 666345
    move-object v3, v3

    .line 666346
    check-cast v3, LX/41Q;

    invoke-direct {v4, p0, v3}, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;-><init>(LX/0Or;LX/41Q;)V

    .line 666347
    move-object v0, v4

    .line 666348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 666349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 666350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 666351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 666324
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 666325
    const-string v1, "check_approved_machine"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 666326
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 666327
    if-eqz v1, :cond_0

    .line 666328
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 666329
    const-string v1, "checkApprovedMachineParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineParams;

    .line 666330
    iget-object v1, p0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;->a:LX/41Q;

    const-class v3, Lcom/facebook/auth/login/CheckApprovedMachineServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    .line 666331
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 666332
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
