.class public final Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/41I;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/login/AuthOperations;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 666203
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a:Lcom/facebook/auth/login/AuthOperations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666204
    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->b:Ljava/lang/String;

    .line 666205
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/auth/component/AuthenticationResult;
    .locals 5

    .prologue
    .line 666206
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 666207
    new-instance v2, LX/41y;

    iget-object v3, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v3, v1, v0}, LX/41y;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 666208
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 666209
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->f:LX/2Xl;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "AuthOperations"

    invoke-static {v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    return-object v0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666210
    invoke-virtual {p0}, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a()Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
