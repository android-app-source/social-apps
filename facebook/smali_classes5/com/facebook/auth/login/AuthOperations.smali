.class public Lcom/facebook/auth/login/AuthOperations;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile y:Lcom/facebook/auth/login/AuthOperations;


# instance fields
.field private final a:LX/0WJ;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2E3;

.field public final e:LX/2Xn;

.field public final f:LX/2Xl;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/28U;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/41w;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2E4;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/2Xo;

.field public final m:LX/2Bn;

.field public final n:LX/2Bo;

.field private final o:LX/2Ym;

.field public final p:LX/28Z;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/2A0;

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/28S;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/2Dp;

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/28a;


# direct methods
.method public constructor <init>(LX/0WJ;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;LX/2E3;LX/2Xn;LX/2Xl;LX/28U;LX/0Ot;LX/0Or;LX/2E4;LX/0Or;LX/2Xo;LX/2Bn;LX/2Bo;LX/2Ym;LX/28Z;LX/0Ot;LX/2A0;LX/0Ot;LX/2Dp;LX/0Ot;LX/0Ot;LX/0Ot;LX/28a;)V
    .locals 1
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ShouldRequestSessionCookiesWithAuth;
        .end annotation
    .end param
    .param p23    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WJ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/auth/component/AuthComponent;",
            ">;",
            "LX/2E3;",
            "LX/2Xn;",
            "LX/2Xl;",
            "LX/28U;",
            "LX/0Ot",
            "<",
            "LX/41w;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2E4;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Xo;",
            "LX/2Bn;",
            "LX/2Bo;",
            "LX/2Ym;",
            "LX/28Z;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/2A0;",
            "LX/0Ot",
            "<",
            "LX/28S;",
            ">;",
            "LX/2Dp;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/28a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 570005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570006
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    .line 570007
    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 570008
    iput-object p3, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    .line 570009
    iput-object p4, p0, Lcom/facebook/auth/login/AuthOperations;->d:LX/2E3;

    .line 570010
    iput-object p5, p0, Lcom/facebook/auth/login/AuthOperations;->e:LX/2Xn;

    .line 570011
    iput-object p6, p0, Lcom/facebook/auth/login/AuthOperations;->f:LX/2Xl;

    .line 570012
    iput-object p7, p0, Lcom/facebook/auth/login/AuthOperations;->h:LX/28U;

    .line 570013
    iput-object p9, p0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    .line 570014
    iput-object p10, p0, Lcom/facebook/auth/login/AuthOperations;->j:LX/2E4;

    .line 570015
    iput-object p11, p0, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    .line 570016
    iput-object p12, p0, Lcom/facebook/auth/login/AuthOperations;->l:LX/2Xo;

    .line 570017
    iput-object p13, p0, Lcom/facebook/auth/login/AuthOperations;->m:LX/2Bn;

    .line 570018
    iput-object p14, p0, Lcom/facebook/auth/login/AuthOperations;->n:LX/2Bo;

    .line 570019
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->o:LX/2Ym;

    .line 570020
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->p:LX/28Z;

    .line 570021
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    .line 570022
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->r:LX/2A0;

    .line 570023
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->s:LX/0Ot;

    .line 570024
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->t:LX/2Dp;

    .line 570025
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->u:LX/0Ot;

    .line 570026
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->v:LX/0Ot;

    .line 570027
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->w:LX/0Ot;

    .line 570028
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->x:LX/28a;

    .line 570029
    iput-object p8, p0, Lcom/facebook/auth/login/AuthOperations;->i:LX/0Ot;

    .line 570030
    return-void
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;
    .locals 8
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 570031
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 570032
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 570033
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 570034
    move-object v3, v0

    .line 570035
    :goto_0
    if-eqz v2, :cond_1

    .line 570036
    iget-object v0, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 570037
    move-object v2, v0

    .line 570038
    :goto_1
    if-eqz p2, :cond_2

    if-eqz v2, :cond_2

    .line 570039
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/26p;->f:LX/0Tn;

    invoke-interface {v0, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 570040
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v5, p0, Lcom/facebook/auth/login/AuthOperations;->f:LX/2Xl;

    new-instance v6, LX/41y;

    invoke-direct {v6, v2, v4, p2, v7}, LX/41y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "AuthOperations"

    invoke-static {v2, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v5, v6, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    .line 570041
    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v0

    .line 570042
    iget-object v2, v0, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v0, v2

    .line 570043
    :goto_2
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v4, LX/26p;->A:LX/0Tn;

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 570044
    invoke-virtual {p0, v1, v7}, Lcom/facebook/auth/login/AuthOperations;->a(Ljava/lang/String;Z)V

    .line 570045
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->r:LX/2A0;

    invoke-virtual {v1}, LX/2A0;->c()V

    .line 570046
    new-instance v1, LX/41K;

    invoke-direct {v1, p0, p1}, LX/41K;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;)V

    invoke-static {p0, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v1

    .line 570047
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v4, LX/26p;->A:LX/0Tn;

    invoke-interface {v2, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 570048
    new-instance v2, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/component/AuthenticationResult;)V

    return-object v2

    :cond_0
    move-object v3, v1

    .line 570049
    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 570050
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 570052
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, v1, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;ZLjava/lang/String;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;LX/41I;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 2
    .param p1    # LX/41I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 570051
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, p2, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;ZLjava/lang/String;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;LX/41I;ZLjava/lang/String;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 4
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 569898
    :try_start_0
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 569899
    invoke-virtual {p0}, Lcom/facebook/auth/login/AuthOperations;->a()V

    .line 569900
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "LogoutDidNotComplete"

    const-string v2, "Trying to login, but logout did not complete."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 569901
    :cond_0
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16B;

    .line 569902
    invoke-virtual {v0}, LX/16B;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 569903
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 569904
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16B;

    .line 569905
    invoke-virtual {v0}, LX/16B;->d()V

    goto :goto_1

    .line 569906
    :cond_1
    :try_start_1
    invoke-interface {p1}, LX/41I;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    .line 569907
    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 569908
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 569909
    sget-object v2, LX/26p;->f:LX/0Tn;

    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 569910
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 569911
    :cond_2
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->j:LX/2E4;

    .line 569912
    iget-object v2, v1, LX/2E4;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/26p;->w:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 569913
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-interface {v0}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0WJ;->a(Lcom/facebook/auth/credentials/FacebookCredentials;)V

    .line 569914
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    .line 569915
    iget-object v2, v1, LX/0WJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/26p;->j:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v2, v3, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 569916
    if-eqz p2, :cond_3

    .line 569917
    invoke-direct {p0, v0, p3}, Lcom/facebook/auth/login/AuthOperations;->b(Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    .line 569918
    :goto_2
    return-object v0

    .line 569919
    :cond_3
    invoke-static {p0, v0, p4}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Lcom/facebook/work/auth/request/model/WorkCommunity;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 569920
    :cond_4
    throw v1
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 2
    .param p1    # Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 570053
    new-instance v0, Lcom/facebook/auth/login/AuthOperations$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/auth/login/AuthOperations$4;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;)V

    .line 570054
    const/4 v1, 0x1

    const/4 p1, 0x0

    invoke-static {p0, v0, v1, p1, p2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;ZLjava/lang/String;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v1

    move-object v0, v1

    .line 570055
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/auth/login/AuthOperations;
    .locals 3

    .prologue
    .line 569986
    sget-object v0, Lcom/facebook/auth/login/AuthOperations;->y:Lcom/facebook/auth/login/AuthOperations;

    if-nez v0, :cond_1

    .line 569987
    const-class v1, Lcom/facebook/auth/login/AuthOperations;

    monitor-enter v1

    .line 569988
    :try_start_0
    sget-object v0, Lcom/facebook/auth/login/AuthOperations;->y:Lcom/facebook/auth/login/AuthOperations;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 569989
    if-eqz v2, :cond_0

    .line 569990
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/auth/login/AuthOperations;->b(LX/0QB;)Lcom/facebook/auth/login/AuthOperations;

    move-result-object v0

    sput-object v0, Lcom/facebook/auth/login/AuthOperations;->y:Lcom/facebook/auth/login/AuthOperations;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 569991
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 569992
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 569993
    :cond_1
    sget-object v0, Lcom/facebook/auth/login/AuthOperations;->y:Lcom/facebook/auth/login/AuthOperations;

    return-object v0

    .line 569994
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 569995
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Lcom/facebook/work/auth/request/model/WorkCommunity;)V
    .locals 4
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 569996
    if-eqz p2, :cond_0

    .line 569997
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->t:LX/2Dp;

    .line 569998
    iget-object v1, p2, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    move-object v1, v1

    .line 569999
    iget-object v2, p2, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    move-object v2, v2

    .line 570000
    iget-object v3, p2, Lcom/facebook/work/auth/request/model/WorkCommunity;->e:Ljava/lang/String;

    move-object v3, v3

    .line 570001
    invoke-virtual {v0, v1, v2, v3}, LX/2Dp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570002
    :cond_0
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16B;

    .line 570003
    invoke-virtual {v0, p1}, LX/16B;->a(Lcom/facebook/auth/component/AuthenticationResult;)V

    goto :goto_0

    .line 570004
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 12
    .param p0    # Lcom/facebook/auth/login/AuthOperations;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v11, 0x940001

    const/4 v10, 0x2

    .line 569926
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->v:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 569927
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->x:LX/28a;

    invoke-virtual {v1}, LX/28a;->a()Z

    move-result v4

    .line 569928
    invoke-interface {v3, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569929
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->j()V

    .line 569930
    const v1, 0x940002

    :try_start_0
    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569931
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569932
    const v5, 0x940003

    invoke-interface {v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569933
    const v5, 0x940003

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569934
    invoke-virtual {v1}, LX/16B;->e()V

    .line 569935
    const v1, 0x940003

    const/4 v5, 0x2

    invoke-interface {v3, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 569936
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->k()V

    .line 569937
    invoke-interface {v3, v11, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v1

    .line 569938
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2XA;

    .line 569939
    const v5, 0x940003

    invoke-interface {v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569940
    const v5, 0x940003

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569941
    invoke-interface {v1}, LX/2XA;->d()V

    .line 569942
    const v1, 0x940003

    const/4 v5, 0x2

    invoke-interface {v3, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_1

    .line 569943
    :cond_1
    const v1, 0x940002

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569944
    const v1, 0x940004

    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569945
    if-eqz v4, :cond_2

    .line 569946
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/auth/login/AuthOperations$7;

    invoke-direct {v2, p0, v3}, Lcom/facebook/auth/login/AuthOperations$7;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    const v5, 0x4360f49b

    invoke-static {v1, v2, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569947
    :cond_2
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569948
    const v2, 0x940005

    invoke-interface {v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569949
    const v2, 0x940005

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569950
    if-eqz v4, :cond_3

    .line 569951
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->w:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/auth/login/AuthOperations$8;

    invoke-direct {v6, p0, v3, v1}, Lcom/facebook/auth/login/AuthOperations$8;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/16B;)V

    const v1, 0x11b29456

    invoke-static {v2, v6, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569952
    :goto_3
    const v1, 0x940005

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_2

    .line 569953
    :cond_3
    invoke-virtual {v1}, LX/16B;->f()V

    goto :goto_3

    .line 569954
    :cond_4
    if-eqz v4, :cond_5

    .line 569955
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/auth/login/AuthOperations$9;

    invoke-direct {v2, p0, v3}, Lcom/facebook/auth/login/AuthOperations$9;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    const v5, 0x3099c44c

    invoke-static {v1, v2, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569956
    :cond_5
    const v1, 0x940004

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569957
    const v1, 0x940006

    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569958
    if-eqz v4, :cond_6

    .line 569959
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    move-object v8, v0

    new-instance v1, Lcom/facebook/auth/login/AuthOperations$10;

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/auth/login/AuthOperations$10;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const v2, -0x516d14c9

    invoke-static {v8, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 569960
    const v1, 0x940006

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569961
    :goto_4
    const v1, 0x940007

    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569962
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->l:LX/2Xo;

    new-instance v2, Lcom/facebook/auth/login/AuthOperations$11;

    invoke-direct {v2, p0}, Lcom/facebook/auth/login/AuthOperations$11;-><init>(Lcom/facebook/auth/login/AuthOperations;)V

    invoke-virtual {v1, v2}, LX/2Xo;->a(Ljava/lang/Runnable;)V

    .line 569963
    const v1, 0x940007

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569964
    const v1, 0x940008

    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569965
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569966
    const v4, 0x940009

    invoke-interface {v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569967
    const v4, 0x940009

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569968
    invoke-virtual {v1}, LX/16B;->i()V

    .line 569969
    const v1, 0x940009

    const/4 v4, 0x2

    invoke-interface {v3, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 569970
    :cond_6
    :try_start_2
    const-string v5, "AUTH_OPERATION"

    move-object v4, p0

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move/from16 v9, p4

    invoke-static/range {v4 .. v9}, Lcom/facebook/auth/login/AuthOperations;->a$redex0(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 569971
    const v1, 0x940006

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 569972
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 569973
    const v1, 0x940006

    const/4 v4, 0x3

    :try_start_3
    invoke-interface {v3, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569974
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v4, "AuthExpireSession failure"

    invoke-virtual {v1, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 569975
    :cond_7
    const v1, 0x940008

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569976
    const v1, 0x94000a

    invoke-interface {v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569977
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569978
    const v4, 0x94000b

    invoke-interface {v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569979
    const v4, 0x94000b

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569980
    invoke-virtual {v1}, LX/16B;->j()V

    .line 569981
    const v1, 0x94000b

    const/4 v4, 0x2

    invoke-interface {v3, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_6

    .line 569982
    :cond_8
    const v1, 0x94000a

    const/4 v2, 0x2

    invoke-interface {v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 569983
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->k()V

    .line 569984
    invoke-interface {v3, v11, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569985
    return-void
.end method

.method public static a$redex0(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 569921
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 569922
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 569923
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    new-instance v2, LX/41u;

    invoke-direct {v2, p2, p3}, LX/41u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 569924
    :goto_0
    return-void

    .line 569925
    :cond_0
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->h:LX/28U;

    new-instance v2, LX/41t;

    invoke-direct {v2, p4, p5}, LX/41t;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private b(Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 569856
    :try_start_0
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->o:LX/2Ym;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "AuthOperations"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 569857
    iget-object v1, v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->c:LX/0Px;

    move-object v3, v1

    .line 569858
    iget-boolean v1, v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->a:Z

    move v1, v1

    .line 569859
    if-eqz v1, :cond_0

    .line 569860
    iget-object v1, v0, Lcom/facebook/auth/protocol/WorkCommunityPeekResult;->b:Lcom/facebook/work/auth/request/model/WorkCommunity;

    move-object v0, v1

    .line 569861
    invoke-static {p0, p1, v0}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Lcom/facebook/work/auth/request/model/WorkCommunity;)V

    .line 569862
    :goto_0
    return-object p1

    .line 569863
    :catch_0
    new-instance v0, LX/41f;

    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-direct {v0, v1}, LX/41f;-><init>(LX/1nY;)V

    throw v0

    .line 569864
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 569865
    :cond_1
    new-instance v0, LX/41f;

    sget-object v1, LX/1nY;->WORK_AUTH_FAILED:LX/1nY;

    invoke-direct {v0, v1}, LX/41f;-><init>(LX/1nY;)V

    throw v0

    .line 569866
    :cond_2
    if-eqz p2, :cond_5

    .line 569867
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    .line 569868
    iget-object v5, v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->d:Ljava/lang/String;

    move-object v5, v5

    .line 569869
    invoke-virtual {p2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 569870
    new-instance v1, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    .line 569871
    iget-object v2, v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    move-object v2, v2

    .line 569872
    iget-object v3, v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    move-object v3, v3

    .line 569873
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v4

    .line 569874
    iget-object v5, v4, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v4, v5

    .line 569875
    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569876
    invoke-static {p0, v1, v0}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object p1

    goto :goto_0

    .line 569877
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 569878
    :cond_4
    const-string v0, "SwitchToWorkAccountFailed"

    invoke-virtual {p0, v0, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Ljava/lang/String;Z)V

    .line 569879
    new-instance v0, LX/41f;

    sget-object v1, LX/1nY;->WORK_AUTH_FAILED:LX/1nY;

    invoke-direct {v0, v1}, LX/41f;-><init>(LX/1nY;)V

    throw v0

    .line 569880
    :cond_5
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 569881
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/auth/request/model/WorkCommunity;

    .line 569882
    new-instance v1, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    .line 569883
    iget-object v2, v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->a:Ljava/lang/String;

    move-object v2, v2

    .line 569884
    iget-object v3, v0, Lcom/facebook/work/auth/request/model/WorkCommunity;->b:Ljava/lang/String;

    move-object v3, v3

    .line 569885
    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v4

    .line 569886
    iget-object v5, v4, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v4, v5

    .line 569887
    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569888
    invoke-static {p0, v1, v0}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object p1

    goto :goto_0

    .line 569889
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 569890
    const-string v1, "username"

    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v2

    .line 569891
    iget-object v4, v2, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    move-object v2, v4

    .line 569892
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569893
    const-string v1, "token"

    invoke-interface {p1}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v2

    .line 569894
    iget-object v4, v2, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v2, v4

    .line 569895
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569896
    const-string v1, "work_communities_param"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 569897
    new-instance v1, LX/41f;

    sget-object v2, LX/1nY;->WORK_AUTH_COMMUNITY_ID_REQUIRED:LX/1nY;

    invoke-direct {v1, v2, v0}, LX/41f;-><init>(LX/1nY;Landroid/os/Bundle;)V

    throw v1
.end method

.method private static b(LX/0QB;)Lcom/facebook/auth/login/AuthOperations;
    .locals 27

    .prologue
    .line 569854
    new-instance v2, Lcom/facebook/auth/login/AuthOperations;

    invoke-static/range {p0 .. p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2Xi;->b(LX/0QB;)Ljava/util/Set;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/2E3;->a(LX/0QB;)LX/2E3;

    move-result-object v6

    check-cast v6, LX/2E3;

    invoke-static/range {p0 .. p0}, LX/2Xn;->a(LX/0QB;)LX/2Xn;

    move-result-object v7

    check-cast v7, LX/2Xn;

    invoke-static/range {p0 .. p0}, LX/2Xl;->a(LX/0QB;)LX/2Xl;

    move-result-object v8

    check-cast v8, LX/2Xl;

    invoke-static/range {p0 .. p0}, LX/28U;->a(LX/0QB;)LX/28U;

    move-result-object v9

    check-cast v9, LX/28U;

    const/16 v10, 0x17d2

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xb83

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/2E4;->a(LX/0QB;)LX/2E4;

    move-result-object v12

    check-cast v12, LX/2E4;

    const/16 v13, 0x1455

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2Xo;->a(LX/0QB;)LX/2Xo;

    move-result-object v14

    check-cast v14, LX/2Xo;

    invoke-static/range {p0 .. p0}, LX/2Bn;->a(LX/0QB;)LX/2Bn;

    move-result-object v15

    check-cast v15, LX/2Bn;

    invoke-static/range {p0 .. p0}, LX/2Bo;->a(LX/0QB;)LX/2Bo;

    move-result-object v16

    check-cast v16, LX/2Bo;

    invoke-static/range {p0 .. p0}, LX/2Ym;->a(LX/0QB;)LX/2Ym;

    move-result-object v17

    check-cast v17, LX/2Ym;

    invoke-static/range {p0 .. p0}, LX/28Z;->a(LX/0QB;)LX/28Z;

    move-result-object v18

    check-cast v18, LX/28Z;

    const/16 v19, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/29y;->a(LX/0QB;)LX/2A0;

    move-result-object v20

    check-cast v20, LX/2A0;

    const/16 v21, 0x18f

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/2Dp;->a(LX/0QB;)LX/2Dp;

    move-result-object v22

    check-cast v22, LX/2Dp;

    invoke-static/range {p0 .. p0}, LX/2Xj;->a(LX/0QB;)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x103d

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x164d    # 8.0E-42f

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/28a;->a(LX/0QB;)LX/28a;

    move-result-object v26

    check-cast v26, LX/28a;

    invoke-direct/range {v2 .. v26}, Lcom/facebook/auth/login/AuthOperations;-><init>(LX/0WJ;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;LX/2E3;LX/2Xn;LX/2Xl;LX/28U;LX/0Ot;LX/0Or;LX/2E4;LX/0Or;LX/2Xo;LX/2Bn;LX/2Bo;LX/2Ym;LX/28Z;LX/0Ot;LX/2A0;LX/0Ot;LX/2Dp;LX/0Ot;LX/0Ot;LX/0Ot;LX/28a;)V

    .line 569855
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const v10, 0x94000c

    const/4 v9, 0x3

    const v8, 0x94000f

    const v7, 0x94000d

    const/4 v6, 0x2

    .line 569829
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 569830
    invoke-interface {v0, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569831
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569832
    const v2, 0x94000d

    :try_start_0
    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569833
    const v2, 0x94000d

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569834
    invoke-virtual {v1}, LX/16B;->g()V

    .line 569835
    const v2, 0x94000d

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 569836
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 569837
    invoke-interface {v0, v7, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569838
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v5, "ClearPrivacyCriticalKeys failure"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 569839
    :cond_0
    invoke-interface {v0, v10, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569840
    const v1, 0x94000e

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569841
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16B;

    .line 569842
    const v2, 0x94000f

    :try_start_1
    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569843
    const v2, 0x94000f

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 569844
    invoke-virtual {v1}, LX/16B;->h()V

    .line 569845
    const v2, 0x94000f

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 569846
    :catch_1
    move-exception v2

    move-object v3, v2

    .line 569847
    invoke-interface {v0, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569848
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v5, "ClearUserData failure"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 569849
    :cond_1
    const v1, 0x94000e

    invoke-interface {v0, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569850
    const v1, 0x940010

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 569851
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->f()V

    .line 569852
    const v1, 0x940010

    invoke-interface {v0, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 569853
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 569827
    invoke-static {p0, v0, v0, p1, p2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 569828
    return-void
.end method
