.class public final Lcom/facebook/auth/login/AuthOperations$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/41I;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/auth/login/AuthOperations;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 666149
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$3;->c:Lcom/facebook/auth/login/AuthOperations;

    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/auth/login/AuthOperations$3;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 666150
    const/4 v3, 0x0

    .line 666151
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$3;->c:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 666152
    new-instance v0, LX/28d;

    new-instance v1, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$3;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/auth/login/AuthOperations$3;->b:Ljava/lang/String;

    sget-object v6, LX/28K;->WORK_REGISTRATION_AUTOLOGIN_NONCE:LX/28K;

    invoke-direct {v1, v4, v5, v6}, Lcom/facebook/auth/credentials/PasswordCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$3;->c:Lcom/facebook/auth/login/AuthOperations;

    iget-object v4, v4, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/28d;-><init>(Lcom/facebook/auth/credentials/PasswordCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;Ljava/lang/String;)V

    .line 666153
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$3;->c:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 666154
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations$3;->c:Lcom/facebook/auth/login/AuthOperations;

    iget-object v2, v2, Lcom/facebook/auth/login/AuthOperations;->e:LX/2Xn;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "AuthOperations"

    invoke-static {v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    return-object v0
.end method
