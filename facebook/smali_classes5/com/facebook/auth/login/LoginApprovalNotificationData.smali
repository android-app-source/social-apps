.class public Lcom/facebook/auth/login/LoginApprovalNotificationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/login/LoginApprovalNotificationData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:LX/41b;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666435
    new-instance v0, LX/41a;

    invoke-direct {v0}, LX/41a;-><init>()V

    sput-object v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 666436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a:Ljava/lang/String;

    .line 666438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->b:Ljava/lang/String;

    .line 666439
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->c:Ljava/lang/String;

    .line 666440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->d:Ljava/lang/String;

    .line 666441
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/41b;

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->e:LX/41b;

    .line 666442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->f:Ljava/lang/String;

    .line 666443
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->g:Ljava/lang/String;

    .line 666444
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->h:Ljava/lang/String;

    .line 666445
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 666446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666447
    iput-object p1, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a:Ljava/lang/String;

    .line 666448
    iput-object p2, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->b:Ljava/lang/String;

    .line 666449
    iput-object p3, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->c:Ljava/lang/String;

    .line 666450
    iput-object p4, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->d:Ljava/lang/String;

    .line 666451
    iput-object p5, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->e:LX/41b;

    .line 666452
    iput-object p6, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->f:Ljava/lang/String;

    .line 666453
    iput-object p7, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->g:Ljava/lang/String;

    .line 666454
    iput-object p8, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->h:Ljava/lang/String;

    .line 666455
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/auth/login/LoginApprovalNotificationData;
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 666456
    const-string v0, ""

    .line 666457
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 666458
    const-string v1, "datr"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 666459
    const-string v1, "ip"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 666460
    const-string v1, "device"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 666461
    const-string v1, "time"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 666462
    const-string v1, "locale"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 666463
    const-string v1, "location"

    invoke-virtual {v9, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 666464
    const-string v8, "title"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 666465
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    move-object v0, v5

    .line 666466
    :goto_0
    return-object v0

    .line 666467
    :catch_0
    move-object v0, v5

    goto :goto_0

    .line 666468
    :cond_1
    :try_start_1
    const-string v5, "device_type"

    invoke-virtual {v9, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 666469
    :goto_1
    invoke-static {v0}, LX/41b;->getDeviceType(Ljava/lang/String;)LX/41b;

    move-result-object v5

    .line 666470
    new-instance v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/auth/login/LoginApprovalNotificationData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 666471
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 666472
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666473
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666474
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666475
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666476
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->e:LX/41b;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 666477
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666478
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666479
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666480
    return-void
.end method
