.class public final Lcom/facebook/auth/login/KindleSsoUtil$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/27d;


# direct methods
.method public constructor <init>(LX/27d;)V
    .locals 0

    .prologue
    .line 666384
    iput-object p1, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 666385
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 666386
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 666387
    :try_start_0
    const-string v0, "com.amazon.FacebookSSOService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 666388
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 666389
    const-string v3, "method_name"

    const-string v4, "getFBAccessToken"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666390
    const-string v3, "app_id"

    iget-object v4, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    iget-object v4, v4, LX/27d;->c:LX/00H;

    invoke-virtual {v4}, LX/00H;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 666391
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 666392
    iget-object v0, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    iget-object v0, v0, LX/27d;->f:Landroid/os/IBinder;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 666393
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 666394
    invoke-virtual {v2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 666395
    const-string v3, "access_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 666396
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 666397
    iget-object v3, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    iget-object v3, v3, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v4, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No access token in reply: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 666398
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 666399
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 666400
    :goto_1
    return-void

    .line 666401
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    iget-object v0, v0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const v4, 0x1f56dfba

    invoke-static {v0, v3, v4}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 666402
    :catch_0
    move-exception v0

    .line 666403
    :try_start_2
    iget-object v3, p0, Lcom/facebook/auth/login/KindleSsoUtil$3;->a:LX/27d;

    iget-object v3, v3, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v3, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 666404
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 666405
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 666406
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 666407
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method
