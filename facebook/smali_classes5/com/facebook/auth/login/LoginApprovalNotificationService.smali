.class public Lcom/facebook/auth/login/LoginApprovalNotificationService;
.super LX/1ZN;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/concurrent/ExecutorService;

.field public d:LX/4gS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666492
    const-class v0, Lcom/facebook/auth/login/LoginApprovalNotificationService;

    sput-object v0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 666493
    sget-object v0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 666494
    return-void
.end method

.method private a(LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/4gS;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/4gS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 666495
    iput-object p1, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->b:LX/0Ot;

    .line 666496
    iput-object p2, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->c:Ljava/util/concurrent/ExecutorService;

    .line 666497
    iput-object p3, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->d:LX/4gS;

    .line 666498
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;

    const/16 v0, 0xafd

    invoke-static {v1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {v1}, LX/4gS;->a(LX/0QB;)LX/4gS;

    move-result-object v1

    check-cast v1, LX/4gS;

    invoke-direct {p0, v2, v0, v1}, Lcom/facebook/auth/login/LoginApprovalNotificationService;->a(LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/4gS;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x11f851cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 666499
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 666500
    :cond_0
    const/16 v0, 0x25

    const v1, 0x149ceeef

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 666501
    :goto_0
    return-void

    .line 666502
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 666503
    const-string v1, "arg_action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 666504
    const-string v3, "extra_show_toast"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 666505
    const-string v4, "extra_login_approval_notification_data"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;

    .line 666506
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    if-nez v0, :cond_3

    .line 666507
    :cond_2
    const v0, -0x5572bea1

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto :goto_0

    .line 666508
    :cond_3
    iget-object v4, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->d:LX/4gS;

    .line 666509
    iget-object v5, v4, LX/4gS;->a:LX/0if;

    iget-object v6, v4, LX/4gS;->b:LX/0ih;

    invoke-virtual {v5, v6}, LX/0if;->a(LX/0ih;)V

    .line 666510
    iget-object v5, v4, LX/4gS;->a:LX/0if;

    iget-object v6, v4, LX/4gS;->b:LX/0ih;

    const-string p1, "APPROVE_FROM_ACTION"

    invoke-virtual {v5, v6, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 666511
    const-string v4, "action_approve"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 666512
    const-string v1, "LOGIN_APPROVE"

    .line 666513
    :goto_1
    new-instance v4, LX/4K6;

    invoke-direct {v4}, LX/4K6;-><init>()V

    .line 666514
    const-string v5, "response_type"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666515
    move-object v1, v4

    .line 666516
    iget-object v4, v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->b:Ljava/lang/String;

    move-object v4, v4

    .line 666517
    const-string v5, "datr"

    invoke-virtual {v1, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666518
    move-object v1, v1

    .line 666519
    iget-object v4, v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->c:Ljava/lang/String;

    move-object v4, v4

    .line 666520
    const-string v5, "ip"

    invoke-virtual {v1, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666521
    move-object v1, v1

    .line 666522
    iget-object v4, v0, Lcom/facebook/auth/login/LoginApprovalNotificationData;->d:Ljava/lang/String;

    move-object v0, v4

    .line 666523
    const-string v4, "device"

    invoke-virtual {v1, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666524
    move-object v0, v1

    .line 666525
    new-instance v1, LX/42L;

    invoke-direct {v1}, LX/42L;-><init>()V

    move-object v1, v1

    .line 666526
    const-string v4, "input"

    invoke-virtual {v1, v4, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 666527
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 666528
    iget-object v0, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 666529
    if-eqz v3, :cond_4

    .line 666530
    new-instance v1, LX/41c;

    invoke-direct {v1, p0}, LX/41c;-><init>(Lcom/facebook/auth/login/LoginApprovalNotificationService;)V

    iget-object v3, p0, Lcom/facebook/auth/login/LoginApprovalNotificationService;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 666531
    :cond_4
    const v0, -0x5aaa114a

    invoke-static {v0, v2}, LX/02F;->d(II)V

    goto/16 :goto_0

    .line 666532
    :cond_5
    const-string v1, "LOGIN_DENY"

    goto :goto_1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x21a2ab43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 666533
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 666534
    invoke-static {p0, p0}, Lcom/facebook/auth/login/LoginApprovalNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 666535
    const/16 v1, 0x25

    const v2, -0xafeba1b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
