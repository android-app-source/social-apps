.class public Lcom/facebook/auth/login/CheckApprovedMachineParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/login/CheckApprovedMachineParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568970
    new-instance v0, LX/41T;

    invoke-direct {v0}, LX/41T;-><init>()V

    sput-object v0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 568958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568959
    iput-wide p1, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->a:J

    .line 568960
    iput-object p3, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->b:Ljava/lang/String;

    .line 568961
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 568966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568967
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->a:J

    .line 568968
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->b:Ljava/lang/String;

    .line 568969
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 568965
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 568962
    iget-wide v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 568963
    iget-object v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568964
    return-void
.end method
