.class public final Lcom/facebook/auth/login/AuthOperations$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/41I;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

.field public final synthetic b:Lcom/facebook/auth/login/AuthOperations;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;)V
    .locals 0

    .prologue
    .line 666166
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$6;->b:Lcom/facebook/auth/login/AuthOperations;

    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$6;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 666167
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$6;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 666168
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$6;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 666169
    new-instance v3, LX/42P;

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$6;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$6;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v3, v4, v1, v2}, LX/42P;-><init>(Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;ZLjava/lang/String;)V

    .line 666170
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$6;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->n:LX/2Bo;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "AuthOperations"

    invoke-static {v2, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    return-object v0
.end method
