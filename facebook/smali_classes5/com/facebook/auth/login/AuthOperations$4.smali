.class public final Lcom/facebook/auth/login/AuthOperations$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/41I;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

.field public final synthetic b:Lcom/facebook/auth/login/AuthOperations;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;)V
    .locals 0

    .prologue
    .line 666155
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$4;->b:Lcom/facebook/auth/login/AuthOperations;

    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$4;->a:Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 666156
    const/4 v3, 0x0

    .line 666157
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$4;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 666158
    new-instance v0, LX/42T;

    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$4;->a:Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$4;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v4, v4, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, LX/42T;-><init>(Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;)V

    .line 666159
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$4;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 666160
    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations$4;->b:Lcom/facebook/auth/login/AuthOperations;

    iget-object v2, v2, Lcom/facebook/auth/login/AuthOperations;->p:LX/28Z;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "AuthOperations"

    invoke-static {v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    return-object v0
.end method
