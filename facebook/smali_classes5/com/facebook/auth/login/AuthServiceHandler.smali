.class public Lcom/facebook/auth/login/AuthServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/auth/login/AuthOperations;

.field private final b:LX/28b;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;LX/28b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 570056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570057
    iput-object p1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570058
    iput-object p2, p0, Lcom/facebook/auth/login/AuthServiceHandler;->b:LX/28b;

    .line 570059
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/auth/login/AuthServiceHandler;
    .locals 5

    .prologue
    .line 570177
    const-class v1, Lcom/facebook/auth/login/AuthServiceHandler;

    monitor-enter v1

    .line 570178
    :try_start_0
    sget-object v0, Lcom/facebook/auth/login/AuthServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 570179
    sput-object v2, Lcom/facebook/auth/login/AuthServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 570180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 570182
    new-instance p0, Lcom/facebook/auth/login/AuthServiceHandler;

    invoke-static {v0}, Lcom/facebook/auth/login/AuthOperations;->a(LX/0QB;)Lcom/facebook/auth/login/AuthOperations;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/login/AuthOperations;

    invoke-static {v0}, LX/28b;->a(LX/0QB;)LX/28b;

    move-result-object v4

    check-cast v4, LX/28b;

    invoke-direct {p0, v3, v4}, Lcom/facebook/auth/login/AuthServiceHandler;-><init>(Lcom/facebook/auth/login/AuthOperations;LX/28b;)V

    .line 570183
    move-object v0, p0

    .line 570184
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 570185
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/auth/login/AuthServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 570186
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 570187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 570060
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 570061
    const-string v1, "auth_sso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 570062
    iget-object v0, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570063
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 570064
    const-string v2, "accessToken"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570065
    new-instance v2, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;

    invoke-direct {v2, v0, v1}, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    move-object v0, v2

    .line 570066
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 570067
    :goto_0
    return-object v0

    .line 570068
    :cond_0
    const-string v1, "auth_work_sso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 570069
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570070
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    const-string v2, "accessToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "targetWorkEmail"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 570071
    new-instance v3, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;

    invoke-direct {v3, v1, v2}, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;)V

    invoke-static {v1, v3, v0}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v3

    move-object v0, v3

    .line 570072
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 570073
    :cond_1
    const-string v1, "auth_password"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 570074
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570075
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 570076
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570077
    new-instance v2, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;

    invoke-direct {v2, v1, v0}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/PasswordCredentials;)V

    invoke-static {v1, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    move-object v0, v2

    .line 570078
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 570079
    :cond_2
    const-string v1, "auth_password_work"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 570080
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 570081
    const-string v0, "passwordCredentials"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 570082
    const-string v2, "targetWorkEmail"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 570083
    const-string v3, "workCodeVerifier"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570084
    iget-object v3, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570085
    new-instance v4, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;

    invoke-direct {v4, v3, v0, v1}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/PasswordCredentials;Ljava/lang/String;)V

    invoke-static {v3, v4, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v4

    move-object v0, v4

    .line 570086
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 570087
    :cond_3
    const-string v1, "auth_work_user_switch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570088
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570089
    const-string v1, "workUserSwitchCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;

    .line 570090
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570091
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;Lcom/facebook/work/auth/request/model/WorkCommunity;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    move-object v0, v2

    .line 570092
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570093
    :cond_4
    const-string v1, "auth_logout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 570094
    iget-object v0, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    invoke-virtual {v0, v4, v3}, Lcom/facebook/auth/login/AuthOperations;->a(Ljava/lang/String;Z)V

    .line 570095
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 570096
    goto/16 :goto_0

    .line 570097
    :cond_5
    const-string v1, "login"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 570098
    iget-object v0, p0, Lcom/facebook/auth/login/AuthServiceHandler;->b:LX/28b;

    .line 570099
    iget-object v1, v0, LX/28b;->a:LX/0WJ;

    instance-of v1, v1, LX/0WJ;

    const-string v2, "handleLogin can only be used with LoggedInUserSessionManager"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 570100
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 570101
    iget-object v1, v0, LX/28b;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/335;

    .line 570102
    invoke-interface {v1}, LX/335;->a()LX/2ZE;

    move-result-object v1

    .line 570103
    if-eqz v1, :cond_6

    .line 570104
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 570105
    :cond_7
    iget-object v1, v0, LX/28b;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2XA;

    .line 570106
    invoke-interface {v1}, LX/2XA;->a()V

    .line 570107
    invoke-interface {v1}, LX/2XA;->c()LX/2ZE;

    move-result-object v1

    .line 570108
    if-eqz v1, :cond_8

    .line 570109
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 570110
    :cond_9
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 570111
    sget-object v3, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v1, v3}, LX/14U;->a(LX/14V;)V

    .line 570112
    iget-object v3, v0, LX/28b;->b:LX/28W;

    const-string v4, "handleLogin"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-virtual {v3, v4, p0, v2, v1}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 570113
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 570114
    goto/16 :goto_0

    .line 570115
    :cond_a
    const-string v1, "auth_create_messenger_account"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 570116
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570117
    const-string v1, "createAccountParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    .line 570118
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 570119
    const-string v2, "search_for_soft_matched_account"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 570120
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 570121
    const-string v3, "account_recovery_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 570122
    iget-object v3, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570123
    new-instance v4, Lcom/facebook/auth/login/AuthOperations$5;

    invoke-direct {v4, v3, v0, v1, v2}, Lcom/facebook/auth/login/AuthOperations$5;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;ZLjava/lang/String;)V

    invoke-static {v3, v4}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v4

    move-object v0, v4

    .line 570124
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570125
    :cond_b
    const-string v1, "auth_login_bypass_with_messenger_credentials"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 570126
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570127
    const-string v1, "loginMessengerAccountParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    .line 570128
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570129
    new-instance v2, Lcom/facebook/auth/login/AuthOperations$6;

    invoke-direct {v2, v1, v0}, Lcom/facebook/auth/login/AuthOperations$6;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;)V

    invoke-static {v1, v2}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    move-object v0, v2

    .line 570130
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570131
    :cond_c
    const-string v1, "auth_switch_accounts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 570132
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570133
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 570134
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 570135
    const-string v2, "alternative_token_app_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570136
    iget-object v2, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570137
    new-instance v3, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;

    invoke-direct {v3, v2, v0}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/PasswordCredentials;)V

    invoke-virtual {v3}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;->a()Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    move-result-object v3

    move-object v0, v3

    .line 570138
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570139
    :cond_d
    const-string v1, "auth_switch_accounts_sso"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 570140
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570141
    const-string v1, "accessToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 570142
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 570143
    const-string v2, "alternative_token_app_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570144
    iget-object v2, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570145
    new-instance v3, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;

    invoke-direct {v3, v2, v0}, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/auth/login/AuthOperations$SsoAuthOperation;->a()Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    move-result-object v3

    move-object v0, v3

    .line 570146
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570147
    :cond_e
    const-string v1, "auth_switch_accounts_dbl"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 570148
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570149
    const-string v1, "dblCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 570150
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 570151
    const-string v2, "alternative_token_app_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570152
    iget-object v2, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570153
    new-instance v3, Lcom/facebook/auth/login/AuthOperations$DblAuthOperation;

    invoke-direct {v3, v2, v0}, Lcom/facebook/auth/login/AuthOperations$DblAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;)V

    invoke-virtual {v3}, Lcom/facebook/auth/login/AuthOperations$DblAuthOperation;->a()Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;Ljava/lang/String;)Lcom/facebook/auth/component/AccountSwitchingAuthenticationResult;

    move-result-object v3

    move-object v0, v3

    .line 570154
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570155
    :cond_f
    const-string v1, "auth_messenger_only_migrate_accounts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 570156
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570157
    const-string v1, "passwordCredentials"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/PasswordCredentials;

    .line 570158
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    .line 570159
    new-instance v2, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;

    invoke-direct {v2, v1, v0}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/credentials/PasswordCredentials;)V

    invoke-virtual {v2}, Lcom/facebook/auth/login/AuthOperations$PasswordAuthOperation;->a()Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    .line 570160
    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v3

    .line 570161
    iget-object v4, v3, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    move-object v3, v4

    .line 570162
    invoke-interface {v2}, Lcom/facebook/auth/component/AuthenticationResult;->b()Lcom/facebook/auth/credentials/FacebookCredentials;

    move-result-object v4

    .line 570163
    iget-object v5, v4, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    move-object v4, v5

    .line 570164
    iget-object v5, v1, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object p0, LX/26p;->A:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v5, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 570165
    const/4 v5, 0x0

    const/4 p0, 0x0

    invoke-static {v1, v3, v4, v5, p0}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 570166
    iget-object v3, v1, Lcom/facebook/auth/login/AuthOperations;->r:LX/2A0;

    invoke-virtual {v3}, LX/2A0;->c()V

    .line 570167
    new-instance v3, LX/41J;

    invoke-direct {v3, v1, v2}, LX/41J;-><init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/component/AuthenticationResult;)V

    invoke-static {v1, v3}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v2

    .line 570168
    iget-object v3, v1, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/26p;->A:LX/0Tn;

    invoke-interface {v3, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 570169
    move-object v0, v2

    .line 570170
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570171
    :cond_10
    const-string v1, "auth_temporary_login_nonce"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 570172
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 570173
    iget-object v1, p0, Lcom/facebook/auth/login/AuthServiceHandler;->a:Lcom/facebook/auth/login/AuthOperations;

    const-string v2, "user_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "temporary_login_nonce"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 570174
    new-instance v3, Lcom/facebook/auth/login/AuthOperations$3;

    invoke-direct {v3, v1, v2, v0}, Lcom/facebook/auth/login/AuthOperations$3;-><init>(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lcom/facebook/auth/login/AuthOperations;->a(Lcom/facebook/auth/login/AuthOperations;LX/41I;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v3

    move-object v0, v3

    .line 570175
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 570176
    :cond_11
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
