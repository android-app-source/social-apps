.class public final Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 666250
    const-class v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod_ResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666239
    new-instance v0, LX/41P;

    invoke-direct {v0}, LX/41P;-><init>()V

    sput-object v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 666240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666241
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 666242
    iput-object v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->a:Ljava/util/List;

    .line 666243
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 666244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666245
    iget-object v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->a:Ljava/util/List;

    sget-object v1, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 666246
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 666247
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 666248
    iget-object v0, p0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 666249
    return-void
.end method
