.class public final Lcom/facebook/auth/login/AuthOperations$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Z

.field public final synthetic f:Lcom/facebook/auth/login/AuthOperations;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 666133
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$10;->f:Lcom/facebook/auth/login/AuthOperations;

    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$10;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p3, p0, Lcom/facebook/auth/login/AuthOperations$10;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/auth/login/AuthOperations$10;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/auth/login/AuthOperations$10;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/facebook/auth/login/AuthOperations$10;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const v6, 0x940019

    .line 666134
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$10;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 666135
    :try_start_0
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$10;->f:Lcom/facebook/auth/login/AuthOperations;

    const-string v1, "MAGIC_LOGOUT_TAG"

    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations$10;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/auth/login/AuthOperations$10;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$10;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/facebook/auth/login/AuthOperations$10;->e:Z

    .line 666136
    invoke-static/range {v0 .. v5}, Lcom/facebook/auth/login/AuthOperations;->a$redex0(Lcom/facebook/auth/login/AuthOperations;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 666137
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$10;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x940019

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 666138
    :goto_0
    return-void

    .line 666139
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 666140
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$10;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x3

    invoke-interface {v0, v6, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 666141
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$10;->f:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "AuthExpireSession failure"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
