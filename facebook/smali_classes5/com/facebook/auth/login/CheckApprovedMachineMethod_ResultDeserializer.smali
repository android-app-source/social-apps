.class public Lcom/facebook/auth/login/CheckApprovedMachineMethod_ResultDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 666296
    const-class v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    new-instance v1, Lcom/facebook/auth/login/CheckApprovedMachineMethod_ResultDeserializer;

    invoke-direct {v1}, Lcom/facebook/auth/login/CheckApprovedMachineMethod_ResultDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 666297
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 666298
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 666299
    const-class v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 666300
    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 666301
    new-instance v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;

    invoke-direct {v0}, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;-><init>()V

    .line 666302
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 666303
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 666304
    const/4 v0, 0x0

    .line 666305
    :cond_0
    move-object v0, v0

    .line 666306
    return-object v0

    .line 666307
    :cond_1
    :goto_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 666308
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 666309
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 666310
    const-string v2, "data"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 666311
    const/4 v2, 0x0

    .line 666312
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->START_ARRAY:LX/15z;

    if-ne p0, p2, :cond_3

    .line 666313
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 666314
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->END_ARRAY:LX/15z;

    if-eq p0, p2, :cond_3

    .line 666315
    invoke-static {p1}, LX/41R;->a(LX/15w;)Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;

    move-result-object p0

    .line 666316
    if-eqz p0, :cond_2

    .line 666317
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 666318
    :cond_3
    iput-object v2, v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$Result;->a:Ljava/util/List;

    .line 666319
    :goto_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_0

    :cond_4
    goto :goto_2
.end method
