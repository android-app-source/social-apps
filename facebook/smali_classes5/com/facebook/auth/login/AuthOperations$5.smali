.class public final Lcom/facebook/auth/login/AuthOperations$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/41I;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/auth/login/AuthOperations;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/AuthOperations;Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 666161
    iput-object p1, p0, Lcom/facebook/auth/login/AuthOperations$5;->d:Lcom/facebook/auth/login/AuthOperations;

    iput-object p2, p0, Lcom/facebook/auth/login/AuthOperations$5;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iput-boolean p3, p0, Lcom/facebook/auth/login/AuthOperations$5;->b:Z

    iput-object p4, p0, Lcom/facebook/auth/login/AuthOperations$5;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 666162
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$5;->d:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/11H;

    .line 666163
    iget-object v0, p0, Lcom/facebook/auth/login/AuthOperations$5;->d:Lcom/facebook/auth/login/AuthOperations;

    iget-object v0, v0, Lcom/facebook/auth/login/AuthOperations;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 666164
    new-instance v0, LX/421;

    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$5;->a:Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;

    iget-object v2, p0, Lcom/facebook/auth/login/AuthOperations$5;->d:Lcom/facebook/auth/login/AuthOperations;

    iget-object v2, v2, Lcom/facebook/auth/login/AuthOperations;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-boolean v3, p0, Lcom/facebook/auth/login/AuthOperations$5;->b:Z

    iget-object v4, p0, Lcom/facebook/auth/login/AuthOperations$5;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, LX/421;-><init>(Lcom/facebook/auth/protocol/CreateMessengerAccountCredentials;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 666165
    iget-object v1, p0, Lcom/facebook/auth/login/AuthOperations$5;->d:Lcom/facebook/auth/login/AuthOperations;

    iget-object v1, v1, Lcom/facebook/auth/login/AuthOperations;->m:LX/2Bn;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "AuthOperations"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v6, v1, v0, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/component/AuthenticationResult;

    return-object v0
.end method
