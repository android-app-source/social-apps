.class public Lcom/facebook/auth/viewercontext/ViewerContextSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 667582
    const-class v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    new-instance v1, Lcom/facebook/auth/viewercontext/ViewerContextSerializer;

    invoke-direct {v1}, Lcom/facebook/auth/viewercontext/ViewerContextSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 667583
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 667584
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 667585
    const-string v0, "Must give a non null SerializerProvider"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667586
    iget-object v0, p2, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 667587
    const-string v1, "SerializerProvider must have a non-null config"

    invoke-static {p2, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667588
    invoke-static {v0}, LX/2BE;->a(LX/0m2;)V

    .line 667589
    if-nez p0, :cond_0

    .line 667590
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 667591
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 667592
    invoke-static {p0, p1, p2}, Lcom/facebook/auth/viewercontext/ViewerContextSerializer;->b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0nX;LX/0my;)V

    .line 667593
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 667594
    return-void
.end method

.method private static b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 667595
    const-string v0, "user_id"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667596
    const-string v0, "auth_token"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667597
    const-string v0, "session_cookies_string"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667598
    const-string v0, "is_page_context"

    iget-boolean v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 667599
    const-string v0, "session_secret"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667600
    const-string v0, "session_key"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667601
    const-string v0, "username"

    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 667602
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 667603
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p1, p2, p3}, Lcom/facebook/auth/viewercontext/ViewerContextSerializer;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0nX;LX/0my;)V

    return-void
.end method
