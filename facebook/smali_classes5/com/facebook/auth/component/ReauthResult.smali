.class public Lcom/facebook/auth/component/ReauthResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/component/ReauthResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 665921
    new-instance v0, LX/417;

    invoke-direct {v0}, LX/417;-><init>()V

    sput-object v0, Lcom/facebook/auth/component/ReauthResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 665906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 665907
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/component/ReauthResult;->a:Ljava/lang/String;

    .line 665908
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/auth/component/ReauthResult;->b:J

    .line 665909
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/auth/component/ReauthResult;->c:J

    .line 665910
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 665911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 665912
    iput-object p1, p0, Lcom/facebook/auth/component/ReauthResult;->a:Ljava/lang/String;

    .line 665913
    iput-wide p2, p0, Lcom/facebook/auth/component/ReauthResult;->b:J

    .line 665914
    iput-wide p4, p0, Lcom/facebook/auth/component/ReauthResult;->c:J

    .line 665915
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 665916
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 665917
    iget-object v0, p0, Lcom/facebook/auth/component/ReauthResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 665918
    iget-wide v0, p0, Lcom/facebook/auth/component/ReauthResult;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 665919
    iget-wide v0, p0, Lcom/facebook/auth/component/ReauthResult;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 665920
    return-void
.end method
