.class public Lcom/facebook/auth/credentials/SessionCookieDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 666074
    const-class v0, Lcom/facebook/auth/credentials/SessionCookie;

    new-instance v1, Lcom/facebook/auth/credentials/SessionCookieDeserializer;

    invoke-direct {v1}, Lcom/facebook/auth/credentials/SessionCookieDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 666075
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 666077
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 666078
    const-class v0, Lcom/facebook/auth/credentials/SessionCookie;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 666079
    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666076
    invoke-static {p1}, LX/3Qs;->a(LX/15w;)Lcom/facebook/auth/credentials/SessionCookie;

    move-result-object v0

    return-object v0
.end method
