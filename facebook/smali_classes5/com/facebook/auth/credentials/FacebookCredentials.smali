.class public Lcom/facebook/auth/credentials/FacebookCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/FacebookCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 575999
    new-instance v0, LX/2Wn;

    invoke-direct {v0}, LX/2Wn;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/FacebookCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 576016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576017
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    .line 576018
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    .line 576019
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->c:Ljava/lang/String;

    .line 576020
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->d:Ljava/lang/String;

    .line 576021
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->e:Ljava/lang/String;

    .line 576022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    .line 576023
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 576008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576009
    iput-object p1, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    .line 576010
    iput-object p2, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    .line 576011
    iput-object p3, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->c:Ljava/lang/String;

    .line 576012
    iput-object p4, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->d:Ljava/lang/String;

    .line 576013
    iput-object p5, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->e:Ljava/lang/String;

    .line 576014
    iput-object p6, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    .line 576015
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 576007
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 576000
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576001
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576002
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576003
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576004
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576005
    iget-object v0, p0, Lcom/facebook/auth/credentials/FacebookCredentials;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576006
    return-void
.end method
