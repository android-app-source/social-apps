.class public Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/auth/credentials/LoginCredentials;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final d:LX/41B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666057
    new-instance v0, LX/41A;

    invoke-direct {v0}, LX/41A;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 666051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    .line 666053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->b:Ljava/lang/String;

    .line 666054
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    .line 666055
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/41B;

    iput-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->d:LX/41B;

    .line 666056
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/41B;)V
    .locals 0

    .prologue
    .line 666045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666046
    iput-object p1, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    .line 666047
    iput-object p2, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->b:Ljava/lang/String;

    .line 666048
    iput-object p3, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    .line 666049
    iput-object p4, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->d:LX/41B;

    .line 666050
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 666044
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 666039
    iget-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666040
    iget-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666041
    iget-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666042
    iget-object v0, p0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->d:LX/41B;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 666043
    return-void
.end method
