.class public Lcom/facebook/auth/credentials/SessionCookieSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/auth/credentials/SessionCookie;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 666080
    const-class v0, Lcom/facebook/auth/credentials/SessionCookie;

    new-instance v1, Lcom/facebook/auth/credentials/SessionCookieSerializer;

    invoke-direct {v1}, Lcom/facebook/auth/credentials/SessionCookieSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 666081
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666082
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/auth/credentials/SessionCookie;LX/0nX;)V
    .locals 3

    .prologue
    .line 666083
    const/4 v0, 0x1

    .line 666084
    if-eqz v0, :cond_0

    .line 666085
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 666086
    :cond_0
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 666087
    const-string v1, "name"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666088
    :cond_1
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 666089
    const-string v1, "value"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666090
    :cond_2
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 666091
    const-string v1, "expires"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mExpires:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666092
    :cond_3
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 666093
    const-string v1, "domain"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666094
    :cond_4
    const-string v1, "secure"

    iget-boolean v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mSecure:Z

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Z)V

    .line 666095
    iget-object v1, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 666096
    const-string v1, "path"

    iget-object v2, p0, Lcom/facebook/auth/credentials/SessionCookie;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666097
    :cond_5
    if-eqz v0, :cond_6

    .line 666098
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 666099
    :cond_6
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 666100
    check-cast p1, Lcom/facebook/auth/credentials/SessionCookie;

    invoke-static {p1, p2}, Lcom/facebook/auth/credentials/SessionCookieSerializer;->a(Lcom/facebook/auth/credentials/SessionCookie;LX/0nX;)V

    return-void
.end method
