.class public Lcom/facebook/auth/credentials/OpenIDLoginCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/auth/credentials/LoginCredentials;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/OpenIDLoginCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Lcom/facebook/openidconnect/model/OpenIDCredential;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:LX/41D;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568458
    new-instance v0, LX/41C;

    invoke-direct {v0}, LX/41C;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 568452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568453
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->a:Ljava/lang/String;

    .line 568454
    const-class v0, Lcom/facebook/openidconnect/model/OpenIDCredential;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/openidconnect/model/OpenIDCredential;

    iput-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->b:Lcom/facebook/openidconnect/model/OpenIDCredential;

    .line 568455
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/41D;

    iput-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->c:LX/41D;

    .line 568456
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/openidconnect/model/OpenIDCredential;LX/41D;)V
    .locals 0

    .prologue
    .line 568459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568460
    iput-object p1, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->a:Ljava/lang/String;

    .line 568461
    iput-object p2, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->b:Lcom/facebook/openidconnect/model/OpenIDCredential;

    .line 568462
    iput-object p3, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->c:LX/41D;

    .line 568463
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 568457
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 568448
    iget-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568449
    iget-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->b:Lcom/facebook/openidconnect/model/OpenIDCredential;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 568450
    iget-object v0, p0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->c:LX/41D;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 568451
    return-void
.end method
