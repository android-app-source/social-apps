.class public Lcom/facebook/auth/credentials/PasswordCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/auth/credentials/LoginCredentials;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/PasswordCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:LX/28K;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568662
    new-instance v0, LX/27s;

    invoke-direct {v0}, LX/27s;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/PasswordCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 568657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568658
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    .line 568659
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->b:Ljava/lang/String;

    .line 568660
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/28K;

    iput-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->c:LX/28K;

    .line 568661
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/28K;)V
    .locals 0

    .prologue
    .line 568647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568648
    iput-object p1, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    .line 568649
    iput-object p2, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->b:Ljava/lang/String;

    .line 568650
    iput-object p3, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->c:LX/28K;

    .line 568651
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 568656
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 568652
    iget-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568653
    iget-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568654
    iget-object v0, p0, Lcom/facebook/auth/credentials/PasswordCredentials;->c:LX/28K;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 568655
    return-void
.end method
