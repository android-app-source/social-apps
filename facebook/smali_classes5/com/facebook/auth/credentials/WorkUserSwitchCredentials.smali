.class public Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/auth/credentials/LoginCredentials;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 666116
    new-instance v0, LX/41G;

    invoke-direct {v0}, LX/41G;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 666117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->a:Ljava/lang/String;

    .line 666119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->b:Ljava/lang/String;

    .line 666120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->c:Ljava/lang/String;

    .line 666121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 666122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666123
    iput-object p1, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->a:Ljava/lang/String;

    .line 666124
    iput-object p2, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->b:Ljava/lang/String;

    .line 666125
    iput-object p3, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->c:Ljava/lang/String;

    .line 666126
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 666127
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 666128
    iget-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666129
    iget-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666130
    iget-object v0, p0, Lcom/facebook/auth/credentials/WorkUserSwitchCredentials;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666131
    return-void
.end method
