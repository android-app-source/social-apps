.class public final Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;
.super Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 577858
    new-instance v0, LX/33R;

    invoke-direct {v0}, LX/33R;-><init>()V

    sput-object v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 577813
    invoke-direct {p0, p1}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Landroid/os/Parcel;)V

    .line 577814
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    .line 577815
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    .line 577816
    return-void

    .line 577817
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 577854
    invoke-direct {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;)V

    .line 577855
    iput-object p3, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    .line 577856
    iput-boolean p4, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    .line 577857
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 577853
    const-string v0, "fetchZeroHeaderRequestParams"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 577839
    instance-of v1, p1, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    if-nez v1, :cond_1

    .line 577840
    :cond_0
    :goto_0
    return v0

    .line 577841
    :cond_1
    check-cast p1, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    .line 577842
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v1, v1

    .line 577843
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 577844
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 577845
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 577846
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 577847
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 577848
    iget-object v1, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 577849
    iget-object v2, p1, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 577850
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    .line 577851
    iget-boolean v2, p1, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    move v2, v2

    .line 577852
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 577830
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 577831
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 577832
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 577833
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 577834
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 577835
    iget-object v2, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 577836
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 577837
    iget-boolean v2, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    move v2, v2

    .line 577838
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 577823
    const-class v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "carrierAndSimMccMnc"

    .line 577824
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 577825
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "networkType"

    .line 577826
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 577827
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "machineId"

    .line 577828
    iget-object v2, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 577829
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "forceRefresh"

    iget-boolean v2, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 577818
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 577819
    iget-object v0, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 577820
    iget-boolean v0, p0, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 577821
    return-void

    .line 577822
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
