.class public Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDescriptionText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description_text"
    .end annotation
.end field

.field private final mFriendsText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friends_text"
    .end annotation
.end field

.field private final mLearnMoreText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "learn_more_text"
    .end annotation
.end field

.field private final mLearnMoreUrl:Landroid/net/Uri;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "learn_more_url"
    .end annotation
.end field

.field private final mLegalDisclaimerText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "legal_disclaimer_text"
    .end annotation
.end field

.field private final mLogoUrl:Landroid/net/Uri;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "logo_url"
    .end annotation
.end field

.field private final mOptinConfirmButtonText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_confirm_button_text"
    .end annotation
.end field

.field private final mOptinDeclineButtonCancelText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_decline_button_cancel_text"
    .end annotation
.end field

.field private final mOptinDeclineButtonConfirmText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_decline_button_confirm_text"
    .end annotation
.end field

.field private final mOptinDeclineButtonText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_decline_button_text"
    .end annotation
.end field

.field private final mOptinDeclineConfirmText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_decline_confirm_text"
    .end annotation
.end field

.field private final mOptinDeclineTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "optin_decline_title"
    .end annotation
.end field

.field private final mProfilePictureUrls:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friends_profile_picture_urls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 811279
    const-class v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 811232
    new-instance v0, LX/4pK;

    invoke-direct {v0}, LX/4pK;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 811262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811263
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    .line 811264
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    .line 811265
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    .line 811266
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    .line 811267
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 811268
    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    .line 811269
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    .line 811270
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    .line 811271
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    .line 811272
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    .line 811273
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    .line 811274
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    .line 811275
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    .line 811276
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    .line 811277
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    .line 811278
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 811244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    .line 811246
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    .line 811247
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    .line 811248
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    .line 811249
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 811250
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 811251
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    .line 811252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    .line 811253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    .line 811254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    .line 811255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    .line 811256
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    .line 811257
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    .line 811258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    .line 811259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    .line 811260
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    .line 811261
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811243
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811242
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 811241
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811240
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 811239
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811238
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811234
    instance-of v1, p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    if-nez v1, :cond_1

    .line 811235
    :cond_0
    :goto_0
    return v0

    .line 811236
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;

    .line 811237
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->e()LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->h()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811233
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811208
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 811209
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 811210
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811211
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811212
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811213
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811214
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811215
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811216
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 811207
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "description_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "logo_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friends_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friends_profile_picture_urls"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "legal_disclaimer_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "learn_more_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "learn_more_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_decline_title"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_decline_confirm_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_decline_button_confirm_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_decline_button_cancel_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_confirm_button_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "optin_decline_button_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 811217
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811218
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811219
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLogoUrl:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811220
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mFriendsText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811221
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 811222
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLegalDisclaimerText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811223
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811224
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mLearnMoreUrl:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811225
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811226
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineConfirmText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811227
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonConfirmText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811228
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonCancelText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811229
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinConfirmButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811230
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestResult;->mOptinDeclineButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811231
    return-void
.end method
