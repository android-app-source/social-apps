.class public Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCampaignId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private final mDelayInterval:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "delay_interval"
    .end annotation
.end field

.field private final mTtl:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ttl"
    .end annotation
.end field

.field private final mType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 811136
    const-class v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 811143
    new-instance v0, LX/4pI;

    invoke-direct {v0}, LX/4pI;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 811137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811138
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    .line 811139
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    .line 811140
    iput v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    .line 811141
    iput v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    .line 811142
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 811115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    .line 811117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    .line 811118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    .line 811119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    .line 811120
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811135
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811134
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 811144
    iget v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 811133
    iget v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 811132
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811128
    instance-of v1, p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;

    if-nez v1, :cond_1

    .line 811129
    :cond_0
    :goto_0
    return v0

    .line 811130
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;

    .line 811131
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    invoke-direct {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 811127
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 811126
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "campaignId"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "ttl"

    iget v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "delayInterval"

    iget v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 811121
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mCampaignId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811122
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811123
    iget v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mTtl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 811124
    iget v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityResult;->mDelayInterval:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 811125
    return-void
.end method
