.class public Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;
.super Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 811202
    new-instance v0, LX/4pJ;

    invoke-direct {v0}, LX/4pJ;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 811199
    invoke-direct {p0, p1}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Landroid/os/Parcel;)V

    .line 811200
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    .line 811201
    return-void
.end method

.method public constructor <init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 811196
    invoke-direct {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;)V

    .line 811197
    iput-object p3, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    .line 811198
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811203
    const-string v0, "fetchZeroOptinContentRequestParams"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811185
    instance-of v1, p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    if-nez v1, :cond_1

    .line 811186
    :cond_0
    :goto_0
    return v0

    .line 811187
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    .line 811188
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v1, v1

    .line 811189
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 811190
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 811191
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 811192
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 811193
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    .line 811194
    iget-object v2, p1, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 811195
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 811172
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 811173
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 811174
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 811175
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 811176
    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 811180
    const-class v0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "carrierAndSimMccMnc"

    .line 811181
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v2, v2

    .line 811182
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "networkType"

    .line 811183
    iget-object v2, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 811184
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "screenScale"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 811177
    invoke-super {p0, p1, p2}, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 811178
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroOptinContentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 811179
    return-void
.end method
