.class public Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCampaignTokenToRefreshType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "campaign_token_to_refresh_type"
    .end annotation
.end field

.field private final mClickableLinkText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "clickable_link_text"
    .end annotation
.end field

.field private final mClickableLinkUrl:Landroid/net/Uri;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "clickable_link_url"
    .end annotation
.end field

.field private final mDescriptionText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description_text"
    .end annotation
.end field

.field private final mDetailText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "detail_text"
    .end annotation
.end field

.field private final mFacepileText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "facepile_text"
    .end annotation
.end field

.field private final mImageUrl:Landroid/net/Uri;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_url"
    .end annotation
.end field

.field private final mPrimaryButtonAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_button_action"
    .end annotation
.end field

.field private final mPrimaryButtonIntentUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_button_intent_url"
    .end annotation
.end field

.field private final mPrimaryButtonStep:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_button_step"
    .end annotation
.end field

.field private final mPrimaryButtonText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "primary_button_text"
    .end annotation
.end field

.field private final mProfilePictureUrls:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "facepile_profile_picture_urls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSecondaryButtonAction:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_button_action"
    .end annotation
.end field

.field private final mSecondaryButtonIntentUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_button_intent_url"
    .end annotation
.end field

.field private final mSecondaryButtonOverrideBackOnly:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_button_override_back_only"
    .end annotation
.end field

.field private final mSecondaryButtonStep:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_button_step"
    .end annotation
.end field

.field private final mSecondaryButtonText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "secondary_button_text"
    .end annotation
.end field

.field private final mShouldUseDefaultImage:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_use_default_image"
    .end annotation
.end field

.field private final mSubTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtitle"
    .end annotation
.end field

.field private final mTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 810987
    const-class v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 810988
    new-instance v0, LX/4pG;

    invoke-direct {v0}, LX/4pG;-><init>()V

    sput-object v0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 810989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810990
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    .line 810991
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    .line 810992
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    .line 810993
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    .line 810994
    iput-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    .line 810995
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    .line 810996
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 810997
    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    .line 810998
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    .line 810999
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    .line 811000
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    .line 811001
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    .line 811002
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    .line 811003
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    .line 811004
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    .line 811005
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    .line 811006
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    .line 811007
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    .line 811008
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    .line 811009
    iput-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    .line 811010
    iput-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    .line 811011
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 811012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811013
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    .line 811014
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    .line 811015
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    .line 811016
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    .line 811017
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    .line 811018
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    .line 811019
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 811020
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 811021
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    .line 811022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    .line 811023
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    .line 811024
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    .line 811025
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    .line 811026
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    .line 811027
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    .line 811028
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    .line 811029
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    .line 811030
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    .line 811031
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    .line 811032
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    .line 811033
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    .line 811034
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    .line 811035
    return-void

    :cond_0
    move v0, v2

    .line 811036
    goto :goto_0

    :cond_1
    move v1, v2

    .line 811037
    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810958
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810985
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811038
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 811039
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 811040
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 811041
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811042
    instance-of v1, p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;

    if-nez v1, :cond_1

    .line 811043
    :cond_0
    :goto_0
    return v0

    .line 811044
    :cond_1
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;

    .line 811045
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->e()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->g()LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->j()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->s()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811046
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811047
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811048
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 810986
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810948
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 810949
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810950
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810951
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810952
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810953
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810954
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810955
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810956
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810957
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 810959
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 810960
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 810961
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "subTitle"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "description_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "image_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "should_use_default_image"

    iget-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "facepile_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profile_picture_urls"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "detail_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "clickable_link_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "clickable_link_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "primary_button_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "primary_button_intent_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "primary_button_step"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "primary_button_action"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "secondary_button_text"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "secondary_button_intent_url"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "secondary_button_step"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "secondary_button_action"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "secondary_button_override_back_only"

    iget-boolean v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "campaign_token_to_refresh_type"

    iget-object v2, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 810962
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810963
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSubTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810964
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDescriptionText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810965
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mImageUrl:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810966
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mShouldUseDefaultImage:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 810967
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mFacepileText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810968
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mProfilePictureUrls:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 810969
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mDetailText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810970
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810971
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mClickableLinkUrl:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810972
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810973
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810974
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonStep:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810975
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mPrimaryButtonAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810976
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810977
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonIntentUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810978
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonStep:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810979
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810980
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mSecondaryButtonOverrideBackOnly:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 810981
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentResult;->mCampaignTokenToRefreshType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 810982
    return-void

    :cond_0
    move v0, v2

    .line 810983
    goto :goto_0

    :cond_1
    move v1, v2

    .line 810984
    goto :goto_1
.end method
