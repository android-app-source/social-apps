.class public Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 578729
    const-class v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    new-instance v1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;

    invoke-direct {v1}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 578730
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 578731
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 578732
    if-nez p0, :cond_0

    .line 578733
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 578734
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 578735
    invoke-static {p0, p1, p2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;->b(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0nX;LX/0my;)V

    .line 578736
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 578737
    return-void
.end method

.method private static b(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 578738
    const-string v0, "matcher"

    iget-object v1, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mMatcher:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 578739
    const-string v0, "replacer"

    iget-object v1, p0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->mReplacer:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 578740
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 578741
    check-cast p1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    invoke-static {p1, p2, p3}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRuleSerializer;->a(Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0nX;LX/0my;)V

    return-void
.end method
