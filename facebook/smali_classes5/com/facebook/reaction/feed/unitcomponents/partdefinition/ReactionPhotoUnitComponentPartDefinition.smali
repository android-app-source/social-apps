.class public abstract Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E41;",
        "LX/2km;",
        "LX/E33;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/1Ad;

.field public final d:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 604864
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 604865
    new-instance v0, LX/3aO;

    invoke-direct {v0}, LX/3aO;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/E1i;)V
    .locals 0

    .prologue
    .line 604860
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604861
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->c:LX/1Ad;

    .line 604862
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->d:LX/E1i;

    .line 604863
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604859
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 604846
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v1, 0x0

    .line 604847
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v0

    .line 604848
    invoke-interface {v4}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->aj_()LX/1Fb;

    move-result-object v6

    .line 604849
    invoke-interface {v4}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->k()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

    move-result-object v0

    .line 604850
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 604851
    new-instance v5, LX/E40;

    invoke-direct {v5, p0, v0, p3, p2}, LX/E40;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 604852
    :goto_0
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->c:LX/1Ad;

    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 604853
    invoke-interface {v4}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->c()LX/1f8;

    move-result-object v0

    .line 604854
    if-nez v0, :cond_0

    move-object v3, v1

    .line 604855
    :goto_1
    invoke-interface {v4}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 604856
    invoke-interface {v4}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 604857
    :goto_2
    new-instance v0, LX/E41;

    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->b()F

    move-result v1

    invoke-interface {v6}, LX/1Fb;->c()I

    move-result v7

    int-to-float v7, v7

    invoke-interface {v6}, LX/1Fb;->a()I

    move-result v6

    int-to-float v6, v6

    div-float v6, v7, v6

    invoke-static {v1, v6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-direct/range {v0 .. v5}, LX/E41;-><init>(FLX/1aZ;Landroid/graphics/PointF;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    .line 604858
    :cond_0
    new-instance v3, Landroid/graphics/PointF;

    invoke-interface {v0}, LX/1f8;->a()D

    move-result-wide v8

    double-to-float v7, v8

    invoke-interface {v0}, LX/1f8;->b()D

    move-result-wide v8

    double-to-float v0, v8

    invoke-direct {v3, v7, v0}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_1

    :cond_1
    move-object v4, v1

    goto :goto_2

    :cond_2
    move-object v5, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x76b396af

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604838
    check-cast p2, LX/E41;

    check-cast p4, LX/E33;

    .line 604839
    iget-object v1, p2, LX/E41;->d:Ljava/lang/String;

    .line 604840
    if-nez v1, :cond_0

    .line 604841
    iget-object v2, p4, LX/E33;->c:Landroid/widget/TextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604842
    :goto_0
    iget v1, p2, LX/E41;->a:F

    iget-object v2, p2, LX/E41;->b:LX/1aZ;

    iget-object p0, p2, LX/E41;->c:Landroid/graphics/PointF;

    iget-object p1, p2, LX/E41;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2, p0, p1}, LX/E33;->a(FLX/1aZ;Landroid/graphics/PointF;Landroid/view/View$OnClickListener;)V

    .line 604843
    const/16 v1, 0x1f

    const v2, -0x46846428

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 604844
    :cond_0
    iget-object v2, p4, LX/E33;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604845
    iget-object v2, p4, LX/E33;->c:Landroid/widget/TextView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 604835
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604836
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604837
    invoke-interface {v0}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->aj_()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()F
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 604832
    check-cast p4, LX/E33;

    const/4 v1, 0x0

    .line 604833
    const/4 v0, 0x0

    invoke-virtual {p4, v0, v1, v1, v1}, LX/E33;->a(FLX/1aZ;Landroid/graphics/PointF;Landroid/view/View$OnClickListener;)V

    .line 604834
    return-void
.end method
