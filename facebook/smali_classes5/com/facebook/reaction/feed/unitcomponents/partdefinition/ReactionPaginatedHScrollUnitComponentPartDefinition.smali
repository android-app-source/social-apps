.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/3U7;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/E4S;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/E4S;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final c:LX/E3C;

.field public final d:LX/1Qx;

.field private final e:LX/2dq;

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final g:LX/1vo;

.field private final h:LX/Cfw;

.field private final i:Lcom/facebook/reaction/ReactionUtil;

.field private final j:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

.field public final k:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604650
    new-instance v0, LX/3aK;

    invoke-direct {v0}, LX/3aK;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    .line 604651
    new-instance v0, LX/3aL;

    invoke-direct {v0}, LX/3aL;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/E3C;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/1Qx;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604639
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604640
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->k:LX/03V;

    .line 604641
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->c:LX/E3C;

    .line 604642
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->j:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    .line 604643
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->e:LX/2dq;

    .line 604644
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 604645
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->d:LX/1Qx;

    .line 604646
    iput-object p7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->g:LX/1vo;

    .line 604647
    iput-object p8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->h:LX/Cfw;

    .line 604648
    iput-object p9, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->i:Lcom/facebook/reaction/ReactionUtil;

    .line 604649
    return-void
.end method

.method private a(LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;)LX/2eJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E2b;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/2eJ",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation

    .prologue
    .line 604622
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604623
    check-cast v0, LX/9uf;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v7

    .line 604624
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 604625
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604626
    check-cast v0, LX/9uf;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v3

    .line 604627
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    .line 604628
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    .line 604629
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 604630
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 604631
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 604632
    move-object v0, p3

    .line 604633
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2b;

    .line 604634
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 604635
    invoke-direct {v1, v3}, LX/E2b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/E2c;

    .line 604636
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    .line 604637
    iput v0, v6, LX/E2c;->c:I

    .line 604638
    new-instance v0, LX/E3s;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, LX/E3s;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2b;LX/E2c;LX/0us;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;
    .locals 13

    .prologue
    .line 604611
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    monitor-enter v1

    .line 604612
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604613
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604616
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/E3C;->a(LX/0QB;)LX/E3C;

    move-result-object v5

    check-cast v5, LX/E3C;

    invoke-static {v0}, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v7

    check-cast v7, LX/1Qx;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v10

    check-cast v10, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v11

    check-cast v11, LX/Cfw;

    invoke-static {v0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v12

    check-cast v12, Lcom/facebook/reaction/ReactionUtil;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;-><init>(LX/03V;LX/E3C;Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;LX/1Qx;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1vo;LX/Cfw;Lcom/facebook/reaction/ReactionUtil;)V

    .line 604617
    move-object v0, v3

    .line 604618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2c;LX/0us;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "LX/E2c;",
            "LX/0us;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604599
    iget v0, p3, LX/E2c;->c:I

    move v0, v0

    .line 604600
    iget v1, p3, LX/E2c;->d:I

    move v1, v1

    .line 604601
    sub-int/2addr v0, v1

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 604602
    invoke-interface {p4}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604603
    iget-boolean v0, p3, LX/E2c;->e:Z

    move v0, v0

    .line 604604
    if-nez v0, :cond_0

    .line 604605
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604606
    check-cast v0, LX/9uf;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 604607
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->i:Lcom/facebook/reaction/ReactionUtil;

    invoke-interface {p4}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/E3t;

    invoke-direct {v2, p0, p1, p2, p3}, LX/E3t;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2c;)V

    const/4 v3, 0x5

    check-cast p2, LX/2kp;

    invoke-interface {p2}, LX/2kp;->t()LX/2jY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;LX/0Ve;ILjava/lang/String;LX/2jY;)V

    .line 604608
    const/4 v0, 0x1

    .line 604609
    iput-boolean v0, p3, LX/E2c;->e:Z

    .line 604610
    :cond_0
    return-void
.end method

.method public static b(LX/9uc;)LX/1Cz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9uc;",
            ")",
            "LX/1Cz",
            "<",
            "LX/E4S;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604572
    invoke-interface {p0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 604573
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v1, :cond_1

    .line 604574
    :cond_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->b:LX/1Cz;

    .line 604575
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604598
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 604580
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/3U7;

    const/4 v6, 0x1

    .line 604581
    new-instance v3, LX/E2b;

    .line 604582
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 604583
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 604584
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/E2c;

    .line 604585
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->c:LX/E3C;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)I

    move-result v1

    move-object v0, p3

    .line 604586
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 604587
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->j:Lcom/facebook/reaction/feed/styling/ReactionPaginatedHScrollUnitStyledBackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, LX/2eF;->a:LX/1Ua;

    invoke-direct {v4, v5}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 604588
    invoke-static {p2}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v1

    .line 604589
    if-nez v1, :cond_0

    .line 604590
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 604591
    :cond_0
    move-object v1, v1

    .line 604592
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v6, :cond_1

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->e:LX/2dq;

    int-to-float v0, v0

    const/high16 v4, 0x41000000    # 8.0f

    add-float/2addr v0, v4

    sget-object v4, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v0, v4, v6}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    .line 604593
    :goto_0
    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    .line 604594
    iget v4, v2, LX/E2c;->d:I

    move v2, v4

    .line 604595
    invoke-direct {p0, v3, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a(LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;)LX/2eJ;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 604596
    const/4 v0, 0x0

    return-object v0

    .line 604597
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->e:LX/2dq;

    sget-object v1, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v0, v1}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 604576
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604577
    invoke-static {p1}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    .line 604578
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 604579
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
