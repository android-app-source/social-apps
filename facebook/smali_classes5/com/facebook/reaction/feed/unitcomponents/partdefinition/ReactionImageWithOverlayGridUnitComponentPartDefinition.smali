.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/TableLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:[I

.field private static d:LX/0Xm;


# instance fields
.field public final c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 604200
    const v0, 0x7f03115a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->a:LX/1Cz;

    .line 604201
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0d28f3

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0d28f4

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0d28f5

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0d28f6

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->b:[I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604164
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604165
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    .line 604166
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 604189
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;

    monitor-enter v1

    .line 604190
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604191
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604194
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;)V

    .line 604195
    move-object v0, p0

    .line 604196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604202
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 604177
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604178
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604179
    invoke-interface {v0}, LX/9uc;->aZ()LX/0Px;

    move-result-object v2

    .line 604180
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 604181
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->b:[I

    aget v3, v0, v1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;

    .line 604182
    new-instance v4, LX/E3n;

    .line 604183
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 604184
    iget-object p3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object p3, p3

    .line 604185
    invoke-direct {v4, v0, v5, p3}, LX/E3n;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitImageWithOverlayComponentFragementModel$ImagesWithOverlayModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 604186
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlaySingleItemUnitComponentPartDefinition;

    invoke-interface {p1, v3, v5, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 604188
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5d037eef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604170
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p4, Landroid/widget/TableLayout;

    .line 604171
    const v1, 0x7f0d0c45

    invoke-virtual {p4, v1}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableRow;

    .line 604172
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 604173
    invoke-interface {v2}, LX/9uc;->aZ()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 p0, 0x4

    if-ne v2, p0, :cond_0

    .line 604174
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 604175
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x289a0c6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 604176
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 604167
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604168
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604169
    invoke-interface {v0}, LX/9uc;->aZ()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/9uc;->aZ()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-interface {v0}, LX/9uc;->aZ()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
