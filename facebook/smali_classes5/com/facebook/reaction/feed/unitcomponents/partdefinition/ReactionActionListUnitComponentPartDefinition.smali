.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602888
    const v0, 0x7f03112d

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602923
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602924
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    .line 602925
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    .line 602926
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 602912
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;

    monitor-enter v1

    .line 602913
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602914
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602915
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602916
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602917
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;)V

    .line 602918
    move-object v0, p0

    .line 602919
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602920
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602921
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602922
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602927
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 602899
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 602900
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602901
    invoke-interface {v0}, LX/9uc;->l()LX/0Px;

    move-result-object v3

    .line 602902
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 602903
    const v1, 0x7f0d28af

    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    new-instance v6, LX/E67;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-direct {v6, v0, p2}, LX/E67;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-interface {p1, v1, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602904
    const v5, 0x7f0d28b0

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    if-le v4, v7, :cond_0

    new-instance v1, LX/E67;

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-direct {v1, v0, p2}, LX/E67;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v0, v1

    :goto_0
    invoke-interface {p1, v5, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602905
    const v5, 0x7f0d28b1

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    if-le v4, v8, :cond_1

    new-instance v1, LX/E67;

    invoke-virtual {v3, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-direct {v1, v0, p2}, LX/E67;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v0, v1

    :goto_1
    invoke-interface {p1, v5, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602906
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListBackgroundPartDefinition;

    .line 602907
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 602908
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 602909
    return-object v2

    :cond_0
    move-object v0, v2

    .line 602910
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 602911
    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 602890
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 602891
    invoke-interface {v0}, LX/9uc;->l()LX/0Px;

    move-result-object v3

    .line 602892
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 602893
    :goto_0
    return v0

    .line 602894
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 602895
    iget-object v5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;

    new-instance v6, LX/E67;

    invoke-direct {v6, v0, p1}, LX/E67;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-virtual {v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleActionOrHiddenPartDefinition;->a(LX/E67;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 602896
    goto :goto_0

    .line 602897
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 602898
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602889
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method
