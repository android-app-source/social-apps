.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603074
    const v0, 0x7f031128

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603069
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603070
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603071
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    .line 603072
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 603073
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 603058
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    monitor-enter v1

    .line 603059
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603060
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603061
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603062
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603063
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V

    .line 603064
    move-object v0, p0

    .line 603065
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603066
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603067
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603057
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 603049
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v2, 0x0

    .line 603050
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionIconMessageWithMenuPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603051
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603052
    new-instance v1, LX/E3J;

    invoke-direct {v1, p0, p3, p2}, LX/E3J;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v1, v1

    .line 603053
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603054
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603055
    return-object v2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 603056
    const/4 v0, 0x1

    return v0
.end method
