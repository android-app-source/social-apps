.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

.field private final d:LX/0W9;

.field private final e:Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

.field private final f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603512
    const v0, 0x7f03117a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;LX/0W9;Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603513
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603514
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 603515
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    .line 603516
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->d:LX/0W9;

    .line 603517
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    .line 603518
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    .line 603519
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;
    .locals 9

    .prologue
    .line 603520
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;

    monitor-enter v1

    .line 603521
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603522
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603523
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603524
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603525
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;LX/0W9;Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;)V

    .line 603526
    move-object v0, v3

    .line 603527
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603531
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 603532
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v5, 0x0

    .line 603533
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603534
    const v1, 0x7f0d0578

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionTextPartDefinition;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->d:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603535
    const v1, 0x7f0d2928

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionUriIconPartDefinition;

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603536
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 603537
    if-eqz v1, :cond_0

    .line 603538
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VERTICAL_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v2, :cond_1

    .line 603539
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionOpenBottomActionSheetPartDefinition;

    new-instance v2, LX/E66;

    .line 603540
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 603541
    invoke-interface {v3}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v3

    .line 603542
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 603543
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 603544
    invoke-direct {v2, v1, v3, v4, v6}, LX/E66;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603545
    :cond_0
    :goto_0
    return-object v5

    .line 603546
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    .line 603547
    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    if-nez v2, :cond_2

    move-object v2, v5

    .line 603548
    :goto_1
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 603549
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 603550
    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-interface {v2}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603551
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603552
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603553
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
