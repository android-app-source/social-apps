.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604868
    new-instance v0, LX/3aP;

    invoke-direct {v0}, LX/3aP;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604869
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604870
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;
    .locals 3

    .prologue
    .line 604871
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;

    monitor-enter v1

    .line 604872
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604873
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604874
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604875
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 604876
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;-><init>()V

    .line 604877
    move-object v0, v0

    .line 604878
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604879
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604880
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604882
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 604883
    const/4 v0, 0x1

    return v0
.end method
