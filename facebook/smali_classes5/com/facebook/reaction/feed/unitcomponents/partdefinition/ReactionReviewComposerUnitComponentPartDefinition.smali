.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605034
    const v0, 0x7f03113b

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605035
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605036
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    .line 605037
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 605038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 605039
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;

    monitor-enter v1

    .line 605040
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605041
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605044
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;)V

    .line 605045
    move-object v0, p0

    .line 605046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605050
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 605051
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 605052
    const v0, 0x7f0d28c7

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 605053
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 605054
    invoke-interface {v2}, LX/9uc;->dc()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605055
    const v0, 0x7f0d28c8

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 605056
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 605057
    invoke-interface {v2}, LX/9uc;->cX()LX/174;

    move-result-object v2

    .line 605058
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    .line 605059
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605060
    const v0, 0x7f0d28ca

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 605061
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 605062
    invoke-interface {v2}, LX/9uc;->aA()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605063
    const v0, 0x7f0d28c9

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionRatingBarPartDefinition;

    new-instance v2, LX/E46;

    .line 605064
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 605065
    invoke-interface {v3}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 605066
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 605067
    invoke-interface {v4}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 605068
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 605069
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 605070
    invoke-direct {v2, v3, v4, v5, v6}, LX/E46;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605071
    const/4 v0, 0x0

    return-object v0

    :cond_1
    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 605072
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 605073
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605074
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v0

    .line 605075
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 605076
    invoke-interface {v1}, LX/9uc;->aA()LX/174;

    move-result-object v1

    .line 605077
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 605078
    invoke-interface {v2}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    .line 605079
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
