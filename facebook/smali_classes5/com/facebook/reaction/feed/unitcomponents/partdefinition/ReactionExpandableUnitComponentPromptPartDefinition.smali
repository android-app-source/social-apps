.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3S;",
        "TE;",
        "Landroid/widget/RelativeLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/23P;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:LX/1vo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603452
    const v0, 0x7f031132

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1vo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603445
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603446
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->b:LX/23P;

    .line 603447
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603448
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 603449
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 603450
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->f:LX/1vo;

    .line 603451
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;
    .locals 9

    .prologue
    .line 603434
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    monitor-enter v1

    .line 603435
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603436
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603437
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603438
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603439
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v8

    check-cast v8, LX/1vo;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;-><init>(LX/23P;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1vo;)V

    .line 603440
    move-object v0, v3

    .line 603441
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603442
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603443
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603444
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603453
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 603416
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v1, 0x0

    .line 603417
    new-instance v2, LX/E3R;

    invoke-direct {v2, p0, p3, p2}, LX/E3R;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;LX/1Pn;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 603418
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->b:LX/23P;

    .line 603419
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603420
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->dz()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 603421
    const v3, 0x7f0d28b7

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603422
    const v2, 0x7f0d28b7

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603423
    const v2, 0x7f0d28b7

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603424
    invoke-static {p2}, LX/Cfu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v2

    .line 603425
    iget-object v0, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603426
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 603427
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->f:LX/1vo;

    invoke-virtual {v3, v0}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 603428
    instance-of v3, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v3, :cond_0

    .line 603429
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 603430
    new-instance v3, LX/5eI;

    invoke-direct {v3, v0, p3}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    .line 603431
    invoke-virtual {v3, v2}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 603432
    new-instance v1, LX/E3S;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a()LX/1Cz;

    move-result-object v0

    invoke-direct {v1, v3, v0}, LX/E3S;-><init>(LX/5eI;LX/1Cz;)V

    move-object v0, v1

    .line 603433
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x72df5a49

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603410
    check-cast p2, LX/E3S;

    check-cast p3, LX/1Pn;

    check-cast p4, Landroid/widget/RelativeLayout;

    .line 603411
    if-nez p2, :cond_0

    .line 603412
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x30843437

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 603413
    :cond_0
    iget-object v1, p2, LX/E3S;->b:LX/1Cz;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 603414
    iget-object v2, p2, LX/E3S;->a:LX/5eI;

    invoke-virtual {v2, v1}, LX/5eI;->a(Landroid/view/View;)V

    .line 603415
    const/4 v2, 0x0

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 p0, -0x1

    const/4 p1, -0x2

    invoke-direct {v4, p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p4, v1, v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 603404
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 603405
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603406
    instance-of v2, v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    if-nez v2, :cond_0

    move v0, v1

    .line 603407
    :goto_0
    return v0

    .line 603408
    :cond_0
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 603409
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->dz()LX/174;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->dz()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, LX/Cfu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 603400
    check-cast p2, LX/E3S;

    check-cast p4, Landroid/widget/RelativeLayout;

    .line 603401
    if-nez p2, :cond_0

    .line 603402
    :goto_0
    return-void

    .line 603403
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    goto :goto_0
.end method
