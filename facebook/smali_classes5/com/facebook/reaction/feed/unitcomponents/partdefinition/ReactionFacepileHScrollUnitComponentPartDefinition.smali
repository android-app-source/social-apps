.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3W;",
        "TE;",
        "Landroid/support/v7/widget/RecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 603510
    const v0, 0x7f031114

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    .line 603511
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;

    const-string v1, "reaction_dialog"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/E1i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/E1i;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603506
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603507
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->c:LX/0Or;

    .line 603508
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->d:LX/E1i;

    .line 603509
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 603495
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;

    monitor-enter v1

    .line 603496
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603497
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603498
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603499
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603500
    new-instance v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;

    const/16 v3, 0x139b

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    invoke-direct {v4, p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;-><init>(LX/0Or;LX/E1i;)V

    .line 603501
    move-object v0, v4

    .line 603502
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603503
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603504
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603505
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 603494
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 603474
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v2, 0x0

    .line 603475
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 603476
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 603477
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603478
    invoke-interface {v0}, LX/9uc;->cg()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    .line 603479
    :goto_0
    if-ge v1, v6, :cond_1

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;

    .line 603480
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->e()LX/5sY;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->e()LX/5sY;

    move-result-object v7

    invoke-interface {v7}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 603481
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 603482
    new-instance p1, LX/E3T;

    invoke-direct {p1, p0, v7, p3, p2}, LX/E3T;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v7, p1

    .line 603483
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603484
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->e()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603485
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 603486
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 603487
    invoke-virtual {v0, v2}, LX/1P1;->b(I)V

    .line 603488
    new-instance v2, LX/E3U;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1, v3, v4}, LX/E3U;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 603489
    new-instance v1, LX/E2b;

    .line 603490
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 603491
    invoke-direct {v1, v3}, LX/E2b;-><init>(Ljava/lang/String;)V

    .line 603492
    check-cast p3, LX/1Pr;

    invoke-interface {p3, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/E2c;

    .line 603493
    new-instance v3, LX/E3W;

    invoke-direct {v3, v2, v0, v1}, LX/E3W;-><init>(LX/E3U;LX/1P1;LX/E2c;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x13032ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603468
    check-cast p2, LX/E3W;

    check-cast p3, LX/2km;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 603469
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0a0048

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 603470
    iget-object v1, p2, LX/E3W;->b:LX/1P1;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 603471
    iget-object v1, p2, LX/E3W;->a:LX/E3U;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 603472
    iget-object v1, p2, LX/E3W;->b:LX/1P1;

    iget-object v2, p2, LX/E3W;->c:LX/E2c;

    invoke-static {v1, v2}, LX/E2d;->a(LX/1P1;LX/E2c;)V

    .line 603473
    const/16 v1, 0x1f

    const v2, -0x785d3c4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 603454
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 603455
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603456
    invoke-interface {v0}, LX/9uc;->cg()LX/0Px;

    move-result-object v3

    .line 603457
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 603458
    :goto_0
    return v0

    .line 603459
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;

    .line 603460
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->e()LX/5sY;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->e()LX/5sY;

    move-result-object v5

    invoke-interface {v5}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ProfilesModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 603461
    const/4 v0, 0x1

    goto :goto_0

    .line 603462
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 603463
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 603464
    check-cast p2, LX/E3W;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 603465
    iget-object v0, p2, LX/E3W;->b:LX/1P1;

    iget-object v1, p2, LX/E3W;->c:LX/E2c;

    invoke-static {v0, v1, p4}, LX/E2d;->a(LX/1P1;LX/E2c;Landroid/support/v7/widget/RecyclerView;)V

    .line 603466
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 603467
    return-void
.end method
