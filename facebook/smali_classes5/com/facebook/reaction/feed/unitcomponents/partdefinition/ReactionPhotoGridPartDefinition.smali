.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3z;",
        "LX/1PW;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final c:LX/26H;

.field public final d:LX/Bre;

.field public final e:LX/1Ad;

.field public final f:LX/1qa;

.field public final g:LX/23R;

.field private final h:LX/9hF;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 604768
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;

    const-string v1, "reaction_photo_grid_view"

    const-string v2, "reaction_photos"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 604769
    new-instance v0, LX/3aM;

    invoke-direct {v0}, LX/3aM;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;LX/9hF;LX/Bre;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604760
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604761
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->c:LX/26H;

    .line 604762
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->d:LX/Bre;

    .line 604763
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->f:LX/1qa;

    .line 604764
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->e:LX/1Ad;

    .line 604765
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->g:LX/23R;

    .line 604766
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->h:LX/9hF;

    .line 604767
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;
    .locals 10

    .prologue
    .line 604749
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;

    monitor-enter v1

    .line 604750
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604751
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604754
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;

    const-class v4, LX/26H;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/26H;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v6

    check-cast v6, LX/1qa;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v7

    check-cast v7, LX/23R;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v8

    check-cast v8, LX/9hF;

    const-class v9, LX/Bre;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Bre;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;-><init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;LX/9hF;LX/Bre;)V

    .line 604755
    move-object v0, v3

    .line 604756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 604741
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 604742
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 604743
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604744
    invoke-interface {v0}, LX/9uc;->bT()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 604745
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 604746
    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 604747
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 604748
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604740
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 604716
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604717
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->d:LX/Bre;

    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 604718
    new-instance v3, LX/Brd;

    const-class v2, LX/Brb;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Brb;

    invoke-direct {v3, v2, v1}, LX/Brd;-><init>(LX/Brb;LX/0Px;)V

    .line 604719
    move-object v2, v3

    .line 604720
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->c:LX/26H;

    invoke-virtual {v0, v2}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v3

    .line 604721
    invoke-virtual {v3}, LX/26O;->a()LX/0Px;

    move-result-object v4

    .line 604722
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 604723
    new-array v6, v5, [LX/1aZ;

    .line 604724
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->e:LX/1Ad;

    sget-object v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v7

    .line 604725
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 604726
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    .line 604727
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    .line 604728
    iget-object v9, v0, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v9

    .line 604729
    iget-object v9, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v9

    .line 604730
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 604731
    invoke-virtual {v2, v0}, LX/Brd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/26P;

    move-result-object v9

    .line 604732
    iget-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->f:LX/1qa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p3

    invoke-virtual {p1, p3, v9}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 604733
    invoke-virtual {v7}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v9

    aput-object v9, v6, v1

    .line 604734
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 604735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 604736
    :cond_0
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 604737
    new-instance v1, LX/E3y;

    invoke-direct {v1, p0, v4, v0}, LX/E3y;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;LX/0Px;LX/0Px;)V

    .line 604738
    new-instance v0, LX/E3z;

    invoke-direct {v0, v1, v3, v6}, LX/E3z;-><init>(LX/Aj7;LX/26O;[LX/1aZ;)V

    move-object v0, v0

    .line 604739
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x74260f73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604704
    check-cast p2, LX/E3z;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 604705
    iget-object v1, p2, LX/E3z;->b:LX/26O;

    iget-object v2, p2, LX/E3z;->c:[LX/1aZ;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 604706
    iget-object v1, p2, LX/E3z;->a:LX/Aj7;

    .line 604707
    iput-object v1, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 604708
    const/16 v1, 0x1f

    const v2, -0x7e7a4290

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 604714
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604715
    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Brd;->a(LX/0Px;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 604709
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 604710
    invoke-virtual {p4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 604711
    const/4 v0, 0x0

    .line 604712
    iput-object v0, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 604713
    return-void
.end method
