.class public abstract Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3Q;",
        "TE;",
        "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603321
    new-instance v0, LX/3aE;

    invoke-direct {v0}, LX/3aE;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0

    .prologue
    .line 603371
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603372
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 603373
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603370
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 603354
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v7, 0x0

    .line 603355
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 603356
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v1, LX/E1o;

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 603357
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 603358
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 603359
    invoke-direct {v1, v3, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603360
    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    .line 603361
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v4

    .line 603362
    invoke-interface {v2}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v5

    .line 603363
    invoke-interface {v2}, LX/9uc;->ae()LX/5sY;

    move-result-object v6

    .line 603364
    invoke-interface {v2}, LX/9uc;->hk_()LX/174;

    move-result-object v8

    move-object v0, p3

    .line 603365
    check-cast v0, LX/2kn;

    invoke-interface {v0}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 603366
    check-cast p3, LX/2kn;

    invoke-interface {p3}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    .line 603367
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 603368
    iput-object v1, v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    .line 603369
    :cond_0
    new-instance v0, LX/E3Q;

    invoke-interface {v2}, LX/9uc;->bE()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, LX/9uc;->ah()Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v4, :cond_3

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    :goto_1
    if-eqz v6, :cond_4

    invoke-interface {v6}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v6

    :goto_2
    if-eqz v8, :cond_1

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v7

    :cond_1
    invoke-direct/range {v0 .. v7}, LX/E3Q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_2
    move-object v3, v7

    goto :goto_0

    :cond_3
    move-object v4, v7

    goto :goto_1

    :cond_4
    move-object v6, v7

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x8afbbcf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603335
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/E3Q;

    check-cast p3, LX/2km;

    check-cast p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;

    .line 603336
    move-object v1, p3

    check-cast v1, LX/3U9;

    invoke-interface {v1}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    .line 603337
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 603338
    iget-object v4, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 603339
    iput-object v1, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->y:LX/2ja;

    .line 603340
    iput-object v2, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->z:Ljava/lang/String;

    .line 603341
    iput-object v4, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->A:Ljava/lang/String;

    .line 603342
    iget-object v1, p2, LX/E3Q;->b:Ljava/lang/String;

    iget-object v2, p2, LX/E3Q;->a:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603343
    iget-object v1, p2, LX/E3Q;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventTitle(Ljava/lang/String;)V

    .line 603344
    iget-object v1, p2, LX/E3Q;->d:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventInfo(Ljava/lang/String;)V

    .line 603345
    iget-object v1, p2, LX/E3Q;->e:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 603346
    iget-object v2, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->s:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 603347
    iput-object v1, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 603348
    iget-object v2, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->s:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    iget-object v4, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->j:LX/38v;

    invoke-virtual {v4, p4}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->p()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->r()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->u()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object p1

    invoke-virtual {v4, v5, v6, p1}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 603349
    invoke-virtual {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->b()Z

    move-result v1

    iget-object v2, p2, LX/E3Q;->f:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->a(ZLjava/lang/String;)V

    .line 603350
    iget-object v1, p2, LX/E3Q;->g:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventSocialContext(Ljava/lang/String;)V

    .line 603351
    check-cast p3, LX/2kn;

    invoke-interface {p3}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v1

    .line 603352
    iput-object v1, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 603353
    const/16 v1, 0x1f

    const v2, -0x729882ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603332
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603333
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603334
    invoke-interface {v0}, LX/9uc;->ah()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bE()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 603322
    check-cast p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;

    .line 603323
    const/4 p1, 0x0

    .line 603324
    invoke-virtual {p4, p1, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603325
    invoke-virtual {p4, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventTitle(Ljava/lang/String;)V

    .line 603326
    invoke-virtual {p4, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventInfo(Ljava/lang/String;)V

    .line 603327
    iget-object p0, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->s:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a()V

    .line 603328
    const/4 p0, 0x0

    invoke-virtual {p4, p0, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->a(ZLjava/lang/String;)V

    .line 603329
    invoke-virtual {p4, p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->setEventSocialContext(Ljava/lang/String;)V

    .line 603330
    iput-object p1, p4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ui/ReactionEventRowComponentView;->x:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 603331
    return-void
.end method

.method public abstract b()Z
.end method
