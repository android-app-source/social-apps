.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603115
    const v0, 0x7f031123

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603116
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603117
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603118
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    .line 603119
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 603120
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 603121
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 603122
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    monitor-enter v1

    .line 603123
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603124
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603125
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603126
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603127
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V

    .line 603128
    move-object v0, p0

    .line 603129
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603130
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603131
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603133
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 603134
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    const/4 v4, 0x0

    .line 603135
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603136
    const v1, 0x7f0d28a2

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603137
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/common/ImageBlockLayoutIconPartDefinition;

    invoke-interface {v0}, LX/9uc;->aS()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603138
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603139
    new-instance v1, LX/E3K;

    invoke-direct {v1, p0, p3, p2}, LX/E3K;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move-object v1, v1

    .line 603140
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603141
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603142
    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4ed69974

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603143
    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 603144
    const v1, 0x7f0d28a7

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 603145
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 603146
    const/16 v1, 0x1f

    const v2, -0x4d4e3114

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 603147
    const/4 v0, 0x1

    return v0
.end method
