.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/E1f;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;

.field private final d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

.field private final e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final f:Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604294
    const v0, 0x7f03115c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/E1f;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604295
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604296
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->b:LX/E1f;

    .line 604297
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;

    .line 604298
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    .line 604299
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 604300
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    .line 604301
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 604302
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;
    .locals 10

    .prologue
    .line 604303
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;

    monitor-enter v1

    .line 604304
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604305
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604306
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604307
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604308
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v4

    check-cast v4, LX/E1f;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;-><init>(LX/E1f;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 604309
    move-object v0, v3

    .line 604310
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604311
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604312
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604313
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/2km;Landroid/text/SpannableStringBuilder;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Landroid/text/SpannableStringBuilder;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604314
    move-object/from16 v1, p1

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 604315
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v5

    .line 604316
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v6

    .line 604317
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v3

    .line 604318
    invoke-interface {v3}, LX/9uc;->bV()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 604319
    invoke-interface {v3}, LX/9uc;->F()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 604320
    invoke-interface {v3}, LX/9uc;->F()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;

    .line 604321
    invoke-interface {v3}, LX/9uc;->F()LX/0Px;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;

    .line 604322
    const v1, 0x7f082232

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "{place_category}"

    aput-object v10, v3, v9

    const/4 v9, 0x1

    const-string v10, "{neighborhood}"

    aput-object v10, v3, v9

    const/4 v9, 0x2

    const-string v10, "{city}"

    aput-object v10, v3, v9

    invoke-virtual {v8, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 604323
    const/4 v1, 0x3

    new-array v10, v1, [LX/E6G;

    const/4 v1, 0x0

    new-instance v3, LX/E6G;

    const-string v11, "{place_category}"

    const/4 v12, 0x0

    invoke-direct {v3, v11, v4, v12}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v3, v10, v1

    const/4 v11, 0x1

    new-instance v12, LX/E6G;

    const-string v13, "{neighborhood}"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-direct {v12, v13, v14, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, LX/E6G;

    const-string v13, "{city}"

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-direct {v12, v13, v14, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v12, v10, v11

    invoke-static {v9, v8, v10}, LX/E6H;->a(Ljava/lang/String;Landroid/content/res/Resources;[LX/E6G;)Landroid/text/SpannableString;

    move-result-object v1

    .line 604324
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 604325
    :goto_2
    return-void

    .line 604326
    :cond_0
    new-instance v1, LX/E69;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->b:LX/E1f;

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v1, LX/E69;

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->b:LX/E1f;

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 604327
    :cond_2
    invoke-interface {v3}, LX/9uc;->F()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;

    .line 604328
    const v1, 0x7f082231

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v9, "{place_category}"

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const-string v9, "{city}"

    aput-object v9, v3, v7

    invoke-virtual {v8, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 604329
    const/4 v1, 0x2

    new-array v9, v1, [LX/E6G;

    const/4 v1, 0x0

    new-instance v3, LX/E6G;

    const-string v10, "{place_category}"

    const/4 v11, 0x0

    invoke-direct {v3, v10, v4, v11}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v3, v9, v1

    const/4 v10, 0x1

    new-instance v11, LX/E6G;

    const-string v12, "{city}"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_3
    invoke-direct {v11, v12, v13, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, LX/E6H;->a(Ljava/lang/String;Landroid/content/res/Resources;[LX/E6G;)Landroid/text/SpannableString;

    move-result-object v1

    .line 604330
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    .line 604331
    :cond_3
    new-instance v1, LX/E69;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->b:LX/E1f;

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604332
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 604333
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 604334
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604335
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 604336
    invoke-interface {v0}, LX/9uc;->F()LX/0Px;

    move-result-object v2

    const/4 v4, 0x0

    .line 604337
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 604338
    :goto_0
    move v2, v3

    .line 604339
    if-eqz v2, :cond_1

    .line 604340
    invoke-direct {p0, p3, v1, p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->a(LX/2km;Landroid/text/SpannableStringBuilder;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 604341
    :goto_1
    const v2, 0x7f0d28fb

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604342
    const v2, 0x7f0d28fc

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604343
    const v1, 0x7f0d28fc

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/subpart/TextMovementMethodPartDefinition;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604344
    const v1, 0x7f0d28fa

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionMapPartDefinition;

    new-instance v3, LX/E63;

    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v4

    invoke-interface {v0}, LX/9uc;->bw()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    move-result-object v5

    const/16 v6, 0x10

    invoke-direct {v3, v4, v5, v6}, LX/E63;-><init>(LX/1k1;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;I)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604345
    const v1, 0x7f0d28fa

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionExtendedWidthPartDefinition;

    const v3, 0x3fcf5c29    # 1.62f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604346
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 604347
    const v1, 0x7f0d28fd

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v3, LX/E1o;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 604348
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 604349
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 604350
    invoke-direct {v3, v0, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604351
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 604352
    :cond_1
    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 604353
    :cond_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_5

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;

    .line 604354
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->b()LX/174;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$BreadcrumbsModel;->b()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v3, v4

    .line 604355
    goto/16 :goto_0

    .line 604356
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 604357
    :cond_5
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 604358
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604359
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604360
    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bV()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
