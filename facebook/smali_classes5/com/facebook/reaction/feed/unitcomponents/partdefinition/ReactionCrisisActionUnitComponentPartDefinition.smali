.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3N;",
        "TE;",
        "LX/E4L;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/3U3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603238
    new-instance v0, LX/3aB;

    invoke-direct {v0}, LX/3aB;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/3U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603235
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603236
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->b:LX/3U3;

    .line 603237
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)LX/E3N;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/E3N;"
        }
    .end annotation

    .prologue
    .line 603229
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 603230
    move-object v0, p2

    .line 603231
    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2T;

    .line 603232
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 603233
    invoke-interface {v2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/E2T;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2S;

    .line 603234
    new-instance v10, LX/E3N;

    invoke-interface {v3}, LX/9uc;->by()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;->a()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3}, LX/9uc;->bx()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;->a()Ljava/lang/String;

    move-result-object v12

    new-instance v0, LX/E3L;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/E3L;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    new-instance v4, LX/E3M;

    move-object v5, p0

    move-object v6, v2

    move-object v7, v3

    move-object v8, p2

    move-object v9, p1

    invoke-direct/range {v4 .. v9}, LX/E3M;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-direct {v10, v11, v12, v0, v4}, LX/E3N;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v10
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 603218
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    monitor-enter v1

    .line 603219
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603220
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603221
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603222
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603223
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    invoke-static {v0}, LX/3U3;->a(LX/0QB;)LX/3U3;

    move-result-object v3

    check-cast v3, LX/3U3;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;-><init>(LX/3U3;)V

    .line 603224
    move-object v0, p0

    .line 603225
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603226
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603227
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603228
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 603239
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603240
    invoke-interface {v0}, LX/9uc;->by()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->by()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bx()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bx()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->af()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->af()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603217
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 603216
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)LX/E3N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5d54c4d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603204
    check-cast p2, LX/E3N;

    check-cast p4, LX/E4L;

    .line 603205
    iget-object v1, p2, LX/E3N;->a:Ljava/lang/String;

    iget-object v2, p2, LX/E3N;->b:Ljava/lang/String;

    .line 603206
    iget-object p0, p4, LX/E4L;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 603207
    iget-object p0, p4, LX/E4L;->b:Lcom/facebook/resources/ui/FbButton;

    iget-object p1, p4, LX/E4L;->a:LX/23P;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 603208
    iget-object p0, p4, LX/E4L;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 603209
    iget-object p0, p4, LX/E4L;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object p1, p4, LX/E4L;->a:LX/23P;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 603210
    iget-object v1, p2, LX/E3N;->c:Landroid/view/View$OnClickListener;

    iget-object v2, p2, LX/E3N;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/E4L;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 603211
    const/16 v1, 0x1f

    const v2, -0x51d755c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 603215
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 603212
    check-cast p4, LX/E4L;

    const/4 v0, 0x0

    .line 603213
    invoke-virtual {p4, v0, v0}, LX/E4L;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 603214
    return-void
.end method
