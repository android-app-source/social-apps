.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/0yc;

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 605717
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;

    const-string v1, "reaction_videos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 605718
    const v0, 0x7f031142

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0yc;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605719
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605720
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 605721
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->d:LX/0yc;

    .line 605722
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 605723
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 605724
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;

    monitor-enter v1

    .line 605725
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605726
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605727
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605728
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605729
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0yc;Lcom/facebook/multirow/parts/FbDraweePartDefinition;)V

    .line 605730
    move-object v0, p0

    .line 605731
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605732
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605733
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 605716
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 605702
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 605703
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605704
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/E4C;

    invoke-direct {v2, p0, v0, p3, p2}, LX/E4C;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;LX/9uc;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605705
    const v1, 0x7f0d288e

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-interface {v0}, LX/9uc;->dn()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v0

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 605706
    iput-object v3, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 605707
    move-object v0, v0

    .line 605708
    const v3, 0x3fe38e39

    .line 605709
    iput v3, v0, LX/2f8;->b:F

    .line 605710
    move-object v0, v0

    .line 605711
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08223d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 605712
    iput-object v3, v0, LX/2f8;->d:Ljava/lang/CharSequence;

    .line 605713
    move-object v0, v0

    .line 605714
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605715
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x50ab5074

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605693
    check-cast p4, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 605694
    const v1, 0x7f0d288f

    invoke-virtual {p4, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 605695
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->d:LX/0yc;

    invoke-virtual {v2}, LX/0yc;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 605696
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 605697
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x3142f8c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 605698
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 605699
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 605700
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605701
    invoke-interface {v0}, LX/9uc;->dn()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dn()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dn()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
