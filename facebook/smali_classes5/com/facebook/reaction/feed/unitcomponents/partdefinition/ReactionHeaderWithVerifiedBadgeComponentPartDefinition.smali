.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

.field private final d:Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field public final f:LX/1Uf;

.field private final g:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 603738
    const v0, 0x7f031154

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->a:LX/1Cz;

    .line 603739
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    const-string v1, "social_good"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603740
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603741
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->c:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 603742
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->d:Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    .line 603743
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 603744
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->g:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 603745
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->f:LX/1Uf;

    .line 603746
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;
    .locals 9

    .prologue
    .line 603747
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    monitor-enter v1

    .line 603748
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603749
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603750
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603751
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603752
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v8

    check-cast v8, LX/1Uf;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;LX/1Uf;)V

    .line 603753
    move-object v0, v3

    .line 603754
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603755
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603756
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603758
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 603759
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 603760
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603761
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 603762
    const v2, 0x7f0d28eb

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->c:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v4, LX/E3j;

    invoke-direct {v4, p0, p2, v1}, LX/E3j;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/content/Context;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603763
    const v1, 0x7f0d28ec

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->d:Lcom/facebook/reaction/feed/rows/subparts/InactiveTextWithEntitiesPartDefinition;

    invoke-interface {v0}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603764
    const v1, 0x7f0d28ea

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-interface {v0}, LX/9uc;->aN()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 603765
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 603766
    move-object v3, v3

    .line 603767
    const/4 v4, 0x1

    .line 603768
    iput-boolean v4, v3, LX/2f8;->g:Z

    .line 603769
    move-object v3, v3

    .line 603770
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603771
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 603772
    const v1, 0x7f0d28ea

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->g:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v3, LX/E1o;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 603773
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 603774
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 603775
    invoke-direct {v3, v0, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603776
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603777
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603778
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603779
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aN()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aN()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
