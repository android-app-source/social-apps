.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3Z;",
        "TE;",
        "Landroid/support/v7/widget/RecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1vo;

.field private final f:LX/Cfw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603780
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->b:Ljava/lang/String;

    .line 603781
    const v0, 0x7f031133

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;LX/1vo;LX/Cfw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/1vo;",
            "LX/Cfw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603837
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603838
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->c:LX/0Ot;

    .line 603839
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->d:LX/0Or;

    .line 603840
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->e:LX/1vo;

    .line 603841
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->f:LX/Cfw;

    .line 603842
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;
    .locals 7

    .prologue
    .line 603826
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;

    monitor-enter v1

    .line 603827
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603828
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603829
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603830
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603831
    new-instance v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x139b

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v3

    check-cast v3, LX/1vo;

    invoke-static {v0}, LX/Cfw;->b(LX/0QB;)LX/Cfw;

    move-result-object v4

    check-cast v4, LX/Cfw;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;-><init>(LX/0Ot;LX/0Or;LX/1vo;LX/Cfw;)V

    .line 603832
    move-object v0, v5

    .line 603833
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603834
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603835
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 603825
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 603794
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v2, 0x0

    .line 603795
    invoke-static {p2}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 603796
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 603797
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 603798
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_0
    if-ge v1, v6, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 603799
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v7

    .line 603800
    new-instance v8, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603801
    iget-object v9, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 603802
    iget-object v10, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v10, v10

    .line 603803
    invoke-direct {v8, v0, v9, v10}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 603804
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->e:LX/1vo;

    invoke-virtual {v0, v7}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 603805
    instance-of v7, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v7, :cond_3

    .line 603806
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 603807
    new-instance v7, LX/E4Y;

    new-instance v9, LX/5eI;

    invoke-direct {v9, v0, p3}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a()LX/1Cz;

    move-result-object v10

    invoke-direct {v7, v9, v10}, LX/E4Y;-><init>(LX/5eI;LX/1Cz;)V

    .line 603808
    :goto_1
    move-object v0, v7

    .line 603809
    if-eqz v0, :cond_1

    .line 603810
    iget-object v7, v0, LX/E4Y;->a:LX/5eI;

    invoke-virtual {v7, v8}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 603811
    iget-object v7, v0, LX/E4Y;->b:LX/1Cz;

    .line 603812
    invoke-virtual {v4, v7}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 603813
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603814
    :cond_0
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603815
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 603816
    :cond_2
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 603817
    invoke-virtual {v0, v2}, LX/1P1;->b(I)V

    .line 603818
    new-instance v1, LX/E2b;

    .line 603819
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 603820
    invoke-direct {v1, v2}, LX/E2b;-><init>(Ljava/lang/String;)V

    .line 603821
    check-cast p3, LX/1Pr;

    invoke-interface {p3, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/E2c;

    .line 603822
    new-instance v2, LX/E3Z;

    new-instance v3, LX/E4X;

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->c:LX/0Ot;

    invoke-direct {v3, v6, v5, v4}, LX/E4X;-><init>(LX/0Ot;Ljava/util/List;Ljava/util/Map;)V

    invoke-direct {v2, v3, v0, v1}, LX/E3Z;-><init>(LX/E4X;LX/1P1;LX/E2c;)V

    return-object v2

    .line 603823
    :cond_3
    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/03V;

    sget-object v9, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->b:Ljava/lang/String;

    const-string v10, "Tried to render GroupPartDefinition inside this part definition, only SinglePartDefinitions currently supported"

    invoke-virtual {v7, v9, v10}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603824
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5816aee5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603788
    check-cast p2, LX/E3Z;

    check-cast p3, LX/2km;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 603789
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0a0048

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 603790
    iget-object v1, p2, LX/E3Z;->b:LX/1P1;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 603791
    iget-object v1, p2, LX/E3Z;->a:LX/E4X;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 603792
    iget-object v1, p2, LX/E3Z;->b:LX/1P1;

    iget-object v2, p2, LX/E3Z;->c:LX/E2c;

    invoke-static {v1, v2}, LX/E2d;->a(LX/1P1;LX/E2c;)V

    .line 603793
    const/16 v1, 0x1f

    const v2, 0x23f72066

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603786
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603787
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->f:LX/Cfw;

    invoke-virtual {v1, p1}, LX/Cfw;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 603782
    check-cast p2, LX/E3Z;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 603783
    iget-object v0, p2, LX/E3Z;->b:LX/1P1;

    iget-object v1, p2, LX/E3Z;->c:LX/E2c;

    invoke-static {v0, v1, p4}, LX/E2d;->a(LX/1P1;LX/E2c;Landroid/support/v7/widget/RecyclerView;)V

    .line 603784
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 603785
    return-void
.end method
