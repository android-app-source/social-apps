.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E44;",
        "LX/1Pn;",
        "Landroid/widget/ProgressBar;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field public static final b:LX/1Cz;

.field public static final c:I

.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605031
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->a:Ljava/lang/Class;

    .line 605032
    const v0, 0x7f03113a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->b:LX/1Cz;

    .line 605033
    const v0, 0x7f0a00d1

    sput v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->c:I

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605028
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605029
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->d:LX/03V;

    .line 605030
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 605017
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;

    monitor-enter v1

    .line 605018
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605019
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605020
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605021
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605022
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;-><init>(LX/03V;)V

    .line 605023
    move-object v0, p0

    .line 605024
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605025
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605026
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604985
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 604995
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x3

    .line 604996
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 604997
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604998
    invoke-interface {v0}, LX/9uc;->ch()Ljava/lang/String;

    move-result-object v0

    .line 604999
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605000
    sget v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->c:I

    invoke-static {v3, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    move v1, v0

    .line 605001
    :goto_0
    const v0, 0x7f0215d8

    invoke-static {v3, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 605002
    const v3, 0x102000d

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 605003
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 605004
    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 605005
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 605006
    invoke-interface {v3}, LX/9uc;->bP()D

    move-result-wide v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v3, v4

    .line 605007
    if-ge v3, v2, :cond_1

    .line 605008
    :goto_1
    new-instance v3, LX/E44;

    invoke-direct {v3, v2, v1, v0}, LX/E44;-><init>(IILandroid/graphics/drawable/Drawable;)V

    return-object v3

    .line 605009
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 605010
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 605011
    invoke-interface {v1}, LX/9uc;->ch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    .line 605012
    goto :goto_0

    .line 605013
    :catch_0
    sget v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->c:I

    invoke-static {v3, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 605014
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->d:LX/03V;

    sget-object v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Malformed color sent from server: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 605015
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 605016
    invoke-interface {v6}, LX/9uc;->ch()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x423bb1d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604990
    check-cast p2, LX/E44;

    check-cast p4, Landroid/widget/ProgressBar;

    .line 604991
    iget-object v1, p2, LX/E44;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 604992
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 604993
    iget v1, p2, LX/E44;->a:I

    invoke-virtual {p4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 604994
    const/16 v1, 0x1f

    const v2, -0x53f8d43a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 604986
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604987
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604988
    invoke-interface {v0}, LX/9uc;->bP()D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 604989
    if-ltz v0, :cond_0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
