.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field public final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field public final f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

.field public final g:LX/3mF;

.field public final h:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603732
    const v0, 0x7f030b97

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;LX/3mF;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603723
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603724
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 603725
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 603726
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603727
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    .line 603728
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 603729
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->g:LX/3mF;

    .line 603730
    iput-object p7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->h:Ljava/util/concurrent/ExecutorService;

    .line 603731
    return-void
.end method

.method public static a(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/E2S;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/E2S;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 603721
    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/9u8;

    move-result-object v3

    .line 603722
    new-instance v0, LX/E3Y;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/E3Y;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;LX/E2S;LX/9u8;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;
    .locals 11

    .prologue
    .line 603710
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    monitor-enter v1

    .line 603711
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603712
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603713
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603714
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603715
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v9

    check-cast v9, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;LX/3mF;Ljava/util/concurrent/ExecutorService;)V

    .line 603716
    move-object v0, v3

    .line 603717
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603718
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603719
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603720
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/9u8;
    .locals 2

    .prologue
    .line 603733
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603734
    instance-of v0, v0, LX/9u8;

    if-eqz v0, :cond_0

    .line 603735
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603736
    return-object v0

    .line 603737
    :cond_0
    new-instance v0, Ljava/lang/Error;

    const-string v1, "Could not convert unit component to GroupDescriptionGraphQLFragment."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603709
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 603672
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    const/4 v2, 0x0

    .line 603673
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603674
    invoke-interface {v0}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 603675
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->t()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    .line 603676
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 603677
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/9u8;

    move-result-object v7

    .line 603678
    invoke-interface {v7}, LX/9u8;->dw()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v8

    .line 603679
    invoke-interface {v7}, LX/9u8;->dx()LX/174;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, LX/9u8;->dx()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 603680
    :goto_0
    invoke-interface {v7}, LX/9u8;->hk_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, LX/9u8;->hk_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 603681
    :goto_1
    new-instance v4, LX/E2T;

    .line 603682
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603683
    invoke-interface {v0}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/E2T;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 603684
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v4, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2S;

    .line 603685
    iget-object v4, v0, LX/E2S;->a:LX/03R;

    move-object v4, v4

    .line 603686
    sget-object v9, LX/03R;->UNSET:LX/03R;

    if-ne v4, v9, :cond_0

    .line 603687
    invoke-interface {v7}, LX/9u8;->bi()Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, LX/03R;->YES:LX/03R;

    .line 603688
    :goto_2
    invoke-virtual {v0, v4}, LX/E2S;->a(LX/03R;)V

    .line 603689
    :cond_0
    invoke-static {p0, p2, v0, p3, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->a(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/E2S;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 603690
    invoke-interface {v7}, LX/9u8;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 603691
    const v4, 0x7f0d1cb7

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->e:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v9, LX/E1o;

    invoke-interface {v7}, LX/9u8;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v7

    .line 603692
    iget-object v10, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v10, v10

    .line 603693
    iget-object v11, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v11, v11

    .line 603694
    invoke-direct {v9, v7, v10, v11}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v4, v6, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603695
    :cond_1
    const v4, 0x7f0d1cb8

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v4, v6, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603696
    const v4, 0x7f0d1cb9

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v4, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603697
    const v1, 0x7f0d1cba

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v1, v4, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603698
    const v1, 0x7f0d1cbb

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603699
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 603700
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v5, v0, :cond_6

    const v0, 0x7f020ad5

    .line 603701
    :goto_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CANNOT_JOIN_OR_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v5, v3, :cond_2

    .line 603702
    const v3, 0x7f0d1cbb

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {p1, v3, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603703
    :cond_2
    move-object v0, v2

    .line 603704
    return-object v0

    :cond_3
    move-object v1, v2

    .line 603705
    goto/16 :goto_0

    :cond_4
    move-object v3, v2

    .line 603706
    goto/16 :goto_1

    .line 603707
    :cond_5
    sget-object v4, LX/03R;->NO:LX/03R;

    goto :goto_2

    .line 603708
    :cond_6
    const v0, 0x7f020a52

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603668
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603669
    invoke-static {p1}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/9u8;

    move-result-object v0

    .line 603670
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 603671
    invoke-interface {v1}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9u8;->dw()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9u8;->dw()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
