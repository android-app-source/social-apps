.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3v;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/03V;

.field private final d:LX/2dq;

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "LX/E3x;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604701
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->a:Ljava/lang/String;

    .line 604702
    const v0, 0x7f03116e

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604659
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604660
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->c:LX/03V;

    .line 604661
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->d:LX/2dq;

    .line 604662
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 604663
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    .line 604664
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 604690
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;

    monitor-enter v1

    .line 604691
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604692
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604693
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604694
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604695
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v4

    check-cast v4, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;-><init>(LX/03V;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;)V

    .line 604696
    move-object v0, p0

    .line 604697
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604698
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604699
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604700
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604703
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 604676
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 604677
    new-instance v2, LX/E2b;

    .line 604678
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 604679
    invoke-direct {v2, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    .line 604680
    new-instance v3, LX/E3u;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->c:LX/03V;

    move-object v0, p3

    check-cast v0, LX/1Pr;

    invoke-interface {v0, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    invoke-direct {v3, v1, v0, v4, p2}, LX/E3u;-><init>(LX/03V;LX/E2c;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 604681
    const v6, 0x7f0d1637

    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->d:LX/2dq;

    .line 604682
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v4

    const/high16 v5, -0x3f000000    # -8.0f

    .line 604683
    iput v5, v4, LX/1UY;->d:F

    .line 604684
    move-object v4, v4

    .line 604685
    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v4

    move-object v1, v4

    .line 604686
    check-cast p3, LX/1Pr;

    invoke-interface {p3, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E2c;

    .line 604687
    iget v4, v2, LX/E2c;->d:I

    move v2, v4

    .line 604688
    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604689
    new-instance v0, LX/E3v;

    invoke-direct {v0, v3}, LX/E3v;-><init>(LX/E3u;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5041808f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604668
    check-cast p2, LX/E3v;

    check-cast p4, Lcom/facebook/widget/CustomFrameLayout;

    .line 604669
    const v1, 0x7f0d2919

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 604670
    iget-object v2, p2, LX/E3v;->a:LX/E3u;

    .line 604671
    iput-object v1, v2, LX/E3u;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 604672
    iget-object p2, v2, LX/E3u;->c:LX/E2c;

    .line 604673
    iget p4, p2, LX/E2c;->d:I

    move p2, p4

    .line 604674
    iget-object p4, v2, LX/E3u;->e:LX/0Px;

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p4

    invoke-static {v2, p2, p4}, LX/E3u;->a(LX/E3u;II)V

    .line 604675
    const/16 v1, 0x1f

    const v2, 0x1b2bf9e4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 604665
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604666
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604667
    invoke-interface {v0}, LX/9uc;->bU()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
