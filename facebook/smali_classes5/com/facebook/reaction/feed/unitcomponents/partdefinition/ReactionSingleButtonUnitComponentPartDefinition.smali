.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605263
    new-instance v0, LX/3aT;

    invoke-direct {v0}, LX/3aT;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605248
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605249
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 605250
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 605251
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 605252
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;

    monitor-enter v1

    .line 605253
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605254
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605257
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 605258
    move-object v0, p0

    .line 605259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 605247
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 605234
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 605235
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605236
    invoke-interface {v0}, LX/9uc;->l()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 605237
    const v1, 0x7f0d28cb

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605238
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v2, LX/E1o;

    .line 605239
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 605240
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 605241
    invoke-direct {v2, v0, v3, v4}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605242
    const v1, 0x7f0d28cb

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v3, LX/E1o;

    .line 605243
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 605244
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 605245
    invoke-direct {v3, v0, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605246
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 605231
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 605232
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 605233
    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->SINGLE_BUTTON:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v3, :cond_0

    invoke-interface {v2}, LX/9uc;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, LX/9uc;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, LX/9uc;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
