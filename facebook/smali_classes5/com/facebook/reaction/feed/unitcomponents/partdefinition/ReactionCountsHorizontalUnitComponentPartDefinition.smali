.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/154;

.field private final c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603201
    new-instance v0, LX/3aA;

    invoke-direct {v0}, LX/3aA;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/154;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603197
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603198
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    .line 603199
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->b:LX/154;

    .line 603200
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 603148
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;

    monitor-enter v1

    .line 603149
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603150
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603153
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v3

    check-cast v3, LX/154;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;-><init>(LX/154;Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;)V

    .line 603154
    move-object v0, p0

    .line 603155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603196
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 603169
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v8, 0x3

    const/4 v7, 0x2

    .line 603170
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603171
    invoke-interface {v0}, LX/9uc;->ad()LX/0Px;

    move-result-object v1

    .line 603172
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    .line 603173
    const v2, 0x7f0d28d6

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    new-instance v4, LX/E68;

    .line 603174
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 603175
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 603176
    invoke-direct {v4, v0, v5, v6}, LX/E68;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603177
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v7, :cond_0

    .line 603178
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    .line 603179
    const v2, 0x7f0d28d7    # 1.876332E38f

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    new-instance v4, LX/E68;

    .line 603180
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 603181
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 603182
    invoke-direct {v4, v0, v5, v6}, LX/E68;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603183
    :cond_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v8, :cond_1

    .line 603184
    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    .line 603185
    const v2, 0x7f0d28d8

    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    new-instance v4, LX/E68;

    .line 603186
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 603187
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 603188
    invoke-direct {v4, v0, v5, v6}, LX/E68;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603189
    :cond_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x4

    if-lt v0, v2, :cond_2

    .line 603190
    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    .line 603191
    const v1, 0x7f0d28d9

    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->c:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionSingleCountUnitComponentPartDefinition;

    new-instance v3, LX/E68;

    .line 603192
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 603193
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 603194
    invoke-direct {v3, v0, v4, v5}, LX/E68;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603195
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 603159
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 603160
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603161
    invoke-interface {v0}, LX/9uc;->ad()LX/0Px;

    move-result-object v3

    .line 603162
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 603163
    :goto_0
    return v0

    .line 603164
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;

    .line 603165
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->c()LX/174;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->c()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCountsComponentFragmentModel$CountsModel;->b()I

    move-result v0

    if-gez v0, :cond_2

    :cond_1
    move v0, v1

    .line 603166
    goto :goto_0

    .line 603167
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 603168
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
