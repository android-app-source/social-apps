.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E3r;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604568
    new-instance v0, LX/3aJ;

    invoke-direct {v0}, LX/3aJ;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604518
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604519
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 604520
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 604521
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;
    .locals 5

    .prologue
    .line 604557
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;

    monitor-enter v1

    .line 604558
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604559
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604560
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604561
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604562
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 604563
    move-object v0, p0

    .line 604564
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604565
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604566
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604567
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/E3r;Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E3r;",
            "Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604547
    iget-object v1, p0, LX/E3r;->a:Ljava/lang/String;

    iget-object v2, p0, LX/E3r;->b:Ljava/lang/String;

    iget-object v3, p0, LX/E3r;->c:Lcom/facebook/location/ImmutableLocation;

    iget-object v4, p0, LX/E3r;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    iget v5, p0, LX/E3r;->e:I

    move-object v0, p1

    .line 604548
    iput-object v3, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->l:Lcom/facebook/location/ImmutableLocation;

    .line 604549
    iget-object v6, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->j:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v6}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->l:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v8

    iget-object v7, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->l:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v10

    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v6

    .line 604550
    if-eqz v4, :cond_0

    .line 604551
    new-instance v7, Landroid/graphics/RectF;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->d()D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->b()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->a()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;->c()D

    move-result-wide v12

    double-to-float v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v6, v7}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 604552
    :cond_0
    iget-object v7, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->m:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v7, v6}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 604553
    iput-object v2, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->o:Ljava/lang/String;

    .line 604554
    iput-object v1, v0, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->p:Ljava/lang/String;

    .line 604555
    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->g(Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    .line 604556
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604569
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 604531
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v2, 0x0

    .line 604532
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v7, v0

    .line 604533
    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 604534
    if-eqz v1, :cond_0

    .line 604535
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    .line 604536
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 604537
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 604538
    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 604539
    :cond_0
    invoke-interface {v7}, LX/9uc;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v5

    .line 604540
    if-eqz v1, :cond_1

    .line 604541
    const v8, 0x7f0d2913

    iget-object v9, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v0, LX/E1o;

    .line 604542
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v1

    .line 604543
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v1

    .line 604544
    move-object v1, v5

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    invoke-interface {p1, v8, v9, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604545
    :cond_1
    const v0, 0x7f0d2914

    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v7}, LX/9uc;->dl()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604546
    new-instance v0, LX/E3r;

    invoke-interface {v7}, LX/9uc;->K()LX/174;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, LX/9uc;->K()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v7}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapWithNavigationComponentFragmentModel$PageModel$CityModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapWithNavigationComponentFragmentModel$PageModel$CityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPageMapWithNavigationComponentFragmentModel$PageModel$CityModel;->a()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-interface {v7}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;->a()D

    move-result-wide v4

    invoke-interface {v7}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;->b()D

    move-result-wide v8

    invoke-static {v4, v5, v8, v9}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v3

    invoke-virtual {v3}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    invoke-interface {v7}, LX/9uc;->bw()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;

    move-result-object v4

    invoke-interface {v7}, LX/9uc;->dv()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/E3r;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;I)V

    return-object v0

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2113a0ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604530
    check-cast p2, LX/E3r;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;

    invoke-static {p2, p4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->a(LX/E3r;Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x1268df29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 604525
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604526
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604527
    invoke-interface {v0}, LX/9uc;->bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v1

    .line 604528
    invoke-interface {v1}, LX/9ui;->a()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    invoke-interface {v1}, LX/9ui;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 604529
    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dl()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dl()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 604522
    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;

    .line 604523
    iget-object p0, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionPageMapWithNavigationComponentView;->f:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 604524
    return-void
.end method
