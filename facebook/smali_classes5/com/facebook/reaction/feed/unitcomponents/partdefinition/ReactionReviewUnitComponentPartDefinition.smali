.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E48;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;

.field public static final c:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/11S;

.field private final f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/03V;

.field public final h:LX/E1i;

.field public final i:LX/BNM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605142
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 605143
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->b:Ljava/lang/String;

    .line 605144
    new-instance v0, LX/3aQ;

    invoke-direct {v0}, LX/3aQ;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/11R;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/03V;LX/E1i;LX/BNM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605134
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605135
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 605136
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->e:LX/11S;

    .line 605137
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 605138
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->g:LX/03V;

    .line 605139
    iput-object p5, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h:LX/E1i;

    .line 605140
    iput-object p6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->i:LX/BNM;

    .line 605141
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;
    .locals 10

    .prologue
    .line 605080
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    monitor-enter v1

    .line 605081
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605082
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605083
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605084
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605085
    new-instance v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v5

    check-cast v5, LX/11R;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v8

    check-cast v8, LX/E1i;

    invoke-static {v0}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v9

    check-cast v9, LX/BNM;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/11R;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/03V;LX/E1i;LX/BNM;)V

    .line 605086
    move-object v0, v3

    .line 605087
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605088
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605089
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605090
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .locals 1

    .prologue
    .line 605132
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605133
    invoke-interface {v0}, LX/9uc;->cM()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605131
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 605099
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 605100
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v6

    .line 605101
    iget-object v8, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->e:LX/11S;

    sget-object v9, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    const-wide/16 v10, 0x3e8

    mul-long/2addr v6, v10

    invoke-interface {v8, v9, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    move-object v1, v6

    .line 605102
    move-object v0, p3

    .line 605103
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 605104
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 605105
    invoke-interface {v2}, LX/9uc;->cM()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v3

    .line 605106
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;->a()LX/174;

    move-result-object v2

    if-nez v2, :cond_3

    .line 605107
    const/4 v2, 0x0

    .line 605108
    :goto_0
    move-object v0, v2

    .line 605109
    const/4 v4, 0x0

    .line 605110
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v3

    .line 605111
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v2

    if-nez v2, :cond_6

    .line 605112
    :cond_0
    const/4 v2, 0x0

    .line 605113
    :goto_1
    move-object v2, v2

    .line 605114
    if-eqz v2, :cond_1

    .line 605115
    const v3, 0x7f0d28cc

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->f:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v2

    sget-object v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 605116
    iput-object v5, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 605117
    move-object v2, v2

    .line 605118
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605119
    :cond_1
    invoke-static {p2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 605120
    if-eqz v2, :cond_2

    .line 605121
    iget-object v3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v4, LX/E47;

    invoke-direct {v4, p0, v2, p2, p3}, LX/E47;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605122
    :cond_2
    new-instance v2, LX/E48;

    invoke-direct {v2, v1, v0}, LX/E48;-><init>(Ljava/lang/String;Landroid/text/SpannableStringBuilder;)V

    return-object v2

    .line 605123
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;->a()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 605124
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u2026"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 605125
    :cond_4
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 605126
    invoke-interface {v3}, LX/9uc;->cM()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->a()I

    move-result v3

    move v3, v3

    .line 605127
    if-nez v3, :cond_5

    .line 605128
    invoke-static {v2}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_0

    .line 605129
    :cond_5
    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->i:LX/BNM;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0052

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v3, v5}, LX/BNM;->a(II)Landroid/text/SpannableString;

    move-result-object v3

    .line 605130
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2370dbef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605095
    check-cast p2, LX/E48;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 605096
    iget-object v1, p2, LX/E48;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 605097
    iget-object v1, p2, LX/E48;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 605098
    const/16 v1, 0x1f

    const v2, 0x384bd735

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 605091
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 605092
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 605093
    invoke-interface {v0}, LX/9uc;->cM()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;

    move-result-object v0

    .line 605094
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->b()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel$FormattedPreviewModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
