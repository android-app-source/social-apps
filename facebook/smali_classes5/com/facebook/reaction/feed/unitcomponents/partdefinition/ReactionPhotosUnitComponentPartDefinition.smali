.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E43;",
        "TE;",
        "Landroid/support/v7/widget/RecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/E4F;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604823
    new-instance v0, LX/3aN;

    invoke-direct {v0}, LX/3aN;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/E4F;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604824
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604825
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->b:LX/E4F;

    .line 604826
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 604811
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;

    monitor-enter v1

    .line 604812
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604813
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604814
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604815
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604816
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;

    const-class v3, LX/E4F;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/E4F;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;-><init>(LX/E4F;)V

    .line 604817
    move-object v0, p0

    .line 604818
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604819
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604820
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604821
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604822
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 604800
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 604801
    new-instance v1, LX/1P1;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 604802
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/1P1;->b(I)V

    .line 604803
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->b:LX/E4F;

    .line 604804
    new-instance v5, LX/E4E;

    move-object v6, p3

    check-cast v6, LX/1Pn;

    invoke-static {v0}, LX/3Tz;->b(LX/0QB;)LX/3Tz;

    move-result-object v9

    check-cast v9, LX/3Tz;

    const-class v7, LX/E4J;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/E4J;

    invoke-static {v0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v11

    check-cast v11, LX/1vo;

    move-object v7, v1

    move-object v8, p2

    invoke-direct/range {v5 .. v11}, LX/E4E;-><init>(LX/1Pn;LX/1P1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3Tz;LX/E4J;LX/1vo;)V

    .line 604805
    move-object v2, v5

    .line 604806
    new-instance v3, LX/E2b;

    .line 604807
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 604808
    invoke-direct {v3, v0}, LX/E2b;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 604809
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 604810
    new-instance v3, LX/E43;

    new-instance v4, LX/E42;

    invoke-direct {v4, p0, v2, p3, p2}, LX/E42;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;LX/E4E;LX/1Pn;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-direct {v3, v2, v1, v0, v4}, LX/E43;-><init>(LX/E4E;LX/1P1;LX/E2c;LX/1OX;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x54f4ade1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 604793
    check-cast p2, LX/E43;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 604794
    iget-object v1, p2, LX/E43;->b:LX/1P1;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 604795
    iget-object v1, p2, LX/E43;->a:LX/E4E;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 604796
    iget-object v1, p2, LX/E43;->d:LX/1OX;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 604797
    iget-object v1, p2, LX/E43;->b:LX/1P1;

    iget-object v2, p2, LX/E43;->c:LX/E2c;

    invoke-static {v1, v2}, LX/E2d;->a(LX/1P1;LX/E2c;)V

    .line 604798
    iget-object v1, p2, LX/E43;->a:LX/E4E;

    invoke-virtual {v1}, LX/E4E;->d()V

    .line 604799
    const/16 v1, 0x1f

    const v2, 0x706e3718

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 604781
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604782
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604783
    check-cast v0, LX/9uf;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v0

    .line 604784
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 604785
    invoke-interface {v1}, LX/9uc;->cR()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 604786
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 604787
    invoke-interface {v1}, LX/9uc;->cR()D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 604788
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    .line 604789
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v1

    invoke-static {v1}, LX/E4I;->a(LX/9uc;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604790
    const/4 v1, 0x1

    .line 604791
    :goto_0
    move v0, v1

    .line 604792
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 604776
    check-cast p2, LX/E43;

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    .line 604777
    iget-object v0, p2, LX/E43;->b:LX/1P1;

    iget-object v1, p2, LX/E43;->c:LX/E2c;

    invoke-static {v0, v1, p4}, LX/E2d;->a(LX/1P1;LX/E2c;Landroid/support/v7/widget/RecyclerView;)V

    .line 604778
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 604779
    iget-object v0, p2, LX/E43;->d:LX/1OX;

    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 604780
    return-void
.end method
