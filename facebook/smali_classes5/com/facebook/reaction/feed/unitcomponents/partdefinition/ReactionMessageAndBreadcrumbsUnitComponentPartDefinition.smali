.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/E52;

.field private final e:LX/2d1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/E52;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604410
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 604411
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->d:LX/E52;

    .line 604412
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->e:LX/2d1;

    .line 604413
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 604383
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604384
    iget-object v1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->d:LX/E52;

    const/4 v2, 0x0

    .line 604385
    new-instance v3, LX/E51;

    invoke-direct {v3, v1}, LX/E51;-><init>(LX/E52;)V

    .line 604386
    iget-object p0, v1, LX/E52;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E50;

    .line 604387
    if-nez p0, :cond_0

    .line 604388
    new-instance p0, LX/E50;

    invoke-direct {p0, v1}, LX/E50;-><init>(LX/E52;)V

    .line 604389
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/E50;->a$redex0(LX/E50;LX/1De;IILX/E51;)V

    .line 604390
    move-object v3, p0

    .line 604391
    move-object v2, v3

    .line 604392
    move-object v1, v2

    .line 604393
    invoke-interface {v0}, LX/9uc;->F()LX/0Px;

    move-result-object v2

    .line 604394
    iget-object v3, v1, LX/E50;->a:LX/E51;

    iput-object v2, v3, LX/E51;->b:LX/0Px;

    .line 604395
    iget-object v3, v1, LX/E50;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 604396
    move-object v1, v1

    .line 604397
    iget-object v2, v1, LX/E50;->a:LX/E51;

    iput-object p3, v2, LX/E51;->c:LX/2km;

    .line 604398
    move-object v1, v1

    .line 604399
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 604400
    iget-object v2, v1, LX/E50;->a:LX/E51;

    iput-object v0, v2, LX/E51;->a:Ljava/lang/String;

    .line 604401
    iget-object v2, v1, LX/E50;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 604402
    move-object v0, v1

    .line 604403
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 604404
    iget-object v2, v0, LX/E50;->a:LX/E51;

    iput-object v1, v2, LX/E51;->d:Ljava/lang/String;

    .line 604405
    move-object v0, v0

    .line 604406
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 604407
    iget-object v2, v0, LX/E50;->a:LX/E51;

    iput-object v1, v2, LX/E51;->e:Ljava/lang/String;

    .line 604408
    move-object v0, v0

    .line 604409
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 604361
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;

    monitor-enter v1

    .line 604362
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604363
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604364
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604365
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604366
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/E52;->a(LX/0QB;)LX/E52;

    move-result-object v5

    check-cast v5, LX/E52;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/E52;)V

    .line 604367
    move-object v0, p0

    .line 604368
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604369
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604370
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 604382
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->e:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 604381
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 604380
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 604379
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 604378
    invoke-direct {p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMessageAndBreadcrumbsUnitComponentPartDefinition;->h()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 604373
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 604374
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604375
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 604376
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 604377
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 604372
    const/4 v0, 0x0

    return-object v0
.end method
