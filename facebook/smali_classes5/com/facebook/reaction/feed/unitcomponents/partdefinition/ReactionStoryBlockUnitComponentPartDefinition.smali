.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/E4A;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field public final c:Landroid/text/style/TextAppearanceSpan;

.field public final d:Landroid/text/style/TextAppearanceSpan;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:LX/1xf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605507
    new-instance v0, LX/3aV;

    invoke-direct {v0}, LX/3aV;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1xf;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605500
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605501
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 605502
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 605503
    iput-object p4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->f:LX/1xf;

    .line 605504
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0911

    invoke-direct {v0, p2, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->c:Landroid/text/style/TextAppearanceSpan;

    .line 605505
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0912

    invoke-direct {v0, p2, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->d:Landroid/text/style/TextAppearanceSpan;

    .line 605506
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/E4A;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/E4A;"
        }
    .end annotation

    .prologue
    .line 605474
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v0

    .line 605475
    invoke-interface {v3}, LX/9uc;->cQ()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v1

    .line 605476
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 605477
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 605478
    invoke-interface {v3}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 605479
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v6

    .line 605480
    iget-object v0, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->f:LX/1xf;

    sget-object v2, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {v0, v2, v8, v9}, LX/1xf;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 605481
    invoke-interface {v3}, LX/9uc;->bk()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082236

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v1, v2, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 605482
    :cond_0
    invoke-interface {v3}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 605483
    :goto_0
    invoke-interface {v3}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, LX/9uc;->aj()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    .line 605484
    :goto_1
    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v8, LX/E1o;

    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 605485
    iget-object v9, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 605486
    iget-object v10, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v10, v10

    .line 605487
    invoke-direct {v8, v3, v9, v10}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v7, v8}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605488
    const v3, 0x7f0d292b

    iget-object v7, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    const/16 p2, 0x21

    .line 605489
    new-instance v8, Landroid/text/SpannableStringBuilder;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " \u2014 "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 605490
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    .line 605491
    iget-object v10, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->c:Landroid/text/style/TextAppearanceSpan;

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11, v9, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 605492
    iget-object v10, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->d:Landroid/text/style/TextAppearanceSpan;

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    invoke-virtual {v8, v10, v9, v11, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 605493
    move-object v4, v8

    .line 605494
    invoke-interface {p1, v3, v7, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605495
    const v3, 0x7f0d292c

    iget-object v4, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v3, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605496
    new-instance v3, LX/E4A;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v1, :cond_3

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_2
    if-eqz v2, :cond_4

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_3
    invoke-direct {v3, v4, v1, v0}, LX/E4A;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    return-object v3

    .line 605497
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 605498
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 605499
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 605430
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 605431
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605432
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605433
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605434
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605435
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/1xf;->a(LX/0QB;)LX/1xf;

    move-result-object v6

    check-cast v6, LX/1xf;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1xf;)V

    .line 605436
    move-object v0, p0

    .line 605437
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605438
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605439
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 605473
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 605472
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->a(LX/1aD;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/E4A;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2cc1379e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605448
    check-cast p2, LX/E4A;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;

    .line 605449
    iget-object v1, p2, LX/E4A;->a:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 605450
    iget-object v1, p2, LX/E4A;->b:Landroid/net/Uri;

    iget-object v2, p2, LX/E4A;->c:Landroid/net/Uri;

    const/4 p0, 0x0

    .line 605451
    if-nez v1, :cond_1

    if-nez v2, :cond_1

    .line 605452
    iget-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v4, :cond_0

    .line 605453
    iget-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p0, 0x8

    invoke-virtual {v4, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 605454
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x2a412d10

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 605455
    :cond_1
    iget-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v4, :cond_2

    .line 605456
    const v4, 0x7f0d292d

    invoke-virtual {p4, v4}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 605457
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 605458
    :cond_2
    iget-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 605459
    if-eqz v1, :cond_3

    const/4 v4, 0x1

    .line 605460
    :goto_1
    invoke-virtual {p4}, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 605461
    if-eqz v4, :cond_5

    const p0, 0x7f0b164b

    :goto_2
    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 605462
    if-eqz v4, :cond_6

    const p0, 0x7f0b164b

    :goto_3
    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 605463
    iget-object p1, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 605464
    iget-object p1, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iput p3, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 605465
    iget-object p1, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iput p0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 605466
    :goto_4
    iget-object v4, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v1, :cond_4

    :goto_5
    sget-object p0, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    :cond_3
    move v4, p0

    .line 605467
    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 605468
    goto :goto_5

    .line 605469
    :cond_5
    const p0, 0x7f0b0089

    goto :goto_2

    .line 605470
    :cond_6
    const p0, 0x7f0b0089

    goto :goto_3

    .line 605471
    :cond_7
    iget-object p1, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionStoryBlockUnitComponentView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p2, p3, p0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    .line 605441
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 605442
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 605443
    invoke-interface {v2}, LX/9uc;->cQ()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v3

    .line 605444
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 605445
    :goto_0
    return v0

    .line 605446
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    .line 605447
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->c()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
