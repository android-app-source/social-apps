.class public Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Landroid/graphics/drawable/Drawable;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Cz;

.field public static final c:I

.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/03V;

.field private final e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

.field private final f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602966
    const-class v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->a:Ljava/lang/String;

    .line 602967
    const v0, 0x7f03111c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->b:LX/1Cz;

    .line 602968
    const v0, 0x7f0a00d1

    sput v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->c:I

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602969
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602970
    iput-object p1, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->d:LX/03V;

    .line 602971
    iput-object p2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    .line 602972
    iput-object p3, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    .line 602973
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 602974
    const-class v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;

    monitor-enter v1

    .line 602975
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602976
    sput-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602977
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602978
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 602979
    new-instance p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;-><init>(LX/03V;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V

    .line 602980
    move-object v0, p0

    .line 602981
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602982
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602983
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602984
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 602985
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 602986
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 602987
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v0

    .line 602988
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v0

    .line 602989
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v0

    .line 602990
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 602991
    invoke-interface {v4}, LX/9uc;->aP()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 602992
    sget v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->c:I

    invoke-static {v5, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    move v2, v0

    .line 602993
    :goto_0
    const v0, 0x7f0d2894

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v7, LX/E2v;

    invoke-interface {v4}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v8

    invoke-direct {v7, v8, v1, v3}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602994
    const v0, 0x7f0d2894

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v0, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602995
    const/4 v0, 0x0

    .line 602996
    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 602997
    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    .line 602998
    const v4, 0x7f0d2895

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesColorPartDefinition;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v4, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 602999
    :cond_0
    const v4, 0x7f0d2895

    iget-object v6, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v7, LX/E2v;

    invoke-direct {v7, v0, v1, v3}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v4, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603000
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1604

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 603001
    const v0, 0x7f0215b7

    invoke-static {v5, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 603002
    const v1, 0x7f0d31f1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 603003
    invoke-virtual {v1, v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 603004
    return-object v0

    .line 603005
    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, LX/9uc;->aP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v2, v0

    .line 603006
    goto/16 :goto_0

    .line 603007
    :catch_0
    sget v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->c:I

    invoke-static {v5, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 603008
    iget-object v2, p0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->d:LX/03V;

    sget-object v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Malformed color sent from server: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, LX/9uc;->aP()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x9cb8b4c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603009
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 603010
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 603011
    invoke-virtual {p4, p2}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 603012
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x72135b30

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 603013
    :cond_0
    invoke-virtual {p4, p2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 603014
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 603015
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603016
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 603017
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 603018
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
