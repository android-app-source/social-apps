.class public Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "LX/3Tw;",
        "LX/E8I;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605648
    new-instance v0, LX/3aY;

    invoke-direct {v0}, LX/3aY;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605649
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605650
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;
    .locals 3

    .prologue
    .line 605651
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;

    monitor-enter v1

    .line 605652
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605653
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 605656
    new-instance v0, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;-><init>()V

    .line 605657
    move-object v0, v0

    .line 605658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/E8I;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605662
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x127a3032

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605663
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/3Tw;

    check-cast p4, LX/E8I;

    .line 605664
    invoke-interface {p3}, LX/3Tw;->u()LX/0o8;

    move-result-object v1

    .line 605665
    iput-object v1, p4, LX/E8I;->b:LX/0o8;

    .line 605666
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    iput-object v1, p4, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 605667
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionCardNode;->b:LX/Cfx;

    move-object v1, v1

    .line 605668
    iput-object v1, p4, LX/E8I;->e:LX/Cfx;

    .line 605669
    iget-object v1, p4, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->q()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionAggregateUnitFragmentModel$ReactionAggregatedUnitsModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, p4, LX/E8I;->d:LX/0Px;

    .line 605670
    invoke-virtual {p4}, LX/BeU;->a()V

    .line 605671
    const/16 v1, 0x1f

    const v2, 0x32cc961f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 605672
    const/4 v0, 0x1

    return v0
.end method
