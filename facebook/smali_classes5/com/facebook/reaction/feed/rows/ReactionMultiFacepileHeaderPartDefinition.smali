.class public Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

.field private final e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

.field private final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604444
    const v0, 0x7f031124

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;LX/2d1;Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604445
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604446
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 604447
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    .line 604448
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->d:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    .line 604449
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    .line 604450
    invoke-virtual {p3}, LX/2d1;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->f:Z

    .line 604451
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;
    .locals 9

    .prologue
    .line 604452
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;

    monitor-enter v1

    .line 604453
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604454
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604455
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604456
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604457
    new-instance v3, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v6

    check-cast v6, LX/2d1;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;LX/2d1;Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V

    .line 604458
    move-object v0, v3

    .line 604459
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604460
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604461
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604463
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 604464
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    const/4 v1, 0x0

    .line 604465
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 604466
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 604467
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 604468
    iget-boolean v4, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->f:Z

    if-eqz v4, :cond_0

    .line 604469
    iget-object v4, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    new-instance v5, LX/E3A;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    sget-object v7, LX/E3D;->a:LX/1Ua;

    invoke-direct {v5, v6, v7}, LX/E3A;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 604470
    :cond_0
    const v4, 0x7f0d28e2

    iget-object v5, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v6, LX/E2v;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v7

    invoke-direct {v6, v7, v2, v3}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604471
    const v4, 0x7f0d28a5

    iget-object v5, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->d:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object v6

    .line 604472
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    invoke-static {v7, v6}, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;->a(ILX/0Px;)LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 604473
    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604474
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    .line 604475
    if-eqz v4, :cond_1

    .line 604476
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 604477
    :goto_0
    const v5, 0x7f0d28a5

    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v7, LX/E1o;

    invoke-direct {v7, v4, v0, v2, v3}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v5, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604478
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    .line 604479
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 604480
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 604481
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 604482
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object v0

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 604483
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-gt v1, p0, :cond_2

    .line 604484
    :cond_0
    :goto_0
    move v0, v3

    .line 604485
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 604486
    :goto_2
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 604487
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5sX;

    .line 604488
    invoke-interface {v1}, LX/5sX;->d()LX/5sY;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {v1}, LX/5sX;->d()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 604489
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_3
    move v3, p0

    .line 604490
    goto :goto_0
.end method
