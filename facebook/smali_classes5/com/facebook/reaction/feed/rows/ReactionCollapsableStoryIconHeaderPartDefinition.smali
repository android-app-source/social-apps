.class public Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603075
    new-instance v0, LX/3a9;

    invoke-direct {v0}, LX/3a9;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603076
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603077
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 603078
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    .line 603079
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 603080
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;
    .locals 6

    .prologue
    .line 603081
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    monitor-enter v1

    .line 603082
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603083
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603084
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603085
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603086
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;)V

    .line 603087
    move-object v0, p0

    .line 603088
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603089
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603090
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603092
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 603093
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/1Pq;

    const/4 v5, 0x0

    .line 603094
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603095
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 603096
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 603097
    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    new-instance v4, LX/E2s;

    invoke-direct {v4, v0, v1, v2}, LX/E2s;-><init>(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603098
    new-instance v0, LX/E2k;

    invoke-direct {v0, p0, p3, p2}, LX/E2k;-><init>(Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;LX/1Pq;Lcom/facebook/reaction/common/ReactionCardNode;)V

    .line 603099
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603100
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603101
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6f551ef3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603102
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/1Pq;

    check-cast p4, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;

    .line 603103
    check-cast p3, LX/1Pr;

    new-instance v1, LX/E2U;

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/E2U;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v1, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/E2V;

    .line 603104
    iget-boolean v2, v1, LX/E2V;->a:Z

    move v1, v2

    .line 603105
    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 603106
    :goto_0
    iget-object p1, p4, Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 603107
    const/16 v1, 0x1f

    const v2, 0x2c668c36

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 603108
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 603109
    :cond_1
    const/16 v2, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603110
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603111
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
