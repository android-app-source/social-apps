.class public Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 604122
    const-class v0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;

    const-string v1, "reaction_dialog"

    const-string v2, "component_icon"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 604123
    new-instance v0, LX/3aG;

    invoke-direct {v0}, LX/3aG;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 604124
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 604125
    const v0, 0x7f031115

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 604126
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 604127
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f01072b

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604128
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->setBackgroundResource(I)V

    .line 604129
    :cond_0
    const v0, 0x7f0d2882

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 604130
    const v0, 0x7f0d2883

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->d:Landroid/widget/TextView;

    .line 604131
    const v0, 0x7f0d2884

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->e:Landroid/widget/TextView;

    .line 604132
    return-void
.end method
