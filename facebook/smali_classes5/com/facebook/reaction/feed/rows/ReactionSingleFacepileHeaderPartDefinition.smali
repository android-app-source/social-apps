.class public Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

.field public final e:LX/E1i;

.field private final f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

.field private final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605266
    const v0, 0x7f031125

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2d1;Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;LX/E1i;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605267
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605268
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    .line 605269
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 605270
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    .line 605271
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->e:LX/E1i;

    .line 605272
    iput-object p6, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    .line 605273
    invoke-virtual {p3}, LX/2d1;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->g:Z

    .line 605274
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;
    .locals 10

    .prologue
    .line 605275
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;

    monitor-enter v1

    .line 605276
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605277
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605280
    new-instance v3, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v6

    check-cast v6, LX/2d1;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v8

    check-cast v8, LX/E1i;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;-><init>(Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2d1;Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;LX/E1i;Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;)V

    .line 605281
    move-object v0, v3

    .line 605282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 605286
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 605287
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p3, LX/2km;

    .line 605288
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 605289
    iget-boolean v1, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->g:Z

    if-eqz v1, :cond_0

    .line 605290
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/styling/ReactionBackgroundPartDefinition;

    new-instance v2, LX/E3A;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/E3D;->a:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/E3A;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605291
    :cond_0
    const v1, 0x7f0d28e2

    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->f:Lcom/facebook/reaction/feed/rows/subparts/ReactionTextWithEntitiesPartDefinition;

    new-instance v3, LX/E2v;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, LX/E2v;-><init>(LX/3Ab;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605292
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sX;

    .line 605293
    new-instance v1, LX/E2l;

    invoke-direct {v1, p0, v0, p3, p2}, LX/E2l;-><init>(Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;LX/5sX;LX/2km;Lcom/facebook/reaction/common/ReactionCardNode;)V

    .line 605294
    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605295
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    new-instance v2, LX/8Cq;

    invoke-direct {v2}, LX/8Cq;-><init>()V

    const v3, 0x7f0a010a

    .line 605296
    iput v3, v2, LX/8Cq;->c:I

    .line 605297
    move-object v2, v2

    .line 605298
    invoke-interface {v0}, LX/5sX;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/8Cq;->a(Ljava/lang/String;)LX/8Cq;

    move-result-object v0

    invoke-virtual {v0}, LX/8Cq;->a()LX/8Cr;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605299
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 605300
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 605301
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 605302
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object v0

    const/4 p0, 0x1

    const/4 v2, 0x0

    .line 605303
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-eq v1, p0, :cond_1

    move v1, v2

    .line 605304
    :goto_0
    move v0, v1

    .line 605305
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 605306
    :cond_1
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5sX;

    .line 605307
    invoke-interface {v1}, LX/5sX;->d()LX/5sY;

    move-result-object p1

    .line 605308
    if-eqz p1, :cond_2

    invoke-interface {p1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    invoke-interface {v1}, LX/5sX;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, p0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method
