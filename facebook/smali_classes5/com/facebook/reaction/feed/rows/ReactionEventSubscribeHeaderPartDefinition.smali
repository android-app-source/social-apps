.class public Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603376
    const v0, 0x7f031127

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603377
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603378
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;

    .line 603379
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    .line 603380
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;
    .locals 5

    .prologue
    .line 603381
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    monitor-enter v1

    .line 603382
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603383
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603384
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603385
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603386
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;)V

    .line 603387
    move-object v0, p0

    .line 603388
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603389
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603390
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603392
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 603393
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603394
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionHeaderPartDefinition;

    new-instance v1, LX/E2q;

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/E2q;-><init>(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603395
    const v0, 0x7f0d28a8

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventSubscribeButtonPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603396
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603397
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603398
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603399
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->EVENTS_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
