.class public Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "LX/E2j;",
        "LX/1PW;",
        "LX/E2z;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603045
    new-instance v0, LX/3a8;

    invoke-direct {v0}, LX/3a8;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603019
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603020
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;
    .locals 3

    .prologue
    .line 603034
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;

    monitor-enter v1

    .line 603035
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603036
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603037
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603038
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 603039
    new-instance v0, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;-><init>()V

    .line 603040
    move-object v0, v0

    .line 603041
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603042
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603043
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 603046
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 603031
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603032
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603033
    new-instance v1, LX/E2j;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/E2j;-><init>(LX/3Ab;LX/3Ab;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xdaecff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 603024
    check-cast p2, LX/E2j;

    check-cast p4, LX/E2z;

    .line 603025
    iget-object v1, p2, LX/E2j;->b:LX/3Ab;

    iget-object v2, p2, LX/E2j;->a:LX/3Ab;

    .line 603026
    :try_start_0
    iget-object p0, p4, LX/E2z;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V

    .line 603027
    iget-object p0, p4, LX/E2z;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 603028
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x42474f08

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 603029
    :catch_0
    move-exception p0

    .line 603030
    const-string p1, "PlaceTipsFeedUnitView"

    invoke-virtual {p0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, p0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603021
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603022
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603023
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
