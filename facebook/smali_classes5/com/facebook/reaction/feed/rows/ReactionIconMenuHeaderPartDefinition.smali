.class public Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603931
    new-instance v0, LX/3aF;

    invoke-direct {v0}, LX/3aF;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603932
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603933
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 603934
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    .line 603935
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;
    .locals 5

    .prologue
    .line 603936
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;

    monitor-enter v1

    .line 603937
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603938
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603939
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603940
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603941
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;)V

    .line 603942
    move-object v0, p0

    .line 603943
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603944
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603945
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3Ab;)Z
    .locals 1
    .param p0    # LX/3Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 603947
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/3Ab;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/reaction/feed/rows/ui/ReactionIconMenuHeaderView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 603948
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 603949
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603950
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603951
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 603952
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 603953
    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->c:Lcom/facebook/reaction/feed/rows/subparts/BasicReactionMenuHeaderPartDefinition;

    new-instance v4, LX/E2s;

    invoke-direct {v4, v0, v1, v2}, LX/E2s;-><init>(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603954
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 603955
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v2, LX/E1o;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v0, v4, v5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603956
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 603957
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 603958
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    .line 603959
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-interface {v1}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 603960
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->a(LX/3Ab;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->a(LX/3Ab;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 603961
    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gY_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gY_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
