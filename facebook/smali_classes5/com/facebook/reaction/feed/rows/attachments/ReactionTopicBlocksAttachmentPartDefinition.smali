.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final f:LX/E1i;

.field private final g:Landroid/text/style/StyleSpan;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605581
    const v0, 0x7f03115e

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/E1i;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605582
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605583
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->b:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    .line 605584
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    .line 605585
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 605586
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 605587
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->f:LX/E1i;

    .line 605588
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->g:Landroid/text/style/StyleSpan;

    .line 605589
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 605590
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;

    monitor-enter v1

    .line 605591
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605592
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605593
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605594
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605595
    new-instance v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v8

    check-cast v8, LX/E1i;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;LX/E1i;)V

    .line 605596
    move-object v0, v3

    .line 605597
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605598
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605599
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605601
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 605602
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    const/4 v7, 0x0

    .line 605603
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-result-object v0

    .line 605604
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 605605
    new-instance v2, Landroid/text/SpannableStringBuilder;

    const-string v3, "%s: %s"

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->gU_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 605606
    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->g:Landroid/text/style/StyleSpan;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/16 v5, 0x11

    invoke-virtual {v2, v3, v4, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 605607
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 605608
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->b:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    new-instance v3, LX/E2o;

    iget-object v4, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->f:LX/E1i;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/E1i;->f(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v4

    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->b:Ljava/lang/String;

    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, LX/E2o;-><init>(LX/Cfl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605609
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605610
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605611
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v0, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605612
    return-object v7
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6c20ed2b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605613
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 605614
    const v1, 0x7f0e0906

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 605615
    const v1, 0x7f0a010a

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 605616
    sget-object v1, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 605617
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 605618
    const/16 v1, 0x1f

    const v2, 0x17d7242

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 605619
    check-cast p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 605620
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-result-object v0

    .line 605621
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->gU_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
