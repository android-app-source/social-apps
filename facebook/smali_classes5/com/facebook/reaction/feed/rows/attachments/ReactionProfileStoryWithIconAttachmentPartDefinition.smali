.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final d:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 604956
    const v0, 0x7f031160

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->a:LX/1Cz;

    .line 604957
    const-class v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    const-string v1, "reaction_dialog"

    const-string v2, "attachment_icon"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604958
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604959
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 604960
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->d:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    .line 604961
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 604962
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    monitor-enter v1

    .line 604963
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604964
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604965
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604966
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604967
    new-instance p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;)V

    .line 604968
    move-object v0, p0

    .line 604969
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604970
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604971
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 604973
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 604974
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 604975
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 604976
    const v1, 0x7f0d2885

    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v0

    sget-object v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 604977
    iput-object v3, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 604978
    move-object v0, v0

    .line 604979
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604980
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->d:Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryImageBlockPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 604981
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 604982
    check-cast p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    const/4 v1, 0x0

    .line 604983
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->W()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v0

    .line 604984
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->au_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->Y()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
