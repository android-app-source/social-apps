.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 605169
    const v0, 0x7f031161

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605166
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605167
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    .line 605168
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 605155
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;

    monitor-enter v1

    .line 605156
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605157
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605158
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605159
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605160
    new-instance p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;)V

    .line 605161
    move-object v0, p0

    .line 605162
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605163
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605164
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605147
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 605151
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 605152
    const v0, 0x7f0d2887

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    new-instance v2, LX/E2x;

    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0e08f8

    const v6, 0x7f0e08f9

    invoke-direct {v2, v3, v4, v5, v6}, LX/E2x;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605153
    const v0, 0x7f0d2889

    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->b:Lcom/facebook/reaction/feed/rows/subparts/ReactionTitleAndLabelViewPartDefinition;

    new-instance v2, LX/E2x;

    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0e08f8

    const v6, 0x7f0e08f9

    invoke-direct {v2, v3, v4, v5, v6}, LX/E2x;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 605154
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 605148
    check-cast p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 605149
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 605150
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
