.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2kp;",
        ":",
        "LX/3U9;",
        ":",
        "LX/3Tw;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "LX/E2p;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 602928
    new-instance v0, LX/3a7;

    invoke-direct {v0}, LX/3a7;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602929
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 602930
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;
    .locals 3

    .prologue
    .line 602931
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    monitor-enter v1

    .line 602932
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 602933
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 602934
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602935
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 602936
    new-instance v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;

    invoke-direct {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;-><init>()V

    .line 602937
    move-object v0, v0

    .line 602938
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 602939
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602940
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 602941
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 602942
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 602943
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 602944
    new-instance v0, LX/E2p;

    .line 602945
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionCardNode;->b:LX/Cfx;

    move-object v1, v1

    .line 602946
    iget-object v2, v1, LX/Cfx;->a:LX/Cfk;

    move-object v1, v2

    .line 602947
    invoke-direct {v0, v1}, LX/E2p;-><init>(LX/Cfk;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7ad629d2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 602948
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p2, LX/E2p;

    check-cast p3, LX/2kp;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 602949
    iget-object v4, p2, LX/E2p;->a:LX/Cfk;

    move-object v5, p3

    check-cast v5, LX/3U9;

    invoke-interface {v5}, LX/3U9;->n()LX/2ja;

    move-result-object v5

    move-object v6, p3

    check-cast v6, LX/3Tw;

    invoke-interface {v6}, LX/3Tw;->u()LX/0o8;

    move-result-object v7

    invoke-interface {p3}, LX/2kp;->t()LX/2jY;

    move-result-object v6

    .line 602950
    iget-object v8, v6, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v8

    .line 602951
    invoke-interface {p3}, LX/2kp;->t()LX/2jY;

    move-result-object v6

    .line 602952
    iget-object v9, v6, LX/2jY;->b:Ljava/lang/String;

    move-object v9, v9

    .line 602953
    const/4 v10, 0x0

    move-object v6, p4

    invoke-virtual/range {v4 .. v10}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 602954
    iget-object v4, p2, LX/E2p;->a:LX/Cfk;

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z

    .line 602955
    const/16 v1, 0x1f

    const v2, 0x49ec0df7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 602956
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 602957
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 602958
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionCardNode;->b:LX/Cfx;

    move-object v0, v0

    .line 602959
    iget-object p0, v0, LX/Cfx;->a:LX/Cfk;

    move-object v0, p0

    .line 602960
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 602961
    check-cast p4, Landroid/widget/LinearLayout;

    .line 602962
    invoke-virtual {p4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 602963
    return-void
.end method
