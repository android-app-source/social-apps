.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field public final d:LX/6RZ;

.field private final e:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

.field private final g:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

.field private final h:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603286
    const v0, 0x7f031163

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/6RZ;Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 603312
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 603313
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 603314
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 603315
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->d:LX/6RZ;

    .line 603316
    iput-object p4, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    .line 603317
    iput-object p5, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    .line 603318
    iput-object p6, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->g:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    .line 603319
    iput-object p7, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->h:LX/E1i;

    .line 603320
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 603300
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;

    monitor-enter v1

    .line 603301
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 603302
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 603303
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603304
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 603305
    new-instance v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v6

    check-cast v6, LX/6RZ;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v10

    check-cast v10, LX/E1i;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/6RZ;Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;LX/E1i;)V

    .line 603306
    move-object v0, v3

    .line 603307
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 603308
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 603309
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 603310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 603311
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 603290
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    const/4 v5, 0x0

    .line 603291
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    .line 603292
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/HighlightViewOnTouchListenerPartDefinition;

    invoke-interface {p1, v1, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603293
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {v0}, LX/7oX;->eR_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603294
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 603295
    iget-object v6, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->d:LX/6RZ;

    invoke-interface {v0}, LX/7oX;->eQ_()Z

    move-result v7

    invoke-interface {v0}, LX/7oX;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v8

    invoke-interface {v0}, LX/7oX;->b()J

    move-result-wide v10

    invoke-static {v10, v11}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v9}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 603296
    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603297
    const v1, 0x7f0d2900

    iget-object v2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->e:Lcom/facebook/reaction/feed/rows/subparts/ReactionEventRowProfilePicturePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 603298
    iget-object v1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->g:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    new-instance v2, LX/E2o;

    iget-object v3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->h:LX/E1i;

    invoke-interface {v0}, LX/7oX;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/E1i;->c(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->c:Ljava/lang/String;

    invoke-direct {v2, v0, v3, v4}, LX/E2o;-><init>(LX/Cfl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 603299
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 603287
    check-cast p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 603288
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 603289
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->eR_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->c()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
