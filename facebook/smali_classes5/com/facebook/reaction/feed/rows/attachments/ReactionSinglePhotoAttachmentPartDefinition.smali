.class public Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/3Tw;",
        ":",
        "LX/3U9;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 605387
    const-class v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;

    const-string v1, "reaction_photos"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 605388
    const v0, 0x7f031162

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605382
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605383
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->c:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    .line 605384
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 605385
    iput-object p3, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->e:LX/E1i;

    .line 605386
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 605389
    const-class v1, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 605390
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605391
    sput-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605392
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605393
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605394
    new-instance p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v5

    check-cast v5, LX/E1i;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;-><init>(Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/E1i;)V

    .line 605395
    move-object v0, p0

    .line 605396
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605397
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605398
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 605381
    sget-object v0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 605365
    check-cast p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    check-cast p3, LX/1Pn;

    .line 605366
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v1

    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v2

    invoke-interface {v2}, LX/1U8;->e()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    const v2, 0x40155555

    .line 605367
    iput v2, v1, LX/2f8;->b:F

    .line 605368
    move-object v1, v1

    .line 605369
    sget-object v2, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 605370
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 605371
    move-object v1, v1

    .line 605372
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605373
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->c:Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentActionPartDefinition;

    new-instance v1, LX/E2o;

    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    const/4 v11, 0x0

    .line 605374
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v5

    .line 605375
    :try_start_0
    invoke-interface {v5}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 605376
    invoke-interface {v5}, LX/1U8;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v10

    .line 605377
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static/range {v6 .. v10}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 605378
    :goto_0
    move-object v2, v5

    .line 605379
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionAttachmentNode;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, LX/E2o;-><init>(LX/Cfl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605380
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-object v5, v11

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 605362
    check-cast p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;

    .line 605363
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionAttachmentNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    .line 605364
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/1U8;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
