.class public Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionCardNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604441
    new-instance v0, LX/3aI;

    invoke-direct {v0}, LX/3aI;-><init>()V

    sput-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 604437
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 604438
    iput-object p1, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 604439
    iput-object p2, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 604440
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;
    .locals 5

    .prologue
    .line 604426
    const-class v1, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;

    monitor-enter v1

    .line 604427
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 604428
    sput-object v2, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604429
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604430
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 604431
    new-instance p0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 604432
    move-object v0, p0

    .line 604433
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 604434
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604435
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 604436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;ILX/1aD;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "I",
            "LX/1aD",
            "<",
            "LX/1PW;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604423
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, p2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604424
    iget-object v0, p0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v1, LX/E1o;

    invoke-direct {v1, p1, p4, p5}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, p2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 604425
    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 604414
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 604422
    sget-object v0, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 604418
    check-cast p2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 604419
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    const v2, 0x7f0d28fe

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;ILX/1aD;Ljava/lang/String;Ljava/lang/String;)V

    .line 604420
    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    const v2, 0x7f0d28ff

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;ILX/1aD;Ljava/lang/String;Ljava/lang/String;)V

    .line 604421
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 604415
    check-cast p1, Lcom/facebook/reaction/common/ReactionCardNode;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 604416
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v3

    .line 604417
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v0}, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
