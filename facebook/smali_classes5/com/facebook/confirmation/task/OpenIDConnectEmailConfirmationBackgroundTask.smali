.class public Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;
.super LX/1Eg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile n:Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/0SG;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ej3;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2U8;

.field public final h:LX/2U9;

.field private final i:LX/1Ml;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final l:LX/0Uo;

.field private final m:LX/2Du;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573992
    const-class v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    .line 573993
    sput-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0Ot;LX/0Ot;LX/2U8;LX/2U9;LX/1Ml;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uo;LX/2Du;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p8    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ej3;",
            ">;",
            "LX/2U8;",
            "LX/2U9;",
            "LX/1Ml;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Uo;",
            "LX/2Du;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574093
    const-string v0, "OPENID_CONNECT_EMAIL_CONFIRMATION_TASK"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 574094
    iput-object p1, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->c:Landroid/content/Context;

    .line 574095
    iput-object p2, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->d:LX/0SG;

    .line 574096
    iput-object p3, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->e:LX/0Ot;

    .line 574097
    iput-object p4, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->f:LX/0Ot;

    .line 574098
    iput-object p5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->g:LX/2U8;

    .line 574099
    iput-object p6, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->h:LX/2U9;

    .line 574100
    iput-object p7, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->i:LX/1Ml;

    .line 574101
    iput-object p8, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->j:LX/0Ot;

    .line 574102
    iput-object p9, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 574103
    iput-object p10, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->l:LX/0Uo;

    .line 574104
    iput-object p11, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->m:LX/2Du;

    .line 574105
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;
    .locals 15

    .prologue
    .line 574080
    sget-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->n:Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    if-nez v0, :cond_1

    .line 574081
    const-class v1, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    monitor-enter v1

    .line 574082
    :try_start_0
    sget-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->n:Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574083
    if-eqz v2, :cond_0

    .line 574084
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 574085
    new-instance v3, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0xb83

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x19f6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/2U8;->b(LX/0QB;)LX/2U8;

    move-result-object v8

    check-cast v8, LX/2U8;

    invoke-static {v0}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v9

    check-cast v9, LX/2U9;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v10

    check-cast v10, LX/1Ml;

    const/16 v11, 0x1ce

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v13

    check-cast v13, LX/0Uo;

    invoke-static {v0}, LX/2Du;->b(LX/0QB;)LX/2Du;

    move-result-object v14

    check-cast v14, LX/2Du;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;-><init>(Landroid/content/Context;LX/0SG;LX/0Ot;LX/0Ot;LX/2U8;LX/2U9;LX/1Ml;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uo;LX/2Du;)V

    .line 574086
    move-object v0, v3

    .line 574087
    sput-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->n:Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574088
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574089
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574090
    :cond_1
    sget-object v0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->n:Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    return-object v0

    .line 574091
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574092
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static k(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574079
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->g:LX/2U8;

    sget-object v1, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    invoke-virtual {v0, v1}, LX/2U8;->a(LX/2UD;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 6

    .prologue
    .line 574063
    invoke-static {p0}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 574064
    new-instance v0, LX/Ej7;

    invoke-direct {v0, p0}, LX/Ej7;-><init>(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)V

    invoke-static {v1, v0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 574065
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->i:LX/1Ml;

    const-string v3, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574066
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 574067
    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 574068
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v3, LX/Ej8;

    invoke-direct {v3, p0}, LX/Ej8;-><init>(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)V

    invoke-static {v0, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574069
    :goto_0
    iget-object v3, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->h:LX/2U9;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 574070
    iget-object v4, v3, LX/2U9;->a:LX/0Zb;

    sget-object v5, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_TASK_START:LX/Eiw;

    invoke-virtual {v5}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v5

    const/4 p0, 0x1

    invoke-interface {v4, v5, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 574071
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 574072
    const-string v5, "confirmation"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 574073
    const-string v5, "pending_contactpoint_count"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 574074
    const-string v5, "pending_contactpoints"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 574075
    const-string v5, "google_accounts"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 574076
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 574077
    :cond_0
    return-void

    .line 574078
    :cond_1
    const-string v0, "GET_ACCOUNTS_PERMISSION_NOT_AVAILABLE"

    goto :goto_0
.end method


# virtual methods
.method public final f()J
    .locals 6

    .prologue
    .line 574056
    invoke-virtual {p0}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574057
    const-wide/16 v0, -0x1

    .line 574058
    :goto_0
    return-wide v0

    .line 574059
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3df;->g:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 574060
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->l:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x14

    .line 574061
    :goto_1
    int-to-long v0, v0

    const-wide/32 v4, 0xea60

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    goto :goto_0

    .line 574062
    :cond_1
    const/4 v0, 0x5

    goto :goto_1
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574055
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 574046
    invoke-static {p0}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)Ljava/util/Map;

    move-result-object v0

    .line 574047
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 574048
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 574049
    iget-object v1, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x5265c00

    cmp-long v1, v6, v8

    if-gtz v1, :cond_0

    .line 574050
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 574051
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574052
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 574053
    iget-object v1, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->g:LX/2U8;

    sget-object v5, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    new-array v0, v3, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v1, v5, v0}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 574054
    :cond_2
    return v2
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573994
    iget-object v0, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3df;->g:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 573995
    invoke-static {p0}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k(Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 573996
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 573997
    invoke-direct {p0}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->l()V

    .line 573998
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    .line 573999
    iget-object v4, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3df;->h:LX/0Tn;

    iget-object v5, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {v3, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 574000
    add-int/lit8 v4, v3, 0x1

    .line 574001
    iget-object v3, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v3, LX/3df;->h:LX/0Tn;

    iget-object v6, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    invoke-interface {v5, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 574002
    move v3, v4

    .line 574003
    const/4 v4, 0x3

    if-gt v3, v4, :cond_1

    iget-object v3, v0, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v4, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-eq v3, v4, :cond_2

    .line 574004
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574005
    :cond_2
    iget-object v3, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->m:LX/2Du;

    iget-object v4, v0, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    const/4 v5, 0x0

    .line 574006
    iget-object v6, v3, LX/2Du;->d:LX/1Ml;

    const-string v7, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v6, v7}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 574007
    iget-object v6, v3, LX/2Du;->c:LX/27p;

    const-string v7, "GET_ACCOUNTS_PERMISSION_NOT_AVAILABLE"

    invoke-virtual {v6, v7}, LX/27p;->a(Ljava/lang/String;)V

    .line 574008
    iget-object v6, v3, LX/2Du;->c:LX/27p;

    const-string v7, "GET_ACCOUNTS_PERMISSION_NOT_AVAILABLE"

    invoke-virtual {v6, v7}, LX/27p;->b(Ljava/lang/String;)V

    .line 574009
    :goto_1
    move-object v3, v5

    .line 574010
    if-nez v3, :cond_3

    .line 574011
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574012
    :cond_3
    iget-object v4, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->m:LX/2Du;

    iget-object v5, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2Du;->b(Ljava/lang/String;)LX/4gy;

    move-result-object v4

    .line 574013
    if-nez v4, :cond_4

    .line 574014
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574015
    :cond_4
    iget-object v5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->m:LX/2Du;

    invoke-virtual {v5, v3, v4}, LX/2Du;->a(Landroid/accounts/Account;LX/4gy;)Ljava/lang/String;

    move-result-object v3

    .line 574016
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 574017
    new-instance v8, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;

    sget-object v5, LX/4gx;->ANDROID_CLIFF_CONFIRMATION:LX/4gx;

    invoke-direct {v8, v0, v3, v5, v4}, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;-><init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/4gx;LX/4gy;)V

    .line 574018
    const/4 v7, 0x0

    .line 574019
    :try_start_0
    iget-object v5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/11H;

    iget-object v6, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0e6;

    sget-object v9, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6, v8, v9}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 574020
    if-eqz v6, :cond_a

    .line 574021
    :try_start_1
    iget-object v5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->h:LX/2U9;

    .line 574022
    iget-object v7, v5, LX/2U9;->a:LX/0Zb;

    sget-object v8, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_SUCCESS:LX/Eiw;

    invoke-virtual {v8}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 574023
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 574024
    const-string v8, "confirmation"

    invoke-virtual {v7, v8}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 574025
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 574026
    :cond_5
    new-instance v5, Landroid/content/Intent;

    const-string v7, "action_background_contactpoint_confirmed"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "extra_background_confirmed_contactpoint"

    invoke-virtual {v5, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v7

    .line 574027
    iget-object v5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-interface {v5, v7}, LX/0Xl;->a(Landroid/content/Intent;)V

    move v5, v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 574028
    :goto_2
    move v3, v5

    .line 574029
    if-eqz v3, :cond_0

    .line 574030
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 574031
    :cond_6
    iget-object v2, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->g:LX/2U8;

    sget-object v3, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v2, v3, v0}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 574032
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 574033
    :cond_7
    iget-object v6, v3, LX/2Du;->b:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    .line 574034
    const-string v7, "com.google"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 574035
    array-length v9, v8

    const/4 v6, 0x0

    move v7, v6

    :goto_3
    if-ge v7, v9, :cond_9

    aget-object v6, v8, v7

    .line 574036
    iget-object v10, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    move-object v5, v6

    .line 574037
    goto/16 :goto_1

    .line 574038
    :cond_8
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_3

    .line 574039
    :cond_9
    iget-object v6, v3, LX/2Du;->c:LX/27p;

    const-string v7, "NO_ACCOUNT_IN_DEVICE"

    invoke-virtual {v6, v7}, LX/27p;->a(Ljava/lang/String;)V

    .line 574040
    iget-object v6, v3, LX/2Du;->c:LX/27p;

    const-string v7, "NO_ACCOUNT_IN_DEVICE"

    invoke-virtual {v6, v7}, LX/27p;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 574041
    :cond_a
    :try_start_2
    iget-object v5, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->h:LX/2U9;

    const-string v7, "CONFIRM_EMAIL_METHOD_FAILED"

    invoke-virtual {v5, v7}, LX/2U9;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move v5, v6

    .line 574042
    goto :goto_2

    .line 574043
    :catch_0
    move-exception v5

    move-object v6, v5

    move v5, v7

    .line 574044
    :goto_4
    iget-object v7, p0, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->h:LX/2U9;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Confirm email method exception: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/2U9;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 574045
    :catch_1
    move-exception v5

    move-object v10, v5

    move v5, v6

    move-object v6, v10

    goto :goto_4
.end method
