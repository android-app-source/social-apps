.class public Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;
.super LX/1Eg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/2UD;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile p:Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;


# instance fields
.field private final c:LX/0Uo;

.field public final d:LX/2U8;

.field private final e:LX/0SG;

.field public final f:LX/2UE;

.field private final g:LX/1Ml;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eiz;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U9;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Uh;

.field private final m:LX/0W3;

.field private n:I

.field public final o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574308
    sget-object v0, LX/2UD;->EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

    sput-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a:LX/2UD;

    .line 574309
    const-class v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Uo;LX/2U8;LX/0SG;LX/2UE;LX/1Ml;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/0W3;)V
    .locals 1
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "LX/2U8;",
            "LX/0SG;",
            "LX/2UE;",
            "LX/1Ml;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Eiz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2U9;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574293
    const-string v0, "SMS_CONFIRMATION_READER"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 574294
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->n:I

    .line 574295
    iput-object p1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->c:LX/0Uo;

    .line 574296
    iput-object p2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    .line 574297
    iput-object p3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->e:LX/0SG;

    .line 574298
    iput-object p4, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->f:LX/2UE;

    .line 574299
    iput-object p5, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->g:LX/1Ml;

    .line 574300
    iput-object p6, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->h:LX/0Ot;

    .line 574301
    iput-object p7, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->i:LX/0Ot;

    .line 574302
    iput-object p8, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    .line 574303
    iput-object p9, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->k:LX/0Ot;

    .line 574304
    iput-object p10, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->l:LX/0Uh;

    .line 574305
    iput-object p11, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->m:LX/0W3;

    .line 574306
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    .line 574307
    return-void
.end method

.method public static a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/EiF;)LX/03R;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 574268
    iget v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->n:I

    if-lez v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->n:I

    if-eq v0, v1, :cond_1

    .line 574269
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 574270
    :cond_0
    :goto_0
    return-object v1

    .line 574271
    :cond_1
    new-instance v4, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;

    sget-object v0, LX/Ej0;->ANDROID_AUTO_SMS_API:LX/Ej0;

    const-string v1, "auto_confirmation"

    invoke-direct {v4, p1, p2, v0, v1}, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;-><init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/Ej0;Ljava/lang/String;)V

    .line 574272
    sget-object v2, LX/03R;->NO:LX/03R;

    .line 574273
    const/4 v3, 0x0

    .line 574274
    :try_start_0
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    sget-object v5, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 574275
    sget-object v0, LX/03R;->YES:LX/03R;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    move-object v1, v0

    .line 574276
    :cond_2
    :goto_2
    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne v1, v0, :cond_0

    .line 574277
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U9;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2U9;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 574278
    :catch_0
    move-exception v0

    .line 574279
    :try_start_1
    invoke-static {v0}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    .line 574280
    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v1, v0}, LX/2U8;->a(Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;

    move-result-object v1

    .line 574281
    invoke-static {v1, v0}, LX/2U8;->a(LX/3rL;Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;

    move-result-object v1

    .line 574282
    iget-object v0, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574283
    :try_start_2
    iget-object v1, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, LX/03R;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574284
    if-eqz v0, :cond_2

    .line 574285
    iget-object v2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 574286
    iget-object v2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574287
    :cond_3
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 574288
    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_5

    .line 574289
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 574290
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574291
    :cond_4
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    throw v1

    .line 574292
    :catchall_1
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;
    .locals 15

    .prologue
    .line 574255
    sget-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->p:Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    if-nez v0, :cond_1

    .line 574256
    const-class v1, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    monitor-enter v1

    .line 574257
    :try_start_0
    sget-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->p:Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574258
    if-eqz v2, :cond_0

    .line 574259
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 574260
    new-instance v3, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/2U8;->b(LX/0QB;)LX/2U8;

    move-result-object v5

    check-cast v5, LX/2U8;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/2UE;->c(LX/0QB;)LX/2UE;

    move-result-object v7

    check-cast v7, LX/2UE;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    const/16 v9, 0x1ce

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x19f4

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3e8

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xb83

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v14

    check-cast v14, LX/0W3;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;-><init>(LX/0Uo;LX/2U8;LX/0SG;LX/2UE;LX/1Ml;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/0W3;)V

    .line 574261
    move-object v0, v3

    .line 574262
    sput-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->p:Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574263
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574264
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574265
    :cond_1
    sget-object v0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->p:Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    return-object v0

    .line 574266
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Ljava/util/Set;Lcom/facebook/growth/model/Contactpoint;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            ">;",
            "Lcom/facebook/growth/model/Contactpoint;",
            ")V"
        }
    .end annotation

    .prologue
    .line 574310
    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    sget-object v2, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a:LX/2UD;

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v1, v2, v0}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 574311
    new-instance v0, Landroid/content/Intent;

    const-string v1, "action_background_contactpoint_confirmed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "extra_background_confirmed_contactpoint"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 574312
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 574313
    return-void
.end method

.method public static l(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574254
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    sget-object v1, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a:LX/2UD;

    invoke-virtual {v0, v1}, LX/2U8;->a(LX/2UD;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final f()J
    .locals 4

    .prologue
    .line 574247
    invoke-virtual {p0}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574248
    const-wide/16 v0, -0x1

    .line 574249
    :goto_0
    return-wide v0

    .line 574250
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574251
    const-wide/32 v0, 0x124f80

    .line 574252
    :goto_1
    iget-object v2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 574253
    :cond_1
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v0}, LX/2U8;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    const-wide/16 v2, 0xfa

    mul-long/2addr v0, v2

    goto :goto_1
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574246
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 574237
    invoke-static {p0}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->l(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;)Ljava/util/Map;

    move-result-object v0

    .line 574238
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 574239
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 574240
    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x240c8400

    cmp-long v1, v6, v8

    if-gtz v1, :cond_0

    .line 574241
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 574242
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574243
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 574244
    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    sget-object v5, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a:LX/2UD;

    new-array v0, v3, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v1, v5, v0}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 574245
    :cond_2
    return v2
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 574151
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->g:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    .line 574152
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U9;

    invoke-virtual {v0, v1}, LX/2U9;->a(Z)V

    .line 574153
    if-eqz v1, :cond_1

    .line 574154
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->l:LX/0Uh;

    const/16 v1, 0x384

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574155
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->m:LX/0W3;

    sget-wide v2, LX/0X5;->hU:J

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->n:I

    .line 574156
    :cond_0
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v1}, LX/2U8;->e()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LX/2U8;->a(I)V

    .line 574157
    invoke-static {p0}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->l(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;)Ljava/util/Map;

    move-result-object v0

    .line 574158
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 574159
    :cond_1
    :goto_0
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 574160
    :cond_2
    iget-object v1, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    const/4 v3, 0x0

    .line 574161
    iget-object v2, v1, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3df;->e:LX/0Tn;

    invoke-interface {v2, v4, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 574162
    :try_start_0
    iget-object v4, v1, LX/2U8;->d:LX/0lC;

    const-class v5, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;

    invoke-virtual {v4, v2, v5}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 574163
    :goto_1
    move-object v1, v2

    .line 574164
    iget-object v2, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->f:LX/2UE;

    .line 574165
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 574166
    invoke-static {v2, v1}, LX/2UF;->a(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 574167
    invoke-static {v2, v1}, LX/2UF;->c(LX/2UF;Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 574168
    move-object v3, v3

    .line 574169
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v2, v4}, LX/2UE;->a(LX/2UE;I)V

    .line 574170
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 574171
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 574172
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 574173
    invoke-static {v3}, LX/2UE;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v9

    .line 574174
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EiD;

    .line 574175
    iget-object v4, v3, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v8, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, v3, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move v5, v4

    .line 574176
    :goto_2
    const/4 v4, 0x5

    if-ge v5, v4, :cond_3

    .line 574177
    iget-object v4, v3, LX/EiD;->d:Ljava/lang/String;

    invoke-static {v4}, LX/2UE;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 574178
    if-eqz v11, :cond_3

    .line 574179
    invoke-interface {v7, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 574180
    invoke-interface {v7, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 574181
    sget-object v4, LX/EiF;->NORMAL:LX/EiF;

    .line 574182
    iget-boolean v12, v3, LX/EiD;->e:Z

    if-nez v12, :cond_4

    iget-object v12, v3, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v9, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 574183
    :cond_4
    iget-boolean v4, v3, LX/EiD;->e:Z

    if-eqz v4, :cond_a

    sget-object v4, LX/EiF;->PRIORITY_FB_TOKEN_1:LX/EiF;

    .line 574184
    :cond_5
    :goto_3
    new-instance v12, LX/EiE;

    invoke-direct {v12, v11, v4}, LX/EiE;-><init>(Ljava/lang/String;LX/EiF;)V

    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 574185
    iget-object v3, v3, LX/EiD;->c:Ljava/lang/String;

    add-int/lit8 v4, v5, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v8, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574186
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x64

    if-le v3, v4, :cond_3

    .line 574187
    :cond_6
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 574188
    move-object v2, v6

    .line 574189
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v6, 0x1

    .line 574190
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    .line 574191
    sget-object v4, LX/3df;->c:LX/0Tn;

    invoke-static {v3, v4}, LX/2U8;->a(LX/2U8;LX/0Tn;)Ljava/util/Set;

    move-result-object v4

    move-object v7, v4

    .line 574192
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    :cond_7
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 574193
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Z

    goto/16 :goto_0

    .line 574194
    :catch_0
    move-exception v2

    .line 574195
    sget-object v4, LX/2U8;->a:Ljava/lang/Class;

    const-string v5, "Error with parsing sms reader pointer data"

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    .line 574196
    goto/16 :goto_1

    .line 574197
    :cond_9
    const/4 v4, 0x0

    move v5, v4

    goto :goto_2

    .line 574198
    :cond_a
    sget-object v4, LX/EiF;->PRIORITY_FB_TOKEN_2:LX/EiF;

    goto :goto_3

    .line 574199
    :cond_b
    sget-object v12, LX/2UE;->a:Ljava/util/Set;

    iget-object v13, v3, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 574200
    sget-object v4, LX/EiF;->PRIORITY_SENDER:LX/EiF;

    goto :goto_3

    .line 574201
    :cond_c
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2U9;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    add-int/2addr v5, v8

    invoke-virtual {v3, v4, v5}, LX/2U9;->a(II)V

    .line 574202
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    .line 574203
    sget-object v4, LX/3df;->b:LX/0Tn;

    invoke-static {v3, v4}, LX/2U8;->a(LX/2U8;LX/0Tn;)Ljava/util/Set;

    move-result-object v4

    move-object v8, v4

    .line 574204
    new-instance v9, Ljava/util/LinkedHashSet;

    invoke-direct {v9}, Ljava/util/LinkedHashSet;-><init>()V

    .line 574205
    const/4 v3, 0x0

    .line 574206
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v4, v3

    :cond_d
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EiE;

    .line 574207
    iget-object v5, v3, LX/EiE;->a:Ljava/lang/String;

    invoke-interface {v8, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 574208
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v5, v4

    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/growth/model/Contactpoint;

    .line 574209
    iget-object v12, v3, LX/EiE;->a:Ljava/lang/String;

    iget-object v13, v3, LX/EiE;->b:LX/EiF;

    invoke-static {p0, v4, v12, v13}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/EiF;)LX/03R;

    move-result-object v12

    .line 574210
    sget-object v13, LX/03R;->YES:LX/03R;

    if-ne v12, v13, :cond_e

    .line 574211
    invoke-static {p0, v0, v4}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Ljava/util/Set;Lcom/facebook/growth/model/Contactpoint;)V

    goto/16 :goto_4

    .line 574212
    :cond_e
    sget-object v4, LX/03R;->UNSET:LX/03R;

    if-ne v12, v4, :cond_f

    .line 574213
    iget-object v4, v3, LX/EiE;->a:Ljava/lang/String;

    invoke-interface {v9, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v5, v6

    goto :goto_6

    .line 574214
    :cond_f
    iget-object v4, v3, LX/EiE;->a:Ljava/lang/String;

    invoke-interface {v8, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_10
    move v4, v5

    .line 574215
    goto :goto_5

    .line 574216
    :cond_11
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_12
    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 574217
    invoke-interface {v8, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 574218
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v4

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/growth/model/Contactpoint;

    .line 574219
    sget-object v11, LX/EiF;->RETRY:LX/EiF;

    invoke-static {p0, v4, v3, v11}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/EiF;)LX/03R;

    move-result-object v11

    .line 574220
    sget-object v12, LX/03R;->YES:LX/03R;

    if-ne v11, v12, :cond_13

    .line 574221
    invoke-static {p0, v0, v4}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a(Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;Ljava/util/Set;Lcom/facebook/growth/model/Contactpoint;)V

    goto/16 :goto_4

    .line 574222
    :cond_13
    sget-object v4, LX/03R;->UNSET:LX/03R;

    if-ne v11, v4, :cond_14

    .line 574223
    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v5, v6

    goto :goto_8

    .line 574224
    :cond_14
    invoke-interface {v8, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_15
    move v4, v5

    .line 574225
    goto :goto_7

    .line 574226
    :cond_16
    if-eqz v4, :cond_17

    .line 574227
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2U9;

    .line 574228
    sget-object v4, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_NETWORK:LX/Eiw;

    .line 574229
    iget-object v5, v3, LX/2U9;->a:LX/0Zb;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v5, v4, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 574230
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 574231
    const-string v5, "confirmation"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 574232
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 574233
    :cond_17
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2U9;

    iget-object v4, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, LX/2U9;->a(Ljava/util/HashMap;)V

    .line 574234
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->o:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 574235
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v3, v9}, LX/2U8;->b(Ljava/util/Set;)Z

    .line 574236
    iget-object v3, p0, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->d:LX/2U8;

    invoke-virtual {v3, v8}, LX/2U8;->a(Ljava/util/Set;)Z

    goto/16 :goto_4
.end method
