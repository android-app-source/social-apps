.class public Lcom/facebook/confirmation/model/AccountConfirmationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/confirmation/model/AccountConfirmationData;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/growth/model/Contactpoint;

.field public b:Z

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574106
    new-instance v0, LX/2UA;

    invoke-direct {v0}, LX/2UA;-><init>()V

    sput-object v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 574107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 574109
    iput-boolean v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    .line 574110
    iput-boolean v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    .line 574111
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    .line 574112
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    .line 574113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    .line 574114
    iput-boolean v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->g:Z

    .line 574115
    iput-boolean v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->h:Z

    .line 574116
    iput-boolean v1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->i:Z

    .line 574117
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 574118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574119
    const-class v0, Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 574120
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    .line 574121
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    .line 574122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    .line 574123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    .line 574124
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    .line 574125
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->g:Z

    .line 574126
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->h:Z

    .line 574127
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->i:Z

    .line 574128
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/confirmation/model/AccountConfirmationData;
    .locals 3

    .prologue
    .line 574129
    const-class v1, Lcom/facebook/confirmation/model/AccountConfirmationData;

    monitor-enter v1

    .line 574130
    :try_start_0
    sget-object v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 574131
    sput-object v2, Lcom/facebook/confirmation/model/AccountConfirmationData;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 574132
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574133
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 574134
    new-instance v0, Lcom/facebook/confirmation/model/AccountConfirmationData;

    invoke-direct {v0}, Lcom/facebook/confirmation/model/AccountConfirmationData;-><init>()V

    .line 574135
    move-object v0, v0

    .line 574136
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 574137
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/confirmation/model/AccountConfirmationData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574138
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 574139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/growth/model/Contactpoint;)V
    .locals 1

    .prologue
    .line 574140
    invoke-virtual {p1}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574141
    iput-object p1, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    .line 574142
    :cond_0
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 574143
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 574144
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 574145
    iget-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 574146
    iget-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 574147
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 574148
    iget-object v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 574149
    iget-boolean v0, p0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 574150
    return-void
.end method
