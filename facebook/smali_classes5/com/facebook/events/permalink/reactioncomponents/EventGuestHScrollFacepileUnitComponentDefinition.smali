.class public Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2kp;",
        ":",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/facepile/FacepileView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

.field private final c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586475
    new-instance v0, LX/3Ua;

    invoke-direct {v0}, LX/3Ua;-><init>()V

    sput-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586476
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586477
    iput-object p1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->b:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    .line 586478
    iput-object p2, p0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 586479
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;
    .locals 5

    .prologue
    .line 586480
    const-class v1, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;

    monitor-enter v1

    .line 586481
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586482
    sput-object v2, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586483
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586484
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586485
    new-instance p0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;-><init>(Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 586486
    move-object v0, p0

    .line 586487
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586488
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586489
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586491
    sget-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 586492
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2kp;

    .line 586493
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 586494
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 586495
    iget-object v1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->b:Lcom/facebook/reaction/feed/common/ReactionFacepilePartDefinition;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 586496
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 586497
    invoke-interface {v3}, LX/9uc;->A()LX/0Px;

    move-result-object v3

    .line 586498
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 586499
    const/16 v4, 0x8

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 586500
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    .line 586501
    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$BadgableProfilesModel;

    .line 586502
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$BadgableProfilesModel;->b()LX/5sX;

    move-result-object v8

    .line 586503
    new-instance v9, LX/6UY;

    invoke-interface {v8}, LX/5sX;->d()LX/5sY;

    move-result-object v8

    invoke-interface {v8}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$BadgableProfilesModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;

    move-result-object v4

    .line 586504
    sget-object v10, LX/IBW;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionProfileBadgeType;->ordinal()I

    move-result p3

    aget v10, v10, p3

    packed-switch v10, :pswitch_data_0

    .line 586505
    const/4 v10, 0x0

    :goto_1
    move-object v4, v10

    .line 586506
    invoke-direct {v9, v8, v4}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 586507
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 586508
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 586509
    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586510
    if-eqz v0, :cond_1

    .line 586511
    iget-object v1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventGuestHScrollFacepileUnitComponentDefinition;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v2, LX/E1o;

    .line 586512
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v3

    .line 586513
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v4

    .line 586514
    invoke-direct {v2, v0, v3, v4}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586515
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 586516
    :pswitch_0
    const v10, 0x7f020118

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    goto :goto_1

    .line 586517
    :pswitch_1
    const v10, 0x7f020119

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    goto :goto_1

    .line 586518
    :pswitch_2
    const v10, 0x7f02011a

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 586519
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 586520
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 586521
    invoke-interface {v0}, LX/9uc;->A()LX/0Px;

    move-result-object v0

    const/4 p0, 0x0

    .line 586522
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 586523
    :cond_0
    :goto_0
    move v0, p0

    .line 586524
    return v0

    :cond_1
    move v2, p0

    .line 586525
    :goto_1
    const/16 v1, 0x8

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 586526
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$BadgableProfilesModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitBadgedProfilesComponentFragmentModel$BadgableProfilesModel;->b()LX/5sX;

    move-result-object v1

    .line 586527
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/5sX;->d()LX/5sY;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {v1}, LX/5sX;->d()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 586528
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 586529
    :cond_2
    const/4 p0, 0x1

    goto :goto_0
.end method
