.class public Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/IBV;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/8tu;

.field public final d:LX/1Uf;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586404
    new-instance v0, LX/3UZ;

    invoke-direct {v0}, LX/3UZ;-><init>()V

    sput-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/8tu;LX/1Uf;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586375
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586376
    iput-object p1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->b:LX/0Zb;

    .line 586377
    iput-object p2, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->c:LX/8tu;

    .line 586378
    iput-object p3, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->d:LX/1Uf;

    .line 586379
    const v0, 0x7f081ee4

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->e:Ljava/lang/String;

    .line 586380
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;
    .locals 7

    .prologue
    .line 586393
    const-class v1, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;

    monitor-enter v1

    .line 586394
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586395
    sput-object v2, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586398
    new-instance p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v4

    check-cast v4, LX/8tu;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;-><init>(LX/0Zb;LX/8tu;LX/1Uf;Landroid/content/res/Resources;)V

    .line 586399
    move-object v0, p0

    .line 586400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586392
    sget-object v0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 586405
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 586406
    iget-object v0, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->d:LX/1Uf;

    .line 586407
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 586408
    invoke-interface {v1}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-static {v1}, LX/9JZ;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v1

    .line 586409
    new-instance v2, LX/IBT;

    .line 586410
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 586411
    invoke-interface {v0}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/IBT;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 586412
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bt7;

    .line 586413
    new-instance v2, LX/IBV;

    .line 586414
    iget-boolean v3, v0, LX/Bt7;->a:Z

    move v3, v3

    .line 586415
    if-eqz v3, :cond_1

    .line 586416
    :cond_0
    :goto_0
    move-object v1, v1

    .line 586417
    invoke-direct {v2, v0, v1}, LX/IBV;-><init>(LX/Bt7;Landroid/text/Spannable;)V

    return-object v2

    .line 586418
    :cond_1
    iget-object v3, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->d:LX/1Uf;

    new-instance v4, LX/IBU;

    invoke-direct {v4, p0, v0, p3, p2}, LX/IBU;-><init>(Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;LX/Bt7;LX/1Pn;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    iget-object p1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->e:Ljava/lang/String;

    invoke-virtual {v3, v1, v4, p1}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v3

    .line 586419
    if-eqz v3, :cond_0

    move-object v1, v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x60745d99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586388
    check-cast p2, LX/IBV;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 586389
    iget-object v1, p0, Lcom/facebook/events/permalink/reactioncomponents/EventDetailsUnitComponentPartDefinition;->c:LX/8tu;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 586390
    iget-object v1, p2, LX/IBV;->b:Landroid/text/Spannable;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 586391
    const/16 v1, 0x1f

    const v2, -0x50e165b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586384
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 586385
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 586386
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    .line 586387
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586381
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 586382
    const-string v0, ""

    invoke-virtual {p4, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 586383
    return-void
.end method
