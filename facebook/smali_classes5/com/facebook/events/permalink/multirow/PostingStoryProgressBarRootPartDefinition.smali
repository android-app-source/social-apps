.class public Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586532
    const v0, 0x7f0304ff

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586535
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586536
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;
    .locals 3

    .prologue
    .line 586537
    const-class v1, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;

    monitor-enter v1

    .line 586538
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586539
    sput-object v2, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586540
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586541
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 586542
    new-instance v0, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;

    invoke-direct {v0}, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;-><init>()V

    .line 586543
    move-object v0, v0

    .line 586544
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586545
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586546
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586534
    sget-object v0, Lcom/facebook/events/permalink/multirow/PostingStoryProgressBarRootPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586533
    sget-object v0, LX/I8W;->a:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
