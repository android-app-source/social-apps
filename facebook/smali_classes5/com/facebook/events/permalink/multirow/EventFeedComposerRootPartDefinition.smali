.class public Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/events/model/Event;",
        "LX/IBC;",
        "LX/IB8;",
        "Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final c:LX/1Ad;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 586473
    const-class v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    const-string v1, "event_permalink"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 586474
    const v0, 0x7f0304cc

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586468
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586469
    iput-object p2, p0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->d:LX/0Or;

    .line 586470
    iput-object p3, p0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->e:LX/0Or;

    .line 586471
    iput-object p1, p0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->c:LX/1Ad;

    .line 586472
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;
    .locals 6

    .prologue
    .line 586457
    const-class v1, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    monitor-enter v1

    .line 586458
    :try_start_0
    sget-object v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586459
    sput-object v2, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586460
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586461
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586462
    new-instance v4, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    const/16 v5, 0x12cb

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0xbc6

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;-><init>(LX/1Ad;LX/0Or;LX/0Or;)V

    .line 586463
    move-object v0, v4

    .line 586464
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586465
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586466
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586456
    sget-object v0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 586442
    check-cast p2, Lcom/facebook/events/model/Event;

    check-cast p3, LX/IB8;

    .line 586443
    new-instance v0, LX/IBC;

    iget-object v1, p0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->c:LX/1Ad;

    sget-object v2, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    .line 586444
    iget-object v2, p2, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v2, v2

    .line 586445
    if-eqz v2, :cond_1

    .line 586446
    iget-object v2, p2, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v2, v2

    .line 586447
    :goto_0
    move-object v2, v2

    .line 586448
    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 586449
    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 586450
    if-nez p2, :cond_2

    .line 586451
    const/4 v2, 0x0

    .line 586452
    :goto_2
    move-object v2, v2

    .line 586453
    new-instance v3, LX/IBA;

    invoke-direct {v3, p0, p3, p2}, LX/IBA;-><init>(Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;LX/IB8;Lcom/facebook/events/model/Event;)V

    move-object v3, v3

    .line 586454
    new-instance v4, LX/IBB;

    invoke-direct {v4, p0, p3, p2}, LX/IBB;-><init>(Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;LX/IB8;Lcom/facebook/events/model/Event;)V

    move-object v4, v4

    .line 586455
    invoke-direct {v0, v1, v2, v3, v4}, LX/IBC;-><init>(LX/1aZ;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    new-instance v2, LX/IB9;

    invoke-direct {v2, p0, p2}, LX/IB9;-><init>(Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;Lcom/facebook/events/model/Event;)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x59d43a9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586434
    check-cast p2, LX/IBC;

    check-cast p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;

    .line 586435
    iget-object v1, p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v1

    .line 586436
    iget-object v2, p2, LX/IBC;->a:LX/1aZ;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 586437
    iget-object v2, p2, LX/IBC;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586438
    iget-object v1, p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->c:Lcom/facebook/fbui/glyph/GlyphView;

    move-object v1, v1

    .line 586439
    iget-object v2, p2, LX/IBC;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586440
    iget-object v1, p2, LX/IBC;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586441
    const/16 v1, 0x1f

    const v2, -0x4c15d9eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586424
    check-cast p1, Lcom/facebook/events/model/Event;

    .line 586425
    sget-object v0, LX/7vK;->POST:LX/7vK;

    invoke-virtual {p1, v0}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586426
    check-cast p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;

    const/4 v1, 0x0

    .line 586427
    iget-object v0, p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 586428
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 586429
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586430
    iget-object v0, p4, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->c:Lcom/facebook/fbui/glyph/GlyphView;

    move-object v0, v0

    .line 586431
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586432
    invoke-virtual {p4, v1}, Lcom/facebook/events/permalink/composeractionbar/EventFeedComposer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586433
    return-void
.end method
