.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/events/model/Event;",
        "LX/I2U;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Lcom/facebook/events/dashboard/EventsDashboardRowView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/I76;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586165
    const v0, 0x7f030568

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/I76;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586162
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586163
    iput-object p1, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->b:LX/I76;

    .line 586164
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;
    .locals 4

    .prologue
    .line 586151
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;

    monitor-enter v1

    .line 586152
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586153
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586154
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586155
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586156
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;

    invoke-static {v0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v3

    check-cast v3, LX/I76;

    invoke-direct {p0, v3}, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;-><init>(LX/I76;)V

    .line 586157
    move-object v0, p0

    .line 586158
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586159
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586160
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586161
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/events/dashboard/EventsDashboardRowView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586166
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 586147
    check-cast p3, LX/I2o;

    .line 586148
    invoke-interface {p3}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v0

    .line 586149
    new-instance v1, LX/I2T;

    invoke-direct {v1, p0, v0}, LX/I2T;-><init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 586150
    new-instance v0, LX/I2U;

    invoke-direct {v0, p0, v1}, LX/I2U;-><init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x26c917de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586143
    check-cast p1, Lcom/facebook/events/model/Event;

    check-cast p2, LX/I2U;

    check-cast p3, LX/I2o;

    check-cast p4, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    const/4 v6, 0x0

    .line 586144
    invoke-interface {p3}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v7

    invoke-interface {p3}, LX/I2k;->w()LX/Hx6;

    move-result-object v8

    const/4 v10, 0x1

    move-object v4, p4

    move-object v5, p1

    move v9, v6

    invoke-virtual/range {v4 .. v10}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/model/Event;ZLcom/facebook/events/common/EventAnalyticsParams;LX/Hx6;ZZ)V

    .line 586145
    iget-object v4, p2, LX/I2U;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v4}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586146
    const/16 v1, 0x1f

    const v2, 0x3f43e421

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586142
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586139
    check-cast p4, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    .line 586140
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586141
    return-void
.end method
