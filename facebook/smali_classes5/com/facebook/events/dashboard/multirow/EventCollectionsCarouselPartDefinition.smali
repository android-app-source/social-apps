.class public Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586049
    new-instance v0, LX/3US;

    invoke-direct {v0}, LX/3US;-><init>()V

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586050
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586051
    iput-object p2, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    .line 586052
    iput-object p1, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    .line 586053
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;
    .locals 5

    .prologue
    .line 586054
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;

    monitor-enter v1

    .line 586055
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586056
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586057
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586058
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586059
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;

    invoke-static {v0}, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;-><init>(Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;)V

    .line 586060
    move-object v0, p0

    .line 586061
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586062
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586063
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586065
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 586066
    check-cast p2, LX/0Px;

    check-cast p3, LX/I2o;

    .line 586067
    invoke-static {}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->b()LX/99l;

    move-result-object v4

    .line 586068
    const v0, 0x7f0d0dbd

    iget-object v1, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    new-instance v2, LX/I2L;

    invoke-direct {v2, p2, v4}, LX/I2L;-><init>(LX/0Px;LX/99l;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 586069
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 586070
    const v6, 0x7f0d0dbe

    iget-object v7, p0, Lcom/facebook/events/dashboard/multirow/EventCollectionsCarouselPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PagerIndicatorPartDefinition;

    new-instance v0, LX/99m;

    const-string v1, "event_collection_carousel_key"

    const/4 v2, 0x0

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    invoke-static {}, LX/99o;->a()LX/99o;

    move-result-object v8

    const v9, 0x7f0a004f

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 586071
    iput v9, v8, LX/99o;->d:I

    .line 586072
    move-object v8, v8

    .line 586073
    const v9, 0x7f0a0097

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 586074
    iput v9, v8, LX/99o;->a:I

    .line 586075
    move-object v8, v8

    .line 586076
    const v9, 0x7f0a00b5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 586077
    iput v5, v8, LX/99o;->b:I

    .line 586078
    move-object v5, v8

    .line 586079
    invoke-direct/range {v0 .. v5}, LX/99m;-><init>(Ljava/lang/String;IILX/8yy;LX/99o;)V

    invoke-interface {p1, v6, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 586080
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 586081
    check-cast p1, LX/0Px;

    const/4 v1, 0x0

    .line 586082
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
