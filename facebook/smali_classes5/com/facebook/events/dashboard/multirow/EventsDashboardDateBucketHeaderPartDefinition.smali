.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586120
    const v0, 0x7f03055f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586137
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586138
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;
    .locals 3

    .prologue
    .line 586126
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;

    monitor-enter v1

    .line 586127
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586128
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586129
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586130
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 586131
    new-instance v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;

    invoke-direct {v0}, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;-><init>()V

    .line 586132
    move-object v0, v0

    .line 586133
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586134
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586135
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586125
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardDateBucketHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x66d908d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586122
    check-cast p1, Ljava/lang/String;

    .line 586123
    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586124
    const/16 v1, 0x1f

    const v2, 0x6a318005

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586121
    const/4 v0, 0x1

    return v0
.end method
