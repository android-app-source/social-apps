.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586228
    const v0, 0x7f03056a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586226
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586227
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;
    .locals 3

    .prologue
    .line 586215
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;

    monitor-enter v1

    .line 586216
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586217
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586218
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586219
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 586220
    new-instance v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;

    invoke-direct {v0}, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;-><init>()V

    .line 586221
    move-object v0, v0

    .line 586222
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586223
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586224
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586225
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586229
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 586213
    check-cast p3, LX/I2o;

    .line 586214
    new-instance v0, LX/I2h;

    invoke-direct {v0, p0, p3}, LX/I2h;-><init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardViewMoreEventsRowPartDefinition;LX/I2o;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7b682e9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586210
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 586211
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586212
    const/16 v1, 0x1f

    const v2, -0x4bbb1617

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586209
    sget-object v0, LX/I2Z;->a:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586207
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586208
    return-void
.end method
