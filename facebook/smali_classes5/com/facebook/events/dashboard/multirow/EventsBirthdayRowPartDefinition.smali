.class public Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/HyQ;",
        "LX/I2M;",
        "LX/I2l;",
        "LX/HxG;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public a:LX/6RZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586328
    new-instance v0, LX/3UW;

    invoke-direct {v0}, LX/3UW;-><init>()V

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/6RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586325
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586326
    iput-object p1, p0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->a:LX/6RZ;

    .line 586327
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;
    .locals 4

    .prologue
    .line 586314
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;

    monitor-enter v1

    .line 586315
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586316
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586317
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586318
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586319
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v3

    check-cast v3, LX/6RZ;

    invoke-direct {p0, v3}, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;-><init>(LX/6RZ;)V

    .line 586320
    move-object v0, p0

    .line 586321
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586322
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586323
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586324
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/HxG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586290
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 586309
    check-cast p2, LX/HyQ;

    check-cast p3, LX/I2l;

    .line 586310
    new-instance v0, LX/I2M;

    .line 586311
    iget-object v1, p2, LX/HyQ;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    move-object v1, v1

    .line 586312
    invoke-interface {p3}, LX/I2l;->x()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, LX/I2M;-><init>(Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/base/fragment/FbFragment;Z)V

    .line 586313
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5a3dcb6e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586292
    check-cast p1, LX/HyQ;

    check-cast p2, LX/I2M;

    check-cast p3, LX/I2l;

    check-cast p4, LX/HxG;

    .line 586293
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 586294
    iget-object v4, p1, LX/HyQ;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    move-object v5, v4

    .line 586295
    invoke-interface {p3}, LX/I2l;->x()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v6

    .line 586296
    iget-object v4, p1, LX/HyQ;->a:Ljava/lang/String;

    move-object v7, v4

    .line 586297
    iget-boolean v8, p2, LX/I2M;->h:Z

    iget-boolean v9, p2, LX/I2M;->g:Z

    move-object v4, p4

    .line 586298
    iput-object v5, v4, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 586299
    iput-object v6, v4, LX/HxG;->j:Lcom/facebook/base/fragment/FbFragment;

    .line 586300
    iput-object v7, v4, LX/HxG;->r:Ljava/lang/String;

    .line 586301
    iput-boolean v8, v4, LX/HxG;->q:Z

    .line 586302
    iput-boolean v9, v4, LX/HxG;->p:Z

    .line 586303
    iget-object v5, p2, LX/I2M;->b:Ljava/lang/String;

    iget-object v6, p2, LX/I2M;->c:Ljava/lang/String;

    iget-object v7, p2, LX/I2M;->d:Ljava/lang/String;

    iget-object v8, p2, LX/I2M;->e:Ljava/lang/String;

    iget-object v9, p2, LX/I2M;->f:Ljava/lang/StringBuilder;

    move-object v4, p4

    invoke-virtual/range {v4 .. v9}, LX/HxG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 586304
    iget-boolean v4, p2, LX/I2M;->i:Z

    if-nez v4, :cond_0

    .line 586305
    iget-boolean v4, p1, LX/HyQ;->c:Z

    move v4, v4

    .line 586306
    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    invoke-virtual {p4, v4}, LX/HxG;->setComposeViews(Z)V

    .line 586307
    const/16 v1, 0x1f

    const v2, 0x5f30dfb9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 586308
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586291
    const/4 v0, 0x1

    return v0
.end method
