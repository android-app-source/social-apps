.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private e:LX/I2e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586257
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/I2e;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586287
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 586288
    iput-object p2, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->e:LX/I2e;

    .line 586289
    return-void
.end method

.method private a(LX/1De;LX/I2o;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 586274
    iget-object v0, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->e:LX/I2e;

    const/4 v1, 0x0

    .line 586275
    new-instance v2, LX/I2d;

    invoke-direct {v2, v0}, LX/I2d;-><init>(LX/I2e;)V

    .line 586276
    iget-object p0, v0, LX/I2e;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/I2c;

    .line 586277
    if-nez p0, :cond_0

    .line 586278
    new-instance p0, LX/I2c;

    invoke-direct {p0, v0}, LX/I2c;-><init>(LX/I2e;)V

    .line 586279
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/I2c;->a$redex0(LX/I2c;LX/1De;IILX/I2d;)V

    .line 586280
    move-object v2, p0

    .line 586281
    move-object v1, v2

    .line 586282
    move-object v0, v1

    .line 586283
    iget-object v1, v0, LX/I2c;->a:LX/I2d;

    iput-object p2, v1, LX/I2d;->a:LX/I2j;

    .line 586284
    iget-object v1, v0, LX/I2c;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 586285
    move-object v0, v0

    .line 586286
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;
    .locals 5

    .prologue
    .line 586263
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;

    monitor-enter v1

    .line 586264
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586265
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586266
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586267
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586268
    new-instance p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/I2e;->a(LX/0QB;)LX/I2e;

    move-result-object v4

    check-cast v4, LX/I2e;

    invoke-direct {p0, v3, v4}, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;-><init>(Landroid/content/Context;LX/I2e;)V

    .line 586269
    move-object v0, p0

    .line 586270
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586271
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586272
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 586262
    check-cast p3, LX/I2o;

    invoke-direct {p0, p1, p3}, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->a(LX/1De;LX/I2o;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 586261
    check-cast p3, LX/I2o;

    invoke-direct {p0, p1, p3}, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->a(LX/1De;LX/I2o;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586260
    sget-object v0, LX/I2Z;->b:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 586259
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 586258
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
