.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nQ;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586256
    const v0, 0x7f030569

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1nQ;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nQ;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586251
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586252
    iput-object p1, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->b:LX/1nQ;

    .line 586253
    iput-object p2, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->c:LX/0Or;

    .line 586254
    iput-object p3, p0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 586255
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;
    .locals 6

    .prologue
    .line 586240
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    monitor-enter v1

    .line 586241
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586242
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586245
    new-instance v5, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    const/16 v4, 0xc

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;-><init>(LX/1nQ;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 586246
    move-object v0, v5

    .line 586247
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586248
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586249
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586239
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 586230
    check-cast p3, LX/I2o;

    .line 586231
    new-instance v0, LX/I2g;

    invoke-direct {v0, p0, p3}, LX/I2g;-><init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;LX/I2o;)V

    .line 586232
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7c6ad491

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586236
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 586237
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586238
    const/16 v1, 0x1f

    const v2, 0x331eebce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586235
    sget-object v0, LX/I2Z;->b:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 586233
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586234
    return-void
.end method
