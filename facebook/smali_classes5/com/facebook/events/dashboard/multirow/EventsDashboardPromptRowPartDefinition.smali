.class public Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Hyy;",
        "Ljava/lang/Void;",
        "LX/I2j;",
        "LX/I32;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586331
    new-instance v0, LX/3UX;

    invoke-direct {v0}, LX/3UX;-><init>()V

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586332
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586333
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;
    .locals 3

    .prologue
    .line 586334
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;

    monitor-enter v1

    .line 586335
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586336
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586337
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586338
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 586339
    new-instance v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;

    invoke-direct {v0}, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;-><init>()V

    .line 586340
    move-object v0, v0

    .line 586341
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586342
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586343
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/I32;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586345
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardPromptRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x358212e9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586346
    check-cast p1, LX/Hyy;

    check-cast p3, LX/I2j;

    check-cast p4, LX/I32;

    .line 586347
    invoke-interface {p3}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v1

    invoke-virtual {p4, p1, v1}, LX/I32;->a(LX/Hyy;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 586348
    const/16 v1, 0x1f

    const v2, -0x400333e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586349
    const/4 v0, 0x1

    return v0
.end method
