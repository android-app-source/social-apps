.class public Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        "Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586085
    new-instance v0, LX/3UT;

    invoke-direct {v0}, LX/3UT;-><init>()V

    sput-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586086
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 586087
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;
    .locals 3

    .prologue
    .line 586088
    const-class v1, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    monitor-enter v1

    .line 586089
    :try_start_0
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586090
    sput-object v2, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586091
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586092
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 586093
    new-instance v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    invoke-direct {v0}, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;-><init>()V

    .line 586094
    move-object v0, v0

    .line 586095
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586096
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586097
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 586099
    sget-object v0, Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x60c3e9b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586100
    check-cast p1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    check-cast p3, LX/I2o;

    check-cast p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    .line 586101
    const/4 v2, 0x0

    .line 586102
    iput-object p1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 586103
    iget-object v1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {p4, v1}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setEventCollectionTitle(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V

    .line 586104
    const/4 p0, 0x0

    .line 586105
    iget-object v1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    .line 586106
    iget-object v1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->j()Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel$HeaderPhotoModel;->j()LX/1vs;

    move-result-object v1

    iget-object p0, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {p0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 586107
    :goto_1
    invoke-static {p4, v1}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setEventCollectionPhoto(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V

    .line 586108
    invoke-interface {p3}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v1

    .line 586109
    iput-object v1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 586110
    const/16 v1, 0x1f

    const v2, -0x7b8ca898

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move v1, v2

    .line 586111
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move-object v1, p0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 586112
    check-cast p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    .line 586113
    const/4 p1, 0x0

    .line 586114
    iput-object p1, p4, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 586115
    const-string p0, ""

    invoke-static {p4, p0}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setEventCollectionTitle(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V

    .line 586116
    invoke-static {p4, p1}, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->setEventCollectionPhoto(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;Ljava/lang/String;)V

    .line 586117
    return-void
.end method
