.class public Lcom/facebook/events/widget/eventcard/EventCardHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/ImageView;

.field public c:Z

.field private d:LX/1af;

.field private e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 590945
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 590946
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->g()V

    .line 590947
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590895
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590896
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->g()V

    .line 590897
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590942
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590943
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->g()V

    .line 590944
    return-void
.end method

.method private a(Lcom/facebook/drawee/view/DraweeView;LX/1aZ;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 590936
    iget-boolean v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->c:Z

    if-eqz v1, :cond_1

    .line 590937
    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 590938
    :goto_1
    invoke-virtual {p1, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 590939
    return-void

    .line 590940
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 590941
    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    goto :goto_1
.end method

.method private g()V
    .locals 3

    .prologue
    .line 590925
    const v0, 0x7f0304ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 590926
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setBackgroundResource(I)V

    .line 590927
    const v0, 0x7f0d0dad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 590928
    const v0, 0x7f0d0dae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    .line 590929
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    .line 590930
    new-instance v0, LX/1Uo;

    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a00e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 590931
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 590932
    move-object v0, v0

    .line 590933
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->d:LX/1af;

    .line 590934
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->d:LX/1af;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 590935
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 590921
    const/4 v0, 0x0

    .line 590922
    iput-boolean v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->c:Z

    .line 590923
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e()V

    .line 590924
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 590919
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 590920
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 590916
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 590917
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590918
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 590909
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    .line 590910
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b0b56

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 590911
    int-to-float v2, v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1, v3, v3}, LX/4Ab;->a(FFFF)LX/4Ab;

    .line 590912
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e:Landroid/content/res/Resources;

    const v3, 0x7f0b0b57

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LX/4Ab;->a(IF)LX/4Ab;

    .line 590913
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->d:LX/1af;

    invoke-virtual {v1, v0}, LX/1af;->a(LX/4Ab;)V

    .line 590914
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->d:LX/1af;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 590915
    return-void
.end method

.method public getCoverPhotoView()Lcom/facebook/drawee/view/DraweeView;
    .locals 1

    .prologue
    .line 590908
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getRemoveButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 590907
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method public setCoverPhotoAspectRatio(F)V
    .locals 1

    .prologue
    .line 590905
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 590906
    return-void
.end method

.method public setCoverPhotoController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590903
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0, v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a(Lcom/facebook/drawee/view/DraweeView;LX/1aZ;)V

    .line 590904
    return-void
.end method

.method public setCoverPhotoFocusPoint(Landroid/graphics/PointF;)V
    .locals 1
    .param p1    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590900
    if-nez p1, :cond_0

    .line 590901
    :goto_0
    return-void

    .line 590902
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setShouldHideNullCoverPhotoView(Z)V
    .locals 0

    .prologue
    .line 590898
    iput-boolean p1, p0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->c:Z

    .line 590899
    return-void
.end method
