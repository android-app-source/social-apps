.class public Lcom/facebook/events/widget/eventcard/EventCardFooterView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public j:Lcom/facebook/widget/text/BetterTextView;

.field private k:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 590879
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 590880
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->g()V

    .line 590881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 590876
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590877
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->g()V

    .line 590878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 590873
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590874
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->g()V

    .line 590875
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 590866
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 590867
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0812ac

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 590868
    :cond_0
    :goto_0
    return-object p1

    .line 590869
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590870
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object p1, p2

    .line 590871
    goto :goto_0

    .line 590872
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 590802
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 590803
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 590804
    return-void

    .line 590805
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 590860
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 590861
    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setVisibility(I)V

    .line 590862
    :goto_0
    return-void

    .line 590863
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;Ljava/util/Date;)V
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 590856
    if-nez p1, :cond_0

    .line 590857
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setVisibility(I)V

    .line 590858
    :goto_0
    return-void

    .line 590859
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 590848
    const v0, 0x7f0304aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 590849
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setBackgroundResource(I)V

    .line 590850
    const v0, 0x7f0d0da9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 590851
    const v0, 0x7f0d0da8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->k:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    .line 590852
    const v0, 0x7f0d0daa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 590853
    const v0, 0x7f0d0dab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 590854
    const v0, 0x7f0d0dac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    .line 590855
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;I)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 590845
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 590846
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 590847
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590843
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 590844
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 590841
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->k:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-static {v0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;Ljava/lang/String;Ljava/lang/String;)V

    .line 590842
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 590835
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 590836
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 590837
    invoke-virtual {p0, v0, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 590838
    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 590839
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a()V

    .line 590840
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 590864
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 590865
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 590833
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 590834
    return-void
.end method

.method public getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;
    .locals 1

    .prologue
    .line 590832
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    return-object v0
.end method

.method public getSocialContextTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 590831
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getTitleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 590830
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/16 v3, 0x8

    .line 590818
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 590819
    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 590820
    :cond_0
    :goto_0
    return-void

    .line 590821
    :cond_1
    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getLineCount()I

    move-result v2

    .line 590822
    if-gt v2, v0, :cond_2

    .line 590823
    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v2

    if-ne v2, v3, :cond_3

    .line 590824
    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 590825
    :goto_1
    if-eqz v0, :cond_0

    .line 590826
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 590827
    goto :goto_0

    .line 590828
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v2

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 590829
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public setCalendarFormatStartDate(Ljava/util/Date;)V
    .locals 1
    .param p1    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590816
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->k:Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-static {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;Ljava/util/Date;)V

    .line 590817
    return-void
.end method

.method public setEventInfoTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 590814
    iput-object p1, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 590815
    return-void
.end method

.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590812
    const v0, 0x7f0a010e

    invoke-virtual {p0, p1, v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Ljava/lang/CharSequence;I)V

    .line 590813
    return-void
.end method

.method public setSocialContextTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 590810
    iput-object p1, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 590811
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590808
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 590809
    return-void
.end method

.method public setTitleTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 590806
    iput-object p1, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 590807
    return-void
.end method
