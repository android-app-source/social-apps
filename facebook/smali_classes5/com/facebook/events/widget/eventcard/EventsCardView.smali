.class public Lcom/facebook/events/widget/eventcard/EventsCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

.field private b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

.field public c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 587898
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 587899
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->i()V

    .line 587900
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 587901
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587902
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->i()V

    .line 587903
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 587904
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587905
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->i()V

    .line 587906
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 587907
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 587908
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0812ac

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 587909
    :cond_0
    :goto_0
    return-object p1

    .line 587910
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587911
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object p1, p2

    .line 587912
    goto :goto_0

    .line 587913
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 587914
    const v0, 0x7f0304ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 587915
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setOrientation(I)V

    .line 587916
    const v0, 0x7f0d0daf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    .line 587917
    const v0, 0x7f0d0db0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587918
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 587919
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587920
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a()V

    .line 587921
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->d()V

    .line 587922
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->d()V

    .line 587923
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 587924
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 587925
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;I)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 587928
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Ljava/lang/CharSequence;I)V

    .line 587929
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587890
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 587891
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 587926
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->f()V

    .line 587927
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 587939
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b()V

    .line 587940
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 587937
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->e()V

    .line 587938
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 587935
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e()V

    .line 587936
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 587932
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 587933
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b5e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setPadding(IIII)V

    .line 587934
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 587930
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->f()V

    .line 587931
    return-void
.end method

.method public getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;
    .locals 1

    .prologue
    .line 587892
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587893
    iget-object p0, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->n:Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-object v0, p0

    .line 587894
    return-object v0
.end method

.method public getCoverPhotoView()Lcom/facebook/drawee/view/DraweeView;
    .locals 1

    .prologue
    .line 587895
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    .line 587896
    iget-object p0, v0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, p0

    .line 587897
    return-object v0
.end method

.method public getEventCardBottomActionView()Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 587869
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    return-object v0
.end method

.method public getEventCardFooterView()Lcom/facebook/events/widget/eventcard/EventCardFooterView;
    .locals 1

    .prologue
    .line 587850
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    return-object v0
.end method

.method public getRemoveButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 587851
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    .line 587852
    iget-object p0, v0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->b:Landroid/widget/ImageView;

    move-object v0, p0

    .line 587853
    return-object v0
.end method

.method public getSocialContextTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 587854
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587855
    iget-object p0, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    move-object v0, p0

    .line 587856
    return-object v0
.end method

.method public getTitleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 587857
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587858
    iget-object p0, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    move-object v0, p0

    .line 587859
    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 587860
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    if-nez v0, :cond_0

    .line 587861
    const v0, 0x7f0d0db1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 587862
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    .line 587863
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->setVisibility(I)V

    .line 587864
    return-void
.end method

.method public setCalendarFormatStartDate(Ljava/util/Date;)V
    .locals 1
    .param p1    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587865
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 587866
    return-void
.end method

.method public setCoverPhotoAspectRatio(F)V
    .locals 1

    .prologue
    .line 587867
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setCoverPhotoAspectRatio(F)V

    .line 587868
    return-void
.end method

.method public setCoverPhotoController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587870
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setCoverPhotoController(LX/1aZ;)V

    .line 587871
    return-void
.end method

.method public setCoverPhotoFocusPoint(Landroid/graphics/PointF;)V
    .locals 1
    .param p1    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587872
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setCoverPhotoFocusPoint(Landroid/graphics/PointF;)V

    .line 587873
    return-void
.end method

.method public setEventInfoTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 587874
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587875
    iput-object p1, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 587876
    return-void
.end method

.method public setShouldHideNullCoverPhotoView(Z)V
    .locals 1

    .prologue
    .line 587877
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->a:Lcom/facebook/events/widget/eventcard/EventCardHeaderView;

    .line 587878
    iput-boolean p1, v0, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->c:Z

    .line 587879
    return-void
.end method

.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587880
    const v0, 0x7f0a010e

    invoke-virtual {p0, p1, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;I)V

    .line 587881
    return-void
.end method

.method public setSocialContextTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 587882
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587883
    iput-object p1, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 587884
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587885
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 587886
    return-void
.end method

.method public setTitleTextView(Lcom/facebook/widget/text/BetterTextView;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 587887
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventsCardView;->b:Lcom/facebook/events/widget/eventcard/EventCardFooterView;

    .line 587888
    iput-object p1, v0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 587889
    return-void
.end method
