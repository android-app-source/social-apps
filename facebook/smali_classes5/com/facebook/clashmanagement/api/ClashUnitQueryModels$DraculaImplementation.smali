.class public final Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 583040
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 583041
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 583042
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 583043
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 583044
    if-nez p1, :cond_0

    .line 583045
    :goto_0
    return v0

    .line 583046
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 583047
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 583048
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 583049
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 583050
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 583051
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 583052
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 583053
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 583054
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3b31d69d
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 583062
    if-nez p0, :cond_0

    move v0, v1

    .line 583063
    :goto_0
    return v0

    .line 583064
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 583065
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 583066
    :goto_1
    if-ge v1, v2, :cond_2

    .line 583067
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 583068
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 583069
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 583070
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 583055
    const/4 v7, 0x0

    .line 583056
    const/4 v1, 0x0

    .line 583057
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 583058
    invoke-static {v2, v3, v0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 583059
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 583060
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 583061
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 583027
    new-instance v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 583072
    packed-switch p0, :pswitch_data_0

    .line 583073
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 583074
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x3b31d69d
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 583071
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 583033
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->b(I)V

    .line 583034
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 583035
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 583036
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 583037
    :cond_0
    iput-object p1, p0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a:LX/15i;

    .line 583038
    iput p2, p0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->b:I

    .line 583039
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 583032
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 583031
    new-instance v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 583028
    iget v0, p0, LX/1vt;->c:I

    .line 583029
    move v0, v0

    .line 583030
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 583024
    iget v0, p0, LX/1vt;->c:I

    .line 583025
    move v0, v0

    .line 583026
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 583021
    iget v0, p0, LX/1vt;->b:I

    .line 583022
    move v0, v0

    .line 583023
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583018
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 583019
    move-object v0, v0

    .line 583020
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 583009
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 583010
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 583011
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 583012
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 583013
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 583014
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 583015
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 583016
    invoke-static {v3, v9, v2}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 583017
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 583006
    iget v0, p0, LX/1vt;->c:I

    .line 583007
    move v0, v0

    .line 583008
    return v0
.end method
