.class public final Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ce2d8c2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 582110
    const-class v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 582109
    const-class v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 582107
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 582108
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 582101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 582102
    invoke-virtual {p0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 582103
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 582104
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 582105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 582106
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 582093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 582094
    invoke-virtual {p0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 582095
    invoke-virtual {p0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    .line 582096
    invoke-virtual {p0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 582097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;

    .line 582098
    iput-object v0, v1, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->e:Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    .line 582099
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 582100
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEligibleClashUnits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 582091
    iget-object v0, p0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->e:Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    iput-object v0, p0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->e:Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    .line 582092
    iget-object v0, p0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;->e:Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 582088
    new-instance v0, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;

    invoke-direct {v0}, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;-><init>()V

    .line 582089
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 582090
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 582087
    const v0, 0x5348d896

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 582086
    const v0, -0x6747e1ce

    return v0
.end method
