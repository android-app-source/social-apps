.class public interface abstract annotation Lcom/facebook/flatbuffers/FlattenableField;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/facebook/flatbuffers/FlattenableField;
        keyFlattener = Lcom/facebook/flatbuffers/FlattenableField$NilFlattener;
        keyTypeResolver = Lcom/facebook/flatbuffers/FlattenableField$NilResolver;
        valueFlattener = Lcom/facebook/flatbuffers/FlattenableField$NilFlattener;
        valueTypeResolver = Lcom/facebook/flatbuffers/FlattenableField$NilResolver;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->FIELD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract keyFlattener()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/4Bv",
            "<*>;>;"
        }
    .end annotation
.end method

.method public abstract keyTypeResolver()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/16a;",
            ">;"
        }
    .end annotation
.end method

.method public abstract valueFlattener()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/4Bv",
            "<*>;>;"
        }
    .end annotation
.end method

.method public abstract valueTypeResolver()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/16a;",
            ">;"
        }
    .end annotation
.end method
