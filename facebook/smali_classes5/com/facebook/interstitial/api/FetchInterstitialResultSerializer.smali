.class public Lcom/facebook/interstitial/api/FetchInterstitialResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 577704
    const-class v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    new-instance v1, Lcom/facebook/interstitial/api/FetchInterstitialResultSerializer;

    invoke-direct {v1}, Lcom/facebook/interstitial/api/FetchInterstitialResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 577705
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 577706
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 577707
    if-nez p0, :cond_0

    .line 577708
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 577709
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 577710
    invoke-static {p0, p1, p2}, Lcom/facebook/interstitial/api/FetchInterstitialResultSerializer;->b(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/0nX;LX/0my;)V

    .line 577711
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 577712
    return-void
.end method

.method private static b(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 577713
    const-string v0, "rank"

    iget v1, p0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->rank:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 577714
    const-string v0, "nux_id"

    iget-object v1, p0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 577715
    const-string v0, "nux_data"

    iget-object v1, p0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->data:Landroid/os/Parcelable;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 577716
    const-string v0, "fetchTimeMs"

    iget-wide v2, p0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->fetchTimeMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 577717
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 577718
    check-cast p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/interstitial/api/FetchInterstitialResultSerializer;->a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/0nX;LX/0my;)V

    return-void
.end method
