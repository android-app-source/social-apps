.class public interface abstract annotation Lcom/facebook/crudolib/urimap/componenthelper/impl/ComponentHelperFactoryEntry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation build Lcom/facebook/metagen/CodeGeneratorMetadataAnnotation;
    generator = "com.facebook.metagen.generator.mustache.MustacheCodeGenerator"
.end annotation

.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/facebook/crudolib/urimap/componenthelper/impl/ComponentHelperFactoryEntry;
        helperStr = ""
        moduleStr = ""
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# virtual methods
.method public abstract helperStr()Ljava/lang/String;
.end method

.method public abstract moduleStr()Ljava/lang/String;
.end method
