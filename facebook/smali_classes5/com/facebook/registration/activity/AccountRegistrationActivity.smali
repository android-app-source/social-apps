.class public Lcom/facebook/registration/activity/AccountRegistrationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/1ZF;


# instance fields
.field public p:LX/HZt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/HaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/registration/model/SimpleRegFormData;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0h5;

.field private u:Lcom/facebook/registration/controller/RegistrationFragmentController;

.field private v:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 569386
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 569388
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/registration/activity/AccountRegistrationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 569389
    const-string v1, "extra_ref"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 569390
    return-object v0
.end method

.method private static a(Lcom/facebook/registration/activity/AccountRegistrationActivity;LX/HZt;LX/0if;LX/HaQ;Lcom/facebook/registration/model/SimpleRegFormData;)V
    .locals 0

    .prologue
    .line 569387
    iput-object p1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->p:LX/HZt;

    iput-object p2, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->q:LX/0if;

    iput-object p3, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->r:LX/HaQ;

    iput-object p4, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;

    invoke-static {v3}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v0

    check-cast v0, LX/HZt;

    invoke-static {v3}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-static {v3}, LX/HaQ;->b(LX/0QB;)LX/HaQ;

    move-result-object v2

    check-cast v2, LX/HaQ;

    invoke-static {v3}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v3

    check-cast v3, Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Lcom/facebook/registration/activity/AccountRegistrationActivity;LX/HZt;LX/0if;LX/HaQ;Lcom/facebook/registration/model/SimpleRegFormData;)V

    return-void
.end method

.method public static b(Lcom/facebook/registration/activity/AccountRegistrationActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 569361
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->r:LX/HaQ;

    invoke-virtual {v0}, LX/HaQ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->u:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->e()Z

    move-result v0

    .line 569362
    :goto_0
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->r:LX/HaQ;

    invoke-virtual {v1, p0, p1, v0}, LX/HaQ;->a(Landroid/app/Activity;Ljava/lang/String;Z)V

    .line 569363
    return-void

    .line 569364
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->u:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/registration/controller/RegistrationFragmentController;->d()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/63W;)V
    .locals 0

    .prologue
    .line 569385
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 569384
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 569382
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->t:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 569383
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 569391
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 569392
    invoke-static {p0, p0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 569393
    const v0, 0x7f0311ac

    invoke-virtual {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->setContentView(I)V

    .line 569394
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 569395
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->t:LX/0h5;

    .line 569396
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->t:LX/0h5;

    new-instance v1, LX/HYX;

    invoke-direct {v1, p0}, LX/HYX;-><init>(Lcom/facebook/registration/activity/AccountRegistrationActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 569397
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d297f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/controller/RegistrationFragmentController;

    iput-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->u:Lcom/facebook/registration/controller/RegistrationFragmentController;

    .line 569398
    invoke-virtual {p0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_ref"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 569399
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->p:LX/HZt;

    invoke-virtual {p0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_ref"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HZt;->a(Ljava/lang/String;)V

    .line 569400
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "login_field_username"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569401
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 569402
    sget-object v1, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 569403
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/RegistrationFormData;->setEmail(Ljava/lang/String;)V

    .line 569404
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    .line 569405
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "login_field_password"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569406
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 569407
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/RegistrationFormData;->c(Ljava/lang/String;)V

    .line 569408
    :cond_1
    return-void

    .line 569409
    :cond_2
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->p:LX/HZt;

    const-string v1, "no_ref"

    invoke-virtual {v0, v1}, LX/HZt;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 569410
    :cond_3
    sget-object v1, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 569411
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/RegistrationFormData;->setPhoneNumberInputRaw(Ljava/lang/String;)V

    .line 569412
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->s:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/model/RegistrationFormData;->a(Lcom/facebook/growth/model/ContactpointType;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 0

    .prologue
    .line 569381
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 569380
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->v:Landroid/view/View;

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 569379
    return-void
.end method

.method public final lH_()V
    .locals 0

    .prologue
    .line 569378
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 569374
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->u:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569375
    const-string v0, "back_button"

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->b(Lcom/facebook/registration/activity/AccountRegistrationActivity;Ljava/lang/String;)V

    .line 569376
    :goto_0
    return-void

    .line 569377
    :cond_0
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->u:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->c()Z

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7490ae14

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 569370
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->q:LX/0if;

    if-eqz v1, :cond_0

    .line 569371
    iget-object v1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->q:LX/0if;

    sget-object v2, LX/0ig;->c:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 569372
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 569373
    const/16 v1, 0x23

    const v2, -0x1bf06e54

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 569367
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->t:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 569368
    iput-object p1, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->v:Landroid/view/View;

    .line 569369
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 569365
    iget-object v0, p0, Lcom/facebook/registration/activity/AccountRegistrationActivity;->t:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 569366
    return-void
.end method
