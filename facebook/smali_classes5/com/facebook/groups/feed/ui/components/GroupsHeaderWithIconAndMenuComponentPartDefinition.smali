.class public Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/IVG;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599912
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/IVG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599930
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 599931
    iput-object p3, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->e:LX/IVG;

    .line 599932
    iput-object p2, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->f:LX/1V0;

    .line 599933
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pp;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 599913
    iget-object v0, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->e:LX/IVG;

    const/4 v1, 0x0

    .line 599914
    new-instance v2, LX/IVF;

    invoke-direct {v2, v0}, LX/IVF;-><init>(LX/IVG;)V

    .line 599915
    iget-object v3, v0, LX/IVG;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IVE;

    .line 599916
    if-nez v3, :cond_0

    .line 599917
    new-instance v3, LX/IVE;

    invoke-direct {v3, v0}, LX/IVE;-><init>(LX/IVG;)V

    .line 599918
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/IVE;->a$redex0(LX/IVE;LX/1De;IILX/IVF;)V

    .line 599919
    move-object v2, v3

    .line 599920
    move-object v1, v2

    .line 599921
    move-object v0, v1

    .line 599922
    iget-object v1, v0, LX/IVE;->a:LX/IVF;

    iput-object p2, v1, LX/IVF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599923
    iget-object v1, v0, LX/IVE;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 599924
    move-object v0, v0

    .line 599925
    iget-object v1, v0, LX/IVE;->a:LX/IVF;

    iput-object p3, v1, LX/IVF;->b:LX/1Pp;

    .line 599926
    iget-object v1, v0, LX/IVE;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 599927
    move-object v0, v0

    .line 599928
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 599929
    iget-object v1, p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->i:LX/1Ua;

    sget-object v4, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v2, p2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;
    .locals 6

    .prologue
    .line 599895
    const-class v1, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;

    monitor-enter v1

    .line 599896
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599897
    sput-object v2, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599900
    new-instance p0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/IVG;->a(LX/0QB;)LX/IVG;

    move-result-object v5

    check-cast v5, LX/IVG;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/IVG;)V

    .line 599901
    move-object v0, p0

    .line 599902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 599906
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599907
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 599908
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599909
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 599910
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599911
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 599888
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pp;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pp;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 599889
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pp;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pp;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599890
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599891
    invoke-static {p1}, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 599892
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599893
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 599894
    sget-object v0, Lcom/facebook/groups/feed/ui/components/GroupsHeaderWithIconAndMenuComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
