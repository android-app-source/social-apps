.class public Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599865
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599866
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 599867
    return-void
.end method

.method private static a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;",
            ">;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 599868
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599869
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 599870
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 599871
    invoke-static {p0}, LX/DOy;->c(LX/1De;)LX/DOw;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/DOw;->b(Ljava/lang/String;)LX/DOw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;
    .locals 4

    .prologue
    .line 599872
    const-class v1, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;

    monitor-enter v1

    .line 599873
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599874
    sput-object v2, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599875
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599876
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599877
    new-instance p0, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 599878
    move-object v0, p0

    .line 599879
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599880
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599881
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599882
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 599883
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1, p2}, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 599884
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1, p2}, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599885
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 599886
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 599887
    sget-object v0, Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
