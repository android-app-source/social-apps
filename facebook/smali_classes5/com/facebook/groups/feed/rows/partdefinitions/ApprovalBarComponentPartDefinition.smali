.class public Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/DP3;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 599819
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/1UY;->b(I)LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DP3;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599815
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 599816
    iput-object p3, p0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->f:LX/1V0;

    .line 599817
    iput-object p2, p0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->e:LX/DP3;

    .line 599818
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 599784
    iget-object v0, p0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->e:LX/DP3;

    const/4 v1, 0x0

    .line 599785
    new-instance v2, LX/DP1;

    invoke-direct {v2, v0}, LX/DP1;-><init>(LX/DP3;)V

    .line 599786
    sget-object v3, LX/DP3;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DP2;

    .line 599787
    if-nez v3, :cond_0

    .line 599788
    new-instance v3, LX/DP2;

    invoke-direct {v3}, LX/DP2;-><init>()V

    .line 599789
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DP2;->a$redex0(LX/DP2;LX/1De;IILX/DP1;)V

    .line 599790
    move-object v2, v3

    .line 599791
    move-object v1, v2

    .line 599792
    move-object v0, v1

    .line 599793
    iget-object v1, v0, LX/DP2;->a:LX/DP1;

    iput-object p2, v1, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599794
    iget-object v1, v0, LX/DP2;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 599795
    move-object v0, v0

    .line 599796
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 599797
    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->d:LX/1Ua;

    const v3, 0x7f0a00d5

    const/4 v4, -0x1

    invoke-direct {v1, p2, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 599798
    iget-object v2, p0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;
    .locals 6

    .prologue
    .line 599804
    const-class v1, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    monitor-enter v1

    .line 599805
    :try_start_0
    sget-object v0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599806
    sput-object v2, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599807
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599808
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599809
    new-instance p0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DP3;->a(LX/0QB;)LX/DP3;

    move-result-object v4

    check-cast v4, LX/DP3;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;-><init>(Landroid/content/Context;LX/DP3;LX/1V0;)V

    .line 599810
    move-object v0, p0

    .line 599811
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599812
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599813
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599814
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 599803
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 599802
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/groups/feed/rows/partdefinitions/ApprovalBarComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599801
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 599799
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599800
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
