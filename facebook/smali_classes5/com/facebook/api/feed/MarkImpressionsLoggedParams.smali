.class public Lcom/facebook/api/feed/MarkImpressionsLoggedParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/feed/MarkImpressionsLoggedParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 638106
    new-instance v0, LX/2uA;

    invoke-direct {v0}, LX/2uA;-><init>()V

    sput-object v0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 638101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->a:Ljava/lang/String;

    .line 638103
    const-class v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 638104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->c:I

    .line 638105
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;I)V
    .locals 0

    .prologue
    .line 638107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638108
    iput-object p1, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->a:Ljava/lang/String;

    .line 638109
    iput-object p2, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 638110
    iput p3, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->c:I

    .line 638111
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 638100
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 638096
    iget-object v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 638097
    iget-object v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->b:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 638098
    iget v0, p0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 638099
    return-void
.end method
