.class public final Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1oQ;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7000bf75
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 663604
    const-class v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 663610
    const-class v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 663608
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 663609
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 663605
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 663606
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 663607
    return-void
.end method

.method public static a(LX/1oQ;)Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;
    .locals 9

    .prologue
    .line 663564
    if-nez p0, :cond_0

    .line 663565
    const/4 p0, 0x0

    .line 663566
    :goto_0
    return-object p0

    .line 663567
    :cond_0
    instance-of v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;

    if-eqz v0, :cond_1

    .line 663568
    check-cast p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;

    goto :goto_0

    .line 663569
    :cond_1
    new-instance v0, LX/40W;

    invoke-direct {v0}, LX/40W;-><init>()V

    .line 663570
    invoke-interface {p0}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/40W;->a:Ljava/lang/String;

    .line 663571
    invoke-interface {p0}, LX/1oQ;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/40W;->b:Ljava/lang/String;

    .line 663572
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 663573
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 663574
    iget-object v3, v0, LX/40W;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 663575
    iget-object v5, v0, LX/40W;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 663576
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 663577
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 663578
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 663579
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 663580
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 663581
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 663582
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 663583
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 663584
    new-instance v3, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;-><init>(LX/15i;)V

    .line 663585
    move-object p0, v3

    .line 663586
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 663596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 663597
    invoke-virtual {p0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 663598
    invoke-virtual {p0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 663599
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 663600
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 663601
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 663602
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 663603
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 663611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 663612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 663613
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 663593
    new-instance v0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;-><init>()V

    .line 663594
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 663595
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 663591
    iget-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->e:Ljava/lang/String;

    .line 663592
    iget-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 663590
    const v0, -0x4b339c7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 663589
    const v0, -0x7646fe03    # -4.4539E-33f

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 663587
    iget-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->f:Ljava/lang/String;

    .line 663588
    iget-object v0, p0, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$NewItemDefaultPrivacyModel;->f:Ljava/lang/String;

    return-object v0
.end method
