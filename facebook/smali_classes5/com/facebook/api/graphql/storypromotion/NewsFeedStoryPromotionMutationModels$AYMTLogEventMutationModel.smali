.class public final Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6c2676c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 664395
    const-class v0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 664394
    const-class v0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 664409
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 664410
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664407
    iget-object v0, p0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->e:Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->e:Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    .line 664408
    iget-object v0, p0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->e:Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 664401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 664402
    invoke-direct {p0}, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->a()Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 664403
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 664404
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 664405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 664406
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 664411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 664412
    invoke-direct {p0}, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->a()Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 664413
    invoke-direct {p0}, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->a()Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    .line 664414
    invoke-direct {p0}, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->a()Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 664415
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;

    .line 664416
    iput-object v0, v1, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;->e:Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel$AymtChannelModel;

    .line 664417
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 664418
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 664398
    new-instance v0, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/storypromotion/NewsFeedStoryPromotionMutationModels$AYMTLogEventMutationModel;-><init>()V

    .line 664399
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 664400
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 664397
    const v0, -0x5405d3f1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 664396
    const v0, -0x4c62c188

    return v0
.end method
