.class public final Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x128e82e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 663172
    const-class v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 663211
    const-class v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 663209
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 663210
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 663194
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 663195
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 663196
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663197
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663198
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663199
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663200
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663201
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663202
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663203
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663204
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663205
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663206
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 663207
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 663208
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 663191
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 663192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 663193
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 663178
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 663179
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->e:Z

    .line 663180
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->f:Z

    .line 663181
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->g:Z

    .line 663182
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->h:Z

    .line 663183
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->i:Z

    .line 663184
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->j:Z

    .line 663185
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->k:Z

    .line 663186
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->l:Z

    .line 663187
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->m:Z

    .line 663188
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->n:Z

    .line 663189
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;->o:Z

    .line 663190
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 663175
    new-instance v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel$InstantExperienceFeatureEnabledListModel;-><init>()V

    .line 663176
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 663177
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 663174
    const v0, 0x39070b06

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 663173
    const v0, -0x4b42095f

    return v0
.end method
