.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/3cr;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x30b0e8ee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:I

.field private G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 615359
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 615351
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 615349
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 615350
    return-void
.end method

.method private H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615347
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615348
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615345
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615346
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615343
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615344
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    return-object v0
.end method

.method private K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615341
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615342
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615252
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615253
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    return-object v0
.end method

.method private M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615276
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615277
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    return-object v0
.end method

.method private N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615274
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615275
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615272
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615273
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615270
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615271
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615268
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615269
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615266
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615267
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 615260
    iput p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F:I

    .line 615261
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615262
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615263
    if-eqz v0, :cond_0

    .line 615264
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 615265
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615254
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615255
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615256
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615257
    if-eqz v0, :cond_0

    .line 615258
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615259
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615246
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615247
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615248
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615249
    if-eqz v0, :cond_0

    .line 615250
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615251
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615353
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615354
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615355
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615356
    if-eqz v0, :cond_0

    .line 615357
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615358
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615442
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615443
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615444
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615445
    if-eqz v0, :cond_0

    .line 615446
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615447
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 615436
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->g:Z

    .line 615437
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615438
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615439
    if-eqz v0, :cond_0

    .line 615440
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615441
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 615430
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->k:Z

    .line 615431
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615432
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615433
    if-eqz v0, :cond_0

    .line 615434
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615435
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 615424
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p:Z

    .line 615425
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615426
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615427
    if-eqz v0, :cond_0

    .line 615428
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615429
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 615418
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->s:Z

    .line 615419
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615420
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615421
    if-eqz v0, :cond_0

    .line 615422
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615423
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615417
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615416
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615415
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615414
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615413
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final F()I
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 615411
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615412
    iget v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F:I

    return v0
.end method

.method public final synthetic G()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615410
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 615360
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 615361
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 615362
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 615363
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 615364
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 615365
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 615366
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 615367
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 615368
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 615369
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 615370
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 615371
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 615372
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 615373
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 615374
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 615375
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 615376
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 615377
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 615378
    const/16 v20, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 615379
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->e:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615380
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->f:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615381
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->g:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615382
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->h:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615383
    const/16 v20, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->i:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615384
    const/16 v20, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->j:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615385
    const/16 v20, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->k:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615386
    const/16 v20, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->l:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615387
    const/16 v20, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->m:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615388
    const/16 v20, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 615389
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 615390
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 615391
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 615392
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 615393
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 615394
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 615395
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 615396
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 615397
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 615398
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 615399
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 615400
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 615401
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 615402
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 615403
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615404
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615405
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615406
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 615407
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615408
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 615409
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 615278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 615279
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 615280
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615281
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 615282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615283
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615284
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 615285
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615286
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 615287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615288
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615289
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 615290
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615291
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 615292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615293
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615294
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 615295
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615296
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 615297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615298
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615299
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 615300
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615301
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 615302
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615303
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615304
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 615305
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 615306
    if-eqz v2, :cond_5

    .line 615307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615308
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z:Ljava/util/List;

    move-object v1, v0

    .line 615309
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 615310
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615311
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 615312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615313
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615314
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 615315
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615316
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 615317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615318
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615319
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 615320
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615321
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 615322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615323
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615324
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 615325
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615326
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 615327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615328
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615329
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 615330
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615331
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 615332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615333
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615334
    :cond_a
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 615335
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615336
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 615337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    .line 615338
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615339
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 615340
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 615352
    new-instance v0, LX/5B3;

    invoke-direct {v0, p1}, LX/5B3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615221
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 615207
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 615208
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->e:Z

    .line 615209
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->f:Z

    .line 615210
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->g:Z

    .line 615211
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->h:Z

    .line 615212
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->i:Z

    .line 615213
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->j:Z

    .line 615214
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->k:Z

    .line 615215
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->l:Z

    .line 615216
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->m:Z

    .line 615217
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p:Z

    .line 615218
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->s:Z

    .line 615219
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F:I

    .line 615220
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615155
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615156
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615157
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615158
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 615159
    :goto_0
    return-void

    .line 615160
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615161
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615163
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615164
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615165
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615166
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615167
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615168
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615169
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615171
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615172
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 615173
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    .line 615174
    if-eqz v0, :cond_9

    .line 615175
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615176
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615177
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 615178
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 615179
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 615180
    if-eqz v0, :cond_9

    .line 615181
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615182
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615183
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615184
    :cond_5
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 615185
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    .line 615186
    if-eqz v0, :cond_9

    .line 615187
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615188
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615189
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615190
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 615191
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615192
    if-eqz v0, :cond_9

    .line 615193
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615194
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615195
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615196
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 615197
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615198
    if-eqz v0, :cond_9

    .line 615199
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615200
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615201
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615202
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 615203
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->F()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615205
    const/16 v0, 0x1b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615206
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 615146
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615147
    check-cast p2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)V

    .line 615148
    :cond_0
    :goto_0
    return-void

    .line 615149
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615150
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 615151
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615152
    check-cast p2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V

    goto :goto_0

    .line 615153
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615154
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 615095
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615096
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(Z)V

    .line 615097
    :cond_0
    :goto_0
    return-void

    .line 615098
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615099
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->b(Z)V

    goto :goto_0

    .line 615100
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615101
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->c(Z)V

    goto :goto_0

    .line 615102
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 615103
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->d(Z)V

    goto :goto_0

    .line 615104
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 615105
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    .line 615106
    if-eqz v0, :cond_0

    .line 615107
    if-eqz p3, :cond_5

    .line 615108
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615109
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a(I)V

    .line 615110
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    goto :goto_0

    .line 615111
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a(I)V

    goto :goto_0

    .line 615112
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 615113
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 615114
    if-eqz v0, :cond_0

    .line 615115
    if-eqz p3, :cond_7

    .line 615116
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615117
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 615118
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 615119
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 615120
    :cond_8
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 615121
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    .line 615122
    if-eqz v0, :cond_0

    .line 615123
    if-eqz p3, :cond_9

    .line 615124
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615125
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a(I)V

    .line 615126
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    goto/16 :goto_0

    .line 615127
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a(I)V

    goto/16 :goto_0

    .line 615128
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 615129
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615130
    if-eqz v0, :cond_0

    .line 615131
    if-eqz p3, :cond_b

    .line 615132
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615133
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    .line 615134
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 615135
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    goto/16 :goto_0

    .line 615136
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 615137
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615138
    if-eqz v0, :cond_0

    .line 615139
    if-eqz p3, :cond_d

    .line 615140
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615141
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    .line 615142
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 615143
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    goto/16 :goto_0

    .line 615144
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615145
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 615093
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615094
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 615091
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615092
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 615088
    new-instance v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;-><init>()V

    .line 615089
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 615090
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 615086
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615087
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 615084
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615085
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 615082
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615083
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 615081
    const v0, -0x3888bbd9

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 615079
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615080
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 615078
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 615076
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615077
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 615074
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615075
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615222
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->n:Ljava/lang/String;

    .line 615223
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 615224
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615225
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->p:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615226
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->q:Ljava/lang/String;

    .line 615227
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 615228
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615229
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->s:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615230
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->t:Ljava/lang/String;

    .line 615231
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615232
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->x:Ljava/lang/String;

    .line 615233
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615234
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 615235
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615236
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->l:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615237
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->o:Ljava/lang/String;

    .line 615238
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615239
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615240
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615241
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615242
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615243
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 615244
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z:Ljava/util/List;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z:Ljava/util/List;

    .line 615245
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;->z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
