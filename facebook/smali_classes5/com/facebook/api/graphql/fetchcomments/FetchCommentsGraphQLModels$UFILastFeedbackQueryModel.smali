.class public final Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/3cr;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x30b0e8ee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:I

.field private G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 615702
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 615701
    const-class v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 615699
    const/16 v0, 0x1d

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 615700
    return-void
.end method

.method private H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615697
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615698
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615695
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615696
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615693
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615694
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    return-object v0
.end method

.method private K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615621
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615622
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615641
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615642
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    return-object v0
.end method

.method private M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615639
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615640
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    return-object v0
.end method

.method private N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615637
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615638
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615635
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615636
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615633
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615634
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615631
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615632
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615629
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615630
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 615623
    iput p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F:I

    .line 615624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615625
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615626
    if-eqz v0, :cond_0

    .line 615627
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 615628
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615767
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615768
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615769
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615770
    if-eqz v0, :cond_0

    .line 615771
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615772
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615816
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615817
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615818
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615819
    if-eqz v0, :cond_0

    .line 615820
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615821
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615810
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615811
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615812
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615813
    if-eqz v0, :cond_0

    .line 615814
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615815
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615804
    iput-object p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615805
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615806
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615807
    if-eqz v0, :cond_0

    .line 615808
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 615809
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 615798
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->g:Z

    .line 615799
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615800
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615801
    if-eqz v0, :cond_0

    .line 615802
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615803
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 615792
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->k:Z

    .line 615793
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615794
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615795
    if-eqz v0, :cond_0

    .line 615796
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615797
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 615786
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p:Z

    .line 615787
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615788
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615789
    if-eqz v0, :cond_0

    .line 615790
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615791
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 615780
    iput-boolean p1, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->s:Z

    .line 615781
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 615782
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 615783
    if-eqz v0, :cond_0

    .line 615784
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 615785
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615779
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615766
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615778
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615777
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615776
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final F()I
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 615774
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615775
    iget v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F:I

    return v0
.end method

.method public final synthetic G()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615773
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 615643
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 615644
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->l()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 615645
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 615646
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 615647
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 615648
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 615649
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 615650
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 615651
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 615652
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 615653
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 615654
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v13

    .line 615655
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 615656
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 615657
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 615658
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 615659
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 615660
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 615661
    const/16 v20, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 615662
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->e:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615663
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->f:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615664
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->g:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615665
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->h:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615666
    const/16 v20, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->i:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615667
    const/16 v20, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->j:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615668
    const/16 v20, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->k:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615669
    const/16 v20, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->l:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615670
    const/16 v20, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->m:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 615671
    const/16 v20, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 615672
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 615673
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 615674
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 615675
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 615676
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 615677
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 615678
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 615679
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 615680
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 615681
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 615682
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 615683
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 615684
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 615685
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 615686
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615687
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615688
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615689
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 615690
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 615691
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 615692
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 615703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 615704
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 615705
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615706
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 615707
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615708
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->r:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 615709
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 615710
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615711
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 615712
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615713
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->u:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615714
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 615715
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615716
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 615717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615718
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615719
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 615720
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615721
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 615722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615723
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615724
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 615725
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615726
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 615727
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615728
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615729
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 615730
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 615731
    if-eqz v2, :cond_5

    .line 615732
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615733
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z:Ljava/util/List;

    move-object v1, v0

    .line 615734
    :cond_5
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 615735
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615736
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 615737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615738
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615739
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 615740
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615741
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->N()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 615742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615743
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->B:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 615744
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 615745
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615746
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 615747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615748
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->C:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    .line 615749
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 615750
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615751
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 615752
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615753
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->D:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 615754
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 615755
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615756
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->Q()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 615757
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615758
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->E:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615759
    :cond_a
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 615760
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615761
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->R()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 615762
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    .line 615763
    iput-object v0, v1, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->G:Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    .line 615764
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 615765
    if-nez v1, :cond_c

    :goto_0
    return-object p0

    :cond_c
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 615452
    new-instance v0, LX/5B4;

    invoke-direct {v0, p1}, LX/5B4;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615592
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 615578
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 615579
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->e:Z

    .line 615580
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->f:Z

    .line 615581
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->g:Z

    .line 615582
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->h:Z

    .line 615583
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->i:Z

    .line 615584
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->j:Z

    .line 615585
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->k:Z

    .line 615586
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->l:Z

    .line 615587
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->m:Z

    .line 615588
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p:Z

    .line 615589
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->s:Z

    .line 615590
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F:I

    .line 615591
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615526
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615527
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615529
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 615530
    :goto_0
    return-void

    .line 615531
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615532
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615533
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615534
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615535
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615536
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615537
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615538
    const/16 v0, 0xb

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615539
    :cond_2
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615540
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615542
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 615543
    :cond_3
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 615544
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    .line 615545
    if-eqz v0, :cond_9

    .line 615546
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615547
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615548
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 615549
    :cond_4
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 615550
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 615551
    if-eqz v0, :cond_9

    .line 615552
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615553
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615554
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615555
    :cond_5
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 615556
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    .line 615557
    if-eqz v0, :cond_9

    .line 615558
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615559
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615560
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615561
    :cond_6
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 615562
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615563
    if-eqz v0, :cond_9

    .line 615564
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615565
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615566
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615567
    :cond_7
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 615568
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615569
    if-eqz v0, :cond_9

    .line 615570
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615571
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615572
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615573
    :cond_8
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 615574
    invoke-virtual {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->F()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 615575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 615576
    const/16 v0, 0x1b

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 615577
    :cond_9
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 615517
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615518
    check-cast p2, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;)V

    .line 615519
    :cond_0
    :goto_0
    return-void

    .line 615520
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615521
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 615522
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615523
    check-cast p2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V

    goto :goto_0

    .line 615524
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615525
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 615466
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615467
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(Z)V

    .line 615468
    :cond_0
    :goto_0
    return-void

    .line 615469
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615470
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->b(Z)V

    goto :goto_0

    .line 615471
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615472
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->c(Z)V

    goto :goto_0

    .line 615473
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 615474
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->d(Z)V

    goto :goto_0

    .line 615475
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 615476
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    .line 615477
    if-eqz v0, :cond_0

    .line 615478
    if-eqz p3, :cond_5

    .line 615479
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    .line 615480
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a(I)V

    .line 615481
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->v:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    goto :goto_0

    .line 615482
    :cond_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;->a(I)V

    goto :goto_0

    .line 615483
    :cond_6
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 615484
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 615485
    if-eqz v0, :cond_0

    .line 615486
    if-eqz p3, :cond_7

    .line 615487
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    .line 615488
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 615489
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->w:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 615490
    :cond_7
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 615491
    :cond_8
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 615492
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    .line 615493
    if-eqz v0, :cond_0

    .line 615494
    if-eqz p3, :cond_9

    .line 615495
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    .line 615496
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a(I)V

    .line 615497
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->y:Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    goto/16 :goto_0

    .line 615498
    :cond_9
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;->a(I)V

    goto/16 :goto_0

    .line 615499
    :cond_a
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 615500
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615501
    if-eqz v0, :cond_0

    .line 615502
    if-eqz p3, :cond_b

    .line 615503
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615504
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    .line 615505
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 615506
    :cond_b
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    goto/16 :goto_0

    .line 615507
    :cond_c
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 615508
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 615509
    if-eqz v0, :cond_0

    .line 615510
    if-eqz p3, :cond_d

    .line 615511
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 615512
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    .line 615513
    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 615514
    :cond_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    goto/16 :goto_0

    .line 615515
    :cond_e
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615516
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 615464
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615465
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 615462
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615463
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 615459
    new-instance v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;-><init>()V

    .line 615460
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 615461
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 615457
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615458
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 615455
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615456
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 615453
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615454
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 615451
    const v0, -0x55e1ad4a

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 615449
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615450
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 615448
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 615595
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615596
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 615597
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615598
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615599
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->n:Ljava/lang/String;

    .line 615600
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 615601
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615602
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->p:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615593
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->q:Ljava/lang/String;

    .line 615594
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 615603
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615604
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->s:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615605
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->t:Ljava/lang/String;

    .line 615606
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615607
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->x:Ljava/lang/String;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->x:Ljava/lang/String;

    .line 615608
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615609
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->O()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 615610
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 615611
    iget-boolean v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->l:Z

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615612
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->o:Ljava/lang/String;

    .line 615613
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615614
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615615
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->I()Lcom/facebook/api/graphql/textwithentities/NewsFeedTextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithRangesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615616
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->J()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615617
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->K()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 615618
    invoke-direct {p0}, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->L()Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$FeedbackWithoutCommentsFieldsModel$SeenByModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 615619
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z:Ljava/util/List;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z:Ljava/util/List;

    .line 615620
    iget-object v0, p0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;->z:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
