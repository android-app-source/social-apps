.class public final Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x30bb5416
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:D

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 664863
    const-class v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 664862
    const-class v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 664860
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 664861
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664858
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->e:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->e:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 664859
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->e:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664856
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->f:Ljava/lang/String;

    .line 664857
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664854
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 664855
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664852
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->i:Ljava/lang/String;

    .line 664853
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664765
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664766
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664850
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->k:Ljava/lang/String;

    .line 664851
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664848
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 664849
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664846
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->m:Ljava/lang/String;

    .line 664847
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664844
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n:Ljava/lang/String;

    .line 664845
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664842
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664843
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664840
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 664841
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664838
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->q:Ljava/lang/String;

    .line 664839
    iget-object v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 17

    .prologue
    .line 664809
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 664810
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 664811
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 664812
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 664813
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->m()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 664814
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 664815
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 664816
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 664817
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->q()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 664818
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->r()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 664819
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 664820
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 664821
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->u()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 664822
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 664823
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v2}, LX/186;->b(II)V

    .line 664824
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 664825
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 664826
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->h:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 664827
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 664828
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 664829
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 664830
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 664831
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 664832
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 664833
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 664834
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 664835
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 664836
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 664837
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 664776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 664777
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 664778
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 664779
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j()Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 664780
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664781
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->e:Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$InnerApplicationFieldsModel$AndroidAppConfigModel;

    .line 664782
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 664783
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 664784
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 664785
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664786
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 664787
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 664788
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664789
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 664790
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664791
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664792
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 664793
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 664794
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p()Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 664795
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664796
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->l:Lcom/facebook/api/graphql/instantexperiences/InstantExperiencesModels$InstantExperiencesSettingFieldsModel;

    .line 664797
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 664798
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664799
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 664800
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664801
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 664802
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 664803
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 664804
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 664805
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    .line 664806
    iput-object v0, v1, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->p:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    .line 664807
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 664808
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 664775
    invoke-direct {p0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 664772
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 664773
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;->h:D

    .line 664774
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 664769
    new-instance v0, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/textwithentities/NewsFeedApplicationGraphQLModels$AppStorePlatformApplicationFieldsModel;-><init>()V

    .line 664770
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 664771
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 664768
    const v0, 0x3c92e467

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 664767
    const v0, -0x3ff252d0

    return v0
.end method
