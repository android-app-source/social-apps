.class public Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile p:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;


# instance fields
.field public final b:LX/0jU;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0aG;

.field private final f:LX/3mH;

.field public final g:LX/03V;

.field public final h:LX/0Zb;

.field public final i:LX/1CW;

.field public final j:LX/17V;

.field private final k:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/25G;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1LA;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0qn;

.field private final o:LX/1EQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 636108
    const-class v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0jU;LX/0Ot;LX/0aG;LX/0Or;LX/3mH;LX/03V;LX/0Zb;LX/1CW;LX/17V;LX/1Ck;LX/25G;LX/0Ot;LX/0qn;LX/1EQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0jU;",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/3mH;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "LX/1CW;",
            "LX/17V;",
            "LX/1Ck;",
            "LX/25G;",
            "LX/0Ot",
            "<",
            "LX/1LA;",
            ">;",
            "LX/0qn;",
            "LX/1EQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636093
    iput-object p1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    .line 636094
    iput-object p2, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    .line 636095
    iput-object p3, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->e:LX/0aG;

    .line 636096
    iput-object p4, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->c:LX/0Or;

    .line 636097
    iput-object p5, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->f:LX/3mH;

    .line 636098
    iput-object p6, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->g:LX/03V;

    .line 636099
    iput-object p7, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->h:LX/0Zb;

    .line 636100
    iput-object p8, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->i:LX/1CW;

    .line 636101
    iput-object p9, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->j:LX/17V;

    .line 636102
    iput-object p10, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->k:LX/1Ck;

    .line 636103
    iput-object p11, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->l:LX/25G;

    .line 636104
    iput-object p12, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->m:LX/0Ot;

    .line 636105
    iput-object p13, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->n:LX/0qn;

    .line 636106
    iput-object p14, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->o:LX/1EQ;

    .line 636107
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;
    .locals 3

    .prologue
    .line 636082
    sget-object v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->p:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    if-nez v0, :cond_1

    .line 636083
    const-class v1, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    monitor-enter v1

    .line 636084
    :try_start_0
    sget-object v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->p:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 636085
    if-eqz v2, :cond_0

    .line 636086
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v0

    sput-object v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->p:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636087
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 636088
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 636089
    :cond_1
    sget-object v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->p:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    return-object v0

    .line 636090
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 636091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 635800
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 635801
    const-string v0, "blacklistGroupsYouShouldJoinParamsKey"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 635802
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->e:LX/0aG;

    const-string v1, "group_blacklist_groups_you_should_join"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x2d7168ae

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 635803
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 636057
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v0

    .line 636058
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 636059
    iput-object v1, v0, LX/21A;->a:LX/162;

    .line 636060
    move-object v1, v0

    .line 636061
    const-string v2, "newsfeed_ufi"

    .line 636062
    iput-object v2, v1, LX/21A;->b:Ljava/lang/String;

    .line 636063
    move-object v1, v1

    .line 636064
    invoke-static {}, LX/0Yi;->v()Ljava/lang/String;

    move-result-object v2

    .line 636065
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 636066
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->o:LX/1EQ;

    invoke-virtual {v1, p4, v0}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 636067
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    .line 636068
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    .line 636069
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a()LX/5HA;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v4

    .line 636070
    iput-boolean v4, v1, LX/5HA;->b:Z

    .line 636071
    move-object v1, v1

    .line 636072
    iput-object p3, v1, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 636073
    move-object v1, v1

    .line 636074
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 636075
    iput-object v0, v1, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 636076
    move-object v0, v1

    .line 636077
    iput-object p2, v0, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 636078
    move-object v0, v0

    .line 636079
    invoke-virtual {v0}, LX/5HA;->a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    move-result-object v0

    .line 636080
    iget-object v7, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->k:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "task_key_newsfeed_set_like_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/ADZ;

    invoke-direct {v9, p0, v0}, LX/ADZ;-><init>(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V

    new-instance v0, LX/ADa;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/ADa;-><init>(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v7, v8, v9, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 636081
    return-void
.end method

.method public static a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V
    .locals 6

    .prologue
    .line 636055
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->f:LX/3mH;

    move-object v1, p1

    move-object v2, p0

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/3mH;->a(Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/String;LX/AkV;)V

    .line 636056
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;
    .locals 15

    .prologue
    .line 636053
    new-instance v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v1

    check-cast v1, LX/0jU;

    const/16 v2, 0x473

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const/16 v4, 0x123

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v5

    check-cast v5, LX/3mH;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {p0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v8

    check-cast v8, LX/1CW;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v9

    check-cast v9, LX/17V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {p0}, LX/25G;->b(LX/0QB;)LX/25G;

    move-result-object v11

    check-cast v11, LX/25G;

    const/16 v12, 0x5f5

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v13

    check-cast v13, LX/0qn;

    invoke-static {p0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v14

    check-cast v14, LX/1EQ;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;-><init>(LX/0jU;LX/0Ot;LX/0aG;LX/0Or;LX/3mH;LX/03V;LX/0Zb;LX/1CW;LX/17V;LX/1Ck;LX/25G;LX/0Ot;LX/0qn;LX/1EQ;)V

    .line 636054
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 636017
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    .line 636018
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 636019
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->o()LX/0Px;

    move-result-object v5

    .line 636020
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 636021
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 636022
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 636023
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 636024
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    invoke-static {v2}, LX/4Xw;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;)LX/4Xw;

    move-result-object v2

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 636025
    iput-object v3, v2, LX/4Xw;->b:LX/0Px;

    .line 636026
    move-object v2, v2

    .line 636027
    invoke-virtual {v2}, LX/4Xw;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v2

    .line 636028
    invoke-static {p1}, LX/4Xu;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/4Xu;

    move-result-object v3

    .line 636029
    iput-object v2, v3, LX/4Xu;->d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 636030
    move-object v2, v3

    .line 636031
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->R()LX/0Px;

    move-result-object v3

    invoke-static {v3, p2}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 636032
    iput-object v3, v2, LX/4Xu;->q:LX/0Px;

    .line 636033
    move-object v2, v2

    .line 636034
    iget-object v3, v0, LX/189;->i:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 636035
    iput-wide v4, v2, LX/4Xu;->l:J

    .line 636036
    move-object v2, v2

    .line 636037
    invoke-virtual {v2}, LX/4Xu;->a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    move-result-object v2

    .line 636038
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I_()I

    move-result v3

    invoke-static {v2, v3}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 636039
    const/4 v4, 0x0

    .line 636040
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v3

    if-nez v3, :cond_5

    :cond_2
    move v3, v4

    .line 636041
    :goto_1
    move v3, v3

    .line 636042
    if-eqz v3, :cond_4

    :goto_2
    move-object v0, v2

    .line 636043
    if-eqz v0, :cond_3

    .line 636044
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    .line 636045
    invoke-static {v1, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 636046
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 636047
    :cond_3
    return-object v0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 636048
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_3
    if-ge v5, v7, :cond_7

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 636049
    invoke-static {v3}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v4

    .line 636050
    goto :goto_1

    .line 636051
    :cond_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 636052
    :cond_7
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 635983
    if-nez p1, :cond_1

    .line 635984
    :cond_0
    :goto_0
    return-void

    .line 635985
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 635986
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 635987
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 635988
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 635989
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    move v3, v1

    .line 635990
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 635991
    if-nez v1, :cond_3

    .line 635992
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Like a story with no feedback id is not supported yet"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 635993
    :cond_2
    const/4 v1, 0x0

    move v3, v1

    goto :goto_1

    .line 635994
    :cond_3
    sget-object v2, LX/AkV;->EVENT:LX/AkV;

    invoke-static {p0, v1, v3, v9, v2}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V

    .line 635995
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/189;

    invoke-virtual {v1, p1, v4, v3}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 635996
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 635997
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 635998
    invoke-virtual {p1, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 635999
    invoke-static {v2}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 636000
    if-nez v2, :cond_5

    .line 636001
    iget-object v6, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->g:LX/03V;

    const-string v7, "FeedUnitCacheMutator"

    const-string v8, "Feedbackable should either be a FeedUnit or it\'s root should be a FeedUnit"

    invoke-virtual {v6, v7, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 636002
    :goto_2
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    instance-of v6, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v6, :cond_4

    .line 636003
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    .line 636004
    iget-object v7, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->m:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1LA;

    sget-object v8, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v2, v6, v8}, LX/1LA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 636005
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 636006
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    .line 636007
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "empty feedback for type "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 636008
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 636009
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " pubstate="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->n:LX/0qn;

    invoke-virtual {v2, v1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 636010
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->g:LX/03V;

    const-string v2, "like_attempt_empty_feedback"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 636011
    :cond_5
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    .line 636012
    iget-object v6, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v6, v2}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    goto/16 :goto_2

    .line 636013
    :cond_6
    iget-object v6, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v6, v2}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    goto/16 :goto_2

    .line 636014
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    sget-object v6, LX/AkV;->ATTEMPT:LX/AkV;

    invoke-static {p0, v1, v3, v9, v6}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V

    .line 636015
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->h:LX/0Zb;

    const-string v3, "newsfeed_story_like"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    const-string v8, "native_newsfeed"

    invoke-static {v3, v6, v7, v8}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 636016
    invoke-direct {p0, v5, v2, v4, v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Ljava/lang/String;)V
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 635941
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    .line 635942
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 635943
    invoke-static {p1}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 635944
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 635945
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 635946
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 635947
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 635948
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 635949
    new-instance v9, LX/4Vx;

    invoke-direct {v9}, LX/4Vx;-><init>()V

    .line 635950
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 635951
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->b:Ljava/lang/String;

    .line 635952
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->E_()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->c:Ljava/lang/String;

    .line 635953
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->F_()J

    move-result-wide v10

    iput-wide v10, v9, LX/4Vx;->d:J

    .line 635954
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->C_()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->e:Ljava/lang/String;

    .line 635955
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->f:LX/0Px;

    .line 635956
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->u()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->g:Ljava/lang/String;

    .line 635957
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->A()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->h:LX/0Px;

    .line 635958
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->v()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->i:Ljava/lang/String;

    .line 635959
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->w()I

    move-result v8

    iput v8, v9, LX/4Vx;->j:I

    .line 635960
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->k:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 635961
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->y()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->l:Ljava/lang/String;

    .line 635962
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 635963
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Vx;->n:Ljava/lang/String;

    .line 635964
    invoke-static {v9, p1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 635965
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->L_()LX/0x2;

    move-result-object v8

    invoke-virtual {v8}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0x2;

    iput-object v8, v9, LX/4Vx;->o:LX/0x2;

    .line 635966
    move-object v3, v9

    .line 635967
    iput-object v2, v3, LX/4Vx;->f:LX/0Px;

    .line 635968
    move-object v3, v3

    .line 635969
    iget-object v4, v0, LX/189;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 635970
    iput-wide v4, v3, LX/4Vx;->d:J

    .line 635971
    move-object v3, v3

    .line 635972
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->A()LX/0Px;

    move-result-object v4

    invoke-static {v4, p2}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 635973
    iput-object v4, v3, LX/4Vx;->h:LX/0Px;

    .line 635974
    move-object v3, v3

    .line 635975
    new-instance v4, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-direct {v4, v3}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;-><init>(LX/4Vx;)V

    .line 635976
    move-object v3, v4

    .line 635977
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->I_()I

    move-result v4

    invoke-static {v2, v4}, LX/189;->a(II)I

    move-result v2

    invoke-static {v3, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 635978
    move-object v0, v3

    .line 635979
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 635980
    :cond_2
    :goto_1
    return-void

    .line 635981
    :cond_3
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)V

    .line 635982
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 635913
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    .line 635914
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 635915
    invoke-static {p1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 635916
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 635917
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 635918
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 635919
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 635920
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 635921
    invoke-static {p1}, LX/4Xg;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/4Xg;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v4

    invoke-static {v4}, LX/4Xh;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;)LX/4Xh;

    move-result-object v4

    .line 635922
    iput-object v2, v4, LX/4Xh;->b:LX/0Px;

    .line 635923
    move-object v4, v4

    .line 635924
    invoke-virtual {v4}, LX/4Xh;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v4

    .line 635925
    iput-object v4, v3, LX/4Xg;->i:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 635926
    move-object v3, v3

    .line 635927
    iget-object v4, v0, LX/189;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 635928
    iput-wide v4, v3, LX/4Xg;->d:J

    .line 635929
    move-object v3, v3

    .line 635930
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->x()LX/0Px;

    move-result-object v4

    invoke-static {v4, p2}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 635931
    iput-object v4, v3, LX/4Xg;->g:LX/0Px;

    .line 635932
    move-object v3, v3

    .line 635933
    invoke-virtual {v3}, LX/4Xg;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    move-result-object v3

    .line 635934
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->I_()I

    move-result v4

    invoke-static {v2, v4}, LX/189;->a(II)I

    move-result v2

    invoke-static {v3, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 635935
    move-object v0, v3

    .line 635936
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 635937
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    .line 635938
    invoke-static {v1, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 635939
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 635940
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;Ljava/lang/String;)V
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 635841
    invoke-static {p0, p2}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;)V

    .line 635842
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_3

    .line 635843
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    .line 635844
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 635845
    invoke-static {p1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 635846
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 635847
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 635848
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 635849
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 635850
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 635851
    new-instance v9, LX/4Wr;

    invoke-direct {v9}, LX/4Wr;-><init>()V

    .line 635852
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 635853
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->b:Ljava/lang/String;

    .line 635854
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->c:Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    .line 635855
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->E_()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->d:Ljava/lang/String;

    .line 635856
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->F_()J

    move-result-wide v10

    iput-wide v10, v9, LX/4Wr;->e:J

    .line 635857
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->k()I

    move-result v8

    iput v8, v9, LX/4Wr;->f:I

    .line 635858
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->g:LX/0Px;

    .line 635859
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 635860
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->r()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->i:Ljava/lang/String;

    .line 635861
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->j:LX/0Px;

    .line 635862
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->v()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->k:LX/0Px;

    .line 635863
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->l:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 635864
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->t()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->m:Ljava/lang/String;

    .line 635865
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 635866
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wr;->o:Ljava/lang/String;

    .line 635867
    invoke-static {v9, p1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 635868
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->L_()LX/0x2;

    move-result-object v8

    invoke-virtual {v8}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0x2;

    iput-object v8, v9, LX/4Wr;->p:LX/0x2;

    .line 635869
    move-object v3, v9

    .line 635870
    iput-object v2, v3, LX/4Wr;->j:LX/0Px;

    .line 635871
    move-object v3, v3

    .line 635872
    iput-object v2, v3, LX/4Wr;->g:LX/0Px;

    .line 635873
    move-object v3, v3

    .line 635874
    iget-object v4, v0, LX/189;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 635875
    iput-wide v4, v3, LX/4Wr;->e:J

    .line 635876
    move-object v3, v3

    .line 635877
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->v()LX/0Px;

    move-result-object v4

    invoke-static {v4, p2}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 635878
    iput-object v4, v3, LX/4Wr;->k:LX/0Px;

    .line 635879
    move-object v3, v3

    .line 635880
    new-instance v4, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    invoke-direct {v4, v3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;-><init>(LX/4Wr;)V

    .line 635881
    move-object v3, v4

    .line 635882
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->I_()I

    move-result v4

    invoke-static {v2, v4}, LX/189;->a(II)I

    move-result v2

    invoke-static {v3, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 635883
    move-object v0, v3

    .line 635884
    :goto_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 635885
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    .line 635886
    invoke-static {v1, v0}, LX/0jU;->f(LX/0jU;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 635887
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 635888
    :cond_2
    return-void

    .line 635889
    :cond_3
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    .line 635890
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 635891
    invoke-static {p1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 635892
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_5

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    .line 635893
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 635894
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 635895
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 635896
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 635897
    invoke-static {p1}, LX/4Xj;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/4Xj;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v4

    invoke-static {v4}, LX/4Xk;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;)LX/4Xk;

    move-result-object v4

    .line 635898
    iput-object v2, v4, LX/4Xk;->b:LX/0Px;

    .line 635899
    move-object v4, v4

    .line 635900
    invoke-virtual {v4}, LX/4Xk;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v4

    .line 635901
    iput-object v4, v3, LX/4Xj;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 635902
    move-object v3, v3

    .line 635903
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->w()LX/0Px;

    move-result-object v4

    invoke-static {v4, p2}, LX/189;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 635904
    iput-object v4, v3, LX/4Xj;->h:LX/0Px;

    .line 635905
    move-object v3, v3

    .line 635906
    iget-object v4, v0, LX/189;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 635907
    iput-wide v4, v3, LX/4Xj;->e:J

    .line 635908
    move-object v3, v3

    .line 635909
    invoke-virtual {v3}, LX/4Xj;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    move-result-object v3

    .line 635910
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->I_()I

    move-result v4

    invoke-static {v2, v4}, LX/189;->a(II)I

    move-result v2

    invoke-static {v3, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 635911
    move-object v0, v3

    .line 635912
    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 635804
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v0, p1}, LX/0jU;->d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 635805
    if-nez v1, :cond_1

    .line 635806
    :cond_0
    :goto_0
    return-void

    .line 635807
    :cond_1
    iget-object v0, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 635808
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v4

    .line 635809
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 635810
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 635811
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 635812
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 635813
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 635814
    :cond_3
    new-instance v9, LX/4Wq;

    invoke-direct {v9}, LX/4Wq;-><init>()V

    .line 635815
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 635816
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->b:Ljava/lang/String;

    .line 635817
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->E_()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->c:Ljava/lang/String;

    .line 635818
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->F_()J

    move-result-wide v10

    iput-wide v10, v9, LX/4Wq;->d:J

    .line 635819
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->n()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->e:LX/0Px;

    .line 635820
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 635821
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->r()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->g:Ljava/lang/String;

    .line 635822
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->s()LX/0Px;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->h:LX/0Px;

    .line 635823
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->t()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->i:Ljava/lang/String;

    .line 635824
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 635825
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4Wq;->k:Ljava/lang/String;

    .line 635826
    invoke-static {v9, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 635827
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->L_()LX/0x2;

    move-result-object v8

    invoke-virtual {v8}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0x2;

    iput-object v8, v9, LX/4Wq;->l:LX/0x2;

    .line 635828
    move-object v2, v9

    .line 635829
    iget-object v3, v0, LX/189;->i:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    .line 635830
    iput-wide v6, v2, LX/4Wq;->d:J

    .line 635831
    move-object v2, v2

    .line 635832
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 635833
    iput-object v3, v2, LX/4Wq;->h:LX/0Px;

    .line 635834
    move-object v2, v2

    .line 635835
    new-instance v3, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;-><init>(LX/4Wq;)V

    .line 635836
    move-object v2, v3

    .line 635837
    move-object v0, v2

    .line 635838
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 635839
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)V

    .line 635840
    iget-object v1, p0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    goto/16 :goto_0
.end method
