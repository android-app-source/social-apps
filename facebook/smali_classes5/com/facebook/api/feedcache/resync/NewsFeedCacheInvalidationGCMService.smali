.class public Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;
.super LX/2fD;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/HlG;

.field private c:LX/HlH;

.field private d:LX/0qX;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 570773
    invoke-direct {p0}, LX/2fD;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 570774
    :try_start_0
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    const-class v1, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/2E6;->b(LX/2E6;Ljava/lang/String;)V

    invoke-static {v0}, LX/2E6;->a(LX/2E6;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 570775
    :goto_0
    return-void

    .line 570776
    :catch_0
    move-exception v0

    .line 570777
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "NewsFeedCacheInvalidationGCMService"

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p0, v1, v0}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    goto :goto_0

    .line 570778
    :catch_1
    move-exception v0

    .line 570779
    const-string v1, "NewsFeedCacheInvalidationGCMService"

    const-string v2, "Unexpected Security Exception. Nothing to be done! Check t8784969"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const-string v4, "scheduler_action"

    const-string v1, "CANCEL_ALL"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "component"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v4, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 7

    .prologue
    const-wide/16 v0, 0x3c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 570756
    sget-object v2, LX/1vX;->c:LX/1vX;

    move-object v2, v2

    .line 570757
    invoke-virtual {v2, p0}, LX/1od;->a(Landroid/content/Context;)I

    move-result v2

    .line 570758
    if-eqz v2, :cond_0

    .line 570759
    :goto_0
    return-void

    .line 570760
    :cond_0
    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    move-wide p1, v0

    .line 570761
    :cond_1
    const-wide/16 v0, 0x1e

    add-long/2addr v0, p1

    .line 570762
    new-instance v2, LX/2E9;

    invoke-direct {v2}, LX/2E9;-><init>()V

    const-class v3, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;

    invoke-virtual {v2, v3}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    move-result-object v2

    const-string v3, "NewsFeedCacheInvalidationGCMService"

    iput-object v3, v2, LX/2EA;->c:Ljava/lang/String;

    move-object v2, v2

    .line 570763
    invoke-virtual {v2, p1, p2, v0, v1}, LX/2E9;->a(JJ)LX/2E9;

    move-result-object v0

    iput v4, v0, LX/2EA;->a:I

    move-object v0, v0

    .line 570764
    iput-boolean v5, v0, LX/2EA;->e:Z

    move-object v0, v0

    .line 570765
    iput-boolean v5, v0, LX/2EA;->d:Z

    move-object v0, v0

    .line 570766
    iput-boolean v4, v0, LX/2E9;->f:Z

    move-object v0, v0

    .line 570767
    invoke-virtual {v0}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    .line 570768
    :try_start_0
    invoke-static {p0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 570769
    :catch_0
    move-exception v0

    .line 570770
    const-string v1, "NewsFeedCacheInvalidationGCMService"

    const-string v2, "GcmNetworkManager bug, packageManager cannot find the registered component"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 570771
    :catch_1
    move-exception v0

    .line 570772
    const-string v1, "NewsFeedCacheInvalidationGCMService"

    const-string v2, "Unexpected Security Exception. Nothing to be done! Check t8784969"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;LX/HlG;LX/HlH;LX/0qX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 570751
    iput-object p2, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->b:LX/HlG;

    .line 570752
    iput-object p3, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->c:LX/HlH;

    .line 570753
    iput-object p1, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a:Landroid/content/Context;

    .line 570754
    iput-object p4, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->d:LX/0qX;

    .line 570755
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;

    const-class v0, Landroid/content/Context;

    invoke-interface {v3, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v3}, LX/HlG;->a(LX/0QB;)LX/HlG;

    move-result-object v1

    check-cast v1, LX/HlG;

    invoke-static {v3}, LX/HlH;->a(LX/0QB;)LX/HlH;

    move-result-object v2

    check-cast v2, LX/HlH;

    invoke-static {v3}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v3

    check-cast v3, LX/0qX;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a(Landroid/content/Context;LX/HlG;LX/HlH;LX/0qX;)V

    return-void
.end method


# virtual methods
.method public final a(LX/2fH;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    .line 570717
    iget-object v2, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->c:LX/HlH;

    .line 570718
    invoke-static {v2}, LX/HlH;->c(LX/HlH;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 570719
    const/4 v3, 0x2

    .line 570720
    :goto_0
    move v2, v3

    .line 570721
    if-ne v0, v2, :cond_1

    .line 570722
    :cond_0
    :goto_1
    return v0

    .line 570723
    :cond_1
    if-ne v1, v2, :cond_2

    move v0, v1

    .line 570724
    goto :goto_1

    .line 570725
    :cond_2
    iget-object v1, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->b:LX/HlG;

    const/4 v2, 0x0

    .line 570726
    iget-object v3, v1, LX/HlG;->b:LX/HlJ;

    .line 570727
    iget-object v4, v3, LX/HlJ;->b:LX/0Zb;

    const-string v5, "android_async_feed_cache_invalidation"

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    move-object v4, v4

    .line 570728
    :try_start_0
    iget-object v3, v1, LX/HlG;->c:LX/HlI;

    invoke-virtual {v3}, LX/HlI;->a()Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 570729
    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 570730
    :cond_3
    iget-object v3, v1, LX/HlG;->b:LX/HlJ;

    const-string v5, "no_cache_found"

    invoke-virtual {v3, v4, v5}, LX/HlJ;->a(LX/0oG;Ljava/lang/String;)V

    .line 570731
    :goto_2
    move v1, v2

    .line 570732
    if-eqz v1, :cond_0

    .line 570733
    iget-object v0, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->d:LX/0qX;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, LX/0qX;->c(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3c

    mul-long/2addr v0, v2

    .line 570734
    iget-object v2, p0, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a(Landroid/content/Context;J)V

    .line 570735
    const/4 v0, 0x0

    goto :goto_1

    .line 570736
    :cond_4
    iget-object v3, v2, LX/HlH;->c:LX/0Uq;

    invoke-virtual {v3}, LX/0Uq;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, v2, LX/HlH;->b:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->j()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 570737
    if-nez v3, :cond_5

    .line 570738
    const/4 v3, 0x1

    goto :goto_0

    .line 570739
    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    .line 570740
    :catch_0
    move-exception v3

    .line 570741
    iget-object v5, v1, LX/HlG;->b:LX/HlJ;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v4, v3}, LX/HlJ;->a(LX/0oG;Ljava/lang/String;)V

    goto :goto_2

    .line 570742
    :cond_7
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 570743
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 570744
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 570745
    :cond_8
    new-instance v2, LX/7fY;

    invoke-direct {v2}, LX/7fY;-><init>()V

    move-object v2, v2

    .line 570746
    const-string p1, "ids"

    invoke-virtual {v2, p1, v5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 570747
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 570748
    iget-object v5, v1, LX/HlG;->a:LX/0tX;

    invoke-virtual {v5, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    invoke-static {v2}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 570749
    new-instance v5, LX/HlF;

    invoke-direct {v5, v1, v3, v4}, LX/HlF;-><init>(LX/HlG;Ljava/util/Map;LX/0oG;)V

    iget-object v3, v1, LX/HlG;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v5, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 570750
    const/4 v2, 0x1

    goto/16 :goto_2
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5b50fd1e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 570715
    invoke-static {p0, p0}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 570716
    const/16 v1, 0x25

    const v2, -0x7caeebdc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x3bd021db

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 570708
    if-nez p1, :cond_0

    .line 570709
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Received a null intent, did you ever return START_STICKY?"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x2

    const/16 v4, 0x25

    const v5, 0x4f1cd5bf    # 2.63125376E9f

    invoke-static {v3, v4, v5, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570710
    :catch_0
    move-exception v1

    .line 570711
    const-string v3, "NewsFeedCacheInvalidationGCMService"

    const-string v4, "Unexpected service start parameters"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570712
    invoke-virtual {p0, p3}, Lcom/facebook/api/feedcache/resync/NewsFeedCacheInvalidationGCMService;->stopSelf(I)V

    .line 570713
    const v1, 0x6b6f8b63

    invoke-static {v1, v2}, LX/02F;->d(II)V

    :goto_0
    return v0

    .line 570714
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, LX/2fD;->onStartCommand(Landroid/content/Intent;II)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    const v1, 0xa18d032

    invoke-static {v1, v2}, LX/02F;->d(II)V

    goto :goto_0
.end method
