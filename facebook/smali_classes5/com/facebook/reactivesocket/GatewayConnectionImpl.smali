.class public Lcom/facebook/reactivesocket/GatewayConnectionImpl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 803770
    const-string v0, "reactivesocket"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 803771
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 803763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803764
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->d:Z

    .line 803765
    iput-object p1, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->a:Ljava/lang/String;

    .line 803766
    iput-object p2, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->b:Ljava/lang/String;

    .line 803767
    iput-object p3, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->c:Ljava/lang/String;

    .line 803768
    invoke-static {p1, p2, p3}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->initHybrid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 803769
    return-void
.end method

.method public static synthetic a(Lcom/facebook/reactivesocket/GatewayConnectionImpl;)V
    .locals 0

    .prologue
    .line 803762
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeRunLoop()V

    return-void
.end method

.method private declared-synchronized c()V
    .locals 3

    .prologue
    .line 803755
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->d:Z

    if-nez v0, :cond_0

    .line 803756
    new-instance v0, Lcom/facebook/reactivesocket/GatewayConnectionImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl$1;-><init>(Lcom/facebook/reactivesocket/GatewayConnectionImpl;)V

    const-string v1, "reactivesocket"

    const v2, -0x1048c25a

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 803757
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeWaitForStart()V

    .line 803758
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->d:Z

    .line 803759
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeReconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803760
    monitor-exit p0

    return-void

    .line 803761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native initHybrid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private native nativeDisconnect()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private native nativeReconnect()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private native nativeRunLoop()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private native nativeShutdown()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method private native nativeSubscribe(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;)Lcom/facebook/reactivesocket/Subscription;
.end method

.method private native nativeWaitForStart()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;)Lcom/facebook/reactivesocket/Subscription;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 803753
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->c()V

    .line 803754
    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeSubscribe(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/reactivesocket/GatewayCallback;)Lcom/facebook/reactivesocket/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 803751
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeDisconnect()V

    .line 803752
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 803749
    invoke-direct {p0}, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->nativeShutdown()V

    .line 803750
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 803748
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GatewayConnectionImpl["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
