.class public final Lcom/facebook/reactivesocket/GatewayConnectionImpl$SubscriptionImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/reactivesocket/Subscription;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;


# direct methods
.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 803745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803746
    iput-object p1, p0, Lcom/facebook/reactivesocket/GatewayConnectionImpl$SubscriptionImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 803747
    return-void
.end method


# virtual methods
.method public native cancel()V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method

.method public native request(I)V
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation
.end method
