.class public Lcom/facebook/feedback/ui/FeedbackControllerParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/feedback/ui/FeedbackControllerParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/feedback/ui/FeedbackControllerParams;


# instance fields
.field private final b:Ljava/lang/Boolean;

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 629148
    new-instance v0, LX/3iI;

    invoke-direct {v0}, LX/3iI;-><init>()V

    invoke-virtual {v0}, LX/3iI;->a()Lcom/facebook/feedback/ui/FeedbackControllerParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    .line 629149
    new-instance v0, LX/3iJ;

    invoke-direct {v0}, LX/3iJ;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3iI;)V
    .locals 1

    .prologue
    .line 629144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629145
    iget-boolean v0, p1, LX/3iI;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->b:Ljava/lang/Boolean;

    .line 629146
    iget-boolean v0, p1, LX/3iI;->b:Z

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->c:Z

    .line 629147
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 629140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629141
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->b:Ljava/lang/Boolean;

    .line 629142
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->c:Z

    .line 629143
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 629134
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 629139
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->c:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 629138
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 629135
    iget-object v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 629136
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 629137
    return-void
.end method
