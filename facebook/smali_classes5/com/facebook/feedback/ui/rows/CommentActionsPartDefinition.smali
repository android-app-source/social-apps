.class public Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "Ljava/util/List",
        "<",
        "Landroid/text/Spannable;",
        ">;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/CommentActionsView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field private final c:LX/9Hj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591622
    new-instance v0, LX/3Wx;

    invoke-direct {v0}, LX/3Wx;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/9Hj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591590
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591591
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591592
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->c:LX/9Hj;

    .line 591593
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;
    .locals 5

    .prologue
    .line 591611
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    monitor-enter v1

    .line 591612
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591613
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591616
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, LX/9Hj;->a(LX/0QB;)LX/9Hj;

    move-result-object v4

    check-cast v4, LX/9Hj;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/9Hj;)V

    .line 591617
    move-object v0, p0

    .line 591618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 591610
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 591605
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    .line 591606
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 591607
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591608
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsPartDefinition;->c:LX/9Hj;

    invoke-virtual {v0, p2, p3}, LX/9Hj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 591609
    :cond_0
    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2b53292a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591595
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/util/List;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;

    .line 591596
    sget-object v1, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p1}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 591597
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 591598
    iget v2, v1, LX/9FD;->a:I

    move v1, v2

    .line 591599
    :goto_0
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->getPaddingRight()I

    move-result p0

    invoke-virtual {p4, v2, v4, p0, v1}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->setPadding(IIII)V

    .line 591600
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p2, v1}, LX/8qs;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentActionsView;->setMetadataText(Ljava/lang/CharSequence;)V

    .line 591601
    const/16 v1, 0x1f

    const v2, -0x564a591f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 591602
    :cond_0
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 591603
    iget v2, v1, LX/9FD;->b:I

    move v1, v2

    .line 591604
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591594
    const/4 v0, 0x1

    return v0
.end method
