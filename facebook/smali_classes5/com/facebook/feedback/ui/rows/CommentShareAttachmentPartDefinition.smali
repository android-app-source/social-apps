.class public Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9HD;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591977
    new-instance v0, LX/3X3;

    invoke-direct {v0}, LX/3X3;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591974
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591975
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591976
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 591963
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;

    monitor-enter v1

    .line 591964
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591965
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591966
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591967
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591968
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V

    .line 591969
    move-object v0, p0

    .line 591970
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591971
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591972
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591973
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9HD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591949
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 591958
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591959
    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 591960
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591961
    const/4 v0, 0x0

    return-object v0

    .line 591962
    :cond_0
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7dd19ea5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591953
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/9HD;

    .line 591954
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 591955
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591956
    iget-object v2, p4, LX/9HD;->b:LX/8xP;

    iget-object p1, p4, LX/9HD;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v2, p1, v1}, LX/8xP;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 591957
    const/16 v1, 0x1f

    const v2, -0x316b953

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591950
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591951
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591952
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
