.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9GC;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H9;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H9;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private final e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

.field private final f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592266
    new-instance v0, LX/3XA;

    invoke-direct {v0}, LX/3XA;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592267
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592268
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->b:Landroid/content/Context;

    .line 592269
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    .line 592270
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592271
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    .line 592272
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    .line 592273
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 592274
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 592275
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    monitor-enter v1

    .line 592276
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592277
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592280
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 592281
    move-object v0, v3

    .line 592282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592286
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 592287
    check-cast p2, LX/9GC;

    .line 592288
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9GC;->c:I

    iget-object v3, p2, LX/9GC;->d:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592289
    const v0, 0x7f0d09e8

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    iget-object v2, p2, LX/9GC;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592290
    const v0, 0x7f0d09e7

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->b:Landroid/content/Context;

    const v3, 0x7f081242

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592291
    const v0, 0x7f0d09eb

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    new-instance v2, LX/9G4;

    iget-object v3, p2, LX/9GC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9GC;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-direct {v2, v3, v4}, LX/9G4;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592292
    const v0, 0x7f0d09ea

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingAttachmentPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    new-instance v2, LX/9GG;

    iget-object v3, p2, LX/9GC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9GC;->b:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, LX/9GG;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592293
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592294
    const/4 v0, 0x1

    return v0
.end method
