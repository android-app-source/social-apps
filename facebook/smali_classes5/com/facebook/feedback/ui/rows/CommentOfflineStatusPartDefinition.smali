.class public Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        "Ljava/lang/CharSequence;",
        "Lcom/facebook/feedback/ui/environment/HasCommentStylingResolver;",
        "Lcom/facebook/widget/FlowLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field private final c:LX/3iM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591561
    new-instance v0, LX/3Ww;

    invoke-direct {v0}, LX/3Ww;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/3iM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591562
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591563
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591564
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->c:LX/3iM;

    .line 591565
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;
    .locals 5

    .prologue
    .line 591566
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    monitor-enter v1

    .line 591567
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591568
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591569
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591570
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591571
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v4

    check-cast v4, LX/3iM;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/3iM;)V

    .line 591572
    move-object v0, p0

    .line 591573
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591574
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591575
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591576
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 591577
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 591578
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v1, LX/9GN;

    sget-object v2, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-direct {v1, v2}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591579
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x47cc9790

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591580
    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/widget/FlowLayout;

    .line 591581
    invoke-virtual {p4}, Lcom/facebook/widget/FlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p4}, Lcom/facebook/widget/FlowLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p4}, Lcom/facebook/widget/FlowLayout;->getPaddingRight()I

    move-result p0

    .line 591582
    iget-object p1, p3, LX/9FA;->m:LX/9FD;

    move-object p1, p1

    .line 591583
    iget p3, p1, LX/9FD;->a:I

    move p1, p3

    .line 591584
    invoke-virtual {p4, v1, v2, p0, p1}, Lcom/facebook/widget/FlowLayout;->setPadding(IIII)V

    .line 591585
    const/16 v1, 0x1f

    const v2, 0x1693a77b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 591586
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 591587
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentOfflineStatusPartDefinition;->c:LX/3iM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
