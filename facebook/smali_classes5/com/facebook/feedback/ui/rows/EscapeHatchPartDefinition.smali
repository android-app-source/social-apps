.class public Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Gu;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592621
    new-instance v0, LX/3XG;

    invoke-direct {v0}, LX/3XG;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592622
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592623
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;
    .locals 3

    .prologue
    .line 592624
    const-class v1, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;

    monitor-enter v1

    .line 592625
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592626
    sput-object v2, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592627
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592628
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 592629
    new-instance v0, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;-><init>()V

    .line 592630
    move-object v0, v0

    .line 592631
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592632
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592633
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592634
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/9Gu;)Z
    .locals 3

    .prologue
    .line 592635
    iget-object v0, p0, LX/9Gu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592636
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 592637
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 592638
    iget-object v1, p0, LX/9Gu;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 592639
    iget-object v2, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 592640
    if-eqz v2, :cond_0

    .line 592641
    iget-object v2, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 592642
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592643
    sget-object v0, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x756af923

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592644
    check-cast p1, LX/9Gu;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    .line 592645
    iget-object v1, p1, LX/9Gu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592646
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 592647
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p1, LX/9Gu;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 592648
    iget-object p0, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object p0, p0

    .line 592649
    invoke-virtual {p4, v1, v2, p0}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/api/ufiservices/FetchSingleCommentParams;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 592650
    const/16 v1, 0x1f

    const v2, -0x23bd00b4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592651
    check-cast p1, LX/9Gu;

    .line 592652
    invoke-static {p1}, Lcom/facebook/feedback/ui/rows/EscapeHatchPartDefinition;->a(LX/9Gu;)Z

    move-result v0

    return v0
.end method
