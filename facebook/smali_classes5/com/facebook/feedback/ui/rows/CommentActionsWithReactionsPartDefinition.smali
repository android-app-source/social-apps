.class public Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "LX/9FR;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static q:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field private final c:LX/8qs;

.field private final d:LX/1zf;

.field private final e:LX/20w;

.field private final f:LX/0tH;

.field private final g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final h:Lcom/facebook/content/SecureContextHelper;

.field private final i:LX/154;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1za;

.field public final l:LX/20l;

.field public final m:LX/0iA;

.field public final n:LX/20m;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/9FH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591625
    new-instance v0, LX/3Wy;

    invoke-direct {v0}, LX/3Wy;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/8qs;LX/1zf;LX/20w;LX/0tH;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/154;LX/0Or;LX/1za;LX/20l;LX/0iA;LX/20m;LX/0Ot;LX/9FH;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;",
            "LX/8qs;",
            "LX/1zf;",
            "LX/20w;",
            "LX/0tH;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/154;",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;",
            "LX/1za;",
            "LX/20l;",
            "LX/0iA;",
            "LX/20m;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/9FH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591713
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591714
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591715
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->c:LX/8qs;

    .line 591716
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->d:LX/1zf;

    .line 591717
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->e:LX/20w;

    .line 591718
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->f:LX/0tH;

    .line 591719
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 591720
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->h:Lcom/facebook/content/SecureContextHelper;

    .line 591721
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->i:LX/154;

    .line 591722
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->j:LX/0Or;

    .line 591723
    iput-object p10, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->k:LX/1za;

    .line 591724
    iput-object p11, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->l:LX/20l;

    .line 591725
    iput-object p12, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->m:LX/0iA;

    .line 591726
    iput-object p13, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->n:LX/20m;

    .line 591727
    iput-object p14, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->o:LX/0Ot;

    .line 591728
    iput-object p15, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->p:LX/9FH;

    .line 591729
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;
    .locals 3

    .prologue
    .line 591705
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    monitor-enter v1

    .line 591706
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591707
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591708
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591709
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->b(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591710
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591711
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591712
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 591664
    invoke-static {p1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 591665
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 591666
    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    .line 591667
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p1}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v8, v4

    move-object v7, v3

    .line 591668
    :goto_0
    iget-boolean v0, p2, LX/9FA;->d:Z

    move v0, v0

    .line 591669
    if-nez v0, :cond_3

    const/4 v5, 0x1

    .line 591670
    :goto_1
    if-eqz v5, :cond_4

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->p:LX/9FH;

    .line 591671
    iget-object v1, p2, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 591672
    invoke-virtual {v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/9FH;->a(J)LX/9FG;

    move-result-object v6

    .line 591673
    :goto_2
    new-instance v0, LX/9FQ;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v8}, LX/9FQ;-><init>(Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;ZLX/9FG;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 591674
    new-instance v1, LX/8qr;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/8qr;-><init>(Landroid/content/Context;)V

    .line 591675
    iput-object v0, v1, LX/8qr;->b:LX/8qq;

    .line 591676
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->i:LX/154;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    invoke-virtual {v0, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 591677
    iput-object v0, v1, LX/8qr;->d:Ljava/lang/String;

    .line 591678
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->f:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 591679
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20D;

    .line 591680
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->k:LX/1za;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/20D;->a(Ljava/util/List;)V

    .line 591681
    iput-object v0, v1, LX/8qr;->c:Landroid/graphics/drawable/Drawable;

    .line 591682
    :goto_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->d:LX/1zf;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    .line 591683
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->c:LX/8qs;

    .line 591684
    const/4 v4, 0x1

    invoke-static {v2, v1, v3, v4}, LX/8qs;->a(LX/8qs;LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Z)LX/8r1;

    move-result-object v4

    .line 591685
    iput-object v0, v4, LX/8r1;->g:LX/1zt;

    .line 591686
    invoke-virtual {v4, v3, v7}, LX/8r1;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v2

    move-object v4, v2

    .line 591687
    move-object v0, v4

    .line 591688
    return-object v0

    .line 591689
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 591690
    move-object v1, v0

    .line 591691
    :goto_4
    if-eqz v1, :cond_1

    .line 591692
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591693
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v0, :cond_1

    .line 591694
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 591695
    move-object v1, v0

    goto :goto_4

    .line 591696
    :cond_1
    if-nez v1, :cond_2

    move-object v8, v4

    move-object v7, v3

    .line 591697
    goto/16 :goto_0

    .line 591698
    :cond_2
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591699
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 591700
    invoke-static {v1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    move-object v7, v0

    goto/16 :goto_0

    .line 591701
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_4
    move-object v6, v9

    .line 591702
    goto/16 :goto_2

    .line 591703
    :cond_5
    iput-object v9, v1, LX/8qr;->c:Landroid/graphics/drawable/Drawable;

    .line 591704
    goto :goto_3
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;
    .locals 18

    .prologue
    .line 591662
    new-instance v2, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static/range {p0 .. p0}, LX/8qs;->a(LX/0QB;)LX/8qs;

    move-result-object v4

    check-cast v4, LX/8qs;

    invoke-static/range {p0 .. p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v5

    check-cast v5, LX/1zf;

    const-class v6, LX/20w;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/20w;

    invoke-static/range {p0 .. p0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v7

    check-cast v7, LX/0tH;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v10

    check-cast v10, LX/154;

    const/16 v11, 0x7af

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v12

    check-cast v12, LX/1za;

    invoke-static/range {p0 .. p0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v13

    check-cast v13, LX/20l;

    invoke-static/range {p0 .. p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v14

    check-cast v14, LX/0iA;

    invoke-static/range {p0 .. p0}, LX/20m;->a(LX/0QB;)LX/20m;

    move-result-object v15

    check-cast v15, LX/20m;

    const/16 v16, 0x3567

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const-class v17, LX/9FH;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/9FH;

    invoke-direct/range {v2 .. v17}, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/8qs;LX/1zf;LX/20w;LX/0tH;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/154;LX/0Or;LX/1za;LX/20l;LX/0iA;LX/20m;LX/0Ot;LX/9FH;)V

    .line 591663
    return-object v2
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 591661
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 591645
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/9FA;

    const/4 v2, 0x0

    .line 591646
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 591647
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v3, LX/9GN;

    invoke-direct {v3, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591648
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591649
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 591650
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->d:LX/1zf;

    .line 591651
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591652
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 591653
    :goto_1
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591654
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 591655
    sget-object v3, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "feedback_top_level_comments"

    .line 591656
    :goto_2
    iget-object v4, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->e:LX/20w;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {v4, v5, v2, v3}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v0

    .line 591657
    new-instance v2, LX/9FP;

    invoke-direct {v2, p0, p3, p2}, LX/9FP;-><init>(Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 591658
    new-instance v3, LX/9FR;

    invoke-direct {p0, p2, p3}, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v1, v2, v4, v0}, LX/9FR;-><init>(LX/0Px;LX/21M;Ljava/util/List;LX/20z;)V

    return-object v3

    .line 591659
    :cond_1
    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0

    .line 591660
    :cond_2
    const-string v3, "feedback_threaded_comments"

    goto :goto_2

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x27c46808

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591633
    check-cast p2, LX/9FR;

    check-cast p4, LX/9H0;

    .line 591634
    iget-object v1, p2, LX/9FR;->a:LX/0Px;

    .line 591635
    iput-object v1, p4, LX/9H0;->j:LX/0Px;

    .line 591636
    iget-object v1, p2, LX/9FR;->b:LX/21M;

    .line 591637
    iput-object v1, p4, LX/9H0;->l:LX/21M;

    .line 591638
    iget-object v1, p2, LX/9FR;->d:LX/20z;

    .line 591639
    iput-object v1, p4, LX/9H0;->k:LX/20z;

    .line 591640
    iget-object v1, p2, LX/9FR;->c:Ljava/util/List;

    invoke-virtual {p4}, LX/9H0;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, LX/8qs;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 591641
    iget-object v2, p4, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 591642
    iget-object v2, p4, LX/9H0;->g:Lcom/facebook/widget/accessibility/AccessibleTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/accessibility/AccessibleTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 591643
    sget-object v1, LX/20H;->DEFAULT:LX/20H;

    invoke-virtual {p4, v1}, LX/9H0;->setMode(LX/20H;)V

    .line 591644
    const/16 v1, 0x1f

    const v2, -0x22cc8f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 591629
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591630
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591631
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 591632
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->f:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 591626
    check-cast p4, LX/9H0;

    .line 591627
    invoke-virtual {p4}, LX/9H0;->a()V

    .line 591628
    return-void
.end method
