.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9GD;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9HI;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9HI;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592132
    new-instance v0, LX/3X7;

    invoke-direct {v0}, LX/3X7;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592133
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592134
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    .line 592135
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592136
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    .line 592137
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;
    .locals 6

    .prologue
    .line 592138
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    monitor-enter v1

    .line 592139
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592140
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592141
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592142
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592143
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;)V

    .line 592144
    move-object v0, p0

    .line 592145
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592146
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592147
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9HI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592149
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 592150
    check-cast p2, LX/9GD;

    const/4 v5, 0x0

    .line 592151
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9GD;->c:I

    iget-object v3, p2, LX/9GD;->d:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592152
    const v0, 0x7f0d0a00

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    new-instance v2, LX/9GJ;

    iget-object v3, p2, LX/9GD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9GD;->b:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/9GJ;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592153
    new-instance v0, LX/9Fk;

    iget-object v1, p2, LX/9GD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/9GD;->b:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-direct {v0, v1, v2, v5}, LX/9Fk;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)V

    .line 592154
    const v1, 0x7f0d09ff

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592155
    const v1, 0x7f0d0a06

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoPendingLightweightRecPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592156
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x56f2586f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592157
    check-cast p1, LX/9GD;

    check-cast p4, LX/9HI;

    .line 592158
    iget-object v1, p1, LX/9GD;->b:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->l()Ljava/lang/String;

    move-result-object v1

    .line 592159
    iget-object p1, p4, LX/9HI;->b:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592160
    const/16 v1, 0x1f

    const v2, 0x331de8a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592161
    const/4 v0, 0x1

    return v0
.end method
