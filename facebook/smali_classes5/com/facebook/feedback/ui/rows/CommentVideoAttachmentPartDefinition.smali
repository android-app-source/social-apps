.class public Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/9GY;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9HJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field public final c:LX/0ad;

.field public final d:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "LX/9HJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1nB;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/support/v4/app/FragmentActivity;

.field public final h:LX/121;

.field public final i:LX/0yI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591915
    new-instance v0, LX/3X2;

    invoke-direct {v0}, LX/3X2;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/0ad;LX/1AV;LX/1nB;LX/0Ot;Landroid/support/v4/app/FragmentActivity;LX/121;LX/0yI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;",
            "LX/0ad;",
            "LX/1AV;",
            "LX/1nB;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/121;",
            "LX/0yI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591852
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591853
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591854
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->c:LX/0ad;

    .line 591855
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->d:LX/1AV;

    .line 591856
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->e:LX/1nB;

    .line 591857
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->f:LX/0Ot;

    .line 591858
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->g:Landroid/support/v4/app/FragmentActivity;

    .line 591859
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->h:LX/121;

    .line 591860
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->i:LX/0yI;

    .line 591861
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;
    .locals 12

    .prologue
    .line 591904
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 591905
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591906
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591909
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v6

    check-cast v6, LX/1AV;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, LX/1nB;

    const/16 v8, 0x455

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    check-cast v9, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v10

    check-cast v10, LX/121;

    invoke-static {v0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v11

    check-cast v11, LX/0yI;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/0ad;LX/1AV;LX/1nB;LX/0Ot;Landroid/support/v4/app/FragmentActivity;LX/121;LX/0yI;)V

    .line 591910
    move-object v0, v3

    .line 591911
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591912
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591913
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9HJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591903
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 591916
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591917
    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 591918
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591919
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591920
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 591921
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591922
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    .line 591923
    new-instance v2, LX/9GY;

    .line 591924
    if-eqz v1, :cond_3

    .line 591925
    new-instance v3, LX/9GU;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v4}, LX/9GU;-><init>(Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;Ljava/lang/String;)V

    .line 591926
    :goto_1
    move-object v3, v3

    .line 591927
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v6

    invoke-static {v5, v6}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a(II)F

    move-result v5

    float-to-double v5, v5

    .line 591928
    new-instance v7, LX/2pZ;

    invoke-direct {v7}, LX/2pZ;-><init>()V

    invoke-static {v1}, LX/9D4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v8

    .line 591929
    iput-object v8, v7, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 591930
    move-object v7, v7

    .line 591931
    iput-wide v5, v7, LX/2pZ;->e:D

    .line 591932
    move-object v5, v7

    .line 591933
    const-string v6, "CoverImageParamsKey"

    invoke-static {v1}, LX/9D4;->b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1bf;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v5

    .line 591934
    if-eqz v0, :cond_0

    .line 591935
    const-string v6, "ShowGifPlayIconKey"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 591936
    :cond_0
    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    move-object v4, v5

    .line 591937
    if-eqz v0, :cond_2

    .line 591938
    if-nez v1, :cond_4

    .line 591939
    const/4 v0, 0x0

    .line 591940
    :goto_2
    move-object v0, v0

    .line 591941
    :goto_3
    invoke-direct {v2, v3, v4, v0}, LX/9GY;-><init>(LX/2oV;LX/2pa;Landroid/view/View$OnClickListener;)V

    return-object v2

    .line 591942
    :cond_1
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0

    .line 591943
    :cond_2
    if-nez v1, :cond_5

    .line 591944
    const/4 v0, 0x0

    .line 591945
    :goto_4
    move-object v0, v0

    .line 591946
    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    new-instance v0, LX/9GX;

    invoke-direct {v0, p0}, LX/9GX;-><init>(Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;)V

    goto :goto_2

    :cond_5
    new-instance v0, LX/9GW;

    invoke-direct {v0, p0, v1}, LX/9GW;-><init>(Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLMedia;)V

    goto :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3b676f47

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591872
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9GY;

    check-cast p4, LX/9HJ;

    .line 591873
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 591874
    iget-object v2, p2, LX/9GY;->b:LX/2pa;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 591875
    iput-object p1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591876
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 591877
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591878
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_2

    .line 591879
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->setVisibility(I)V

    .line 591880
    :goto_0
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 591881
    sget-object v2, LX/04D;->COMMENT:LX/04D;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 591882
    iget-object v1, p2, LX/9GY;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/9HJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591883
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->i:LX/0yI;

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->c:LX/0ad;

    invoke-static {v1, v2}, LX/9IN;->a(LX/0yI;LX/0ad;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 591884
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;->d:LX/1AV;

    iget-object v2, p2, LX/9GY;->a:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 591885
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x7c363464

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 591886
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 591887
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-static {v2, v1}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a(II)F

    move-result v1

    iput v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->f:F

    .line 591888
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->setVisibility(I)V

    .line 591889
    invoke-virtual {p4}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->requestLayout()V

    .line 591890
    const/4 v1, 0x0

    .line 591891
    iget-object v2, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 591892
    if-nez v2, :cond_7

    .line 591893
    :cond_3
    :goto_1
    move v1, v1

    .line 591894
    if-nez v1, :cond_6

    .line 591895
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->e:Landroid/view/View;

    if-nez v1, :cond_4

    .line 591896
    const v1, 0x7f0d05de

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->e:Landroid/view/View;

    .line 591897
    :cond_4
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->e:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 591898
    :cond_5
    :goto_2
    goto :goto_0

    .line 591899
    :cond_6
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->e:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 591900
    iget-object v1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->e:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 591901
    :cond_7
    iget-object p1, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a:LX/3iM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v2

    .line 591902
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, p1, :cond_3

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, p1, :cond_3

    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591867
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591868
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591869
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 591870
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591871
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 591862
    check-cast p4, LX/9HJ;

    .line 591863
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/9HJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591864
    iget-object v0, p4, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v0, v0

    .line 591865
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 591866
    return-void
.end method
