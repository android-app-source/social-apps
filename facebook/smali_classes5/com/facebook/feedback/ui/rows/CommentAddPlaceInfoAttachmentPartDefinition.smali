.class public Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9FT;",
        "LX/9FU;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H1;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/9jH;

.field private final e:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592297
    new-instance v0, LX/3XB;

    invoke-direct {v0}, LX/3XB;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9jH;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592298
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592299
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 592300
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592301
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->d:LX/9jH;

    .line 592302
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    .line 592303
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 592304
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    monitor-enter v1

    .line 592305
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592306
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592307
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592308
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592309
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/9jH;->b(LX/0QB;)LX/9jH;

    move-result-object v5

    check-cast v5, LX/9jH;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9jH;Landroid/content/Context;)V

    .line 592310
    move-object v0, p0

    .line 592311
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592312
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592313
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592314
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592315
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 592316
    check-cast p2, LX/9FT;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 592317
    iget v0, p2, LX/9FT;->c:I

    if-nez v0, :cond_1

    move v0, v1

    .line 592318
    :goto_0
    if-eqz v0, :cond_0

    .line 592319
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v3, LX/9GN;

    iget-object v4, p2, LX/9FT;->d:LX/9HA;

    invoke-direct {v3, v4}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592320
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592321
    new-instance v3, LX/9FS;

    invoke-direct {v3, p0, p2}, LX/9FS;-><init>(Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;LX/9FT;)V

    move-object v3, v3

    .line 592322
    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592323
    iget-object v0, p2, LX/9FT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 592324
    if-eqz v0, :cond_2

    .line 592325
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 592326
    if-eqz v3, :cond_2

    .line 592327
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 592328
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 592329
    :goto_1
    iget v1, p2, LX/9FT;->c:I

    if-nez v1, :cond_5

    .line 592330
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f08124d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 592331
    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f08124e

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 592332
    :goto_3
    new-instance v2, LX/9FU;

    invoke-direct {v2, v1, v0}, LX/9FU;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_1
    move v0, v2

    .line 592333
    goto :goto_0

    :cond_2
    move v0, v2

    .line 592334
    goto :goto_1

    .line 592335
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f08124b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 592336
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f08124c

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 592337
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f081251

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 592338
    :goto_4
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f081252

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 592339
    :cond_6
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f08124f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 592340
    :cond_7
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->e:Landroid/content/Context;

    const v2, 0x7f081250

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xd167f20

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592341
    check-cast p2, LX/9FU;

    check-cast p4, LX/9H1;

    .line 592342
    iget-object v1, p2, LX/9FU;->a:Ljava/lang/String;

    .line 592343
    iget-object v2, p4, LX/9H1;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592344
    iget-object v1, p2, LX/9FU;->b:Ljava/lang/String;

    .line 592345
    iget-object v2, p4, LX/9H1;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592346
    const/16 v1, 0x1f

    const v2, -0xa32d8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592347
    const/4 v0, 0x1

    return v0
.end method
