.class public Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Fz;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H4;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H4;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/8xq;

.field public final d:LX/8y9;

.field private e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private f:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

.field public final g:LX/20j;

.field public final h:LX/1K9;

.field public final i:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592007
    new-instance v0, LX/3X5;

    invoke-direct {v0}, LX/3X5;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xq;LX/8y9;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;LX/20j;LX/1K9;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592008
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592009
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592010
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->c:LX/8xq;

    .line 592011
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->d:LX/8y9;

    .line 592012
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592013
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    .line 592014
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->g:LX/20j;

    .line 592015
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->h:LX/1K9;

    .line 592016
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->i:LX/0kL;

    .line 592017
    return-void
.end method

.method public static synthetic a(Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)LX/6PS;
    .locals 9

    .prologue
    .line 592018
    if-nez p1, :cond_0

    .line 592019
    const/4 v1, 0x0

    .line 592020
    :goto_0
    move-object v0, v1

    .line 592021
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->g:LX/20j;

    const/4 v3, 0x0

    .line 592022
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v5

    .line 592023
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v3

    .line 592024
    :goto_1
    move-object v1, v2

    .line 592025
    goto :goto_0

    .line 592026
    :cond_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v6, :cond_4

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLComment;

    .line 592027
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 592028
    :goto_3
    if-nez v2, :cond_3

    move-object v2, v3

    .line 592029
    goto :goto_1

    .line 592030
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 592031
    :cond_3
    invoke-static {v2, p2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    .line 592032
    invoke-static {v5, v2, v3}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v2

    .line 592033
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v4

    iget-object v5, v1, LX/20j;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    .line 592034
    iput-wide v6, v4, LX/3dM;->v:J

    .line 592035
    move-object v4, v4

    .line 592036
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v6

    invoke-static {v4, v2, v5, v6}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 592037
    new-instance v2, LX/6PS;

    invoke-direct {v2, v4, v3}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_1

    :cond_4
    move-object v2, v3

    goto :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;
    .locals 12

    .prologue
    .line 592038
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    monitor-enter v1

    .line 592039
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592040
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592041
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592042
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592043
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592044
    new-instance v7, LX/8xq;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct {v7, v5, v6}, LX/8xq;-><init>(LX/0tX;LX/1Ck;)V

    .line 592045
    move-object v5, v7

    .line 592046
    check-cast v5, LX/8xq;

    .line 592047
    new-instance v8, LX/8y9;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-direct {v8, v6, v7}, LX/8y9;-><init>(LX/0tX;LX/1Ck;)V

    .line 592048
    move-object v6, v8

    .line 592049
    check-cast v6, LX/8y9;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v9

    check-cast v9, LX/20j;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v10

    check-cast v10, LX/1K9;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8xq;LX/8y9;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;LX/20j;LX/1K9;LX/0kL;)V

    .line 592050
    move-object v0, v3

    .line 592051
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592052
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592053
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592054
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592055
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 592056
    check-cast p2, LX/9Fz;

    const/4 v5, 0x0

    .line 592057
    iget-object v0, p2, LX/9Fz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 592058
    :goto_0
    return-object v5

    .line 592059
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9Fz;->c:I

    iget-object v3, p2, LX/9Fz;->d:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592060
    const v0, 0x7f0d09df

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->f:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    new-instance v2, LX/9Fu;

    iget-object v3, p2, LX/9Fz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9Fz;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {v2, v3, v4}, LX/9Fu;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592061
    iget-object v0, p2, LX/9Fz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    .line 592062
    iget-object v1, p2, LX/9Fz;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 592063
    const v2, 0x7f0d09eb

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592064
    new-instance v4, LX/9Fw;

    invoke-direct {v4, p0, p2, v0, v1}, LX/9Fw;-><init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;LX/9Fz;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v4

    .line 592065
    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592066
    const v2, 0x7f0d09ea

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592067
    new-instance v4, LX/9Fy;

    invoke-direct {v4, p0, p2, v0, v1}, LX/9Fy;-><init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardPendingAttachmentPartDefinition;LX/9Fz;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 592068
    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592069
    const/4 v0, 0x1

    return v0
.end method
