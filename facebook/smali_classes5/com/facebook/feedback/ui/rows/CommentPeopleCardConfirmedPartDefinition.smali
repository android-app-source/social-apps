.class public Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Fn;",
        "LX/9Fo;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H3;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H3;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/8y4;

.field public final g:LX/20j;

.field public final h:LX/1K9;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592072
    new-instance v0, LX/3X6;

    invoke-direct {v0}, LX/3X6;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8y4;LX/20j;LX/1K9;LX/0Or;LX/0kL;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;",
            "Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;",
            "Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/8y4;",
            "LX/20j;",
            "LX/1K9;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592073
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592074
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592075
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    .line 592076
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    .line 592077
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592078
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->f:LX/8y4;

    .line 592079
    iput-object p6, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->g:LX/20j;

    .line 592080
    iput-object p7, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->h:LX/1K9;

    .line 592081
    iput-object p8, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->i:LX/0Or;

    .line 592082
    iput-object p9, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->j:LX/0kL;

    .line 592083
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;
    .locals 13

    .prologue
    .line 592084
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    monitor-enter v1

    .line 592085
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592086
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592089
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592090
    new-instance v10, LX/8y4;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-direct {v10, v8, v9}, LX/8y4;-><init>(LX/0tX;LX/1Ck;)V

    .line 592091
    move-object v8, v10

    .line 592092
    check-cast v8, LX/8y4;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v9

    check-cast v9, LX/20j;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v10

    check-cast v10, LX/1K9;

    const/16 v11, 0x12cb

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/8y4;LX/20j;LX/1K9;LX/0Or;LX/0kL;)V

    .line 592093
    move-object v0, v3

    .line 592094
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592095
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592096
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592097
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592098
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 592099
    check-cast p2, LX/9Fn;

    .line 592100
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9Fn;->c:I

    iget-object v3, p2, LX/9Fn;->d:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592101
    const v0, 0x7f0d09d5

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPeopleCardPartDefinition;

    new-instance v2, LX/9Fu;

    iget-object v3, p2, LX/9Fn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9Fn;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {v2, v3, v4}, LX/9Fu;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592102
    const v0, 0x7f0d09d6

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPeopleCardMessageButtonPartDefinition;

    new-instance v2, LX/9Fr;

    iget-object v3, p2, LX/9Fn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9Fn;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {v2, v3, v4}, LX/9Fr;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592103
    iget-object v0, p2, LX/9Fn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592104
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 592105
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 592106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v0

    .line 592107
    if-eqz v0, :cond_0

    .line 592108
    const v1, 0x7f0d09d7

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592109
    iget-object v3, p2, LX/9Fn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    .line 592110
    new-instance v4, LX/9Fm;

    invoke-direct {v4, p0, p2, v3}, LX/9Fm;-><init>(Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;LX/9Fn;Ljava/lang/String;)V

    move-object v3, v4

    .line 592111
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592112
    :cond_0
    new-instance v1, LX/9Fo;

    iget-object v2, p2, LX/9Fn;->b:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v4, 0x0

    .line 592113
    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentPeopleCardConfirmedPartDefinition;->i:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 592114
    if-eqz v3, :cond_1

    .line 592115
    iget-object p1, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p1, p1

    .line 592116
    if-eqz p1, :cond_1

    if-nez v2, :cond_2

    :cond_1
    move v3, v4

    .line 592117
    :goto_0
    move v2, v3

    .line 592118
    invoke-direct {v1, v0, v2}, LX/9Fo;-><init>(ZZ)V

    return-object v1

    .line 592119
    :cond_2
    iget-object p1, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p1

    .line 592120
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x51a6eb6e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592121
    check-cast p2, LX/9Fo;

    check-cast p4, LX/9H3;

    .line 592122
    iget-boolean v1, p2, LX/9Fo;->a:Z

    .line 592123
    iget-object p0, p4, LX/9H3;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 592124
    iget-boolean v1, p2, LX/9Fo;->b:Z

    .line 592125
    iget-object p0, p4, LX/9H3;->c:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 592126
    const/16 v1, 0x1f

    const v2, 0x62435c24

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592127
    :cond_0
    const/16 v2, 0x8

    goto :goto_0

    .line 592128
    :cond_1
    const/16 v2, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592129
    const/4 v0, 0x1

    return v0
.end method
