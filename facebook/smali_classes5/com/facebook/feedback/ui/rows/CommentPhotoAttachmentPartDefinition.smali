.class public Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H5;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field public final c:LX/0sX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591849
    new-instance v0, LX/3X1;

    invoke-direct {v0}, LX/3X1;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/0sX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591809
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591810
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591811
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->c:LX/0sX;

    .line 591812
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 591838
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 591839
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591840
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591841
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591842
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591843
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v4

    check-cast v4, LX/0sX;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/0sX;)V

    .line 591844
    move-object v0, p0

    .line 591845
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591846
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591847
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591837
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 591832
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591833
    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 591834
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591835
    const/4 v0, 0x0

    return-object v0

    .line 591836
    :cond_0
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x790f2c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591823
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/9H5;

    .line 591824
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 591825
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591826
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;->c:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v1, v2

    .line 591827
    iget-object v2, p4, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v2, v2

    .line 591828
    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 591829
    invoke-virtual {p4, p1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 591830
    const/16 v1, 0x1f

    const v2, -0x657aec0b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 591831
    :cond_0
    invoke-virtual {p4}, LX/9H5;->getContext()Landroid/content/Context;

    move-result-object v1

    const p2, 0x7f080072

    invoke-virtual {v1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591818
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591819
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591820
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591821
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 591822
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 591813
    check-cast p4, LX/9H5;

    const/4 v1, 0x0

    .line 591814
    invoke-virtual {p4, v1}, LX/9H5;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591815
    iget-object v0, p4, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 591816
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591817
    return-void
.end method
