.class public Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/9FY;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

.field public final c:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/17W;

.field private final e:LX/0ad;

.field private final f:LX/17R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592350
    new-instance v0, LX/3XC;

    invoke-direct {v0}, LX/3XC;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/1AV;LX/17W;LX/0ad;LX/17R;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592351
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592352
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 592353
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->c:LX/1AV;

    .line 592354
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->d:LX/17W;

    .line 592355
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->e:LX/0ad;

    .line 592356
    iput-object p5, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->f:LX/17R;

    .line 592357
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 592358
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;

    monitor-enter v1

    .line 592359
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592360
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592363
    new-instance v3, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v5

    check-cast v5, LX/1AV;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v8

    check-cast v8, LX/17R;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;LX/1AV;LX/17W;LX/0ad;LX/17R;)V

    .line 592364
    move-object v0, v3

    .line 592365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592369
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 592370
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592371
    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    invoke-static {p2}, LX/9Fi;->getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 592372
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592373
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592374
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 592375
    new-instance v1, LX/9FY;

    .line 592376
    if-eqz v0, :cond_1

    .line 592377
    new-instance v2, LX/9FX;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/9FX;-><init>(Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;Ljava/lang/String;)V

    .line 592378
    :goto_1
    move-object v2, v2

    .line 592379
    new-instance v5, LX/2pZ;

    invoke-direct {v5}, LX/2pZ;-><init>()V

    invoke-static {v0}, LX/9D4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v6

    .line 592380
    iput-object v6, v5, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 592381
    move-object v5, v5

    .line 592382
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v7

    invoke-static {v6, v7}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a(II)F

    move-result v6

    float-to-double v7, v6

    .line 592383
    iput-wide v7, v5, LX/2pZ;->e:D

    .line 592384
    move-object v5, v5

    .line 592385
    const-string v6, "CoverImageParamsKey"

    invoke-static {v0}, LX/9D4;->b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1bf;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v5

    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    move-object v3, v5

    .line 592386
    if-nez v0, :cond_2

    .line 592387
    const/4 v4, 0x0

    .line 592388
    :goto_2
    move-object v4, v4

    .line 592389
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592390
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 592391
    if-nez v0, :cond_3

    .line 592392
    const/4 v5, 0x0

    .line 592393
    :goto_3
    move-object v0, v5

    .line 592394
    invoke-direct {v1, v2, v3, v4, v0}, LX/9FY;-><init>(LX/2oV;LX/2pa;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v1

    .line 592395
    :cond_0
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    new-instance v4, LX/9FV;

    invoke-direct {v4, p0}, LX/9FV;-><init>(Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;)V

    goto :goto_2

    :cond_3
    new-instance v5, LX/9FW;

    invoke-direct {v5, p0, v0}, LX/9FW;-><init>(Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x41b0eeab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592396
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/9FY;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    .line 592397
    iget-object v1, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 592398
    iget-object v2, p2, LX/9FY;->a:LX/2pa;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 592399
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 592400
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 592401
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_2

    .line 592402
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setVisibility(I)V

    .line 592403
    :goto_0
    iget-object v1, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 592404
    sget-object v2, LX/04D;->COMMENT:LX/04D;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 592405
    iget-object v1, p2, LX/9FY;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 592406
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 592407
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 592408
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592409
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 592410
    :goto_1
    iget-object p1, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->f:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 592411
    iget-object v1, p2, LX/9FY;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setBottomTextClickListener(Landroid/view/View$OnClickListener;)V

    .line 592412
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;->c:LX/1AV;

    iget-object v2, p2, LX/9FY;->b:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 592413
    const/16 v1, 0x1f

    const v2, 0x69718279

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592414
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 592415
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 592416
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-static {v2, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a(II)F

    move-result v1

    iput v1, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->g:F

    .line 592417
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setVisibility(I)V

    .line 592418
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->requestLayout()V

    goto :goto_0

    .line 592419
    :cond_3
    const/16 v2, 0x8

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592420
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592421
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592422
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 592423
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592424
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 592425
    check-cast p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;

    const/4 v1, 0x0

    .line 592426
    iget-object v0, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v0, v0

    .line 592427
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 592428
    iget-object v0, p4, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->e:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v0, v0

    .line 592429
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 592430
    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->setBottomTextClickListener(Landroid/view/View$OnClickListener;)V

    .line 592431
    return-void
.end method
