.class public Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;
.super LX/3Wq;
.source ""


# static fields
.field public static final p:LX/1Cz;

.field public static final q:Landroid/view/View$OnClickListener;


# instance fields
.field public r:LX/8pY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final s:Lcom/facebook/resources/ui/FbTextView;

.field private t:Ljava/lang/CharSequence;

.field private u:Ljava/lang/CharSequence;

.field private v:Z

.field public w:LX/9GO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591465
    new-instance v0, LX/3Wu;

    invoke-direct {v0}, LX/3Wu;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->p:LX/1Cz;

    .line 591466
    new-instance v0, LX/3Wv;

    invoke-direct {v0}, LX/3Wv;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->q:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 591467
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 591468
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 591469
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 591470
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 591471
    invoke-direct {p0, p1, p2, p3}, LX/3Wq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 591472
    const-class v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 591473
    const v0, 0x7f0302e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 591474
    invoke-virtual {p0}, LX/3Wq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 591475
    iget-object v0, p0, LX/3Wq;->q:LX/9CG;

    invoke-virtual {v0, p1}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Wq;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 591476
    const v0, 0x7f0d09ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 591477
    iget-object v0, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 591478
    const v0, 0x7f0d09cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/3Wq;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 591479
    iget-object v0, p0, LX/3Wq;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 591480
    const v0, 0x7f0d09cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/translation/ui/TranslatableTextView;

    iput-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    .line 591481
    const v0, 0x7f0d09cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/3Wq;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 591482
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    .line 591483
    iget-object v1, v0, Lcom/facebook/translation/ui/TranslatableTextView;->d:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 591484
    sget-object v1, LX/1vY;->COMMENT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 591485
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    new-instance v1, LX/9HF;

    invoke-direct {v1, p0}, LX/9HF;-><init>(Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/translation/ui/TranslatableTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591486
    const v0, 0x7f0d09f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 591487
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/9HG;

    invoke-direct {v1, p0}, LX/9HG;-><init>(Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591488
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591489
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->t:Ljava/lang/CharSequence;

    .line 591490
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->u:Ljava/lang/CharSequence;

    .line 591491
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_1

    .line 591492
    :cond_0
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/translation/ui/TranslatableTextView;->setVisibility(I)V

    .line 591493
    :goto_0
    return-void

    .line 591494
    :cond_1
    if-eqz p2, :cond_2

    .line 591495
    :goto_1
    invoke-virtual {p0, p2}, LX/3Wq;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 591496
    iget-object v1, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/translation/ui/TranslatableTextView;->setVisibility(I)V

    .line 591497
    iget-object v1, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/translation/ui/TranslatableTextView;->setUntranslatedText(Ljava/lang/CharSequence;)V

    .line 591498
    iget-object v1, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/translation/ui/TranslatableTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 591499
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v0}, Lcom/facebook/translation/ui/TranslatableTextView;->b()V

    goto :goto_0

    :cond_2
    move-object p2, p1

    .line 591500
    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    invoke-static {v0}, LX/8pY;->b(LX/0QB;)LX/8pY;

    move-result-object v0

    check-cast v0, LX/8pY;

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->r:LX/8pY;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 591454
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->n:Ljava/lang/String;

    .line 591455
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->o:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    .line 591456
    iget-object v0, p0, LX/3Wq;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->v:Z

    .line 591457
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->v:Z

    if-nez v0, :cond_2

    .line 591458
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 591459
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 591460
    goto :goto_0

    .line 591461
    :cond_2
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, LX/3Wq;->o:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v0, v3, :cond_3

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f081111

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 591462
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 591463
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f081035

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static getContinueReadingListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 591464
    sget-object v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->q:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static j(Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;)V
    .locals 2

    .prologue
    .line 591451
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->v:Z

    if-eqz v0, :cond_0

    .line 591452
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->r:LX/8pY;

    iget-object v1, p0, LX/3Wq;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/8pY;->a(Ljava/lang/String;LX/3Wt;)V

    .line 591453
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591448
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 591449
    invoke-direct {p0, p3, p4}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V

    .line 591450
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 591432
    invoke-static {p1}, LX/47i;->a(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591433
    invoke-virtual {p0}, LX/3Wq;->hv_()V

    .line 591434
    :cond_0
    :goto_0
    return-void

    .line 591435
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 591436
    const/4 v1, 0x0

    .line 591437
    if-nez v0, :cond_3

    .line 591438
    :goto_1
    move-object v0, v1

    .line 591439
    if-eqz v0, :cond_0

    .line 591440
    iget-object v1, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    iget-object v2, p0, LX/3Wq;->o:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/translation/ui/TranslatableTextView;->a(Ljava/lang/CharSequence;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Z

    move-result v0

    .line 591441
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 591442
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    if-eqz v0, :cond_0

    .line 591443
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    invoke-interface {v0}, LX/9GO;->a()V

    goto :goto_0

    .line 591444
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 591445
    :cond_3
    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 591446
    iget-object v3, p0, LX/3Wq;->j:LX/1Uf;

    invoke-static {v2}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v2

    const/4 p1, 0x1

    invoke-virtual {v3, v2, p1, v1}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v1}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 591447
    invoke-virtual {p0, v1}, LX/3Wq;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 591428
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v0}, Lcom/facebook/translation/ui/TranslatableTextView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 591429
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->i()Z

    .line 591430
    invoke-static {p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->j(Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;)V

    .line 591431
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 591427
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->u:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 591415
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591416
    const/4 v0, 0x0

    .line 591417
    :goto_0
    return v0

    .line 591418
    :cond_0
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->t:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/translation/ui/TranslatableTextView;->setUntranslatedText(Ljava/lang/CharSequence;)V

    .line 591419
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->u:Ljava/lang/CharSequence;

    .line 591420
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    if-eqz v0, :cond_1

    .line 591421
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    invoke-interface {v0}, LX/9GO;->a()V

    .line 591422
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setBody(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 591425
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)V

    .line 591426
    return-void
.end method

.method public setListener(LX/9GO;)V
    .locals 0
    .param p1    # LX/9GO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591423
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->w:LX/9GO;

    .line 591424
    return-void
.end method
