.class public Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9HP;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:Z

.field private final c:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591775
    new-instance v0, LX/3X0;

    invoke-direct {v0}, LX/3X0;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591776
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591777
    sget-short v0, LX/0wn;->aJ:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->b:Z

    .line 591778
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    .line 591779
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;
    .locals 5

    .prologue
    .line 591780
    const-class v1, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;

    monitor-enter v1

    .line 591781
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591782
    sput-object v2, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591783
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591784
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591785
    new-instance p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;-><init>(LX/0ad;Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;)V

    .line 591786
    move-object v0, p0

    .line 591787
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591788
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591789
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 591791
    sget-object v0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 591792
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/9HA;->NO_OFFSET:LX/9HA;

    .line 591793
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;

    new-instance v2, LX/9GN;

    invoke-direct {v2, v0}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591794
    const/4 v0, 0x0

    return-object v0

    .line 591795
    :cond_0
    sget-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x514dcba2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591796
    check-cast p3, LX/9FA;

    check-cast p4, LX/9HP;

    .line 591797
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 591798
    iget v2, v1, LX/9FD;->a:I

    move v1, v2

    .line 591799
    invoke-virtual {p4}, LX/9HP;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p4}, LX/9HP;->getPaddingRight()I

    move-result p1

    invoke-virtual {p4, v2, v1, p1, v1}, LX/9HP;->setPadding(IIII)V

    .line 591800
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/rows/TypingIndicatorPartDefinition;->b:Z

    .line 591801
    iget-object p1, p4, LX/9HP;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 591802
    const/16 v1, 0x1f

    const v2, -0x569ea39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 591803
    :cond_0
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591804
    check-cast p1, Ljava/lang/Integer;

    .line 591805
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 591806
    return-void
.end method
