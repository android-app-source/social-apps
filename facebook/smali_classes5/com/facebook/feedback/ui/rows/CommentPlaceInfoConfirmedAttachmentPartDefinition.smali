.class public Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9G8;",
        "LX/9G9;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9H7;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9H7;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/8xo;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

.field private final e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592216
    new-instance v0, LX/3X8;

    invoke-direct {v0}, LX/3X8;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;LX/8xo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592164
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592165
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592166
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    .line 592167
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    .line 592168
    iput-object p4, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->b:LX/8xo;

    .line 592169
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 592205
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    monitor-enter v1

    .line 592206
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592207
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592208
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592209
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592210
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    invoke-static {v0}, LX/8xo;->b(LX/0QB;)LX/8xo;

    move-result-object v6

    check-cast v6, LX/8xo;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;LX/8xo;)V

    .line 592211
    move-object v0, p0

    .line 592212
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592213
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592214
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592215
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9H7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592204
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 592188
    check-cast p2, LX/9G8;

    .line 592189
    iget-object v0, p2, LX/9G8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592190
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 592191
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 592192
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v2, LX/9GK;

    iget v3, p2, LX/9G8;->c:I

    iget-object v4, p2, LX/9G8;->d:LX/9HA;

    invoke-direct {v2, v3, v4}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592193
    const v1, 0x7f0d09e4

    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoCardPartDefinition;

    iget-object v3, p2, LX/9G8;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592194
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592195
    const v0, 0x7f0d09e5

    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    new-instance v2, LX/9GG;

    iget-object v3, p2, LX/9G8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/9G8;->b:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, LX/9GG;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;Z)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592196
    :cond_0
    new-instance v1, LX/9G9;

    iget-object v0, p2, LX/9G8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592197
    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 592198
    if-eqz v2, :cond_2

    .line 592199
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 592200
    if-eqz v3, :cond_2

    .line 592201
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 592202
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 592203
    if-eqz v0, :cond_1

    const v0, 0x7f0823d2

    :goto_1
    invoke-direct {v1, v0}, LX/9G9;-><init>(I)V

    return-object v1

    :cond_1
    const v0, 0x7f0823d3

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7acf284d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592171
    check-cast p1, LX/9G8;

    check-cast p2, LX/9G9;

    check-cast p4, LX/9H7;

    .line 592172
    iget-object v1, p1, LX/9G8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592173
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 592174
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 592175
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v1

    .line 592176
    iget-object p1, p4, LX/9H7;->b:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 592177
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoConfirmedAttachmentPartDefinition;->b:LX/8xo;

    iget v2, p2, LX/9G9;->a:I

    .line 592178
    iget-object p0, v1, LX/8xo;->d:LX/0iA;

    sget-object p1, LX/8xo;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p2, LX/8xk;

    invoke-virtual {p0, p1, p2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object p0

    .line 592179
    if-eqz p0, :cond_0

    .line 592180
    check-cast p0, LX/8xk;

    .line 592181
    iget-boolean p1, p0, LX/8xk;->c:Z

    move p1, p1

    .line 592182
    if-nez p1, :cond_0

    .line 592183
    invoke-static {v2, p4}, LX/8xo;->a(ILandroid/view/View;)V

    .line 592184
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/8xk;->c:Z

    .line 592185
    iget-object p1, p0, LX/8xk;->b:LX/0iA;

    invoke-virtual {p1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object p1

    const-string p2, "4482"

    invoke-virtual {p1, p2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 592186
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x3b384c6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592187
    :cond_1
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592170
    const/4 v0, 0x1

    return v0
.end method
