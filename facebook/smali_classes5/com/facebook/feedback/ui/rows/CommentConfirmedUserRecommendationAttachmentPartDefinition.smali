.class public Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Fb;",
        "LX/9Fc;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9HH;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/9HH;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

.field private final c:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

.field private final d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592247
    new-instance v0, LX/3X9;

    invoke-direct {v0}, LX/3X9;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592248
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592249
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    .line 592250
    iput-object p2, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    .line 592251
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    .line 592252
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 592253
    const-class v1, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    monitor-enter v1

    .line 592254
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592255
    sput-object v2, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592256
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592257
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592258
    new-instance p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;-><init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;)V

    .line 592259
    move-object v0, p0

    .line 592260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/9HH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592246
    sget-object v0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 592219
    check-cast p2, LX/9Fb;

    .line 592220
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->b:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoStylingPartDefinition;

    new-instance v1, LX/9GK;

    iget v2, p2, LX/9Fb;->c:I

    iget-object v3, p2, LX/9Fb;->d:LX/9HA;

    invoke-direct {v1, v2, v3}, LX/9GK;-><init>(ILX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592221
    iget-object v0, p2, LX/9Fb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592222
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 592223
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 592224
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v1

    .line 592225
    if-eqz v1, :cond_0

    .line 592226
    const v2, 0x7f0d09fe

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->d:Lcom/facebook/feedback/ui/rows/CommentUserRecommendationRemoveAttachmentClickListenerPartDefinition;

    new-instance v4, LX/9GT;

    iget-object v5, p2, LX/9Fb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p2, LX/9Fb;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-direct {v4, v5, v6}, LX/9GT;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592227
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v0

    .line 592228
    if-eqz v0, :cond_1

    .line 592229
    const v2, 0x7f0d09fd

    iget-object v3, p0, Lcom/facebook/feedback/ui/rows/CommentConfirmedUserRecommendationAttachmentPartDefinition;->c:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    new-instance v4, LX/9Fk;

    iget-object v5, p2, LX/9Fb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    iget-object v7, p2, LX/9Fb;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-direct {v4, v5, v6, v7}, LX/9Fk;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592230
    :cond_1
    new-instance v2, LX/9Fc;

    invoke-direct {v2, v1, v0}, LX/9Fc;-><init>(ZZ)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x53f9be5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592232
    check-cast p1, LX/9Fb;

    check-cast p2, LX/9Fc;

    check-cast p4, LX/9HH;

    .line 592233
    iget-object v1, p1, LX/9Fb;->b:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 592234
    if-nez v1, :cond_0

    .line 592235
    :goto_0
    iget-boolean v1, p2, LX/9Fc;->a:Z

    .line 592236
    iget-object p0, p4, LX/9HH;->i:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 592237
    iget-boolean v1, p2, LX/9Fc;->b:Z

    .line 592238
    iget-object p0, p4, LX/9HH;->j:Landroid/view/View;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 592239
    const/16 v1, 0x1f

    const v2, -0x5d4482d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592240
    :cond_0
    iget-object v2, p4, LX/9HH;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->m()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592241
    iget-object v2, p4, LX/9HH;->c:Landroid/view/View;

    iget-object p0, p4, LX/9HH;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->n()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p0, p1}, LX/9HH;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 592242
    iget-object v2, p4, LX/9HH;->e:Landroid/view/View;

    iget-object p0, p4, LX/9HH;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p0, p1}, LX/9HH;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 592243
    iget-object v2, p4, LX/9HH;->g:Landroid/view/View;

    iget-object p0, p4, LX/9HH;->h:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->o()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p0, p1}, LX/9HH;->a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    .line 592244
    :cond_1
    const/16 v2, 0x8

    goto :goto_1

    .line 592245
    :cond_2
    const/16 v2, 0x8

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592231
    const/4 v0, 0x1

    return v0
.end method
