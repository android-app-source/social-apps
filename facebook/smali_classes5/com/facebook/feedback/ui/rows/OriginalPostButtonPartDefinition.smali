.class public Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Gp;",
        "LX/9Gq;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "LX/9EH;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/9EG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591732
    new-instance v0, LX/3Wz;

    invoke-direct {v0}, LX/3Wz;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/9EG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591733
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591734
    iput-object p1, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->b:LX/9EG;

    .line 591735
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;
    .locals 4

    .prologue
    .line 591736
    const-class v1, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    monitor-enter v1

    .line 591737
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591738
    sput-object v2, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591739
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591740
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591741
    new-instance p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;

    invoke-static {v0}, LX/9EG;->a(LX/0QB;)LX/9EG;

    move-result-object v3

    check-cast v3, LX/9EG;

    invoke-direct {p0, v3}, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;-><init>(LX/9EG;)V

    .line 591742
    move-object v0, p0

    .line 591743
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591744
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591745
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591746
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 591747
    sget-object v0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 591748
    check-cast p2, LX/9Gp;

    check-cast p3, LX/9FA;

    .line 591749
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->b:LX/9EG;

    iget-object v1, p2, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/9EG;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 591750
    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 591751
    const/4 v0, 0x0

    .line 591752
    if-eqz v2, :cond_0

    .line 591753
    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    .line 591754
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 591755
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 591756
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->b:LX/9EG;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p2, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3, v4, v1}, LX/9EG;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 591757
    iget-object v3, p2, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591758
    new-instance v4, LX/9Go;

    invoke-direct {v4, p0, p3, v3, v1}, LX/9Go;-><init>(Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 591759
    new-instance v1, LX/9Gq;

    invoke-direct {v1, v0, v2, v4}, LX/9Gq;-><init>(Landroid/net/Uri;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2734a89c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591760
    check-cast p2, LX/9Gq;

    check-cast p4, LX/9EH;

    .line 591761
    iget-object v1, p2, LX/9Gq;->a:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 591762
    iget-object v1, p2, LX/9Gq;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/9EH;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591763
    const v1, 0x7f0d21fb

    invoke-virtual {p4, v1}, LX/9EH;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 591764
    if-eqz v1, :cond_0

    .line 591765
    iget-object v2, p2, LX/9Gq;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 591766
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x1386dd0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 591767
    check-cast p1, LX/9Gp;

    .line 591768
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/OriginalPostButtonPartDefinition;->b:LX/9EG;

    iget-object v1, p1, LX/9Gp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/9EG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 591769
    check-cast p4, LX/9EH;

    .line 591770
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/9EH;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591771
    const v0, 0x7f02111f

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 591772
    return-void
.end method
