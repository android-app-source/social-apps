.class public Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Gc;",
        "LX/9Gd;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/9FH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0tF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592562
    new-instance v0, LX/3XE;

    invoke-direct {v0}, LX/3XE;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 592554
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592555
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592556
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->b:LX/0Ot;

    .line 592557
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592558
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->c:LX/0Ot;

    .line 592559
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592560
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->d:LX/0Ot;

    .line 592561
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;
    .locals 8

    .prologue
    .line 592531
    const-class v1, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    monitor-enter v1

    .line 592532
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592533
    sput-object v2, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592534
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592535
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592536
    new-instance v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;

    invoke-direct {v3}, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;-><init>()V

    .line 592537
    const/16 v4, 0xe0f

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1dc0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1d9c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, LX/9FH;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/9FH;

    invoke-static {v0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object p0

    check-cast p0, LX/0tF;

    .line 592538
    iput-object v4, v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->b:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->c:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->d:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->e:LX/9FH;

    iput-object p0, v3, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->f:LX/0tF;

    .line 592539
    move-object v0, v3

    .line 592540
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592541
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592542
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592543
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592544
    sget-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 592545
    check-cast p2, LX/9Gc;

    check-cast p3, LX/9FA;

    .line 592546
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/9GN;

    sget-object v2, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-direct {v1, v2}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592547
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->e:LX/9FH;

    .line 592548
    iget-object v1, p3, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 592549
    iget-wide v4, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v4

    .line 592550
    invoke-virtual {v0, v2, v3}, LX/9FH;->a(J)LX/9FG;

    move-result-object v1

    .line 592551
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/9Gb;

    invoke-direct {v2, p0, v1, p3, p2}, LX/9Gb;-><init>(Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;LX/9FG;LX/9FA;LX/9Gc;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592552
    iget-object v0, p2, LX/9Gc;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 592553
    new-instance v2, LX/9Gd;

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Dp;

    invoke-virtual {v0, v1}, LX/9Dp;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v0}, LX/9Gd;-><init>(Landroid/net/Uri;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x36ae7ebe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592507
    check-cast p1, LX/9Gc;

    check-cast p2, LX/9Gd;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;

    .line 592508
    iget-object v1, p1, LX/9Gc;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 592509
    const/4 v2, 0x0

    .line 592510
    iget-object v4, p3, LX/9FA;->f:LX/9Ce;

    move-object v4, v4

    .line 592511
    if-eqz v4, :cond_1

    invoke-virtual {v4, p4, v1, v2}, LX/9Ce;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 592512
    :goto_0
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 592513
    if-eqz v1, :cond_0

    .line 592514
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->getPaddingLeft()I

    move-result v2

    .line 592515
    iget v4, v1, LX/9FD;->b:I

    move v4, v4

    .line 592516
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->getPaddingRight()I

    move-result v5

    .line 592517
    iget p0, v1, LX/9FD;->b:I

    move p0, p0

    .line 592518
    invoke-virtual {p4, v2, v4, v5, p0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->setPadding(IIII)V

    .line 592519
    iget v2, v1, LX/9FD;->g:I

    move v1, v2

    .line 592520
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 592521
    :cond_0
    iget-object v1, p2, LX/9Gd;->a:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592522
    const/16 v1, 0x1f

    const v2, -0x6d5a1ca1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 592523
    check-cast p1, LX/9Gc;

    .line 592524
    iget-object v0, p1, LX/9Gc;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 592525
    invoke-static {v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyCallToActionPartDefinition;->f:LX/0tF;

    const/4 v1, 0x0

    .line 592526
    invoke-virtual {v0}, LX/0tF;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/0tF;->a:LX/0ad;

    sget-short p1, LX/0wn;->ag:S

    invoke-interface {p0, p1, v1}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 592527
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 592528
    check-cast p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;

    .line 592529
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyCallToActionView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592530
    return-void
.end method
