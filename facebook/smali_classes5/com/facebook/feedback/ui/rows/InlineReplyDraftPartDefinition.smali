.class public Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Gg;",
        "LX/9Gh;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8pb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592565
    new-instance v0, LX/3XF;

    invoke-direct {v0}, LX/3XF;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 592566
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592567
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592568
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->b:LX/0Ot;

    .line 592569
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592570
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->c:LX/0Ot;

    .line 592571
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592572
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->d:LX/0Ot;

    .line 592573
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592574
    sget-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 592575
    check-cast p2, LX/9Gg;

    check-cast p3, LX/9FA;

    .line 592576
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/9GN;

    sget-object v2, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-direct {v1, v2}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592577
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/9Gf;

    invoke-direct {v1, p0, p3, p2}, LX/9Gf;-><init>(Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;LX/9FA;LX/9Gg;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592578
    iget-object v0, p2, LX/9Gg;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 592579
    new-instance v2, LX/9Gh;

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pb;

    iget-object v3, p2, LX/9Gg;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 592580
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object v3, v4

    .line 592581
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 592582
    const/4 v4, 0x0

    .line 592583
    :goto_1
    move-object v3, v4

    .line 592584
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Dp;

    invoke-virtual {v0, v1}, LX/9Dp;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyDraftPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Dp;

    invoke-virtual {v0, v1}, LX/9Dp;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v4, v0}, LX/9Gh;-><init>(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v2

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    iget-object v4, v0, LX/8pb;->b:LX/0aq;

    invoke-virtual {v4, v3}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3eef4437

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592585
    check-cast p2, LX/9Gh;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;

    .line 592586
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 592587
    if-eqz v1, :cond_0

    .line 592588
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->getPaddingLeft()I

    move-result v2

    .line 592589
    iget-object v4, p3, LX/9FA;->m:LX/9FD;

    move-object v4, v4

    .line 592590
    iget p0, v4, LX/9FD;->b:I

    move v4, p0

    .line 592591
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->getPaddingRight()I

    move-result p0

    .line 592592
    iget-object p1, p3, LX/9FA;->m:LX/9FD;

    move-object p1, p1

    .line 592593
    iget p3, p1, LX/9FD;->b:I

    move p1, p3

    .line 592594
    invoke-virtual {p4, v2, v4, p0, p1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->setPadding(IIII)V

    .line 592595
    iget v2, v1, LX/9FD;->g:I

    move v1, v2

    .line 592596
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 592597
    :cond_0
    iget-object v1, p2, LX/9Gh;->a:Ljava/lang/String;

    .line 592598
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 592599
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p0, 0x7f080ff3

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 592600
    :goto_0
    iget-object v1, p2, LX/9Gh;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 592601
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 592602
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->f()V

    .line 592603
    :goto_1
    iget-object v1, p2, LX/9Gh;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->setDisplayName(Ljava/lang/String;)V

    .line 592604
    iget-object v1, p2, LX/9Gh;->d:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592605
    const/16 v1, 0x1f

    const v2, 0x4dc1b5f6    # 4.0624096E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592606
    :cond_2
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 592607
    :cond_3
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v2, :cond_4

    .line 592608
    const v2, 0x7f0d1754

    invoke-virtual {p4, v2}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 592609
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 592610
    :cond_4
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9Gi;

    sget-object v4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->j:Lcom/facebook/common/callercontext/CallerContext;

    iget-object p0, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance p1, Ljava/io/File;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v2, v4, p0, p1}, LX/9Gi;->a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/net/Uri;)V

    .line 592611
    iget-object v2, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592612
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 592613
    check-cast p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;

    .line 592614
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592615
    const-string v0, ""

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->setDisplayName(Ljava/lang/String;)V

    .line 592616
    iget-object v0, p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->n:Lcom/facebook/resources/ui/FbTextView;

    const-string p0, ""

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 592617
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyDraftView;->f()V

    .line 592618
    return-void
.end method
