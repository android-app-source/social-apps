.class public Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/9Ge;",
        "LX/9E2;",
        "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
        "Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9E2;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentStylingPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592434
    new-instance v0, LX/3XD;

    invoke-direct {v0}, LX/3XD;-><init>()V

    sput-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 592499
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592500
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592501
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->c:LX/0Ot;

    .line 592502
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 592503
    iput-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->d:LX/0Ot;

    .line 592504
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592498
    sget-object v0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 592489
    check-cast p2, LX/9Ge;

    .line 592490
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/9GN;

    sget-object v2, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    invoke-direct {v1, v2}, LX/9GN;-><init>(LX/9HA;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592491
    iget-object v0, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9E2;

    .line 592492
    iget-object v1, p2, LX/9Ge;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 592493
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Dp;

    invoke-virtual {v1, v2}, LX/9Dp;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v1

    .line 592494
    iput-object v1, v0, LX/9E2;->h:Ljava/lang/String;

    .line 592495
    iget-object v1, p0, Lcom/facebook/feedback/ui/rows/InlineReplyComposerPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Dp;

    invoke-virtual {v1, v2}, LX/9Dp;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Landroid/net/Uri;

    move-result-object v1

    .line 592496
    iput-object v1, v0, LX/9E2;->i:Landroid/net/Uri;

    .line 592497
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x49a1e68e    # 1326289.8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592454
    check-cast p1, LX/9Ge;

    check-cast p2, LX/9E2;

    check-cast p3, LX/9FA;

    check-cast p4, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592455
    iget-object v1, p3, LX/9FA;->m:LX/9FD;

    move-object v1, v1

    .line 592456
    if-eqz v1, :cond_0

    .line 592457
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->getPaddingLeft()I

    move-result v2

    .line 592458
    iget-object v4, p3, LX/9FA;->m:LX/9FD;

    move-object v4, v4

    .line 592459
    iget v5, v4, LX/9FD;->b:I

    move v4, v5

    .line 592460
    invoke-virtual {p4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->getPaddingRight()I

    move-result v5

    .line 592461
    iget-object v6, p3, LX/9FA;->m:LX/9FD;

    move-object v6, v6

    .line 592462
    iget p0, v6, LX/9FD;->b:I

    move v6, p0

    .line 592463
    invoke-virtual {p4, v2, v4, v5, v6}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setPadding(IIII)V

    .line 592464
    iget v2, v1, LX/9FD;->g:I

    move v1, v2

    .line 592465
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 592466
    :cond_0
    iget-object v1, p1, LX/9Ge;->a:Lcom/facebook/graphql/model/GraphQLComment;

    const/4 v4, 0x0

    .line 592467
    iput-object p4, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592468
    iput-object p3, p2, LX/9E2;->d:LX/9FA;

    .line 592469
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    :goto_0
    iput-object v2, p2, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 592470
    iget-object v2, p2, LX/9E2;->d:LX/9FA;

    if-eqz v2, :cond_1

    iget-object v2, p2, LX/9E2;->d:LX/9FA;

    .line 592471
    iget-object v4, v2, LX/9FA;->p:LX/9CP;

    move-object v4, v4

    .line 592472
    :cond_1
    iput-object v4, p2, LX/9E2;->g:LX/9CP;

    .line 592473
    iget-object v2, p2, LX/9E2;->g:LX/9CP;

    if-eqz v2, :cond_2

    .line 592474
    invoke-static {}, LX/9CP;->d()V

    .line 592475
    :cond_2
    invoke-static {p2}, LX/9E2;->k(LX/9E2;)V

    .line 592476
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v2, :cond_3

    .line 592477
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v4, p2, LX/9E2;->h:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setDisplayName(Ljava/lang/String;)V

    .line 592478
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v4, p2, LX/9E2;->i:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592479
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v4, p2, LX/9E2;->a:Lcom/facebook/feedback/ui/CommentComposerHelper;

    iget-object v5, p2, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4, v5}, Lcom/facebook/feedback/ui/CommentComposerHelper;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v4

    .line 592480
    iget-object p4, v2, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->p:Landroid/widget/ImageView;

    if-eqz v4, :cond_5

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {p4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 592481
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592482
    iget-object v4, v2, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v4, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 592483
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v2, p2}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setPostButtonListener(LX/21m;)V

    .line 592484
    iget-object v2, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v4, p2, LX/9E2;->b:LX/0QK;

    .line 592485
    iput-object v4, v2, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->v:LX/0QK;

    .line 592486
    :cond_3
    const/16 v1, 0x1f

    const v2, -0xab18199

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_4
    move-object v2, v4

    .line 592487
    goto :goto_0

    .line 592488
    :cond_5
    const/16 v5, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592453
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 592435
    check-cast p2, LX/9E2;

    .line 592436
    const/4 p3, 0x0

    .line 592437
    iget-object p0, p2, LX/9E2;->g:LX/9CP;

    if-eqz p0, :cond_0

    .line 592438
    invoke-static {}, LX/9CP;->e()V

    .line 592439
    iput-object p3, p2, LX/9E2;->g:LX/9CP;

    .line 592440
    :cond_0
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz p0, :cond_1

    .line 592441
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592442
    iput-object p3, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->v:LX/0QK;

    .line 592443
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {p0, p3}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setPostButtonListener(LX/21m;)V

    .line 592444
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592445
    iget-object p1, p0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 592446
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {p0, p3}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 592447
    iget-object p0, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setDisplayName(Ljava/lang/String;)V

    .line 592448
    :cond_1
    invoke-virtual {p2, p3}, LX/9E2;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 592449
    iput-object p3, p2, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 592450
    iput-object p3, p2, LX/9E2;->d:LX/9FA;

    .line 592451
    iput-object p3, p2, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 592452
    return-void
.end method
