.class public final Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2f6838ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 626401
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 626402
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 626403
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 626404
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 626405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 626406
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 626407
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 626408
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 626409
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 626410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 626411
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 626412
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 626413
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 626414
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    .line 626415
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 626416
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    .line 626417
    iput-object v0, v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->e:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    .line 626418
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 626419
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 626420
    iget-object v0, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->e:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->e:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    .line 626421
    iget-object v0, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->e:Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 626422
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 626423
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->f:I

    .line 626424
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 626425
    new-instance v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-direct {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;-><init>()V

    .line 626426
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 626427
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 626428
    const v0, -0x4070a23e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 626429
    const v0, -0x629d3544

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 626430
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 626431
    iget v0, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->f:I

    return v0
.end method
