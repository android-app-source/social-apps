.class public final Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1zl;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/io/File;


# direct methods
.method public constructor <init>(LX/1zl;Ljava/lang/String;ILjava/io/File;)V
    .locals 0

    .prologue
    .line 626526
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->a:LX/1zl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626527
    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->b:Ljava/lang/String;

    .line 626528
    iput p3, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->c:I

    .line 626529
    iput-object p4, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->d:Ljava/io/File;

    .line 626530
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 626531
    :try_start_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->d:Ljava/io/File;

    invoke-static {v1}, LX/1zm;->a(Ljava/io/File;)LX/3K3;

    move-result-object v0

    .line 626532
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->a:LX/1zl;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626533
    :try_start_1
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->a:LX/1zl;

    iget-object v2, v2, LX/1zl;->e:LX/0YU;

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->c:I

    invoke-virtual {v2, v3, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 626534
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 626535
    :try_start_2
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->a:LX/1zl;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1zl;->c(LX/1zl;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 626536
    :goto_0
    return-void

    .line 626537
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 626538
    :catch_0
    move-exception v0

    .line 626539
    sget-object v1, LX/1zl;->a:Ljava/lang/Class;

    const-string v2, "Error reading file for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
