.class public Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private A:LX/9At;

.field private B:Ljava/lang/String;

.field private C:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public D:I

.field private E:[LX/3m7;

.field private F:I

.field private G:I

.field private H:I

.field private I:LX/3m7;

.field private J:LX/3m7;

.field private K:LX/3m7;

.field private L:LX/3m7;

.field private M:LX/3m7;

.field private N:LX/3m7;

.field private O:LX/3m7;

.field private P:LX/3m7;

.field private Q:LX/3m7;

.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1za;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/feedback/reactions/ui/PillsBlingBarTextSize;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/153;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1VI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final j:Landroid/content/res/Resources;

.field private k:F

.field private l:F

.field private final m:[F

.field private n:Landroid/graphics/Paint;

.field private o:I

.field private p:I

.field private q:I

.field private r:F

.field public s:Z

.field private t:Z

.field private u:Landroid/view/View$OnClickListener;

.field private v:Landroid/view/View$OnClickListener;

.field private w:Landroid/view/View$OnClickListener;

.field private x:LX/20D;

.field private y:Lcom/facebook/fbui/facepile/FacepileView;

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589169
    new-instance v0, LX/3WB;

    invoke-direct {v0}, LX/3WB;-><init>()V

    sput-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 589367
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 589368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 589369
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 589370
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 589371
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 589372
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->m:[F

    .line 589373
    const-class v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 589374
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setWillNotDraw(Z)V

    .line 589375
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->j:Landroid/content/res/Resources;

    .line 589376
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->l:F

    .line 589377
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a()V

    .line 589378
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->j:Landroid/content/res/Resources;

    invoke-direct {p0, v0, p2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(Landroid/content/res/Resources;Landroid/util/AttributeSet;)V

    .line 589379
    return-void
.end method

.method private a(LX/3m7;)I
    .locals 2

    .prologue
    .line 589380
    iget v0, p1, LX/3m7;->c:I

    move v0, v0

    .line 589381
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 589382
    :cond_0
    iget v0, p1, LX/3m7;->b:I

    move v0, v0

    .line 589383
    iget v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private a(II)LX/39I;
    .locals 6

    .prologue
    .line 589384
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b:LX/154;

    invoke-virtual {v0, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 589385
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->j:Landroid/content/res/Resources;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, p2, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 589386
    :cond_0
    new-instance v0, LX/39I;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->k:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->descent()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v5, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    move v4, p1

    invoke-direct/range {v0 .. v5}, LX/39I;-><init>(Ljava/lang/String;IFILandroid/graphics/Paint;)V

    return-object v0
.end method

.method private a(ILcom/facebook/graphql/model/GraphQLFeedback;)LX/3m7;
    .locals 3

    .prologue
    .line 589387
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    if-nez v0, :cond_0

    .line 589388
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20D;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    .line 589389
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->e:LX/1za;

    invoke-virtual {v1, p2}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/20D;->a(Ljava/util/List;)V

    .line 589390
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    iget-boolean v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->s:Z

    invoke-virtual {v0, v1}, LX/20D;->a(Z)V

    .line 589391
    new-instance v0, LX/39J;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    invoke-virtual {v1}, LX/20D;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->x:LX/20D;

    invoke-direct {v0, p1, v1, v2}, LX/39J;-><init>(IILX/20D;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 589392
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    .line 589393
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    .line 589394
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    .line 589395
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    .line 589396
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    .line 589397
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    .line 589398
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    .line 589399
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    .line 589400
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    .line 589401
    return-void
.end method

.method private a(Landroid/content/res/Resources;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 589402
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    .line 589403
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 589404
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 589405
    new-array v0, v4, [I

    const v1, 0x1010098

    aput v1, v0, v3

    .line 589406
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, p2, v0, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 589407
    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->i:LX/1VI;

    invoke-virtual {v0}, LX/1VI;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a00aa

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_1
    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 589408
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 589409
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 589410
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setLinearText(Z)V

    .line 589411
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 589412
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->k:F

    .line 589413
    const v0, 0x7f0b0fe4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    .line 589414
    const v0, 0x7f0b0fe5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    .line 589415
    const v0, 0x7f0b0fe1

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->o:I

    .line 589416
    return-void

    .line 589417
    :cond_0
    const v0, 0x7f0b004e

    goto :goto_0

    .line 589418
    :cond_1
    const v0, 0x7f0a00e6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 589419
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->c:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v7

    .line 589420
    if-nez v7, :cond_0

    move v6, v1

    .line 589421
    :goto_0
    if-nez v7, :cond_1

    move v4, v5

    .line 589422
    :goto_1
    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getPaddingStart()I

    move-result v0

    :goto_2
    move v2, v0

    move v0, v1

    .line 589423
    :goto_3
    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->E:[LX/3m7;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 589424
    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->F:I

    shl-int v8, v5, v0

    and-int/2addr v3, v8

    shl-int v8, v5, v0

    if-ne v3, v8, :cond_5

    .line 589425
    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->E:[LX/3m7;

    aget-object v8, v3, v0

    .line 589426
    if-eqz v7, :cond_3

    if-nez v0, :cond_3

    move v3, v1

    .line 589427
    :goto_4
    iget v9, v8, LX/3m7;->b:I

    move v9, v9

    .line 589428
    add-int/2addr v3, v9

    .line 589429
    mul-int v9, v3, v6

    add-int/2addr v2, v9

    .line 589430
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    int-to-float v10, v2

    cmpl-float v9, v9, v10

    if-lez v9, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 589431
    iget v10, v8, LX/3m7;->b:I

    move v10, v10

    .line 589432
    add-int/2addr v10, v2

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 589433
    iget-object v0, v8, LX/3m7;->d:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 589434
    if-eqz v0, :cond_7

    .line 589435
    iget-object v0, v8, LX/3m7;->d:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 589436
    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 589437
    :goto_5
    return-void

    .line 589438
    :cond_0
    const/4 v0, -0x1

    move v6, v0

    goto :goto_0

    :cond_1
    move v4, v1

    .line 589439
    goto :goto_1

    .line 589440
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getPaddingStart()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_2

    .line 589441
    :cond_3
    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    goto :goto_4

    .line 589442
    :cond_4
    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 589443
    :cond_5
    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->G:I

    if-ne v0, v3, :cond_6

    .line 589444
    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->H:I

    mul-int/2addr v3, v6

    add-int/2addr v2, v3

    .line 589445
    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->H:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 589446
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 589447
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->callOnClick()Z

    goto :goto_5
.end method

.method private static a(Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;LX/154;LX/0hL;LX/0Or;LX/1za;LX/0Or;LX/153;Landroid/view/LayoutInflater;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
            "LX/154;",
            "LX/0hL;",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;",
            "LX/1za;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/153;",
            "Landroid/view/LayoutInflater;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 589448
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b:LX/154;

    iput-object p2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->c:LX/0hL;

    iput-object p3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->d:LX/0Or;

    iput-object p4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->e:LX/1za;

    iput-object p5, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->f:LX/0Or;

    iput-object p6, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->g:LX/153;

    iput-object p7, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->h:Landroid/view/LayoutInflater;

    iput-object p8, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->i:LX/1VI;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {v8}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    invoke-static {v8}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v2

    check-cast v2, LX/0hL;

    const/16 v3, 0x7af

    invoke-static {v8, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v8}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v4

    check-cast v4, LX/1za;

    const/16 v5, 0x15d0

    invoke-static {v8, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v8}, LX/153;->b(LX/0QB;)LX/153;

    move-result-object v6

    check-cast v6, LX/153;

    invoke-static {v8}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    invoke-static {v8}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v8

    check-cast v8, LX/1VI;

    invoke-static/range {v0 .. v8}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;LX/154;LX/0hL;LX/0Or;LX/1za;LX/0Or;LX/153;Landroid/view/LayoutInflater;LX/1VI;)V

    return-void
.end method

.method private b(ILcom/facebook/graphql/model/GraphQLFeedback;)LX/3m7;
    .locals 6

    .prologue
    .line 589449
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->g:LX/153;

    invoke-virtual {v0, p2}, LX/153;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v1

    .line 589450
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/39I;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->k:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->descent()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v5, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    move v4, p1

    invoke-direct/range {v0 .. v5}, LX/39I;-><init>(Ljava/lang/String;IFILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 589451
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->invalidate()V

    .line 589452
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->requestLayout()V

    .line 589453
    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 589342
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    .line 589343
    iget v1, v0, LX/3m7;->c:I

    move v0, v1

    .line 589344
    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->B:Ljava/lang/String;

    if-eq v0, p3, :cond_2

    .line 589345
    :cond_0
    iput-object p3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->B:Ljava/lang/String;

    .line 589346
    if-eqz p1, :cond_1

    if-nez p2, :cond_3

    .line 589347
    :cond_1
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    .line 589348
    :goto_0
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589349
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByFacepileClickListener(Landroid/view/View$OnClickListener;)V

    .line 589350
    return-void

    .line 589351
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->z:Ljava/util/List;

    if-nez v0, :cond_4

    .line 589352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->z:Ljava/util/List;

    .line 589353
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 589354
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_5

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 589355
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->z:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 589356
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 589357
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->j:Landroid/content/res/Resources;

    const v1, 0x7f08199f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 589358
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->y:Lcom/facebook/fbui/facepile/FacepileView;

    if-nez v0, :cond_6

    .line 589359
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->h:Landroid/view/LayoutInflater;

    const v1, 0x7f03019f

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->y:Lcom/facebook/fbui/facepile/FacepileView;

    .line 589360
    :cond_6
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->y:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceCountForOverflow(I)V

    .line 589361
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->y:Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->z:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 589362
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->j:Landroid/content/res/Resources;

    const v1, 0x7f0819a2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 589363
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->A:LX/9At;

    if-nez v0, :cond_7

    .line 589364
    new-instance v0, LX/9At;

    invoke-direct {v0}, LX/9At;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->A:LX/9At;

    .line 589365
    :cond_7
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->A:LX/9At;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->y:Lcom/facebook/fbui/facepile/FacepileView;

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v2, v6

    float-to-int v6, v2

    iget v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v7, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->k:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v2, v7

    iget-object v7, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->descent()F

    move-result v7

    sub-float v7, v2, v7

    iget-object v8, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->c:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v9

    move v2, p1

    invoke-virtual/range {v0 .. v9}, LX/9At;->a(Lcom/facebook/fbui/facepile/FacepileView;IILjava/lang/String;Ljava/lang/String;IFLandroid/graphics/Paint;Z)V

    .line 589366
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->A:LX/9At;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    goto/16 :goto_0
.end method

.method public getCommentsCount()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 589454
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    .line 589455
    iget p0, v0, LX/3m7;->c:I

    move v0, p0

    .line 589456
    return v0
.end method

.method public getReactionsCount()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 589168
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->D:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 589170
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 589171
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->c:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v6

    .line 589172
    if-nez v6, :cond_2

    move v5, v2

    .line 589173
    :goto_0
    if-nez v6, :cond_3

    move v0, v1

    .line 589174
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 589175
    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v3

    .line 589176
    if-nez v6, :cond_4

    int-to-float v3, v3

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    iget v7, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->r:F

    add-float/2addr v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move v3, v2

    .line 589177
    :goto_3
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->E:[LX/3m7;

    array-length v4, v4

    if-ge v3, v4, :cond_6

    .line 589178
    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->F:I

    shl-int v7, v1, v3

    and-int/2addr v4, v7

    if-lez v4, :cond_0

    .line 589179
    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->E:[LX/3m7;

    aget-object v7, v4, v3

    .line 589180
    if-eqz v6, :cond_5

    if-nez v3, :cond_5

    move v4, v2

    .line 589181
    :goto_4
    iget v8, v7, LX/3m7;->b:I

    move v8, v8

    .line 589182
    add-int/2addr v4, v8

    .line 589183
    mul-int v8, v4, v5

    int-to-float v8, v8

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589184
    invoke-virtual {v7, p1}, LX/3m7;->a(Landroid/graphics/Canvas;)V

    .line 589185
    mul-int/2addr v4, v0

    int-to-float v4, v4

    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589186
    :cond_0
    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->G:I

    if-ne v3, v4, :cond_1

    .line 589187
    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->H:I

    mul-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589188
    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->H:I

    mul-int/2addr v4, v0

    int-to-float v4, v4

    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589189
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 589190
    :cond_2
    const/4 v0, -0x1

    move v5, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 589191
    goto :goto_1

    .line 589192
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getMeasuredWidth()I

    move-result v4

    sub-int v3, v4, v3

    int-to-float v3, v3

    goto :goto_2

    .line 589193
    :cond_5
    iget v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    goto :goto_4

    .line 589194
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 589195
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 589196
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 589197
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-ne v0, v1, :cond_3

    const v0, 0x7f0f0060

    .line 589198
    :goto_0
    iget v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->D:I

    invoke-direct {p0, v1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    iget-object v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    iget-object v5, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    iget-object v6, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 589199
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 589200
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589201
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 589202
    :cond_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3m7;

    .line 589203
    iget v5, v0, LX/3m7;->c:I

    move v5, v5

    .line 589204
    if-lez v5, :cond_1

    .line 589205
    instance-of v5, v0, LX/39I;

    if-eqz v5, :cond_4

    .line 589206
    check-cast v0, LX/39I;

    .line 589207
    iget-object v5, v0, LX/39I;->e:Ljava/lang/String;

    move-object v0, v5

    .line 589208
    invoke-static {v3, v0, v7}, LX/0wL;->a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V

    .line 589209
    :cond_1
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 589210
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 589211
    :cond_2
    const-class v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 589212
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 589213
    :cond_3
    const v0, 0x7f0f00c4

    goto :goto_0

    .line 589214
    :cond_4
    instance-of v5, v0, LX/9At;

    if-eqz v5, :cond_1

    .line 589215
    check-cast v0, LX/9At;

    .line 589216
    iget-object v5, v0, LX/9At;->g:Ljava/lang/String;

    move-object v0, v5

    .line 589217
    invoke-static {v3, v0, v7}, LX/0wL;->a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V

    goto :goto_2

    .line 589218
    :cond_5
    return-void
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 589219
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 589220
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getMeasuredWidth()I

    move-result v0

    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v6

    sub-int/2addr v0, v6

    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v6

    sub-int v6, v0, v6

    .line 589221
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    .line 589222
    iget v7, v0, LX/3m7;->c:I

    move v0, v7

    .line 589223
    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    invoke-direct {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(LX/3m7;)I

    move-result v0

    iget-object v7, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    invoke-direct {p0, v7}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(LX/3m7;)I

    move-result v7

    sub-int v7, v6, v7

    iget-object v8, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    invoke-direct {p0, v8}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(LX/3m7;)I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    invoke-direct {p0, v8}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(LX/3m7;)I

    move-result v8

    sub-int/2addr v7, v8

    if-gt v0, v7, :cond_2

    move v0, v1

    .line 589224
    :goto_0
    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    shl-int v7, v1, v0

    .line 589225
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->D:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    .line 589226
    iget v8, v0, LX/3m7;->c:I

    move v0, v8

    .line 589227
    if-lez v0, :cond_4

    :cond_0
    move v0, v4

    :goto_2
    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->G:I

    .line 589228
    const/16 v0, 0x9

    new-array v8, v0, [LX/3m7;

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    aput-object v0, v8, v5

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    aput-object v0, v8, v1

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    aput-object v0, v8, v2

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    aput-object v0, v8, v4

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    aput-object v0, v8, v3

    const/4 v0, 0x5

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    aput-object v2, v8, v0

    const/4 v0, 0x6

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    aput-object v2, v8, v0

    const/4 v0, 0x7

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    aput-object v2, v8, v0

    const/16 v0, 0x8

    iget-object v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    aput-object v2, v8, v0

    move v3, v5

    move v0, v6

    move v2, v5

    .line 589229
    :goto_3
    const/16 v4, 0x9

    if-ge v3, v4, :cond_5

    .line 589230
    aget-object v4, v8, v3

    .line 589231
    shl-int v6, v1, v3

    and-int/2addr v6, v7

    if-nez v6, :cond_1

    .line 589232
    iget v6, v4, LX/3m7;->c:I

    move v6, v6

    .line 589233
    if-lez v6, :cond_1

    .line 589234
    iget v6, v4, LX/3m7;->b:I

    move v6, v6

    .line 589235
    iget v9, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    add-int/2addr v6, v9

    if-gt v6, v0, :cond_1

    .line 589236
    shl-int v6, v1, v3

    or-int/2addr v2, v6

    .line 589237
    iget v6, v4, LX/3m7;->b:I

    move v4, v6

    .line 589238
    iget v6, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    add-int/2addr v4, v6

    sub-int/2addr v0, v4

    .line 589239
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_2
    move v0, v5

    .line 589240
    goto :goto_0

    :cond_3
    move v0, v2

    .line 589241
    goto :goto_1

    :cond_4
    move v0, v3

    .line 589242
    goto :goto_2

    .line 589243
    :cond_5
    iget v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->p:I

    add-int/2addr v0, v1

    .line 589244
    iput-object v8, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->E:[LX/3m7;

    .line 589245
    iput v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->F:I

    .line 589246
    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->H:I

    .line 589247
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getPaddingTop()I

    move-result v1

    iget v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->F:I

    if-nez v0, :cond_6

    move v0, v5

    :goto_4
    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 589248
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v1, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setMeasuredDimension(II)V

    .line 589249
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->o:I

    iget v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->r:F

    .line 589250
    return-void

    .line 589251
    :cond_6
    iget v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->o:I

    goto :goto_4
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x229e3ee2

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589252
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 589253
    :cond_0
    :goto_0
    const v1, 0x4fe5876d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return v3

    .line 589254
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->m:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aput v2, v1, v4

    .line 589255
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->m:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aput v2, v1, v3

    .line 589256
    iput-boolean v4, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->t:Z

    goto :goto_0

    .line 589257
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->m:[F

    aget v1, v1, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->l:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->m:[F

    aget v1, v1, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->l:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 589258
    :cond_1
    iput-boolean v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->t:Z

    goto :goto_0

    .line 589259
    :pswitch_2
    iget-boolean v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->t:Z

    if-nez v1, :cond_0

    .line 589260
    invoke-direct {p0, p1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(Landroid/view/MotionEvent;)V

    .line 589261
    invoke-virtual {p0, v3}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->sendAccessibilityEvent(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setClipTokens(Z)V
    .locals 0

    .prologue
    .line 589262
    iput-boolean p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->s:Z

    .line 589263
    return-void
.end method

.method public setCommentsCount(I)V
    .locals 2

    .prologue
    .line 589264
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    .line 589265
    iget v1, v0, LX/3m7;->c:I

    move v0, v1

    .line 589266
    if-eq v0, p1, :cond_0

    .line 589267
    if-nez p1, :cond_1

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->M:LX/3m7;

    .line 589268
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589269
    :cond_0
    return-void

    .line 589270
    :cond_1
    const v0, 0x7f0f0064

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    goto :goto_0
.end method

.method public setProfileVideoViewsCount(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 589271
    if-nez p1, :cond_1

    .line 589272
    sget-object v0, LX/3m7;->a:LX/3m7;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    .line 589273
    :cond_0
    :goto_0
    return-void

    .line 589274
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    instance-of v0, v0, LX/39I;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    check-cast v0, LX/39I;

    .line 589275
    iget-object v1, v0, LX/39I;->e:Ljava/lang/String;

    move-object v0, v1

    .line 589276
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 589277
    :cond_2
    new-instance v0, LX/39I;

    iget-object v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v2, v1

    iget v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->q:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->k:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    sub-float v3, v1, v3

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->n:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/39I;-><init>(Ljava/lang/String;IFILandroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->L:LX/3m7;

    .line 589278
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    goto :goto_0
.end method

.method public setReactionsClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 589279
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->v:Landroid/view/View$OnClickListener;

    .line 589280
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_0

    .line 589281
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    .line 589282
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589283
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_1

    .line 589284
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    .line 589285
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589286
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_2

    .line 589287
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    .line 589288
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589289
    :cond_2
    return-void
.end method

.method public setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 589290
    invoke-static {p1}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 589291
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    .line 589292
    iget v2, v0, LX/3m7;->c:I

    move v0, v2

    .line 589293
    if-eq v0, v1, :cond_0

    .line 589294
    iput v1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->D:I

    .line 589295
    if-nez v1, :cond_1

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->K:LX/3m7;

    .line 589296
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->C:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 589297
    invoke-direct {p0, v1, p1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(ILcom/facebook/graphql/model/GraphQLFeedback;)LX/3m7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->I:LX/3m7;

    .line 589298
    if-nez v1, :cond_2

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_1
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->J:LX/3m7;

    .line 589299
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589300
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactionsClickListener(Landroid/view/View$OnClickListener;)V

    .line 589301
    return-void

    .line 589302
    :cond_1
    const v0, 0x7f0f00c1

    invoke-direct {p0, v1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    goto :goto_0

    .line 589303
    :cond_2
    invoke-direct {p0, v1, p1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b(ILcom/facebook/graphql/model/GraphQLFeedback;)LX/3m7;

    move-result-object v0

    goto :goto_1
.end method

.method public setSeenByClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 589304
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->u:Landroid/view/View$OnClickListener;

    .line 589305
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_0

    .line 589306
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    .line 589307
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589308
    :cond_0
    return-void
.end method

.method public setSeenByCount(I)V
    .locals 2

    .prologue
    .line 589309
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    .line 589310
    iget v1, v0, LX/3m7;->c:I

    move v0, v1

    .line 589311
    if-eq v0, p1, :cond_0

    .line 589312
    if-nez p1, :cond_1

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->P:LX/3m7;

    .line 589313
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589314
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByClickListener(Landroid/view/View$OnClickListener;)V

    .line 589315
    return-void

    .line 589316
    :cond_1
    const v0, 0x7f0f00c3

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    goto :goto_0
.end method

.method public setSeenByFacepileClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 589317
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->u:Landroid/view/View$OnClickListener;

    .line 589318
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_0

    .line 589319
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->Q:LX/3m7;

    .line 589320
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589321
    :cond_0
    return-void
.end method

.method public setSharesCount(I)V
    .locals 2

    .prologue
    .line 589322
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    .line 589323
    iget v1, v0, LX/3m7;->c:I

    move v0, v1

    .line 589324
    if-eq v0, p1, :cond_0

    .line 589325
    if-nez p1, :cond_1

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->N:LX/3m7;

    .line 589326
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589327
    :cond_0
    return-void

    .line 589328
    :cond_1
    const v0, 0x7f0f0063

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    goto :goto_0
.end method

.method public setViewsCount(I)V
    .locals 2

    .prologue
    .line 589329
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    .line 589330
    iget v1, v0, LX/3m7;->c:I

    move v0, v1

    .line 589331
    if-eq v0, p1, :cond_0

    .line 589332
    if-nez p1, :cond_1

    sget-object v0, LX/3m7;->a:LX/3m7;

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    .line 589333
    invoke-direct {p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->b()V

    .line 589334
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setViewsCountClickListener(Landroid/view/View$OnClickListener;)V

    .line 589335
    return-void

    .line 589336
    :cond_1
    const v0, 0x7f0f00c2

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(II)LX/39I;

    move-result-object v0

    goto :goto_0
.end method

.method public setViewsCountClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 589337
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->w:Landroid/view/View$OnClickListener;

    .line 589338
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    sget-object v1, LX/3m7;->a:LX/3m7;

    if-eq v0, v1, :cond_0

    .line 589339
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->O:LX/3m7;

    .line 589340
    iput-object p1, v0, LX/3m7;->d:Landroid/view/View$OnClickListener;

    .line 589341
    :cond_0
    return-void
.end method
