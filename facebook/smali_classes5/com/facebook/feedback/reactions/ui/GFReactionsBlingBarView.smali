.class public Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:I

.field public d:I

.field public e:Landroid/content/Context;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589459
    const-class v0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 589460
    new-instance v0, LX/3WC;

    invoke-direct {v0}, LX/3WC;-><init>()V

    sput-object v0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 589461
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 589462
    iput v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->c:I

    .line 589463
    iput v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->d:I

    .line 589464
    iput-object p1, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->e:Landroid/content/Context;

    .line 589465
    const v0, 0x7f0307a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 589466
    const v0, 0x7f0d146a

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 589467
    const v0, 0x7f0d1469

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 589468
    const v0, 0x7f0d1467

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 589469
    const v0, 0x7f0d1468

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 589470
    new-instance v1, LX/4Ab;

    invoke-direct {v1}, LX/4Ab;-><init>()V

    .line 589471
    const/4 v0, 0x1

    .line 589472
    iput-boolean v0, v1, LX/4Ab;->b:Z

    .line 589473
    iget-object v0, p0, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(LX/4Ab;)V

    .line 589474
    return-void
.end method
