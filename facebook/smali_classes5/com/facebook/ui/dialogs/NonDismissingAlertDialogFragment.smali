.class public abstract Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/31Y;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 806165
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/31Y;
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 806182
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;->a()LX/31Y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;->m:LX/31Y;

    .line 806183
    iget-object v0, p0, Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;->m:LX/31Y;

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x75973b15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 806166
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 806167
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806168
    check-cast v0, LX/2EJ;

    .line 806169
    if-nez v0, :cond_0

    .line 806170
    const/16 v0, 0x2b

    const v2, 0xb07e451

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 806171
    :goto_0
    return-void

    .line 806172
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v2

    .line 806173
    if-eqz v2, :cond_1

    .line 806174
    new-instance v3, LX/4mh;

    invoke-direct {v3, p0, v0}, LX/4mh;-><init>(Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;LX/2EJ;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 806175
    :cond_1
    const/4 v2, -0x3

    invoke-virtual {v0, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v2

    .line 806176
    if-eqz v2, :cond_2

    .line 806177
    new-instance v3, LX/4mi;

    invoke-direct {v3, p0, v0}, LX/4mi;-><init>(Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;LX/2EJ;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 806178
    :cond_2
    const/4 v2, -0x2

    invoke-virtual {v0, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v2

    .line 806179
    if-eqz v2, :cond_3

    .line 806180
    new-instance v3, LX/4mj;

    invoke-direct {v3, p0, v0}, LX/4mj;-><init>(Lcom/facebook/ui/dialogs/NonDismissingAlertDialogFragment;LX/2EJ;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 806181
    :cond_3
    const v0, 0x4d80df3d    # 2.70264224E8f

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method
