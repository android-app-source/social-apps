.class public Lcom/facebook/ui/dialogs/ProgressDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field private j:Z

.field private k:Landroid/content/DialogInterface$OnCancelListener;

.field private l:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 806220
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(IZZ)Landroid/support/v4/app/DialogFragment;
    .locals 2

    .prologue
    .line 806219
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, p2, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(IZZZ)Landroid/support/v4/app/DialogFragment;
    .locals 1

    .prologue
    .line 806218
    const-string v0, ""

    invoke-static {v0, p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;IZZZ)Landroid/support/v4/app/DialogFragment;
    .locals 3

    .prologue
    .line 806209
    new-instance v0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-direct {v0}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;-><init>()V

    .line 806210
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 806211
    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 806212
    const-string v2, "message_res_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 806213
    const-string v2, "is_indeterminate"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 806214
    const-string v2, "is_cancelable"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 806215
    const-string v2, "dismiss_on_pause"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 806216
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 806217
    return-object v0
.end method

.method public static a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;
    .locals 2

    .prologue
    .line 806208
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, p2, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 806221
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 806222
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 806223
    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 806224
    const-string v3, "message_res_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 806225
    const-string v4, "is_indeterminate"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 806226
    const-string v5, "is_cancelable"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 806227
    const-string v6, "dismiss_on_pause"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->j:Z

    .line 806228
    const-string v6, "window_type"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 806229
    new-instance v6, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 806230
    const/4 v7, 0x0

    .line 806231
    iput v7, v6, LX/4BY;->d:I

    .line 806232
    invoke-virtual {v6, v4}, LX/4BY;->a(Z)V

    .line 806233
    invoke-virtual {v6, v5}, LX/4BY;->setCancelable(Z)V

    .line 806234
    invoke-virtual {p0, v5}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 806235
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 806236
    invoke-virtual {v6, v1}, LX/4BY;->setTitle(Ljava/lang/CharSequence;)V

    .line 806237
    :cond_0
    if-lez v3, :cond_5

    .line 806238
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 806239
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->k:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v1, :cond_2

    .line 806240
    iget-object v1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->k:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v6, v1}, LX/4BY;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 806241
    :cond_2
    iget-object v1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->l:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v1, :cond_3

    .line 806242
    iget-object v1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->l:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v6, v1}, LX/4BY;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 806243
    :cond_3
    if-lez v0, :cond_4

    .line 806244
    invoke-virtual {v6}, LX/4BY;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setType(I)V

    .line 806245
    :cond_4
    return-object v6

    .line 806246
    :cond_5
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 806247
    invoke-virtual {v6, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    .prologue
    .line 806202
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806203
    if-eqz v0, :cond_0

    .line 806204
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806205
    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 806206
    :goto_0
    return-void

    .line 806207
    :cond_0
    iput-object p1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->k:Landroid/content/DialogInterface$OnCancelListener;

    goto :goto_0
.end method

.method public final a(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 1

    .prologue
    .line 806196
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806197
    if-eqz v0, :cond_0

    .line 806198
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806199
    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 806200
    :goto_0
    return-void

    .line 806201
    :cond_0
    iput-object p1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->l:Landroid/content/DialogInterface$OnDismissListener;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 806192
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806193
    if-eqz v0, :cond_0

    .line 806194
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 806195
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4bc9a756

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 806188
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 806189
    iput-object v2, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->k:Landroid/content/DialogInterface$OnCancelListener;

    .line 806190
    iput-object v2, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->l:Landroid/content/DialogInterface$OnDismissListener;

    .line 806191
    const/16 v1, 0x2b

    const v2, 0x20137081

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d727762

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 806184
    iget-boolean v1, p0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->j:Z

    if-eqz v1, :cond_0

    .line 806185
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 806186
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 806187
    const/16 v1, 0x2b

    const v2, 0x30a0ad8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
