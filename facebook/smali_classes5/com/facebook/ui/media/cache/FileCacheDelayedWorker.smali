.class public Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;
.super Lcom/facebook/delayedworker/AbstractDelayedWorker;
.source ""


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Ha;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/2TG;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 573681
    invoke-direct {p0}, Lcom/facebook/delayedworker/AbstractDelayedWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 573682
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;

    new-instance v3, LX/0U8;

    invoke-interface {v2}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    new-instance v1, LX/43g;

    invoke-direct {v1, v2}, LX/43g;-><init>(LX/0QB;)V

    invoke-direct {v3, v0, v1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v3, v3

    invoke-static {v2}, LX/2TG;->a(LX/0QB;)LX/2TG;

    move-result-object v2

    check-cast v2, LX/2TG;

    iput-object v3, p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;->a:Ljava/util/Set;

    iput-object v2, p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;->b:LX/2TG;

    .line 573683
    return-void
.end method

.method public final b()V
    .locals 15

    .prologue
    .line 573684
    const-wide/16 v0, 0x0

    .line 573685
    iget-object v2, p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;->a:Ljava/util/Set;

    if-eqz v2, :cond_0

    .line 573686
    iget-object v2, p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ha;

    .line 573687
    const-wide v6, 0x134fd9000L

    invoke-interface {v0, v6, v7}, LX/1Ha;->a(J)J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 573688
    goto :goto_0

    :cond_0
    move-wide v2, v0

    .line 573689
    :cond_1
    iget-object v0, p0, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;->b:LX/2TG;

    const-wide v12, 0x134fd9000L

    .line 573690
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_2

    .line 573691
    iget-object v8, v0, LX/2TG;->a:LX/1ES;

    const-class v9, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;

    const-wide/32 v10, 0x15180

    sub-long/2addr v12, v2

    invoke-static {v12, v13}, LX/1lQ;->m(J)J

    move-result-wide v12

    add-long/2addr v10, v12

    invoke-virtual {v8, v9, v10, v11}, LX/1ES;->a(Ljava/lang/Class;J)V

    .line 573692
    :goto_1
    return-void

    .line 573693
    :cond_2
    iget-object v8, v0, LX/2TG;->a:LX/1ES;

    const-class v9, Lcom/facebook/ui/media/cache/FileCacheDelayedWorker;

    invoke-static {v12, v13}, LX/1lQ;->m(J)J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, LX/1ES;->a(Ljava/lang/Class;J)V

    goto :goto_1
.end method
