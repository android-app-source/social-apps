.class public Lcom/facebook/ui/media/attachments/MediaResourceHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:Lcom/facebook/ui/media/attachments/MediaResourceHelper;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/ContentResolver;

.field public final d:LX/2MV;

.field private final e:LX/2MZ;

.field private final f:LX/1Er;

.field private final g:LX/2MM;

.field public final h:LX/2Ma;

.field private final i:LX/0Sh;

.field private final j:LX/03V;

.field private final k:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 572059
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    sput-object v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Landroid/content/ContentResolver;LX/2MV;LX/2MZ;LX/2MM;LX/2Ma;LX/1Er;LX/0Sh;LX/03V;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 572281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572282
    iput-object p2, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c:Landroid/content/ContentResolver;

    .line 572283
    iput-object p3, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->d:LX/2MV;

    .line 572284
    iput-object p4, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->e:LX/2MZ;

    .line 572285
    iput-object p5, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g:LX/2MM;

    .line 572286
    iput-object p6, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->h:LX/2Ma;

    .line 572287
    iput-object p7, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->f:LX/1Er;

    .line 572288
    iput-object p8, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->i:LX/0Sh;

    .line 572289
    iput-object p9, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->j:LX/03V;

    .line 572290
    iput-object p1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->k:LX/0Uh;

    .line 572291
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;
    .locals 13

    .prologue
    .line 572266
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->l:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    if-nez v0, :cond_1

    .line 572267
    const-class v1, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    monitor-enter v1

    .line 572268
    :try_start_0
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->l:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 572269
    if-eqz v2, :cond_0

    .line 572270
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 572271
    new-instance v3, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v5

    check-cast v5, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v6

    check-cast v6, LX/2MV;

    invoke-static {v0}, LX/2MZ;->a(LX/0QB;)LX/2MZ;

    move-result-object v7

    check-cast v7, LX/2MZ;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v8

    check-cast v8, LX/2MM;

    invoke-static {v0}, LX/2Ma;->a(LX/0QB;)LX/2Ma;

    move-result-object v9

    check-cast v9, LX/2Ma;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v10

    check-cast v10, LX/1Er;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;-><init>(LX/0Uh;Landroid/content/ContentResolver;LX/2MV;LX/2MZ;LX/2MM;LX/2Ma;LX/1Er;LX/0Sh;LX/03V;)V

    .line 572272
    const/16 v4, 0xb99

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 572273
    iput-object v4, v3, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a:LX/0Or;

    .line 572274
    move-object v0, v3

    .line 572275
    sput-object v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->l:Lcom/facebook/ui/media/attachments/MediaResourceHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572276
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 572277
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 572278
    :cond_1
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->l:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    return-object v0

    .line 572279
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 572280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/ui/media/attachments/MediaResourceHelper;Landroid/graphics/Canvas;Landroid/net/Uri;LX/47d;Z)V
    .locals 8

    .prologue
    .line 572249
    const/4 v3, 0x0

    .line 572250
    invoke-static {p3}, LX/47e;->a(LX/47d;)I

    move-result v0

    .line 572251
    if-eqz v0, :cond_0

    const/16 v1, 0xb4

    if-ne v0, v1, :cond_1

    .line 572252
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 572253
    :goto_0
    move-object v3, v0

    .line 572254
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    .line 572255
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-direct {v2, v4, v5}, LX/1o9;-><init>(II)V

    .line 572256
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 572257
    move-object v1, v1

    .line 572258
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    const-class v2, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v7

    .line 572259
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v6

    .line 572260
    new-instance v0, LX/5zp;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, LX/5zp;-><init>(Lcom/facebook/ui/media/attachments/MediaResourceHelper;Landroid/graphics/Canvas;Landroid/graphics/Rect;LX/47d;ZLcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v7, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 572261
    const v0, -0x2a355a89

    :try_start_0
    invoke-static {v6, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572262
    invoke-interface {v7}, LX/1ca;->g()Z

    .line 572263
    :goto_1
    return-void

    .line 572264
    :catch_0
    :goto_2
    invoke-interface {v7}, LX/1ca;->g()Z

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v7}, LX/1ca;->g()Z

    throw v0

    :catch_1
    goto :goto_2

    .line 572265
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 572244
    sget-object v2, LX/5zq;->a:[I

    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v3}, LX/2MK;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 572245
    :cond_0
    :goto_0
    return v0

    .line 572246
    :pswitch_0
    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    sget-object v3, LX/47d;->UNDEFINED:LX/47d;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 572247
    :pswitch_1
    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 572248
    :pswitch_2
    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(LX/5zn;)V
    .locals 4

    .prologue
    .line 572230
    iget-object v0, p1, LX/5zn;->q:Ljava/lang/String;

    move-object v0, v0

    .line 572231
    if-eqz v0, :cond_0

    .line 572232
    :goto_0
    return-void

    .line 572233
    :cond_0
    iget-object v0, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 572234
    const/4 v1, 0x0

    .line 572235
    const-string v2, "content"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 572236
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 572237
    :cond_1
    :goto_1
    move-object v0, v1

    .line 572238
    iput-object v0, p1, LX/5zn;->q:Ljava/lang/String;

    .line 572239
    goto :goto_0

    .line 572240
    :cond_2
    const-string v2, "file"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 572241
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 572242
    if-eqz v2, :cond_1

    .line 572243
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->h:LX/2Ma;

    invoke-virtual {v1, v2}, LX/2Ma;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private c(LX/5zn;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 572196
    iget v0, p1, LX/5zn;->j:I

    move v0, v0

    .line 572197
    if-eqz v0, :cond_0

    .line 572198
    iget v0, p1, LX/5zn;->k:I

    move v0, v0

    .line 572199
    if-eqz v0, :cond_0

    .line 572200
    iget-object v0, p1, LX/5zn;->l:LX/47d;

    move-object v0, v0

    .line 572201
    sget-object v1, LX/47d;->UNDEFINED:LX/47d;

    if-eq v0, v1, :cond_0

    .line 572202
    :goto_0
    return-void

    .line 572203
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g:LX/2MM;

    .line 572204
    iget-object v0, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v2, v0

    .line 572205
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->k:LX/0Uh;

    const/16 v3, 0x268

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 572206
    :try_start_1
    new-instance v0, Landroid/media/ExifInterface;

    iget-object v2, v1, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 572207
    const-string v2, "Orientation"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    .line 572208
    invoke-static {v0}, LX/47d;->fromExifInterfaceOrientation(I)LX/47d;

    move-result-object v2

    .line 572209
    iput-object v2, p1, LX/5zn;->l:LX/47d;

    .line 572210
    iget-object v2, p1, LX/5zn;->l:LX/47d;

    move-object v2, v2

    .line 572211
    sget-object v3, LX/47d;->UNDEFINED:LX/47d;

    if-ne v2, v3, :cond_1

    .line 572212
    sget-object v2, LX/47d;->NORMAL:LX/47d;

    .line 572213
    iput-object v2, p1, LX/5zn;->l:LX/47d;

    .line 572214
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 572215
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 572216
    iget-object v3, v1, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 572217
    if-eqz v0, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    .line 572218
    :cond_2
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 572219
    iput v0, p1, LX/5zn;->j:I

    .line 572220
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 572221
    iput v0, p1, LX/5zn;->k:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572222
    :goto_2
    :try_start_2
    invoke-virtual {v1}, LX/46f;->a()V

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 572223
    :cond_3
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 572224
    :cond_4
    :try_start_3
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 572225
    iput v0, p1, LX/5zn;->j:I

    .line 572226
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 572227
    iput v0, p1, LX/5zn;->k:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 572228
    goto :goto_2

    .line 572229
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/46f;->a()V

    throw v0
.end method

.method public static c(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 2

    .prologue
    .line 572193
    invoke-static {p0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 572194
    sget-object v0, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 572195
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 5

    .prologue
    .line 572189
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 572190
    if-eqz v0, :cond_2

    .line 572191
    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 572192
    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static e(LX/5zn;)V
    .locals 6

    .prologue
    .line 572175
    iget-wide v4, p0, LX/5zn;->i:J

    move-wide v0, v4

    .line 572176
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 572177
    :goto_0
    return-void

    .line 572178
    :cond_0
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 572179
    :try_start_0
    iget-object v0, p0, LX/5zn;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 572180
    const-string v2, "file"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 572181
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 572182
    :cond_1
    :goto_1
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 572183
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v2, v0

    .line 572184
    iput-wide v2, p0, LX/5zn;->i:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572185
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 572186
    :cond_2
    :try_start_1
    const-string v2, "https"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "http"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 572187
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 572188
    :catch_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method

.method private f(LX/5zn;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 572104
    iget-object v0, p1, LX/5zn;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 572105
    if-eqz v0, :cond_0

    .line 572106
    :goto_0
    return-void

    .line 572107
    :cond_0
    :try_start_0
    iget-object v0, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 572108
    iget-boolean v3, p1, LX/5zn;->t:Z

    move v3, v3

    .line 572109
    if-eqz v3, :cond_5

    .line 572110
    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->e:LX/2MZ;

    const/4 v4, 0x1

    .line 572111
    iget-object v5, v3, LX/2MZ;->d:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->b()V

    .line 572112
    const/4 v5, 0x0

    invoke-static {v3, v0, v4, v5}, LX/2MZ;->b(LX/2MZ;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v5

    move-object v0, v5

    .line 572113
    :goto_1
    if-eqz v0, :cond_b

    .line 572114
    iget-object v3, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v3, v3

    .line 572115
    if-eqz v3, :cond_7

    .line 572116
    iget-object v3, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v3, v3

    .line 572117
    sget-object v4, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    if-eq v3, v4, :cond_7

    .line 572118
    :goto_2
    if-nez v1, :cond_1

    .line 572119
    iget-boolean v2, p1, LX/5zn;->m:Z

    move v2, v2

    .line 572120
    if-eqz v2, :cond_c

    .line 572121
    :cond_1
    if-nez v1, :cond_8

    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v2, v1

    .line 572122
    :goto_3
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 572123
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 572124
    iget-boolean v4, p1, LX/5zn;->m:Z

    move v4, v4

    .line 572125
    if-eqz v4, :cond_2

    .line 572126
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 572127
    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    .line 572128
    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 572129
    :cond_2
    iget v4, v2, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    int-to-float v4, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 572130
    :goto_4
    iget-object v0, p1, LX/5zn;->n:Landroid/net/Uri;

    move-object v0, v0

    .line 572131
    if-eqz v0, :cond_4

    .line 572132
    iget-object v0, p1, LX/5zn;->l:LX/47d;

    move-object v0, v0

    .line 572133
    iget v2, p1, LX/5zn;->j:I

    move v2, v2

    .line 572134
    iget v3, p1, LX/5zn;->k:I

    move v3, v3

    .line 572135
    iget-object v4, p1, LX/5zn;->e:LX/5zi;

    move-object v4, v4

    .line 572136
    sget-object v5, LX/5zi;->CAMERA_CORE:LX/5zi;

    if-ne v4, v5, :cond_3

    if-lez v2, :cond_3

    if-lez v3, :cond_3

    .line 572137
    if-le v3, v2, :cond_9

    sget-object v0, LX/47d;->ROTATE_90:LX/47d;

    .line 572138
    :cond_3
    :goto_5
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 572139
    iget-object v3, p1, LX/5zn;->n:Landroid/net/Uri;

    move-object v3, v3

    .line 572140
    iget-boolean v4, p1, LX/5zn;->m:Z

    move v4, v4

    .line 572141
    invoke-static {p0, v2, v3, v0, v4}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Lcom/facebook/ui/media/attachments/MediaResourceHelper;Landroid/graphics/Canvas;Landroid/net/Uri;LX/47d;Z)V

    .line 572142
    :cond_4
    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->f:LX/1Er;

    const-string v3, "thumbnail"

    const-string v4, ".jpg"

    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->k:LX/0Uh;

    const/16 v5, 0x268

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_6
    invoke-virtual {v2, v3, v4, v0}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 572143
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 572144
    :try_start_1
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x55

    invoke-virtual {v1, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 572145
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572146
    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 572147
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 572148
    iput-object v0, p1, LX/5zn;->f:Landroid/net/Uri;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 572149
    goto/16 :goto_0

    .line 572150
    :catch_0
    move-exception v0

    .line 572151
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->j:LX/03V;

    const-string v2, "MediaResourceHelper_VideoThumbnailException"

    const-string v3, "Couldn\'t add video thumbnail"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 572152
    :cond_5
    :try_start_3
    iget v3, p1, LX/5zn;->u:I

    move v3, v3

    .line 572153
    if-lez v3, :cond_6

    .line 572154
    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->e:LX/2MZ;

    const/4 v4, 0x1

    .line 572155
    iget v5, p1, LX/5zn;->u:I

    move v5, v5

    .line 572156
    mul-int/lit16 v5, v5, 0x3e8

    .line 572157
    iget-object v6, v3, LX/2MZ;->d:LX/0Sh;

    invoke-virtual {v6}, LX/0Sh;->b()V

    .line 572158
    invoke-static {v3, v0, v4, v5}, LX/2MZ;->b(LX/2MZ;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v6

    move-object v0, v6

    .line 572159
    goto/16 :goto_1

    .line 572160
    :cond_6
    iget-object v3, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->e:LX/2MZ;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, LX/2MZ;->a(Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v1, v2

    .line 572161
    goto/16 :goto_2

    .line 572162
    :cond_8
    new-instance v1, Landroid/graphics/Rect;

    .line 572163
    iget-object v2, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v2, v2

    .line 572164
    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 572165
    iget-object v3, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v3, v3

    .line 572166
    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 572167
    iget-object v4, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v4, v4

    .line 572168
    iget v4, v4, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 572169
    iget-object v5, p1, LX/5zn;->s:Landroid/graphics/RectF;

    move-object v5, v5

    .line 572170
    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v2, v1

    goto/16 :goto_3

    .line 572171
    :cond_9
    sget-object v0, LX/47d;->NORMAL:LX/47d;

    goto/16 :goto_5

    .line 572172
    :cond_a
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    goto/16 :goto_6

    .line 572173
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v0

    .line 572174
    :cond_b
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->j:LX/03V;

    const-string v1, "MediaResourceHelper_VideoThumbnailFailed"

    const-string v2, "Could not fetch thumbnail for video"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :cond_c
    move-object v1, v0

    goto/16 :goto_4
.end method

.method private g(LX/5zn;)V
    .locals 6

    .prologue
    .line 572096
    iget-wide v4, p1, LX/5zn;->r:J

    move-wide v0, v4

    .line 572097
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 572098
    :goto_0
    return-void

    .line 572099
    :cond_0
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g:LX/2MM;

    .line 572100
    iget-object v1, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 572101
    invoke-virtual {v0, v1}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v0

    .line 572102
    iput-wide v0, p1, LX/5zn;->r:J

    .line 572103
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 572091
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c:Landroid/content/ContentResolver;

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 572092
    if-nez v0, :cond_0

    .line 572093
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572094
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->h:LX/2Ma;

    invoke-virtual {v1, v0}, LX/2Ma;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572095
    :cond_0
    return-object v0
.end method

.method public final a(LX/5zn;)V
    .locals 5

    .prologue
    .line 572063
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 572064
    sget-object v0, LX/5zq;->a:[I

    .line 572065
    iget-object v1, p1, LX/5zn;->c:LX/2MK;

    move-object v1, v1

    .line 572066
    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 572067
    :goto_0
    return-void

    .line 572068
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(LX/5zn;)V

    .line 572069
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(LX/5zn;)V

    .line 572070
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g(LX/5zn;)V

    goto :goto_0

    .line 572071
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(LX/5zn;)V

    .line 572072
    :try_start_0
    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->d:LX/2MV;

    .line 572073
    iget-object v3, p1, LX/5zn;->b:Landroid/net/Uri;

    move-object v3, v3

    .line 572074
    invoke-interface {v2, v3}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v2

    .line 572075
    iget v3, v2, LX/60x;->d:I

    invoke-static {v3}, LX/47e;->a(I)LX/47d;

    move-result-object v3

    .line 572076
    iput-object v3, p1, LX/5zn;->l:LX/47d;

    .line 572077
    iget v3, v2, LX/60x;->b:I

    if-lez v3, :cond_0

    iget v3, v2, LX/60x;->c:I

    if-lez v3, :cond_0

    .line 572078
    iget v3, v2, LX/60x;->b:I

    .line 572079
    iput v3, p1, LX/5zn;->j:I

    .line 572080
    iget v3, v2, LX/60x;->c:I

    .line 572081
    iput v3, p1, LX/5zn;->k:I

    .line 572082
    :cond_0
    iget-wide v2, v2, LX/60x;->a:J

    .line 572083
    iput-wide v2, p1, LX/5zn;->i:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 572084
    :goto_1
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g(LX/5zn;)V

    .line 572085
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->f(LX/5zn;)V

    goto :goto_0

    .line 572086
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(LX/5zn;)V

    .line 572087
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g(LX/5zn;)V

    .line 572088
    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->e(LX/5zn;)V

    goto :goto_0

    .line 572089
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(LX/5zn;)V

    .line 572090
    invoke-direct {p0, p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->g(LX/5zn;)V

    goto :goto_0

    :catch_0
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 1

    .prologue
    .line 572060
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    .line 572061
    invoke-virtual {p0, v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/5zn;)V

    .line 572062
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0
.end method
