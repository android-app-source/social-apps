.class public final Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4nS;

.field public final synthetic b:LX/2hV;


# direct methods
.method public constructor <init>(LX/2hV;LX/4nS;)V
    .locals 0

    .prologue
    .line 806851
    iput-object p1, p0, Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;->b:LX/2hV;

    iput-object p2, p0, Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;->a:LX/4nS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 806852
    iget-object v0, p0, Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;->a:LX/4nS;

    .line 806853
    iget-object v1, v0, LX/4nS;->e:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    .line 806854
    :try_start_0
    iget-object v1, v0, LX/4nS;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 806855
    :cond_0
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 806856
    iget-object v1, v0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, LX/4nS;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 806857
    :goto_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/4nS;->e:Landroid/widget/PopupWindow;

    .line 806858
    return-void

    .line 806859
    :catch_0
    move-exception v1

    .line 806860
    sget-object v2, LX/4nS;->a:Ljava/lang/String;

    const-string p0, "Exception while trying to dismiss the popup window."

    invoke-static {v2, p0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 806861
    :cond_1
    iget-object v1, v0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, LX/4nS;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method
