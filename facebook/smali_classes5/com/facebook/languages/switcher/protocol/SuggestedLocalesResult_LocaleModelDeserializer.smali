.class public Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 570272
    const-class v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult$LocaleModel;

    new-instance v1, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;

    invoke-direct {v1}, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 570273
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 570290
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 570291
    const-class v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult$LocaleModel;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 570292
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 570274
    const-class v1, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;

    monitor-enter v1

    .line 570275
    :try_start_0
    sget-object v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 570276
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 570277
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 570278
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 570279
    :goto_1
    return-object v0

    .line 570280
    :cond_2
    sget-object v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 570281
    if-eqz v0, :cond_0

    .line 570282
    monitor-exit v1

    goto :goto_1

    .line 570283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 570284
    :pswitch_0
    :try_start_3
    const-string v2, "locale"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 570285
    :pswitch_1
    const-class v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult$LocaleModel;

    const-string v2, "locale"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 570286
    sget-object v2, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult_LocaleModelDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570287
    monitor-exit v1

    goto :goto_1

    .line 570288
    :catch_0
    move-exception v0

    .line 570289
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch -0x4169f1a6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
