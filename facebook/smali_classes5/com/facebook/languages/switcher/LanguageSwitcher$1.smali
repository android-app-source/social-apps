.class public final Lcom/facebook/languages/switcher/LanguageSwitcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0dz;


# direct methods
.method public constructor <init>(LX/0dz;)V
    .locals 0

    .prologue
    .line 569591
    iput-object p1, p0, Lcom/facebook/languages/switcher/LanguageSwitcher$1;->a:LX/0dz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 569592
    const/4 v1, 0x0

    .line 569593
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 569594
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitcher$1;->a:LX/0dz;

    iget-object v0, v0, LX/0dz;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569595
    :try_start_0
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitcher$1;->a:LX/0dz;

    iget-object v0, v0, LX/0dz;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v2, p0, Lcom/facebook/languages/switcher/LanguageSwitcher$1;->a:LX/0dz;

    iget-object v2, v2, LX/0dz;->k:LX/0e5;

    const/4 v4, 0x0

    const-class v5, LX/0dz;

    const-class v6, LX/0dz;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v0, v2, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;

    .line 569596
    if-eqz v0, :cond_0

    .line 569597
    iget-object v4, v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;->locales:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult$LocaleModel;

    .line 569598
    iget-object v0, v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult$LocaleModel;->locale:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 569599
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 569600
    :catch_0
    move-exception v0

    .line 569601
    const-class v2, LX/0dz;

    const-string v4, "Error fetching suggested locales"

    invoke-static {v2, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 569602
    :cond_0
    iget-object v0, p0, Lcom/facebook/languages/switcher/LanguageSwitcher$1;->a:LX/0dz;

    invoke-virtual {v0}, LX/0dz;->c()LX/0Py;

    move-result-object v2

    .line 569603
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 569604
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 569605
    invoke-static {v2, v0}, LX/0e8;->a(LX/0Py;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569606
    if-eqz v0, :cond_1

    .line 569607
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 569608
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 569609
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-le v2, v1, :cond_2

    .line 569610
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 569611
    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 569612
    :cond_4
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    .line 569613
    :cond_5
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
