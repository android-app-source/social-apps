.class public Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/2JL;


# instance fields
.field public p:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 569434
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 569413
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 569414
    const v0, 0x7f040013

    const v1, 0x7f040014

    invoke-virtual {p0, v0, v1}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->overridePendingTransition(II)V

    .line 569415
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 569422
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 569423
    invoke-static {p0, p0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 569424
    const v0, 0x7f0309b3

    invoke-virtual {p0, v0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->setContentView(I)V

    .line 569425
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 569426
    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 569427
    if-nez v1, :cond_0

    .line 569428
    new-instance v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-direct {v1}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;-><init>()V

    .line 569429
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 569430
    :cond_0
    if-eqz p1, :cond_1

    .line 569431
    const-string v0, "original_locale"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->q:Ljava/lang/String;

    .line 569432
    :goto_0
    return-void

    .line 569433
    :cond_1
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eC;->b:LX/0Tn;

    const-string v2, "device"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 569419
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 569420
    iget-object v0, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0eC;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 569421
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 569416
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 569417
    const-string v0, "original_locale"

    iget-object v1, p0, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569418
    return-void
.end method
