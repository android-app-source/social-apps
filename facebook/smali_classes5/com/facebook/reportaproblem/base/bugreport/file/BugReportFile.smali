.class public Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 803777
    new-instance v0, LX/2qu;

    invoke-direct {v0}, LX/2qu;-><init>()V

    sput-object v0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 803778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803779
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    .line 803780
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    .line 803781
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    .line 803782
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 803783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803784
    iput-object p1, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    .line 803785
    iput-object p2, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    .line 803786
    iput-object p3, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    .line 803787
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 803788
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 803789
    if-ne p0, p1, :cond_1

    .line 803790
    :cond_0
    :goto_0
    return v0

    .line 803791
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 803792
    :cond_3
    check-cast p1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    .line 803793
    iget-object v2, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 803794
    :cond_4
    iget-object v2, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    .line 803795
    :cond_5
    iget-object v2, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 803796
    iget-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 803797
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 803798
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 803799
    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 803800
    iget-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 803801
    iget-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 803802
    iget-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 803803
    return-void
.end method
