.class public final Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/3i7;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3i1;


# direct methods
.method public constructor <init>(LX/3i1;)V
    .locals 0

    .prologue
    .line 628443
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 628390
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 628391
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 628392
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 628393
    const v2, -0x1e53800c

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 628394
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 628395
    invoke-static {v3}, LX/3i1;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v4

    move v2, v4

    .line 628396
    if-nez v2, :cond_1

    move-object v0, v1

    .line 628397
    :cond_0
    :goto_0
    return-object v0

    .line 628398
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v4

    .line 628399
    const p3, -0x6e685d

    const/4 v5, 0x0

    .line 628400
    invoke-static {v0}, LX/1VO;->A(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 628401
    :goto_1
    move-object v2, v5

    .line 628402
    if-eqz v2, :cond_2

    move-object v0, v2

    .line 628403
    goto :goto_0

    .line 628404
    :cond_2
    const/4 v2, 0x0

    .line 628405
    invoke-static {v0}, LX/3i4;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v5

    .line 628406
    if-nez v5, :cond_c

    .line 628407
    :goto_2
    move-object v0, v2

    .line 628408
    if-nez v0, :cond_0

    .line 628409
    const/4 v5, 0x0

    .line 628410
    invoke-static {v3}, LX/2ms;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 628411
    invoke-static {v3}, LX/2ms;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v2

    .line 628412
    invoke-static {v3}, LX/2ms;->c(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v7

    .line 628413
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v6

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v6, p1, :cond_3

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 628414
    :cond_3
    :goto_3
    move-object v0, v5

    .line 628415
    if-nez v0, :cond_0

    .line 628416
    const/4 v2, 0x0

    const v6, -0x6e685d

    .line 628417
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOffer;->k()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_4
    move-object v0, v2

    .line 628418
    :goto_4
    move-object v0, v0

    .line 628419
    if-nez v0, :cond_0

    .line 628420
    invoke-static {v3}, LX/2ms;->c(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 628421
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v2, v2, LX/3i1;->b:LX/1nA;

    invoke-virtual {v2, p2, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 628422
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "default"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 628423
    :cond_5
    new-instance v0, LX/3i7;

    invoke-direct {v0, v4, v2, v1, v1}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 628424
    goto :goto_0

    .line 628425
    :cond_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v6

    .line 628426
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER_EXTENSIONS:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-virtual {v6, v2}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v2, v2, LX/3i1;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v7, 0x150

    const/4 p1, 0x0

    invoke-virtual {v2, v7, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 628427
    :cond_8
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v2, v2, LX/3i1;->g:LX/0wM;

    const v7, 0x7f020741

    invoke-virtual {v2, v7, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 628428
    invoke-static {v2, p3}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 628429
    :goto_5
    invoke-static {v3}, LX/2ms;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v7

    .line 628430
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 628431
    new-instance v6, LX/3i7;

    invoke-direct {v6, v4, v5, v5, v2}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    move-object v5, v6

    goto/16 :goto_1

    .line 628432
    :cond_9
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    invoke-virtual {v6, v5}, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v5, v5, LX/3i1;->e:LX/3i2;

    invoke-virtual {v5, p2}, LX/3i2;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/7gF;

    move-result-object v5

    .line 628433
    :goto_6
    new-instance v6, LX/3i7;

    invoke-direct {v6, v4, v5, v5, v2}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    move-object v5, v6

    goto/16 :goto_1

    .line 628434
    :cond_a
    iget-object v5, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v5, v5, LX/3i1;->f:LX/3i3;

    invoke-virtual {v5, p2, v3}, LX/3i3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/7gI;

    move-result-object v5

    goto :goto_6

    :cond_b
    move-object v2, v5

    goto :goto_5

    .line 628435
    :cond_c
    new-instance v6, LX/AEe;

    invoke-direct {v6, p0, v5}, LX/AEe;-><init>(Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V

    .line 628436
    new-instance v5, LX/3i7;

    invoke-direct {v5, v4, v6, v6, v2}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    move-object v2, v5

    goto/16 :goto_2

    .line 628437
    :cond_d
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v0, v0, LX/3i1;->e:LX/3i2;

    invoke-virtual {v0, p2}, LX/3i2;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/7gF;

    move-result-object v2

    .line 628438
    new-instance v6, LX/3i7;

    const-string v0, "messenger_deeplink"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    move-object v0, v2

    :goto_7
    invoke-direct {v6, v4, v2, v0, v5}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    move-object v5, v6

    goto/16 :goto_3

    :cond_e
    move-object v0, v5

    goto :goto_7

    .line 628439
    :cond_f
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v0, v0, LX/3i1;->g:LX/0wM;

    const v5, 0x7f020742

    invoke-virtual {v0, v5, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 628440
    invoke-static {v5, v6}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 628441
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/LinkOpenActionButton$LinkOpenActionButtonPartDefinition;->a:LX/3i1;

    iget-object v0, v0, LX/3i1;->b:LX/1nA;

    invoke-virtual {v0, p2, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 628442
    new-instance v0, LX/3i7;

    invoke-direct {v0, v4, v6, v2, v5}, LX/3i7;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x68f4097c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628366
    check-cast p2, LX/3i7;

    const/4 p0, 0x0

    .line 628367
    move-object v1, p4

    check-cast v1, LX/35p;

    invoke-interface {v1}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 628368
    if-nez p2, :cond_0

    .line 628369
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 628370
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x1bf1d0ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 628371
    :cond_0
    iget-object v2, p2, LX/3i7;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1

    .line 628372
    iget-object v2, p2, LX/3i7;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628373
    :cond_1
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v2

    .line 628374
    iget-object p4, v2, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, p4

    .line 628375
    invoke-virtual {v1, p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 628376
    iput-boolean p0, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 628377
    iget-object v1, p2, LX/3i7;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 628378
    const v1, 0x7f02079d

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 628379
    iget-object v1, p2, LX/3i7;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628380
    iget-object v1, p2, LX/3i7;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 628381
    iget-object v1, p2, LX/3i7;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 628382
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    .line 628383
    const/16 v1, 0x14

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    goto :goto_0

    .line 628384
    :cond_2
    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    .line 628385
    invoke-virtual {v2, p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 628386
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, p4

    .line 628387
    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 628388
    :goto_0
    return-void

    .line 628389
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
