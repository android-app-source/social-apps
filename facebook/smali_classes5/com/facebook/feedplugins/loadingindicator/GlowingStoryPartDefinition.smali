.class public Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TT;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590718
    new-instance v0, LX/3Wd;

    invoke-direct {v0}, LX/3Wd;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590693
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590694
    iput-object p1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590695
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;
    .locals 4

    .prologue
    .line 590707
    const-class v1, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;

    monitor-enter v1

    .line 590708
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590709
    sput-object v2, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590710
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590711
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590712
    new-instance p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 590713
    move-object v0, p0

    .line 590714
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590715
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590716
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590706
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590705
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 590703
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/C8w;->a:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590704
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x588fe81a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590700
    check-cast p4, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    .line 590701
    invoke-virtual {p4}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a()V

    .line 590702
    const/16 v1, 0x1f

    const v2, 0x6cff6d83

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 590699
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 590696
    check-cast p4, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    .line 590697
    invoke-virtual {p4}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b()V

    .line 590698
    return-void
.end method
