.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        "LX/JXI;",
        "TE;",
        "LX/JXP;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597070
    new-instance v0, LX/3Yk;

    invoke-direct {v0}, LX/3Yk;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597066
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597067
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    .line 597068
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 597069
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;
    .locals 5

    .prologue
    .line 597055
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    monitor-enter v1

    .line 597056
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597057
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597058
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597059
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597060
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;)V

    .line 597061
    move-object v0, p0

    .line 597062
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597063
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597064
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXP;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V
    .locals 12

    .prologue
    .line 597028
    const/4 v0, 0x0

    .line 597029
    iput-boolean v0, p2, LX/JXB;->e:Z

    .line 597030
    iget-object v0, p2, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v0, v0

    .line 597031
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v1

    .line 597032
    iget-object v2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m()Ljava/lang/String;

    move-result-object v3

    .line 597033
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 597034
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 597035
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v7

    .line 597036
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_1

    .line 597037
    iget-object v4, p1, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v4, v5}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->c(I)Z

    move-result v4

    move v4, v4

    .line 597038
    if-eqz v4, :cond_0

    .line 597039
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 597040
    :cond_0
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 597041
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 597042
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    .line 597043
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 597044
    new-instance v5, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object v6, v1

    move-object v7, v0

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/facebook/api/feed/SubmitResearchPollResponseParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V

    .line 597045
    const-string v6, "submitResearchPollResponseParamsKey"

    invoke-virtual {v11, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 597046
    iget-object v5, v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a:LX/0aG;

    const-string v6, "feed_submit_research_poll_response"

    sget-object v8, LX/1ME;->BY_EXCEPTION:LX/1ME;

    iget-object v9, v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v10, -0x55338775

    move-object v7, v11

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    .line 597047
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    const-string v2, "vote"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597048
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/JXP;->setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 597049
    iget-object v0, p2, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v0, v0

    .line 597050
    invoke-static {p2, p3, v0}, LX/JXK;->a(LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 597051
    if-eqz v0, :cond_2

    .line 597052
    invoke-virtual {p2}, LX/JXB;->d()V

    .line 597053
    invoke-static {p3, p2, v0, p1}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;LX/JXP;)V

    .line 597054
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXP;Ljava/lang/Integer;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V
    .locals 11

    .prologue
    .line 597012
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 597013
    iput-object v0, p3, LX/JXB;->f:Ljava/lang/Integer;

    .line 597014
    iget-object v0, p3, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v3, v0

    .line 597015
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    move v6, v0

    .line 597016
    :goto_0
    if-eqz v6, :cond_0

    .line 597017
    iget-object v0, p1, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v0, p2}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->a(Ljava/lang/Integer;)V

    .line 597018
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v4

    .line 597019
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-virtual {p3, v4}, LX/JXB;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v5

    .line 597020
    if-eqz v1, :cond_3

    const-string v7, "select_response"

    .line 597021
    :goto_1
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "research_poll_interaction"

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v9, "interaction_type"

    invoke-virtual {v8, v9, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "response_id"

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "answer_index"

    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "question_id"

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "survey_id"

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 597022
    iget-object v8, v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->c:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 597023
    if-eqz v6, :cond_1

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597024
    invoke-static {p0, p1, p3, p4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXP;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    .line 597025
    :cond_1
    return-void

    .line 597026
    :cond_2
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0

    .line 597027
    :cond_3
    const-string v7, "deselect_response"

    goto :goto_1
.end method

.method private static a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXP;)V
    .locals 9

    .prologue
    .line 597003
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 597004
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 597005
    new-instance v3, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;

    invoke-direct {v3, v1, v2}, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 597006
    const-string v4, "markResearchPollCompletedParamsKey"

    invoke-virtual {v5, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 597007
    iget-object v3, v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a:LX/0aG;

    const-string v4, "feed_mark_research_poll_completed"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    iget-object v7, v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v8, 0x220751d0

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    .line 597008
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v0

    .line 597009
    iget-object v1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    const-string v2, "completed_poll"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597010
    invoke-virtual {p2}, LX/JXP;->a()V

    .line 597011
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;LX/JXP;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 596900
    sget-object v0, LX/JXH;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->n()Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 596901
    :goto_0
    iput-object p2, p1, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 596902
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->o()Ljava/lang/String;

    move-result-object v0

    .line 596903
    iget-object v2, p3, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v2, v0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setQuestionText(Ljava/lang/String;)V

    .line 596904
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 596905
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 596906
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->p()Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollQuestionResponsesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;

    .line 596907
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->j()Ljava/lang/String;

    move-result-object p0

    .line 596908
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceResponse;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/JXB;->b(Ljava/lang/String;)Z

    move-result v0

    .line 596909
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 596910
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 596911
    :cond_0
    move-object v0, v3

    .line 596912
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 596913
    iget-object v2, p3, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    invoke-virtual {v2, v0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setAnswers(LX/0P1;)V

    .line 596914
    iget-object v2, p3, LX/JXP;->i:Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/feedplugins/researchpoll/views/ResearchPollQuestionView;->setVisibility(I)V

    .line 596915
    invoke-virtual {p1}, LX/JXB;->e()Z

    move-result v0

    invoke-virtual {p3, v0}, LX/JXP;->setVoteButtonEnabled(Z)V

    .line 596916
    return-void

    .line 596917
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->G()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_3
    invoke-virtual {p3, v0}, LX/JXP;->setVoteButtonActive(Z)V

    .line 596918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/JXP;->setQuestionHint(Ljava/lang/String;)V

    goto :goto_0

    .line 596919
    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    .line 596920
    :pswitch_1
    invoke-virtual {p3, v1}, LX/JXP;->setVoteButtonActive(Z)V

    .line 596921
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/JXP;->setQuestionHint(Ljava/lang/String;)V

    goto :goto_0

    .line 596922
    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXI;LX/JXP;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
            "LX/JXI;",
            "LX/JXP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 596955
    iget-object v0, p2, LX/JXI;->a:LX/JXB;

    .line 596956
    iget-object v1, v0, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v1, v1

    .line 596957
    iget-boolean v2, v0, LX/JXB;->g:Z

    move v2, v2

    .line 596958
    if-eqz v2, :cond_0

    .line 596959
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 596960
    iget-object v5, p3, LX/JXP;->b:Landroid/view/View;

    goto/16 :goto_6

    :goto_0
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 596961
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->n()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 596962
    invoke-virtual {p3}, LX/JXP;->a()V

    .line 596963
    :cond_0
    :goto_1
    iget-boolean v2, v0, LX/JXB;->e:Z

    move v2, v2

    .line 596964
    if-eqz v2, :cond_1

    .line 596965
    invoke-static {p0, p3, v0, p1}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXP;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    .line 596966
    :cond_1
    iget-object v2, v0, LX/JXB;->f:Ljava/lang/Integer;

    move-object v2, v2

    .line 596967
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 596968
    iget-object v2, v0, LX/JXB;->f:Ljava/lang/Integer;

    move-object v2, v2

    .line 596969
    invoke-static {p0, p3, v2, v0, p1}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXP;Ljava/lang/Integer;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    .line 596970
    :cond_2
    invoke-virtual {v0}, LX/JXB;->e()Z

    move-result v2

    invoke-virtual {p3, v2}, LX/JXP;->setVoteButtonEnabled(Z)V

    .line 596971
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->o()Ljava/lang/String;

    move-result-object v1

    .line 596972
    iget-object v2, p3, LX/JXP;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596973
    iget-object v1, p2, LX/JXI;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v1}, LX/JXP;->setVoteButtonListener(Landroid/view/View$OnClickListener;)V

    .line 596974
    iget-object v1, p2, LX/JXI;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v1}, LX/JXP;->setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 596975
    iget-object v1, p2, LX/JXI;->d:Ljava/lang/String;

    .line 596976
    iget-object v2, p3, LX/JXP;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596977
    iget-object v1, p2, LX/JXI;->f:Ljava/lang/String;

    .line 596978
    iget-object v2, p3, LX/JXP;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596979
    iget-object v1, p2, LX/JXI;->g:Ljava/lang/String;

    .line 596980
    iget-object v2, p3, LX/JXP;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596981
    iget-object v1, p2, LX/JXI;->e:Ljava/lang/String;

    .line 596982
    iget-object v2, p3, LX/JXP;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596983
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v1

    if-nez v1, :cond_3

    .line 596984
    iget-boolean v1, v0, LX/JXB;->h:Z

    move v0, v1

    .line 596985
    if-eqz v0, :cond_4

    .line 596986
    :cond_3
    invoke-static {p0, p1, p3}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXP;)V

    .line 596987
    :cond_4
    return-void

    .line 596988
    :cond_5
    invoke-virtual {p3, v9}, LX/JXP;->setVoteButtonActive(Z)V

    .line 596989
    iget-object v5, p3, LX/JXP;->e:Landroid/widget/TextView;

    if-eqz v9, :cond_8

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596990
    iget-object v5, p3, LX/JXP;->g:Landroid/view/View;

    if-eqz v8, :cond_9

    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 596991
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;->a()LX/0Px;

    move-result-object v4

    .line 596992
    :goto_4
    iget-object v5, v0, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v6, v5

    .line 596993
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v5

    .line 596994
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 596995
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v5

    .line 596996
    :cond_6
    :goto_5
    move-object v4, v5

    .line 596997
    invoke-static {p1, v4}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v4

    invoke-static {p1, v0, v4, p3}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;LX/JXP;)V

    goto/16 :goto_1

    .line 596998
    :cond_7
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 596999
    goto :goto_4

    .line 597000
    :goto_6
    const/16 v4, 0x8

    goto/16 :goto_0

    .line 597001
    :cond_8
    const/16 v4, 0x8

    goto :goto_2

    .line 597002
    :cond_9
    const/16 v4, 0x8

    goto :goto_3

    :cond_a
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v5

    goto :goto_5
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z
    .locals 1

    .prologue
    .line 596954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->o()Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurveyQuestionHistoryConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596953
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 596931
    check-cast p2, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    check-cast p3, LX/1Pq;

    .line 596932
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move-object v0, p3

    .line 596933
    check-cast v0, LX/1Pr;

    new-instance v1, LX/JXD;

    invoke-direct {v1, p2}, LX/JXD;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JXB;

    .line 596934
    const/4 v2, 0x1

    .line 596935
    iget-boolean v0, v1, LX/JXB;->c:Z

    move v0, v0

    .line 596936
    if-eqz v0, :cond_1

    .line 596937
    iget-object v0, v1, LX/JXB;->d:Ljava/lang/String;

    move-object v0, v0

    .line 596938
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 596939
    :goto_0
    if-eqz v0, :cond_2

    .line 596940
    :goto_1
    iget-object v0, v1, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-object v0, v0

    .line 596941
    if-nez v0, :cond_0

    .line 596942
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;->k()Ljava/lang/String;

    move-result-object v0

    .line 596943
    invoke-static {p2, v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object v0

    .line 596944
    iput-object v0, v1, LX/JXB;->b:Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    .line 596945
    :cond_0
    new-instance v2, LX/JXF;

    invoke-direct {v2, p0, v1, p2, p3}, LX/JXF;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V

    .line 596946
    new-instance v3, LX/JXG;

    invoke-direct {v3, p0, v1, p2, p3}, LX/JXG;-><init>(Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;LX/JXB;Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/1Pq;)V

    .line 596947
    new-instance v0, LX/JXI;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->I()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->B()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/JXI;-><init>(LX/JXB;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 596948
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 596949
    :cond_2
    iput-boolean v2, v1, LX/JXB;->c:Z

    .line 596950
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 596951
    iput-object v0, v1, LX/JXB;->d:Ljava/lang/String;

    .line 596952
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->b:Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "poll_loaded"

    invoke-virtual {v0, v2, v3}, Lcom/facebook/feedplugins/researchpoll/ResearchPollLoggerUtil;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x32382f30    # -4.1904384E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596930
    check-cast p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    check-cast p2, LX/JXI;

    check-cast p4, LX/JXP;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSurveyPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;LX/JXI;LX/JXP;)V

    const/16 v1, 0x1f

    const v2, -0x4f23e642

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596928
    const/4 v0, 0x1

    move v0, v0

    .line 596929
    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 596923
    check-cast p4, LX/JXP;

    const/4 v0, 0x0

    .line 596924
    invoke-virtual {p4, v0}, LX/JXP;->setCallToActionViewListener(Landroid/view/View$OnClickListener;)V

    .line 596925
    invoke-virtual {p4, v0}, LX/JXP;->setVoteButtonListener(Landroid/view/View$OnClickListener;)V

    .line 596926
    invoke-virtual {p4, v0}, LX/JXP;->setOnAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 596927
    return-void
.end method
