.class public Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/JXO;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597108
    new-instance v0, LX/3Yl;

    invoke-direct {v0}, LX/3Yl;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597073
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597074
    iput-object p1, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 597075
    iput-object p2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->c:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    .line 597076
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;
    .locals 5

    .prologue
    .line 597097
    const-class v1, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    monitor-enter v1

    .line 597098
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597099
    sput-object v2, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597100
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597101
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597102
    new-instance p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;)V

    .line 597103
    move-object v0, p0

    .line 597104
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597105
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597106
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597096
    sget-object v0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 597089
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597090
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597091
    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 597092
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;

    move-result-object v1

    .line 597093
    iget-object v0, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597094
    iget-object v2, p0, Lcom/facebook/feedplugins/researchpoll/ResearchPollSingleResultPartDefinition;->c:Lcom/facebook/feedplugins/researchpoll/ResultItemPartDefinition;

    new-instance v3, LX/JXM;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    invoke-static {v1}, LX/JXK;->a(LX/0Px;)I

    move-result v1

    invoke-direct {v3, v0, v1}, LX/JXM;-><init>(Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;I)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597095
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x398c19f4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597081
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/JXO;

    const/4 v5, 0x0

    .line 597082
    iget-object v4, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 597083
    check-cast v4, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 597084
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->n()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    .line 597085
    :goto_0
    if-eqz v4, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {p4, v4}, LX/JXO;->setVisibility(I)V

    .line 597086
    const/16 v1, 0x1f

    const v2, 0x321d59a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v4, v5

    .line 597087
    goto :goto_0

    .line 597088
    :cond_2
    const/16 v4, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 597077
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 597078
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597079
    check-cast v0, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    .line 597080
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-static {v0}, LX/JXK;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
