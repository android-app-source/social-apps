.class public Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597914
    new-instance v0, LX/3Z1;

    invoke-direct {v0}, LX/3Z1;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597915
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597916
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 597917
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;
    .locals 4

    .prologue
    .line 597918
    const-class v1, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;

    monitor-enter v1

    .line 597919
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597920
    sput-object v2, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597923
    new-instance p0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 597924
    move-object v0, p0

    .line 597925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597929
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 597930
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597931
    iget-object v0, p0, Lcom/facebook/feedplugins/storygallerysurvey/EmptyStoryGallerySurveyFeedUnitPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597932
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597933
    const/4 v0, 0x0

    return v0
.end method
