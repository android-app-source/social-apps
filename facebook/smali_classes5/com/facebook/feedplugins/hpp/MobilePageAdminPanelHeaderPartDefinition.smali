.class public Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
        ">;",
        "LX/JPI;",
        "LX/1Ps;",
        "LX/JPo;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/11T;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595664
    new-instance v0, LX/3YE;

    invoke-direct {v0}, LX/3YE;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11T;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595704
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595705
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595706
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->c:LX/11T;

    .line 595707
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->d:Landroid/content/res/Resources;

    .line 595708
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;
    .locals 6

    .prologue
    .line 595693
    const-class v1, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    monitor-enter v1

    .line 595694
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595695
    sput-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595696
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595697
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595698
    new-instance p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v4

    check-cast v4, LX/11T;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11T;Landroid/content/res/Resources;)V

    .line 595699
    move-object v0, p0

    .line 595700
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595701
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595702
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c()Ljava/lang/String;
    .locals 8

    .prologue
    .line 595683
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 595684
    :goto_0
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 595685
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 595686
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->d:Landroid/content/res/Resources;

    const v2, 0x7f083a80

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->c:LX/11T;

    .line 595687
    iget-object v6, v5, LX/11T;->h:Ljava/text/SimpleDateFormat;

    if-nez v6, :cond_1

    .line 595688
    invoke-virtual {v5}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/text/SimpleDateFormat;

    .line 595689
    const-string v7, "EEEE, MMMM d"

    iget-object p0, v5, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v6, v7, p0}, LX/11T;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V

    .line 595690
    iput-object v6, v5, LX/11T;->h:Ljava/text/SimpleDateFormat;

    .line 595691
    :cond_1
    iget-object v6, v5, LX/11T;->h:Ljava/text/SimpleDateFormat;

    move-object v5, v6

    .line 595692
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595682
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 595672
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595673
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595674
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 595675
    iget-object v1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595676
    new-instance v1, LX/JPI;

    .line 595677
    const/4 v2, 0x0

    .line 595678
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 595679
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 595680
    :cond_0
    move-object v0, v2

    .line 595681
    invoke-direct {p0}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/JPI;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4b30f0e5    # 1.1596005E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595666
    check-cast p2, LX/JPI;

    check-cast p4, LX/JPo;

    .line 595667
    iget-object v1, p2, LX/JPI;->a:Ljava/lang/String;

    .line 595668
    iget-object v2, p4, LX/JPo;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595669
    iget-object v1, p2, LX/JPI;->b:Ljava/lang/String;

    .line 595670
    iget-object v2, p4, LX/JPo;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595671
    const/16 v1, 0x1f

    const v2, 0xd92e287

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595665
    const/4 v0, 0x1

    return v0
.end method
