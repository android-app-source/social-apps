.class public Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
        "Landroid/text/SpannableStringBuilder;",
        "TE;",
        "LX/C7B;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/String;

.field private static h:LX/0Xm;


# instance fields
.field public final c:LX/03V;

.field public final d:LX/0bH;

.field private final e:Landroid/content/res/Resources;

.field public final f:LX/1e8;

.field public final g:LX/1e7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595017
    new-instance v0, LX/3Xy;

    invoke-direct {v0}, LX/3Xy;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->a:LX/1Cz;

    .line 595018
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0bH;Landroid/content/res/Resources;LX/1e8;LX/1e7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595019
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595020
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->c:LX/03V;

    .line 595021
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->d:LX/0bH;

    .line 595022
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->e:Landroid/content/res/Resources;

    .line 595023
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->f:LX/1e8;

    .line 595024
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->g:LX/1e7;

    .line 595025
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;
    .locals 9

    .prologue
    .line 595026
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    monitor-enter v1

    .line 595027
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595028
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595031
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1e8;->a(LX/0QB;)LX/1e8;

    move-result-object v7

    check-cast v7, LX/1e8;

    invoke-static {v0}, LX/1e7;->b(LX/0QB;)LX/1e7;

    move-result-object v8

    check-cast v8, LX/1e7;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;-><init>(LX/03V;LX/0bH;Landroid/content/res/Resources;LX/1e8;LX/1e7;)V

    .line 595032
    move-object v0, v3

    .line 595033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595037
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 595038
    check-cast p2, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    check-cast p3, LX/1Pq;

    const/4 v6, 0x0

    .line 595039
    move-object v0, p3

    check-cast v0, LX/1Pr;

    new-instance v1, LX/C7W;

    invoke-direct {v1, p2}, LX/C7W;-><init>(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7V;

    .line 595040
    iget-object v1, v0, LX/C7V;->a:Ljava/lang/String;

    move-object v1, v1

    .line 595041
    if-nez v1, :cond_0

    .line 595042
    const/4 v0, 0x0

    .line 595043
    :goto_0
    return-object v0

    .line 595044
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->e:Landroid/content/res/Resources;

    const v2, 0x7f081a25

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 595045
    iget-object v4, v0, LX/C7V;->d:Ljava/lang/String;

    move-object v4, v4

    .line 595046
    aput-object v4, v3, v6

    const/4 v4, 0x1

    .line 595047
    iget-object v5, v0, LX/C7V;->b:Ljava/lang/String;

    move-object v5, v5

    .line 595048
    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 595049
    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->e:Landroid/content/res/Resources;

    const v4, 0x7f080032

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 595050
    new-instance v3, LX/C7T;

    invoke-direct {v3, p0, p2, v0, p3}, LX/C7T;-><init>(Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C7V;LX/1Pq;)V

    .line 595051
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v4, 0x12

    invoke-virtual {v2, v3, v6, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 595052
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x33b6adc0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595053
    check-cast p2, Landroid/text/SpannableStringBuilder;

    check-cast p4, LX/C7B;

    .line 595054
    if-eqz p2, :cond_0

    .line 595055
    invoke-virtual {p4, p2}, LX/C7B;->setTitle(Ljava/lang/CharSequence;)V

    .line 595056
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x4be74786    # 3.0314252E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 595057
    check-cast p1, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 595058
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->f:LX/1e8;

    .line 595059
    iget-object v1, v0, LX/1e8;->a:Ljava/util/HashSet;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 595060
    return v0
.end method
