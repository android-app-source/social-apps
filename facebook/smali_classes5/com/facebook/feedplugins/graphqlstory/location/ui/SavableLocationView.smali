.class public Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 622171
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 622172
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a()V

    .line 622173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 622168
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 622169
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a()V

    .line 622170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 622160
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 622161
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a()V

    .line 622162
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 622165
    const v0, 0x7f03124a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 622166
    const v0, 0x7f0d2b06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    .line 622167
    return-void
.end method


# virtual methods
.method public getPlaceInfoView()Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;
    .locals 1

    .prologue
    .line 622164
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    return-object v0
.end method

.method public getProfilePictureView()Landroid/view/View;
    .locals 2

    .prologue
    .line 622163
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/location/ui/SavableLocationView;->a:Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;

    const v1, 0x7f0d116d

    invoke-virtual {v0, v1}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
