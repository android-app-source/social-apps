.class public Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/C57;",
        "TE;",
        "LX/AnD;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static l:LX/0Xm;


# instance fields
.field public final b:LX/0hB;

.field public final c:LX/1LV;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnG;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/03V;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Zb;

.field private final h:LX/17Q;

.field public final i:LX/17S;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/3iX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588349
    new-instance v0, LX/3Vj;

    invoke-direct {v0}, LX/3Vj;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1LV;LX/0Ot;LX/03V;LX/0Zb;LX/17Q;LX/0Ot;LX/0hB;LX/17S;LX/0Ot;LX/3iX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1LV;",
            "LX/0Ot",
            "<",
            "LX/AnG;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;",
            "LX/0hB;",
            "LX/17S;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/3iX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588350
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588351
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->c:LX/1LV;

    .line 588352
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->d:LX/0Ot;

    .line 588353
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->e:LX/03V;

    .line 588354
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->f:LX/0Ot;

    .line 588355
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->g:LX/0Zb;

    .line 588356
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->h:LX/17Q;

    .line 588357
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->b:LX/0hB;

    .line 588358
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    .line 588359
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->j:LX/0Ot;

    .line 588360
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->k:LX/3iX;

    .line 588361
    return-void
.end method

.method public static synthetic a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Landroid/view/View;)LX/AnD;
    .locals 2

    .prologue
    .line 588362
    const/4 v1, 0x0

    .line 588363
    move-object v0, p1

    :goto_0
    if-eqz v0, :cond_2

    .line 588364
    instance-of p0, v0, LX/AnD;

    if-eqz p0, :cond_0

    .line 588365
    check-cast v0, LX/AnD;

    .line 588366
    :goto_1
    move-object v0, v0

    .line 588367
    return-object v0

    .line 588368
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 588369
    instance-of p0, v0, Landroid/view/View;

    if-nez p0, :cond_1

    move-object v0, v1

    .line 588370
    goto :goto_1

    .line 588371
    :cond_1
    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 588372
    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;
    .locals 14

    .prologue
    .line 588380
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    monitor-enter v1

    .line 588381
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588382
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588383
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588384
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588385
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v4

    check-cast v4, LX/1LV;

    const/16 v5, 0x1d62

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    const/16 v9, 0x12bd

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v11

    check-cast v11, LX/17S;

    const/16 v12, 0x474

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/3iX;->a(LX/0QB;)LX/3iX;

    move-result-object v13

    check-cast v13, LX/3iX;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;-><init>(LX/1LV;LX/0Ot;LX/03V;LX/0Zb;LX/17Q;LX/0Ot;LX/0hB;LX/17S;LX/0Ot;LX/3iX;)V

    .line 588386
    move-object v0, v3

    .line 588387
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588388
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588389
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 588373
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v2}, LX/17S;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 588374
    invoke-static {p1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v2}, LX/17S;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 588375
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 588376
    goto :goto_0

    .line 588377
    :cond_1
    invoke-static {p1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v2}, LX/17S;->a()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v2}, LX/17S;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(LX/AnD;LX/35K;)V
    .locals 2

    .prologue
    .line 588285
    const/16 v1, 0x8

    .line 588286
    iget-object v0, p0, LX/AnD;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 588287
    iget-object v0, p0, LX/AnD;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 588288
    const/4 v0, 0x0

    .line 588289
    iput-boolean v0, p1, LX/35K;->a:Z

    .line 588290
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;LX/AnD;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "TE;",
            "LX/AnD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 588378
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition$6;

    invoke-direct {v0, p0, p2, p1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition$6;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {p3, v0}, LX/AnD;->post(Ljava/lang/Runnable;)Z

    .line 588379
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/C57;LX/1Pq;LX/AnD;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/C57;",
            "TE;",
            "LX/AnD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 588291
    invoke-static {p1}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 588292
    invoke-static {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 588293
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnG;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AnG;->a(Ljava/lang/Class;)LX/AnF;

    move-result-object v2

    .line 588294
    if-nez v2, :cond_1

    .line 588295
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->e:LX/03V;

    const-string v1, "HScrollChainingFeedUnitSection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No controller for HScrollChainingFeedUnitSection for class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 588296
    :cond_0
    :goto_0
    return-void

    .line 588297
    :cond_1
    invoke-virtual {v2, p3}, LX/AnF;->a(LX/1PW;)V

    .line 588298
    iput-object v2, p4, LX/AnD;->h:LX/AnF;

    .line 588299
    new-instance v0, LX/AnE;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ja;

    move-object v3, p4

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/AnE;-><init>(LX/0ja;LX/AnF;LX/AnC;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 588300
    invoke-virtual {v2, v6, p4}, LX/AnF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/AnD;)V

    .line 588301
    instance-of v1, v5, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v1, :cond_4

    .line 588302
    iget-object v1, p2, LX/C57;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/AnD;->setMenuButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 588303
    :goto_1
    iget-object v1, p2, LX/C57;->d:LX/3sJ;

    invoke-virtual {p4, v1}, LX/AnD;->setOnPageChangeListener(LX/0hc;)V

    .line 588304
    iget-object v1, p2, LX/C57;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/AnD;->setActionInstagramButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 588305
    iget-object v1, p4, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 588306
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v1}, LX/17S;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 588307
    iget-object v1, p4, LX/AnD;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p4}, LX/AnD;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0810e5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 588308
    :cond_2
    invoke-interface {v5}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v1

    .line 588309
    iget-object v3, p4, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    move-object v3, v3

    .line 588310
    invoke-interface {v5}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, LX/2sw;->a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V

    .line 588311
    invoke-virtual {p4}, LX/AnD;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/2sw;->a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V

    .line 588312
    invoke-virtual {v0, v1}, LX/99W;->a(Ljava/util/List;)V

    .line 588313
    invoke-virtual {v2}, LX/2sw;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 588314
    invoke-interface {v5}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 588315
    iget-object v0, p2, LX/C57;->e:LX/35K;

    .line 588316
    iget-object v1, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    move-object v0, v1

    .line 588317
    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_ANIMATE:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    if-ne v0, v1, :cond_5

    .line 588318
    iget-object v0, p2, LX/C57;->f:LX/99r;

    iget-object v1, p2, LX/C57;->e:LX/35K;

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 588319
    invoke-static {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 588320
    iget-object v7, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->i:LX/17S;

    invoke-virtual {v7, v0}, LX/17S;->b(LX/99r;)V

    .line 588321
    iput-boolean v10, v1, LX/35K;->a:Z

    .line 588322
    invoke-virtual {p4}, LX/AnD;->b()V

    .line 588323
    :cond_3
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {p4, v7, v8}, LX/AnD;->measure(II)V

    .line 588324
    invoke-virtual {p4}, LX/AnD;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iput v10, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 588325
    invoke-static {p0, p1, p4, v1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a$redex0(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V

    .line 588326
    new-instance v7, Landroid/view/animation/TranslateAnimation;

    iget-object v8, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->b:LX/0hB;

    invoke-virtual {v8}, LX/0hB;->c()I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v7, v8, v9, v9, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 588327
    const-wide/16 v9, 0x15e

    invoke-virtual {v7, v9, v10}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 588328
    iget-object v8, p4, LX/AnD;->a:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v8, v7}, Lcom/facebook/widget/CustomViewPager;->startAnimation(Landroid/view/animation/Animation;)V

    .line 588329
    iget-object v0, p2, LX/C57;->e:LX/35K;

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->FULL:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 588330
    iput-object v1, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 588331
    invoke-static {p0, p1, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;LX/AnD;)V

    goto/16 :goto_0

    .line 588332
    :cond_4
    iget-object v1, p2, LX/C57;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/AnD;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 588333
    :cond_5
    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->FULL:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    if-ne v0, v1, :cond_6

    .line 588334
    iget-object v0, p2, LX/C57;->e:LX/35K;

    invoke-static {p0, p1, p4, v0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a$redex0(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V

    goto/16 :goto_0

    .line 588335
    :cond_6
    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->START_HIDING:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    if-ne v0, v1, :cond_0

    .line 588336
    iget-object v0, p2, LX/C57;->e:LX/35K;

    invoke-static {p0, p1, p4, v0}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->c(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V

    .line 588337
    iget-object v0, p2, LX/C57;->e:LX/35K;

    sget-object v1, Lcom/facebook/graphql/enums/ChainingSectionViewState;->EMPTY:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 588338
    iput-object v1, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    .line 588339
    invoke-static {p0, p1, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;LX/AnD;)V

    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V
    .locals 3

    .prologue
    .line 588340
    invoke-static {p1}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 588341
    invoke-virtual {p2}, LX/AnD;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, -0x2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 588342
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->c:LX/1LV;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 588343
    invoke-static {p0, p1}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 588344
    invoke-static {p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(LX/AnD;LX/35K;)V

    .line 588345
    :goto_0
    return-void

    .line 588346
    :cond_0
    const/4 v0, 0x1

    .line 588347
    iput-boolean v0, p3, LX/35K;->a:Z

    .line 588348
    invoke-virtual {p2}, LX/AnD;->b()V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/AnD;LX/35K;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 588251
    invoke-virtual {p2}, LX/AnD;->getMeasuredHeight()I

    move-result v0

    .line 588252
    new-instance v1, LX/AhY;

    invoke-direct {v1, p2, v0, v4}, LX/AhY;-><init>(Landroid/view/View;II)V

    .line 588253
    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, LX/AhY;->setDuration(J)V

    .line 588254
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 588255
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 588256
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 588257
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 588258
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 588259
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 588260
    new-instance v0, LX/C56;

    invoke-direct {v0, p0, p2, p3, p1}, LX/C56;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;LX/AnD;LX/35K;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 588261
    invoke-virtual {p2, v2}, LX/AnD;->startAnimation(Landroid/view/animation/Animation;)V

    .line 588262
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588263
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 588264
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p3, LX/1Pq;

    .line 588265
    new-instance v4, LX/C52;

    invoke-direct {v4, p0, p2}, LX/C52;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 588266
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->k:LX/3iX;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iX;->b(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-result-object v0

    invoke-static {v0}, LX/99r;->getEntryPoint(Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)LX/99r;

    move-result-object v6

    .line 588267
    new-instance v1, LX/C53;

    invoke-direct {v1, p0, v6, p2}, LX/C53;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, p3

    .line 588268
    check-cast v0, LX/1Pr;

    new-instance v2, LX/35J;

    invoke-direct {v2, p2}, LX/35J;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/35K;

    .line 588269
    new-instance v2, LX/C54;

    invoke-direct {v2, p0, p2, v5}, LX/C54;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/35K;)V

    .line 588270
    new-instance v3, LX/C55;

    invoke-direct {v3, p0, p2, p3}, LX/C55;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pq;)V

    .line 588271
    new-instance v0, LX/C57;

    invoke-direct/range {v0 .. v6}, LX/C57;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/3sJ;LX/35K;LX/99r;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x249a65ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588272
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/C57;

    check-cast p3, LX/1Pq;

    check-cast p4, LX/AnD;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/C57;LX/1Pq;LX/AnD;)V

    const/16 v1, 0x1f

    const v2, -0x441f7c01

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588273
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 588274
    sget-object v0, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 588275
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p4, LX/AnD;

    const/4 v2, 0x0

    .line 588276
    invoke-virtual {p4, v2}, LX/AnD;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 588277
    invoke-virtual {p4, v2}, LX/AnD;->setMenuButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 588278
    invoke-virtual {p4, v2}, LX/AnD;->setOnPageChangeListener(LX/0hc;)V

    .line 588279
    invoke-virtual {p4, v2}, LX/AnD;->setActionInstagramButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 588280
    invoke-static {p1}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 588281
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AnG;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AnG;->a(Ljava/lang/Class;)LX/AnF;

    move-result-object v0

    .line 588282
    if-eqz v0, :cond_0

    .line 588283
    invoke-virtual {v0, v2}, LX/AnF;->a(LX/1PW;)V

    .line 588284
    :cond_0
    return-void
.end method
