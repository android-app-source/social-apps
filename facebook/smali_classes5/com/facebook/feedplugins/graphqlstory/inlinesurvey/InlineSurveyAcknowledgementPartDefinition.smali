.class public Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C6F;",
        "Ljava/lang/Void;",
        "LX/1Pr;",
        "LX/C6I;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field private final c:LX/1Rg;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590483
    new-instance v0, LX/3Wa;

    invoke-direct {v0}, LX/3Wa;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590478
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590479
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->b:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 590480
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->c:LX/1Rg;

    .line 590481
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->d:Landroid/content/res/Resources;

    .line 590482
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;
    .locals 6

    .prologue
    .line 590467
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;

    monitor-enter v1

    .line 590468
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590469
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590470
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590471
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590472
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    invoke-static {v0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v4

    check-cast v4, LX/1Rg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;-><init>(Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V

    .line 590473
    move-object v0, p0

    .line 590474
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590475
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590476
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C6I;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590445
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 590460
    check-cast p2, LX/C6F;

    const/4 v5, 0x0

    .line 590461
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b10ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 590462
    iget-object v0, p2, LX/C6F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590463
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v1

    .line 590464
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 590465
    iget-object v7, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->b:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v0, LX/82M;

    new-instance v1, LX/C6C;

    iget-object v2, p2, LX/C6F;->b:LX/C6H;

    iget-object v4, p2, LX/C6F;->b:LX/C6H;

    invoke-static {v3, v4}, LX/C6T;->a(Lcom/facebook/graphql/model/FeedUnit;LX/C6G;)LX/3Ii;

    move-result-object v4

    invoke-direct {v1, v2, v4}, LX/C6C;-><init>(LX/C6H;LX/3Ii;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/C6D;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/C6D;

    iget-object v8, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->c:LX/1Rg;

    invoke-direct {v4, v8, v6}, LX/C6D;-><init>(LX/1Rg;I)V

    invoke-direct/range {v0 .. v5}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590466
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x35fefe21

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590453
    check-cast p1, LX/C6F;

    check-cast p4, LX/C6I;

    .line 590454
    invoke-virtual {p4}, LX/C6I;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 590455
    invoke-virtual {p4}, LX/C6I;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v1, p1, LX/C6F;->b:LX/C6H;

    .line 590456
    iget-boolean p1, v1, LX/C6H;->b:Z

    move v1, p1

    .line 590457
    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 590458
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x145818ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 590459
    :cond_1
    const/4 v1, -0x2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590449
    check-cast p1, LX/C6F;

    .line 590450
    iget-object v0, p1, LX/C6F;->b:LX/C6H;

    .line 590451
    iget-boolean p0, v0, LX/C6H;->a:Z

    move v0, p0

    .line 590452
    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 590446
    check-cast p1, LX/C6F;

    .line 590447
    iget-object v0, p1, LX/C6F;->b:LX/C6H;

    invoke-virtual {v0}, LX/C6H;->d()V

    .line 590448
    return-void
.end method
