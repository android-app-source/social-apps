.class public Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C6N;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1Cz;

.field private static m:LX/0Xm;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final g:LX/C6g;

.field private final h:LX/0SG;

.field private final i:LX/03V;

.field private final j:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field private final k:LX/1Rg;

.field private final l:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590641
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->a:Ljava/lang/String;

    .line 590642
    new-instance v0, LX/3Wc;

    invoke-direct {v0}, LX/3Wc;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;LX/C6g;LX/0SG;LX/03V;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590629
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590630
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->c:Landroid/content/Context;

    .line 590631
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 590632
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    .line 590633
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 590634
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->g:LX/C6g;

    .line 590635
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->h:LX/0SG;

    .line 590636
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->i:LX/03V;

    .line 590637
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->j:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 590638
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->k:LX/1Rg;

    .line 590639
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->l:Landroid/content/res/Resources;

    .line 590640
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;
    .locals 14

    .prologue
    .line 590643
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;

    monitor-enter v1

    .line 590644
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590645
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590646
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590647
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590648
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, LX/C6g;->b(LX/0QB;)LX/C6g;

    move-result-object v8

    check-cast v8, LX/C6g;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    invoke-static {v0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v12

    check-cast v12, LX/1Rg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;LX/C6g;LX/0SG;LX/03V;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V

    .line 590649
    move-object v0, v3

    .line 590650
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590651
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590652
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590653
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590628
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 590614
    check-cast p2, LX/C6N;

    check-cast p3, LX/1Pq;

    const/4 v5, 0x0

    .line 590615
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->l:Landroid/content/res/Resources;

    const v1, 0x7f0b10bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 590616
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->g:LX/C6g;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->h:LX/0SG;

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->i:LX/03V;

    invoke-static {p2, p3, v1, v2, v3}, LX/C6T;->a(LX/C6N;LX/1Pq;LX/C6g;LX/0SG;LX/03V;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590617
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    const v2, 0x7f081a54

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590618
    iget-object v0, p2, LX/C6N;->b:LX/C6a;

    .line 590619
    iget-boolean v1, v0, LX/C6a;->d:Z

    move v0, v1

    .line 590620
    if-eqz v0, :cond_0

    const v0, 0x7f0e073c

    .line 590621
    :goto_0
    const v1, 0x7f0d175f

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590622
    iget-object v0, p2, LX/C6N;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590623
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v1

    .line 590624
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 590625
    iget-object v7, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->j:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v0, LX/82M;

    new-instance v1, LX/C6L;

    iget-object v2, p2, LX/C6N;->b:LX/C6a;

    invoke-direct {v1, v2}, LX/C6L;-><init>(LX/C6a;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/C6M;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/C6M;

    iget-object v8, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->k:LX/1Rg;

    invoke-direct {v4, v8, v6}, LX/C6M;-><init>(LX/1Rg;I)V

    invoke-direct/range {v0 .. v5}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590626
    return-object v5

    .line 590627
    :cond_0
    const v0, 0x7f0e073b

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590606
    check-cast p1, LX/C6N;

    .line 590607
    iget-object v0, p1, LX/C6N;->c:LX/C6H;

    .line 590608
    iget-boolean p0, v0, LX/C6H;->a:Z

    move v0, p0

    .line 590609
    if-nez v0, :cond_0

    iget-object v0, p1, LX/C6N;->b:LX/C6a;

    .line 590610
    iget-boolean p0, v0, LX/C6a;->c:Z

    move v0, p0

    .line 590611
    if-eqz v0, :cond_0

    iget-object v0, p1, LX/C6N;->b:LX/C6a;

    .line 590612
    iget-boolean p0, v0, LX/C6a;->k:Z

    move v0, p0

    .line 590613
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
