.class public Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C6Y;",
        "LX/C6Z;",
        "TE;",
        "LX/C6b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/17Q;

.field private final d:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

.field private final e:LX/1Rg;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590603
    new-instance v0, LX/3Wb;

    invoke-direct {v0}, LX/3Wb;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/17Q;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590596
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590597
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->b:LX/0Zb;

    .line 590598
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->c:LX/17Q;

    .line 590599
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->d:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    .line 590600
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->e:LX/1Rg;

    .line 590601
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->f:Landroid/content/res/Resources;

    .line 590602
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;
    .locals 9

    .prologue
    .line 590585
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;

    monitor-enter v1

    .line 590586
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590587
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590588
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590589
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590590
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-static {v0}, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    invoke-static {v0}, LX/1Rg;->a(LX/0QB;)LX/1Rg;

    move-result-object v7

    check-cast v7, LX/1Rg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;-><init>(LX/0Zb;LX/17Q;Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;LX/1Rg;Landroid/content/res/Resources;)V

    .line 590591
    move-object v0, v3

    .line 590592
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590593
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590594
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C6b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590486
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 590569
    check-cast p2, LX/C6Y;

    check-cast p3, LX/1Pq;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 590570
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b10b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 590571
    iget-object v0, p2, LX/C6Y;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590572
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v1

    .line 590573
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 590574
    iget-object v7, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->d:Lcom/facebook/feed/rows/animations/AnimationsPartDefinition;

    new-instance v0, LX/82M;

    new-instance v1, LX/C6V;

    iget-object v8, p2, LX/C6Y;->g:LX/C6a;

    iget-boolean v2, p2, LX/C6Y;->a:Z

    if-nez v2, :cond_2

    move v2, v4

    :goto_0
    iget-object v9, p2, LX/C6Y;->h:LX/C6H;

    .line 590575
    iget-boolean v10, v9, LX/C6H;->a:Z

    move v9, v10

    .line 590576
    if-nez v9, :cond_0

    iget-object v9, p2, LX/C6Y;->g:LX/C6a;

    .line 590577
    iget-boolean v10, v9, LX/C6a;->k:Z

    move v9, v10

    .line 590578
    if-eqz v9, :cond_1

    :cond_0
    move v5, v4

    :cond_1
    iget-object v4, p2, LX/C6Y;->g:LX/C6a;

    invoke-static {v3, v4}, LX/C6T;->a(Lcom/facebook/graphql/model/FeedUnit;LX/C6G;)LX/3Ii;

    move-result-object v4

    invoke-direct {v1, v8, v2, v5, v4}, LX/C6V;-><init>(LX/C6a;ZZLX/3Ii;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/C6W;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/C6W;

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->e:LX/1Rg;

    invoke-direct {v4, v5, v6}, LX/C6W;-><init>(LX/1Rg;I)V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590579
    new-instance v0, LX/C6Z;

    .line 590580
    new-instance v1, LX/C6O;

    invoke-direct {v1, p2, p3}, LX/C6O;-><init>(LX/C6Y;LX/1Pq;)V

    move-object v1, v1

    .line 590581
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->c:LX/17Q;

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->b:LX/0Zb;

    .line 590582
    new-instance v4, LX/C6P;

    invoke-direct {v4, p2, p3, v2, v3}, LX/C6P;-><init>(LX/C6Y;LX/1Pq;LX/17Q;LX/0Zb;)V

    move-object v2, v4

    .line 590583
    iget-object v3, p2, LX/C6Y;->g:LX/C6a;

    invoke-direct {v0, v1, v2, v3}, LX/C6Z;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/C6a;)V

    return-object v0

    :cond_2
    move v2, v5

    .line 590584
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x52940bb4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590495
    check-cast p1, LX/C6Y;

    check-cast p2, LX/C6Z;

    check-cast p4, LX/C6b;

    .line 590496
    iget-object v1, p1, LX/C6Y;->e:Ljava/lang/String;

    .line 590497
    iget-object v2, p4, LX/C6b;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590498
    iget-object v1, p1, LX/C6Y;->c:Ljava/lang/String;

    iget-object v2, p1, LX/C6Y;->f:LX/0Px;

    const/4 v5, 0x0

    .line 590499
    iget-object v4, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v4

    if-gtz v4, :cond_0

    if-nez v1, :cond_7

    .line 590500
    :cond_0
    :goto_0
    iget-object v1, p2, LX/C6Z;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/C6b;->setAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 590501
    iget-boolean v1, p1, LX/C6Y;->a:Z

    iget-object v2, p1, LX/C6Y;->g:LX/C6a;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 590502
    iget-object v4, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 590503
    iget-object v4, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v9

    move v8, v7

    .line 590504
    :goto_1
    if-ge v8, v9, :cond_4

    .line 590505
    iget-object v4, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v8}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbRadioButton;

    .line 590506
    if-eqz v1, :cond_2

    .line 590507
    iget v5, v2, LX/C6a;->e:I

    move v5, v5

    .line 590508
    if-ne v5, v8, :cond_1

    move v5, v6

    :goto_2
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 590509
    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    :cond_1
    move v5, v7

    .line 590510
    goto :goto_2

    .line 590511
    :cond_2
    iget v5, v2, LX/C6a;->f:I

    move v5, v5

    .line 590512
    if-ne v5, v8, :cond_3

    move v5, v6

    :goto_4
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    goto :goto_3

    :cond_3
    move v5, v7

    goto :goto_4

    .line 590513
    :cond_4
    iget-boolean v1, p1, LX/C6Y;->a:Z

    iget-object v2, p1, LX/C6Y;->d:Ljava/lang/String;

    iget-object v4, p2, LX/C6Z;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2, v4}, LX/C6b;->a(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 590514
    invoke-virtual {p4}, LX/C6b;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 590515
    invoke-virtual {p4}, LX/C6b;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v1, p1, LX/C6Y;->g:LX/C6a;

    .line 590516
    iget-boolean v4, v1, LX/C6a;->i:Z

    move v1, v4

    .line 590517
    if-eqz v1, :cond_6

    const/4 v1, 0x0

    :goto_5
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 590518
    :cond_5
    iget-object v1, p1, LX/C6Y;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C6Z;->c:LX/C6a;

    iget-object v4, p2, LX/C6Z;->c:LX/C6a;

    .line 590519
    iget-object v5, v4, LX/C6a;->j:Ljava/lang/String;

    move-object v4, v5

    .line 590520
    iget-boolean v5, v2, LX/C6a;->l:Z

    move v5, v5

    .line 590521
    if-eqz v5, :cond_f

    .line 590522
    :goto_6
    const/16 v1, 0x1f

    const v2, 0x5a57666f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 590523
    :cond_6
    const/4 v1, -0x2

    goto :goto_5

    .line 590524
    :cond_7
    const-string v4, "three_options"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    .line 590525
    if-eqz v6, :cond_8

    .line 590526
    const/4 v4, 0x3

    iput v4, p4, LX/C6b;->q:I

    :cond_8
    move v4, v5

    .line 590527
    :goto_7
    iget v7, p4, LX/C6b;->q:I

    if-ge v4, v7, :cond_a

    .line 590528
    new-instance v7, Lcom/facebook/resources/ui/FbRadioButton;

    iget-object v8, p4, LX/C6b;->e:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    .line 590529
    invoke-static {v1}, LX/C6b;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 590530
    add-int/lit8 v8, v4, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 590531
    :cond_9
    invoke-virtual {v7, v4}, Lcom/facebook/resources/ui/FbRadioButton;->setId(I)V

    .line 590532
    const/4 p3, 0x0

    .line 590533
    new-instance v8, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 590534
    invoke-virtual {p4}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0209a0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, p3, v8, p3, p3}, Lcom/facebook/resources/ui/FbRadioButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 590535
    iget v8, p4, LX/C6b;->r:I

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setCompoundDrawablePadding(I)V

    .line 590536
    iget v8, p4, LX/C6b;->t:I

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setTextSize(F)V

    .line 590537
    iget v8, p4, LX/C6b;->s:I

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setTextColor(I)V

    .line 590538
    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setGravity(I)V

    .line 590539
    iget-object v8, p4, LX/C6b;->u:Landroid/widget/RadioGroup$LayoutParams;

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbRadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 590540
    iget-object v8, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v8, v7}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 590541
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 590542
    :cond_a
    invoke-static {v1}, LX/C6b;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 590543
    iget-object v4, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {p4}, LX/C6b;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b10b1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {v4, v5, v5, v5, v7}, Landroid/widget/RadioGroup;->setPadding(IIII)V

    .line 590544
    :cond_b
    const-string v4, "only_num"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 590545
    iget-object v4, p4, LX/C6b;->c:Landroid/widget/FrameLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 590546
    :cond_c
    if-eqz v6, :cond_d

    iget v4, p4, LX/C6b;->n:I

    move v5, v4

    :goto_8
    if-eqz v6, :cond_e

    iget v4, p4, LX/C6b;->o:I

    :goto_9
    const/4 v9, 0x0

    .line 590547
    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 590548
    iget-object v7, p4, LX/C6b;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590549
    iget-object v7, p4, LX/C6b;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v8, p4, LX/C6b;->a:Landroid/graphics/Paint;

    invoke-virtual {v8, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    invoke-static {v8, v5, v4}, LX/C6b;->a(FII)I

    move-result v8

    invoke-static {v7, v8, v9, v9, v9}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 590550
    iget-object v7, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v7, v9}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590551
    const/4 v6, 0x4

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 590552
    iget-object v7, p4, LX/C6b;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590553
    iget-object v7, p4, LX/C6b;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v8, p4, LX/C6b;->a:Landroid/graphics/Paint;

    invoke-virtual {v8, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    invoke-static {v8, v5, v4}, LX/C6b;->a(FII)I

    move-result v8

    invoke-static {v7, v9, v9, v8, v9}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 590554
    iget-object v7, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    iget v8, p4, LX/C6b;->q:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590555
    const/4 v6, 0x2

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 590556
    iget-object v7, p4, LX/C6b;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590557
    iget-object v7, p4, LX/C6b;->b:Landroid/widget/RadioGroup;

    iget v8, p4, LX/C6b;->q:I

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 590558
    goto/16 :goto_0

    :cond_d
    iget v4, p4, LX/C6b;->l:I

    move v5, v4

    goto :goto_8

    :cond_e
    iget v4, p4, LX/C6b;->m:I

    goto :goto_9

    .line 590559
    :cond_f
    invoke-static {v1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    .line 590560
    invoke-static {v5}, LX/17Q;->F(LX/0lF;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 590561
    const/4 v6, 0x0

    .line 590562
    :goto_a
    move-object v5, v6

    .line 590563
    iget-object v6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 590564
    const/4 v5, 0x1

    iput-boolean v5, v2, LX/C6a;->l:Z

    .line 590565
    goto/16 :goto_6

    :cond_10
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "inline_seen"

    invoke-direct {v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    invoke-virtual {v6, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v1, "followup_question"

    invoke-virtual {v6, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v1, "native_newsfeed"

    .line 590566
    iput-object v1, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 590567
    move-object v6, v6

    .line 590568
    goto :goto_a
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590491
    check-cast p1, LX/C6Y;

    .line 590492
    iget-object v0, p1, LX/C6Y;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p1, LX/C6Y;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p1, LX/C6Y;->g:LX/C6a;

    .line 590493
    iget-boolean p0, v0, LX/C6a;->b:Z

    move v0, p0

    .line 590494
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 590487
    check-cast p4, LX/C6b;

    const/4 v1, 0x0

    .line 590488
    invoke-virtual {p4, v1}, LX/C6b;->setAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 590489
    const/4 v0, 0x0

    invoke-virtual {p4, v0, v1, v1}, LX/C6b;->a(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 590490
    return-void
.end method
