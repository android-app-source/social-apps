.class public Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Integer;",
        "TE;",
        "LX/C64;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

.field private final d:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587686
    new-instance v0, LX/3VI;

    invoke-direct {v0}, LX/3VI;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;Lcom/facebook/feedplugins/base/TextLinkPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587705
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587706
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    .line 587707
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    .line 587708
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->d:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 587709
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587704
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 587693
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 587694
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587695
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587696
    invoke-static {v0}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v2

    .line 587697
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->k()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v1

    if-gtz v1, :cond_0

    if-lez v2, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 587698
    :goto_0
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    new-instance v4, LX/Anj;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v0, v5, v6, v1}, LX/Anj;-><init>(LX/0jW;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Z)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587699
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->k()I

    move-result v1

    if-gtz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v0

    if-lez v0, :cond_2

    .line 587700
    :cond_1
    const v0, 0x7f0d0704

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->c:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 587701
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->d:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587702
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 587703
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2aa9e2ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587690
    check-cast p2, Ljava/lang/Integer;

    check-cast p4, LX/C64;

    .line 587691
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, LX/C64;->setPlayCountText(I)V

    .line 587692
    const/16 v1, 0x1f

    const v2, -0x4d635fd9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587687
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 587688
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 587689
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
