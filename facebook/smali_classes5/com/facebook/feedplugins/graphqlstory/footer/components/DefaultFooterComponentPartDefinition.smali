.class public Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static l:LX/0Xm;


# instance fields
.field private final e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/1We;

.field private final h:LX/1Wm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Wm",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final i:LX/Anw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Anw",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/36F;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 589934
    new-instance v0, LX/3WH;

    invoke-direct {v0}, LX/3WH;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0ad;LX/1We;LX/1Wm;LX/Anw;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            "LX/0ad;",
            "LX/1We;",
            "LX/1Wm;",
            "LX/Anw;",
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;",
            "LX/0Ot",
            "<",
            "LX/36F;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 589960
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 589961
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 589962
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->f:LX/0ad;

    .line 589963
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->g:LX/1We;

    .line 589964
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->h:LX/1Wm;

    .line 589965
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->i:LX/Anw;

    .line 589966
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->j:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    .line 589967
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->k:LX/0Ot;

    .line 589968
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 589952
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589953
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 589954
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->g:LX/1We;

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-virtual {v3, p2, v1}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v2

    .line 589955
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->j:Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-virtual {v1, v2}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;)I

    move-result v3

    .line 589956
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->i:LX/Anw;

    invoke-virtual {v1, p1}, LX/Anw;->c(LX/1De;)LX/Anv;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/Anv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Anv;

    move-result-object v4

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-virtual {v4, v1}, LX/Anv;->a(LX/1Pr;)LX/Anv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v4

    invoke-virtual {v1, v4}, LX/Anv;->a(Z)LX/Anv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v4

    invoke-virtual {v1, v4}, LX/Anv;->b(Z)LX/Anv;

    move-result-object v1

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-virtual {v1, v0}, LX/Anv;->c(Z)LX/Anv;

    move-result-object v0

    iget-object v1, v2, LX/1Wj;->f:LX/1Wk;

    invoke-virtual {v0, v1}, LX/Anv;->a(LX/1Wk;)LX/Anv;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/Anv;->h(I)LX/Anv;

    move-result-object v0

    .line 589957
    new-instance v1, LX/C5q;

    invoke-direct {v1, p0, p2, p3}, LX/C5q;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)V

    move-object v1, v1

    .line 589958
    invoke-virtual {v0, v1}, LX/Anv;->a(LX/20Z;)LX/Anv;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 589959
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->h:LX/1Wm;

    invoke-virtual {v1, p1}, LX/1Wm;->c(LX/1De;)LX/39Z;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/39Z;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39Z;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/39Z;->a(LX/1Wj;)LX/39Z;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/39Z;->a(LX/1X1;)LX/39Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;
    .locals 12

    .prologue
    .line 589941
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;

    monitor-enter v1

    .line 589942
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589943
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589946
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v7

    check-cast v7, LX/1We;

    invoke-static {v0}, LX/1Wm;->a(LX/0QB;)LX/1Wm;

    move-result-object v8

    check-cast v8, LX/1Wm;

    invoke-static {v0}, LX/Anw;->a(LX/0QB;)LX/Anw;

    move-result-object v9

    check-cast v9, LX/Anw;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    const/16 v11, 0x868

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;LX/0ad;LX/1We;LX/1Wm;LX/Anw;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;LX/0Ot;)V

    .line 589947
    move-object v0, v3

    .line 589948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 589969
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 589940
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 589938
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 589939
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    invoke-virtual {v1, p1}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->f:LX/0ad;

    sget-short v2, LX/C5r;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 589936
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589937
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 589935
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
