.class public Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:LX/14w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590656
    const v0, 0x7f030a25

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590657
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590658
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->b:Landroid/content/Context;

    .line 590659
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 590660
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590661
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->e:LX/14w;

    .line 590662
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;
    .locals 7

    .prologue
    .line 590663
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;

    monitor-enter v1

    .line 590664
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590665
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590666
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590667
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590668
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v6

    check-cast v6, LX/14w;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/14w;)V

    .line 590669
    move-object v0, p0

    .line 590670
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590671
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590672
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590674
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 590675
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590676
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 590677
    if-eqz p2, :cond_0

    .line 590678
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590679
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 590680
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 590682
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590683
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 590684
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 590685
    if-eqz v0, :cond_0

    .line 590686
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    const v3, 0x7f080c2a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590687
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590688
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 590689
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590690
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590691
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 590692
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
