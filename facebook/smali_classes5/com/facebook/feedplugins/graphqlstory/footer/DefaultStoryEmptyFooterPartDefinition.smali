.class public Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587738
    new-instance v0, LX/3VJ;

    invoke-direct {v0}, LX/3VJ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587735
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587736
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 587737
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;
    .locals 4

    .prologue
    .line 587724
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;

    monitor-enter v1

    .line 587725
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587726
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587727
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587728
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587729
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 587730
    move-object v0, p0

    .line 587731
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587732
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587733
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 587712
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 587714
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 587715
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 587716
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 587717
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 587718
    invoke-static {p2}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    .line 587719
    invoke-static {v1}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez p0, :cond_0

    sget-object v1, LX/1Ua;->p:LX/1Ua;

    .line 587720
    :goto_0
    new-instance p0, LX/1X6;

    invoke-direct {p0, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    move-object v1, p0

    .line 587721
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587722
    const/4 v0, 0x0

    return-object v0

    .line 587723
    :cond_0
    sget-object v1, LX/1Ua;->a:LX/1Ua;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587713
    const/4 v0, 0x1

    return v0
.end method
