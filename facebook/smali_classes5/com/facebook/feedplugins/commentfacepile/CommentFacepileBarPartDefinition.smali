.class public Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C2j;",
        "LX/1Pf;",
        "LX/C2i;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/C2i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/C2k;

.field public final d:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593032
    new-instance v0, LX/3XP;

    invoke-direct {v0}, LX/3XP;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/C2k;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593027
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593028
    iput-object p1, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->b:LX/0ad;

    .line 593029
    iput-object p2, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->c:LX/C2k;

    .line 593030
    iput-object p3, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->d:Landroid/content/res/Resources;

    .line 593031
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C2i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 593026
    sget-object v0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 592989
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592990
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592991
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 592992
    new-instance v1, LX/C2j;

    .line 592993
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 592994
    new-instance v2, LX/6UY;

    iget-object v3, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->d:Landroid/content/res/Resources;

    const v4, 0x7f02029f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, v3}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 592995
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCommentersConnection;->j()LX/0Px;

    move-result-object v2

    move-object v4, v2

    .line 592996
    :goto_0
    if-eqz v4, :cond_1

    .line 592997
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 592998
    new-instance p1, LX/6UY;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p1, v2}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 592999
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 593000
    :cond_0
    const/4 v2, 0x0

    move-object v4, v2

    goto :goto_0

    .line 593001
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 593002
    iget-object v3, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->c:LX/C2k;

    iget-object v4, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->b:LX/0ad;

    sget v5, LX/0fe;->B:I

    const v6, 0x7fffffff

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    const/4 v5, 0x0

    const/4 p3, 0x2

    const/4 p2, 0x1

    const/4 v7, 0x0

    .line 593003
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    .line 593004
    if-nez v8, :cond_3

    .line 593005
    :cond_2
    :goto_2
    move-object v3, v5

    .line 593006
    iget-object v4, p0, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->c:LX/C2k;

    invoke-virtual {v4, v0}, LX/C2k;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/C2j;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 593007
    :cond_3
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v6

    .line 593008
    if-nez v6, :cond_4

    move v6, v7

    .line 593009
    :goto_3
    if-eqz v6, :cond_2

    .line 593010
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v5

    .line 593011
    if-eqz v5, :cond_8

    .line 593012
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLCommentersConnection;->j()LX/0Px;

    move-result-object v8

    .line 593013
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 593014
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v6

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 593015
    if-lez v6, :cond_8

    .line 593016
    sub-int v9, v5, v6

    .line 593017
    if-ne v6, p2, :cond_6

    .line 593018
    if-nez v9, :cond_5

    .line 593019
    iget-object v6, v3, LX/C2k;->a:Landroid/content/res/Resources;

    const v9, 0x7f082a0a

    new-array v10, p2, [Ljava/lang/Object;

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v7

    invoke-virtual {v6, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 593020
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLCommentersConnection;->a()I

    move-result v6

    goto :goto_3

    .line 593021
    :cond_5
    iget-object v6, v3, LX/C2k;->a:Landroid/content/res/Resources;

    const v10, 0x7f0f0129

    new-array p1, p3, [Ljava/lang/Object;

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v7

    iget-object v5, v3, LX/C2k;->b:LX/154;

    invoke-virtual {v5, v9}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, p2

    invoke-virtual {v6, v10, v9, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 593022
    :cond_6
    if-nez v9, :cond_7

    .line 593023
    iget-object v6, v3, LX/C2k;->a:Landroid/content/res/Resources;

    const v9, 0x7f082a0b

    new-array v10, p3, [Ljava/lang/Object;

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v7

    invoke-virtual {v8, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, p2

    invoke-virtual {v6, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 593024
    :cond_7
    iget-object v6, v3, LX/C2k;->a:Landroid/content/res/Resources;

    const v10, 0x7f0f012a

    const/4 v5, 0x3

    new-array p1, v5, [Ljava/lang/Object;

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v7

    invoke-virtual {v8, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, p2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, p3

    invoke-virtual {v6, v10, v9, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 593025
    :cond_8
    invoke-virtual {v3, v0}, LX/C2k;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x46480d51

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592981
    check-cast p2, LX/C2j;

    check-cast p4, LX/C2i;

    .line 592982
    iget-object v1, p2, LX/C2j;->a:LX/0Px;

    .line 592983
    iget-object v2, p4, LX/C2i;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 592984
    iget-object v1, p2, LX/C2j;->b:Ljava/lang/String;

    .line 592985
    iput-object v1, p4, LX/C2i;->b:Ljava/lang/String;

    .line 592986
    iget-object v1, p2, LX/C2j;->c:Ljava/lang/String;

    .line 592987
    iput-object v1, p4, LX/C2i;->c:Ljava/lang/String;

    .line 592988
    const/16 v1, 0x1f

    const v2, 0x61baec47

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592980
    const/4 v0, 0x1

    return v0
.end method
