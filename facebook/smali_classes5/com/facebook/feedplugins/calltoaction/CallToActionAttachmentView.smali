.class public Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;
.super LX/3Vv;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588833
    new-instance v0, LX/3Vy;

    invoke-direct {v0}, LX/3Vy;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 588834
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 588835
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588836
    invoke-direct {p0, p1, p2}, LX/3Vv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 588837
    const v0, 0x7f03021f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 588838
    const v0, 0x7f0d0849

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 588839
    const v0, 0x7f0d084b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->c:Landroid/widget/TextView;

    .line 588840
    const v0, 0x7f0d084d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->d:Landroid/widget/TextView;

    .line 588841
    const v0, 0x7f0d084e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->e:Landroid/widget/TextView;

    .line 588842
    const v0, 0x7f0d084c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->f:Landroid/widget/RatingBar;

    .line 588843
    const v0, 0x7f0d084a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 588844
    return-void
.end method


# virtual methods
.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 588845
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 588846
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588847
    return-void
.end method
