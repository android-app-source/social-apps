.class public Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JNb;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0s6;

.field private final e:LX/0Uh;

.field public final f:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593527
    new-instance v0, LX/3Xb;

    invoke-direct {v0}, LX/3Xb;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0s6;LX/1AV;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Zb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 593528
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593529
    iput-object p1, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->d:LX/0s6;

    .line 593530
    iput-object p2, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->b:LX/1AV;

    .line 593531
    iput-object p3, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 593532
    iput-object p4, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->e:LX/0Uh;

    .line 593533
    iput-object p5, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->f:LX/0Zb;

    .line 593534
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pf;)Landroid/view/View$OnClickListener;
    .locals 8

    .prologue
    .line 593535
    if-nez p1, :cond_0

    .line 593536
    const/4 v0, 0x0

    .line 593537
    :goto_0
    return-object v0

    .line 593538
    :cond_0
    iget-object v7, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 593539
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->d:LX/0s6;

    invoke-static {v0}, LX/36d;->a(LX/0s6;)Z

    move-result v2

    .line 593540
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->e:LX/0Uh;

    const/16 v1, 0x4c3

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 593541
    new-instance v0, LX/JNa;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, LX/JNa;-><init>(Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;ZZLcom/facebook/graphql/model/GraphQLStory;LX/1Pf;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;Lcom/facebook/content/SecureContextHelper;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;
    .locals 9

    .prologue
    .line 593542
    const-class v1, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 593543
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593544
    sput-object v2, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593547
    new-instance v3, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v5

    check-cast v5, LX/1AV;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;-><init>(LX/0s6;LX/1AV;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Zb;)V

    .line 593548
    move-object v0, v3

    .line 593549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593553
    sget-object v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 593554
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 593555
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593556
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 593557
    const v1, -0x47391ef5

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 593558
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 593559
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 593560
    new-instance v3, LX/JNb;

    .line 593561
    if-nez v0, :cond_0

    .line 593562
    const/4 v4, 0x0

    .line 593563
    :goto_0
    move-object v4, v4

    .line 593564
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 593565
    if-nez v0, :cond_1

    .line 593566
    const/4 v6, 0x0

    .line 593567
    :goto_1
    move-object v5, v6

    .line 593568
    invoke-direct {p0, v1, v2, p3}, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pf;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-direct {v3, v4, v5, v1, v0}, LX/JNb;-><init>(LX/2oV;LX/2pa;Landroid/view/View$OnClickListener;Lcom/facebook/graphql/model/GraphQLMedia;)V

    return-object v3

    :cond_0
    new-instance v4, LX/JNZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5}, LX/JNZ;-><init>(Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;Ljava/lang/String;)V

    goto :goto_0

    .line 593569
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v6

    .line 593570
    if-nez v6, :cond_2

    .line 593571
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F()Ljava/lang/String;

    move-result-object v6

    .line 593572
    :cond_2
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v8

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 593573
    iput-object v6, v8, LX/2oE;->a:Landroid/net/Uri;

    .line 593574
    move-object v6, v8

    .line 593575
    sget-object v8, LX/097;->FROM_CACHE:LX/097;

    .line 593576
    iput-object v8, v6, LX/2oE;->e:LX/097;

    .line 593577
    move-object v6, v6

    .line 593578
    invoke-virtual {v6}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v6

    .line 593579
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v8

    .line 593580
    iput-object v8, v6, LX/2oH;->b:Ljava/lang/String;

    .line 593581
    move-object v6, v6

    .line 593582
    iput-boolean v9, v6, LX/2oH;->g:Z

    .line 593583
    move-object v6, v6

    .line 593584
    iput-boolean v9, v6, LX/2oH;->o:Z

    .line 593585
    move-object v9, v6

    .line 593586
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    .line 593587
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v6

    move v8, v6

    :goto_2
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v6

    :goto_3
    invoke-static {v8, v6}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a(II)F

    move-result v6

    .line 593588
    new-instance v7, LX/2pZ;

    invoke-direct {v7}, LX/2pZ;-><init>()V

    invoke-virtual {v9}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v8

    .line 593589
    iput-object v8, v7, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 593590
    move-object v7, v7

    .line 593591
    float-to-double v8, v6

    .line 593592
    iput-wide v8, v7, LX/2pZ;->e:D

    .line 593593
    move-object v6, v7

    .line 593594
    if-eqz v10, :cond_3

    .line 593595
    const-string v7, "CoverImageParamsKey"

    .line 593596
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 593597
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v8

    .line 593598
    :goto_4
    move-object v8, v8

    .line 593599
    invoke-virtual {v6, v7, v8}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v6

    .line 593600
    :cond_3
    invoke-virtual {v6}, LX/2pZ;->b()LX/2pa;

    move-result-object v6

    goto/16 :goto_1

    :cond_4
    move v8, v7

    .line 593601
    goto :goto_2

    :cond_5
    move v6, v7

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    goto :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x19edce3a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 593602
    check-cast p2, LX/JNb;

    check-cast p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;

    .line 593603
    iget-object v1, p2, LX/JNb;->b:LX/2pa;

    iget-object v2, p2, LX/JNb;->d:Lcom/facebook/graphql/model/GraphQLMedia;

    const/4 v4, 0x0

    .line 593604
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    .line 593605
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p1

    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    :cond_0
    invoke-static {p1, v4}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a(II)F

    move-result v4

    iput v4, p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->d:F

    .line 593606
    invoke-virtual {p4}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->requestLayout()V

    .line 593607
    iget-object v4, p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 593608
    iget-object v4, p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->c:LX/7NI;

    invoke-virtual {v4}, LX/7NI;->h()V

    .line 593609
    iget-object v4, p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object p1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v4, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 593610
    iget-object v1, p2, LX/JNb;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593611
    iget-object v1, p0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->b:LX/1AV;

    iget-object v2, p2, LX/JNb;->a:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 593612
    const/16 v1, 0x1f

    const v2, -0x3a84a1fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move p1, v4

    .line 593613
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 593614
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593615
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593616
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 593617
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 593618
    const v2, -0x47391ef5

    invoke-static {v0, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v2

    .line 593619
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 593620
    check-cast p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;

    .line 593621
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593622
    iget-object v0, p4, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 593623
    return-void
.end method
