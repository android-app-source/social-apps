.class public Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;",
        ">;",
        "LX/JZ6;",
        "TE;",
        "LX/JZ0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/0kL;

.field public final c:LX/0aG;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final e:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598016
    new-instance v0, LX/3Z2;

    invoke-direct {v0}, LX/3Z2;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0kL;LX/0aG;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598010
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598011
    iput-object p1, p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->b:LX/0kL;

    .line 598012
    iput-object p2, p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->c:LX/0aG;

    .line 598013
    iput-object p3, p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 598014
    iput-object p4, p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->e:Ljava/util/concurrent/Executor;

    .line 598015
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;
    .locals 7

    .prologue
    .line 597936
    const-class v1, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    monitor-enter v1

    .line 597937
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597938
    sput-object v2, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597939
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597940
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597941
    new-instance p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;-><init>(LX/0kL;LX/0aG;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Ljava/util/concurrent/Executor;)V

    .line 597942
    move-object v0, p0

    .line 597943
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597944
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597945
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/JZ0;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;LX/JZ6;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 597976
    iget-object v1, p2, LX/JZ6;->a:LX/JZ8;

    .line 597977
    iget-object v2, v1, LX/JZ8;->d:Ljava/lang/String;

    move-object v1, v2

    .line 597978
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v2

    .line 597979
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 597980
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 597981
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object v1

    .line 597982
    invoke-static {v2, v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStructuredSurvey;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    move-result-object v1

    .line 597983
    :goto_0
    move-object v1, v1

    .line 597984
    :goto_1
    move-object v2, v1

    .line 597985
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, LX/JZ6;->a:LX/JZ8;

    .line 597986
    iget-object v3, v1, LX/JZ8;->a:LX/JZ7;

    move-object v1, v3

    .line 597987
    sget-object v3, LX/JZ7;->COMPLETE:LX/JZ7;

    if-eq v1, v3, :cond_0

    if-nez v2, :cond_2

    .line 597988
    :cond_0
    invoke-virtual {p0, v0}, LX/JZ0;->setQuestionActive(Z)V

    .line 597989
    invoke-virtual {p0, v4}, LX/JZ0;->setCompleteLayoutActive(Z)V

    .line 597990
    :cond_1
    return-void

    .line 597991
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 597992
    iget-object v3, p0, LX/JZ0;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597993
    invoke-virtual {p0, v4}, LX/JZ0;->setQuestionActive(Z)V

    .line 597994
    invoke-virtual {p0, v0}, LX/JZ0;->setCompleteLayoutActive(Z)V

    .line 597995
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object v3

    .line 597996
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    .line 597997
    :goto_2
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    .line 597998
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;

    .line 597999
    :goto_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object v5

    const/4 p2, 0x0

    .line 598000
    iget-object v6, p0, LX/JZ0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 598001
    if-nez v0, :cond_6

    .line 598002
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 598003
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 598004
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    invoke-static {v2, v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStructuredSurvey;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    move-result-object v1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 598005
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 598006
    const v7, 0x7f0d0079

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyResponseOption;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/widget/RadioButton;->setTag(ILjava/lang/Object;)V

    .line 598007
    const v7, 0x7f0d0078

    invoke-virtual {v6, v7, v5}, Landroid/widget/RadioButton;->setTag(ILjava/lang/Object;)V

    .line 598008
    invoke-virtual {v6, p2}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 598009
    invoke-virtual {v6, p2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_4
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597975
    sget-object v0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 597968
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 597969
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597970
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 597971
    iget-object v1, p0, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597972
    new-instance v1, LX/JZ9;

    invoke-direct {v1, v0}, LX/JZ9;-><init>(Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JZ8;

    .line 597973
    new-instance v2, LX/JZ4;

    invoke-direct {v2, p0, v0, v1, p3}, LX/JZ4;-><init>(Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;LX/JZ8;LX/1Pr;)V

    .line 597974
    new-instance v0, LX/JZ6;

    invoke-direct {v0, v1, v2}, LX/JZ6;-><init>(LX/JZ8;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x61ef68d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597951
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JZ6;

    check-cast p4, LX/JZ0;

    const/4 v5, 0x0

    .line 597952
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597953
    check-cast v1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    .line 597954
    iget-object v2, p2, LX/JZ6;->a:LX/JZ8;

    .line 597955
    iget-boolean v4, v2, LX/JZ8;->e:Z

    move v4, v4

    .line 597956
    if-eqz v4, :cond_2

    .line 597957
    const/4 v6, 0x0

    .line 597958
    iget-object v4, p4, LX/JZ0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p3

    move v7, v6

    .line 597959
    :goto_0
    if-ge v7, p3, :cond_1

    .line 597960
    iget-object v4, p4, LX/JZ0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    .line 597961
    if-nez v5, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 597962
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_0

    :cond_0
    move v4, v6

    .line 597963
    goto :goto_1

    .line 597964
    :cond_1
    iput-boolean v5, v2, LX/JZ8;->e:Z

    .line 597965
    :cond_2
    invoke-static {p4, v1, p2}, Lcom/facebook/feedplugins/survey/SurveyPagePartDefinition;->a(LX/JZ0;Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;LX/JZ6;)V

    .line 597966
    iget-object v2, p2, LX/JZ6;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v2}, LX/JZ0;->setAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 597967
    const/16 v1, 0x1f

    const v2, 0x6350cf26

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597950
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 597947
    check-cast p4, LX/JZ0;

    .line 597948
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/JZ0;->setAnswerClickListener(Landroid/view/View$OnClickListener;)V

    .line 597949
    return-void
.end method
