.class public Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field public final g:LX/17W;

.field public final h:LX/0Zb;

.field private final i:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597810
    const v0, 0x7f030760

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/17W;LX/0Zb;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597811
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597812
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 597813
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 597814
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->d:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 597815
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 597816
    iput-object p6, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->g:LX/17W;

    .line 597817
    iput-object p7, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->h:LX/0Zb;

    .line 597818
    iput-object p5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->f:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 597819
    iput-object p8, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->i:Landroid/content/Context;

    .line 597820
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;
    .locals 12

    .prologue
    .line 597821
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;

    monitor-enter v1

    .line 597822
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597823
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597824
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597825
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597826
    new-instance v3, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;LX/17W;LX/0Zb;Landroid/content/Context;)V

    .line 597827
    move-object v0, v3

    .line 597828
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597829
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597830
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 597832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597833
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 597834
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v9, 0x0

    const/4 v1, 0x0

    const/high16 v8, -0x80000000

    .line 597835
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597836
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597837
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v5

    const/high16 v6, 0x41200000    # 10.0f

    .line 597838
    iput v6, v5, LX/1UY;->d:F

    .line 597839
    move-object v5, v5

    .line 597840
    invoke-virtual {v5}, LX/1UY;->i()LX/1Ua;

    move-result-object v5

    const v6, 0x7f020bd2

    const/4 v7, -0x1

    invoke-direct {v3, v4, v5, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597841
    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/JYx;

    invoke-direct {v3, p0, v0}, LX/JYx;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597842
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 597843
    const v3, 0x7f0d13b1

    iget-object v4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597844
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 597845
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->mA()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->mA()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 597846
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->mA()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 597847
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 597848
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597849
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 597850
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 597851
    :goto_1
    new-instance v1, LX/1ds;

    invoke-direct {v1, v0, v8, v8, v8}, LX/1ds;-><init>(IIII)V

    .line 597852
    const v0, 0x7f0d13b1

    iget-object v2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->f:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597853
    const v0, 0x7f0d13b0

    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->d:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    new-instance v2, LX/8Cj;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v4, v9, v3}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;I)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597854
    return-object v9

    .line 597855
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b25f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597856
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597857
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597858
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597859
    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
