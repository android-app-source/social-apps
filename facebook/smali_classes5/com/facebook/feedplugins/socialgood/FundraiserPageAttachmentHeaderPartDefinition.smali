.class public Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JYw;",
        "TE;",
        "Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/17W;

.field private final e:Landroid/content/Context;

.field private final f:Landroid/content/res/Resources;

.field public final g:LX/0Zb;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597664
    new-instance v0, LX/3Yx;

    invoke-direct {v0}, LX/3Yx;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Landroid/content/res/Resources;Landroid/content/Context;LX/0Zb;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/17W;",
            "Landroid/content/res/Resources;",
            "Landroid/content/Context;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597665
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597666
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 597667
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 597668
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->d:LX/17W;

    .line 597669
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->f:Landroid/content/res/Resources;

    .line 597670
    iput-object p5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->e:Landroid/content/Context;

    .line 597671
    iput-object p6, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->g:LX/0Zb;

    .line 597672
    iput-object p7, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->h:LX/0Ot;

    .line 597673
    iput-object p8, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->i:LX/0Ot;

    .line 597674
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/JYw;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/JYw;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 597675
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597676
    move-object v10, v0

    check-cast v10, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597677
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/JYu;

    invoke-direct {v2, p0, v10}, LX/JYu;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597678
    invoke-static {v10}, LX/JYy;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    .line 597679
    if-eqz v2, :cond_5

    .line 597680
    new-instance v0, LX/JYv;

    invoke-direct {v0, p0, p3, v2}, LX/JYv;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;LX/1Ps;Ljava/lang/String;)V

    .line 597681
    :goto_0
    const v2, 0x7f0d13a3

    iget-object v4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v2, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597682
    invoke-static {v10}, LX/JYy;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v11

    .line 597683
    invoke-static {v10}, LX/JYy;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v10}, LX/JYy;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 597684
    :cond_0
    const-string v0, ""

    move-object v5, v0

    .line 597685
    :goto_1
    invoke-static {v10}, LX/JYy;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v4

    .line 597686
    if-eqz v4, :cond_1

    .line 597687
    const/4 v0, 0x1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->e:Landroid/content/Context;

    iget-object v5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->f:Landroid/content/res/Resources;

    const v7, 0x7f0b25f1

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->f:Landroid/content/res/Resources;

    const v8, 0x7f0b25f2

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    move v5, v1

    move-object v9, v3

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 597688
    :cond_1
    invoke-static {v10}, LX/JYy;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 597689
    invoke-static {v10}, LX/JYy;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 597690
    if-eqz v0, :cond_4

    .line 597691
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 597692
    :goto_2
    if-eqz v1, :cond_2

    .line 597693
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    .line 597694
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v6

    const/high16 v7, 0x41200000    # 10.0f

    .line 597695
    iput v7, v6, LX/1UY;->d:F

    .line 597696
    move-object v6, v6

    .line 597697
    const/high16 v7, 0x41000000    # 8.0f

    .line 597698
    iput v7, v6, LX/1UY;->b:F

    .line 597699
    move-object v6, v6

    .line 597700
    invoke-virtual {v6}, LX/1UY;->i()LX/1Ua;

    move-result-object v6

    const v7, 0x7f020a48

    const/4 v8, -0x1

    invoke-direct {v2, v4, v6, v7, v8}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597701
    new-instance v1, LX/JYw;

    invoke-direct {v1, v0, v3, v11, v5}, LX/JYw;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-object v1

    .line 597702
    :cond_3
    invoke-static {v10}, LX/JYy;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    move-object v0, v3

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;
    .locals 12

    .prologue
    .line 597703
    const-class v1, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    monitor-enter v1

    .line 597704
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597705
    sput-object v2, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597706
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597707
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597708
    new-instance v3, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    const/16 v10, 0x455

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xbc6

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Landroid/content/res/Resources;Landroid/content/Context;LX/0Zb;LX/0Ot;LX/0Ot;)V

    .line 597709
    move-object v0, v3

    .line 597710
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597711
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597712
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597714
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 597715
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/JYw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2e42adc5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597716
    check-cast p2, LX/JYw;

    check-cast p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;

    .line 597717
    iget-object v1, p2, LX/JYw;->a:Ljava/lang/String;

    const v2, 0x3ff745d1

    .line 597718
    if-nez v1, :cond_1

    .line 597719
    iget-object v4, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 597720
    iget-object v4, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    const/4 v5, -0x2

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 597721
    :goto_0
    iget-object v1, p2, LX/JYw;->b:Ljava/lang/String;

    .line 597722
    if-nez v1, :cond_2

    .line 597723
    iget-object v2, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 597724
    :goto_1
    iget-object v1, p2, LX/JYw;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_2
    iget-object v2, p2, LX/JYw;->c:Ljava/lang/String;

    iget-object v4, p2, LX/JYw;->d:Ljava/lang/CharSequence;

    const/4 p0, 0x0

    .line 597725
    if-eqz v1, :cond_3

    .line 597726
    invoke-virtual {p4}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0994

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 597727
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p4, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->a(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V

    .line 597728
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p4, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->a(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V

    .line 597729
    :goto_3
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 597730
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, v5}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 597731
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 597732
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 597733
    new-instance p1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {p1, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 p3, 0x0

    invoke-interface {v6}, Landroid/text/Spannable;->length()I

    move-result p2

    const/16 v1, 0x11

    invoke-interface {v6, p1, p3, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 597734
    iget-object v5, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 597735
    iget-object v5, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 597736
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v5}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 597737
    const/16 v1, 0x1f

    const v2, 0x4bd1b045    # 2.7484298E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 597738
    :cond_0
    const/4 v1, 0x0

    goto :goto_2

    .line 597739
    :cond_1
    iget-object v4, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 597740
    iget-object v4, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 597741
    iget-object v4, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    goto/16 :goto_0

    .line 597742
    :cond_2
    iget-object v2, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 597743
    iget-object v2, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_1

    .line 597744
    :cond_3
    invoke-virtual {p4}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0995

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 597745
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p4, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V

    .line 597746
    iget-object v6, p4, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p4, v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;->b(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentHeaderView;Landroid/widget/TextView;)V

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597747
    const/4 v0, 0x1

    return v0
.end method
