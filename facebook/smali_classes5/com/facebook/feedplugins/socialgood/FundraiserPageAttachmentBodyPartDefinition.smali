.class public Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JYs;",
        "LX/1Ps;",
        "LX/JYt;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/2sO;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/17W;

.field public final f:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597750
    new-instance v0, LX/3Yy;

    invoke-direct {v0}, LX/3Yy;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2sO;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597751
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597752
    iput-object p1, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 597753
    iput-object p2, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->c:LX/2sO;

    .line 597754
    iput-object p3, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 597755
    iput-object p4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->e:LX/17W;

    .line 597756
    iput-object p5, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->f:LX/0Zb;

    .line 597757
    return-void
.end method

.method private static a(LX/JYs;LX/JYt;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JYs;",
            "LX/JYt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 597758
    iget-object v0, p0, LX/JYs;->b:Ljava/lang/String;

    .line 597759
    iget-object v1, p1, LX/JYt;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 597760
    iget-wide v0, p0, LX/JYs;->d:D

    iget-boolean v2, p0, LX/JYs;->e:Z

    const/4 v7, 0x3

    .line 597761
    iget-object v4, p1, LX/JYt;->c:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 597762
    invoke-virtual {p1}, LX/JYt;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020bd3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/LayerDrawable;

    .line 597763
    const v4, 0x102000d

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 597764
    if-eqz v4, :cond_0

    .line 597765
    const-wide v5, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v5, v0, v5

    if-gez v5, :cond_2

    .line 597766
    const/16 v5, 0x7d

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 597767
    :cond_0
    :goto_1
    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v0

    double-to-int v4, v5

    .line 597768
    if-ge v4, v7, :cond_3

    .line 597769
    iget-object v4, p1, LX/JYt;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 597770
    :goto_2
    iget-object v4, p1, LX/JYt;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v3}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 597771
    iget-boolean v0, p0, LX/JYs;->a:Z

    iget-object v1, p0, LX/JYs;->c:Ljava/lang/String;

    .line 597772
    if-nez v0, :cond_4

    .line 597773
    iget-object v2, p1, LX/JYt;->b:Lcom/facebook/fig/button/FigButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 597774
    :goto_3
    return-void

    .line 597775
    :cond_1
    const/16 v3, 0x8

    goto :goto_0

    .line 597776
    :cond_2
    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1

    .line 597777
    :cond_3
    iget-object v5, p1, LX/JYt;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_2

    .line 597778
    :cond_4
    iget-object v2, p1, LX/JYt;->b:Lcom/facebook/fig/button/FigButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 597779
    iget-object v2, p1, LX/JYt;->b:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597780
    sget-object v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 597781
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597782
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597783
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597784
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->c:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 597785
    const/4 v3, 0x0

    .line 597786
    if-eqz v1, :cond_0

    invoke-static {v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 597787
    invoke-static {v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "attachment_type"

    const-string v3, "native"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 597788
    invoke-static {v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    .line 597789
    const v2, 0x7f0d13a1

    iget-object v4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v5, LX/JYq;

    invoke-direct {v5, p0, v6, v0}, LX/JYq;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)V

    invoke-interface {p1, v2, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597790
    :cond_0
    invoke-static {v6}, LX/JYy;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_2

    .line 597791
    invoke-static {v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597792
    const v0, 0x7f020bd1

    .line 597793
    :goto_0
    invoke-static {v6}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/high16 v4, 0x41200000    # 10.0f

    .line 597794
    iput v4, v2, LX/1UY;->d:F

    .line 597795
    move-object v2, v2

    .line 597796
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    .line 597797
    :goto_1
    iget-object v4, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v5, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    const/4 v8, -0x1

    invoke-direct {v5, v7, v2, v0, v8}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597798
    iget-object v0, p0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/JYr;

    invoke-direct {v2, p0, v6}, LX/JYr;-><init>(Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597799
    invoke-static {v6}, LX/JYy;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    .line 597800
    new-instance v0, LX/JYs;

    invoke-static {v6}, LX/JYy;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)D

    move-result-wide v4

    invoke-static {v6}, LX/JYy;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/JYs;-><init>(ZLjava/lang/String;Ljava/lang/String;DZ)V

    return-object v0

    .line 597801
    :cond_1
    const v0, 0x7f020bd0

    goto :goto_0

    .line 597802
    :cond_2
    invoke-static {v6}, LX/JYy;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v6}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentFacepilePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 597803
    const v0, 0x7f020a46

    goto :goto_0

    .line 597804
    :cond_3
    const v0, 0x7f020a41

    goto :goto_0

    .line 597805
    :cond_4
    sget-object v2, LX/1Ua;->m:LX/1Ua;

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x777545da

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597806
    check-cast p2, LX/JYs;

    check-cast p4, LX/JYt;

    invoke-static {p2, p4}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentBodyPartDefinition;->a(LX/JYs;LX/JYt;)V

    const/16 v1, 0x1f

    const v2, -0x5581de8a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597807
    const/4 v0, 0x1

    return v0
.end method
