.class public Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JRO;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/3hg;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(LX/3hg;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627408
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 627409
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->a:LX/3hg;

    .line 627410
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 627411
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;
    .locals 5

    .prologue
    .line 627412
    const-class v1, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    monitor-enter v1

    .line 627413
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627414
    sput-object v2, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627415
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627416
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 627417
    new-instance p0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    const-class v3, LX/3hg;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3hg;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;-><init>(LX/3hg;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 627418
    move-object v0, p0

    .line 627419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 627423
    check-cast p2, LX/JRO;

    .line 627424
    iget-object v0, p2, LX/JRO;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 627425
    const v1, 0x7f0d1445

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v3, p0, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->a:LX/3hg;

    iget-object v4, p2, LX/JRO;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v4

    iget v5, p2, LX/JRO;->d:I

    invoke-virtual {v3, v0, v4, v5}, LX/3hg;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)LX/JRP;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627426
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4290d246

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627427
    check-cast p1, LX/JRO;

    check-cast p4, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    const/4 v2, 0x0

    .line 627428
    invoke-virtual {p4, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 627429
    const v1, 0x7f020a8d

    invoke-virtual {p4, v1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 627430
    iget-object v1, p4, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v1

    .line 627431
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 627432
    iget v2, p1, LX/JRO;->a:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 627433
    const v2, -0xb1a99b

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 627434
    const/16 v1, 0x1f

    const v2, -0x7f9a5262

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 627435
    check-cast p4, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 627436
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 627437
    return-void
.end method
