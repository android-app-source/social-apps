.class public Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/3hq;",
        "Landroid/net/Uri;",
        "TE;",
        "LX/JRu;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JRu;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/17V;

.field private final e:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition",
            "<TE;",
            "LX/JRu;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final h:LX/1nA;

.field private final i:LX/33C;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 628108
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 628109
    new-instance v0, LX/3hn;

    invoke-direct {v0}, LX/3hn;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/17V;Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nA;LX/33C;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17V;",
            "Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;",
            "Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/1nA;",
            "LX/33C;",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/3i4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628110
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 628111
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->d:LX/17V;

    .line 628112
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->e:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    .line 628113
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->f:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    .line 628114
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 628115
    iput-object p5, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->h:LX/1nA;

    .line 628116
    iput-object p6, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->i:LX/33C;

    .line 628117
    iput-object p7, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 628118
    iput-object p8, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->j:LX/0Ot;

    .line 628119
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/3hq;)Landroid/view/View$OnClickListener;
    .locals 10

    .prologue
    .line 628085
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 628086
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/3i4;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 628087
    if-eqz v1, :cond_0

    .line 628088
    new-instance v0, LX/JRa;

    invoke-direct {v0, p0, v1}, LX/JRa;-><init>(Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V

    .line 628089
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->h:LX/1nA;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 628090
    iget v2, p2, LX/3hq;->d:I

    move v2, v2

    .line 628091
    iget-object v4, p2, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v7, v4

    .line 628092
    iget-object v4, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->d:LX/17V;

    .line 628093
    iget-object v5, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v5

    .line 628094
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v6

    .line 628095
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    const-string v8, "native_newsfeed"

    move v9, v2

    invoke-virtual/range {v4 .. v9}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object v2, v4

    .line 628096
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;
    .locals 12

    .prologue
    .line 628097
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    monitor-enter v1

    .line 628098
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 628099
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 628100
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628101
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 628102
    new-instance v3, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v8

    check-cast v8, LX/1nA;

    invoke-static {v0}, LX/33C;->a(LX/0QB;)LX/33C;

    move-result-object v9

    check-cast v9, LX/33C;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    const/16 v11, 0x1ff

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;-><init>(LX/17V;Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nA;LX/33C;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/0Ot;)V

    .line 628103
    move-object v0, v3

    .line 628104
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 628105
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628106
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 628107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JRu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628084
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 628062
    check-cast p2, LX/3hq;

    .line 628063
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v0

    .line 628064
    if-nez v1, :cond_0

    .line 628065
    const/4 v0, 0x0

    .line 628066
    :goto_0
    return-object v0

    .line 628067
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, v1}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628068
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->e:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628069
    const v0, 0x7f0d1cc4

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->f:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 628070
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 628071
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 628072
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 628073
    invoke-static {v0}, LX/33C;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 628074
    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v0, p2}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/3hq;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628075
    :cond_1
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 628076
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 628077
    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 628078
    invoke-static {v2}, LX/3hv;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p0

    .line 628079
    invoke-static {p0}, LX/3hv;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/net/Uri;

    move-result-object p0

    move-object v2, p0

    .line 628080
    if-nez v2, :cond_2

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 628081
    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 628082
    :goto_1
    move-object v0, v0

    .line 628083
    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6d03ae11

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628047
    check-cast p1, LX/3hq;

    check-cast p2, Landroid/net/Uri;

    check-cast p4, LX/JRu;

    .line 628048
    sget-object v1, Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 628049
    iget-object v2, p4, LX/JRu;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 628050
    iget-object v1, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 628051
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 628052
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 628053
    iget-boolean v2, p1, LX/3hq;->f:Z

    move v2, v2

    .line 628054
    if-nez v2, :cond_0

    .line 628055
    const/4 p1, 0x2

    .line 628056
    iget-object v2, p4, LX/JRu;->c:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setLines(I)V

    .line 628057
    iget-object v2, p4, LX/JRu;->c:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 628058
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 628059
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 628060
    iget-object v2, p4, LX/JRu;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 628061
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x23dd1d19

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
