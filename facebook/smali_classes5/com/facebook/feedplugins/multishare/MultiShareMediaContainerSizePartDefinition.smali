.class public Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/ViewGroup;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hq;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627958
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 627959
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;
    .locals 3

    .prologue
    .line 627960
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    monitor-enter v1

    .line 627961
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627962
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627963
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627964
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 627965
    new-instance v0, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;-><init>()V

    .line 627966
    move-object v0, v0

    .line 627967
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627968
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627969
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x50184747

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627971
    check-cast p1, LX/3hq;

    check-cast p4, Landroid/view/ViewGroup;

    .line 627972
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 627973
    iget v2, p1, LX/3hq;->k:F

    move v2, v2

    .line 627974
    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 627975
    iget v2, p1, LX/3hq;->j:F

    move v2, v2

    .line 627976
    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 627977
    const/16 v1, 0x1f

    const v2, -0x29a8a502

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
