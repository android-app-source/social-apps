.class public Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        "V:",
        "LX/3i9;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hq;",
        "LX/3i0;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627944
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 627945
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;
    .locals 3

    .prologue
    .line 627933
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    monitor-enter v1

    .line 627934
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627935
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627936
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627937
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 627938
    new-instance v0, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;-><init>()V

    .line 627939
    move-object v0, v0

    .line 627940
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627941
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627942
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 627946
    check-cast p2, LX/3hq;

    const/4 v0, 0x0

    .line 627947
    iget-object v1, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v1

    .line 627948
    invoke-static {v3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 627949
    iget-object v1, p2, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 627950
    if-nez v1, :cond_0

    move-object v2, v0

    .line 627951
    :goto_0
    if-nez v4, :cond_1

    .line 627952
    new-instance v1, LX/3i0;

    invoke-direct {v1, v0, v2}, LX/3i0;-><init>(LX/1ZS;LX/162;)V

    move-object v0, v1

    .line 627953
    :goto_1
    return-object v0

    .line 627954
    :cond_0
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 627955
    :cond_1
    invoke-static {v3}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 627956
    if-eqz v1, :cond_2

    if-eq v1, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 627957
    :cond_2
    new-instance v1, LX/3i0;

    new-instance v3, LX/1ZS;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, LX/1ZS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v2}, LX/3i0;-><init>(LX/1ZS;LX/162;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1c24e974

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627922
    check-cast p2, LX/3i0;

    check-cast p4, LX/3i9;

    .line 627923
    const v1, 0x7f0d0082

    iget-object v2, p2, LX/3i0;->b:LX/162;

    invoke-virtual {p4, v1, v2}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627924
    const v2, 0x7f0d0087

    iget-object v1, p2, LX/3i0;->b:LX/162;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p4, v2, v1}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627925
    const v1, 0x7f0d0067

    iget-object v2, p2, LX/3i0;->a:LX/1ZS;

    invoke-virtual {p4, v1, v2}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627926
    const/16 v1, 0x1f

    const v2, -0x6aa2e936

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 627927
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 627928
    check-cast p4, LX/3i9;

    const/4 v1, 0x0

    .line 627929
    const v0, 0x7f0d0067

    invoke-virtual {p4, v0, v1}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627930
    const v0, 0x7f0d0082

    invoke-virtual {p4, v0, v1}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627931
    const v0, 0x7f0d0087

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, LX/3i9;->setTag(ILjava/lang/Object;)V

    .line 627932
    return-void
.end method
