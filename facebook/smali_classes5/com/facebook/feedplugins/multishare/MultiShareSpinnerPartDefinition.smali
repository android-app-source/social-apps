.class public Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/3hq;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3i8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/3i8;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 628199
    new-instance v0, LX/3hp;

    invoke-direct {v0}, LX/3hp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628181
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 628182
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    .line 628183
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;
    .locals 2

    .prologue
    .line 628196
    new-instance v1, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-direct {v1, v0}, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;)V

    .line 628197
    move-object v0, v1

    .line 628198
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3i8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628195
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 628192
    check-cast p2, LX/3hq;

    .line 628193
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628194
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x54e2fa4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628185
    check-cast p1, LX/3hq;

    check-cast p4, LX/3i8;

    .line 628186
    iget-object v1, p4, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p4}, LX/3i8;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0a0278

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundColor(I)V

    .line 628187
    iget-boolean v1, p1, LX/3hq;->f:Z

    move v1, v1

    .line 628188
    if-eqz v1, :cond_0

    .line 628189
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/3i8;->setDescriptionTextVisibility(I)V

    .line 628190
    :goto_0
    const/16 v1, 0x1f

    const v2, 0xf2015e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 628191
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/3i8;->setDescriptionTextVisibility(I)V

    goto :goto_0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 628184
    return-void
.end method
