.class public Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;
.super LX/2oW;
.source ""

# interfaces
.implements LX/3FQ;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final n:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/0iY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/3GZ;

.field private p:LX/2pM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 628859
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 628857
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 628858
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 628855
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 628856
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 628851
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 628852
    sget-object v0, LX/04D;->CAROUSEL_VIDEO:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 628853
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 628854
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v0

    check-cast v0, LX/0iY;

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->m:LX/0iY;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628860
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 628861
    new-instance v0, LX/2pM;

    invoke-direct {v0, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->p:LX/2pM;

    .line 628862
    new-instance v0, LX/3GZ;

    invoke-direct {v0, p1}, LX/3GZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->o:LX/3GZ;

    .line 628863
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3iC;

    sget-object v2, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, p1, v2}, LX/3iC;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->p:LX/2pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->o:LX/3GZ;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 628864
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->m:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 628865
    new-instance v1, LX/Bx2;

    invoke-direct {v1, p1}, LX/Bx2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 628866
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 628848
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->o:LX/3GZ;

    new-instance v1, LX/JRb;

    invoke-direct {v1, p0, p1, p2}, LX/JRb;-><init>(Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 628849
    iput-object v1, v0, LX/3GZ;->o:Landroid/view/View$OnClickListener;

    .line 628850
    return-void
.end method

.method public getAudioChannelLayout()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 628847
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProjectionType()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 628846
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 628843
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public getTransitionNode()LX/3FT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 628845
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 628844
    const/4 v0, 0x0

    return-object v0
.end method
