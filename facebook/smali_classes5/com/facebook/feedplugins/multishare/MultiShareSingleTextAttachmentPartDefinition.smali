.class public Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3m8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/3hW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595869
    new-instance v0, LX/3YP;

    invoke-direct {v0}, LX/3YP;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;LX/3hW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595901
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595902
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    .line 595903
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    .line 595904
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    .line 595905
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->e:LX/3hW;

    .line 595906
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 595890
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;

    monitor-enter v1

    .line 595891
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595892
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595893
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595894
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595895
    new-instance p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    invoke-static {v0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v6

    check-cast v6, LX/3hW;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;LX/3hW;)V

    .line 595896
    move-object v0, p0

    .line 595897
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595898
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595899
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595889
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 595880
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595881
    const v0, 0x7f0d1637

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595882
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595883
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595884
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 595885
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 595886
    const v1, 0x7f0d1ccb

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595887
    const v1, 0x7f0d1cca

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareSingleTextAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595888
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3465c1e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595871
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/3m8;

    const/4 p0, 0x0

    .line 595872
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 595873
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595874
    iget-object p1, p4, LX/3m8;->b:Landroid/view/View;

    goto :goto_2

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 595875
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 595876
    invoke-virtual {p4, p0}, LX/3m8;->setTextBelowVisible(Z)V

    .line 595877
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x1b4d8751    # 1.7000926E-22f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 595878
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/3m8;->setTextBelowVisible(Z)V

    goto :goto_1

    .line 595879
    :goto_2
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595870
    const/4 v0, 0x1

    return v0
.end method
