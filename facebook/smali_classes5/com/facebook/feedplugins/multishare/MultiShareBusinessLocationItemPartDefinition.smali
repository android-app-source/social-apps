.class public Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/3hq;",
        "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
        "TE;",
        "LX/JRv;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JRv;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field public final c:LX/3hg;

.field public final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Lcom/facebook/maps/rows/MapPartDefinition;

.field public final f:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

.field public final g:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final h:Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

.field public final i:LX/3hh;

.field public final j:LX/1Ad;

.field public final k:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 627324
    new-instance v0, LX/3hf;

    invoke-direct {v0}, LX/3hf;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->a:LX/1Cz;

    .line 627325
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3hg;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;LX/3hh;LX/1Ad;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627326
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 627327
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->c:LX/3hg;

    .line 627328
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 627329
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->e:Lcom/facebook/maps/rows/MapPartDefinition;

    .line 627330
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->f:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    .line 627331
    iput-object p5, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 627332
    iput-object p6, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->h:Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    .line 627333
    iput-object p7, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->i:LX/3hh;

    .line 627334
    iput-object p8, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->j:LX/1Ad;

    .line 627335
    iput-object p9, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->k:Landroid/content/res/Resources;

    .line 627336
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)LX/0Px;
    .locals 12
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;",
            ")",
            "LX/0Px",
            "<",
            "LX/68O;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 627337
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 627338
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 627339
    new-instance v5, LX/68O;

    new-instance v6, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const-string v0, "images/ads/common/pins/map-card-pin-2x.png"

    invoke-direct {v5, v6, v0, v7, v7}, LX/68O;-><init>(Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;FF)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 627340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 627341
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;
    .locals 13

    .prologue
    .line 627342
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    monitor-enter v1

    .line 627343
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627344
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 627347
    new-instance v3, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    const-class v4, LX/3hg;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/3hg;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/maps/rows/MapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/MapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/maps/rows/MapPartDefinition;

    invoke-static {v0}, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    invoke-static {v0}, LX/3hh;->b(LX/0QB;)LX/3hh;

    move-result-object v10

    check-cast v10, LX/3hh;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v11

    check-cast v11, LX/1Ad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;-><init>(LX/3hg;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;LX/3hh;LX/1Ad;Landroid/content/res/Resources;)V

    .line 627348
    move-object v0, v3

    .line 627349
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627350
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627351
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JRv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627353
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 627354
    check-cast p2, LX/3hq;

    const/4 p3, 0x0

    .line 627355
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 627356
    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 627357
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 627358
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627359
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 627360
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 627361
    iget-object v5, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->c:LX/3hg;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    .line 627362
    iget v7, p2, LX/3hq;->d:I

    move v7, v7

    .line 627363
    invoke-virtual {v5, v3, v6, v7}, LX/3hg;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)LX/JRP;

    move-result-object v5

    .line 627364
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v6

    .line 627365
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v7

    const-string v8, "business_location_story"

    invoke-static {v7, v8, v6}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v6

    .line 627366
    const v7, 0x7f0d1cd2

    iget-object v8, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v7, v8, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627367
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    if-nez v7, :cond_0

    .line 627368
    :goto_0
    invoke-static {v1}, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)LX/0Px;

    move-result-object v0

    .line 627369
    invoke-virtual {v6, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 627370
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->k:Landroid/content/res/Resources;

    .line 627371
    iget v7, p2, LX/3hq;->i:F

    move v7, v7

    .line 627372
    invoke-static {v0, v7}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    .line 627373
    iget-object v7, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->k:Landroid/content/res/Resources;

    .line 627374
    iget v8, p2, LX/3hq;->i:F

    move v8, v8

    .line 627375
    invoke-static {v7, v8}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v7

    .line 627376
    const v8, 0x7f0d1cd0

    iget-object v9, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->e:Lcom/facebook/maps/rows/MapPartDefinition;

    new-instance v10, LX/3BY;

    invoke-direct {v10, v6, p3, v0, v7}, LX/3BY;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;ZII)V

    invoke-interface {p1, v8, v9, v10}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627377
    const v8, 0x7f0d1cd0

    iget-object v9, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->f:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    new-instance v10, LX/3Ba;

    invoke-direct {v10, v3, v6, v0, v7}, LX/3Ba;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;II)V

    invoke-interface {p1, v8, v9, v10}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627378
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v0, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627379
    const v0, 0x7f0d1cd0

    iget-object v3, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v0, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627380
    const v0, 0x7f0d1cd4

    iget-object v3, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->h:Lcom/facebook/feedplugins/multishare/BusinessActionButtonPartDefinition;

    new-instance v5, LX/JRO;

    const v6, 0x7f020837

    .line 627381
    iget v7, p2, LX/3hq;->d:I

    move v7, v7

    .line 627382
    invoke-direct {v5, v6, v4, v2, v7}, LX/JRO;-><init>(ILcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627383
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->j:LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->i:LX/3hh;

    .line 627384
    iput-object v2, v1, LX/1bX;->j:LX/33B;

    .line 627385
    move-object v1, v1

    .line 627386
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    move-object v0, v0

    .line 627387
    return-object v0

    .line 627388
    :cond_0
    const v7, 0x7f0d1cd3

    iget-object v8, p0, Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v7, v8, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7ded6742

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627389
    check-cast p1, LX/3hq;

    check-cast p2, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    check-cast p4, LX/JRv;

    .line 627390
    iget v1, p1, LX/3hq;->i:F

    move v1, v1

    .line 627391
    invoke-virtual {p4}, LX/JRv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0b09ce

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 627392
    invoke-virtual {p4}, LX/JRv;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result p0

    int-to-float p0, p0

    const/high16 p1, 0x40000000    # 2.0f

    mul-float/2addr v2, p1

    sub-float v2, p0, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 627393
    iget-object p0, p4, LX/JRv;->b:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 627394
    iput v2, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 627395
    iput v2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 627396
    iget-object v2, p4, LX/JRv;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v2, p0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 627397
    invoke-virtual {p4, p2}, LX/JRv;->setController(LX/1aZ;)V

    .line 627398
    const/16 v1, 0x1f

    const v2, 0x45d05326

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 627399
    check-cast p4, LX/JRv;

    .line 627400
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/JRv;->setController(LX/1aZ;)V

    .line 627401
    return-void
.end method
