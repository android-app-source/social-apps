.class public Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/3hq;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3i8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/3i8;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition",
            "<TE;",
            "LX/3i8;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/33C;

.field private final d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;",
            "LX/3i8;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition",
            "<TE;",
            "LX/3i8;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

.field private final h:LX/3hW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 627549
    new-instance v0, LX/3hj;

    invoke-direct {v0}, LX/3hj;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/33C;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;LX/3hW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627556
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 627557
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    .line 627558
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->c:LX/33C;

    .line 627559
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 627560
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 627561
    iput-object p5, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->f:Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    .line 627562
    iput-object p6, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    .line 627563
    iput-object p7, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->h:LX/3hW;

    .line 627564
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;
    .locals 11

    .prologue
    .line 627565
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    monitor-enter v1

    .line 627566
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627567
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627568
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627569
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 627570
    new-instance v3, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-static {v0}, LX/33C;->a(LX/0QB;)LX/33C;

    move-result-object v5

    check-cast v5, LX/33C;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    invoke-static {v0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v10

    check-cast v10, LX/3hW;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/33C;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;LX/3hW;)V

    .line 627571
    move-object v0, v3

    .line 627572
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627573
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627574
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627575
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3hq;)Z
    .locals 2

    .prologue
    .line 627550
    iget-object v0, p0, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 627551
    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 627552
    iget v1, p0, LX/3hq;->c:I

    move v1, v1

    .line 627553
    invoke-static {v0, v1}, LX/3hv;->b(Lcom/facebook/graphql/model/GraphQLStory;I)LX/0Px;

    move-result-object v0

    .line 627554
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3i8;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627555
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 627493
    check-cast p2, LX/3hq;

    const/4 v2, 0x0

    .line 627494
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v0

    .line 627495
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 627496
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627497
    if-eqz v0, :cond_0

    .line 627498
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->b:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627499
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627500
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move-object v1, v2

    .line 627501
    :goto_0
    invoke-static {v0}, LX/33C;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v1, :cond_1

    invoke-static {v1}, LX/1xP;->b(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v1}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    invoke-static {v0}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 627502
    :cond_2
    iget-object v4, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v5, LX/2ya;

    invoke-direct {v5, v3}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627503
    :cond_3
    invoke-static {p2}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a(LX/3hq;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 627504
    iget-object v4, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->f:Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    invoke-interface {p1, v4, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627505
    :cond_4
    iget v4, p2, LX/3hq;->c:I

    move v4, v4

    .line 627506
    if-nez v4, :cond_5

    invoke-static {v1}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 627507
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->g:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    new-instance v4, LX/2yf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    invoke-direct {v4, v3, v0, v5}, LX/2yf;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627508
    :cond_5
    return-object v2

    .line 627509
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x59d10202

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627510
    check-cast p1, LX/3hq;

    check-cast p4, LX/3i8;

    const/4 v4, 0x0

    .line 627511
    iget-object v1, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 627512
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 627513
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627514
    iget-object v2, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 627515
    const/4 p0, 0x1

    invoke-virtual {p4, p0}, LX/3i8;->setItemImageViewVisibility(Z)V

    .line 627516
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, LX/3i8;->setInlineVideoViewVisibility(Z)V

    .line 627517
    invoke-static {p1}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;->a(LX/3hq;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 627518
    iget-object p0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 627519
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627520
    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    .line 627521
    iget p3, p1, LX/3hq;->c:I

    move p3, p3

    .line 627522
    invoke-static {p2, p3}, LX/3hv;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/net/Uri;

    move-result-object p2

    .line 627523
    if-nez p2, :cond_4

    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    if-eqz p3, :cond_4

    .line 627524
    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    .line 627525
    :goto_0
    sget-object p2, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 627526
    iget-object p3, p4, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p3, p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 627527
    :cond_0
    iget-object p0, p4, LX/3i8;->j:Landroid/view/View;

    if-eqz v4, :cond_5

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 627528
    iget-boolean v2, p1, LX/3hq;->g:Z

    move v2, v2

    .line 627529
    if-eqz v2, :cond_1

    .line 627530
    invoke-virtual {p4, v4}, LX/3i8;->setItemFooterViewVisibility(Z)V

    .line 627531
    :goto_2
    const/16 v1, 0x1f

    const v2, -0x6f3b9d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 627532
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 627533
    :goto_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 627534
    iget-object p0, p4, LX/3i8;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627535
    iget-boolean v1, p1, LX/3hq;->f:Z

    move v1, v1

    .line 627536
    if-eqz v1, :cond_3

    .line 627537
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/3i8;->setDescriptionTextVisibility(I)V

    goto :goto_2

    .line 627538
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 627539
    :cond_3
    invoke-virtual {p4, v4}, LX/3i8;->setDescriptionTextVisibility(I)V

    .line 627540
    iget-object v1, p4, LX/3i8;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627541
    goto :goto_2

    :cond_4
    move-object p0, p2

    goto :goto_0

    .line 627542
    :cond_5
    const/16 v2, 0x8

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 627543
    check-cast p4, LX/3i8;

    const/4 v0, 0x0

    .line 627544
    invoke-virtual {p4, v0}, LX/3i8;->setItemImageViewVisibility(Z)V

    .line 627545
    invoke-virtual {p4, v0}, LX/3i8;->setInlineVideoViewVisibility(Z)V

    .line 627546
    invoke-virtual {p4, v0}, LX/3i8;->setDescriptionTextVisibility(I)V

    .line 627547
    const/4 v0, 0x1

    invoke-virtual {p4, v0}, LX/3i8;->setItemFooterViewVisibility(Z)V

    .line 627548
    return-void
.end method
