.class public Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        "V:",
        "LX/3i8;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hq;",
        "LX/3hx;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static s:LX/0Xm;


# instance fields
.field private final b:LX/2mY;

.field public final c:Z

.field private final d:LX/094;

.field public final e:LX/1AY;

.field private final f:LX/1VK;

.field private final g:LX/33C;

.field public final h:LX/3hk;

.field private final i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final j:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

.field private final k:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

.field private final l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field private final m:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final n:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

.field private final o:LX/23r;

.field private final p:LX/23s;

.field private final q:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final r:LX/2mn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 627884
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2mY;LX/1AY;LX/1VK;LX/094;LX/33C;LX/3hk;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;LX/23r;LX/23s;Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;LX/2mn;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/rows/gk/IsVideoCarouselRendering;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2mY;",
            "LX/1AY;",
            "LX/1VK;",
            "LX/094;",
            "LX/33C;",
            "LX/3hk;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;",
            "LX/23r;",
            "LX/23s;",
            "Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;",
            "Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;",
            "Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;",
            "LX/2mn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627865
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 627866
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->e:LX/1AY;

    .line 627867
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->f:LX/1VK;

    .line 627868
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->b:LX/2mY;

    .line 627869
    iput-object p7, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->h:LX/3hk;

    .line 627870
    iput-object p8, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 627871
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->j:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    .line 627872
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->k:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    .line 627873
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    .line 627874
    iput-object p5, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->d:LX/094;

    .line 627875
    iput-object p6, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->g:LX/33C;

    .line 627876
    iput-object p9, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 627877
    iput-object p10, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->m:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 627878
    iput-object p11, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->n:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    .line 627879
    iput-object p12, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->o:LX/23r;

    .line 627880
    iput-object p13, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->p:LX/23s;

    .line 627881
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->q:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    .line 627882
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->r:LX/2mn;

    .line 627883
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;DI)LX/2pa;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "DI)",
            "LX/2pa;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 627820
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 627821
    :cond_0
    const/4 v0, 0x0

    .line 627822
    :goto_0
    return-object v0

    .line 627823
    :cond_1
    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 627824
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    sget-object v2, LX/097;->FROM_STREAM:LX/097;

    .line 627825
    iput-object v2, v1, LX/2oE;->e:LX/097;

    .line 627826
    move-object v1, v1

    .line 627827
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 627828
    iput-object v2, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 627829
    move-object v1, v1

    .line 627830
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->aw()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 627831
    iput-object v2, v1, LX/2oE;->b:Landroid/net/Uri;

    .line 627832
    move-object v1, v1

    .line 627833
    invoke-virtual {v1}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v1

    .line 627834
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    .line 627835
    iput-object v2, v1, LX/2oH;->b:Ljava/lang/String;

    .line 627836
    move-object v1, v1

    .line 627837
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/2oH;->a(II)LX/2oH;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v2

    .line 627838
    iput v2, v1, LX/2oH;->l:I

    .line 627839
    move-object v1, v1

    .line 627840
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v2

    .line 627841
    iput v2, v1, LX/2oH;->c:I

    .line 627842
    move-object v1, v1

    .line 627843
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 627844
    iput-object v2, v1, LX/2oH;->e:LX/162;

    .line 627845
    move-object v1, v1

    .line 627846
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 627847
    iput-boolean v2, v1, LX/2oH;->f:Z

    .line 627848
    move-object v1, v1

    .line 627849
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->ab()Z

    move-result v2

    .line 627850
    iput-boolean v2, v1, LX/2oH;->g:Z

    .line 627851
    move-object v1, v1

    .line 627852
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->am()I

    move-result v2

    .line 627853
    iput v2, v1, LX/2oH;->r:I

    .line 627854
    move-object v1, v1

    .line 627855
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v1

    .line 627856
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 627857
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "CoverImageParamsKey"

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v2, "MultiShareGraphQLSubStoryIndexKey"

    add-int/lit8 v3, p5, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v2, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v2, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 627858
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    .line 627859
    iput-object v1, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 627860
    move-object v1, v2

    .line 627861
    invoke-virtual {v1, v0}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    .line 627862
    iput-wide p3, v0, LX/2pZ;->e:D

    .line 627863
    move-object v0, v0

    .line 627864
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;
    .locals 3

    .prologue
    .line 627812
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    monitor-enter v1

    .line 627813
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627814
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627815
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627816
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627817
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627818
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627819
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 627788
    const/4 v0, 0x0

    .line 627789
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {p2, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 627790
    :cond_0
    :goto_0
    move-object v4, v0

    .line 627791
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 627792
    :goto_1
    const v5, 0x7f0d1cd8

    iget-object v6, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-eqz v0, :cond_3

    move v3, v1

    :goto_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v5, v6, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627793
    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 627794
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 627795
    goto :goto_1

    .line 627796
    :cond_3
    const/16 v3, 0x8

    goto :goto_2

    .line 627797
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->k()LX/0Px;

    move-result-object v5

    .line 627798
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 627799
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move v3, v1

    .line 627800
    :goto_4
    if-ge v3, v6, :cond_6

    .line 627801
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;

    .line 627802
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->a()Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->BOLD:Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    if-ne v1, v4, :cond_5

    .line 627803
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->k()I

    move-result v1

    .line 627804
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineStyleAtRange;->j()I

    move-result v0

    add-int/2addr v0, v1

    .line 627805
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v8, 0x21

    invoke-virtual {v7, v4, v1, v0, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 627806
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    .line 627807
    :cond_6
    const v0, 0x7f0d1cd8

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->m:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627808
    const v0, 0x7f0d1cd8

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->n:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    new-instance v2, LX/2eV;

    const v3, 0x7f020966

    const v4, -0xb4b0aa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2eV;-><init>(ILjava/lang/Integer;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_3

    .line 627809
    :cond_7
    const v3, 0x3d8ce43a

    invoke-static {p2, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v3

    .line 627810
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 627811
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(LX/3hq;LX/3hx;LX/1Po;LX/1aD;)V
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3hq;",
            "LX/3hx;",
            "TE;",
            "LX/1aD",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 627766
    invoke-virtual {p1}, LX/3hq;->a()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v9

    .line 627767
    invoke-virtual {v9}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627768
    invoke-static {v3}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 627769
    :cond_0
    :goto_0
    return-void

    .line 627770
    :cond_1
    invoke-interface/range {p3 .. p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v11

    .line 627771
    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->b:LX/2mY;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    sget-object v5, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v9, v4, v5}, LX/2mY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0am;Lcom/facebook/common/callercontext/CallerContext;)LX/C2F;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->c:LX/C2F;

    .line 627772
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->c:LX/C2F;

    invoke-virtual {v2}, LX/C2F;->c()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 627773
    move-object/from16 v0, p2

    iget-object v4, v0, LX/3hx;->c:LX/C2F;

    move-object/from16 v2, p3

    check-cast v2, LX/1Pt;

    invoke-virtual {v4, v11, v2}, LX/C2F;->a(LX/04D;LX/1Pt;)V

    .line 627774
    invoke-static {v9}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 627775
    new-instance v12, LX/JRi;

    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {p1}, LX/3hq;->f()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->f:LX/1VK;

    invoke-direct {v12, v4, v2, v5, v6}, LX/JRi;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ILX/1VK;)V

    .line 627776
    invoke-virtual {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/facebook/graphql/model/GraphQLStory;

    move-object/from16 v2, p3

    .line 627777
    check-cast v2, LX/1Pr;

    invoke-interface {v2, v12, v10}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2oL;

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->j:LX/2oL;

    .line 627778
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->j:LX/2oL;

    invoke-virtual {v2}, LX/2oL;->b()LX/2oO;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->l:LX/2oO;

    .line 627779
    new-instance v2, LX/093;

    invoke-direct {v2}, LX/093;-><init>()V

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->h:LX/093;

    .line 627780
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->j:LX/2oL;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/2oL;->a(I)V

    .line 627781
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->j:LX/2oL;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/2oL;->b(Z)V

    .line 627782
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->j:LX/2oL;

    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v2, v5}, LX/2oL;->a(LX/04g;)V

    .line 627783
    move-object/from16 v0, p2

    iget-object v5, v0, LX/3hx;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->r:LX/2mn;

    const/4 v6, 0x0

    invoke-virtual {v2, v9, v6}, LX/2mn;->d(Lcom/facebook/feed/rows/core/props/FeedProps;F)D

    move-result-wide v6

    invoke-virtual {p1}, LX/3hq;->f()I

    move-result v8

    invoke-static/range {v3 .. v8}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;DI)LX/2pa;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, LX/3hx;->g:LX/2pa;

    .line 627784
    move-object/from16 v0, p2

    iget-object v2, v0, LX/3hx;->g:LX/2pa;

    if-eqz v2, :cond_0

    .line 627785
    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->p:LX/23s;

    invoke-interface/range {p3 .. p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v2

    .line 627786
    new-instance v3, LX/0AW;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    invoke-direct {v3, v5}, LX/0AW;-><init>(LX/162;)V

    invoke-static {v4}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/0AW;->a(Z)LX/0AW;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0AW;->a(LX/04H;)LX/0AW;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, LX/3hx;->j:LX/2oL;

    invoke-virtual {v3}, LX/2oL;->c()LX/04g;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0AW;->a(LX/04g;)LX/0AW;

    move-result-object v2

    invoke-virtual {v2}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v7

    .line 627787
    iget-object v13, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->q:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    new-instance v2, LX/3J1;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/3hx;->h:LX/093;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/3hx;->g:LX/2pa;

    iget-object v6, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    new-instance v9, LX/JRc;

    invoke-direct {v9, p0}, LX/JRc;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;)V

    move-object v3, v12

    move-object v4, v10

    move-object v8, v11

    invoke-direct/range {v2 .. v9}, LX/3J1;-><init>(LX/1KL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v13, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static a(LX/3hx;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 627760
    iget-object v0, p0, LX/3hx;->j:LX/2oL;

    .line 627761
    iget-boolean v1, v0, LX/2oL;->d:Z

    move v0, v1

    .line 627762
    iget-object v1, p0, LX/3hx;->m:LX/3Qx;

    invoke-virtual {v1, p1}, LX/3Qx;->onClick(Landroid/view/View;)V

    .line 627763
    if-eqz v0, :cond_0

    .line 627764
    iget-object v0, p0, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, LX/2oW;->b()V

    .line 627765
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;
    .locals 19

    .prologue
    .line 627758
    new-instance v1, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    const/16 v2, 0x14b6

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const-class v3, LX/2mY;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mY;

    invoke-static/range {p0 .. p0}, LX/1AY;->a(LX/0QB;)LX/1AY;

    move-result-object v4

    check-cast v4, LX/1AY;

    const-class v5, LX/1VK;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1VK;

    invoke-static/range {p0 .. p0}, LX/094;->a(LX/0QB;)LX/094;

    move-result-object v6

    check-cast v6, LX/094;

    invoke-static/range {p0 .. p0}, LX/33C;->a(LX/0QB;)LX/33C;

    move-result-object v7

    check-cast v7, LX/33C;

    invoke-static/range {p0 .. p0}, LX/3hk;->a(LX/0QB;)LX/3hk;

    move-result-object v8

    check-cast v8, LX/3hk;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    const-class v13, LX/23r;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/23r;

    invoke-static/range {p0 .. p0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v14

    check-cast v14, LX/23s;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    move-result-object v17

    check-cast v17, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    invoke-static/range {p0 .. p0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v18

    check-cast v18, LX/2mn;

    invoke-direct/range {v1 .. v18}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;-><init>(LX/0Or;LX/2mY;LX/1AY;LX/1VK;LX/094;LX/33C;LX/3hk;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;LX/23r;LX/23s;Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;LX/2mn;)V

    .line 627759
    return-object v1
.end method

.method public static b(LX/3hx;LX/3i8;)V
    .locals 2

    .prologue
    .line 627578
    iget-boolean v0, p0, LX/3hx;->i:Z

    if-eqz v0, :cond_0

    .line 627579
    sget-object v0, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    iget-object v1, p0, LX/3hx;->j:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/3i8;->a(LX/04g;I)V

    .line 627580
    :cond_0
    iget-object v0, p0, LX/3hx;->h:LX/093;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/093;->a(Z)V

    .line 627581
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 627671
    check-cast p2, LX/3hq;

    check-cast p3, LX/1Po;

    const/4 v4, 0x0

    .line 627672
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->j:Lcom/facebook/feedplugins/multishare/MultiShareLoggingTagsPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627673
    const v0, 0x7f0d1cc4

    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->k:Lcom/facebook/feedplugins/multishare/MultiShareMediaContainerSizePartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 627674
    iget v0, p2, LX/3hq;->d:I

    move v2, v0

    .line 627675
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 627676
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 627677
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627678
    invoke-static {v0}, LX/33C;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 627679
    new-instance v1, LX/3hw;

    invoke-direct {v1, p0, p2, v2}, LX/3hw;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hq;I)V

    .line 627680
    iget-object v3, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->i:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v3, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 627681
    :cond_0
    new-instance v6, LX/3hx;

    invoke-direct {v6}, LX/3hx;-><init>()V

    .line 627682
    iget-boolean v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    if-eqz v1, :cond_1

    .line 627683
    iget-boolean v1, p2, LX/3hq;->h:Z

    move v1, v1

    .line 627684
    if-nez v1, :cond_1

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627685
    invoke-direct {p0, p2, v6, p3, p1}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/3hq;LX/3hx;LX/1Po;LX/1aD;)V

    .line 627686
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/1aD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 627687
    iget-boolean v0, p2, LX/3hq;->g:Z

    move v0, v0

    .line 627688
    if-eqz v0, :cond_2

    .line 627689
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    .line 627690
    iget-object v0, p2, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 627691
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 627692
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 627693
    const/4 v5, 0x0

    .line 627694
    invoke-static {v0}, LX/17E;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v1, v5

    .line 627695
    :goto_0
    move-object v5, v1

    .line 627696
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 627697
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 627698
    :goto_1
    new-instance v7, LX/3Qv;

    invoke-direct {v7}, LX/3Qv;-><init>()V

    .line 627699
    iget v0, p2, LX/3hq;->c:I

    move v0, v0

    .line 627700
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627701
    iget-object v8, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 627702
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    if-nez v8, :cond_6

    .line 627703
    :goto_2
    move-object v0, v0

    .line 627704
    iput-object v0, v7, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627705
    move-object v0, v7

    .line 627706
    iput-object v5, v0, LX/3Qv;->b:Ljava/util/List;

    .line 627707
    move-object v0, v0

    .line 627708
    sget-object v5, LX/04g;->BY_USER:LX/04g;

    .line 627709
    iput-object v5, v0, LX/3Qv;->h:LX/04g;

    .line 627710
    move-object v0, v0

    .line 627711
    sget-object v5, LX/04D;->VIDEO_SETS:LX/04D;

    .line 627712
    iput-object v5, v0, LX/3Qv;->g:LX/04D;

    .line 627713
    move-object v0, v0

    .line 627714
    invoke-static {v3}, LX/23s;->a(LX/1Qt;)Ljava/lang/String;

    move-result-object v3

    .line 627715
    iput-object v3, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 627716
    move-object v0, v0

    .line 627717
    add-int/lit8 v2, v2, -0x1

    .line 627718
    iput v2, v0, LX/3Qv;->n:I

    .line 627719
    move-object v0, v0

    .line 627720
    invoke-virtual {v0, v1}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v0

    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v1

    .line 627721
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, v6, LX/3hx;->n:Ljava/util/concurrent/atomic/AtomicReference;

    .line 627722
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->o:LX/23r;

    iget-object v2, v6, LX/3hx;->j:LX/2oL;

    iget-object v3, v6, LX/3hx;->n:Ljava/util/concurrent/atomic/AtomicReference;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    iput-object v0, v6, LX/3hx;->m:LX/3Qx;

    .line 627723
    :cond_2
    new-instance v0, LX/3hy;

    invoke-direct {v0, p0, p2, v6}, LX/3hy;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hq;LX/3hx;)V

    iput-object v0, v6, LX/3hx;->a:Landroid/view/View$OnClickListener;

    .line 627724
    new-instance v0, LX/3hz;

    invoke-direct {v0, p0, p2, v6}, LX/3hz;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hq;LX/3hx;)V

    iput-object v0, v6, LX/3hx;->b:Landroid/view/View$OnClickListener;

    .line 627725
    return-object v6

    :cond_3
    move-object v1, v4

    .line 627726
    goto :goto_1

    .line 627727
    :cond_4
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 627728
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    .line 627729
    invoke-static {v0}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v1

    .line 627730
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627731
    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v10

    .line 627732
    iput-object v5, v10, LX/39x;->b:LX/0Px;

    .line 627733
    move-object v10, v10

    .line 627734
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object p3

    invoke-virtual {p1, p3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object p3

    invoke-virtual {p1, p3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p1

    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 627735
    iput-object p1, v10, LX/39x;->p:LX/0Px;

    .line 627736
    move-object v10, v10

    .line 627737
    invoke-virtual {v10}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v10

    .line 627738
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object p1

    invoke-static {v10}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 627739
    iput-object v10, p1, LX/23u;->k:LX/0Px;

    .line 627740
    move-object v10, p1

    .line 627741
    iput-object v0, v10, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 627742
    move-object v10, v10

    .line 627743
    new-instance p1, LX/173;

    invoke-direct {p1}, LX/173;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 627744
    iput-object v1, p1, LX/173;->f:Ljava/lang/String;

    .line 627745
    move-object v1, p1

    .line 627746
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 627747
    iput-object v1, v10, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 627748
    move-object v1, v10

    .line 627749
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 627750
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move-object v1, v7

    .line 627751
    goto/16 :goto_0

    .line 627752
    :cond_6
    iget-object v8, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 627753
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v8}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v8

    const/4 v9, 0x0

    .line 627754
    iput-object v9, v8, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 627755
    move-object v8, v8

    .line 627756
    invoke-virtual {v8}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    .line 627757
    invoke-static {v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final a(LX/3hq;LX/3hx;Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 627656
    iget-object v0, p2, LX/3hx;->j:LX/2oL;

    .line 627657
    iget-boolean v1, v0, LX/2oL;->d:Z

    move v0, v1

    .line 627658
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->d:LX/094;

    iget-object v2, p2, LX/3hx;->g:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/094;->a(Ljava/lang/String;)V

    .line 627659
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->d:LX/094;

    iget-object v2, p2, LX/3hx;->g:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/094;->a(Ljava/lang/String;Z)V

    .line 627660
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "MultiShareGraphQLSubStoryIndexKey"

    .line 627661
    iget v3, p1, LX/3hq;->d:I

    move v3, v3

    .line 627662
    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "MultiShareGraphQLSubStoryPropsKey"

    .line 627663
    iget-object v3, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 627664
    invoke-static {v3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 627665
    iget-object v2, p2, LX/3hx;->c:LX/C2F;

    invoke-virtual {v2, p3}, LX/C2F;->a(Landroid/view/View;)V

    .line 627666
    iget-object v2, p2, LX/3hx;->l:LX/2oO;

    invoke-virtual {v2}, LX/2oO;->a()V

    .line 627667
    iget-object v2, p2, LX/3hx;->c:LX/C2F;

    iget-object v3, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v3

    invoke-virtual {v2, v3, p4, v0, v1}, LX/C2F;->a(IIZLX/0P1;)V

    .line 627668
    if-eqz v0, :cond_0

    .line 627669
    iget-object v0, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, LX/2oW;->b()V

    .line 627670
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6ee9a71d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627605
    check-cast p1, LX/3hq;

    check-cast p2, LX/3hx;

    check-cast p4, LX/3i8;

    const/4 p3, 0x1

    .line 627606
    iget v1, p1, LX/3hq;->d:I

    move v1, v1

    .line 627607
    iput v1, p4, LX/3i9;->a:I

    .line 627608
    iget-object v1, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 627609
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 627610
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627611
    iget-boolean v2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    if-eqz v2, :cond_0

    .line 627612
    iget-boolean v2, p1, LX/3hq;->h:Z

    move v2, v2

    .line 627613
    if-nez v2, :cond_0

    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 627614
    iget-object v2, p2, LX/3hx;->a:Landroid/view/View$OnClickListener;

    .line 627615
    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 627616
    iget-object v4, p4, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v4, v2}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 627617
    const/4 v4, 0x1

    invoke-virtual {p4, v4}, LX/3i8;->setInlineVideoViewVisibility(Z)V

    .line 627618
    const/4 v4, 0x0

    invoke-virtual {p4, v4}, LX/3i8;->setItemImageViewVisibility(Z)V

    .line 627619
    :cond_0
    invoke-virtual {p4}, LX/3i9;->getCallToActionView()Landroid/view/View;

    move-result-object v2

    iput-object v2, p2, LX/3hx;->k:Landroid/view/View;

    .line 627620
    iget-object v2, p2, LX/3hx;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    const v4, 0x7f0d1445

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    check-cast v2, Landroid/widget/TextView;

    .line 627621
    iget-object v4, p2, LX/3hx;->k:Landroid/view/View;

    const v5, 0x7f0d007d

    .line 627622
    iget v6, p1, LX/3hq;->d:I

    move v6, v6

    .line 627623
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 627624
    iget-object v4, p2, LX/3hx;->k:Landroid/view/View;

    const v5, 0x7f0d007e

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 627625
    if-eqz v2, :cond_1

    .line 627626
    const v4, 0x7f0d007d

    .line 627627
    iget v5, p1, LX/3hq;->d:I

    move v5, v5

    .line 627628
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 627629
    const v4, 0x7f0d007e

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 627630
    :cond_1
    iget-boolean v2, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    if-eqz v2, :cond_2

    .line 627631
    iget-boolean v2, p1, LX/3hq;->h:Z

    move v2, v2

    .line 627632
    if-nez v2, :cond_2

    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 627633
    iget-object v1, p2, LX/3hx;->g:LX/2pa;

    if-nez v1, :cond_3

    .line 627634
    :cond_2
    :goto_0
    const/16 v1, 0x1f

    const v2, 0xd125d01

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 627635
    :cond_3
    iget-object v1, p2, LX/3hx;->g:LX/2pa;

    .line 627636
    iget-object v2, p4, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 627637
    iput-object p4, p2, LX/3hx;->f:LX/3i8;

    .line 627638
    iget-object v1, p4, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    move-object v1, v1

    .line 627639
    iput-object v1, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    .line 627640
    iget-object v1, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 627641
    iget-object v1, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    new-instance v2, LX/JRe;

    iget-object v4, p2, LX/3hx;->j:LX/2oL;

    iget-object v5, p2, LX/3hx;->l:LX/2oO;

    invoke-direct {v2, p0, v4, v5}, LX/JRe;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/2oL;LX/2oO;)V

    .line 627642
    iput-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 627643
    iget-object v1, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    iget-object v2, p2, LX/3hx;->b:Landroid/view/View$OnClickListener;

    iget-object v4, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v1, v2, v4}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->a(Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 627644
    iget-object v1, p2, LX/3hx;->j:LX/2oL;

    .line 627645
    iget-boolean v2, v1, LX/2oL;->a:Z

    move v1, v2

    .line 627646
    if-eqz v1, :cond_4

    .line 627647
    iget-object v1, p2, LX/3hx;->j:LX/2oL;

    sget-object v2, LX/04g;->UNSET:LX/04g;

    invoke-virtual {v1, v2}, LX/2oL;->a(LX/04g;)V

    .line 627648
    iget-object v1, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    .line 627649
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 627650
    invoke-virtual {v1}, LX/2pb;->c()V

    .line 627651
    :cond_4
    new-instance v1, LX/JRd;

    invoke-direct {v1, p0, p2, p4}, LX/JRd;-><init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hx;LX/3i8;)V

    .line 627652
    iget-object v2, p2, LX/3hx;->c:LX/C2F;

    invoke-virtual {v2, v1}, LX/C2F;->a(LX/3J0;)V

    .line 627653
    iget-object v2, p2, LX/3hx;->n:Ljava/util/concurrent/atomic/AtomicReference;

    if-eqz v2, :cond_5

    .line 627654
    iget-object v2, p2, LX/3hx;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 627655
    :cond_5
    invoke-static {p2, p4}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->b(LX/3hx;LX/3i8;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 627582
    check-cast p1, LX/3hq;

    check-cast p2, LX/3hx;

    check-cast p4, LX/3i8;

    const/4 v3, 0x0

    .line 627583
    iget-boolean v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    if-eqz v0, :cond_0

    .line 627584
    invoke-virtual {p4}, LX/3i8;->e()V

    .line 627585
    :cond_0
    iget-object v0, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 627586
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 627587
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627588
    invoke-virtual {p4, v3}, LX/3i8;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 627589
    iget-object v1, p2, LX/3hx;->k:Landroid/view/View;

    const v2, 0x7f0d007d

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 627590
    iget-boolean v1, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->c:Z

    if-eqz v1, :cond_1

    .line 627591
    iget-boolean v1, p1, LX/3hq;->h:Z

    move v1, v1

    .line 627592
    if-nez v1, :cond_1

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 627593
    const/4 v1, 0x0

    .line 627594
    iget-object v0, p1, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 627595
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 627596
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 627597
    :cond_1
    :goto_0
    return-void

    .line 627598
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->e:LX/1AY;

    invoke-virtual {v0, p4}, LX/1AY;->a(Landroid/view/View;)LX/7zf;

    move-result-object v0

    invoke-static {v0}, LX/1AY;->a(LX/7zf;)Z

    move-result v0

    .line 627599
    iget-object v2, p2, LX/3hx;->j:LX/2oL;

    if-eqz v0, :cond_3

    iget-object v0, p2, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 627600
    :goto_1
    iput-boolean v0, v2, LX/2oL;->d:Z

    .line 627601
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p4, v0}, LX/3i8;->b(LX/04g;)V

    .line 627602
    invoke-virtual {p4}, LX/3i8;->e()V

    .line 627603
    iput-boolean v1, p2, LX/3hx;->i:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 627604
    goto :goto_1
.end method
