.class public Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/3ht;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/2dq;

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/3hW;

.field private final g:F

.field public final h:F

.field private final i:LX/2v0;

.field private final j:Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;

.field private final k:LX/0ti;

.field private final l:Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

.field public final m:LX/2di;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 626992
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;LX/0hB;LX/3hW;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2v0;Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;LX/0ti;Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;LX/2di;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 626978
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 626979
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 626980
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    .line 626981
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->d:LX/2dq;

    .line 626982
    iput-object p6, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 626983
    iput-object p5, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->f:LX/3hW;

    .line 626984
    iput-object p7, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->i:LX/2v0;

    .line 626985
    iput-object p8, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->j:Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;

    .line 626986
    iput-object p9, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->k:LX/0ti;

    .line 626987
    iput-object p10, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->l:Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    .line 626988
    iput-object p11, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->m:LX/2di;

    .line 626989
    invoke-virtual {p4}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p4}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->g:F

    .line 626990
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b09d9

    invoke-static {v0, v1}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->h:F

    .line 626991
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/3ht;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/3ht;"
        }
    .end annotation

    .prologue
    .line 626924
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 626925
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 626926
    const v2, 0x7f0b09de

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 626927
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    move v1, v0

    .line 626928
    iget v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->g:F

    mul-float v4, v0, v1

    .line 626929
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 626930
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/high16 v5, 0x3f800000    # 1.0f

    .line 626931
    invoke-static {v0}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_2

    move v3, v5

    .line 626932
    :cond_0
    :goto_0
    move v0, v3

    .line 626933
    div-float v3, v4, v0

    .line 626934
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b09ce

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-static {v0, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    move v0, v0

    .line 626935
    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->b:Landroid/content/Context;

    iget v5, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->g:F

    invoke-static {v2, v5}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    add-float v2, v1, v0

    .line 626936
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    .line 626937
    if-nez v8, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    .line 626938
    :goto_1
    new-instance v5, LX/3hb;

    invoke-direct {v5, p2}, LX/3hb;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, p3

    .line 626939
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v5, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3hc;

    .line 626940
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 626941
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 626942
    invoke-static {v0}, LX/3ha;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 626943
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v1

    const/high16 v6, 0x41000000    # 8.0f

    .line 626944
    iput v6, v1, LX/1UY;->b:F

    .line 626945
    move-object v1, v1

    .line 626946
    iget v6, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->h:F

    .line 626947
    iput v6, v1, LX/1UY;->c:F

    .line 626948
    move-object v1, v1

    .line 626949
    const/high16 v6, -0x3f000000    # -8.0f

    .line 626950
    iput v6, v1, LX/1UY;->d:F

    .line 626951
    move-object v1, v1

    .line 626952
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    .line 626953
    :goto_2
    move-object v10, v1

    .line 626954
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->d:LX/2dq;

    const/high16 v1, 0x41000000    # 8.0f

    add-float/2addr v1, v2

    sget-object v6, LX/2eF;->a:LX/1Ua;

    const/4 v7, 0x1

    invoke-virtual {v0, v1, v6, v7}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v0

    move-object v7, v0

    .line 626955
    new-instance v6, LX/3hd;

    invoke-direct {v6}, LX/3hd;-><init>()V

    .line 626956
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->i:LX/2v0;

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, LX/2v0;->a(Lcom/facebook/feed/rows/core/props/FeedProps;FFFLX/3hc;LX/3hd;)LX/3he;

    move-result-object v9

    .line 626957
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->l:Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 626958
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-direct {v1, v8, v10}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 626959
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v6, LX/2eG;

    .line 626960
    iget v1, v5, LX/3hc;->a:I

    move v8, v1

    .line 626961
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MultiShareAttachmentPartDefinition"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v11

    invoke-direct/range {v6 .. v11}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 626962
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->j:Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;

    new-instance v1, LX/3hs;

    invoke-direct {v1, p2, v5}, LX/3hs;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3hc;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 626963
    new-instance v0, LX/3ht;

    .line 626964
    new-instance v1, LX/3hu;

    invoke-direct {v1, p0, v5, p2, p3}, LX/3hu;-><init>(Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;LX/3hc;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    move-object v1, v1

    .line 626965
    invoke-direct {v0, v1}, LX/3ht;-><init>(LX/1OX;)V

    return-object v0

    .line 626966
    :cond_1
    iget-object v0, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 626967
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v0

    goto/16 :goto_1

    .line 626968
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    .line 626969
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 626970
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :cond_3
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 626971
    invoke-static {v2}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 626972
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 626973
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v7

    if-lez v7, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v7

    if-lez v7, :cond_3

    .line 626974
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v7, v2

    .line 626975
    cmpg-float v7, v2, v3

    if-gez v7, :cond_5

    :goto_4
    move v3, v2

    .line 626976
    goto :goto_3

    .line 626977
    :cond_4
    const v2, 0x3dcccccd    # 0.1f

    cmpg-float v2, v3, v2

    if-gez v2, :cond_0

    move v3, v5

    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto :goto_4

    :cond_6
    sget-object v1, LX/2eF;->a:LX/1Ua;

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;
    .locals 15

    .prologue
    .line 626910
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    monitor-enter v1

    .line 626911
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 626912
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 626913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 626915
    new-instance v3, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v6

    check-cast v6, LX/2dq;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static {v0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v8

    check-cast v8, LX/3hW;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const-class v10, LX/2v0;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/2v0;

    .line 626916
    new-instance v14, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v12

    check-cast v12, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v13

    check-cast v13, LX/0Zb;

    invoke-direct {v14, v11, v12, v13}, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;-><init>(LX/0So;LX/17Q;LX/0Zb;)V

    .line 626917
    move-object v11, v14

    .line 626918
    check-cast v11, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;

    invoke-static {v0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v12

    check-cast v12, LX/0ti;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    invoke-static {v0}, LX/2di;->b(LX/0QB;)LX/2di;

    move-result-object v14

    check-cast v14, LX/2di;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;LX/0hB;LX/3hW;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2v0;Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;LX/0ti;Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;LX/2di;)V

    .line 626919
    move-object v0, v3

    .line 626920
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 626921
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 626922
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 626923
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 626909
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 626908
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/3ht;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x490f5dad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 626899
    check-cast p2, LX/3ht;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 626900
    iget-object v1, p2, LX/3ht;->a:LX/1OX;

    move-object v1, v1

    .line 626901
    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 626902
    const/16 v1, 0x1f

    const v2, 0x766afbe5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 626907
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 626903
    check-cast p2, LX/3ht;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 626904
    iget-object v0, p2, LX/3ht;->a:LX/1OX;

    move-object v0, v0

    .line 626905
    invoke-virtual {p4, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 626906
    return-void
.end method
