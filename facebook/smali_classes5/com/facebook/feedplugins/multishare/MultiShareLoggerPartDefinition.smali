.class public Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hs;",
        "Lcom/facebook/analytics/logger/HoneyClientEvent;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0So;

.field private final b:LX/17Q;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0So;LX/17Q;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 626993
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 626994
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->a:LX/0So;

    .line 626995
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->b:LX/17Q;

    .line 626996
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->c:LX/0Zb;

    .line 626997
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 626998
    check-cast p2, LX/3hs;

    .line 626999
    iget-object v0, p2, LX/3hs;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 627000
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 627001
    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 627002
    const/4 v0, 0x0

    .line 627003
    if-eqz v2, :cond_0

    .line 627004
    iget-object v0, p2, LX/3hs;->b:LX/3hc;

    .line 627005
    iget-boolean p0, v0, LX/3hc;->e:Z

    move p0, p0

    .line 627006
    iget p1, v0, LX/3hc;->a:I

    move p1, p1

    .line 627007
    if-eqz p0, :cond_1

    .line 627008
    iget-object p0, v0, LX/3hc;->b:LX/3hr;

    move-object p0, p0

    .line 627009
    iget-object v0, p0, LX/3hr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move p0, v0

    .line 627010
    add-int/lit8 p0, p0, -0x1

    if-ne p1, p0, :cond_1

    const/4 p0, 0x0

    :goto_0
    move v0, p0

    .line 627011
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 627012
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 627013
    const/4 p0, 0x0

    .line 627014
    :goto_1
    move-object v0, p0

    .line 627015
    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 p0, p1, 0x1

    goto :goto_0

    .line 627016
    :cond_2
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "infinite_carousel_exit_event"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "scroll_index"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-virtual {p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "native_newsfeed"

    .line 627017
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 627018
    move-object p0, p0

    .line 627019
    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5651cbd2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627020
    check-cast p1, LX/3hs;

    .line 627021
    iget-object v4, p1, LX/3hs;->b:LX/3hc;

    iget-object v5, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->a:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    .line 627022
    iput-wide v6, v4, LX/3hc;->g:J

    .line 627023
    const/16 v1, 0x1f

    const v2, 0x4ecebda0    # 1.73426688E9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 627024
    check-cast p1, LX/3hs;

    check-cast p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 627025
    iget-object v0, p1, LX/3hs;->b:LX/3hc;

    .line 627026
    iget-boolean v1, v0, LX/3hc;->h:Z

    move v1, v1

    .line 627027
    if-nez v1, :cond_0

    .line 627028
    iget-boolean v1, v0, LX/3hc;->i:Z

    move v1, v1

    .line 627029
    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 627030
    iget-object v5, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->a:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    .line 627031
    iget-wide v9, v0, LX/3hc;->g:J

    move-wide v7, v9

    .line 627032
    sub-long/2addr v5, v7

    move-wide v2, v5

    .line 627033
    const-string v4, "last_page_duration"

    invoke-virtual {p2, v4, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 627034
    iget-object v2, p0, Lcom/facebook/feedplugins/multishare/MultiShareLoggerPartDefinition;->c:LX/0Zb;

    invoke-interface {v2, p2}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 627035
    :goto_0
    return-void

    .line 627036
    :cond_0
    const/4 v1, 0x0

    .line 627037
    iput-boolean v1, v0, LX/3hc;->h:Z

    .line 627038
    goto :goto_0
.end method
