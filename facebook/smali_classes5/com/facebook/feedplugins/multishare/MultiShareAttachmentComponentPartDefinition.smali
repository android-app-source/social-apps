.class public Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/3hV;

.field private final f:LX/3hW;

.field private final g:LX/33C;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595909
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3hV;LX/3hW;LX/33C;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595910
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 595911
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->e:LX/3hV;

    .line 595912
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->f:LX/3hW;

    .line 595913
    iput-object p4, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->g:LX/33C;

    .line 595914
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 595915
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595916
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595917
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 595918
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->e:LX/3hV;

    const/4 v2, 0x0

    .line 595919
    new-instance v3, LX/JRR;

    invoke-direct {v3, v1}, LX/JRR;-><init>(LX/3hV;)V

    .line 595920
    sget-object p0, LX/3hV;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JRQ;

    .line 595921
    if-nez p0, :cond_0

    .line 595922
    new-instance p0, LX/JRQ;

    invoke-direct {p0}, LX/JRQ;-><init>()V

    .line 595923
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/JRQ;->a$redex0(LX/JRQ;LX/1De;IILX/JRR;)V

    .line 595924
    move-object v3, p0

    .line 595925
    move-object v2, v3

    .line 595926
    move-object v1, v2

    .line 595927
    iget-object v2, v1, LX/JRQ;->a:LX/JRR;

    iput-object p2, v2, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595928
    iget-object v2, v1, LX/JRQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 595929
    move-object v1, v1

    .line 595930
    iget-object v2, v1, LX/JRQ;->a:LX/JRR;

    iput-object p3, v2, LX/JRR;->a:LX/1Pm;

    .line 595931
    iget-object v2, v1, LX/JRQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 595932
    move-object v1, v1

    .line 595933
    iget-object v2, v1, LX/JRQ;->a:LX/JRR;

    iput-boolean v0, v2, LX/JRR;->c:Z

    .line 595934
    iget-object v2, v1, LX/JRQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 595935
    move-object v0, v1

    .line 595936
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;
    .locals 7

    .prologue
    .line 595937
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 595938
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595939
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595940
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595941
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595942
    new-instance p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3hV;->a(LX/0QB;)LX/3hV;

    move-result-object v4

    check-cast v4, LX/3hV;

    invoke-static {v0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v5

    check-cast v5, LX/3hW;

    invoke-static {v0}, LX/33C;->a(LX/0QB;)LX/33C;

    move-result-object v6

    check-cast v6, LX/33C;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/3hV;LX/3hW;LX/33C;)V

    .line 595943
    move-object v0, p0

    .line 595944
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595945
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595946
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 595948
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 595949
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 11

    .prologue
    .line 595950
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 595951
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595952
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v7

    .line 595953
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595954
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_FIXED_TEXT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v8

    .line 595955
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595956
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v9

    .line 595957
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v1

    move v6, v1

    move v3, v1

    move v4, v1

    :goto_0
    if-ge v5, v10, :cond_6

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595958
    if-nez v4, :cond_0

    invoke-static {v0}, LX/33C;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    move v4, v2

    .line 595959
    :goto_1
    if-nez v3, :cond_1

    invoke-static {v0}, LX/1VO;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    move v3, v2

    .line 595960
    :goto_2
    if-nez v6, :cond_2

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v2

    .line 595961
    :goto_3
    add-int/lit8 v5, v5, 0x1

    move v6, v0

    goto :goto_0

    :cond_3
    move v4, v1

    .line 595962
    goto :goto_1

    :cond_4
    move v3, v1

    .line 595963
    goto :goto_2

    :cond_5
    move v0, v1

    .line 595964
    goto :goto_3

    .line 595965
    :cond_6
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->f:LX/3hW;

    .line 595966
    iget-object v5, v0, LX/3hW;->i:Ljava/lang/Boolean;

    if-nez v5, :cond_7

    .line 595967
    iget-object v5, v0, LX/3hW;->a:LX/0ad;

    sget-short v9, LX/3hX;->f:S

    const/4 v10, 0x0

    invoke-interface {v5, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v0, LX/3hW;->i:Ljava/lang/Boolean;

    .line 595968
    :cond_7
    iget-object v5, v0, LX/3hW;->i:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move v0, v5

    .line 595969
    if-eqz v0, :cond_8

    if-eqz v7, :cond_8

    if-nez v8, :cond_8

    if-nez v4, :cond_8

    if-nez v3, :cond_8

    if-nez v6, :cond_8

    :goto_4
    return v2

    :cond_8
    move v2, v1

    goto :goto_4
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 595970
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595971
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 595972
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
