.class public Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3iA;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hq;",
        "LX/0Px",
        "<",
        "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
        ">;TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field private final c:LX/1Ad;

.field public final d:LX/3hm;

.field private final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 628027
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 628028
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/3hm;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628021
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 628022
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->c:LX/1Ad;

    .line 628023
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->c:LX/1Ad;

    sget-object v1, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 628024
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->d:LX/3hm;

    .line 628025
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->e:LX/03V;

    .line 628026
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;
    .locals 7

    .prologue
    .line 627980
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    monitor-enter v1

    .line 627981
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627982
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627983
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627984
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 627985
    new-instance v6, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    .line 627986
    new-instance p0, LX/3hm;

    invoke-static {v0}, LX/1AY;->b(LX/0QB;)LX/1AY;

    move-result-object v4

    check-cast v4, LX/1AY;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-direct {p0, v4, v5}, LX/3hm;-><init>(LX/1AY;Landroid/os/Handler;)V

    .line 627987
    move-object v4, p0

    .line 627988
    check-cast v4, LX/3hm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;-><init>(LX/1Ad;LX/3hm;LX/03V;)V

    .line 627989
    move-object v0, v6

    .line 627990
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627991
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627992
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627993
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 628006
    check-cast p2, LX/3hq;

    check-cast p3, LX/1Po;

    .line 628007
    iget-object v0, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 628008
    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 628009
    iget v1, p2, LX/3hq;->c:I

    move v1, v1

    .line 628010
    invoke-static {v0, v1}, LX/3hv;->b(Lcom/facebook/graphql/model/GraphQLStory;I)LX/0Px;

    move-result-object v3

    .line 628011
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 628012
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628013
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".prepare"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Cannot use this class without non-empty slideshow images"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 628014
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 628015
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 628016
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 628017
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->c:LX/1Ad;

    invoke-virtual {v1, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 628018
    if-lez v2, :cond_3

    move-object v1, p3

    .line 628019
    check-cast v1, LX/1Pt;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    sget-object v5, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v0, v5}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 628020
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5366d13b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627998
    check-cast p2, LX/0Px;

    .line 627999
    move-object v1, p4

    check-cast v1, LX/3iA;

    invoke-interface {v1, p2}, LX/3iA;->setupSlideshow(LX/0Px;)V

    .line 628000
    iget-object v1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->d:LX/3hm;

    .line 628001
    iget-object p0, v1, LX/3hm;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    .line 628002
    iget-object p2, v1, LX/3hm;->c:Ljava/util/ArrayList;

    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628003
    if-eqz p0, :cond_0

    .line 628004
    invoke-static {v1}, LX/3hm;->b(LX/3hm;)V

    .line 628005
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x25e18b3b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 627994
    move-object v0, p4

    check-cast v0, LX/3iA;

    invoke-interface {v0}, LX/3iA;->b()V

    .line 627995
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowPartDefinition;->d:LX/3hm;

    .line 627996
    iget-object p0, v0, LX/3hm;->c:Ljava/util/ArrayList;

    invoke-virtual {p0, p4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 627997
    return-void
.end method
