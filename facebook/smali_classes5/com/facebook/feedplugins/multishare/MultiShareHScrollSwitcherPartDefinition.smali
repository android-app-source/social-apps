.class public Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3hd;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627039
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 627040
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;
    .locals 3

    .prologue
    .line 627041
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    monitor-enter v1

    .line 627042
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 627043
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 627044
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627045
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 627046
    new-instance v0, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;-><init>()V

    .line 627047
    move-object v0, v0

    .line 627048
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 627049
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareHScrollSwitcherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627050
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 627051
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5dd2675a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 627052
    check-cast p1, LX/3hd;

    check-cast p3, LX/1Pq;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 627053
    iput-object p4, p1, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 627054
    iput-object p3, p1, LX/3hd;->b:LX/1Pq;

    .line 627055
    const/16 v1, 0x1f

    const v2, 0x4933e520    # 736850.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 627056
    check-cast p1, LX/3hd;

    const/4 v0, 0x0

    .line 627057
    iput-object v0, p1, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 627058
    iput-object v0, p1, LX/3hd;->b:LX/1Pq;

    .line 627059
    return-void
.end method
