.class public Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/DDI;

.field private final f:LX/1V0;

.field private final g:LX/DD3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594931
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DDI;LX/1V0;LX/DD3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594932
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 594933
    iput-object p2, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->e:LX/DDI;

    .line 594934
    iput-object p3, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->f:LX/1V0;

    .line 594935
    iput-object p4, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->g:LX/DD3;

    .line 594936
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 594937
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->e:LX/DDI;

    const/4 v1, 0x0

    .line 594938
    new-instance v2, LX/DDH;

    invoke-direct {v2, v0}, LX/DDH;-><init>(LX/DDI;)V

    .line 594939
    iget-object v3, v0, LX/DDI;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DDG;

    .line 594940
    if-nez v3, :cond_0

    .line 594941
    new-instance v3, LX/DDG;

    invoke-direct {v3, v0}, LX/DDG;-><init>(LX/DDI;)V

    .line 594942
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DDG;->a$redex0(LX/DDG;LX/1De;IILX/DDH;)V

    .line 594943
    move-object v2, v3

    .line 594944
    move-object v1, v2

    .line 594945
    move-object v1, v1

    .line 594946
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 594947
    iget-object v2, v1, LX/DDG;->a:LX/DDH;

    iput-object v0, v2, LX/DDH;->b:LX/1Po;

    .line 594948
    iget-object v2, v1, LX/DDG;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 594949
    move-object v0, v1

    .line 594950
    iget-object v1, v0, LX/DDG;->a:LX/DDH;

    iput-object p2, v1, LX/DDH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594951
    iget-object v1, v0, LX/DDG;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 594952
    move-object v0, v0

    .line 594953
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 594954
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 594955
    iget-object v2, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;
    .locals 7

    .prologue
    .line 594956
    const-class v1, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;

    monitor-enter v1

    .line 594957
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594958
    sput-object v2, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594961
    new-instance p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DDI;->a(LX/0QB;)LX/DDI;

    move-result-object v4

    check-cast v4, LX/DDI;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/DD3;->b(LX/0QB;)LX/DD3;

    move-result-object v6

    check-cast v6, LX/DD3;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;-><init>(Landroid/content/Context;LX/DDI;LX/1V0;LX/DD3;)V

    .line 594962
    move-object v0, p0

    .line 594963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 594967
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 594968
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594969
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 594970
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594971
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->g:LX/DD3;

    invoke-virtual {v0}, LX/DD3;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 594972
    :goto_0
    return v0

    .line 594973
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594974
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 594975
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594976
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 594977
    goto :goto_0

    .line 594978
    :cond_3
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594979
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->k()LX/0Px;

    move-result-object v0

    .line 594980
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 594981
    goto :goto_0

    .line 594982
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 594983
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594984
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594985
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 594986
    sget-object v0, Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
