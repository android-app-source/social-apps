.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
        ">;",
        "LX/JUC;",
        "LX/1Ps;",
        "LX/JUL;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596221
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->b:LX/1Ua;

    .line 596222
    new-instance v0, LX/3YW;

    invoke-direct {v0}, LX/3YW;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596218
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596219
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 596220
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;
    .locals 4

    .prologue
    .line 596207
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    monitor-enter v1

    .line 596208
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596209
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596212
    new-instance p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 596213
    move-object v0, p0

    .line 596214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/JUC;LX/JUL;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JUC;",
            "LX/JUL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 596193
    iget v0, p0, LX/JUC;->a:I

    .line 596194
    const v2, 0x7f02093c

    if-ne v0, v2, :cond_1

    const v5, 0x7f0a0590

    .line 596195
    :goto_0
    new-instance v2, LX/JUM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v6, 0x18

    const/4 v7, 0x4

    move v4, v0

    invoke-direct/range {v2 .. v7}, LX/JUM;-><init>(Landroid/content/Context;IIII)V

    move-object v0, v2

    .line 596196
    iget-object v1, p1, LX/JUL;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 596197
    iget-object v0, p0, LX/JUC;->b:Ljava/lang/String;

    iget-object v1, p0, LX/JUC;->c:Ljava/lang/String;

    const/16 p0, 0x21

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 596198
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 596199
    :cond_0
    :goto_1
    return-void

    .line 596200
    :cond_1
    const v5, 0x7f0a00d2

    goto :goto_0

    .line 596201
    :cond_2
    invoke-virtual {p1}, LX/JUL;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 596202
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 596203
    new-instance v4, LX/JUK;

    const v5, 0x7f0a0158

    invoke-static {v2, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v4, v8, v5}, LX/JUK;-><init>(II)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v7, v5, p0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 596204
    new-instance v4, Landroid/text/SpannableString;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " \u2022 "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 596205
    new-instance v5, LX/JUK;

    const v6, 0x7f0a015f

    invoke-static {v2, v6}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v5, v7, v2}, LX/JUK;-><init>(II)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v4, v5, v7, v2, p0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 596206
    iget-object v2, p1, LX/JUL;->b:Landroid/widget/TextView;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/CharSequence;

    aput-object v3, v5, v7

    aput-object v4, v5, v8

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596183
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 596186
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596187
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596188
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 596189
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596190
    new-instance v2, LX/JUC;

    .line 596191
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;->TRAVELING:Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    if-ne v1, v3, :cond_2

    const v1, 0x7f02072c

    :goto_0
    move v3, v1

    .line 596192
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, ""

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v0, ""

    :goto_2
    invoke-direct {v2, v3, v1, v0}, LX/JUC;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    const v1, 0x7f02093c

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xd45b805

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596185
    check-cast p2, LX/JUC;

    check-cast p4, LX/JUL;

    invoke-static {p2, p4}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->a(LX/JUC;LX/JUL;)V

    const/16 v1, 0x1f

    const v2, -0x5188d284

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596184
    const/4 v0, 0x1

    return v0
.end method
