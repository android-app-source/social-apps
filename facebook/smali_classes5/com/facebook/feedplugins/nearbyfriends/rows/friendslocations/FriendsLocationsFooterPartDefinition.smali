.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/String;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final g:LX/JUO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 596241
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    const-string v1, "feed_friends_locations_see_all"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->b:Ljava/lang/String;

    .line 596242
    new-instance v0, LX/3YX;

    invoke-direct {v0}, LX/3YX;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/JUO;)V
    .locals 0
    .param p4    # Lcom/facebook/intent/feed/IFeedIntentBuilder;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596234
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596235
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 596236
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 596237
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 596238
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 596239
    iput-object p5, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->g:LX/JUO;

    .line 596240
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;
    .locals 9

    .prologue
    .line 596243
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    monitor-enter v1

    .line 596244
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596245
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596246
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596247
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596248
    new-instance v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v7

    invoke-static {v7}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v8

    check-cast v8, LX/JUO;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/JUO;)V

    .line 596249
    move-object v0, v3

    .line 596250
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596251
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596252
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596233
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 596226
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    const/4 v3, 0x0

    .line 596227
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 596228
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596229
    const v1, 0x7f0d12e0

    iget-object v2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 596230
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/JUB;

    invoke-direct {v1, p0, p2}, LX/JUB;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596231
    return-object v3

    .line 596232
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596225
    const/4 v0, 0x1

    return v0
.end method
