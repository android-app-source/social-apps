.class public Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Byp;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/1V7;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 590063
    new-instance v0, LX/3WI;

    invoke-direct {v0}, LX/3WI;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a:LX/1Cz;

    .line 590064
    const-class v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590056
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590057
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590058
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->d:LX/1V7;

    .line 590059
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 590060
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->f:Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    .line 590061
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 590062
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;
    .locals 9

    .prologue
    .line 590045
    const-class v1, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    monitor-enter v1

    .line 590046
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590047
    sput-object v2, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590048
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590049
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590050
    new-instance v3, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V7;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V

    .line 590051
    move-object v0, v3

    .line 590052
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590053
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590054
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPage;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 590065
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Byp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590044
    sget-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 590023
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590024
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590025
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 590026
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->d:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->d()F

    move-result v2

    neg-float v2, v2

    .line 590027
    iput v2, v1, LX/1UY;->b:F

    .line 590028
    move-object v1, v1

    .line 590029
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->d:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->e()F

    move-result v2

    neg-float v2, v2

    .line 590030
    iput v2, v1, LX/1UY;->c:F

    .line 590031
    move-object v1, v1

    .line 590032
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    .line 590033
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    const v5, 0x7f021884

    const/4 v6, -0x1

    invoke-direct {v3, v4, v1, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590034
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    .line 590035
    new-instance v1, LX/Byl;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->k()I

    move-result v4

    sget-object v5, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3, v4, v5}, LX/Byl;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 590036
    new-instance v2, LX/Byl;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPage;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->n()I

    move-result v5

    sget-object v6, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4, v5, v6}, LX/Byl;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 590037
    const v3, 0x7f0d2d53

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v5, v1, LX/Byl;->a:Ljava/lang/String;

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590038
    const v3, 0x7f0d2d54

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v5, v2, LX/Byl;->a:Ljava/lang/String;

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590039
    const v3, 0x7f0d1913

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->q()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590040
    const v0, 0x7f0d2d55

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->f:Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    invoke-interface {p1, v0, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590041
    const v0, 0x7f0d2d57

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->f:Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590042
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590043
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1e0b2839

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590015
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/Byp;

    .line 590016
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 590017
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 590018
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->l()Ljava/lang/String;

    move-result-object v1

    .line 590019
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 590020
    :cond_0
    iget-object v2, p4, LX/Byp;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 p1, 0x8

    invoke-virtual {v2, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 590021
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6ccb285e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 590022
    :cond_1
    iget-object v2, p4, LX/Byp;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 590011
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590012
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590013
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 590014
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->m()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
