.class public Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1RB;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/ByG;",
        "TE;",
        "LX/3Wh;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/1RB",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/1V7;

.field public final d:LX/38w;

.field private final e:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition",
            "<",
            "LX/3Wh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 591260
    const-class v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/38w;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591208
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 591209
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 591210
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->c:LX/1V7;

    .line 591211
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->d:LX/38w;

    .line 591212
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->e:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    .line 591213
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;
    .locals 7

    .prologue
    .line 591249
    const-class v1, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    monitor-enter v1

    .line 591250
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591251
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591252
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591253
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591254
    new-instance p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/38w;->b(LX/0QB;)LX/38w;

    move-result-object v5

    check-cast v5, LX/38w;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/38w;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V

    .line 591255
    move-object v0, p0

    .line 591256
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591257
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591258
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3Wh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591248
    sget-object v0, LX/3Wh;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 591231
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591232
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591233
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591234
    invoke-static {p2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 591235
    invoke-static {v1}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    move v2, v1

    .line 591236
    if-nez v2, :cond_0

    .line 591237
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->c:LX/1V7;

    invoke-virtual {v5}, LX/1V7;->k()LX/1Ua;

    move-result-object v5

    const v6, 0x7f020ae9

    const/4 v7, -0x1

    invoke-direct {v3, v4, v5, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591238
    :cond_0
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 591239
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 591240
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 591241
    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 591242
    :goto_0
    move-object v0, v3

    .line 591243
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->e:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    new-instance v4, LX/36Q;

    sget-object v5, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v6, 0x3ff745d1

    invoke-direct {v4, v0, v5, v6}, LX/36Q;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;F)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 591244
    new-instance v3, LX/ByG;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-direct {v3, v2, v0}, LX/ByG;-><init>(ZLjava/lang/String;)V

    return-object v3

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 591245
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-static {v3}, LX/4Za;->b(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 591246
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 591247
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-static {v3}, LX/4Za;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4b413e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 591221
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/ByG;

    check-cast p3, LX/1Pq;

    check-cast p4, LX/3Wh;

    .line 591222
    invoke-virtual {p4}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->e()V

    .line 591223
    const/4 v4, 0x1

    .line 591224
    iput-boolean v4, p4, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->c:Z

    .line 591225
    iget-boolean v4, p2, LX/ByG;->a:Z

    if-nez v4, :cond_0

    .line 591226
    const/4 v8, 0x0

    .line 591227
    iget-object v4, p4, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v4, v4

    .line 591228
    iget-object v5, p4, LX/3Wh;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0035

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v6, p4, LX/3Wh;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0035

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-virtual {v4, v5, v8, v6, v8}, Lcom/facebook/drawee/view/DraweeView;->setPadding(IIII)V

    .line 591229
    :cond_0
    new-instance v4, LX/ByF;

    move-object v5, p0

    move-object v6, p4

    move-object v7, p3

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v4 .. v9}, LX/ByF;-><init>(Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;LX/3Wh;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/ByG;)V

    invoke-virtual {p4, v4}, LX/3Wh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591230
    const/16 v1, 0x1f

    const v2, 0x31edb863

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591218
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591219
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591220
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 591214
    check-cast p4, LX/3Wh;

    .line 591215
    invoke-virtual {p4}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->a()V

    .line 591216
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/3Wh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591217
    return-void
.end method
