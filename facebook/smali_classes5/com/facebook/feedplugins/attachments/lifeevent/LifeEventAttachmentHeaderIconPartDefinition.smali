.class public Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/BzI;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 588658
    const-class v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 588659
    new-instance v0, LX/3Vp;

    invoke-direct {v0}, LX/3Vp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588653
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588654
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588655
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 588656
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->e:LX/03V;

    .line 588657
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;
    .locals 6

    .prologue
    .line 588642
    const-class v1, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    monitor-enter v1

    .line 588643
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588644
    sput-object v2, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588645
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588646
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588647
    new-instance p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/03V;)V

    .line 588648
    move-object v0, p0

    .line 588649
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588650
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588651
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588652
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588641
    sget-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 588626
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588627
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588628
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 588629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_1

    .line 588630
    :cond_0
    const-string v1, "Icon"

    invoke-static {v0, v1}, LX/BzD;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 588631
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 588632
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->e:LX/03V;

    const-string v3, "life_event"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "experience target/icon missing: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 588633
    :goto_1
    const v1, 0x7f0d0aa1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v0

    sget-object v3, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 588634
    iput-object v3, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 588635
    move-object v0, v0

    .line 588636
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 588637
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588638
    const/4 v0, 0x0

    return-object v0

    .line 588639
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588640
    const/4 v0, 0x1

    return v0
.end method
