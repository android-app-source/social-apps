.class public Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1aZ;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:LX/1V7;

.field private final f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 590009
    const v0, 0x7f03139f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->a:LX/1Cz;

    .line 590010
    const-class v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    const-string v1, "photo"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/1V7;",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590003
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590004
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->c:LX/0Or;

    .line 590005
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590006
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->e:LX/1V7;

    .line 590007
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 590008
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;
    .locals 7

    .prologue
    .line 589972
    const-class v1, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    monitor-enter v1

    .line 589973
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 589974
    sput-object v2, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589975
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589976
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 589977
    new-instance v6, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-direct {v6, p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;-><init>(LX/0Or;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V

    .line 589978
    move-object v0, v6

    .line 589979
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 589980
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589981
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 589982
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590002
    sget-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 589991
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589992
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589993
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 589994
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->e:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->e()F

    move-result v2

    neg-float v2, v2

    .line 589995
    iput v2, v1, LX/1UY;->c:F

    .line 589996
    move-object v1, v1

    .line 589997
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    .line 589998
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-direct {v3, v4, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 589999
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590000
    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 590001
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xeaaba08

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 589987
    check-cast p2, LX/1aZ;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 589988
    invoke-virtual {p4, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 589989
    const v1, 0x3ff745d1

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 589990
    const/16 v1, 0x1f

    const v2, 0x1ce42610

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 589983
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 589984
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 589985
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 589986
    invoke-static {v0}, LX/1VO;->s(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
