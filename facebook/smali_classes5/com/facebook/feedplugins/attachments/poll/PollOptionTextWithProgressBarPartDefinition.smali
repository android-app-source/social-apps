.class public Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C1I;",
        "LX/C1J;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592861
    new-instance v0, LX/3XN;

    invoke-direct {v0}, LX/3XN;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592881
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592882
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 592883
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;
    .locals 4

    .prologue
    .line 592870
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    monitor-enter v1

    .line 592871
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592872
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592873
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592874
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592875
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 592876
    move-object v0, p0

    .line 592877
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592878
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592879
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592880
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592884
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 592867
    check-cast p2, LX/C1I;

    .line 592868
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/C1I;->c:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592869
    new-instance v0, LX/C1J;

    iget-object v1, p2, LX/C1I;->d:Ljava/lang/String;

    iget v2, p2, LX/C1I;->a:I

    iget v3, p2, LX/C1I;->b:I

    invoke-direct {v0, v1, v2, v3}, LX/C1J;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2623416b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592863
    check-cast p2, LX/C1J;

    check-cast p4, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;

    .line 592864
    iget-object v1, p2, LX/C1J;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;->setText(Ljava/lang/CharSequence;)V

    .line 592865
    iget v1, p2, LX/C1J;->c:I

    int-to-float v1, v1

    iget v2, p2, LX/C1J;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;->setProgress(F)V

    .line 592866
    const/16 v1, 0x1f

    const v2, 0x4df7748c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592862
    const/4 v0, 0x1

    return v0
.end method
