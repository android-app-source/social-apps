.class public Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C1H;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:I

.field public static final c:I

.field private static final d:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field private final e:Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

.field private final i:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590161
    const v0, 0x7f030fe7

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->a:LX/1Cz;

    .line 590162
    const-class v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->d:Ljava/lang/String;

    .line 590163
    const v0, 0x7f0207d5

    sput v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->b:I

    .line 590164
    const v0, 0x7f0209a0

    sput v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590165
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590166
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590167
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->e:Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    .line 590168
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->f:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 590169
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->h:Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    .line 590170
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->i:LX/03V;

    .line 590171
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;
    .locals 9

    .prologue
    .line 590172
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    monitor-enter v1

    .line 590173
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590174
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590177
    new-instance v3, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;LX/03V;)V

    .line 590178
    move-object v0, v3

    .line 590179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590183
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 590184
    check-cast p2, LX/C1H;

    const/4 v2, 0x0

    .line 590185
    iget-boolean v0, p2, LX/C1H;->a:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/1Ua;->a:LX/1Ua;

    .line 590186
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    iget-object v4, p2, LX/C1H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-direct {v3, v4, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590187
    iget-object v0, p2, LX/C1H;->g:LX/C1E;

    sget-object v1, LX/C1E;->CHOOSE_MULTIPLE:LX/C1E;

    invoke-virtual {v0, v1}, LX/C1E;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->b:I

    .line 590188
    :goto_1
    const v1, 0x7f0d2659

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->e:Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    new-instance v4, LX/C1X;

    iget-object v5, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v5

    iget-boolean v6, p2, LX/C1H;->h:Z

    iget-object v7, p2, LX/C1H;->e:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v0, v5, v6, v7}, LX/C1X;-><init>(IZZLandroid/view/View$OnClickListener;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590189
    const/4 v0, 0x0

    .line 590190
    iget-object v1, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    if-eqz v1, :cond_0

    iget-object v1, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 590191
    iget-object v0, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v0

    .line 590192
    :cond_0
    const v1, 0x7f0d265a

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->h:Lcom/facebook/feedplugins/attachments/poll/PollOptionTextWithProgressBarPartDefinition;

    new-instance v4, LX/C1I;

    iget v5, p2, LX/C1H;->d:I

    iget-object v6, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p2, LX/C1H;->e:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, LX/C1I;-><init>(IILjava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590193
    iget-object v1, p2, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    const/4 v3, 0x0

    .line 590194
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 590195
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->j()LX/0Px;

    move-result-object v4

    if-nez v4, :cond_5

    .line 590196
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 590197
    :goto_2
    move-object v3, v3

    .line 590198
    if-nez v0, :cond_4

    move-object v1, v2

    .line 590199
    :goto_3
    const v4, 0x7f0d265b

    iget-object v5, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->f:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    new-instance v6, LX/8Cj;

    invoke-direct {v6, v3, v1, v0}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;I)V

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590200
    return-object v2

    .line 590201
    :cond_2
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ec00000    # -12.0f

    .line 590202
    iput v1, v0, LX/1UY;->b:F

    .line 590203
    move-object v0, v0

    .line 590204
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    goto/16 :goto_0

    .line 590205
    :cond_3
    sget v0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->c:I

    goto/16 :goto_1

    .line 590206
    :cond_4
    iget-object v1, p2, LX/C1H;->f:Landroid/view/View$OnClickListener;

    goto :goto_3

    .line 590207
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    move v4, v3

    :goto_4
    if-ge v5, v8, :cond_7

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLUser;

    .line 590208
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->C()Z

    move-result p3

    if-eqz p3, :cond_8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    if-eqz p3, :cond_8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_8

    .line 590209
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 590210
    add-int/lit8 v3, v4, 0x1

    .line 590211
    const/4 v4, 0x3

    if-eq v0, v4, :cond_6

    .line 590212
    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    .line 590213
    :cond_6
    :goto_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_4

    .line 590214
    :cond_7
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto :goto_2

    :cond_8
    move v3, v4

    goto :goto_5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 590215
    check-cast p1, LX/C1H;

    .line 590216
    iget-object v0, p1, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    .line 590217
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->i:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TextWithEntities is null for poll "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 590218
    const/4 v0, 0x0

    .line 590219
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
