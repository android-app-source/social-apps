.class public Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C1X;",
        "LX/C1Y;",
        "LX/1PW;",
        "Landroid/widget/RadioButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590245
    new-instance v0, LX/3WO;

    invoke-direct {v0}, LX/3WO;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590220
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590221
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 590222
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;
    .locals 4

    .prologue
    .line 590234
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    monitor-enter v1

    .line 590235
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590236
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590237
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590238
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590239
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 590240
    move-object v0, p0

    .line 590241
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590242
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590243
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590244
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/RadioButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590246
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 590231
    check-cast p2, LX/C1X;

    .line 590232
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/C1X;->d:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590233
    new-instance v0, LX/C1Y;

    iget v1, p2, LX/C1X;->a:I

    iget-boolean v2, p2, LX/C1X;->b:Z

    iget-boolean v3, p2, LX/C1X;->c:Z

    invoke-direct {v0, v1, v2, v3}, LX/C1Y;-><init>(IZZ)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7fff5c96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 590224
    check-cast p2, LX/C1Y;

    check-cast p4, Landroid/widget/RadioButton;

    .line 590225
    iget-boolean v1, p2, LX/C1Y;->c:Z

    if-eqz v1, :cond_0

    .line 590226
    iget-boolean v1, p2, LX/C1Y;->b:Z

    invoke-virtual {p4, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 590227
    iget v1, p2, LX/C1Y;->a:I

    invoke-virtual {p4, v1}, Landroid/widget/RadioButton;->setButtonDrawable(I)V

    .line 590228
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 590229
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x6733af7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 590230
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590223
    const/4 v0, 0x1

    return v0
.end method
