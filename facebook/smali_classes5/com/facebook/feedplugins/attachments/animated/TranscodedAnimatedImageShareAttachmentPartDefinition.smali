.class public Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1PV;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JMh;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final g:LX/17R;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592842
    sget-object v0, LX/1Ua;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->b:LX/1Ua;

    .line 592843
    new-instance v0, LX/3XL;

    invoke-direct {v0}, LX/3XL;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/17R;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/gif/annotations/IsInAnimatedImageVideosBlacklistGateKeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            "Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;",
            "LX/17R;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592834
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592835
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 592836
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 592837
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 592838
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 592839
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->g:LX/17R;

    .line 592840
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->h:LX/0Or;

    .line 592841
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 592823
    const-class v1, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;

    monitor-enter v1

    .line 592824
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592825
    sput-object v2, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592826
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592827
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592828
    new-instance v3, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v8

    check-cast v8, LX/17R;

    const/16 v9, 0x31b

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;LX/17R;LX/0Or;)V

    .line 592829
    move-object v0, v3

    .line 592830
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592831
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592832
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JMh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592803
    sget-object v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 592810
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    const/4 v7, -0x1

    .line 592811
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592812
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 592813
    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 592814
    :goto_0
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v3, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 592815
    const v1, 0x7f0d2fae

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v4, LX/3EE;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v4, p2, v7, v5, v6}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592816
    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 592817
    :goto_1
    const v3, 0x7f0d16ec

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v3, v4, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592818
    invoke-static {v0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 592819
    const v0, 0x7f0d16ec

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v3, LX/2ya;

    invoke-direct {v3, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592820
    :cond_0
    return-object v2

    .line 592821
    :cond_1
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->b:LX/1Ua;

    const v5, 0x7f020a3c

    invoke-direct {v1, v3, v4, v5, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 592822
    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 592805
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592806
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 592807
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 592808
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    move v1, v1

    .line 592809
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592804
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
