.class public Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/ByP;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/17R;

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588829
    sget-object v0, LX/1Ua;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->b:LX/1Ua;

    .line 588830
    new-instance v0, LX/3Vu;

    invoke-direct {v0}, LX/3Vu;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17R;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588821
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588822
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588823
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->d:LX/17R;

    .line 588824
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 588825
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 588826
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    .line 588827
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    .line 588828
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 588810
    const-class v1, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;

    monitor-enter v1

    .line 588811
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588812
    sput-object v2, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588813
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588814
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588815
    new-instance v3, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v5

    check-cast v5, LX/17R;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17R;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;)V

    .line 588816
    move-object v0, v3

    .line 588817
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588818
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588819
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 588807
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588808
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 588809
    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588806
    sget-object v0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 588794
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588795
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588796
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 588797
    const v1, 0x7f0d16ec

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 588798
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->b:LX/1Ua;

    const v5, 0x7f020a3c

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588799
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588800
    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 588801
    if-nez v1, :cond_0

    .line 588802
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588803
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 588804
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588805
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
