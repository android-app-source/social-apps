.class public Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/DCb;",
        "TE;",
        "LX/DCc;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/1Ad;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0yc;

.field private final h:LX/26D;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 592780
    const-class v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;

    const-string v1, "newsfeed_story_attachment_photo_grid_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 592781
    new-instance v0, LX/3XJ;

    invoke-direct {v0}, LX/3XJ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0yc;LX/26D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/0yc;",
            "LX/26D;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592772
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 592773
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->c:LX/1Ad;

    .line 592774
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->d:LX/0Ot;

    .line 592775
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->e:LX/0Ot;

    .line 592776
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->f:LX/0Ot;

    .line 592777
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->g:LX/0yc;

    .line 592778
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->h:LX/26D;

    .line 592779
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 592761
    const-class v1, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;

    monitor-enter v1

    .line 592762
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592763
    sput-object v2, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592766
    new-instance v3, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    const/16 v5, 0x7f5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa59

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x848

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    invoke-static {v0}, LX/26D;->a(LX/0QB;)LX/26D;

    move-result-object v9

    check-cast v9, LX/26D;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;-><init>(LX/1Ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0yc;LX/26D;)V

    .line 592767
    move-object v0, v3

    .line 592768
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592769
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592770
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DCc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592782
    sget-object v0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 592755
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592756
    const v1, 0x7f0d1cc1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592757
    new-instance v1, LX/DCb;

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->h:LX/26D;

    invoke-virtual {v0, p2}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->c:LX/1Ad;

    sget-object v3, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/DCb;-><init>(ILX/1aZ;)V

    .line 592758
    const v2, 0x7f0d00fe

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->h:LX/26D;

    iget v4, v1, LX/DCb;->b:I

    invoke-virtual {v3, p2, v4}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/3EE;

    move-result-object v3

    invoke-interface {p1, v2, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592759
    const v2, 0x7f0d00fe

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/3FZ;

    new-instance v4, LX/DCa;

    invoke-direct {v4, p0, v1}, LX/DCa;-><init>(Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;LX/DCb;)V

    invoke-direct {v3, v4}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v2, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 592760
    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6f76f5bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 592736
    check-cast p2, LX/DCb;

    check-cast p4, LX/DCc;

    .line 592737
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->g:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    invoke-virtual {p4, v1}, LX/DCc;->setDialtoneEnabled(Z)V

    .line 592738
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->g:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p2, LX/DCb;->a:LX/1aZ;

    instance-of v1, v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v1, :cond_0

    .line 592739
    iget-object v1, p2, LX/DCb;->a:LX/1aZ;

    check-cast v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    const/4 v2, 0x0

    sget-object p1, LX/397;->VIDEO:LX/397;

    .line 592740
    invoke-static {v1, v2, p1}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 592741
    iget-object v1, p2, LX/DCb;->a:LX/1aZ;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->g:LX/0yc;

    const/4 p0, 0x1

    .line 592742
    invoke-virtual {p4, p0}, LX/DCc;->setDialtoneEnabled(Z)V

    move-object p1, v1

    .line 592743
    check-cast p1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    .line 592744
    iput-boolean p0, p1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    .line 592745
    iget-object p1, p4, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const/16 p0, 0x8

    invoke-virtual {p1, p0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setVisibility(I)V

    .line 592746
    iput-object v2, p4, LX/DCc;->a:LX/0yc;

    .line 592747
    iget-object p1, p4, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 592748
    iget-object p1, p4, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object p0, p4, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 592749
    iget-object v2, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->p:LX/26O;

    move-object p0, v2

    .line 592750
    invoke-virtual {p1, p0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;LX/1aZ;)V

    .line 592751
    iget-object p1, p4, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    const/4 p0, 0x0

    .line 592752
    iput-boolean p0, p1, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->w:Z

    .line 592753
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x78cc1672

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 592754
    :cond_0
    iget v1, p2, LX/DCb;->b:I

    invoke-virtual {p4, v1}, LX/DCc;->setVideoIndex(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 592734
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592735
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->h:LX/26D;

    invoke-virtual {v0, p1}, LX/26D;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/multimedia/MultimediaSinglePlayerAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 592731
    check-cast p4, LX/DCc;

    .line 592732
    const/4 v0, -0x1

    invoke-virtual {p4, v0}, LX/DCc;->setVideoIndex(I)V

    .line 592733
    return-void
.end method
