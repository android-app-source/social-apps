.class public Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final e:LX/C1a;

.field private final f:LX/1V0;

.field private final g:LX/1V7;

.field private final h:LX/By7;

.field private final i:LX/2mt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591076
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C1a;LX/1V0;LX/1V7;LX/By7;LX/2mt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591091
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 591092
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->e:LX/C1a;

    .line 591093
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->f:LX/1V0;

    .line 591094
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->g:LX/1V7;

    .line 591095
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->h:LX/By7;

    .line 591096
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->i:LX/2mt;

    .line 591097
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 591106
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 591107
    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->g:LX/1V7;

    invoke-virtual {v3}, LX/1V7;->h()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 591108
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->h:LX/By7;

    invoke-virtual {v0, p1}, LX/By7;->c(LX/1De;)LX/By5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/By5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/By5;

    move-result-object v3

    if-eqz v1, :cond_0

    const v0, 0x7f020ae3

    :goto_0
    invoke-virtual {v3, v0}, LX/By5;->h(I)LX/By5;

    move-result-object v3

    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-virtual {v3, v0}, LX/By5;->a(LX/1Pq;)LX/By5;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->i:LX/2mt;

    invoke-virtual {v3, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/By5;->b(Ljava/lang/String;)LX/By5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 591109
    if-nez v1, :cond_1

    .line 591110
    :goto_1
    return-object v0

    .line 591111
    :cond_0
    const v0, 0x7f0a00d5

    goto :goto_0

    .line 591112
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;
    .locals 3

    .prologue
    .line 591098
    const-class v1, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    monitor-enter v1

    .line 591099
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591100
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591103
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591104
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;
    .locals 7

    .prologue
    .line 591082
    new-instance v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/C1a;->a(LX/0QB;)LX/C1a;

    move-result-object v2

    check-cast v2, LX/C1a;

    invoke-static {p0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v3

    check-cast v3, LX/1V0;

    invoke-static {p0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {p0}, LX/By7;->a(LX/0QB;)LX/By7;

    move-result-object v5

    check-cast v5, LX/By7;

    invoke-static {p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v6

    check-cast v6, LX/2mt;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;-><init>(Landroid/content/Context;LX/C1a;LX/1V0;LX/1V7;LX/By7;LX/2mt;)V

    .line 591083
    return-object v0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 591084
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591085
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591086
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Bnh;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 591087
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->v()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 591088
    :goto_0
    return v0

    .line 591089
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591090
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->e:LX/C1a;

    invoke-virtual {v0}, LX/C1a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 591081
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 591080
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591079
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 591078
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 591077
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
