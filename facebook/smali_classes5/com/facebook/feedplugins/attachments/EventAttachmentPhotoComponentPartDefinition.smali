.class public Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field private final f:LX/2mt;

.field private final g:LX/ByD;

.field private final h:LX/1Ad;

.field private final i:LX/1V0;

.field private final j:LX/C1a;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 591113
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->d:LX/1Cz;

    .line 591114
    const-class v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2mt;LX/ByD;LX/1Ad;LX/1V0;LX/C1a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591115
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 591116
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->f:LX/2mt;

    .line 591117
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->g:LX/ByD;

    .line 591118
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->h:LX/1Ad;

    .line 591119
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->i:LX/1V0;

    .line 591120
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->j:LX/C1a;

    .line 591121
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 591122
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591123
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 591124
    sget-object v1, LX/1Ua;->c:LX/1Ua;

    .line 591125
    iget-object v2, v1, LX/1Ua;->t:LX/1Ub;

    move-object v2, v2

    .line 591126
    new-instance v3, LX/1UY;

    iget-object v4, v1, LX/1Ua;->s:LX/1UZ;

    invoke-direct {v3, v4}, LX/1UY;-><init>(LX/1UZ;)V

    .line 591127
    iget v4, v2, LX/1Ub;->a:F

    move v4, v4

    .line 591128
    iput v4, v3, LX/1UY;->b:F

    .line 591129
    move-object v3, v3

    .line 591130
    iget v4, v2, LX/1Ub;->b:F

    move v4, v4

    .line 591131
    iput v4, v3, LX/1UY;->c:F

    .line 591132
    move-object v3, v3

    .line 591133
    iget-object v4, v2, LX/1Ub;->d:LX/1Uc;

    move-object v4, v4

    .line 591134
    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Uc;->a(I)F

    move-result v4

    .line 591135
    iput v4, v3, LX/1UY;->d:F

    .line 591136
    move-object v3, v3

    .line 591137
    iget v4, v2, LX/1Ub;->c:F

    move v2, v4

    .line 591138
    iput v2, v3, LX/1UY;->e:F

    .line 591139
    move-object v2, v3

    .line 591140
    iget-boolean v3, v1, LX/1Ua;->u:Z

    move v3, v3

    .line 591141
    if-eqz v3, :cond_0

    .line 591142
    invoke-virtual {v2}, LX/1UY;->h()LX/1UY;

    .line 591143
    :cond_0
    move-object v2, v2

    .line 591144
    const/high16 v1, -0x3f000000    # -8.0f

    .line 591145
    iput v1, v2, LX/1UY;->c:F

    .line 591146
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 591147
    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 591148
    :goto_0
    move-object v1, v1

    .line 591149
    const/4 v0, 0x0

    .line 591150
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 591151
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->h:LX/1Ad;

    sget-object v3, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 591152
    const/high16 v1, 0x41500000    # 13.0f

    .line 591153
    iput v1, v2, LX/1UY;->d:F

    .line 591154
    move-object v1, v0

    .line 591155
    :goto_1
    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    const v4, 0x7f020ae9

    const/4 v5, -0x1

    invoke-direct {v3, v0, v2, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 591156
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->g:LX/ByD;

    const/4 v2, 0x0

    .line 591157
    new-instance v4, LX/ByC;

    invoke-direct {v4, v0}, LX/ByC;-><init>(LX/ByD;)V

    .line 591158
    iget-object v5, v0, LX/ByD;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/ByB;

    .line 591159
    if-nez v5, :cond_1

    .line 591160
    new-instance v5, LX/ByB;

    invoke-direct {v5, v0}, LX/ByB;-><init>(LX/ByD;)V

    .line 591161
    :cond_1
    invoke-static {v5, p1, v2, v2, v4}, LX/ByB;->a$redex0(LX/ByB;LX/1De;IILX/ByC;)V

    .line 591162
    move-object v4, v5

    .line 591163
    move-object v2, v4

    .line 591164
    move-object v2, v2

    .line 591165
    move-object v0, p3

    check-cast v0, LX/1Pt;

    .line 591166
    iget-object v4, v2, LX/ByB;->a:LX/ByC;

    iput-object v0, v4, LX/ByC;->e:LX/1Pt;

    .line 591167
    iget-object v4, v2, LX/ByB;->e:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 591168
    move-object v0, v2

    .line 591169
    iget-object v2, v0, LX/ByB;->a:LX/ByC;

    iput-object v1, v2, LX/ByC;->a:LX/1aZ;

    .line 591170
    iget-object v2, v0, LX/ByB;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 591171
    move-object v0, v0

    .line 591172
    iget-object v1, v0, LX/ByB;->a:LX/ByC;

    iput-object p2, v1, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591173
    iget-object v1, v0, LX/ByB;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 591174
    move-object v0, v0

    .line 591175
    const v1, 0x3ff745d1

    .line 591176
    iget-object v2, v0, LX/ByB;->a:LX/ByC;

    iput v1, v2, LX/ByC;->c:F

    .line 591177
    iget-object v2, v0, LX/ByB;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 591178
    move-object v0, v0

    .line 591179
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->f:LX/2mt;

    invoke-virtual {v1, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 591180
    iget-object v2, v0, LX/ByB;->a:LX/ByC;

    iput-object v1, v2, LX/ByC;->d:Ljava/lang/String;

    .line 591181
    iget-object v2, v0, LX/ByB;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 591182
    move-object v0, v0

    .line 591183
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 591184
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->i:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v3, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 591185
    :cond_2
    const/high16 v1, 0x41400000    # 12.0f

    .line 591186
    iput v1, v2, LX/1UY;->d:F

    .line 591187
    move-object v1, v0

    goto/16 :goto_1

    .line 591188
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4Za;->b(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 591189
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 591190
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4Za;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;
    .locals 10

    .prologue
    .line 591191
    const-class v1, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    monitor-enter v1

    .line 591192
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591193
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591194
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591195
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591196
    new-instance v3, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v5

    check-cast v5, LX/2mt;

    invoke-static {v0}, LX/ByD;->a(LX/0QB;)LX/ByD;

    move-result-object v6

    check-cast v6, LX/ByD;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-static {v0}, LX/C1a;->a(LX/0QB;)LX/C1a;

    move-result-object v9

    check-cast v9, LX/C1a;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;-><init>(Landroid/content/Context;LX/2mt;LX/ByD;LX/1Ad;LX/1V0;LX/C1a;)V

    .line 591197
    move-object v0, v3

    .line 591198
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591199
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591200
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 591202
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 591203
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591204
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591205
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591206
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->j:LX/C1a;

    invoke-virtual {v0}, LX/C1a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 591207
    const/4 v0, 0x0

    return-object v0
.end method
