.class public Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final f:LX/Bxd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bxd",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592711
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->d:LX/1Cz;

    .line 592712
    sget-object v0, LX/2eF;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->e:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Bxd;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592707
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 592708
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->f:LX/Bxd;

    .line 592709
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->g:LX/1V0;

    .line 592710
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 592689
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->f:LX/Bxd;

    const/4 v1, 0x0

    .line 592690
    new-instance v2, LX/Bxb;

    invoke-direct {v2, v0}, LX/Bxb;-><init>(LX/Bxd;)V

    .line 592691
    iget-object v3, v0, LX/Bxd;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bxc;

    .line 592692
    if-nez v3, :cond_0

    .line 592693
    new-instance v3, LX/Bxc;

    invoke-direct {v3, v0}, LX/Bxc;-><init>(LX/Bxd;)V

    .line 592694
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bxc;->a$redex0(LX/Bxc;LX/1De;IILX/Bxb;)V

    .line 592695
    move-object v2, v3

    .line 592696
    move-object v1, v2

    .line 592697
    move-object v0, v1

    .line 592698
    iget-object v1, v0, LX/Bxc;->a:LX/Bxb;

    iput-object p2, v1, LX/Bxb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592699
    iget-object v1, v0, LX/Bxc;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 592700
    move-object v1, v0

    .line 592701
    move-object v0, p3

    check-cast v0, LX/1Pp;

    .line 592702
    iget-object v2, v1, LX/Bxc;->a:LX/Bxb;

    iput-object v0, v2, LX/Bxb;->b:LX/1Pp;

    .line 592703
    iget-object v2, v1, LX/Bxc;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 592704
    move-object v0, v1

    .line 592705
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 592706
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->e:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;
    .locals 6

    .prologue
    .line 592678
    const-class v1, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;

    monitor-enter v1

    .line 592679
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592680
    sput-object v2, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592681
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592682
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592683
    new-instance p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Bxd;->a(LX/0QB;)LX/Bxd;

    move-result-object v4

    check-cast v4, LX/Bxd;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;-><init>(Landroid/content/Context;LX/Bxd;LX/1V0;)V

    .line 592684
    move-object v0, p0

    .line 592685
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592686
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592687
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 592677
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 592676
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592670
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592671
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 592672
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 592674
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592675
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 592673
    sget-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentHScrollPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
