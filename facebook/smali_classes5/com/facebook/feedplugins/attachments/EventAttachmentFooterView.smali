.class public Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;
.super Lcom/facebook/events/widget/eventcard/EventCardFooterView;
.source ""


# static fields
.field public static final k:LX/1Cz;


# instance fields
.field public j:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590801
    new-instance v0, LX/3Wg;

    invoke-direct {v0}, LX/3Wg;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->k:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 590798
    invoke-direct {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;-><init>(Landroid/content/Context;)V

    .line 590799
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->g()V

    .line 590800
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 590784
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590785
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->g()V

    .line 590786
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 590795
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590796
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->g()V

    .line 590797
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;

    invoke-static {v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->j:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 590787
    const-class v0, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 590788
    const-string v1, "newsfeed_angora_attachment_view"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 590789
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 590790
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->j:Lcom/facebook/widget/text/BetterTextView;

    move-object v0, v0

    .line 590791
    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 590792
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->m:Lcom/facebook/widget/text/BetterTextView;

    move-object v0, v0

    .line 590793
    sget-object v1, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 590794
    return-void
.end method
