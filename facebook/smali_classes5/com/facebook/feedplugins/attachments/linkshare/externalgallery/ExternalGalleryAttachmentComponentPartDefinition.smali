.class public Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pe;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/C0X;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C0X",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591360
    new-instance v0, LX/3Wm;

    invoke-direct {v0}, LX/3Wm;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C0X;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 591357
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 591358
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->e:LX/C0X;

    .line 591359
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 591339
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->e:LX/C0X;

    const/4 v1, 0x0

    .line 591340
    new-instance v2, LX/C0W;

    invoke-direct {v2, v0}, LX/C0W;-><init>(LX/C0X;)V

    .line 591341
    iget-object p0, v0, LX/C0X;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C0V;

    .line 591342
    if-nez p0, :cond_0

    .line 591343
    new-instance p0, LX/C0V;

    invoke-direct {p0, v0}, LX/C0V;-><init>(LX/C0X;)V

    .line 591344
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/C0V;->a$redex0(LX/C0V;LX/1De;IILX/C0W;)V

    .line 591345
    move-object v2, p0

    .line 591346
    move-object v1, v2

    .line 591347
    move-object v0, v1

    .line 591348
    iget-object v1, v0, LX/C0V;->a:LX/C0W;

    iput-object p2, v1, LX/C0W;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591349
    iget-object v1, v0, LX/C0V;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 591350
    move-object v0, v0

    .line 591351
    check-cast p3, LX/1Pq;

    .line 591352
    iget-object v1, v0, LX/C0V;->a:LX/C0W;

    iput-object p3, v1, LX/C0W;->b:LX/1Pq;

    .line 591353
    iget-object v1, v0, LX/C0V;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 591354
    move-object v0, v0

    .line 591355
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 591356
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;
    .locals 5

    .prologue
    .line 591328
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 591329
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591330
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591331
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591332
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591333
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C0X;->a(LX/0QB;)LX/C0X;

    move-result-object v4

    check-cast v4, LX/C0X;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/C0X;)V

    .line 591334
    move-object v0, p0

    .line 591335
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591336
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591337
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 591325
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 591327
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 591326
    const/4 v0, 0x1

    return v0
.end method
