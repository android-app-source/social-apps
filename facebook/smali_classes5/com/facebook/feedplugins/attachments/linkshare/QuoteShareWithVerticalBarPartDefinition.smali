.class public Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

.field private final e:LX/1qa;

.field private final f:I

.field public final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590285
    const v0, 0x7f0310e6

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->a:LX/1Cz;

    .line 590286
    sget-object v0, LX/1Ua;->d:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;LX/1qa;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 590278
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 590279
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 590280
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    .line 590281
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->e:LX/1qa;

    .line 590282
    const v0, 0x7f0b00c5

    invoke-static {p4, v0}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->f:I

    .line 590283
    const v0, 0x7f0b00c5

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->g:I

    .line 590284
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;
    .locals 7

    .prologue
    .line 590267
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    monitor-enter v1

    .line 590268
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 590269
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 590270
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590271
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 590272
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v5

    check-cast v5, LX/1qa;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;LX/1qa;Landroid/content/res/Resources;)V

    .line 590273
    move-object v0, p0

    .line 590274
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 590275
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590276
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 590277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 590266
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 590250
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 590251
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 590252
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590253
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/8Y8;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 590254
    const/4 v2, 0x0

    .line 590255
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 590256
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 590257
    if-nez v0, :cond_2

    move v0, v2

    .line 590258
    :goto_0
    move v0, v0

    .line 590259
    if-eqz v0, :cond_1

    const/4 v0, 0x3

    .line 590260
    :goto_1
    if-eqz v1, :cond_0

    .line 590261
    const v2, 0x7f0d281f

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    new-instance v4, LX/C0C;

    invoke-direct {v4, v1, v0}, LX/C0C;-><init>(Ljava/lang/CharSequence;I)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 590262
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 590263
    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 590264
    :cond_2
    iget v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->g:I

    invoke-static {v0, v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 590265
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLImage;)F

    move-result v0

    const v3, 0x3f4ccccd    # 0.8f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 590249
    const/4 v0, 0x1

    return v0
.end method
