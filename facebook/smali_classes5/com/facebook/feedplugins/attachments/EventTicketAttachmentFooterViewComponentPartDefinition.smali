.class public Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final e:LX/C1a;

.field private final f:LX/1V0;

.field private final g:LX/1V7;

.field private final h:LX/ByM;

.field private final i:LX/2mt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 591316
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C1a;LX/1V0;LX/1V7;LX/ByM;LX/2mt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 591317
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 591318
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->e:LX/C1a;

    .line 591319
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->f:LX/1V0;

    .line 591320
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->g:LX/1V7;

    .line 591321
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->h:LX/ByM;

    .line 591322
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->i:LX/2mt;

    .line 591323
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 591261
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->g:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->h()LX/1Ua;

    move-result-object v2

    const v3, 0x7f020ae3

    const/4 v4, -0x1

    invoke-direct {v1, v0, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 591262
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->h:LX/ByM;

    const/4 v2, 0x0

    .line 591263
    new-instance v3, LX/ByL;

    invoke-direct {v3, v0}, LX/ByL;-><init>(LX/ByM;)V

    .line 591264
    iget-object v4, v0, LX/ByM;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/ByK;

    .line 591265
    if-nez v4, :cond_0

    .line 591266
    new-instance v4, LX/ByK;

    invoke-direct {v4, v0}, LX/ByK;-><init>(LX/ByM;)V

    .line 591267
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/ByK;->a$redex0(LX/ByK;LX/1De;IILX/ByL;)V

    .line 591268
    move-object v3, v4

    .line 591269
    move-object v2, v3

    .line 591270
    move-object v0, v2

    .line 591271
    iget-object v2, v0, LX/ByK;->a:LX/ByL;

    iput-object p2, v2, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591272
    iget-object v2, v0, LX/ByK;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 591273
    move-object v2, v0

    .line 591274
    move-object v0, p3

    check-cast v0, LX/1Pq;

    .line 591275
    iget-object v3, v2, LX/ByK;->a:LX/ByL;

    iput-object v0, v3, LX/ByL;->b:LX/1Pq;

    .line 591276
    iget-object v3, v2, LX/ByK;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 591277
    move-object v0, v2

    .line 591278
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->i:LX/2mt;

    invoke-virtual {v2, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 591279
    iget-object v3, v0, LX/ByK;->a:LX/ByL;

    iput-object v2, v3, LX/ByL;->c:Ljava/lang/String;

    .line 591280
    iget-object v3, v0, LX/ByK;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 591281
    move-object v0, v0

    .line 591282
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 591283
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;
    .locals 10

    .prologue
    .line 591305
    const-class v1, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    monitor-enter v1

    .line 591306
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 591307
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 591310
    new-instance v3, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/C1a;->a(LX/0QB;)LX/C1a;

    move-result-object v5

    check-cast v5, LX/C1a;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v7

    check-cast v7, LX/1V7;

    invoke-static {v0}, LX/ByM;->a(LX/0QB;)LX/ByM;

    move-result-object v8

    check-cast v8, LX/ByM;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v9

    check-cast v9, LX/2mt;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;-><init>(Landroid/content/Context;LX/C1a;LX/1V0;LX/1V7;LX/ByM;LX/2mt;)V

    .line 591311
    move-object v0, v3

    .line 591312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 591313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 591315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 591324
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 591304
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 591287
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591288
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 591289
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v2, 0x0

    .line 591290
    if-nez v0, :cond_2

    move v1, v2

    .line 591291
    :goto_0
    move v0, v1

    .line 591292
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->e:LX/C1a;

    .line 591293
    iget-object v1, v0, LX/C1a;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 591294
    iget-object v1, v0, LX/C1a;->a:LX/0ad;

    sget-short v2, LX/C1Z;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/C1a;->d:Ljava/lang/Boolean;

    .line 591295
    :cond_0
    iget-object v1, v0, LX/C1a;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 591296
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 591297
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v4

    .line 591298
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_5

    .line 591299
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 591300
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-static {v1}, LX/2v7;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-static {v1}, LX/2v7;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 591301
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 591302
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_5
    move v1, v2

    .line 591303
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 591285
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 591286
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 591284
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
