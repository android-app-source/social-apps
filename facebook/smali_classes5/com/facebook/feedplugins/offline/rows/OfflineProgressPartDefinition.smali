.class public Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9g;",
        "TE;",
        "LX/CA5;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:LX/1Cz;

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:LX/1Ua;

.field private static p:LX/0Xm;


# instance fields
.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:LX/9A1;

.field public final g:LX/03V;

.field private final h:LX/0SG;

.field private final i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final j:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

.field private final k:LX/1CJ;

.field private final l:LX/C9e;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9A1;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0ad;

.field private final o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596439
    const-class v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->c:Ljava/lang/Class;

    .line 596440
    sget-object v0, LX/1Ua;->r:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->d:LX/1Ua;

    .line 596441
    new-instance v0, LX/3Yc;

    invoke-direct {v0}, LX/3Yc;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a:LX/1Cz;

    .line 596442
    new-instance v0, LX/3Yd;

    invoke-direct {v0}, LX/3Yd;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9A1;LX/03V;LX/0SG;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/1CJ;LX/C9e;LX/0Ot;LX/0ad;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/9A1;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;",
            "LX/1CJ;",
            "LX/C9e;",
            "LX/0Ot",
            "<",
            "LX/9A1;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596426
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596427
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 596428
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->f:LX/9A1;

    .line 596429
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->g:LX/03V;

    .line 596430
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->h:LX/0SG;

    .line 596431
    iput-object p5, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 596432
    iput-object p6, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->j:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    .line 596433
    iput-object p7, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->k:LX/1CJ;

    .line 596434
    iput-object p8, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->l:LX/C9e;

    .line 596435
    iput-object p9, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->m:LX/0Ot;

    .line 596436
    iput-object p10, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->n:LX/0ad;

    .line 596437
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->n:LX/0ad;

    sget-short v1, LX/1aO;->an:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->o:Z

    .line 596438
    return-void
.end method

.method private a(LX/1Pq;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pq;",
            ")",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596425
    new-instance v0, LX/C9f;

    invoke-direct {v0, p0, p1}, LX/C9f;-><init>(Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;LX/1Pq;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;
    .locals 14

    .prologue
    .line 596414
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    monitor-enter v1

    .line 596415
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596416
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596417
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596418
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596419
    new-instance v3, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v5

    check-cast v5, LX/9A1;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-static {v0}, LX/1CJ;->a(LX/0QB;)LX/1CJ;

    move-result-object v10

    check-cast v10, LX/1CJ;

    invoke-static {v0}, LX/C9e;->a(LX/0QB;)LX/C9e;

    move-result-object v11

    check-cast v11, LX/C9e;

    const/16 v12, 0x1d69

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9A1;LX/03V;LX/0SG;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/1CJ;LX/C9e;LX/0Ot;LX/0ad;)V

    .line 596420
    move-object v0, v3

    .line 596421
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596422
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596423
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C9g;LX/1Pr;LX/CA5;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/C9g;",
            "TE;",
            "LX/CA5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 596443
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596444
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596445
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 596446
    if-nez v1, :cond_0

    .line 596447
    iget-object v0, p2, LX/C9g;->b:LX/0TF;

    invoke-virtual {p4, v0}, LX/CA5;->setCallbackOnProgressStarted(LX/0TF;)V

    .line 596448
    :goto_0
    return-void

    .line 596449
    :cond_0
    iget-object v2, p2, LX/C9g;->c:LX/0TF;

    invoke-virtual {p4, v2}, LX/CA5;->setCallbackOnProgressComplete(LX/0TF;)V

    .line 596450
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->l:LX/C9e;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 596451
    iget-object v4, v2, LX/C9e;->h:LX/0ad;

    sget-short v6, LX/1aO;->ai:S

    invoke-interface {v4, v6, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v2, LX/C9e;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Mj;

    .line 596452
    if-eqz v3, :cond_1

    .line 596453
    iget-object v6, v4, LX/8Mj;->a:LX/7ma;

    invoke-virtual {v6, v3}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v6

    .line 596454
    if-eqz v6, :cond_6

    .line 596455
    iget-object v7, v6, LX/7ml;->b:LX/7mm;

    move-object v7, v7

    .line 596456
    sget-object v2, LX/7mm;->POST:LX/7mm;

    if-ne v7, v2, :cond_6

    .line 596457
    iget-object v7, v6, LX/7ml;->c:LX/7mk;

    move-object v6, v7

    .line 596458
    sget-object v7, LX/7mk;->PHOTO:LX/7mk;

    if-ne v6, v7, :cond_6

    const/4 v6, 0x1

    :goto_1
    move v6, v6

    .line 596459
    if-nez v6, :cond_5

    .line 596460
    :cond_1
    const/4 v6, 0x0

    .line 596461
    :goto_2
    move v4, v6

    .line 596462
    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    move v2, v4

    .line 596463
    if-eqz v2, :cond_2

    .line 596464
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->l:LX/C9e;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    .line 596465
    new-instance v4, LX/C9d;

    invoke-direct {v4, v2, v3}, LX/C9d;-><init>(LX/C9e;Ljava/lang/String;)V

    move-object v2, v4

    .line 596466
    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->m:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {}, LX/9A1;->a()Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, LX/CA5;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    .line 596467
    :cond_2
    iget-object v2, p2, LX/C9g;->a:LX/C96;

    .line 596468
    iget-boolean v3, v2, LX/C96;->a:Z

    move v2, v3

    .line 596469
    if-eqz v2, :cond_3

    .line 596470
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->b(J)V

    .line 596471
    check-cast p3, LX/1Pq;

    invoke-interface {p3}, LX/1Pq;->iN_()V

    goto :goto_0

    .line 596472
    :cond_3
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->k:LX/1CJ;

    invoke-virtual {v2, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v2

    .line 596473
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 596474
    iget-object v4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->f:LX/9A1;

    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v0, v3, v6, v7}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/ref/WeakReference;J)V

    .line 596475
    invoke-virtual {p4}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {v2, v0}, LX/4Zb;->b(I)V

    .line 596476
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v0

    .line 596477
    invoke-virtual {p4, v0}, LX/CA5;->setProgress(I)V

    .line 596478
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/CA5;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    move v4, v5

    goto :goto_3

    .line 596479
    :cond_5
    iget-object v6, v4, LX/8Mj;->a:LX/7ma;

    invoke-virtual {v6, v3}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v6

    .line 596480
    invoke-static {v6}, LX/8Mj;->a(LX/7ml;)Z

    move-result v6

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596411
    iget-boolean v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->o:Z

    if-eqz v0, :cond_0

    .line 596412
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->b:LX/1Cz;

    .line 596413
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a:LX/1Cz;

    goto :goto_0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 596401
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 596402
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596403
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596404
    iget-boolean v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->o:Z

    if-eqz v1, :cond_0

    .line 596405
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596406
    :goto_0
    new-instance v1, LX/C95;

    invoke-direct {v1, v0}, LX/C95;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C96;

    move-object v1, p3

    .line 596407
    check-cast v1, LX/1Pq;

    invoke-direct {p0, v1}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a(LX/1Pq;)LX/0TF;

    move-result-object v1

    .line 596408
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p3}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a(LX/1Pq;)LX/0TF;

    move-result-object v2

    .line 596409
    new-instance v3, LX/C9g;

    invoke-direct {v3, v0, v1, v2}, LX/C9g;-><init>(LX/C96;LX/0TF;LX/0TF;)V

    return-object v3

    .line 596410
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->j:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xa8df91d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596400
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C9g;

    check-cast p3, LX/1Pr;

    check-cast p4, LX/CA5;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C9g;LX/1Pr;LX/CA5;)V

    const/16 v1, 0x1f

    const v2, 0x26110933

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 596387
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596388
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->l:LX/C9e;

    .line 596389
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596390
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/C9e;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 596391
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/CA5;

    const/4 v2, 0x0

    .line 596392
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->k:LX/1CJ;

    .line 596393
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596394
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v0

    .line 596395
    invoke-virtual {p4}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, LX/4Zb;->c(I)V

    .line 596396
    const/4 v1, 0x1

    .line 596397
    iput-boolean v1, v0, LX/4Zb;->a:Z

    .line 596398
    invoke-virtual {p4, v2, v2}, LX/CA5;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V

    .line 596399
    return-void
.end method
