.class public Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/CA3;",
        "TE;",
        "LX/CAC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/CAC;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/CA2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596299
    new-instance v0, LX/3YZ;

    invoke-direct {v0}, LX/3YZ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/CA2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596296
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596297
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->b:LX/CA2;

    .line 596298
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/CAC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596279
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 596292
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596293
    new-instance v0, LX/CA3;

    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->b:LX/CA2;

    .line 596294
    invoke-virtual {v1}, LX/CA2;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, LX/CA1;

    invoke-direct {p0, v1}, LX/CA1;-><init>(LX/CA2;)V

    :goto_0
    move-object v1, p0

    .line 596295
    invoke-direct {v0, v1}, LX/CA3;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x711744c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596287
    check-cast p2, LX/CA3;

    check-cast p4, LX/CAC;

    .line 596288
    const v1, 0x7f082985

    .line 596289
    iget-object v2, p4, LX/CAC;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 596290
    iget-object v1, p2, LX/CA3;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/CAC;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596291
    const/16 v1, 0x1f

    const v2, 0x3c8be750

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 596283
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596284
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->b:LX/CA2;

    .line 596285
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596286
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/CA2;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 596280
    check-cast p4, LX/CAC;

    .line 596281
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/CAC;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596282
    return-void
.end method
