.class public Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9I;",
        "TE;",
        "LX/CA6;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final c:LX/1Ua;

.field private static o:LX/0Xm;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final f:LX/9A1;

.field public final g:LX/03V;

.field private final h:LX/0SG;

.field private final i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final j:LX/1CJ;

.field private final k:LX/0qn;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0kb;

.field private final n:LX/C99;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596697
    const-class v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->b:Ljava/lang/Class;

    .line 596698
    sget-object v0, LX/1Ua;->r:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->c:LX/1Ua;

    .line 596699
    new-instance v0, LX/3Yg;

    invoke-direct {v0}, LX/3Yg;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9A1;LX/03V;LX/0SG;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/1CJ;LX/0qn;LX/0Ot;LX/0kb;LX/C99;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/9A1;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/1CJ;",
            "LX/0qn;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/0kb;",
            "LX/C99;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596684
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596685
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->d:Landroid/content/Context;

    .line 596686
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 596687
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->f:LX/9A1;

    .line 596688
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->g:LX/03V;

    .line 596689
    iput-object p5, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->h:LX/0SG;

    .line 596690
    iput-object p6, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 596691
    iput-object p7, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->j:LX/1CJ;

    .line 596692
    iput-object p8, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->k:LX/0qn;

    .line 596693
    iput-object p9, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->l:LX/0Ot;

    .line 596694
    iput-object p10, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->m:LX/0kb;

    .line 596695
    iput-object p11, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->n:LX/C99;

    .line 596696
    return-void
.end method

.method private a(LX/1Pq;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pq;",
            ")",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596683
    new-instance v0, LX/C9F;

    invoke-direct {v0, p0, p1}, LX/C9F;-><init>(Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;LX/1Pq;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;
    .locals 15

    .prologue
    .line 596672
    const-class v1, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    monitor-enter v1

    .line 596673
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596674
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596675
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596676
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596677
    new-instance v3, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v6

    check-cast v6, LX/9A1;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v9

    check-cast v9, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/1CJ;->a(LX/0QB;)LX/1CJ;

    move-result-object v10

    check-cast v10, LX/1CJ;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v11

    check-cast v11, LX/0qn;

    const/16 v12, 0xf39

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v13

    check-cast v13, LX/0kb;

    invoke-static {v0}, LX/C99;->a(LX/0QB;)LX/C99;

    move-result-object v14

    check-cast v14, LX/C99;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9A1;LX/03V;LX/0SG;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/1CJ;LX/0qn;LX/0Ot;LX/0kb;LX/C99;)V

    .line 596678
    move-object v0, v3

    .line 596679
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596680
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596681
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C9I;LX/1Ps;LX/CA6;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/C9I;",
            "TE;",
            "LX/CA6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 596632
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596633
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596634
    iget-object v1, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    if-nez v1, :cond_1

    .line 596635
    iget-object v0, p2, LX/C9I;->f:LX/0TF;

    invoke-virtual {p4, v0}, LX/CA5;->setCallbackOnProgressStarted(LX/0TF;)V

    .line 596636
    :cond_0
    :goto_0
    return-void

    .line 596637
    :cond_1
    iget-object v1, p2, LX/C9I;->b:LX/C98;

    iget-object v2, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    .line 596638
    iput-object v2, v1, LX/C98;->c:Lcom/facebook/composer/publish/common/PendingStory;

    .line 596639
    iget-object v1, p2, LX/C9I;->b:LX/C98;

    .line 596640
    iput-object p4, v1, LX/C98;->d:LX/CA6;

    .line 596641
    iget-object v1, p2, LX/C9I;->b:LX/C98;

    iget-object v2, p2, LX/C9I;->d:LX/C9G;

    iget-object v3, p2, LX/C9I;->c:LX/C9H;

    iget-object v4, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    const/4 p1, 0x0

    .line 596642
    iput-object v4, v1, LX/C98;->c:Lcom/facebook/composer/publish/common/PendingStory;

    .line 596643
    iput-object v1, p4, LX/CA6;->l:LX/C98;

    .line 596644
    iput-object v4, p4, LX/CA6;->k:Lcom/facebook/composer/publish/common/PendingStory;

    .line 596645
    invoke-virtual {p4}, LX/CA6;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x106000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 596646
    iput v5, v2, LX/C9G;->a:I

    .line 596647
    iget-object v5, p4, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v5, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596648
    iget-object v5, p4, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v5, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 596649
    invoke-virtual {p4, p1}, LX/CA6;->setVisibility(I)V

    .line 596650
    iget-object v5, p4, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v5, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 596651
    iget-object v5, p4, LX/CA6;->h:Lcom/facebook/resources/ui/FbTextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 596652
    iget-object v5, p4, LX/CA6;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 596653
    iget-object v5, p4, LX/CA6;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v5, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 596654
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 596655
    if-eqz v1, :cond_2

    .line 596656
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->m:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->v()Z

    move-result v2

    if-nez v2, :cond_3

    .line 596657
    iget-boolean v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move v1, v2

    .line 596658
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p4, v1}, LX/CA6;->setStoryIsWaitingForWifi(Z)V

    .line 596659
    :cond_2
    iget-object v1, p2, LX/C9I;->g:LX/0TF;

    invoke-virtual {p4, v1}, LX/CA5;->setCallbackOnProgressComplete(LX/0TF;)V

    .line 596660
    iget-object v1, p2, LX/C9I;->a:LX/C96;

    .line 596661
    iget-boolean v2, v1, LX/C96;->a:Z

    move v1, v2

    .line 596662
    if-eqz v1, :cond_4

    .line 596663
    iget-object v0, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->b(J)V

    .line 596664
    check-cast p3, LX/1Pq;

    invoke-interface {p3}, LX/1Pq;->iN_()V

    goto/16 :goto_0

    .line 596665
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 596666
    :cond_4
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->j:LX/1CJ;

    invoke-virtual {v1, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v1

    .line 596667
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 596668
    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->f:LX/9A1;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v0, v2, v4, v5}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/ref/WeakReference;J)V

    .line 596669
    invoke-virtual {p4}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, LX/4Zb;->b(I)V

    .line 596670
    iget-object v0, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    if-eqz v0, :cond_0

    .line 596671
    iget-object v0, p2, LX/C9I;->e:Lcom/facebook/composer/publish/common/PendingStory;

    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v0

    invoke-virtual {p4, v0}, LX/CA5;->setProgress(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596631
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 596584
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 596585
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596586
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596587
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->c:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move-object v1, p3

    .line 596588
    check-cast v1, LX/1Pq;

    invoke-direct {p0, v1}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a(LX/1Pq;)LX/0TF;

    move-result-object v6

    move-object v1, p3

    .line 596589
    check-cast v1, LX/1Pq;

    invoke-direct {p0, v1}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a(LX/1Pq;)LX/0TF;

    move-result-object v7

    .line 596590
    check-cast p3, LX/1Pr;

    new-instance v1, LX/C95;

    invoke-direct {v1, v0}, LX/C95;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C96;

    .line 596591
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v5

    .line 596592
    new-instance v2, LX/C98;

    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->f:LX/9A1;

    iget-object v4, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v4}, LX/C98;-><init>(LX/9A1;Landroid/content/Context;)V

    .line 596593
    new-instance v4, LX/C9G;

    invoke-direct {v4}, LX/C9G;-><init>()V

    .line 596594
    new-instance v3, LX/C9H;

    iget-object v8, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->n:LX/C99;

    invoke-direct {v3, v0, v8}, LX/C9H;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/C99;)V

    .line 596595
    new-instance v0, LX/C9I;

    invoke-direct/range {v0 .. v7}, LX/C9I;-><init>(LX/C96;LX/C98;LX/C9H;LX/C9G;Lcom/facebook/composer/publish/common/PendingStory;LX/0TF;LX/0TF;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7e1ddc5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596630
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C9I;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/CA6;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C9I;LX/1Ps;LX/CA6;)V

    const/16 v1, 0x1f

    const v2, -0x403d027e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    .line 596614
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 596615
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596616
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596617
    invoke-static {v0}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 596618
    :goto_0
    return v0

    .line 596619
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->k:LX/0qn;

    invoke-virtual {v2, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v2

    .line 596620
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v2, v3, :cond_2

    :cond_1
    move v0, v1

    .line 596621
    goto :goto_0

    .line 596622
    :cond_2
    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->i:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v3

    .line 596623
    if-nez v3, :cond_4

    .line 596624
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, v3, :cond_3

    .line 596625
    iget-object v6, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->f:LX/9A1;

    const-wide/16 v8, 0xbb8

    invoke-virtual {v6, v0, v8, v9}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 596626
    :cond_3
    move v0, v1

    .line 596627
    goto :goto_0

    .line 596628
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v0

    .line 596629
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v2, v3, :cond_5

    const/16 v2, 0x3e8

    if-ge v0, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 596596
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C9I;

    check-cast p4, LX/CA6;

    .line 596597
    const/4 v1, 0x0

    .line 596598
    iget-object v0, p4, LX/CA6;->i:LX/5ON;

    if-eqz v0, :cond_0

    .line 596599
    iget-object v0, p4, LX/CA6;->i:LX/5ON;

    .line 596600
    iput-object v1, v0, LX/5OM;->p:LX/5OO;

    .line 596601
    :cond_0
    iget-object v0, p4, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596602
    iget-object v0, p4, LX/CA6;->j:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 596603
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->j:LX/1CJ;

    .line 596604
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596605
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v0

    .line 596606
    invoke-virtual {p4}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, LX/4Zb;->c(I)V

    .line 596607
    const/4 v1, 0x1

    .line 596608
    iput-boolean v1, v0, LX/4Zb;->a:Z

    .line 596609
    iget-object v0, p2, LX/C9I;->b:LX/C98;

    if-eqz v0, :cond_1

    .line 596610
    iget-object v0, p2, LX/C9I;->b:LX/C98;

    .line 596611
    iget-object v1, v0, LX/C98;->f:LX/2EJ;

    invoke-static {v1}, LX/C98;->a(LX/2EJ;)V

    .line 596612
    iget-object v1, v0, LX/C98;->e:LX/2EJ;

    invoke-static {v1}, LX/C98;->a(LX/2EJ;)V

    .line 596613
    :cond_1
    return-void
.end method
