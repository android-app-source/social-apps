.class public Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9a;",
        "TE;",
        "LX/CA8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/9A1;

.field private final d:LX/7ly;

.field private final e:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

.field private final f:LX/C9Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596336
    sget-object v0, LX/1Ua;->r:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->b:LX/1Ua;

    .line 596337
    new-instance v0, LX/3Ya;

    invoke-direct {v0}, LX/3Ya;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/9A1;LX/7ly;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/C9Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596338
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596339
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->c:LX/9A1;

    .line 596340
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->d:LX/7ly;

    .line 596341
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->e:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    .line 596342
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->f:LX/C9Y;

    .line 596343
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;
    .locals 7

    .prologue
    .line 596324
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    monitor-enter v1

    .line 596325
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596326
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596329
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v3

    check-cast v3, LX/9A1;

    const-class v4, LX/7ly;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/7ly;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-static {v0}, LX/C9Y;->a(LX/0QB;)LX/C9Y;

    move-result-object v6

    check-cast v6, LX/C9Y;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;-><init>(LX/9A1;LX/7ly;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/C9Y;)V

    .line 596330
    move-object v0, p0

    .line 596331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596335
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 596314
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596315
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596316
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596317
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->e:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596318
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->d:LX/7ly;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7ly;->a(Ljava/lang/String;)LX/7lx;

    move-result-object v1

    .line 596319
    new-instance v2, LX/C9Z;

    invoke-direct {v2, p0, v1}, LX/C9Z;-><init>(Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;LX/7lx;)V

    .line 596320
    invoke-static {}, LX/9A1;->a()Landroid/view/View$OnTouchListener;

    move-result-object v1

    .line 596321
    new-instance v3, LX/C9a;

    iget-object v4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->f:LX/C9Y;

    .line 596322
    new-instance p0, LX/C9Q;

    invoke-direct {p0, v4, v0}, LX/C9Q;-><init>(LX/C9Y;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, p0

    .line 596323
    invoke-direct {v3, v0, v1, v2}, LX/C9a;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x53dec109

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596310
    check-cast p2, LX/C9a;

    check-cast p4, LX/CA8;

    .line 596311
    iget-object v1, p2, LX/C9a;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/CA8;->setErrorListener(Landroid/view/View$OnClickListener;)V

    .line 596312
    iget-object v1, p2, LX/C9a;->b:Landroid/view/View$OnTouchListener;

    iget-object v2, p2, LX/C9a;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/CA8;->a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596313
    const/16 v1, 0x1f

    const v2, 0x47244c24

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 596306
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596307
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->f:LX/C9Y;

    .line 596308
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596309
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/C9Y;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 596302
    check-cast p4, LX/CA8;

    const/4 v0, 0x0

    .line 596303
    invoke-virtual {p4, v0}, LX/CA8;->setErrorListener(Landroid/view/View$OnClickListener;)V

    .line 596304
    invoke-virtual {p4, v0, v0}, LX/CA8;->a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596305
    return-void
.end method
