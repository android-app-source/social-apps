.class public Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9w;",
        "TE;",
        "LX/CAB;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/9A1;

.field public final c:LX/16I;

.field private final d:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

.field private final e:LX/C9t;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596575
    new-instance v0, LX/3Yf;

    invoke-direct {v0}, LX/3Yf;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/9A1;LX/16I;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/C9t;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596576
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596577
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->b:LX/9A1;

    .line 596578
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->c:LX/16I;

    .line 596579
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->d:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    .line 596580
    iput-object p5, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->e:LX/C9t;

    .line 596581
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;
    .locals 9

    .prologue
    .line 596563
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    monitor-enter v1

    .line 596564
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596565
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596566
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596567
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596568
    new-instance v3, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v4

    check-cast v4, LX/9A1;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v5

    check-cast v5, LX/16I;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-static {v0}, LX/C9t;->b(LX/0QB;)LX/C9t;

    move-result-object v8

    check-cast v8, LX/C9t;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;-><init>(LX/9A1;LX/16I;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;LX/C9t;)V

    .line 596569
    move-object v0, v3

    .line 596570
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596571
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596572
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596574
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 596552
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596553
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596554
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596555
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->d:Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596556
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->e:LX/C9t;

    invoke-virtual {v1, v0}, LX/C9t;->c(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 596557
    new-instance v1, LX/C9w;

    invoke-static {}, LX/9A1;->a()Landroid/view/View$OnTouchListener;

    move-result-object v2

    .line 596558
    new-instance v3, LX/C9v;

    invoke-direct {v3, p0, v0}, LX/C9v;-><init>(Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v3, v3

    .line 596559
    iget-object v4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->e:LX/C9t;

    .line 596560
    new-instance p1, LX/C9s;

    invoke-direct {p1, v4, v0}, LX/C9s;-><init>(LX/C9t;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v4, p1

    .line 596561
    iget-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->c:LX/16I;

    sget-object p2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance p3, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition$3;

    invoke-direct {p3, p0, v0}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition$3;-><init>(Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {p1, p2, p3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object p1

    move-object v0, p1

    .line 596562
    invoke-direct {v1, v2, v3, v4, v0}, LX/C9w;-><init>(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/0Yb;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4f893e5c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596548
    check-cast p2, LX/C9w;

    check-cast p4, LX/CAB;

    .line 596549
    iget-object v1, p2, LX/C9w;->a:Landroid/view/View$OnTouchListener;

    iget-object v2, p2, LX/C9w;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/CAB;->a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596550
    iget-object v1, p2, LX/C9w;->a:Landroid/view/View$OnTouchListener;

    iget-object v2, p2, LX/C9w;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/CAB;->b(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596551
    const/16 v1, 0x1f

    const v2, 0x6887c408

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 596544
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596545
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->e:LX/C9t;

    .line 596546
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596547
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/C9t;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 596538
    check-cast p2, LX/C9w;

    check-cast p4, LX/CAB;

    const/4 v1, 0x0

    .line 596539
    iget-object v0, p2, LX/C9w;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 596540
    invoke-virtual {p4, v1, v1}, LX/CAB;->a(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596541
    invoke-virtual {p4, v1, v1}, LX/CAB;->b(Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 596542
    invoke-virtual {p4, v1}, LX/CAB;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596543
    return-void
.end method
