.class public Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/C9C;

.field private final e:LX/C9E;

.field private final f:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C9E;LX/C9C;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596730
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 596731
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->e:LX/C9E;

    .line 596732
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->d:LX/C9C;

    .line 596733
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->f:LX/1V0;

    .line 596734
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 596720
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->f:LX/1V0;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->r:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->d:LX/C9C;

    const/4 v3, 0x0

    .line 596721
    new-instance p0, LX/C9B;

    invoke-direct {p0, v2}, LX/C9B;-><init>(LX/C9C;)V

    .line 596722
    sget-object p2, LX/C9C;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C9A;

    .line 596723
    if-nez p2, :cond_0

    .line 596724
    new-instance p2, LX/C9A;

    invoke-direct {p2}, LX/C9A;-><init>()V

    .line 596725
    :cond_0
    invoke-static {p2, p1, v3, v3, p0}, LX/C9A;->a$redex0(LX/C9A;LX/1De;IILX/C9B;)V

    .line 596726
    move-object p0, p2

    .line 596727
    move-object v3, p0

    .line 596728
    move-object v2, v3

    .line 596729
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;
    .locals 7

    .prologue
    .line 596709
    const-class v1, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    monitor-enter v1

    .line 596710
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596711
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596714
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C9E;->b(LX/0QB;)LX/C9E;

    move-result-object v4

    check-cast v4, LX/C9E;

    invoke-static {v0}, LX/C9C;->a(LX/0QB;)LX/C9C;

    move-result-object v5

    check-cast v5, LX/C9C;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;-><init>(Landroid/content/Context;LX/C9E;LX/C9C;LX/1V0;)V

    .line 596715
    move-object v0, p0

    .line 596716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 596735
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 596708
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 596705
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->e:LX/C9E;

    .line 596706
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596707
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/C9E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596704
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 596702
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596703
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
