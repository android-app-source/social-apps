.class public Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9b;",
        "TE;",
        "LX/CA7;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/C9Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596384
    new-instance v0, LX/3Yb;

    invoke-direct {v0}, LX/3Yb;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/C9Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596381
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596382
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->b:LX/C9Y;

    .line 596383
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;
    .locals 4

    .prologue
    .line 596370
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    monitor-enter v1

    .line 596371
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596372
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596373
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596374
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596375
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    invoke-static {v0}, LX/C9Y;->a(LX/0QB;)LX/C9Y;

    move-result-object v3

    check-cast v3, LX/C9Y;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;-><init>(LX/C9Y;)V

    .line 596376
    move-object v0, p0

    .line 596377
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596378
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596379
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596369
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 596358
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596359
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596360
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596361
    new-instance v1, LX/C9b;

    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->b:LX/C9Y;

    .line 596362
    new-instance v3, LX/C9U;

    invoke-direct {v3, v2, v0}, LX/C9U;-><init>(LX/C9Y;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v2, v3

    .line 596363
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->b:LX/C9Y;

    .line 596364
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object p1

    .line 596365
    iget-object p0, v4, LX/C9Y;->h:LX/0ad;

    sget-short p2, LX/1aO;->O:S

    const/4 p3, 0x0

    invoke-interface {p0, p2, p3}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 596366
    new-instance p0, LX/C9R;

    invoke-direct {p0, v4}, LX/C9R;-><init>(LX/C9Y;)V

    .line 596367
    :goto_0
    move-object v0, p0

    .line 596368
    invoke-direct {v1, v2, v3, v0}, LX/C9b;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    new-instance p0, LX/C9T;

    invoke-direct {p0, v4, p1, v0}, LX/C9T;-><init>(LX/C9Y;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3cba8a7c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596354
    check-cast p2, LX/C9b;

    check-cast p4, LX/CA7;

    .line 596355
    iget-object v1, p2, LX/C9b;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/CA7;->setCancelButtonListeners(Landroid/view/View$OnClickListener;)V

    .line 596356
    iget-object v1, p2, LX/C9b;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/CA7;->setTextOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596357
    const/16 v1, 0x1f

    const v2, -0x6e15d2af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 596346
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596347
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->b:LX/C9Y;

    .line 596348
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596349
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/C9Y;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 596350
    check-cast p4, LX/CA7;

    const/4 v0, 0x0

    .line 596351
    invoke-virtual {p4, v0}, LX/CA7;->setCancelButtonListeners(Landroid/view/View$OnClickListener;)V

    .line 596352
    invoke-virtual {p4, v0}, LX/CA7;->setTextOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596353
    return-void
.end method
