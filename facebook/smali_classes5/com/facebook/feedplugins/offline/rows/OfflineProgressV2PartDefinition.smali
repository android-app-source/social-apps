.class public Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C9i;",
        "TE;",
        "LX/CA9;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/C9e;

.field private final c:LX/0kb;

.field public final d:LX/8K3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596535
    new-instance v0, LX/3Ye;

    invoke-direct {v0}, LX/3Ye;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/C9e;LX/0kb;LX/8K3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596530
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596531
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->b:LX/C9e;

    .line 596532
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->c:LX/0kb;

    .line 596533
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->d:LX/8K3;

    .line 596534
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;
    .locals 6

    .prologue
    .line 596519
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    monitor-enter v1

    .line 596520
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596521
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596522
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596523
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596524
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    invoke-static {v0}, LX/C9e;->a(LX/0QB;)LX/C9e;

    move-result-object v3

    check-cast v3, LX/C9e;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/8K3;->b(LX/0QB;)LX/8K3;

    move-result-object v5

    check-cast v5, LX/8K3;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;-><init>(LX/C9e;LX/0kb;LX/8K3;)V

    .line 596525
    move-object v0, p0

    .line 596526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;LX/CA9;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 596485
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596486
    const/4 v0, 0x4

    new-array v1, v0, [I

    const v0, 0x7f082980

    aput v0, v1, v3

    const v0, 0x7f082981

    aput v0, v1, v2

    const v0, 0x7f082982

    aput v0, v1, v4

    const/4 v0, 0x3

    const v2, 0x7f082983

    aput v2, v1, v0

    .line 596487
    const/16 v0, 0x1f4

    .line 596488
    invoke-virtual {p1, v3}, LX/CA9;->setAnimated(Z)V

    .line 596489
    :goto_0
    invoke-static {p1}, LX/CA9;->c(LX/CA9;)V

    .line 596490
    iget-object v5, p1, LX/CA9;->a:Lcom/facebook/widget/CyclingTextSwitcher;

    int-to-long v7, v0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v1, v7, v8, v6}, Lcom/facebook/widget/CyclingTextSwitcher;->a([IJLjava/util/concurrent/TimeUnit;)V

    .line 596491
    return-void

    .line 596492
    :cond_0
    new-array v1, v4, [I

    const v0, 0x7f08297e

    aput v0, v1, v3

    const v0, 0x7f08297d

    aput v0, v1, v2

    .line 596493
    const/16 v0, 0x7d0

    .line 596494
    invoke-virtual {p1, v2}, LX/CA9;->setAnimated(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596518
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 596511
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596512
    new-instance v1, LX/C9i;

    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->b:LX/C9e;

    .line 596513
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596514
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 596515
    iget-object p0, v2, LX/C9e;->h:LX/0ad;

    sget-short p1, LX/1aO;->af:S

    const/4 p2, 0x0

    invoke-interface {p0, p1, p2}, LX/0ad;->a(SZ)Z

    move-result p0

    move p0, p0

    .line 596516
    if-eqz p0, :cond_0

    new-instance p0, LX/C9c;

    invoke-direct {p0, v2, v0}, LX/C9c;-><init>(LX/C9e;Lcom/facebook/graphql/model/GraphQLStory;)V

    :goto_0
    move-object v0, p0

    .line 596517
    invoke-direct {v1, v0}, LX/C9i;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v1

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6477762a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596503
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C9i;

    check-cast p4, LX/CA9;

    .line 596504
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->d:LX/8K3;

    new-instance v2, LX/C9h;

    invoke-direct {v2, p0, p4}, LX/C9h;-><init>(Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;LX/CA9;)V

    invoke-virtual {v1, v2}, LX/8K3;->a(LX/8K1;)V

    .line 596505
    invoke-static {p0, p4}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a$redex0(Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;LX/CA9;)V

    .line 596506
    iget-object v1, p2, LX/C9i;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/CA9;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596507
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 596508
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/C9e;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596509
    const v1, 0x7f0d1e7a

    invoke-virtual {p4, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 596510
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x4fc32a7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 596499
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596500
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->b:LX/C9e;

    .line 596501
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596502
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/C9e;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 596495
    check-cast p4, LX/CA9;

    .line 596496
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->d:LX/8K3;

    invoke-virtual {v0}, LX/8K3;->c()V

    .line 596497
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/CA9;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596498
    return-void
.end method
