.class public abstract Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JS1;",
        "LX/1Ps;",
        "Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:I

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/JS0;

.field public final f:LX/JRz;

.field private final g:LX/1Sa;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JTe;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1Cz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 596130
    const v0, 0x7f020a3c

    sput v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->a:I

    .line 596131
    const-class v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;LX/JS0;LX/JRz;LX/1Sa;LX/0Ot;LX/0Or;LX/1Cz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Landroid/content/res/Resources;",
            "LX/JS0;",
            "LX/JRz;",
            "LX/1Sa;",
            "LX/0Ot",
            "<",
            "LX/JTe;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/1Cz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 596120
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596121
    iput-object p3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->e:LX/JS0;

    .line 596122
    iput-object p4, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->f:LX/JRz;

    .line 596123
    iput-object p5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->g:LX/1Sa;

    .line 596124
    iput-object p6, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->h:LX/0Ot;

    .line 596125
    iput-object p7, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->i:LX/0Or;

    .line 596126
    iput-object p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 596127
    iput-object p2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->d:Landroid/content/res/Resources;

    .line 596128
    iput-object p8, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->j:LX/1Cz;

    .line 596129
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2

    .prologue
    .line 596117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 596118
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-object p0, v0

    .line 596119
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596043
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 596093
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596094
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596095
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 596096
    invoke-static {v0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 596097
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->a:LX/1Ua;

    sget v5, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->a:I

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596098
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->d:Landroid/content/res/Resources;

    sget v2, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 596099
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 596100
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 596101
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 596102
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 596103
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    .line 596104
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j()LX/0Px;

    move-result-object v3

    .line 596105
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p()LX/0Px;

    move-result-object v4

    .line 596106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r()LX/0Px;

    move-result-object v5

    .line 596107
    :goto_0
    new-instance v0, LX/JS1;

    invoke-direct/range {v0 .. v5}, LX/JS1;-><init>(Landroid/graphics/Rect;Lcom/facebook/graphql/model/GraphQLApplication;Ljava/util/List;LX/0Px;LX/0Px;)V

    return-object v0

    .line 596108
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 596109
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    .line 596110
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v3

    .line 596111
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fH()LX/0Px;

    move-result-object v4

    .line 596112
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gU()LX/0Px;

    move-result-object v5

    goto :goto_0

    .line 596113
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    .line 596114
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v3

    .line 596115
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fH()LX/0Px;

    move-result-object v4

    .line 596116
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gU()LX/0Px;

    move-result-object v5

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3f4429db

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596050
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JS1;

    check-cast p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    const/4 p3, 0x0

    .line 596051
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 596052
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 596053
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 596054
    invoke-static {v1}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 596055
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingLeft()I

    move-result v1

    iget-object v5, p2, LX/JS1;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v5

    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingTop()I

    move-result v5

    iget-object v6, p2, LX/JS1;->a:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v6

    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingRight()I

    move-result v6

    iget-object v7, p2, LX/JS1;->a:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingBottom()I

    move-result v7

    iget-object v8, p2, LX/JS1;->a:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    invoke-virtual {p4, v1, v5, v6, v7}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->setPadding(IIII)V

    .line 596056
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 596057
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 596058
    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->y:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 596059
    const/4 v5, 0x1

    iput-boolean v5, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->x:Z

    .line 596060
    new-instance v5, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView$1;

    invoke-direct {v5, p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView$1;-><init>(Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;)V

    invoke-virtual {p4, v5}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->post(Ljava/lang/Runnable;)Z

    .line 596061
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->d:Landroid/widget/TextView;

    iget-object v5, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596062
    iget-object v1, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    .line 596063
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596064
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 596065
    const v5, 0x7f0b0f45

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 596066
    iget-object v5, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v5

    new-instance v6, LX/1o9;

    invoke-direct {v6, v1, v1}, LX/1o9;-><init>(II)V

    .line 596067
    iput-object v6, v5, LX/1bX;->c:LX/1o9;

    .line 596068
    move-object v1, v5

    .line 596069
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 596070
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v6, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v6, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 596071
    iget-object v5, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 596072
    iget-object v5, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->c:Landroid/widget/TextView;

    iget-object v1, p2, LX/JS1;->d:LX/0Px;

    invoke-virtual {v1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596073
    iget-object v1, p2, LX/JS1;->e:LX/0Px;

    invoke-virtual {v1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAudio;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAudio;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->h:Landroid/net/Uri;

    .line 596074
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JTe;

    iget-object v5, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->h:Landroid/net/Uri;

    .line 596075
    iget-object v6, v1, LX/JTe;->e:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, v1, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v6, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 596076
    iget-object v6, v1, LX/JTe;->d:Ljava/util/Set;

    invoke-interface {v6, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 596077
    invoke-static {v1}, LX/JTe;->e(LX/JTe;)V

    .line 596078
    iget-object v6, v1, LX/JTe;->l:Ljava/lang/Runnable;

    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    .line 596079
    :cond_0
    iget-object v1, p2, LX/JS1;->c:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->j:Ljava/lang/String;

    .line 596080
    iget-object v1, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->i:Ljava/lang/String;

    .line 596081
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 596082
    iget-object v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->e:LX/JS0;

    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v7, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v1, v7}, LX/JS0;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 596083
    :cond_1
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v4}, LX/1Sa;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 596084
    const v1, -0x3625f733

    invoke-static {v4, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 596085
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    const-string v6, "native_story"

    const/4 v7, 0x0

    invoke-virtual {v1, v5, v6, v2, v7}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V

    .line 596086
    iget-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->m:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 596087
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    .line 596088
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    .line 596089
    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    iput-object v1, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    .line 596090
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->f:LX/JRz;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    iget-object v6, p2, LX/JS1;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v6

    .line 596091
    iget-object v7, v1, LX/JRz;->a:LX/0Zb;

    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p3, "music_preview_impression"

    invoke-direct {v8, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p3, "og_song_id"

    invoke-virtual {v8, p3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p3, "og_object_id"

    invoke-virtual {v8, p3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p3, "tracking_codes"

    invoke-virtual {v8, p3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p3, "provider_name"

    invoke-virtual {v8, p3, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 596092
    const/16 v1, 0x1f

    const v2, 0x4dba7bd9    # 3.91084832E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596049
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 596044
    check-cast p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    .line 596045
    iget-object v0, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596046
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;->e:LX/JS0;

    iget-object v0, p4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 596047
    iget-object p0, v1, LX/JS0;->a:LX/1PJ;

    invoke-virtual {p0, v0}, LX/1PJ;->a(Landroid/view/View;)V

    .line 596048
    :cond_0
    return-void
.end method
