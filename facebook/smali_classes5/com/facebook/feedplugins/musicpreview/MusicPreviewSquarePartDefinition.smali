.class public Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;
.super Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596029
    new-instance v0, LX/3YS;

    invoke-direct {v0}, LX/3YS;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;LX/JS0;LX/JRz;LX/1Sa;LX/0Ot;LX/0Or;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Landroid/content/res/Resources;",
            "LX/JS0;",
            "LX/JRz;",
            "LX/1Sa;",
            "LX/0Ot",
            "<",
            "LX/JTe;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596030
    sget-object v8, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->a:LX/1Cz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;LX/JS0;LX/JRz;LX/1Sa;LX/0Ot;LX/0Or;LX/1Cz;)V

    .line 596031
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;
    .locals 11

    .prologue
    .line 596032
    const-class v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    monitor-enter v1

    .line 596033
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596034
    sput-object v2, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596035
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596036
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596037
    new-instance v3, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/JS0;->b(LX/0QB;)LX/JS0;

    move-result-object v6

    check-cast v6, LX/JS0;

    invoke-static {v0}, LX/JRz;->a(LX/0QB;)LX/JRz;

    move-result-object v7

    check-cast v7, LX/JRz;

    invoke-static {v0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v8

    check-cast v8, LX/1Sa;

    const/16 v9, 0x204c

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x509

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;LX/JS0;LX/JRz;LX/1Sa;LX/0Ot;LX/0Or;)V

    .line 596038
    move-object v0, v3

    .line 596039
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596040
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596041
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
