.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C33;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/C3Q;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593166
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C3Q;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593162
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 593163
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->e:LX/C3Q;

    .line 593164
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->f:LX/1V0;

    .line 593165
    return-void
.end method

.method private a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C33;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 593145
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->e:LX/C3Q;

    invoke-virtual {v0, p1}, LX/C3Q;->c(LX/1De;)LX/C3O;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/C3O;->a(LX/C33;)LX/C3O;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/C3O;->a(LX/1Pb;)LX/C3O;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 593146
    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C33;->b:LX/C34;

    invoke-static {v1, v2}, LX/C31;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    .line 593147
    iget-object v2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;
    .locals 6

    .prologue
    .line 593151
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;

    monitor-enter v1

    .line 593152
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593153
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593154
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593155
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593156
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C3Q;->a(LX/0QB;)LX/C3Q;

    move-result-object v4

    check-cast v4, LX/C3Q;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;-><init>(Landroid/content/Context;LX/C3Q;LX/1V0;)V

    .line 593157
    move-object v0, p0

    .line 593158
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593159
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593160
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593161
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 593167
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 593150
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 593148
    check-cast p1, LX/C33;

    .line 593149
    invoke-static {p1}, LX/C3R;->a(LX/C33;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 593143
    check-cast p1, LX/C33;

    .line 593144
    invoke-virtual {p1}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 593142
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryReshareComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
