.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C33;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/C0P;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593094
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/C0P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593138
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 593139
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->e:LX/1V0;

    .line 593140
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->f:LX/C0P;

    .line 593141
    return-void
.end method

.method private a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C33;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 593131
    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593132
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 593133
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 593134
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->f:LX/C0P;

    invoke-virtual {v0, p1}, LX/C0P;->c(LX/1De;)LX/C0N;

    move-result-object v2

    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593135
    iget-object p2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p2

    .line 593136
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/C0N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0N;

    move-result-object v2

    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-virtual {v2, v0}, LX/C0N;->a(LX/1Pq;)LX/C0N;

    move-result-object v0

    const v2, 0x7f0b1d7b

    invoke-virtual {v0, v2}, LX/C0N;->h(I)LX/C0N;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 593137
    iget-object v2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;
    .locals 6

    .prologue
    .line 593120
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    monitor-enter v1

    .line 593121
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593122
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593125
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/C0P;->a(LX/0QB;)LX/C0P;

    move-result-object v5

    check-cast v5, LX/C0P;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/C0P;)V

    .line 593126
    move-object v0, p0

    .line 593127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 593119
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 593118
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 593098
    check-cast p1, LX/C33;

    .line 593099
    sget-object v0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object v1, p1, LX/C33;->b:LX/C34;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    iget-object v1, p1, LX/C33;->b:LX/C34;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {p1}, LX/C3N;->b(LX/C33;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 593100
    iget-object v2, p1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593101
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593102
    if-eqz v0, :cond_3

    .line 593103
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593104
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 593105
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593106
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 593107
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593108
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 593109
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593110
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 593111
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 593112
    sget-object p0, LX/C36;->a:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 593113
    const/4 v0, 0x1

    .line 593114
    :goto_1
    move v0, v0

    .line 593115
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 593116
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 593117
    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 593096
    check-cast p1, LX/C33;

    .line 593097
    invoke-virtual {p1}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 593095
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
