.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C33;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/C3E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C3E",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593066
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C3E;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593062
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 593063
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->e:LX/C3E;

    .line 593064
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->f:LX/1V0;

    .line 593065
    return-void
.end method

.method private a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C33;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 593059
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->e:LX/C3E;

    invoke-virtual {v0, p1}, LX/C3E;->c(LX/1De;)LX/C3C;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/C3C;->a(LX/C33;)LX/C3C;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/C3C;->a(LX/1Pb;)LX/C3C;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 593060
    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C33;->b:LX/C34;

    invoke-static {v1, v2}, LX/C31;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    .line 593061
    iget-object v2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;
    .locals 6

    .prologue
    .line 593048
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    monitor-enter v1

    .line 593049
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593050
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593053
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C3E;->a(LX/0QB;)LX/C3E;

    move-result-object v4

    check-cast v4, LX/C3E;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;-><init>(Landroid/content/Context;LX/C3E;LX/1V0;)V

    .line 593054
    move-object v0, p0

    .line 593055
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593056
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593057
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 593067
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 593047
    check-cast p2, LX/C33;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->a(LX/1De;LX/C33;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 593045
    check-cast p1, LX/C33;

    .line 593046
    iget-object v0, p1, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 593043
    check-cast p1, LX/C33;

    .line 593044
    invoke-virtual {p1}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 593042
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
