.class public Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/JQZ;

.field private final f:LX/1V0;

.field private g:LX/1Ua;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595822
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/JQZ;LX/1V0;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595844
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 595845
    iput-object p2, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->e:LX/JQZ;

    .line 595846
    iput-object p3, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->f:LX/1V0;

    .line 595847
    sget-object v0, LX/2eF;->a:LX/1Ua;

    iput-object v0, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->g:LX/1Ua;

    .line 595848
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 595823
    new-instance v1, LX/1X6;

    iget-object v0, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->g:LX/1Ua;

    invoke-direct {v1, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 595824
    iget-object v0, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->e:LX/JQZ;

    const/4 v2, 0x0

    .line 595825
    new-instance v3, LX/JQY;

    invoke-direct {v3, v0}, LX/JQY;-><init>(LX/JQZ;)V

    .line 595826
    iget-object v4, v0, LX/JQZ;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JQX;

    .line 595827
    if-nez v4, :cond_0

    .line 595828
    new-instance v4, LX/JQX;

    invoke-direct {v4, v0}, LX/JQX;-><init>(LX/JQZ;)V

    .line 595829
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JQX;->a$redex0(LX/JQX;LX/1De;IILX/JQY;)V

    .line 595830
    move-object v3, v4

    .line 595831
    move-object v2, v3

    .line 595832
    move-object v2, v2

    .line 595833
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595834
    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 595835
    iget-object v3, v2, LX/JQX;->a:LX/JQY;

    iput-object v0, v3, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 595836
    iget-object v3, v2, LX/JQX;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 595837
    move-object v0, v2

    .line 595838
    iget-object v2, v0, LX/JQX;->a:LX/JQY;

    iput-object p3, v2, LX/JQY;->a:LX/1Pn;

    .line 595839
    iget-object v2, v0, LX/JQX;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 595840
    move-object v0, v0

    .line 595841
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 595842
    iget-object v2, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 595843
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;
    .locals 6

    .prologue
    .line 595810
    const-class v1, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;

    monitor-enter v1

    .line 595811
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595812
    sput-object v2, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595813
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595814
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595815
    new-instance p0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JQZ;->a(LX/0QB;)LX/JQZ;

    move-result-object v4

    check-cast v4, LX/JQZ;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;-><init>(Landroid/content/Context;LX/JQZ;LX/1V0;)V

    .line 595816
    move-object v0, p0

    .line 595817
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595818
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595819
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 595821
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 595809
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/jobsearch/JobSearchComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 595808
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 595807
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595804
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595805
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595806
    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 595802
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595803
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
