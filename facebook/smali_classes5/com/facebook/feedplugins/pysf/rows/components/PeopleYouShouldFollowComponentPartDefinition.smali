.class public Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/JWu;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596899
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/JWu;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596895
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 596896
    iput-object p2, p0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->e:LX/JWu;

    .line 596897
    iput-object p3, p0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->f:LX/1V0;

    .line 596898
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 596877
    iget-object v0, p0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->e:LX/JWu;

    const/4 v1, 0x0

    .line 596878
    new-instance v2, LX/JWt;

    invoke-direct {v2, v0}, LX/JWt;-><init>(LX/JWu;)V

    .line 596879
    iget-object v3, v0, LX/JWu;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JWs;

    .line 596880
    if-nez v3, :cond_0

    .line 596881
    new-instance v3, LX/JWs;

    invoke-direct {v3, v0}, LX/JWs;-><init>(LX/JWu;)V

    .line 596882
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JWs;->a$redex0(LX/JWs;LX/1De;IILX/JWt;)V

    .line 596883
    move-object v2, v3

    .line 596884
    move-object v1, v2

    .line 596885
    move-object v0, v1

    .line 596886
    iget-object v1, v0, LX/JWs;->a:LX/JWt;

    iput-object p3, v1, LX/JWt;->a:LX/1Pb;

    .line 596887
    iget-object v1, v0, LX/JWs;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 596888
    move-object v0, v0

    .line 596889
    iget-object v1, v0, LX/JWs;->a:LX/JWt;

    iput-object p2, v1, LX/JWt;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596890
    iget-object v1, v0, LX/JWs;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 596891
    move-object v0, v0

    .line 596892
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 596893
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 596894
    iget-object v2, p0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;
    .locals 6

    .prologue
    .line 596857
    const-class v1, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;

    monitor-enter v1

    .line 596858
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596859
    sput-object v2, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596860
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596861
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596862
    new-instance p0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JWu;->a(LX/0QB;)LX/JWu;

    move-result-object v4

    check-cast v4, LX/JWu;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;-><init>(Landroid/content/Context;LX/JWu;LX/1V0;)V

    .line 596863
    move-object v0, p0

    .line 596864
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596865
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596866
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596867
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 596876
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 596875
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596871
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596872
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596873
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->n()LX/0Px;

    move-result-object v0

    .line 596874
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 596869
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 596870
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 596868
    sget-object v0, Lcom/facebook/feedplugins/pysf/rows/components/PeopleYouShouldFollowComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
