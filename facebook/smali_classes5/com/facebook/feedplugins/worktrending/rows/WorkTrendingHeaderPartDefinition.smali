.class public Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/String;",
        "LX/1Ps;",
        "LX/JZa;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:LX/1Ua;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598242
    const-class v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598243
    new-instance v0, LX/3Z6;

    invoke-direct {v0}, LX/3Z6;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598244
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598245
    iput-object p1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->e:Landroid/content/res/Resources;

    .line 598246
    iput-object p2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 598247
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p3}, LX/1V7;->d()F

    move-result v1

    neg-float v1, v1

    .line 598248
    iput v1, v0, LX/1UY;->b:F

    .line 598249
    move-object v0, v0

    .line 598250
    invoke-virtual {p3}, LX/1V7;->e()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 598251
    iput v1, v0, LX/1UY;->c:F

    .line 598252
    move-object v0, v0

    .line 598253
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->c:LX/1Ua;

    .line 598254
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;
    .locals 6

    .prologue
    .line 598255
    const-class v1, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    monitor-enter v1

    .line 598256
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598257
    sput-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598258
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598259
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598260
    new-instance p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V7;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V

    .line 598261
    move-object v0, p0

    .line 598262
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598263
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598264
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598265
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JZa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598266
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 598267
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598268
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->c:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598269
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 598270
    check-cast v0, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;

    .line 598271
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;

    if-eqz v1, :cond_0

    .line 598272
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 598273
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 598274
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->e:Landroid/content/res/Resources;

    const v1, 0x7f083ab2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x620e5091

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598275
    check-cast p2, Ljava/lang/String;

    check-cast p4, LX/JZa;

    .line 598276
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 598277
    sget-object v1, LX/2ej;->SPONSORED:LX/2ej;

    invoke-virtual {p4, p2, v1}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 598278
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 598279
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0219a8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598280
    iget-object p2, p4, LX/Bst;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 598281
    const/16 v1, 0x1f

    const v2, 0x699357d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598282
    const/4 v0, 0x1

    return v0
.end method
