.class public Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JZZ;",
        "Ljava/lang/Boolean;",
        "TE;",
        "LX/JZb;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JZb;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1za;

.field private final j:LX/154;

.field public final k:Lcom/facebook/content/SecureContextHelper;

.field public final l:LX/0hy;

.field public final m:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598286
    new-instance v0, LX/3Z7;

    invoke-direct {v0}, LX/3Z7;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a:LX/1Cz;

    .line 598287
    const-class v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Or;LX/1za;LX/154;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/0hy;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            "Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;",
            "LX/1za;",
            "LX/154;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17W;",
            "LX/0hy;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598374
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598375
    iput-object p1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598376
    iput-object p4, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 598377
    iput-object p2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 598378
    iput-object p5, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598379
    iput-object p6, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->h:LX/0Or;

    .line 598380
    iput-object p7, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->i:LX/1za;

    .line 598381
    iput-object p8, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->j:LX/154;

    .line 598382
    iput-object p10, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->k:Lcom/facebook/content/SecureContextHelper;

    .line 598383
    iput-object p12, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->l:LX/0hy;

    .line 598384
    iput-object p13, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->m:Landroid/content/res/Resources;

    .line 598385
    iput-object p3, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    .line 598386
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;
    .locals 3

    .prologue
    .line 598366
    const-class v1, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    monitor-enter v1

    .line 598367
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598368
    sput-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598369
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598370
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 598362
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 598363
    if-eqz v0, :cond_0

    .line 598364
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 598365
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;
    .locals 14

    .prologue
    .line 598360
    new-instance v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const/16 v6, 0x7af

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v7

    check-cast v7, LX/1za;

    invoke-static {p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v8

    check-cast v8, LX/154;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v9

    check-cast v9, LX/17Y;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-static {p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v12

    check-cast v12, LX/0hy;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Or;LX/1za;LX/154;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/0hy;Landroid/content/res/Resources;)V

    .line 598361
    return-object v0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)Landroid/graphics/PointF;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 598354
    invoke-static {p0}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 598355
    if-nez v1, :cond_1

    .line 598356
    :cond_0
    :goto_0
    return-object v0

    .line 598357
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    .line 598358
    if-eqz v1, :cond_0

    .line 598359
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JZb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598353
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 598295
    check-cast p2, LX/JZZ;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 598296
    iget-object v3, p2, LX/JZZ;->a:Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;

    .line 598297
    iget-boolean v0, p2, LX/JZZ;->b:Z

    if-eqz v0, :cond_3

    .line 598298
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    const/4 v5, 0x0

    sget-object v6, LX/1Ua;->d:LX/1Ua;

    sget-object v7, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v4, v5, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598299
    :goto_0
    const v0, 0x7f0d31cf

    iget-object v4, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598300
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 598301
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v5

    .line 598302
    :goto_1
    move-object v5, v5

    .line 598303
    invoke-interface {p1, v0, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598304
    const v0, 0x7f0d31d0

    iget-object v4, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598305
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    .line 598306
    if-eqz v5, :cond_8

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 598307
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLActor;

    .line 598308
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    .line 598309
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    .line 598310
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 598311
    iget-object v7, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->m:Landroid/content/res/Resources;

    const p2, 0x7f083ab5

    invoke-virtual {v7, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 598312
    :cond_0
    :goto_2
    move-object v5, v5

    .line 598313
    invoke-interface {p1, v0, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598314
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 598315
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20D;

    .line 598316
    iget-object v5, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->i:LX/1za;

    const/4 v6, 0x2

    .line 598317
    invoke-virtual {v5, v4}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v7

    .line 598318
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p2

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 598319
    const/4 p3, 0x0

    invoke-virtual {v7, p3, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v7

    move-object v5, v7

    .line 598320
    invoke-virtual {v0, v5}, LX/20D;->a(Ljava/util/List;)V

    .line 598321
    invoke-virtual {v0, v1}, LX/20D;->a(Z)V

    .line 598322
    const v5, 0x7f0d31cd

    iget-object v6, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->f:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-interface {p1, v5, v6, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598323
    invoke-static {v4}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    .line 598324
    iget-object v4, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->j:LX/154;

    invoke-virtual {v4, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 598325
    iget-object v5, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->m:Landroid/content/res/Resources;

    const v6, 0x7f0f00c1

    new-array v7, v1, [Ljava/lang/Object;

    aput-object v4, v7, v2

    invoke-virtual {v5, v6, v0, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 598326
    const v4, 0x7f0d31ce

    iget-object v5, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v4, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598327
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->g:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v4, LX/JZY;

    invoke-direct {v4, p0, v3}, LX/JZY;-><init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)V

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598328
    const/4 v0, 0x0

    .line 598329
    invoke-static {v3}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 598330
    if-nez v4, :cond_9

    .line 598331
    :cond_1
    :goto_3
    move-object v4, v0

    .line 598332
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 598333
    :goto_4
    if-eqz v0, :cond_2

    .line 598334
    const v1, 0x7f0d31d1

    iget-object v2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v5, LX/2f8;

    invoke-direct {v5}, LX/2f8;-><init>()V

    sget-object v6, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 598335
    iput-object v6, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 598336
    move-object v5, v5

    .line 598337
    invoke-virtual {v5, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v4

    invoke-static {v3}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;)Landroid/graphics/PointF;

    move-result-object v3

    .line 598338
    iput-object v3, v4, LX/2f8;->h:Landroid/graphics/PointF;

    .line 598339
    move-object v3, v4

    .line 598340
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598341
    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 598342
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v4, v5}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 598343
    goto :goto_4

    .line 598344
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 598345
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 598346
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 598347
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 598348
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 598349
    :cond_7
    const-string v5, ""

    goto/16 :goto_1

    :cond_8
    const-string v5, ""

    goto/16 :goto_2

    .line 598350
    :cond_9
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 598351
    if-eqz v4, :cond_1

    .line 598352
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x35e41c3b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598289
    check-cast p1, LX/JZZ;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, LX/JZb;

    .line 598290
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598291
    iget-object v1, p4, LX/JZb;->b:Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 598292
    :cond_0
    iget-boolean v1, p1, LX/JZZ;->b:Z

    if-eqz v1, :cond_1

    .line 598293
    const/4 v1, 0x1

    iput-boolean v1, p4, LX/JZb;->d:Z

    .line 598294
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x587ff7e8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598288
    const/4 v0, 0x1

    return v0
.end method
