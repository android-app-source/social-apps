.class public Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JZX;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final c:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field private d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598389
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 598390
    iput v1, v0, LX/1UY;->b:F

    .line 598391
    move-object v0, v0

    .line 598392
    const/4 v1, 0x0

    .line 598393
    iput v1, v0, LX/1UY;->c:F

    .line 598394
    move-object v0, v0

    .line 598395
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->c:LX/1Ua;

    .line 598396
    new-instance v0, LX/3Z8;

    invoke-direct {v0}, LX/3Z8;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598397
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598398
    iput-object p1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 598399
    iput-object p2, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598400
    iput-object p3, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->b:Landroid/content/res/Resources;

    .line 598401
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;
    .locals 6

    .prologue
    .line 598402
    const-class v1, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    monitor-enter v1

    .line 598403
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598404
    sput-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598405
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598406
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598407
    new-instance p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;)V

    .line 598408
    move-object v0, p0

    .line 598409
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598410
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598411
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598413
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 598414
    check-cast p2, LX/JZX;

    const/4 v4, 0x0

    .line 598415
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->c:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598416
    iget-object v0, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/JZW;

    invoke-direct {v1, p0, p2}, LX/JZW;-><init>(Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;LX/JZX;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598417
    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1987411f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598418
    check-cast p1, LX/JZX;

    check-cast p4, Landroid/widget/TextView;

    .line 598419
    iget-boolean v1, p1, LX/JZX;->b:Z

    if-eqz v1, :cond_0

    .line 598420
    iget-object v1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->b:Landroid/content/res/Resources;

    const v2, 0x7f083ab4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598421
    :goto_0
    const/16 v1, 0x1f

    const v2, -0xf04bea2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 598422
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->b:Landroid/content/res/Resources;

    const v2, 0x7f083ab3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598423
    const/4 v0, 0x1

    return v0
.end method
