.class public Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/JMq;

.field private final f:LX/1V0;

.field private g:LX/1Ua;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592932
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/JMq;LX/1V0;LX/1V7;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 592924
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 592925
    iput-object p2, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->e:LX/JMq;

    .line 592926
    iput-object p3, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->f:LX/1V0;

    .line 592927
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {p4}, LX/1V7;->d()F

    move-result v1

    .line 592928
    iput v1, v0, LX/1UY;->b:F

    .line 592929
    move-object v0, v0

    .line 592930
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->g:LX/1Ua;

    .line 592931
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 592905
    new-instance v0, LX/1X6;

    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->g:LX/1Ua;

    invoke-direct {v0, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 592906
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->e:LX/JMq;

    const/4 v2, 0x0

    .line 592907
    new-instance v3, LX/JMo;

    invoke-direct {v3, v1}, LX/JMo;-><init>(LX/JMq;)V

    .line 592908
    iget-object v4, v1, LX/JMq;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JMp;

    .line 592909
    if-nez v4, :cond_0

    .line 592910
    new-instance v4, LX/JMp;

    invoke-direct {v4, v1}, LX/JMp;-><init>(LX/JMq;)V

    .line 592911
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JMp;->a$redex0(LX/JMp;LX/1De;IILX/JMo;)V

    .line 592912
    move-object v3, v4

    .line 592913
    move-object v2, v3

    .line 592914
    move-object v1, v2

    .line 592915
    iget-object v2, v1, LX/JMp;->a:LX/JMo;

    iput-object p2, v2, LX/JMo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592916
    iget-object v2, v1, LX/JMp;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 592917
    move-object v1, v1

    .line 592918
    iget-object v2, v1, LX/JMp;->a:LX/JMo;

    iput-object p3, v2, LX/JMo;->b:LX/1Pn;

    .line 592919
    iget-object v2, v1, LX/JMp;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 592920
    move-object v1, v1

    .line 592921
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 592922
    iget-object v2, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 592923
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;
    .locals 7

    .prologue
    .line 592894
    const-class v1, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    monitor-enter v1

    .line 592895
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 592896
    sput-object v2, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 592897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 592899
    new-instance p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JMq;->a(LX/0QB;)LX/JMq;

    move-result-object v4

    check-cast v4, LX/JMq;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V7;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JMq;LX/1V0;LX/1V7;)V

    .line 592900
    move-object v0, p0

    .line 592901
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 592902
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592903
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 592904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 592887
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 592893
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 592891
    const/4 v0, 0x1

    move v0, v0

    .line 592892
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 592889
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 592890
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 592888
    sget-object v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
