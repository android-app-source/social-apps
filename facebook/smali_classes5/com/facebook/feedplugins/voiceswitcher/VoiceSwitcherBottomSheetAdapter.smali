.class public Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CDp;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/CDo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/CDp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629107
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 629108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    .line 629109
    iput-object p1, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->a:Landroid/content/Context;

    .line 629110
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 629111
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid view type for creating view holder."

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 629112
    new-instance v0, LX/CDk;

    .line 629113
    iget-object v1, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0315e4

    const/4 p2, 0x0

    invoke-virtual {v1, v2, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v1, v1

    .line 629114
    invoke-direct {v0, v1}, LX/CDk;-><init>(Landroid/view/View;)V

    return-object v0

    .line 629115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 629116
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid view type for binding view holder."

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 629117
    check-cast p1, LX/CDk;

    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDp;

    .line 629118
    iget-object v1, v0, LX/CDp;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 629119
    iget-object v1, p1, LX/CDk;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, v0, LX/CDp;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-class p2, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 629120
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->d:LX/CDp;

    if-ne v0, v1, :cond_2

    const/4 v1, 0x0

    .line 629121
    :goto_1
    iget-object v2, p1, LX/CDk;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 629122
    iget-object v1, p1, LX/CDk;->l:Lcom/facebook/fbui/widget/text/BadgeTextView;

    iget-object v2, v0, LX/CDp;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 629123
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/CDj;

    invoke-direct {v2, p0, v0}, LX/CDj;-><init>(Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;LX/CDp;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 629124
    return-void

    .line 629125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 629126
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 629127
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDp;

    .line 629128
    iget-object v2, v0, LX/CDp;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629129
    iput-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->d:LX/CDp;

    .line 629130
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 629131
    :cond_1
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 629132
    const/4 v0, 0x0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 629133
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherBottomSheetAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
