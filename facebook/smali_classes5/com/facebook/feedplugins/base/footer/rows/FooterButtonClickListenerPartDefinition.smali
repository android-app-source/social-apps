.class public Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/20Z;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631974
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 631975
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;
    .locals 3

    .prologue
    .line 631976
    const-class v1, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    monitor-enter v1

    .line 631977
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631978
    sput-object v2, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631979
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631980
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 631981
    new-instance v0, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;-><init>()V

    .line 631982
    move-object v0, v0

    .line 631983
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631984
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631985
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x21310984

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 631987
    check-cast p1, LX/20Z;

    .line 631988
    check-cast p4, LX/1wK;

    invoke-interface {p4, p1}, LX/1wK;->setOnButtonClickedListener(LX/20Z;)V

    .line 631989
    const/16 v1, 0x1f

    const v2, 0x4dac22ec    # 3.60996224E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 631990
    check-cast p4, LX/1wK;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/1wK;->setOnButtonClickedListener(LX/20Z;)V

    .line 631991
    return-void
.end method
