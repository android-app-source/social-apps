.class public Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindPagesFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/JNY;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:LX/HR7;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593448
    new-instance v0, LX/3XW;

    invoke-direct {v0}, LX/3XW;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/HR7;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593440
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593441
    iput-object p1, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->b:Landroid/content/Context;

    .line 593442
    iput-object p2, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 593443
    iput-object p3, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->d:LX/HR7;

    .line 593444
    iput-object p4, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 593445
    iput-object p5, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 593446
    iput-object p6, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 593447
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;
    .locals 10

    .prologue
    .line 593429
    const-class v1, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;

    monitor-enter v1

    .line 593430
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593431
    sput-object v2, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593432
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593433
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593434
    new-instance v3, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/HR7;->a(LX/0QB;)LX/HR7;

    move-result-object v6

    check-cast v6, LX/HR7;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/HR7;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 593435
    move-object v0, v3

    .line 593436
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593437
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593438
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593428
    sget-object v0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 593417
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593418
    iget-object v0, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 593419
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 593420
    iget-object v1, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->d:LX/HR7;

    invoke-virtual {v1, v0}, LX/HR7;->a(Ljava/lang/String;)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v0

    .line 593421
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 593422
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->b:Landroid/content/Context;

    const v2, 0x7f083a7c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 593423
    const v1, 0x7f0d115d

    iget-object v2, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593424
    const v0, 0x7f0d115e

    iget-object v1, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/JNX;

    invoke-direct {v2, p0}, LX/JNX;-><init>(Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593425
    const/4 v0, 0x0

    return-object v0

    .line 593426
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/findpages/FindPagesPartDefinition;->b:Landroid/content/Context;

    const v1, 0x7f083a7a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 593427
    const/4 v0, 0x1

    return v0
.end method
