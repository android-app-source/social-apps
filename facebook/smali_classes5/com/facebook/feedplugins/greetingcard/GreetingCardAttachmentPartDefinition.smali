.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/feedplugins/greetingcard/GreetingCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595078
    new-instance v0, LX/3Y0;

    invoke-direct {v0}, LX/3Y0;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595079
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595080
    iput-object p1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595081
    iput-object p2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    .line 595082
    iput-object p3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->d:LX/0Or;

    .line 595083
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 595084
    const-class v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;

    monitor-enter v1

    .line 595085
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595086
    sput-object v2, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595089
    new-instance v5, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/0Or;)V

    .line 595090
    move-object v0, v5

    .line 595091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595095
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 595096
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 595097
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595098
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595099
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 595100
    if-nez v3, :cond_1

    move-object v1, v2

    .line 595101
    :goto_0
    iget-object v4, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v5, LX/1X6;

    sget-object v6, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v5, v3, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595102
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 595103
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 595104
    iget-object v3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    new-instance v4, LX/JOK;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 595105
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, -0x29f300f7

    if-eq v5, v6, :cond_3

    .line 595106
    :cond_0
    const/4 v5, 0x0

    .line 595107
    :goto_2
    move-object v0, v5

    .line 595108
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v4, v0, v5, v1}, LX/JOK;-><init>(Lcom/facebook/graphql/model/GraphQLGreetingCard;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595109
    return-object v2

    .line 595110
    :cond_1
    iget-object v1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 595111
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    .line 595112
    :cond_2
    const-string v1, ""

    move-object v3, v1

    goto :goto_1

    .line 595113
    :cond_3
    new-instance v5, LX/4Wk;

    invoke-direct {v5}, LX/4Wk;-><init>()V

    .line 595114
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dE()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v6

    .line 595115
    iput-object v6, v5, LX/4Wk;->b:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 595116
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    .line 595117
    iput-object v6, v5, LX/4Wk;->c:Ljava/lang/String;

    .line 595118
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ha()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 595119
    iput-object v6, v5, LX/4Wk;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 595120
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iE()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v6

    .line 595121
    iput-object v6, v5, LX/4Wk;->e:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 595122
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jx()Ljava/lang/String;

    move-result-object v6

    .line 595123
    iput-object v6, v5, LX/4Wk;->f:Ljava/lang/String;

    .line 595124
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v6

    .line 595125
    iput-object v6, v5, LX/4Wk;->g:Ljava/lang/String;

    .line 595126
    new-instance v6, Lcom/facebook/graphql/model/GraphQLGreetingCard;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLGreetingCard;-><init>(LX/4Wk;)V

    .line 595127
    move-object v5, v6

    .line 595128
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595129
    const/4 v0, 0x1

    return v0
.end method
