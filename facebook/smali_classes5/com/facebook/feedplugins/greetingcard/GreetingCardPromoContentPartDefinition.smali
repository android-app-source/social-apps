.class public Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/feedplugins/greetingcard/GreetingCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/1V7;

.field private final d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595156
    new-instance v0, LX/3Y1;

    invoke-direct {v0}, LX/3Y1;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595151
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595152
    iput-object p1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595153
    iput-object p2, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->c:LX/1V7;

    .line 595154
    iput-object p3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    .line 595155
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;
    .locals 6

    .prologue
    .line 595140
    const-class v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    monitor-enter v1

    .line 595141
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595142
    sput-object v2, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595143
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595144
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595145
    new-instance p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;)V

    .line 595146
    move-object v0, p0

    .line 595147
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595148
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595149
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595150
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595132
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 595134
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x1

    .line 595135
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595136
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 595137
    iget-object v1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    iget-object v3, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->c:LX/1V7;

    invoke-virtual {v3}, LX/1V7;->h()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595138
    iget-object v1, p0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPromoContentPartDefinition;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    new-instance v2, LX/JOK;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGreetingCard;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, LX/JOK;-><init>(Lcom/facebook/graphql/model/GraphQLGreetingCard;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595139
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595133
    const/4 v0, 0x1

    return v0
.end method
