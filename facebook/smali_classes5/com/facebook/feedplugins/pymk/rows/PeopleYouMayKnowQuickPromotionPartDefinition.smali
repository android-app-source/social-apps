.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;",
        "LX/DEx;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final c:LX/DEt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596811
    const v0, 0x7f030f26

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/DEt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596812
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 596813
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 596814
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->c:LX/DEt;

    .line 596815
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;
    .locals 5

    .prologue
    .line 596816
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;

    monitor-enter v1

    .line 596817
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596818
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596819
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596820
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 596821
    new-instance p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/DEt;->a(LX/0QB;)LX/DEt;

    move-result-object v4

    check-cast v4, LX/DEt;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/DEt;)V

    .line 596822
    move-object v0, p0

    .line 596823
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596824
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596825
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596826
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 596827
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 596828
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/high16 v3, -0x3f000000    # -8.0f

    .line 596829
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 596830
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 596831
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    .line 596832
    iput v3, v1, LX/1UY;->c:F

    .line 596833
    move-object v1, v1

    .line 596834
    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v2

    if-nez v2, :cond_0

    .line 596835
    iput v3, v1, LX/1UY;->b:F

    .line 596836
    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    invoke-direct {v3, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 596837
    new-instance v2, LX/2e7;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v3, LX/2e8;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-direct {v2, v1, v3}, LX/2e7;-><init>(Ljava/lang/String;LX/2e8;)V

    .line 596838
    new-instance v3, LX/DEx;

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2e8;

    new-instance v4, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition$1;

    invoke-direct {v4, p0, p3, v2, v0}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition$1;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;LX/1Ps;LX/2e7;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    invoke-direct {v3, v1, v4}, LX/DEx;-><init>(LX/2e8;Ljava/lang/Runnable;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2f398d55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 596839
    check-cast p2, LX/DEx;

    check-cast p4, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 596840
    iget-object v1, p2, LX/DEx;->a:LX/2e8;

    iget-boolean v1, v1, LX/2e8;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p2, LX/DEx;->a:LX/2e8;

    iget-object v1, v1, LX/2e8;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    if-eqz v1, :cond_0

    .line 596841
    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowQuickPromotionPartDefinition;->c:LX/DEt;

    iget-object v2, p2, LX/DEx;->a:LX/2e8;

    iget-object v2, v2, LX/2e8;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v4, p2, LX/DEx;->b:Ljava/lang/Runnable;

    const/4 p0, 0x0

    .line 596842
    const-class p1, LX/3ka;

    .line 596843
    iget-object p3, v1, LX/DEt;->a:LX/0iA;

    invoke-virtual {p3, v2, p1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object p3

    check-cast p3, LX/13D;

    .line 596844
    if-nez p3, :cond_3

    const/4 p3, 0x0

    :goto_0
    move-object p1, p3

    .line 596845
    if-nez p1, :cond_1

    .line 596846
    :cond_0
    :goto_1
    const/16 v1, 0x1f

    const v2, -0x3a123459

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 596847
    :cond_1
    iget-object p3, v1, LX/DEt;->b:LX/2hf;

    invoke-virtual {p3, p1}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object p3

    .line 596848
    instance-of p1, p3, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;

    if-eqz p1, :cond_2

    move-object p1, p3

    .line 596849
    check-cast p1, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;

    .line 596850
    iput-object v4, p1, Lcom/facebook/feedplugins/pymk/quickpromotion/QuickPromotionFeedPYMKFragment;->a:Ljava/lang/Runnable;

    .line 596851
    new-instance p1, LX/3qV;

    invoke-direct {p1, p4}, LX/3qV;-><init>(Landroid/view/ViewGroup;)V

    .line 596852
    invoke-virtual {p1}, LX/3p4;->c()V

    .line 596853
    invoke-virtual {p1}, LX/0k3;->p()LX/0gc;

    move-result-object p1

    invoke-virtual {p1}, LX/0gc;->a()LX/0hH;

    move-result-object p1

    const p2, 0x7f0400a9

    invoke-virtual {p1, p2, p0}, LX/0hH;->a(II)LX/0hH;

    move-result-object p1

    const p0, 0x7f0d24bf

    invoke-virtual {p1, p0, p3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p1

    invoke-virtual {p1}, LX/0hH;->b()I

    .line 596854
    goto :goto_1

    .line 596855
    :cond_2
    goto :goto_1

    :cond_3
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p3, p2}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 596856
    const/4 v0, 0x1

    return v0
.end method
