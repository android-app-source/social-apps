.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

.field private final c:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631898
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 631899
    iput-object p1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    .line 631900
    iput-object p2, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    .line 631901
    iput-object p3, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->c:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 631902
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;
    .locals 6

    .prologue
    .line 631887
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    monitor-enter v1

    .line 631888
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631889
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631890
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631891
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 631892
    new-instance p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;-><init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V

    .line 631893
    move-object v0, p0

    .line 631894
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631895
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631896
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 631904
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 631905
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 631906
    invoke-static {p2}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631907
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->c:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 631908
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 631903
    const/4 v0, 0x1

    return v0
.end method
