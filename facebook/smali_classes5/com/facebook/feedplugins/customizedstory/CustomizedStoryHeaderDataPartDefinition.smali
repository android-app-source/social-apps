.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        "LX/DCl;",
        "LX/1PW;",
        "LX/C3y;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 631909
    const-class v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631910
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 631911
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;
    .locals 3

    .prologue
    .line 631912
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    monitor-enter v1

    .line 631913
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631914
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631915
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631916
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 631917
    new-instance v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;-><init>()V

    .line 631918
    move-object v0, v0

    .line 631919
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631920
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631921
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631922
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 631923
    check-cast p2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    const/4 v5, 0x0

    .line 631924
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-nez v0, :cond_1

    .line 631925
    :cond_0
    const-string v1, ""

    .line 631926
    const-string v2, ""

    move-object v4, v5

    move-object v3, v5

    .line 631927
    :goto_0
    new-instance v0, LX/DCl;

    invoke-direct/range {v0 .. v5}, LX/DCl;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)V

    return-object v0

    .line 631928
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_2

    .line 631929
    const-string v0, ""

    .line 631930
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_3

    .line 631931
    const-string v1, ""

    .line 631932
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 631933
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 631934
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 631935
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 631936
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1ab0259f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 631937
    check-cast p2, LX/DCl;

    check-cast p4, LX/C3y;

    const/4 p0, 0x1

    .line 631938
    invoke-virtual {p4, p0}, LX/C3y;->setMenuButtonActive(Z)V

    .line 631939
    iget-object v1, p2, LX/DCl;->d:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, p0, v2}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 631940
    iget-object v1, p2, LX/DCl;->a:Ljava/lang/String;

    iget-object v2, p2, LX/DCl;->b:Ljava/lang/CharSequence;

    iget-object p0, p2, LX/DCl;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p4, v1, v2, p0}, LX/C3y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 631941
    iget-object v1, p2, LX/DCl;->e:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 631942
    const/16 v1, 0x1f

    const v2, -0x60ebecd3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
