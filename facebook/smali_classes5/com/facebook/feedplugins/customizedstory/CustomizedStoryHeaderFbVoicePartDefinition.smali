.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/C3y;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593226
    new-instance v0, LX/3XR;

    invoke-direct {v0}, LX/3XR;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593221
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593222
    iput-object p1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593223
    iput-object p2, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 593224
    iput-object p3, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    .line 593225
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;
    .locals 6

    .prologue
    .line 593210
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    monitor-enter v1

    .line 593211
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593212
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593215
    new-instance p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;)V

    .line 593216
    move-object v0, p0

    .line 593217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593195
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 593202
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pk;

    .line 593203
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593204
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 593205
    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->c:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderDataPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593206
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593207
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderFbVoicePartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    :goto_0
    invoke-direct {v3, p2, v0}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593208
    const/4 v0, 0x0

    return-object v0

    .line 593209
    :cond_0
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 593196
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593197
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593198
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 593199
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 593200
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 593201
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
