.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Kf;

.field private final d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;


# direct methods
.method public constructor <init>(LX/1Kf;Landroid/app/Activity;LX/0Ot;Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Kf;",
            "Landroid/app/Activity;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631967
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 631968
    iput-object p1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->c:LX/1Kf;

    .line 631969
    iput-object p2, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->a:Landroid/app/Activity;

    .line 631970
    iput-object p3, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->b:LX/0Ot;

    .line 631971
    iput-object p4, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    .line 631972
    iput-object p5, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 631973
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;
    .locals 9

    .prologue
    .line 631956
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    monitor-enter v1

    .line 631957
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631958
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 631961
    new-instance v3, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v4

    check-cast v4, LX/1Kf;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;-><init>(LX/1Kf;Landroid/app/Activity;LX/0Ot;Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;)V

    .line 631962
    move-object v0, v3

    .line 631963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLCustomizedStory;)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 1

    .prologue
    .line 631955
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->u()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 631954
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 631943
    check-cast p2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    check-cast p3, LX/1Po;

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 631944
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 631945
    new-instance v2, LX/DCk;

    invoke-direct {v2, p0, p2, p3}, LX/DCk;-><init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLCustomizedStory;LX/1Po;)V

    move-object v2, v2

    .line 631946
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 631947
    iget-object v6, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    new-instance v0, LX/21P;

    const/4 v3, 0x1

    sget-object v4, LX/1Wi;->TOP:LX/1Wi;

    move v2, v1

    invoke-direct/range {v0 .. v5}, LX/21P;-><init>(ZZZLX/1Wi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 631948
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x737a2695

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 631951
    check-cast p4, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    .line 631952
    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setButtons(Ljava/util/Set;)V

    .line 631953
    const/16 v1, 0x1f

    const v2, -0x5f41438a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 631949
    check-cast p1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631950
    invoke-static {p1}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLCustomizedStory;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
