.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631822
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 631823
    iput-object p1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    .line 631824
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;
    .locals 4

    .prologue
    .line 631825
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;

    monitor-enter v1

    .line 631826
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631827
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631828
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631829
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 631830
    new-instance p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;-><init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;)V

    .line 631831
    move-object v0, p0

    .line 631832
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 631836
    check-cast p2, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631837
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 631838
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 631839
    check-cast p1, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631840
    iget-object v0, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
