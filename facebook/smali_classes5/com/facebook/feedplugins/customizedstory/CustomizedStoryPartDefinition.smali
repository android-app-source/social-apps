.class public Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final c:Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

.field private final d:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 631858
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 631859
    iput-object p3, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    .line 631860
    iput-object p2, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->b:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 631861
    iput-object p4, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->c:Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    .line 631862
    iput-object p1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->d:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    .line 631863
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;
    .locals 7

    .prologue
    .line 631842
    const-class v1, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    monitor-enter v1

    .line 631843
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 631844
    sput-object v2, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 631845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 631847
    new-instance p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;)V

    .line 631848
    move-object v0, p0

    .line 631849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 631850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 631852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 631864
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 631865
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 631866
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631867
    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->c:Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 631868
    :goto_0
    return-object v3

    .line 631869
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->d:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryHeaderSelector;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 631870
    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->b:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 631871
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->g()Ljava/lang/String;

    move-result-object p2

    .line 631872
    iput-object p2, v2, LX/23u;->m:Ljava/lang/String;

    .line 631873
    move-object v2, v2

    .line 631874
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p2

    .line 631875
    iput-object p2, v2, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 631876
    move-object v2, v2

    .line 631877
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object p2

    .line 631878
    iput-object p2, v2, LX/23u;->k:LX/0Px;

    .line 631879
    move-object v2, v2

    .line 631880
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->c()Ljava/lang/String;

    move-result-object p2

    .line 631881
    iput-object p2, v2, LX/23u;->aM:Ljava/lang/String;

    .line 631882
    move-object v2, v2

    .line 631883
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 631884
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v2, v2

    .line 631885
    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 631886
    iget-object v1, p0, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->a:Lcom/facebook/feedplugins/customizedstory/CustomizedStoryFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 631853
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 631854
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631855
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-nez v1, :cond_0

    .line 631856
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->q()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 631857
    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 631841
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/customizedstory/CustomizedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
