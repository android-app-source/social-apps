.class public Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628873
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 628874
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    .line 628875
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 628876
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->c:LX/0Ot;

    .line 628877
    iput-object p4, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->d:LX/0Ot;

    .line 628878
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;
    .locals 7

    .prologue
    .line 628879
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    monitor-enter v1

    .line 628880
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 628881
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 628882
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628883
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 628884
    new-instance v5, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    const/16 v6, 0xbca

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xbd2

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;LX/0Ot;LX/0Ot;)V

    .line 628885
    move-object v0, v5

    .line 628886
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 628887
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628888
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 628889
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 628890
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628891
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628892
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 628893
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x72c76556

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628894
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 628895
    sget-boolean v1, LX/3nQ;->a:Z

    if-nez v1, :cond_0

    if-eqz p3, :cond_0

    move-object v1, p3

    check-cast v1, LX/1Po;

    invoke-interface {v1}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v1, v2, :cond_0

    .line 628896
    iget v1, p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->D:I

    move v1, v1

    .line 628897
    if-nez v1, :cond_1

    .line 628898
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x3c9afdf6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 628899
    :cond_1
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 628900
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 628901
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 628902
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 628903
    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getBottom()I

    move-result v1

    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0a41

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getTop()I

    move-result v1

    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0a42

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 628904
    const/4 v1, 0x1

    sput-boolean v1, LX/3nQ;->a:Z

    .line 628905
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iA;

    const-string v2, "4475"

    const-class v4, LX/3nQ;

    invoke-virtual {v1, v2, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3nQ;

    .line 628906
    if-eqz v1, :cond_0

    .line 628907
    iput-object p4, v1, LX/3nQ;->b:Landroid/view/View;

    .line 628908
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15W;

    invoke-virtual {p4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_RANK_FEED_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 628909
    const-class p2, LX/0i1;

    const/4 p0, 0x0

    invoke-virtual {v1, v2, v4, p2, p0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 628910
    goto :goto_0
.end method
