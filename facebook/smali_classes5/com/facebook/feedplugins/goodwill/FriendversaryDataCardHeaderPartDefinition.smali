.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final g:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594116
    const v0, 0x7f0307d8

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->a:LX/1Cz;

    .line 594117
    const-class v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594118
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594119
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594120
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 594121
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 594122
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    .line 594123
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594124
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;
    .locals 7

    .prologue
    .line 594125
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    monitor-enter v1

    .line 594126
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594127
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594130
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 594131
    move-object v0, p0

    .line 594132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594136
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 594137
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594138
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594140
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->q()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594141
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->q()LX/0Px;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594142
    const v2, 0x7f0d05d3

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v4

    sget-object v5, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594143
    iput-object v5, v4, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594144
    move-object v4, v4

    .line 594145
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-virtual {v4, v5, v1}, LX/2f8;->a(II)LX/2f8;

    move-result-object v1

    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594146
    const v1, 0x7f0d05d4

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594147
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594148
    move-object v3, v3

    .line 594149
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    invoke-virtual {v3, v4, v0}, LX/2f8;->a(II)LX/2f8;

    move-result-object v0

    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594150
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->c:LX/1Ua;

    const v3, 0x7f0207fa

    const/4 v4, -0x1

    invoke-direct {v1, p2, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594151
    const v0, 0x7f0d0bde

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v2, LX/24b;

    sget-object v3, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v2, p2, v3}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594152
    const v0, 0x7f0d14ba

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594153
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 594154
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 594155
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594156
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594157
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->q()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->q()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v2, v4, :cond_1

    :cond_0
    move v0, v1

    .line 594158
    :goto_0
    return v0

    .line 594159
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->q()LX/0Px;

    move-result-object v3

    move v2, v1

    .line 594160
    :goto_1
    if-ge v2, v4, :cond_4

    .line 594161
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594162
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 594163
    goto :goto_0

    .line 594164
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 594165
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method
