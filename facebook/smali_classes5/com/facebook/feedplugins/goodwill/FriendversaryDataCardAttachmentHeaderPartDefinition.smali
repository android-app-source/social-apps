.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594232
    const v0, 0x7f0307d6

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->a:LX/1Cz;

    .line 594233
    const-class v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594234
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594235
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 594236
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594237
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;

    .line 594238
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;
    .locals 6

    .prologue
    .line 594239
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    monitor-enter v1

    .line 594240
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594241
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594242
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594243
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594244
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;)V

    .line 594245
    move-object v0, p0

    .line 594246
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594247
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594248
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594250
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 594251
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594252
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594253
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 594254
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594255
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594256
    const v2, 0x7f0d05d3

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v4

    sget-object v5, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 594257
    iput-object v5, v4, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594258
    move-object v4, v4

    .line 594259
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-virtual {v4, v5, v1}, LX/2f8;->a(II)LX/2f8;

    move-result-object v1

    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594260
    const v1, 0x7f0d05d4

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 594261
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594262
    move-object v3, v3

    .line 594263
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    invoke-virtual {v3, v4, v0}, LX/2f8;->a(II)LX/2f8;

    move-result-object v0

    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594264
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594265
    const v0, 0x7f0d14ba

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPolaroidPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594266
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 594267
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 594268
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594269
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 594270
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-nez v1, :cond_0

    move v0, v2

    .line 594271
    :goto_0
    return v0

    .line 594272
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->jC()LX/0Px;

    move-result-object v4

    .line 594273
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ge v1, v6, :cond_2

    :cond_1
    move v0, v2

    .line 594274
    goto :goto_0

    :cond_2
    move v3, v2

    .line 594275
    :goto_1
    if-ge v3, v6, :cond_5

    .line 594276
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 594277
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    :cond_3
    move v0, v2

    .line 594278
    goto :goto_0

    .line 594279
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 594280
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v4

    .line 594281
    if-eqz v4, :cond_6

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ge v1, v6, :cond_7

    :cond_6
    move v0, v2

    .line 594282
    goto :goto_0

    :cond_7
    move v3, v2

    .line 594283
    :goto_2
    if-ge v3, v6, :cond_a

    .line 594284
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 594285
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    :cond_8
    move v0, v2

    .line 594286
    goto :goto_0

    .line 594287
    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 594288
    :cond_a
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_c

    :cond_b
    move v0, v2

    .line 594289
    goto :goto_0

    .line 594290
    :cond_c
    const/4 v0, 0x1

    goto :goto_0
.end method
