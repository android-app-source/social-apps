.class public Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/JNl;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594624
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/JNl;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594620
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 594621
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->e:LX/JNl;

    .line 594622
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->f:LX/1V0;

    .line 594623
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 594583
    const/4 v2, 0x0

    .line 594584
    if-nez p2, :cond_2

    move v0, v2

    .line 594585
    :goto_0
    move v0, v0

    .line 594586
    if-nez v0, :cond_0

    .line 594587
    invoke-static {p1}, LX/JNs;->c(LX/1De;)LX/JNq;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 594588
    :goto_1
    return-object v0

    .line 594589
    :cond_0
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v0, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 594590
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->e:LX/JNl;

    const/4 v2, 0x0

    .line 594591
    new-instance v3, LX/JNk;

    invoke-direct {v3, v1}, LX/JNk;-><init>(LX/JNl;)V

    .line 594592
    iget-object v4, v1, LX/JNl;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JNj;

    .line 594593
    if-nez v4, :cond_1

    .line 594594
    new-instance v4, LX/JNj;

    invoke-direct {v4, v1}, LX/JNj;-><init>(LX/JNl;)V

    .line 594595
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/JNj;->a$redex0(LX/JNj;LX/1De;IILX/JNk;)V

    .line 594596
    move-object v3, v4

    .line 594597
    move-object v2, v3

    .line 594598
    move-object v1, v2

    .line 594599
    iget-object v2, v1, LX/JNj;->a:LX/JNk;

    iput-object p3, v2, LX/JNk;->b:LX/1Pb;

    .line 594600
    iget-object v2, v1, LX/JNj;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 594601
    move-object v1, v1

    .line 594602
    iget-object v2, v1, LX/JNj;->a:LX/JNk;

    iput-object p2, v2, LX/JNk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594603
    iget-object v2, v1, LX/JNj;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 594604
    move-object v1, v1

    .line 594605
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 594606
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    goto :goto_1

    .line 594607
    :cond_2
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594608
    check-cast v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 594609
    if-nez v0, :cond_3

    move v0, v2

    .line 594610
    goto :goto_0

    .line 594611
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v4

    .line 594612
    if-eqz v4, :cond_4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v2

    .line 594613
    goto :goto_0

    .line 594614
    :cond_5
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_2
    if-ge v3, v5, :cond_6

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 594615
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 594616
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->X()Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->m()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->V()Z

    move-result v0

    if-nez v0, :cond_8

    .line 594617
    add-int/lit8 v0, v1, 0x1

    .line 594618
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    .line 594619
    :cond_6
    if-eqz v1, :cond_7

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;
    .locals 6

    .prologue
    .line 594572
    const-class v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;

    monitor-enter v1

    .line 594573
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594574
    sput-object v2, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594575
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594576
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594577
    new-instance p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JNl;->a(LX/0QB;)LX/JNl;

    move-result-object v4

    check-cast v4, LX/JNl;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;-><init>(Landroid/content/Context;LX/JNl;LX/1V0;)V

    .line 594578
    move-object v0, p0

    .line 594579
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594580
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594581
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 594571
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 594567
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594570
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 594568
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594569
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
