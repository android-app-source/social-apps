.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:[I

.field private static i:LX/0Xm;


# instance fields
.field public final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/17W;

.field public final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final f:Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

.field public final g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field public final h:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593960
    const v0, 0x7f0314b9

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a:LX/1Cz;

    .line 593961
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->b:[I

    return-void

    :array_0
    .array-data 4
        -0x11f22ab2
        0x4ddbba3c    # 4.6080192E8f
    .end array-data
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593952
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593953
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 593954
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->d:LX/17W;

    .line 593955
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593956
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    .line 593957
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->h:Landroid/content/res/Resources;

    .line 593958
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 593959
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;
    .locals 10

    .prologue
    .line 593941
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    monitor-enter v1

    .line 593942
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593943
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593946
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Landroid/content/res/Resources;)V

    .line 593947
    move-object v0, v3

    .line 593948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593902
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 593911
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    const/4 p3, -0x1

    const/high16 v10, -0x3f000000    # -8.0f

    const/high16 v9, -0x40000000    # -2.0f

    .line 593912
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593913
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593914
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->D()LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->b:[I

    invoke-static {v2, v3}, LX/1VX;->a(Ljava/util/List;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 593915
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 593916
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    .line 593917
    const-string v4, "birthday_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 593918
    const v5, 0x7f0d2f08

    iget-object v6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    new-instance v7, LX/EsI;

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->h:Landroid/content/res/Resources;

    const v8, 0x7f0a046c

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    if-nez v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v7, v8, v3, v0}, LX/EsI;-><init>(ILjava/lang/String;Z)V

    invoke-interface {p1, v5, v6, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593919
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/Eso;

    invoke-direct {v3, p0, v2}, LX/Eso;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593920
    if-eqz v4, :cond_1

    .line 593921
    const v0, 0x7f0d02a6

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593922
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v2

    .line 593923
    iput v9, v2, LX/1UY;->b:F

    .line 593924
    move-object v2, v2

    .line 593925
    iput v10, v2, LX/1UY;->c:F

    .line 593926
    move-object v2, v2

    .line 593927
    const/high16 v3, 0x41400000    # 12.0f

    .line 593928
    iput v3, v2, LX/1UY;->d:F

    .line 593929
    move-object v2, v2

    .line 593930
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    const v3, 0x7f021934

    invoke-direct {v1, p2, v2, p3, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593931
    :goto_1
    const/4 v0, 0x0

    move-object v0, v0

    .line 593932
    return-object v0

    :cond_0
    move v0, v1

    .line 593933
    goto :goto_0

    .line 593934
    :cond_1
    const v0, 0x7f0d02a6

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593935
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v2

    .line 593936
    iput v9, v2, LX/1UY;->b:F

    .line 593937
    move-object v2, v2

    .line 593938
    iput v10, v2, LX/1UY;->c:F

    .line 593939
    move-object v2, v2

    .line 593940
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    const v3, 0x7f021933

    invoke-direct {v1, p2, v2, p3, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 593903
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593904
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593905
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593906
    if-eqz v0, :cond_0

    .line 593907
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->D()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->b:[I

    invoke-static {v0, v1}, LX/1VX;->a(Ljava/util/List;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 593908
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 593909
    const/4 v0, 0x1

    .line 593910
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
