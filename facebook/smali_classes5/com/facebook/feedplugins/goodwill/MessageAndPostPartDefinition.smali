.class public Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/Es8;",
        "LX/1Ps;",
        "LX/EsK;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/1Kf;

.field public final f:LX/1Cn;

.field public final g:LX/1Nq;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3fc00000    # -3.0f

    .line 593721
    new-instance v0, LX/3Xh;

    invoke-direct {v0}, LX/3Xh;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->a:LX/1Cz;

    .line 593722
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    .line 593723
    iput v1, v0, LX/1UY;->c:F

    .line 593724
    move-object v0, v0

    .line 593725
    iput v1, v0, LX/1UY;->b:F

    .line 593726
    move-object v0, v0

    .line 593727
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/1Kf;LX/1Nq;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593714
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593715
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593716
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 593717
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->e:LX/1Kf;

    .line 593718
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->g:LX/1Nq;

    .line 593719
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->f:LX/1Cn;

    .line 593720
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;
    .locals 10

    .prologue
    .line 593703
    const-class v1, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    monitor-enter v1

    .line 593704
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593705
    sput-object v2, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593706
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593707
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593708
    new-instance v3, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v7

    check-cast v7, LX/1Kf;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v8

    check-cast v8, LX/1Nq;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v9

    check-cast v9, LX/1Cn;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/1Kf;LX/1Nq;LX/1Cn;)V

    .line 593709
    move-object v0, v3

    .line 593710
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593711
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593712
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593702
    sget-object v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 593691
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 593692
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593693
    move-object v8, v0

    check-cast v8, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593694
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->b:LX/1Ua;

    sget-object v4, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v2, p2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593695
    invoke-static {v8}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v9

    .line 593696
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    .line 593697
    new-instance v0, LX/Es8;

    new-instance v2, LX/Es6;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, LX/Es6;-><init>(Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->o()Z

    move-result v3

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, LX/Es8;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;ZLjava/lang/String;LX/0Yb;)V

    .line 593698
    new-instance v1, LX/Es7;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v6

    move-object v2, p0

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, LX/Es7;-><init>(Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Es8;)V

    iput-object v1, v0, LX/Es8;->a:Landroid/view/View$OnClickListener;

    .line 593699
    const-string v1, "friend_birthday"

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "friend_birthday_ipb"

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 593700
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->f:LX/1Cn;

    const-string v2, "friend_birthday_promotion"

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    const-string v5, "friend_birthday_campaign"

    const-string v6, "promotion"

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v7

    move-object v8, v0

    invoke-virtual/range {v1 .. v8}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CEy;)LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/Es8;->e:LX/0Yb;

    .line 593701
    :cond_1
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x436ae1bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 593672
    check-cast p2, LX/Es8;

    check-cast p4, LX/EsK;

    const/4 v5, 0x0

    .line 593673
    iget-boolean v4, p2, LX/Es8;->c:Z

    if-eqz v4, :cond_0

    .line 593674
    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f082a15

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f082a15

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f020984

    iget-object v9, p2, LX/Es8;->a:Landroid/view/View$OnClickListener;

    move-object v4, p4

    invoke-virtual/range {v4 .. v9}, LX/EsK;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/view/View$OnClickListener;)V

    .line 593675
    const/4 v5, 0x1

    .line 593676
    :cond_0
    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f082a14

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f082a14

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f020740

    iget-object v9, p2, LX/Es8;->b:Landroid/view/View$OnClickListener;

    move-object v4, p4

    invoke-virtual/range {v4 .. v9}, LX/EsK;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/view/View$OnClickListener;)V

    .line 593677
    add-int/lit8 v4, v5, 0x1

    invoke-virtual {p4, v4}, LX/EsK;->setNumButtons(I)V

    .line 593678
    iget-object v4, p2, LX/Es8;->e:LX/0Yb;

    if-eqz v4, :cond_1

    .line 593679
    iget-object v4, p2, LX/Es8;->e:LX/0Yb;

    invoke-virtual {v4}, LX/0Yb;->b()V

    .line 593680
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x2c03d252

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 593686
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593687
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593688
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593689
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    .line 593690
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 593681
    check-cast p2, LX/Es8;

    check-cast p4, LX/EsK;

    .line 593682
    iget-object v0, p2, LX/Es8;->e:LX/0Yb;

    if-eqz v0, :cond_0

    .line 593683
    iget-object v0, p2, LX/Es8;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 593684
    :cond_0
    invoke-virtual {p4}, LX/EsK;->a()V

    .line 593685
    return-void
.end method
