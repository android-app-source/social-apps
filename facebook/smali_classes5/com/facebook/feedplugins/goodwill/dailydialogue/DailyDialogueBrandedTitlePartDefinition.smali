.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594906
    new-instance v0, LX/3Xv;

    invoke-direct {v0}, LX/3Xv;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594907
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594908
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    .line 594909
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;
    .locals 4

    .prologue
    .line 594910
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    monitor-enter v1

    .line 594911
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594912
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594915
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;)V

    .line 594916
    move-object v0, p0

    .line 594917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594921
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 594922
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594923
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594924
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594925
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594926
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594927
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594928
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
