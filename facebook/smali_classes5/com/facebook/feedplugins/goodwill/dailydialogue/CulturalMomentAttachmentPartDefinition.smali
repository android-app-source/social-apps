.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/C4i;",
        "TE;",
        "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static o:LX/0Xm;


# instance fields
.field public final c:LX/1Cn;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:Landroid/content/Context;

.field private final g:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field public final i:LX/2mJ;

.field public final j:LX/1Bv;

.field private final k:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field private final m:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594695
    new-instance v0, LX/3Xt;

    invoke-direct {v0}, LX/3Xt;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a:LX/1Cz;

    .line 594696
    const-class v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/1Cn;LX/0rq;LX/2mJ;LX/1Bv;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594697
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594698
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594699
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 594700
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    .line 594701
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    .line 594702
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->h:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 594703
    invoke-virtual {p7}, LX/0rq;->e()I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->n:I

    .line 594704
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->c:LX/1Cn;

    .line 594705
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->i:LX/2mJ;

    .line 594706
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->j:LX/1Bv;

    .line 594707
    iput-object p10, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 594708
    iput-object p11, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 594709
    iput-object p12, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->m:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 594710
    return-void
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 594768
    if-nez p0, :cond_0

    .line 594769
    :goto_0
    return p1

    .line 594770
    :cond_0
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 594771
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 594772
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594773
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 594774
    :cond_1
    :try_start_0
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 594775
    :catch_0
    goto :goto_0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C4i;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/C4i;"
        }
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v1, 0x0

    const/16 v11, 0x8

    .line 594711
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594712
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 594713
    invoke-static {v6}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    .line 594714
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->X()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a(Ljava/lang/String;I)I

    move-result v3

    .line 594715
    if-ne v3, v12, :cond_2

    const/16 v2, 0x102

    .line 594716
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v5, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    sget-object v8, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v5, v7, v8}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594717
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, p1, v6, v0, v4}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a(Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;LX/1aD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ZZ)V

    .line 594718
    const v0, 0x7f0d02c4

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->m:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594719
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 594720
    const v0, 0x7f0d02c3

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->m:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594721
    const v0, 0x7f0d02c3

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594722
    :goto_2
    invoke-static {v6}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 594723
    const/4 v0, 0x0

    .line 594724
    :goto_3
    move-object v0, v0

    .line 594725
    const/4 v5, 0x0

    .line 594726
    if-nez v6, :cond_b

    .line 594727
    :cond_0
    :goto_4
    move-object v5, v5

    .line 594728
    if-eqz v5, :cond_5

    .line 594729
    const v7, 0x7f0d0bbd

    iget-object v8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    new-instance v9, LX/C4r;

    iget v10, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->n:I

    invoke-direct {v9, p2, v5, v10}, LX/C4r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;I)V

    invoke-interface {p1, v7, v8, v9}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594730
    const v5, 0x7f0d0bbe

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594731
    if-eqz v0, :cond_1

    .line 594732
    const v5, 0x7f0d0bbd

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v5, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594733
    :cond_1
    :goto_5
    const-string v5, "WHITE"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->cX()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 594734
    const v5, 0x7f0d0bc0

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594735
    const v5, 0x7f0d0bbf

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594736
    :goto_6
    if-eqz v4, :cond_8

    .line 594737
    const v5, 0x7f0d032f

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v5, v7, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594738
    const v1, 0x7f0d032f

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594739
    :goto_7
    new-instance v0, LX/C4i;

    if-eqz v4, :cond_9

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    :goto_8
    iget v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->n:I

    int-to-float v4, v4

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->n:I

    int-to-float v5, v5

    const v7, 0x3f59999a    # 0.85f

    mul-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->Y()Ljava/lang/String;

    move-result-object v6

    const/high16 v7, -0x1000000

    invoke-static {v6, v7}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a(Ljava/lang/String;I)I

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/C4i;-><init>(Ljava/lang/String;IIIII)V

    return-object v0

    .line 594740
    :cond_2
    const/16 v2, 0x202

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 594741
    goto/16 :goto_1

    .line 594742
    :cond_4
    const v0, 0x7f0d02c3

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v0, v5, v7}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 594743
    :cond_5
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v5

    .line 594744
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v7

    .line 594745
    iget v8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->n:I

    mul-int/2addr v7, v8

    div-int v5, v7, v5

    .line 594746
    const v7, 0x7f0d0bbe

    iget-object v8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->h:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v9

    invoke-virtual {v9, v12, v5}, LX/2f8;->a(II)LX/2f8;

    move-result-object v5

    sget-object v9, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594747
    iput-object v9, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594748
    move-object v5, v5

    .line 594749
    invoke-virtual {v5}, LX/2f8;->a()LX/2f9;

    move-result-object v5

    invoke-interface {p1, v7, v8, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594750
    const v5, 0x7f0d0bbe

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594751
    if-eqz v0, :cond_1

    .line 594752
    const v5, 0x7f0d0bbe

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v5, v7, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 594753
    :cond_6
    const-string v5, "BLUE"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->cX()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 594754
    const v5, 0x7f0d0bbf

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->h:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v8

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->cW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v8

    sget-object v9, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594755
    iput-object v9, v8, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 594756
    move-object v8, v8

    .line 594757
    invoke-virtual {v8}, LX/2f8;->a()LX/2f9;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594758
    const v5, 0x7f0d0bc0

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594759
    const v5, 0x7f0d0bbf

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 594760
    :cond_7
    const v5, 0x7f0d0bc0

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594761
    const v5, 0x7f0d0bbf

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {p1, v5, v7, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 594762
    :cond_8
    const v0, 0x7f0d032f

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->l:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, v0, v1, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 594763
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_8

    :cond_a
    new-instance v0, LX/C4h;

    invoke-direct {v0, p0, v6}, LX/C4h;-><init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto/16 :goto_3

    .line 594764
    :cond_b
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 594765
    invoke-static {v7}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v7

    .line 594766
    if-eqz v7, :cond_0

    iget-object v8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->j:LX/1Bv;

    invoke-virtual {v8}, LX/1Bv;->a()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    move-object v5, v7

    .line 594767
    goto/16 :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 594669
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    monitor-enter v1

    .line 594670
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594671
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594672
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594673
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594674
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594675
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594676
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;LX/1aD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 594677
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1d0a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 594678
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ea()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v0, v3}, LX/C4m;->a(Ljava/lang/String;ILandroid/content/res/Resources;)I

    move-result v0

    move v3, v0

    .line 594679
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1d0c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 594680
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ly()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v2, v0, v4}, LX/C4m;->b(Ljava/lang/String;ILandroid/content/res/Resources;)I

    move-result v0

    move v4, v0

    .line 594681
    if-eqz p3, :cond_0

    .line 594682
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1d0b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v0

    .line 594683
    :goto_0
    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0b1d0d

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 594684
    :goto_1
    new-instance v5, LX/1ds;

    invoke-direct {v5, v6, v3, v6, v2}, LX/1ds;-><init>(IIII)V

    .line 594685
    new-instance v2, LX/1ds;

    invoke-direct {v2, v6, v1, v6, v0}, LX/1ds;-><init>(IIII)V

    .line 594686
    new-instance v0, LX/1ds;

    invoke-direct {v0, v6, v6, v6, v4}, LX/1ds;-><init>(IIII)V

    .line 594687
    const v1, 0x7f0d02c4

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-interface {p1, v1, v3, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594688
    const v1, 0x7f0d02c3

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-interface {p1, v1, v3, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594689
    const v1, 0x7f0d0bbc

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594690
    return-void

    .line 594691
    :cond_0
    if-eqz p4, :cond_1

    .line 594692
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1d0d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 594693
    goto :goto_0

    :cond_2
    move v0, v1

    .line 594694
    goto :goto_1
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;
    .locals 13

    .prologue
    .line 594667
    new-instance v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {p0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v6

    check-cast v6, LX/1Cn;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v7

    check-cast v7, LX/0rq;

    invoke-static {p0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v8

    check-cast v8, LX/2mJ;

    invoke-static {p0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v9

    check-cast v9, LX/1Bv;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/Context;Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentVideoPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/1Cn;LX/0rq;LX/2mJ;LX/1Bv;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 594668
    return-object v0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 594666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594665
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 594664
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C4i;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1562d02b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594649
    check-cast p2, LX/C4i;

    check-cast p4, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 594650
    const v1, 0x7f0d02c4

    invoke-virtual {p4, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 594651
    const v2, 0x7f0d02c3

    invoke-virtual {p4, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 594652
    const p0, 0x7f0d032f

    invoke-virtual {p4, p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/fig/button/FigButton;

    .line 594653
    const p1, 0x7f0d0bbc

    invoke-virtual {p4, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 594654
    iget-object p3, p2, LX/C4i;->a:Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 594655
    iget-object p3, p2, LX/C4i;->a:Ljava/lang/String;

    invoke-virtual {p0, p3}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 594656
    :cond_0
    iget p3, p2, LX/C4i;->b:I

    invoke-virtual {p0, p3}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 594657
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x10

    if-lt p0, p3, :cond_1

    .line 594658
    new-instance p0, Landroid/graphics/drawable/ColorDrawable;

    iget p3, p2, LX/C4i;->c:I

    invoke-direct {p0, p3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, p0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 594659
    :cond_1
    iget p0, p2, LX/C4i;->f:I

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 594660
    iget p0, p2, LX/C4i;->f:I

    invoke-virtual {v2, p0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 594661
    iget p0, p2, LX/C4i;->d:I

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    .line 594662
    iget v1, p2, LX/C4i;->e:I

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxWidth(I)V

    .line 594663
    const/16 v1, 0x1f

    const v2, -0x2664086e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594645
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594646
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594647
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 594648
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->cW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
