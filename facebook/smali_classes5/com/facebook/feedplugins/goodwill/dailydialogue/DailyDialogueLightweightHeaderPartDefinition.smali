.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;",
        "LX/3js;",
        "LX/1Pf;",
        "LX/3jr;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static m:LX/0Xm;


# instance fields
.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Cn;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final i:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final j:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final l:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 593229
    new-instance v0, LX/3XS;

    invoke-direct {v0}, LX/3XS;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a:LX/1Cz;

    .line 593230
    const-class v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 593231
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 593232
    iput v1, v0, LX/1UY;->c:F

    .line 593233
    move-object v0, v0

    .line 593234
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/1Cn;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            ">;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593301
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593302
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->l:Landroid/content/res/Resources;

    .line 593303
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->d:LX/0Ot;

    .line 593304
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->e:LX/0Ot;

    .line 593305
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593306
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 593307
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->h:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 593308
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->i:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 593309
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->j:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 593310
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 593311
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;
    .locals 13

    .prologue
    .line 593290
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    monitor-enter v1

    .line 593291
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593292
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593293
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593294
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593295
    new-instance v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    const/16 v5, 0xada

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1043

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;-><init>(Landroid/content/res/Resources;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 593296
    move-object v0, v3

    .line 593297
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593298
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593299
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 593287
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593288
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 593289
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->DAILY_DIALOGUE_LIGHTWEIGHT:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593312
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 593250
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593251
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593252
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 593253
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 593254
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 593255
    :goto_0
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->c:LX/1Ua;

    invoke-direct {v4, p2, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593256
    const v3, 0x7f0d02c4

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593257
    const v3, 0x7f0d02c3

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->h:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v3, v4, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593258
    const/4 v1, 0x0

    .line 593259
    if-nez v2, :cond_7

    .line 593260
    :cond_0
    :goto_1
    move v1, v1

    .line 593261
    if-eqz v1, :cond_1

    .line 593262
    const v1, 0x7f0d0be1

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->h:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593263
    const v1, 0x7f0d0be0

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v4, LX/C4v;

    invoke-direct {v4, p0, v2, v0}, LX/C4v;-><init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLCustomizedStory;)V

    invoke-interface {p1, v1, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593264
    :cond_1
    const v1, 0x7f0d0bdf

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->i:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 593265
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 593266
    move-object v3, v3

    .line 593267
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593268
    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 593269
    iput-object v2, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 593270
    move-object v5, v1

    .line 593271
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    .line 593272
    if-lez v4, :cond_2

    .line 593273
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->l:Landroid/content/res/Resources;

    const v2, 0x7f0b1d13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 593274
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->l:Landroid/content/res/Resources;

    const v3, 0x7f0b1d14

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 593275
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v6

    .line 593276
    if-le v4, v2, :cond_4

    move v3, v2

    .line 593277
    :goto_2
    int-to-double v6, v6

    int-to-double v8, v3

    mul-double/2addr v6, v8

    int-to-double v8, v4

    div-double/2addr v6, v8

    double-to-int v2, v6

    .line 593278
    if-le v2, v1, :cond_5

    :goto_3
    invoke-virtual {v5, v1, v3}, LX/2f8;->a(II)LX/2f8;

    .line 593279
    :cond_2
    const v1, 0x7f0d0340

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->i:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-virtual {v5}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593280
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->j:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593281
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->v()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 593282
    :goto_4
    new-instance v1, LX/3js;

    invoke-direct {v1, v0}, LX/3js;-><init>(Z)V

    return-object v1

    .line 593283
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_4
    move v3, v4

    .line 593284
    goto :goto_2

    :cond_5
    move v1, v2

    .line 593285
    goto :goto_3

    .line 593286
    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x161c40be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 593236
    check-cast p2, LX/3js;

    check-cast p4, LX/3jr;

    .line 593237
    iget-boolean v1, p2, LX/3js;->a:Z

    if-eqz v1, :cond_0

    .line 593238
    const/16 v1, 0x10

    .line 593239
    iget-object v2, p4, LX/3jr;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 593240
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 593241
    iget-object p2, p4, LX/3jr;->a:Landroid/widget/LinearLayout;

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 593242
    iget-object v2, p4, LX/3jr;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 593243
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 593244
    iget-object p2, p4, LX/3jr;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 593245
    const/4 v2, 0x0

    .line 593246
    iget-object v1, p4, LX/3jr;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 593247
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 593248
    iget-object v2, p4, LX/3jr;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 593249
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x6c18f301

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 593235
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
