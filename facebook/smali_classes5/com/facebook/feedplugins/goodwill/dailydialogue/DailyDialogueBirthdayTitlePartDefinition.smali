.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594825
    const v0, 0x7f0303c9

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->a:LX/1Cz;

    .line 594826
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x41c00000    # 24.0f

    .line 594827
    iput v1, v0, LX/1UY;->c:F

    .line 594828
    move-object v0, v0

    .line 594829
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594830
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594831
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    .line 594832
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594833
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;
    .locals 5

    .prologue
    .line 594834
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    monitor-enter v1

    .line 594835
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594836
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594839
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 594840
    move-object v0, p0

    .line 594841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594845
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 594846
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594847
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594848
    invoke-static {p2}, LX/C4e;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594849
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->b:LX/1Ua;

    const v3, 0x7f0207fa

    const/4 v4, -0x1

    invoke-direct {v1, p2, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594850
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594851
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594852
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594853
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594854
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "birthday_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
