.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/C4t;",
        "TE;",
        "LX/C4u;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field public final e:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594891
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->b:LX/1Ua;

    .line 594892
    new-instance v0, LX/3Xu;

    invoke-direct {v0}, LX/3Xu;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594886
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594887
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594888
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 594889
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 594890
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;
    .locals 5

    .prologue
    .line 594893
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    monitor-enter v1

    .line 594894
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594895
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594896
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594897
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594898
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V

    .line 594899
    move-object v0, p0

    .line 594900
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594901
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594902
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594885
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 594871
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594872
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594873
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594874
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->b:LX/1Ua;

    const v4, 0x7f0207fa

    const/4 v5, -0x1

    invoke-direct {v2, p2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594875
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v4}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594876
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    move v3, v1

    .line 594877
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 594878
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 594879
    const/4 v2, 0x0

    .line 594880
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 594881
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x10

    invoke-static {v0, v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v6

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 594882
    :goto_2
    new-instance v2, LX/C4t;

    invoke-direct {v2, v1, v0, v3, v4}, LX/C4t;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Ljava/lang/Integer;ZLcom/facebook/graphql/model/GraphQLImage;)V

    return-object v2

    .line 594883
    :cond_0
    const/4 v1, 0x0

    move v3, v1

    goto :goto_0

    .line 594884
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x716762a9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594856
    check-cast p2, LX/C4t;

    check-cast p4, LX/C4u;

    .line 594857
    iget-object v1, p2, LX/C4t;->a:Lcom/facebook/graphql/model/GraphQLImage;

    iget-boolean v2, p2, LX/C4t;->c:Z

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 594858
    if-eqz v1, :cond_0

    .line 594859
    iput-object v1, p4, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 594860
    iput-boolean v2, p4, LX/C4u;->d:Z

    .line 594861
    iget-object p1, p4, LX/C4u;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p1, p3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594862
    :cond_0
    iget-object v1, p2, LX/C4t;->d:Lcom/facebook/graphql/model/GraphQLImage;

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 594863
    iget-object v4, p4, LX/C4u;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v4, p1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594864
    iget-object v4, p4, LX/C4u;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 594865
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p1

    iput p1, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 594866
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p1

    iput p1, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 594867
    iget-object v1, p2, LX/C4t;->b:Ljava/lang/Integer;

    .line 594868
    iget-object v4, p4, LX/C4u;->b:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 594869
    const/16 v1, 0x1f

    const v2, 0x2d22185a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 594870
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594855
    const/4 v0, 0x1

    return v0
.end method
