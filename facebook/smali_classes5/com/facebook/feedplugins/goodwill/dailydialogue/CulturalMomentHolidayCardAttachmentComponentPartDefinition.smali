.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/C4l;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594821
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ec00000    # -12.0f

    .line 594822
    iput v1, v0, LX/1UY;->d:F

    .line 594823
    move-object v0, v0

    .line 594824
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C4l;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594817
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 594818
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->e:LX/C4l;

    .line 594819
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->f:LX/1V0;

    .line 594820
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 594799
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->e:LX/C4l;

    const/4 v1, 0x0

    .line 594800
    new-instance v2, LX/C4k;

    invoke-direct {v2, v0}, LX/C4k;-><init>(LX/C4l;)V

    .line 594801
    iget-object v3, v0, LX/C4l;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C4j;

    .line 594802
    if-nez v3, :cond_0

    .line 594803
    new-instance v3, LX/C4j;

    invoke-direct {v3, v0}, LX/C4j;-><init>(LX/C4l;)V

    .line 594804
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C4j;->a$redex0(LX/C4j;LX/1De;IILX/C4k;)V

    .line 594805
    move-object v2, v3

    .line 594806
    move-object v1, v2

    .line 594807
    move-object v0, v1

    .line 594808
    iget-object v1, v0, LX/C4j;->a:LX/C4k;

    iput-object p2, v1, LX/C4k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594809
    iget-object v1, v0, LX/C4j;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 594810
    move-object v1, v0

    .line 594811
    move-object v0, p3

    check-cast v0, LX/1Ps;

    .line 594812
    iget-object v2, v1, LX/C4j;->a:LX/C4k;

    iput-object v0, v2, LX/C4k;->b:LX/1Ps;

    .line 594813
    iget-object v2, v1, LX/C4j;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 594814
    move-object v0, v1

    .line 594815
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 594816
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;
    .locals 6

    .prologue
    .line 594788
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 594789
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594790
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594793
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C4l;->a(LX/0QB;)LX/C4l;

    move-result-object v4

    check-cast v4, LX/C4l;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/C4l;LX/1V0;)V

    .line 594794
    move-object v0, p0

    .line 594795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 594787
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 594786
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594780
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594781
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594782
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 594783
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594784
    :cond_0
    const/4 v0, 0x0

    .line 594785
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 594778
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594779
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
