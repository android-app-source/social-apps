.class public Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/EsC;",
        "TE;",
        "LX/EsK;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final e:LX/1Kf;

.field public final f:LX/1Cn;

.field public final g:LX/1Nq;

.field public final h:LX/74n;

.field public final i:LX/1Er;

.field public final j:LX/9iU;

.field public final k:LX/1Fs;

.field private final l:LX/0Xl;

.field public final m:LX/0if;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3fc00000    # -3.0f

    .line 593774
    new-instance v0, LX/3Xi;

    invoke-direct {v0}, LX/3Xi;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a:LX/1Cz;

    .line 593775
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    .line 593776
    iput v1, v0, LX/1UY;->c:F

    .line 593777
    move-object v0, v0

    .line 593778
    iput v1, v0, LX/1UY;->b:F

    .line 593779
    move-object v0, v0

    .line 593780
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->b:LX/1Ua;

    .line 593781
    const-class v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Kf;LX/1Nq;LX/1Cn;LX/74n;LX/1Er;LX/9iU;LX/1Fs;LX/0Xl;LX/0if;)V
    .locals 0
    .param p9    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593784
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593785
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593786
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->e:LX/1Kf;

    .line 593787
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->g:LX/1Nq;

    .line 593788
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->f:LX/1Cn;

    .line 593789
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->h:LX/74n;

    .line 593790
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->i:LX/1Er;

    .line 593791
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->j:LX/9iU;

    .line 593792
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->k:LX/1Fs;

    .line 593793
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->l:LX/0Xl;

    .line 593794
    iput-object p10, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    .line 593795
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;LX/EsE;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;LX/CEy;)LX/0Yb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EsE;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            "LX/CEy;",
            ")",
            "Lcom/facebook/base/broadcast/FbBroadcastManager$SelfRegistrableReceiver;"
        }
    .end annotation

    .prologue
    .line 593782
    new-instance v0, LX/Es9;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/Es9;-><init>(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;LX/CEy;LX/EsE;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 593783
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->l:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/EsC;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;TE;)",
            "LX/EsC;"
        }
    .end annotation

    .prologue
    .line 593752
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593753
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->b:LX/1Ua;

    sget-object v6, LX/1X9;->DIVIDER_TOP:LX/1X9;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593754
    invoke-static {v12}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v18

    .line 593755
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v15

    .line 593756
    new-instance v10, LX/EsC;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v10, v3, v4, v5, v6}, LX/EsC;-><init>(Landroid/view/View$OnClickListener;Ljava/lang/String;LX/0Yb;LX/0Yb;)V

    .line 593757
    const-string v3, "friend_birthday"

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "friend_birthday_ipb"

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "friend_birthday_card"

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 593758
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->f:LX/1Cn;

    const-string v4, "friend_birthday_promotion"

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v6

    const-string v7, "friend_birthday_campaign"

    const-string v8, "promotion"

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v3 .. v10}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CEy;)LX/0Yb;

    move-result-object v3

    iput-object v3, v10, LX/EsC;->c:LX/0Yb;

    .line 593759
    :cond_1
    new-instance v3, LX/EsD;

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, LX/EsD;-><init>(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v3, v12}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EsE;

    .line 593760
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-static {v0, v3, v1, v2, v10}, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;LX/EsE;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;LX/CEy;)LX/0Yb;

    move-result-object v3

    iput-object v3, v10, LX/EsC;->d:LX/0Yb;

    .line 593761
    new-instance v11, LX/EsB;

    invoke-virtual {v15}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v12, p0

    move-object/from16 v20, v10

    invoke-direct/range {v11 .. v20}, LX/EsB;-><init>(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/EsC;)V

    iput-object v11, v10, LX/EsC;->a:Landroid/view/View$OnClickListener;

    .line 593762
    return-object v10
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;
    .locals 14

    .prologue
    .line 593763
    const-class v1, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    monitor-enter v1

    .line 593764
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593765
    sput-object v2, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593768
    new-instance v3, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v6

    check-cast v6, LX/1Nq;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v7

    check-cast v7, LX/1Cn;

    invoke-static {v0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v8

    check-cast v8, LX/74n;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v9

    check-cast v9, LX/1Er;

    invoke-static {v0}, LX/9iU;->a(LX/0QB;)LX/9iU;

    move-result-object v10

    check-cast v10, LX/9iU;

    invoke-static {v0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v11

    check-cast v11, LX/1Fs;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v12

    check-cast v12, LX/0Xl;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v13

    check-cast v13, LX/0if;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Kf;LX/1Nq;LX/1Cn;LX/74n;LX/1Er;LX/9iU;LX/1Fs;LX/0Xl;LX/0if;)V

    .line 593769
    move-object v0, v3

    .line 593770
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593771
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593772
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593751
    sget-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 593750
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/EsC;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1d24a92f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 593742
    check-cast p2, LX/EsC;

    check-cast p4, LX/EsK;

    .line 593743
    const/4 v5, 0x0

    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f082a16

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4}, LX/EsK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f082a16

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f02080f

    iget-object v9, p2, LX/EsC;->a:Landroid/view/View$OnClickListener;

    move-object v4, p4

    invoke-virtual/range {v4 .. v9}, LX/EsK;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/view/View$OnClickListener;)V

    .line 593744
    const/4 v4, 0x1

    invoke-virtual {p4, v4}, LX/EsK;->setNumButtons(I)V

    .line 593745
    iget-object v4, p2, LX/EsC;->c:LX/0Yb;

    if-eqz v4, :cond_0

    .line 593746
    iget-object v4, p2, LX/EsC;->c:LX/0Yb;

    invoke-virtual {v4}, LX/0Yb;->b()V

    .line 593747
    :cond_0
    iget-object v4, p2, LX/EsC;->d:LX/0Yb;

    if-eqz v4, :cond_1

    .line 593748
    iget-object v4, p2, LX/EsC;->d:LX/0Yb;

    invoke-virtual {v4}, LX/0Yb;->b()V

    .line 593749
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x62dba71c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 593737
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 593738
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593739
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593740
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 593741
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 593730
    check-cast p2, LX/EsC;

    check-cast p4, LX/EsK;

    .line 593731
    iget-object v0, p2, LX/EsC;->c:LX/0Yb;

    if-eqz v0, :cond_0

    .line 593732
    iget-object v0, p2, LX/EsC;->c:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 593733
    :cond_0
    iget-object v0, p2, LX/EsC;->d:LX/0Yb;

    if-eqz v0, :cond_1

    .line 593734
    iget-object v0, p2, LX/EsC;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 593735
    :cond_1
    invoke-virtual {p4}, LX/EsK;->a()V

    .line 593736
    return-void
.end method
