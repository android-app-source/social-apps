.class public Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/1Cn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593834
    const v0, 0x7f030199

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593798
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593799
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    .line 593800
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 593801
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->d:LX/1Cn;

    .line 593802
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;
    .locals 6

    .prologue
    .line 593803
    const-class v1, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    monitor-enter v1

    .line 593804
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593805
    sput-object v2, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593806
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593807
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593808
    new-instance p0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v5

    check-cast v5, LX/1Cn;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1Cn;)V

    .line 593809
    move-object v0, p0

    .line 593810
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593811
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593812
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 593814
    sget-object v0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 593815
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593816
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593817
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593818
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 593819
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 593820
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    .line 593821
    const v3, 0x7f0d06f4

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v1

    .line 593822
    new-instance p2, LX/Et7;

    invoke-direct {p2, p0, v2, v0, v1}, LX/Et7;-><init>(Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLImageOverlay;Ljava/lang/String;)V

    .line 593823
    move-object v0, p2

    .line 593824
    invoke-interface {p1, v3, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593825
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 593826
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 593827
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593828
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 593829
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 593830
    if-eqz v2, :cond_0

    const-string v3, "self_birthday_profile_pic_frame"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 593831
    :goto_0
    return v0

    .line 593832
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 593833
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
