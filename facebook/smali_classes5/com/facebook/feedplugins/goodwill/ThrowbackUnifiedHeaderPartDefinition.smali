.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
.super Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final b:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final e:LX/1Uf;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594011
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1Uf;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594012
    invoke-direct {p0}, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;-><init>()V

    .line 594013
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594014
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 594015
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->e:LX/1Uf;

    .line 594016
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->f:Landroid/content/res/Resources;

    .line 594017
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 594018
    const/16 v0, 0x10

    invoke-static {p0, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;)",
            "LX/C3x;"
        }
    .end annotation

    .prologue
    .line 594019
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594020
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->e:LX/1Uf;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v1

    move-object v14, v1

    .line 594021
    :goto_0
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v18

    .line 594022
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v8, 0x1

    .line 594023
    :goto_1
    if-eqz v8, :cond_4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    move-object v11, v1

    .line 594024
    :goto_2
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    :cond_0
    const/high16 v7, -0x1000000

    .line 594025
    :goto_3
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 594026
    invoke-static {v10}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "friendversary_card_collage"

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_1
    const/4 v1, 0x1

    move v12, v1

    .line 594027
    :goto_4
    const/4 v13, 0x0

    .line 594028
    const/16 v16, 0x0

    .line 594029
    const/16 v17, 0x0

    .line 594030
    const/4 v15, 0x0

    .line 594031
    new-instance v1, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->b:LX/1Ua;

    const v4, 0x7f0207fa

    const/4 v5, -0x1

    sget-object v6, LX/1X9;->MIDDLE:LX/1X9;

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    .line 594032
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 594033
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    const-string v3, "friend_birthday"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 594034
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->f:Landroid/content/res/Resources;

    const v3, 0x7f0b09e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 594035
    invoke-static {v10, v2}, LX/23B;->a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 594036
    if-eqz v3, :cond_b

    .line 594037
    const v2, 0x7f021516

    :goto_5
    move v13, v2

    move-object v2, v3

    move/from16 v3, v16

    .line 594038
    :goto_6
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v4

    const-string v5, "friendversary_card_data"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 594039
    const/4 v5, 0x1

    .line 594040
    const v1, 0x7f0d0bde

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v6, LX/24b;

    sget-object v10, LX/1dl;->CLICKABLE:LX/1dl;

    move-object/from16 v0, p2

    invoke-direct {v6, v0, v10}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v4, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594041
    new-instance v1, LX/1X6;

    sget-object v4, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->b:LX/1Ua;

    const v6, 0x7f0207fa

    const/4 v10, -0x1

    move-object/from16 v0, p2

    invoke-direct {v1, v0, v4, v6, v10}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    move v10, v12

    move-object v6, v11

    move v12, v13

    move-object v11, v2

    move v13, v3

    .line 594042
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594043
    new-instance v1, LX/C3x;

    const/4 v4, 0x0

    move-object/from16 v2, v18

    move-object v3, v14

    invoke-direct/range {v1 .. v13}, LX/C3x;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;ZLcom/facebook/graphql/model/GraphQLImage;IZLcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/graphql/model/GraphQLImage;IF)V

    return-object v1

    .line 594044
    :cond_2
    const-string v1, ""

    move-object v14, v1

    goto/16 :goto_0

    .line 594045
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 594046
    :cond_4
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    move-object v11, v1

    goto/16 :goto_2

    .line 594047
    :cond_5
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_3

    .line 594048
    :cond_6
    const/4 v1, 0x0

    move v12, v1

    goto/16 :goto_4

    .line 594049
    :cond_7
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    const-string v3, "friendversary_polaroids"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 594050
    const/4 v12, 0x1

    .line 594051
    const/high16 v2, 0x438c0000    # 280.0f

    move v3, v2

    move-object v2, v15

    goto :goto_6

    .line 594052
    :cond_8
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    const-string v3, "friendversary_card_data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 594053
    const/4 v11, 0x0

    move-object v2, v15

    move/from16 v3, v16

    goto/16 :goto_6

    :cond_9
    move/from16 v5, v17

    move v10, v12

    move-object v6, v11

    move v12, v13

    move-object v11, v2

    move v13, v3

    goto :goto_7

    :cond_a
    move-object v2, v15

    move/from16 v3, v16

    goto/16 :goto_6

    :cond_b
    move v2, v13

    goto/16 :goto_5

    :cond_c
    move/from16 v5, v17

    move v10, v12

    move-object v6, v11

    move v12, v13

    move-object v11, v15

    move/from16 v13, v16

    goto :goto_7
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
    .locals 7

    .prologue
    .line 594054
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    monitor-enter v1

    .line 594055
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594056
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594057
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594058
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594059
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1Uf;Landroid/content/res/Resources;)V

    .line 594060
    move-object v0, p0

    .line 594061
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594062
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594063
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 594065
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 594066
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594067
    const/4 v0, 0x1

    return v0
.end method
