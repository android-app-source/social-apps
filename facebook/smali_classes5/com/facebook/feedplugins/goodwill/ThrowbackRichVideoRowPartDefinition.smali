.class public Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/RelativeLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field private final e:Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594464
    const v0, 0x7f0307df

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594465
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594466
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->b:Landroid/content/Context;

    .line 594467
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    .line 594468
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 594469
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 594470
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;
    .locals 7

    .prologue
    .line 594471
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    monitor-enter v1

    .line 594472
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594473
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594474
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594475
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594476
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;)V

    .line 594477
    move-object v0, p0

    .line 594478
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594479
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594480
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594482
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 594483
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 594484
    invoke-static {p2}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 594485
    if-nez v0, :cond_0

    .line 594486
    :goto_0
    return-object v1

    .line 594487
    :cond_0
    const v2, 0x7f0d14c9

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594488
    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 594489
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 594490
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594491
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    const/4 v3, 0x0

    .line 594492
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 594493
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 594494
    :cond_1
    :goto_1
    move-object v0, v3

    .line 594495
    :goto_2
    const v3, 0x7f0d14ca

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->d:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    if-nez v0, :cond_3

    const/16 v2, 0x8

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594496
    const v2, 0x7f0d0011

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->c:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 594497
    goto :goto_2

    .line 594498
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 594499
    :cond_4
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 594500
    :try_start_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 594501
    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f012b

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v7, p2

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    .line 594502
    :catch_0
    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 594503
    if-eqz p1, :cond_0

    .line 594504
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 594505
    if-nez v1, :cond_1

    .line 594506
    :cond_0
    :goto_0
    return v0

    .line 594507
    :cond_1
    invoke-static {p1}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 594508
    if-eqz v1, :cond_0

    .line 594509
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594510
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
