.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/Esk;",
        "TE;",
        "LX/Esj;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 594556
    new-instance v0, LX/3Xr;

    invoke-direct {v0}, LX/3Xr;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->a:LX/1Cz;

    .line 594557
    const-class v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594558
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    const/4 v1, -0x2

    .line 594559
    mul-int/lit8 v2, v1, 0x4

    int-to-float v2, v2

    .line 594560
    iput v2, v0, LX/1UY;->b:F

    .line 594561
    move-object v2, v0

    .line 594562
    move-object v0, v2

    .line 594563
    const/4 v1, -0x3

    invoke-virtual {v0, v1}, LX/1UY;->b(I)LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594553
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594554
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594555
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;
    .locals 4

    .prologue
    .line 594542
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    monitor-enter v1

    .line 594543
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594544
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594545
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594546
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594547
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 594548
    move-object v0, p0

    .line 594549
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594550
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594551
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594552
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594564
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 594533
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594534
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594535
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594536
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 594537
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 594538
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 594539
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 594540
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    sget-object v5, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->c:LX/1Ua;

    const v6, 0x7f0207fa

    const/4 v7, -0x1

    invoke-direct {v4, p2, v5, v6, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594541
    new-instance v3, LX/Esk;

    invoke-direct {v3, v1, v2, v0}, LX/Esk;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x23206ddb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594516
    check-cast p2, LX/Esk;

    check-cast p4, LX/Esj;

    .line 594517
    iget-object v1, p2, LX/Esk;->a:Ljava/lang/String;

    .line 594518
    iget-object v2, p4, LX/Esj;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594519
    invoke-virtual {p4}, LX/Esj;->requestLayout()V

    .line 594520
    iget-object v1, p2, LX/Esk;->b:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594521
    iput-object v1, p4, LX/Esj;->a:Lcom/facebook/graphql/model/GraphQLImage;

    .line 594522
    iget-object p1, p4, LX/Esj;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v1, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1, p0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594523
    invoke-virtual {p4}, LX/Esj;->requestLayout()V

    .line 594524
    iget-object v1, p2, LX/Esk;->c:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594525
    iget-object p1, p4, LX/Esj;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v1, :cond_1

    const/4 p0, 0x0

    :goto_1
    invoke-virtual {p1, p0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594526
    iget-object p0, p4, LX/Esj;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 594527
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p1

    iput p1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 594528
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p1

    iput p1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 594529
    invoke-virtual {p4}, LX/Esj;->requestLayout()V

    .line 594530
    const/16 v1, 0x1f

    const v2, 0x4c421814    # 5.0880592E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 594531
    :cond_0
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    .line 594532
    :cond_1
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 594511
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594512
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594513
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594514
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 594515
    const-string v2, "friend_birthday_card"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
