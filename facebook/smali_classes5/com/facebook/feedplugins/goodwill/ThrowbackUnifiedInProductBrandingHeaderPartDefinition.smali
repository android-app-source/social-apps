.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/Esm;",
        "TE;",
        "LX/Esn;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1Uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594458
    new-instance v0, LX/3Xq;

    invoke-direct {v0}, LX/3Xq;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a:LX/1Cz;

    .line 594459
    const-class v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594460
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/1Uf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;",
            "LX/1Uf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594451
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594452
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 594453
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594454
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 594455
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->g:LX/0Ot;

    .line 594456
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->h:LX/1Uf;

    .line 594457
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Esm;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;)",
            "LX/Esm;"
        }
    .end annotation

    .prologue
    .line 594411
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594412
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594413
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 594414
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->w()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 594415
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 594416
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    .line 594417
    const/4 v6, 0x0

    .line 594418
    const/4 v7, 0x0

    .line 594419
    const/4 v5, 0x0

    .line 594420
    const-string v8, "friend_birthday_ipb"

    .line 594421
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 594422
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v8, "self_birthday_profile_pic_frame"

    .line 594423
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 594424
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 594425
    :cond_0
    const/4 v1, 0x0

    .line 594426
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v8

    .line 594427
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-nez v5, :cond_7

    .line 594428
    :cond_1
    :goto_0
    move-object v5, v1

    .line 594429
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v8

    .line 594430
    if-eqz v5, :cond_3

    .line 594431
    const v7, 0x7f021517

    .line 594432
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 594433
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->q()LX/0Px;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImageOverlay;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 594434
    :cond_2
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 594435
    const v1, 0x7f0d08f1

    iget-object v10, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    .line 594436
    new-instance v11, LX/Esl;

    invoke-direct {v11, p0, v8}, LX/Esl;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLProfile;)V

    move-object v8, v11

    .line 594437
    invoke-interface {p1, v1, v10, v8}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594438
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v8, LX/1X6;

    sget-object v10, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->c:LX/1Ua;

    const v11, 0x7f0207fa

    const/4 v12, -0x1

    invoke-direct {v8, p2, v10, v11, v12}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v8}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594439
    const v1, 0x7f0d0bde

    iget-object v8, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v10, LX/24b;

    sget-object v11, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v10, p2, v11}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v8, v10}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594440
    const/4 v8, 0x0

    .line 594441
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 594442
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionColorPalette;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 594443
    :cond_4
    new-instance v0, LX/Esm;

    if-nez v2, :cond_5

    const/4 v1, 0x0

    :goto_1
    if-nez v9, :cond_6

    const/4 v2, 0x0

    :goto_2
    invoke-direct/range {v0 .. v8}, LX/Esm;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;ILjava/lang/Integer;)V

    return-object v0

    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->h:LX/1Uf;

    invoke-static {v9}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v2, v9, v10, v11}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    goto :goto_2

    .line 594444
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 594445
    if-nez v5, :cond_8

    .line 594446
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 594447
    :cond_8
    if-nez v5, :cond_9

    .line 594448
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 594449
    :cond_9
    if-eqz v5, :cond_1

    .line 594450
    invoke-static {v5}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v1

    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;
    .locals 9

    .prologue
    .line 594400
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    monitor-enter v1

    .line 594401
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594402
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594403
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594404
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594405
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const/16 v7, 0x6b9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v8

    check-cast v8, LX/1Uf;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/1Uf;)V

    .line 594406
    move-object v0, v3

    .line 594407
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594408
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594409
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594461
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 594399
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Esm;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6cd74bfc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594347
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/Esm;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/Esn;

    .line 594348
    const-string v2, "self_birthday_profile_pic_frame"

    .line 594349
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 594350
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 594351
    iput-boolean v1, p4, LX/Esn;->j:Z

    .line 594352
    iget-object v1, p2, LX/Esm;->a:Ljava/lang/String;

    iget-object v2, p2, LX/Esm;->b:Ljava/lang/CharSequence;

    const/4 v8, 0x0

    .line 594353
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594354
    iget-boolean v4, p4, LX/Esn;->j:Z

    if-eqz v4, :cond_1

    .line 594355
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    const/4 v5, 0x2

    const/high16 v6, 0x41a00000    # 20.0f

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 594356
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v6, LX/0xr;->MEDIUM:LX/0xr;

    iget-object v7, p4, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 594357
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {p4}, LX/Esn;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1d28

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v8, v5, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 594358
    :goto_0
    iget-object v4, p4, LX/Esn;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594359
    iget-object v1, p2, LX/Esm;->c:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594360
    iput-object v1, p4, LX/Esn;->a:Lcom/facebook/graphql/model/GraphQLImage;

    .line 594361
    iget-object v5, p4, LX/Esn;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v1, :cond_2

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v5, v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594362
    invoke-virtual {p4}, LX/Esn;->requestLayout()V

    .line 594363
    iget-object v1, p2, LX/Esm;->d:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594364
    iget-object v5, p4, LX/Esn;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v1, :cond_3

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v5, v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594365
    iget-object v4, p4, LX/Esn;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 594366
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 594367
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 594368
    invoke-virtual {p4}, LX/Esn;->requestLayout()V

    .line 594369
    iget-object v1, p2, LX/Esm;->e:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594370
    iput-object v1, p4, LX/Esn;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 594371
    if-eqz v1, :cond_4

    .line 594372
    iget-object v4, p4, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594373
    iget-object v4, p4, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 594374
    :goto_3
    invoke-virtual {p4}, LX/Esn;->requestLayout()V

    .line 594375
    iget-object v1, p2, LX/Esm;->f:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 594376
    if-eqz v1, :cond_5

    .line 594377
    iget-object v4, p4, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 594378
    iget-object v4, p4, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 594379
    :goto_4
    iget v1, p2, LX/Esm;->g:I

    .line 594380
    iget-boolean v2, p4, LX/Esn;->j:Z

    if-eqz v2, :cond_6

    const v2, 0x7f0b1d26

    .line 594381
    :goto_5
    invoke-virtual {p4}, LX/Esn;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 594382
    if-nez v1, :cond_0

    .line 594383
    const/4 v2, 0x0

    .line 594384
    :cond_0
    iget-object v4, p4, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 594385
    iget-object v4, p4, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 594386
    iget-object v4, p4, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 594387
    iget-object v1, p2, LX/Esm;->h:Ljava/lang/Integer;

    .line 594388
    iget-object v4, p4, LX/Esn;->h:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    :goto_6
    invoke-virtual {v4, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 594389
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 594390
    const/16 v1, 0x1f

    const v2, -0x30a9da23

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 594391
    :cond_1
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    iget-object v5, p4, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0a51

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 594392
    iget-object v4, p4, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {p4}, LX/Esn;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1d24

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v8, v5, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 594393
    :cond_2
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 594394
    :cond_3
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_2

    .line 594395
    :cond_4
    iget-object v4, p4, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_3

    .line 594396
    :cond_5
    iget-object v4, p4, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_4

    .line 594397
    :cond_6
    const v2, 0x7f0b1d25

    goto :goto_5

    .line 594398
    :cond_7
    const/4 v2, 0x0

    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 594339
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 594340
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594341
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594342
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 594343
    const-string v2, "faceversary_card_collage"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "faceversary_video_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friendversary_polaroids_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friendversary_card_collage_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friend_birthday_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friend_birthday_card"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friendversary_video_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "birthday_video_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "self_birthday_profile_pic_frame"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 594344
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 594345
    goto :goto_0

    :cond_2
    move v0, v1

    .line 594346
    goto :goto_0
.end method
