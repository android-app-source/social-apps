.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final c:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594194
    new-instance v0, LX/3Xo;

    invoke-direct {v0}, LX/3Xo;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->a:LX/1Cz;

    .line 594195
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x41000000    # 8.0f

    .line 594196
    iput v1, v0, LX/1UY;->c:F

    .line 594197
    move-object v0, v0

    .line 594198
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594191
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594192
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594193
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;
    .locals 4

    .prologue
    .line 594166
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    monitor-enter v1

    .line 594167
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594168
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594169
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594170
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594171
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 594172
    move-object v0, p0

    .line 594173
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594174
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594175
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594176
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594190
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 594187
    const/4 v4, 0x0

    .line 594188
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->c:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594189
    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7d9619fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594181
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    check-cast p4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;

    .line 594182
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 594183
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 594184
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 594185
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v2, v1}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 594186
    const/16 v1, 0x1f

    const v2, -0x420d049f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 594177
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594178
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 594180
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
