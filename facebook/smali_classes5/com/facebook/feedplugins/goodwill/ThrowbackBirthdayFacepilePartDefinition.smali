.class public Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/FacepilePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594115
    const v0, 0x7f0314a3

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594068
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594069
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594070
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    .line 594071
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 594072
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;
    .locals 6

    .prologue
    .line 594103
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    monitor-enter v1

    .line 594104
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594105
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594106
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594107
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594108
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FacepilePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FacepilePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 594109
    move-object v0, p0

    .line 594110
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594111
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594112
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594114
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 594075
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 594076
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 594077
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 594078
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 594079
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 594080
    invoke-static {v1}, LX/6X0;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    move-result-object v1

    .line 594081
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 594082
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 594083
    :goto_0
    const-string v3, "birthday_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 594084
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v4

    const/high16 v5, 0x40c00000    # 6.0f

    .line 594085
    iput v5, v4, LX/1UY;->b:F

    .line 594086
    move-object v4, v4

    .line 594087
    const/high16 v5, 0x41000000    # 8.0f

    .line 594088
    iput v5, v4, LX/1UY;->c:F

    .line 594089
    move-object v4, v4

    .line 594090
    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    invoke-direct {v3, p2, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594091
    :goto_1
    const v0, 0x7f0d2ee2

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->c:Lcom/facebook/multirow/parts/FacepilePartDefinition;

    new-instance v4, LX/8Cj;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    invoke-direct {v4, v1, v7, v5}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;I)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594092
    const v0, 0x7f0d2ee3

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 594093
    return-object v7

    .line 594094
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;->a()LX/0Px;

    move-result-object v1

    .line 594095
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 594096
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_3

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLUser;

    .line 594097
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    if-eqz p3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_2

    .line 594098
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 594099
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 594100
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v1, v3

    .line 594101
    goto :goto_0

    .line 594102
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v4

    invoke-virtual {v4}, LX/1UY;->i()LX/1Ua;

    move-result-object v4

    const v5, 0x7f0a00ea

    const/4 v6, -0x1

    invoke-direct {v3, p2, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594073
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 594074
    invoke-static {p1}, LX/C4e;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
