.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static n:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/C51;

.field private final g:LX/1Kf;

.field private final h:LX/03V;

.field private final i:LX/1Nq;

.field public final j:LX/23P;

.field private final k:LX/0Uh;

.field private final l:LX/Es5;

.field private final m:LX/1Cn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 593962
    new-instance v0, LX/3Xn;

    invoke-direct {v0}, LX/3Xn;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->a:LX/1Cz;

    .line 593963
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 593964
    iput v1, v0, LX/1UY;->b:F

    .line 593965
    move-object v0, v0

    .line 593966
    iput v1, v0, LX/1UY;->c:F

    .line 593967
    move-object v0, v0

    .line 593968
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Kf;LX/03V;LX/1Nq;LX/23P;LX/0Uh;LX/Es5;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593969
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593970
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593971
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 593972
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 593973
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->f:LX/C51;

    .line 593974
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->g:LX/1Kf;

    .line 593975
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->h:LX/03V;

    .line 593976
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->i:LX/1Nq;

    .line 593977
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->j:LX/23P;

    .line 593978
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->k:LX/0Uh;

    .line 593979
    iput-object p10, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->l:LX/Es5;

    .line 593980
    iput-object p11, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->m:LX/1Cn;

    .line 593981
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;
    .locals 15

    .prologue
    .line 593982
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    monitor-enter v1

    .line 593983
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593984
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593985
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593986
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593987
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    const-class v7, LX/C51;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/C51;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v11

    check-cast v11, LX/23P;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/Es5;->a(LX/0QB;)LX/Es5;

    move-result-object v13

    check-cast v13, LX/Es5;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v14

    check-cast v14, LX/1Cn;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Kf;LX/03V;LX/1Nq;LX/23P;LX/0Uh;LX/Es5;LX/1Cn;)V

    .line 593988
    move-object v0, v3

    .line 593989
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593990
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593991
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593992
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593993
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 593994
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 593995
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593996
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->k:LX/0Uh;

    const/16 v1, 0x4dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 593997
    if-eqz v0, :cond_0

    .line 593998
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593999
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Esx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594000
    iget-object v10, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->f:LX/C51;

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->m:LX/1Cn;

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->h:LX/03V;

    const-string v4, "promotion"

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->i:LX/1Nq;

    iget-object v6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->g:LX/1Kf;

    iget-object v7, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->l:LX/Es5;

    move-object v8, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, LX/Esx;->a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;LX/Es5;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v10, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594001
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 594002
    :cond_0
    iget-object v9, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->f:LX/C51;

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->m:LX/1Cn;

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->h:LX/03V;

    const-string v4, "promotion"

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->i:LX/1Nq;

    iget-object v6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->g:LX/1Kf;

    move-object v7, p2

    move-object v8, p3

    invoke-static/range {v0 .. v8}, LX/Esx;->a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v9, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x68abf187

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594003
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    .line 594004
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 594005
    const v1, 0x7f0d2f06

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 594006
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->j:LX/23P;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 594007
    const/16 v1, 0x1f

    const v2, -0x1b33c38b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594008
    const/4 v0, 0x1

    return v0
.end method
