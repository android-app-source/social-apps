.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Es4;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 594336
    new-instance v0, LX/3Xp;

    invoke-direct {v0}, LX/3Xp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 594333
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 594334
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 594335
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;
    .locals 4

    .prologue
    .line 594291
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    monitor-enter v1

    .line 594292
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 594293
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594294
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594295
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 594296
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 594297
    move-object v0, p0

    .line 594298
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 594299
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594300
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 594301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 594332
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 594318
    check-cast p2, LX/Es4;

    .line 594319
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    .line 594320
    iget v1, p2, LX/Es4;->a:F

    move v1, v1

    .line 594321
    iput v1, v0, LX/1UY;->d:F

    .line 594322
    move-object v0, v0

    .line 594323
    const/high16 v1, 0x41000000    # 8.0f

    .line 594324
    iput v1, v0, LX/1UY;->c:F

    .line 594325
    move-object v0, v0

    .line 594326
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    .line 594327
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    .line 594328
    iget-object v3, p2, LX/Es4;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 594329
    iget-object v4, p2, LX/Es4;->d:LX/1X9;

    move-object v4, v4

    .line 594330
    invoke-direct {v2, v3, v0, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 594331
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x66ecf4ed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 594311
    check-cast p1, LX/Es4;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    check-cast p4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;

    .line 594312
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 594313
    iget-object v1, p1, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    move-object v1, v1

    .line 594314
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 594315
    iget-object v2, p1, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    move-object v2, v2

    .line 594316
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 594317
    const/16 v1, 0x1f

    const v2, -0x22569646

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 594302
    check-cast p1, LX/Es4;

    .line 594303
    iget-object v0, p1, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    move-object v0, v0

    .line 594304
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 594305
    iget-object v0, p1, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    move-object v0, v0

    .line 594306
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 594307
    iget-object v0, p1, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    move-object v0, v0

    .line 594308
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 594309
    iget-object v0, p1, LX/Es4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 594310
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
