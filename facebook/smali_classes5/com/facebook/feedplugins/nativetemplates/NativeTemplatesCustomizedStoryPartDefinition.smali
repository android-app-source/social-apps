.class public Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/3j4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3j4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 632012
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 632013
    iput-object p2, p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->d:LX/3j4;

    .line 632014
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 632009
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 632010
    check-cast v3, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 632011
    iget-object v0, p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->d:LX/3j4;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v1

    move-object v2, p3

    check-cast v2, LX/1Pq;

    move-object v4, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;
    .locals 5

    .prologue
    .line 631998
    const-class v1, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    monitor-enter v1

    .line 631999
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 632000
    sput-object v2, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 632001
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632002
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 632003
    new-instance p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v4

    check-cast v4, LX/3j4;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;-><init>(Landroid/content/Context;LX/3j4;)V

    .line 632004
    move-object v0, p0

    .line 632005
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 632006
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 632007
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 632008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 631995
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 631996
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    .line 631997
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->y()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 632015
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 631994
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 631993
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesCustomizedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 631992
    const/4 v0, 0x0

    return-object v0
.end method
