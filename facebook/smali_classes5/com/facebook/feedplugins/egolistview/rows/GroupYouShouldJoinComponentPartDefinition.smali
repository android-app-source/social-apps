.class public Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/3m9;

.field private final f:LX/1V0;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593364
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3m9;LX/1V0;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593359
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 593360
    const/4 v0, -0x2

    iput v0, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->g:I

    .line 593361
    iput-object p2, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->e:LX/3m9;

    .line 593362
    iput-object p3, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->f:LX/1V0;

    .line 593363
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 593315
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->e:LX/3m9;

    const/4 v1, 0x0

    .line 593316
    new-instance v2, LX/3mA;

    invoke-direct {v2, v0}, LX/3mA;-><init>(LX/3m9;)V

    .line 593317
    iget-object v3, v0, LX/3m9;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3mB;

    .line 593318
    if-nez v3, :cond_0

    .line 593319
    new-instance v3, LX/3mB;

    invoke-direct {v3, v0}, LX/3mB;-><init>(LX/3m9;)V

    .line 593320
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/3mB;->a$redex0(LX/3mB;LX/1De;IILX/3mA;)V

    .line 593321
    move-object v2, v3

    .line 593322
    move-object v1, v2

    .line 593323
    move-object v1, v1

    .line 593324
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 593325
    iget-object v2, v1, LX/3mB;->a:LX/3mA;

    iput-object v0, v2, LX/3mA;->b:LX/1Po;

    .line 593326
    iget-object v2, v1, LX/3mB;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 593327
    move-object v0, v1

    .line 593328
    iget-object v1, v0, LX/3mB;->a:LX/3mA;

    iput-object p2, v1, LX/3mA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593329
    iget-object v1, v0, LX/3mB;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 593330
    move-object v0, v0

    .line 593331
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 593332
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 593333
    iget-object v2, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;
    .locals 6

    .prologue
    .line 593348
    const-class v1, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;

    monitor-enter v1

    .line 593349
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593350
    sput-object v2, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593351
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593352
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593353
    new-instance p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3m9;->a(LX/0QB;)LX/3m9;

    move-result-object v4

    check-cast v4, LX/3m9;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;-><init>(Landroid/content/Context;LX/3m9;LX/1V0;)V

    .line 593354
    move-object v0, p0

    .line 593355
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593356
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593357
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593358
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 593347
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 593346
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 593338
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593339
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593340
    check-cast v0, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    .line 593341
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    .line 593342
    iget v0, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->g:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    .line 593343
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget v1, LX/3mS;->a:I

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->g:I

    .line 593344
    :cond_0
    iget v0, p0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->g:I

    invoke-static {p1, v0}, LX/3mK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/0Px;

    move-result-object v0

    .line 593345
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 593335
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593336
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593337
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 593334
    sget-object v0, Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
