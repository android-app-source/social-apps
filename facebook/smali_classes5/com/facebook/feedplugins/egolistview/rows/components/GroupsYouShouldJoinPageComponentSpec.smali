.class public Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final c:LX/1nu;

.field public final d:LX/3mx;

.field public final e:LX/3mE;

.field public final f:LX/3my;

.field public final g:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 637437
    const-class v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a:Ljava/lang/String;

    .line 637438
    const-class v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/3mx;LX/3mE;LX/3my;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637440
    iput-object p1, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->c:LX/1nu;

    .line 637441
    iput-object p2, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->d:LX/3mx;

    .line 637442
    iput-object p3, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->e:LX/3mE;

    .line 637443
    iput-object p4, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->f:LX/3my;

    .line 637444
    iput-object p5, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->g:LX/0Uh;

    .line 637445
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/String;)LX/1Dh;
    .locals 4

    .prologue
    .line 637446
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->b(LX/1De;Ljava/lang/String;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f020ec3

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0b0939

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x4

    const v3, 0x7f0b0939

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    const v3, 0x7f0b0938

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b093a

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b093a

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProfile;LX/3mR;)LX/1Dh;
    .locals 8

    .prologue
    const/4 v0, 0x3

    const/16 v5, 0x8

    const/4 v3, 0x2

    .line 637447
    iget-object v1, p3, LX/3mR;->b:LX/254;

    .line 637448
    invoke-interface {v1}, LX/254;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    .line 637449
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 637450
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 637451
    :goto_0
    move-object v1, v2

    .line 637452
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProfile;->x()I

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 637453
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020a3c

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    .line 637454
    iget-object v2, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->c:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0933

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0935

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    move-object v1, v2

    .line 637455
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/3mZ;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    .line 637456
    :goto_1
    return-object v0

    .line 637457
    :cond_0
    iget-object v1, p3, LX/3mR;->b:LX/254;

    const/4 v2, 0x0

    const/4 p3, 0x3

    .line 637458
    invoke-interface {v1}, LX/254;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v4

    .line 637459
    if-nez v4, :cond_5

    .line 637460
    :cond_1
    :goto_2
    move-object v4, v2

    .line 637461
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_2

    move v1, v0

    .line 637462
    :goto_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020a3c

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 637463
    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v1, :cond_3

    .line 637464
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 637465
    iget-object v5, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->c:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0934

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0935

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    move-object v0, v5

    .line 637466
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 637467
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 637468
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_3

    .line 637469
    :cond_3
    invoke-static {p1}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/3mZ;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-object v0, v2

    .line 637470
    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 637471
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object v7

    .line 637472
    if-eqz v7, :cond_1

    .line 637473
    invoke-static {p3}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 637474
    const/4 v2, 0x0

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p2

    move v6, v2

    :goto_5
    if-eq v6, p2, :cond_7

    .line 637475
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    .line 637476
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 637477
    if-eqz v2, :cond_6

    .line 637478
    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    .line 637479
    if-eqz v2, :cond_6

    .line 637480
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637481
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, p3, :cond_7

    .line 637482
    :cond_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_5

    :cond_7
    move-object v2, v4

    .line 637483
    goto/16 :goto_2
.end method

.method private static a(LX/1De;)LX/1Di;
    .locals 5

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x1

    .line 637484
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f020afc

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 637485
    const v1, 0x55bbb778

    const/4 v4, 0x0

    invoke-static {p0, v1, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 637486
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0a058e

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;I)LX/1Di;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 637487
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ce

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v7, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1De;II)LX/1ne;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 637488
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00cf

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 637489
    if-lez p1, :cond_0

    .line 637490
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081c2a

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00ce

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, p1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 637491
    :cond_0
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;
    .locals 9

    .prologue
    .line 637492
    const-class v1, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    monitor-enter v1

    .line 637493
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 637494
    sput-object v2, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 637495
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637496
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 637497
    new-instance v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/3mx;->a(LX/0QB;)LX/3mx;

    move-result-object v5

    check-cast v5, LX/3mx;

    invoke-static {v0}, LX/3mE;->a(LX/0QB;)LX/3mE;

    move-result-object v6

    check-cast v6, LX/3mE;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v7

    check-cast v7, LX/3my;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;-><init>(LX/1nu;LX/3mx;LX/3mE;LX/3my;LX/0Uh;)V

    .line 637498
    move-object v0, v3

    .line 637499
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 637500
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637501
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 637502
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1De;Ljava/lang/String;)LX/1Di;
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 637503
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a0428

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f020a8d

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3, v3}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method
