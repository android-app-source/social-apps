.class public Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595407
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595369
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 595370
    iput-object p2, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->e:LX/1V0;

    .line 595371
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 595389
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595390
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 595391
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 595392
    const/4 v1, 0x0

    .line 595393
    new-instance v2, LX/C89;

    invoke-direct {v2}, LX/C89;-><init>()V

    .line 595394
    sget-object v3, LX/C8A;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C88;

    .line 595395
    if-nez v3, :cond_0

    .line 595396
    new-instance v3, LX/C88;

    invoke-direct {v3}, LX/C88;-><init>()V

    .line 595397
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C88;->a$redex0(LX/C88;LX/1De;IILX/C89;)V

    .line 595398
    move-object v2, v3

    .line 595399
    move-object v1, v2

    .line 595400
    move-object v1, v1

    .line 595401
    iget-object v2, v1, LX/C88;->a:LX/C89;

    iput-object v0, v2, LX/C89;->a:Ljava/lang/String;

    .line 595402
    iget-object v2, v1, LX/C88;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 595403
    move-object v0, v1

    .line 595404
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 595405
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 595406
    iget-object v2, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 595378
    const-class v1, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;

    monitor-enter v1

    .line 595379
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595380
    sput-object v2, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595381
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595382
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595383
    new-instance p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;)V

    .line 595384
    move-object v0, p0

    .line 595385
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595386
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595387
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595388
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 595377
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 595376
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595375
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 595373
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595374
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 595372
    sget-object v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedStoriesHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
