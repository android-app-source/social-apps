.class public Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595572
    new-instance v0, LX/3YB;

    invoke-direct {v0}, LX/3YB;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595566
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595567
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    .line 595568
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 595569
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595570
    iput-object p4, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 595571
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;
    .locals 7

    .prologue
    .line 595573
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;

    monitor-enter v1

    .line 595574
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595575
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595576
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595577
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595578
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V

    .line 595579
    move-object v0, p0

    .line 595580
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595581
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595582
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595565
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 595556
    check-cast p2, LX/C33;

    .line 595557
    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595558
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 595559
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595560
    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C33;->b:LX/C34;

    invoke-static {v2, v3}, LX/C31;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595561
    const v1, 0x7f0d1645

    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595562
    const v0, 0x7f0d1644

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595563
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595564
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 595551
    check-cast p1, LX/C33;

    .line 595552
    iget-object v0, p1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595553
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 595554
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 595555
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
