.class public Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Landroid/widget/FrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/C31;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595633
    new-instance v0, LX/3YD;

    invoke-direct {v0}, LX/3YD;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595634
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595635
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    .line 595636
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595637
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->d:LX/C31;

    .line 595638
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;
    .locals 6

    .prologue
    .line 595639
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    monitor-enter v1

    .line 595640
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595641
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595644
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/C31;->b(LX/0QB;)LX/C31;

    move-result-object v5

    check-cast v5, LX/C31;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;)V

    .line 595645
    move-object v0, p0

    .line 595646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595650
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 595651
    check-cast p2, LX/C33;

    .line 595652
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->d:LX/C31;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C33;->b:LX/C34;

    .line 595653
    sget-object v4, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-eq v3, v4, :cond_0

    sget-object v4, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-ne v3, v4, :cond_1

    .line 595654
    :cond_0
    new-instance v4, LX/1X6;

    iget-object v5, v1, LX/C31;->a:LX/C32;

    invoke-virtual {v5}, LX/C32;->a()LX/1Ua;

    move-result-object v5

    sget-object p3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v4, v2, v5, p3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 595655
    :goto_0
    move-object v1, v4

    .line 595656
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595657
    const v1, 0x7f0d1643

    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595658
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 595659
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595660
    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v4, LX/1X6;

    iget-object v5, v1, LX/C31;->a:LX/C32;

    invoke-virtual {v5}, LX/C32;->a()LX/1Ua;

    move-result-object v5

    sget-object p3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v4, v2, v5, p3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595661
    check-cast p1, LX/C33;

    invoke-static {p1}, LX/C3N;->b(LX/C33;)Z

    move-result v0

    return v0
.end method
