.class public Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

.field private final f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595630
    new-instance v0, LX/3YC;

    invoke-direct {v0}, LX/3YC;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595623
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595624
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    .line 595625
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595626
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 595627
    iput-object p4, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 595628
    const v0, 0x7f020cce

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->f:Landroid/graphics/drawable/Drawable;

    .line 595629
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;
    .locals 9

    .prologue
    .line 595612
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;

    monitor-enter v1

    .line 595613
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595614
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595615
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595616
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595617
    new-instance v3, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;Landroid/content/res/Resources;)V

    .line 595618
    move-object v0, v3

    .line 595619
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595620
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595621
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595611
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 595594
    check-cast p2, LX/C33;

    const/4 v1, 0x0

    .line 595595
    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595596
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 595597
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595598
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595599
    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v3, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/C33;->b:LX/C34;

    invoke-static {v3, v4}, LX/C31;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v3

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595600
    const v2, 0x7f0d1646

    iget-object v3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->d:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    .line 595601
    iput-object v5, v4, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 595602
    move-object v4, v4

    .line 595603
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->f:Landroid/graphics/drawable/Drawable;

    .line 595604
    :goto_0
    iput-object v0, v4, LX/2f8;->j:Landroid/graphics/drawable/Drawable;

    .line 595605
    move-object v0, v4

    .line 595606
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595607
    const v0, 0x7f0d1644

    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595608
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->e:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595609
    return-object v1

    :cond_0
    move-object v0, v1

    .line 595610
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 595586
    check-cast p1, LX/C33;

    const/4 v1, 0x0

    .line 595587
    iget-object v0, p1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595588
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 595589
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595590
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 595591
    :goto_0
    return v0

    .line 595592
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 595593
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
