.class public Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "LX/C8S;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1nA;

.field private final c:LX/1zC;

.field private final d:LX/1xv;

.field private final e:Landroid/content/res/Resources;

.field public final f:LX/C3a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595508
    new-instance v0, LX/3YA;

    invoke-direct {v0}, LX/3YA;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1xv;LX/1zC;Landroid/content/res/Resources;LX/1nA;LX/C3a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595509
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595510
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->d:LX/1xv;

    .line 595511
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->c:LX/1zC;

    .line 595512
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->e:Landroid/content/res/Resources;

    .line 595513
    iput-object p4, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->b:LX/1nA;

    .line 595514
    iput-object p5, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->f:LX/C3a;

    .line 595515
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;
    .locals 9

    .prologue
    .line 595516
    const-class v1, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    monitor-enter v1

    .line 595517
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595518
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595521
    new-instance v3, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v4

    check-cast v4, LX/1xv;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v5

    check-cast v5, LX/1zC;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v7

    check-cast v7, LX/1nA;

    invoke-static {v0}, LX/C3a;->b(LX/0QB;)LX/C3a;

    move-result-object v8

    check-cast v8, LX/C3a;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;-><init>(LX/1xv;LX/1zC;Landroid/content/res/Resources;LX/1nA;LX/C3a;)V

    .line 595522
    move-object v0, v3

    .line 595523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595527
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 595528
    check-cast p2, LX/C33;

    .line 595529
    sget-object v0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object v1, p2, LX/C33;->b:LX/C34;

    invoke-virtual {v0, v1}, LX/C34;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->d:LX/1xv;

    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->c()LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v0

    .line 595530
    :goto_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->b:LX/1nA;

    iget-object v3, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 595531
    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->c:LX/1zC;

    iget-object v3, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->e:Landroid/content/res/Resources;

    const v4, 0x7f0b1d6b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v1, v3}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 595532
    new-instance v2, LX/C8S;

    invoke-direct {v2, v0, v1}, LX/C8S;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v2

    .line 595533
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->d:LX/1xv;

    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->a()LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->c()LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4cc53155

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595534
    check-cast p1, LX/C33;

    check-cast p2, LX/C8S;

    check-cast p4, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    .line 595535
    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->f:LX/C3a;

    .line 595536
    iget-object v2, p1, LX/C33;->b:LX/C34;

    sget-object p0, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    invoke-virtual {v2, p0}, LX/C34;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 595537
    const-string v2, "grs_imp"

    invoke-static {p1}, LX/C3a;->c(LX/C33;)LX/162;

    move-result-object p0

    invoke-static {v2, p0}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 595538
    iget-object p0, v1, LX/C3a;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 595539
    :cond_0
    iget-object v1, p2, LX/C8S;->a:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 595540
    iget-object v1, p2, LX/C8S;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 595541
    iget-object v1, p1, LX/C33;->b:LX/C34;

    sget-object v2, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x3

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setMaxMessageLines(I)V

    .line 595542
    const/16 v1, 0x1f

    const v2, -0x1eceeb05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 595543
    :cond_1
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595544
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 595545
    check-cast p4, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    const/4 v0, 0x0

    .line 595546
    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 595547
    invoke-virtual {p4, v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 595548
    return-void
.end method
