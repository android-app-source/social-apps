.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/DDk;",
        "LX/1Ps;",
        "LX/DDc;",
        ">;"
    }
.end annotation


# static fields
.field public static final c:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:LX/9LP;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final f:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595224
    new-instance v0, LX/3Y3;

    invoke-direct {v0}, LX/3Y3;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9LP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595219
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595220
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->d:LX/9LP;

    .line 595221
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595222
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->f:Landroid/content/Context;

    .line 595223
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 595206
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    monitor-enter v1

    .line 595207
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595208
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595209
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595210
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595211
    new-instance p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/9LP;->b(LX/0QB;)LX/9LP;

    move-result-object v5

    check-cast v5, LX/9LP;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/9LP;)V

    .line 595212
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    .line 595213
    iput-object v3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->a:LX/0ad;

    iput-object v4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->b:LX/17W;

    .line 595214
    move-object v0, p0

    .line 595215
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595216
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595217
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595218
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 595202
    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 595203
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595204
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 595205
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iC()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-static {v1}, LX/2vB;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595201
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 595163
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595164
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 595165
    invoke-static {p2}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 595166
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, v0, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595167
    :cond_0
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 595168
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2vB;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 595169
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->a:LX/0ad;

    sget-short v2, LX/DIp;->j:S

    const/4 p2, 0x0

    invoke-interface {v1, v2, p2}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 595170
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->a:LX/0ad;

    sget-short v3, LX/DIp;->k:S

    const/4 p2, 0x0

    invoke-interface {v2, v3, p2}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 595171
    if-eqz v0, :cond_2

    .line 595172
    sget-object v0, LX/DDk;->MARK_AS_AVAILABLE:LX/DDk;

    .line 595173
    :goto_1
    move-object v0, v0

    .line 595174
    return-object v0

    .line 595175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 595176
    :cond_2
    if-eqz v1, :cond_4

    .line 595177
    if-eqz v2, :cond_3

    sget-object v0, LX/DDk;->CROSS_POST_GRAY:LX/DDk;

    goto :goto_1

    :cond_3
    sget-object v0, LX/DDk;->CROSS_POST:LX/DDk;

    goto :goto_1

    .line 595178
    :cond_4
    sget-object v0, LX/DDk;->MARK_AS_SOLD:LX/DDk;

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x528ca7c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595186
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/DDk;

    check-cast p4, LX/DDc;

    .line 595187
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 595188
    invoke-static {p1}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 595189
    const/4 v2, 0x0

    invoke-virtual {p4, v2}, LX/DDc;->setButtonContainerVisibility(I)V

    .line 595190
    iget-object v2, p4, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object p3, p4, LX/DDc;->a:LX/23P;

    invoke-virtual {v2, p3}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 595191
    iget-object v2, p4, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p4}, LX/DDc;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p2, v2, p3}, LX/37Q;->a(LX/DDk;Lcom/facebook/widget/text/BetterTextView;Landroid/content/Context;)V

    .line 595192
    sget-object v2, LX/DDj;->a:[I

    invoke-virtual {p2}, LX/DDk;->ordinal()I

    move-result p3

    aget v2, v2, p3

    packed-switch v2, :pswitch_data_0

    .line 595193
    const v2, 0x7f082503

    .line 595194
    :goto_0
    iget-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->f:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 595195
    iget-object p3, p4, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595196
    new-instance v2, LX/DDi;

    invoke-direct {v2, p0, p2, p1, v1}, LX/DDi;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;LX/DDk;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p4, v2}, LX/DDc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 595197
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x2f1da8a2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 595198
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/DDc;->setButtonContainerVisibility(I)V

    goto :goto_1

    .line 595199
    :pswitch_0
    const v2, 0x7f082504

    goto :goto_0

    .line 595200
    :pswitch_1
    const v2, 0x7f082517

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595184
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595185
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 595179
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/DDc;

    .line 595180
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 595181
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2vB;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595182
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/DDc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 595183
    :cond_0
    return-void
.end method
