.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/DDe;",
        "LX/DDf;",
        "LX/1Ps;",
        "LX/DDg;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DDg;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final f:Landroid/content/Context;

.field public final g:LX/34u;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595362
    new-instance v0, LX/3Y6;

    invoke-direct {v0}, LX/3Y6;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/34u;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595354
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595355
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 595356
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 595357
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 595358
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595359
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->f:Landroid/content/Context;

    .line 595360
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->g:LX/34u;

    .line 595361
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;
    .locals 10

    .prologue
    .line 595343
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    monitor-enter v1

    .line 595344
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595345
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595346
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595347
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595348
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/34u;->a(LX/0QB;)LX/34u;

    move-result-object v9

    check-cast v9, LX/34u;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/34u;)V

    .line 595349
    move-object v0, v3

    .line 595350
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595351
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595352
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595353
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595342
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 595323
    check-cast p2, LX/DDe;

    .line 595324
    iget-object v0, p2, LX/DDe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595325
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 595326
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595327
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    iget-object v3, p2, LX/DDe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v4, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595328
    iget-object v1, p2, LX/DDe;->b:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_0

    .line 595329
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p2, LX/DDe;->b:Landroid/view/View$OnClickListener;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595330
    :cond_0
    invoke-static {v0}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 595331
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 595332
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->f:Landroid/content/Context;

    invoke-static {v2, v1}, LX/37P;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 595333
    iget-object v3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->g:LX/34u;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->gY()Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    move-result-object v2

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->NEGOTIABLE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    if-ne v2, p1, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v5, v2}, LX/34u;->a(Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;Z)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 595334
    invoke-static {v0}, LX/17E;->x(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 595335
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 595336
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    move-object v5, v0

    .line 595337
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->gv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 595338
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 595339
    :cond_1
    const/4 v0, 0x0

    .line 595340
    :goto_2
    move-object v4, v0

    .line 595341
    new-instance v0, LX/DDf;

    invoke-direct/range {v0 .. v5}, LX/DDf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x34bf0a6e    # -1.2645778E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595298
    check-cast p1, LX/DDe;

    check-cast p2, LX/DDf;

    check-cast p4, LX/DDg;

    .line 595299
    iget-object v1, p1, LX/DDe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595300
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 595301
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595302
    iget-object v2, p2, LX/DDf;->a:Ljava/lang/String;

    .line 595303
    iget-object v4, p4, LX/DDg;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595304
    iget-object v2, p2, LX/DDf;->b:Ljava/lang/String;

    .line 595305
    iget-object v4, p4, LX/DDg;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595306
    iget-object v2, p2, LX/DDf;->c:Ljava/lang/String;

    .line 595307
    iget-object v4, p4, LX/DDg;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 595308
    iget-object v2, p2, LX/DDf;->d:Ljava/lang/String;

    .line 595309
    iget-object v4, p4, LX/DDg;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595310
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 595311
    iget-object v4, p4, LX/DDg;->f:Landroid/widget/ImageView;

    const/16 p3, 0x8

    invoke-virtual {v4, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 595312
    :goto_0
    iget-object v2, p2, LX/DDf;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 595313
    if-eqz v2, :cond_1

    .line 595314
    iget-object v4, p4, LX/DDg;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p3

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v4, p3, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 595315
    :goto_1
    new-instance v2, LX/DDd;

    invoke-direct {v2, p0, v1, p1}, LX/DDd;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/DDe;)V

    invoke-virtual {p4, v2}, LX/DDg;->setOnImageBlockClickListener(Landroid/view/View$OnClickListener;)V

    .line 595316
    const/16 v1, 0x1f

    const v2, 0x214aa4a9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 595317
    :cond_0
    iget-object v4, p4, LX/DDg;->f:Landroid/widget/ImageView;

    const/4 p3, 0x0

    invoke-virtual {v4, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 595318
    :cond_1
    iget-object v4, p4, LX/DDg;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 p3, 0x0

    invoke-virtual {v4, p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595322
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 595319
    check-cast p4, LX/DDg;

    .line 595320
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/DDg;->setOnImageBlockClickListener(Landroid/view/View$OnClickListener;)V

    .line 595321
    return-void
.end method
