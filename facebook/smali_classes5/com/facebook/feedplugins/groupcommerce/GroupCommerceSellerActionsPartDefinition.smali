.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/String;",
        "LX/1Ps;",
        "LX/DDv;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DDv;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1Kf;

.field public final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:LX/11R;

.field private final g:LX/0SG;

.field public final h:LX/9LP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595235
    new-instance v0, LX/3Y5;

    invoke-direct {v0}, LX/3Y5;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Kf;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11R;LX/0SG;LX/9LP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595286
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595287
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->b:Landroid/content/Context;

    .line 595288
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->c:LX/1Kf;

    .line 595289
    iput-object p3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 595290
    iput-object p4, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 595291
    iput-object p5, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->f:LX/11R;

    .line 595292
    iput-object p6, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->g:LX/0SG;

    .line 595293
    iput-object p7, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->h:LX/9LP;

    .line 595294
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;
    .locals 11

    .prologue
    .line 595275
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    monitor-enter v1

    .line 595276
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595277
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595280
    new-instance v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v8

    check-cast v8, LX/11R;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/9LP;->b(LX/0QB;)LX/9LP;

    move-result-object v10

    check-cast v10, LX/9LP;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;-><init>(Landroid/content/Context;LX/1Kf;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11R;LX/0SG;LX/9LP;)V

    .line 595281
    move-object v0, v3

    .line 595282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 595270
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    .line 595271
    iget-object v2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->f:LX/11R;

    sget-object v3, LX/1lB;->MONTH_DAY_YEAR_SHORT_STYLE:LX/1lB;

    invoke-virtual {v2, v3, v0, v1}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    .line 595272
    iget-object v3, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08250b

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 595273
    :goto_0
    return-object v0

    .line 595274
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08250c

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595295
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 595263
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595264
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595265
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595266
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 595267
    invoke-static {v0}, LX/2vB;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082503

    .line 595268
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 595269
    :cond_0
    const v0, 0x7f082504

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x190ea601

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 595243
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/String;

    check-cast p4, LX/DDv;

    .line 595244
    iget-object v4, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 595245
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 595246
    invoke-static {v4}, LX/2vB;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    .line 595247
    if-eqz v5, :cond_2

    sget-object v6, LX/DDk;->MARK_AS_SOLD:LX/DDk;

    .line 595248
    :goto_0
    iget-object v7, p4, LX/DDv;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p4}, LX/DDv;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v6, v7, v8}, LX/37Q;->a(LX/DDk;Lcom/facebook/widget/text/BetterTextView;Landroid/content/Context;)V

    .line 595249
    iget-object v5, p4, LX/DDv;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595250
    new-instance v5, LX/DDt;

    invoke-direct {v5, p0, v4}, LX/DDt;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {p4, v5}, LX/DDv;->setOnEditPostClickListener(Landroid/view/View$OnClickListener;)V

    .line 595251
    new-instance v5, LX/DDu;

    invoke-direct {v5, p0, p1}, LX/DDu;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p4, v5}, LX/DDv;->setOnMarkAsSoldClickListener(Landroid/view/View$OnClickListener;)V

    .line 595252
    invoke-static {v4}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    .line 595253
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->cS()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->ey()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v4}, LX/2vB;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 595254
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->cS()J

    move-result-wide v6

    invoke-static {p0, v6, v7}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->a(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;J)Ljava/lang/String;

    move-result-object v4

    .line 595255
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 595256
    iget-object v6, p4, LX/DDv;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 595257
    iget-object v6, p4, LX/DDv;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 595258
    :goto_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->ey()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 595259
    iget-object v4, p4, LX/DDv;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setVisibility(I)V

    .line 595260
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x6cf28124

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 595261
    :cond_2
    sget-object v6, LX/DDk;->MARK_AS_AVAILABLE:LX/DDk;

    goto :goto_0

    .line 595262
    :cond_3
    iget-object v6, p4, LX/DDv;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595240
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 595241
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 595242
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/2vB;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 595236
    check-cast p4, LX/DDv;

    const/4 v0, 0x0

    .line 595237
    invoke-virtual {p4, v0}, LX/DDv;->setOnEditPostClickListener(Landroid/view/View$OnClickListener;)V

    .line 595238
    invoke-virtual {p4, v0}, LX/DDv;->setOnMarkAsSoldClickListener(Landroid/view/View$OnClickListener;)V

    .line 595239
    return-void
.end method
