.class public Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final k:Ljava/lang/String;

.field private static q:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition",
            "<*>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final i:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

.field private final j:LX/03V;

.field private final l:LX/2y3;

.field public final m:LX/0ad;

.field private final n:Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

.field private final p:LX/2mt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597371
    new-instance v0, LX/3Yr;

    invoke-direct {v0}, LX/3Yr;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->a:LX/1Cz;

    .line 597372
    const-class v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/03V;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;LX/2y3;LX/0ad;Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;LX/2yJ;LX/2mt;Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597355
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597356
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 597357
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    .line 597358
    iput-object p6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->j:LX/03V;

    .line 597359
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    .line 597360
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    .line 597361
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 597362
    iput-object p7, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 597363
    iput-object p8, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    .line 597364
    iput-object p9, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->l:LX/2y3;

    .line 597365
    iput-object p10, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    .line 597366
    iput-object p11, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->o:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    .line 597367
    iput-object p12, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->n:Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    .line 597368
    iput-object p14, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->p:LX/2mt;

    .line 597369
    iput-object p15, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    .line 597370
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 597347
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    monitor-enter v1

    .line 597348
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597349
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597350
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597351
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597352
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597353
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;
    .locals 16

    .prologue
    .line 597345
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-static/range {p0 .. p0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v9

    check-cast v9, LX/2y3;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    invoke-static/range {p0 .. p0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v13

    check-cast v13, LX/2yJ;

    invoke-static/range {p0 .. p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v14

    check-cast v14, LX/2mt;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    invoke-direct/range {v0 .. v15}, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/03V;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;LX/2y3;LX/0ad;Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;LX/2yJ;LX/2mt;Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;)V

    .line 597346
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597300
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 597309
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 597310
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597311
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597312
    const/4 v2, 0x1

    .line 597313
    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget-short v4, LX/JXv;->s:S

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 597314
    if-nez v3, :cond_0

    move v2, v1

    .line 597315
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-nez v3, :cond_2

    .line 597316
    :cond_1
    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->j:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".prepare"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error attempting to get Image. Attachment ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    invoke-virtual {v4}, LX/0VK;->g()LX/0VG;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/03V;->a(LX/0VG;)V

    .line 597317
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 597318
    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->l:LX/2y3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 597319
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597320
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget-short v3, LX/JXv;->m:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 597321
    if-eqz v0, :cond_b

    .line 597322
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaLightningBoltAnimationPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    move v0, v1

    .line 597323
    :goto_1
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget-short v3, LX/JXv;->j:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 597324
    if-eqz v2, :cond_6

    .line 597325
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-interface {p1, v2, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597326
    :goto_2
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget-short v3, LX/JXv;->r:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 597327
    if-eqz v2, :cond_9

    .line 597328
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->p:LX/2mt;

    invoke-virtual {v2, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 597329
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 597330
    :cond_4
    :goto_3
    return-object v7

    .line 597331
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 597332
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v2, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_2

    .line 597333
    :cond_7
    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->o:Lcom/facebook/feedplugins/richmedia/RichMediaSwipeToOpenPartDefinition;

    new-instance v4, LX/JXj;

    .line 597334
    iget-object v5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget v6, LX/JXv;->q:I

    const/16 p3, 0xc8

    invoke-interface {v5, v6, p3}, LX/0ad;->a(II)I

    move-result v5

    move v5, v5

    .line 597335
    invoke-direct {v4, p2, v2, v5}, LX/JXj;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;I)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597336
    :goto_4
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget-short v3, LX/JXv;->g:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 597337
    if-eqz v2, :cond_a

    .line 597338
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->n:Lcom/facebook/feedplugins/richmedia/RichMediaBouncyArrowAnimationPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597339
    :goto_5
    if-eqz v1, :cond_8

    .line 597340
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597341
    :cond_8
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->m:LX/0ad;

    sget v1, LX/JXv;->l:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 597342
    if-lez v0, :cond_4

    .line 597343
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/richmedia/RichMediaGradientBottomPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_3

    .line 597344
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v3, LX/2ya;

    invoke-direct {v3, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_4

    :cond_a
    move v1, v0

    goto :goto_5

    :cond_b
    move v0, v2

    goto/16 :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x71a5199a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597304
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;

    .line 597305
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597306
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 597307
    iget-object p1, p4, Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;->i:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597308
    const/16 v1, 0x1f

    const v2, 0x7ac2a8a3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597301
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597302
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597303
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
