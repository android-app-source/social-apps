.class public Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JXr;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JXr;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition",
            "<TE;",
            "LX/JXr;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "LX/JXr;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597296
    new-instance v0, LX/3Yq;

    invoke-direct {v0}, LX/3Yq;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597256
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597257
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    .line 597258
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    .line 597259
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 597260
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    .line 597261
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->f:LX/0ad;

    .line 597262
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;
    .locals 9

    .prologue
    .line 597285
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    monitor-enter v1

    .line 597286
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597287
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597288
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597289
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597290
    new-instance v3, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;-><init>(Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;LX/0ad;)V

    .line 597291
    move-object v0, v3

    .line 597292
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597293
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597294
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597297
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 597278
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597279
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597280
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 597281
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->e:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v2, LX/3EE;

    const/4 v3, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    invoke-direct {v2, p2, v3, v4, v0}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597282
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    new-instance v2, LX/JXt;

    invoke-direct {v2, p2, v0}, LX/JXt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597283
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoTransitionPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597284
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3d78125a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597273
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/JXr;

    .line 597274
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597275
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 597276
    iget-object p1, p4, LX/JXr;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 597277
    const/16 v1, 0x1f

    const v2, 0x233afb0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 597263
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 597264
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597265
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597266
    const v2, -0x3d25a87c

    invoke-static {v0, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v2

    .line 597267
    if-nez v2, :cond_0

    move v0, v1

    .line 597268
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L()LX/0Px;

    move-result-object v2

    sget-object v3, LX/JXg;->VIDEO_TRANSITION:LX/JXg;

    .line 597269
    if-nez v2, :cond_2

    .line 597270
    const/4 p0, 0x0

    .line 597271
    :goto_1
    move v2, p0

    .line 597272
    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x4ed245b

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, LX/JXg;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p0

    goto :goto_1
.end method
