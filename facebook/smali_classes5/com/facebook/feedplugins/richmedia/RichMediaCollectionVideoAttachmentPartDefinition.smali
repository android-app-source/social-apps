.class public Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JXY;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JXY;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition",
            "<TE;",
            "LX/JXr;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "LX/JXr;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

.field public final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597437
    new-instance v0, LX/3Yt;

    invoke-direct {v0}, LX/3Yt;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597438
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597439
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    .line 597440
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 597441
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    .line 597442
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->e:LX/0ad;

    .line 597443
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 597444
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 597445
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597446
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597449
    new-instance p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;LX/0ad;)V

    .line 597450
    move-object v0, p0

    .line 597451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597455
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 597456
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597457
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 597458
    const v1, 0x7f0d2a1a

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v3, LX/3EE;

    const/4 v4, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    invoke-direct {v3, p2, v4, v5, v0}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597459
    const v1, 0x7f0d2a1a

    iget-object v2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoViewLauncherPartDefinition;

    new-instance v3, LX/JXt;

    invoke-direct {v3, p2, v0}, LX/JXt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597460
    const v0, 0x7f0d2a10

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597461
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x78d9c5d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597462
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/JXY;

    .line 597463
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597464
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 597465
    iget-object p1, p4, LX/JXY;->a:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597466
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionVideoAttachmentPartDefinition;->e:LX/0ad;

    sget-char v2, LX/JXv;->d:C

    const-string p1, "#fff6f7f9"

    invoke-interface {v1, v2, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 597467
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x10

    if-lt v2, p0, :cond_0

    .line 597468
    iget-object v2, p4, LX/JXY;->a:Landroid/widget/TextView;

    new-instance p0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 597469
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x625d033

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 597470
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597471
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597472
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597473
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
