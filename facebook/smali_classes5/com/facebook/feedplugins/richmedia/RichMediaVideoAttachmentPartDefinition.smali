.class public Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Lcom/facebook/video/engine/VideoPlayerParams;",
        "TE;",
        "LX/JXn;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static p:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

.field private final c:Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

.field private final d:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition",
            "<*>;"
        }
    .end annotation
.end field

.field private final e:LX/2mZ;

.field private final f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final h:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

.field private final i:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition",
            "<TE;",
            "LX/JXn;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1VK;

.field public final k:LX/198;

.field public final l:LX/0iY;

.field public final m:LX/0iX;

.field public final n:LX/1YQ;

.field public final o:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597253
    new-instance v0, LX/3Yp;

    invoke-direct {v0}, LX/3Yp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;LX/2mZ;LX/1VK;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;LX/198;LX/0iY;LX/0iX;LX/1YQ;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597237
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597238
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    .line 597239
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

    .line 597240
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    .line 597241
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->e:LX/2mZ;

    .line 597242
    iput-object p6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 597243
    iput-object p7, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 597244
    iput-object p8, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    .line 597245
    iput-object p9, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    .line 597246
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->j:LX/1VK;

    .line 597247
    iput-object p10, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->k:LX/198;

    .line 597248
    iput-object p12, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->m:LX/0iX;

    .line 597249
    iput-object p11, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->l:LX/0iY;

    .line 597250
    iput-object p13, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->n:LX/1YQ;

    .line 597251
    iput-object p14, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->o:LX/0ad;

    .line 597252
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 597165
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 597166
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597167
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597168
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597169
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597170
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597171
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/video/engine/VideoPlayerParams;LX/JXn;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "LX/JXn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 597208
    const/4 v3, 0x1

    .line 597209
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597210
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597211
    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v4, 0x4ed245b

    if-ne v2, v4, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 597212
    iget-object v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p2, LX/JXn;->k:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 597213
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597214
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 597215
    iget-object v1, p2, LX/JXn;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597216
    return-void

    .line 597217
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 597218
    :cond_2
    iget-object v2, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v2, p2, LX/JXn;->k:Ljava/lang/String;

    .line 597219
    iget-object v2, p2, LX/JXn;->a:LX/2mn;

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v4}, LX/2mn;->d(Lcom/facebook/feed/rows/core/props/FeedProps;F)D

    move-result-wide v5

    iput-wide v5, p2, LX/JXn;->j:D

    .line 597220
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v4, "GraphQLStoryProps"

    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v4, "CoverImageParamsKey"

    .line 597221
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 597222
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-static {v1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    :goto_2
    move-object v5, v5

    .line 597223
    invoke-virtual {v2, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v4, "SubtitlesLocalesKey"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 597224
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    .line 597225
    iput-object p1, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 597226
    move-object v2, v2

    .line 597227
    invoke-virtual {v2, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v1

    iget-wide v5, p2, LX/JXn;->j:D

    .line 597228
    iput-wide v5, v1, LX/2pZ;->e:D

    .line 597229
    move-object v1, v1

    .line 597230
    invoke-virtual {v1}, LX/2pZ;->b()LX/2pa;

    move-result-object v1

    .line 597231
    invoke-virtual {p2}, LX/JXn;->getPluginSelector()LX/3Ge;

    move-result-object v2

    invoke-virtual {p2}, LX/JXn;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v1, v5}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 597232
    iget-object v2, p2, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v4, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {v2, v4}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    .line 597233
    iget-object v2, p2, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 597234
    iget-object v2, p2, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 597235
    iget-object v2, p2, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v3, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 597236
    iget-object v2, p2, LX/JXn;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto/16 :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;
    .locals 15

    .prologue
    .line 597206
    new-instance v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    const-class v4, LX/2mZ;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    const-class v5, LX/1VK;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1VK;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    invoke-static {p0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v10

    check-cast v10, LX/198;

    invoke-static {p0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v11

    check-cast v11, LX/0iY;

    invoke-static {p0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v12

    check-cast v12, LX/0iX;

    invoke-static {p0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v13

    check-cast v13, LX/1YQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaVideoAutoplayPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;LX/2mZ;LX/1VK;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;LX/198;LX/0iY;LX/0iX;LX/1YQ;LX/0ad;)V

    .line 597207
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597205
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 597178
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597179
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597180
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597181
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->e:LX/2mZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    invoke-virtual {v1}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 597182
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/richmedia/RichMediaVideoDimensionsPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597183
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 597184
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 597185
    new-instance v1, LX/2oK;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->j:LX/1VK;

    invoke-direct {v1, v2, v0, v3}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 597186
    new-instance v0, LX/0AW;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0AW;-><init>(LX/162;)V

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 597187
    iput-boolean v2, v0, LX/0AW;->d:Z

    .line 597188
    move-object v0, v0

    .line 597189
    sget-object v2, LX/04H;->SPONSORED_VIDEO:LX/04H;

    .line 597190
    iput-object v2, v0, LX/0AW;->c:LX/04H;

    .line 597191
    move-object v0, v0

    .line 597192
    invoke-virtual {v0}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v5

    .line 597193
    iget-object v8, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/video/SingleRunnableAutoplayPartDefinition;

    new-instance v0, LX/3J1;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 597194
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 597195
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    new-instance v3, LX/093;

    invoke-direct {v3}, LX/093;-><init>()V

    sget-object v6, LX/04D;->FEED:LX/04D;

    new-instance v7, LX/JXk;

    invoke-direct {v7, p0}, LX/JXk;-><init>(Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;)V

    invoke-direct/range {v0 .. v7}, LX/3J1;-><init>(LX/1KL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597196
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->o:LX/0ad;

    sget-short v1, LX/JXv;->s:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 597197
    if-eqz v0, :cond_1

    .line 597198
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/richmedia/RichMediaTapPromptPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597199
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597200
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->o:LX/0ad;

    sget-short v1, LX/JXv;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 597201
    if-eqz v0, :cond_2

    .line 597202
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentEdgeToEdgeBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597203
    :goto_0
    return-object v4

    .line 597204
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2735e413

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597177
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Lcom/facebook/video/engine/VideoPlayerParams;

    check-cast p4, LX/JXn;

    invoke-static {p1, p2, p4}, Lcom/facebook/feedplugins/richmedia/RichMediaVideoAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/video/engine/VideoPlayerParams;LX/JXn;)V

    const/16 v1, 0x1f

    const v2, 0x48f0f4f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 597173
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597174
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597176
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
