.class public Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/JXX;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/richmedia/RichMediaImageAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final e:LX/2y3;

.field private final f:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

.field public final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597423
    new-instance v0, LX/3Ys;

    invoke-direct {v0}, LX/3Ys;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597378
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597379
    iput-object p1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 597380
    iput-object p2, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    .line 597381
    iput-object p3, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->e:LX/2y3;

    .line 597382
    iput-object p4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 597383
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    .line 597384
    iput-object p6, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->g:LX/0ad;

    .line 597385
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 597424
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;

    monitor-enter v1

    .line 597425
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597426
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597427
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597428
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597429
    new-instance v3, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v6

    check-cast v6, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;LX/0ad;)V

    .line 597430
    move-object v0, v3

    .line 597431
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597432
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597433
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;)Z
    .locals 3

    .prologue
    .line 597421
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->g:LX/0ad;

    sget-short v1, LX/JXv;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597422
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 597410
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597411
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597412
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597413
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->e:LX/2y3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597414
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 597415
    :goto_0
    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->c(Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597416
    const v0, 0x7f0d2a0c

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597417
    :goto_1
    const v0, 0x7f0d2a10

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597418
    const/4 v0, 0x0

    return-object v0

    .line 597419
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/StretchedCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 597420
    :cond_1
    const v0, 0x7f0d2a0d

    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x48409d4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597386
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/JXX;

    .line 597387
    invoke-static {p0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->c(Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 597388
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597389
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 597390
    iget-object v2, p4, LX/JXX;->d:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597391
    iget-object v2, p4, LX/JXX;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597392
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597393
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 597394
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 597395
    iget-object p2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, p2

    .line 597396
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 597397
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 597398
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 597399
    :cond_0
    iget-object v2, p4, LX/JXX;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597400
    iget-object v1, p4, LX/JXX;->b:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597401
    iget-object v1, p4, LX/JXX;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 597402
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x7791338

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 597403
    :cond_1
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597404
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    .line 597405
    iget-object v2, p4, LX/JXX;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597406
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionAttachmentPartDefinition;->g:LX/0ad;

    sget-char v2, LX/JXv;->d:C

    const-string p2, "#fff6f7f9"

    invoke-interface {v1, v2, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 597407
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x10

    if-lt v2, p2, :cond_2

    .line 597408
    iget-object v2, p4, LX/JXX;->b:Landroid/widget/TextView;

    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p0

    invoke-direct {p2, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 597409
    :cond_2
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597375
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597376
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597377
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
