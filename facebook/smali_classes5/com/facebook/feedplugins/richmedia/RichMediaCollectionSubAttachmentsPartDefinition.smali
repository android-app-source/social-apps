.class public Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597476
    new-instance v0, LX/3Yu;

    invoke-direct {v0}, LX/3Yu;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597477
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 597478
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    .line 597479
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597480
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597481
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597482
    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597483
    iput-object p5, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->c:LX/0ad;

    .line 597484
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;
    .locals 9

    .prologue
    .line 597485
    const-class v1, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    monitor-enter v1

    .line 597486
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597487
    sput-object v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597488
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597489
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597490
    new-instance v3, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/0ad;)V

    .line 597491
    move-object v0, v3

    .line 597492
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597493
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597494
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 597496
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 597497
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 597498
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597499
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597500
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v3, 0x4ed245b

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    .line 597501
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    .line 597502
    :goto_1
    const/4 v0, 0x4

    if-ge v2, v0, :cond_2

    .line 597503
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597504
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 597505
    if-nez v1, :cond_0

    .line 597506
    sget-object v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v6, LX/2ya;

    invoke-direct {v6, v4}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v5, v0, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 597507
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 597508
    goto :goto_0

    .line 597509
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5c270f1f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597510
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;

    const/4 p2, 0x4

    const/4 v2, 0x0

    const/4 v7, 0x2

    .line 597511
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 597512
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597513
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    .line 597514
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 597515
    iget-object v4, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->c:LX/0ad;

    sget v6, LX/JXv;->b:I

    const/4 p3, 0x4

    invoke-interface {v4, v6, p3}, LX/0ad;->a(II)I

    move-result v4

    move v4, v4

    .line 597516
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 597517
    if-ne v1, p2, :cond_0

    move v4, v2

    .line 597518
    :goto_0
    if-ge v4, p2, :cond_2

    .line 597519
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597520
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2, v4}, LX/3hv;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/net/Uri;

    move-result-object v2

    .line 597521
    if-nez v2, :cond_7

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 597522
    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 597523
    :goto_1
    const-class v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    const-string v6, "native_newsfeed"

    invoke-static {v2, v6}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {p4, v4, v1, v2}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->a(ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 597524
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v2

    .line 597525
    :goto_2
    if-ge v4, v7, :cond_1

    .line 597526
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597527
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2, v4}, LX/3hv;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/net/Uri;

    move-result-object v2

    .line 597528
    if-nez v2, :cond_6

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 597529
    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 597530
    :goto_3
    const-class v2, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    const-string v6, "native_newsfeed"

    invoke-static {v2, v6}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {p4, v4, v1, v2}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->a(ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 597531
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 597532
    :cond_1
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 597533
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2, v7}, LX/3hv;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/net/Uri;

    move-result-object v2

    .line 597534
    if-nez v2, :cond_5

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 597535
    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 597536
    :goto_4
    const/4 v2, 0x3

    const-class v4, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;

    const-string v5, "native_newsfeed"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {p4, v2, v1, v4}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->a(ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 597537
    invoke-virtual {p4}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->a()V

    .line 597538
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->c:LX/0ad;

    sget-short v2, LX/JXv;->a:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 597539
    if-eqz v1, :cond_3

    .line 597540
    invoke-virtual {p4}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->b()V

    .line 597541
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsPartDefinition;->c:LX/0ad;

    sget-short v2, LX/JXv;->c:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 597542
    if-eqz v1, :cond_4

    .line 597543
    invoke-virtual {p4}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSubAttachmentsView;->c()V

    .line 597544
    :cond_4
    const/16 v1, 0x1f

    const v2, -0x1d1fcb76

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_5
    move-object v1, v2

    goto :goto_4

    :cond_6
    move-object v1, v2

    goto :goto_3

    :cond_7
    move-object v1, v2

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597545
    const/4 v0, 0x1

    return v0
.end method
