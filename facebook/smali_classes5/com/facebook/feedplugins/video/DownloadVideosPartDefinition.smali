.class public Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CCx;",
        "LX/CCy;",
        "TE;",
        "Landroid/widget/FrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static s:LX/0Xm;


# instance fields
.field public final e:LX/1V7;

.field public final f:LX/0tQ;

.field public final g:Landroid/os/Handler;

.field private final h:LX/16U;

.field public final i:LX/0iA;

.field public final j:LX/0yI;

.field private final k:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final l:LX/19w;

.field private final m:LX/D7Y;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/15V;

.field public final q:LX/15X;

.field private final r:LX/BUA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598037
    new-instance v0, LX/3Z4;

    invoke-direct {v0}, LX/3Z4;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a:LX/1Cz;

    .line 598038
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 598039
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 598040
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/15V;LX/BUA;LX/1V7;LX/D7Y;LX/0tQ;LX/15X;LX/0iA;LX/16U;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/19w;LX/0yI;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15V;",
            "LX/BUA;",
            "LX/1V7;",
            "LX/D7Y;",
            "LX/0tQ;",
            "LX/15X;",
            "LX/0iA;",
            "LX/16U;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/0yI;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 598206
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598207
    iput-object p1, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->p:LX/15V;

    .line 598208
    iput-object p2, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->r:LX/BUA;

    .line 598209
    iput-object p3, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->e:LX/1V7;

    .line 598210
    iput-object p4, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->m:LX/D7Y;

    .line 598211
    iput-object p5, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    .line 598212
    iput-object p6, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->q:LX/15X;

    .line 598213
    iput-object p8, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->h:LX/16U;

    .line 598214
    iput-object p7, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->i:LX/0iA;

    .line 598215
    iput-object p11, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->j:LX/0yI;

    .line 598216
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->g:Landroid/os/Handler;

    .line 598217
    iput-object p9, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->k:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 598218
    iput-object p10, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->l:LX/19w;

    .line 598219
    iput-object p12, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->n:LX/0Ot;

    .line 598220
    iput-object p13, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->o:LX/0Ot;

    .line 598221
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;
    .locals 3

    .prologue
    .line 598198
    const-class v1, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;

    monitor-enter v1

    .line 598199
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598200
    sput-object v2, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598203
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598204
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;LX/2fs;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 598185
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 598186
    invoke-static {p2}, LX/15V;->a(LX/2fs;)J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 598187
    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598188
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 598189
    invoke-static {p2}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/2fs;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598190
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->p:LX/15V;

    invoke-virtual {v0, p2}, LX/15V;->b(LX/2fs;)Ljava/lang/String;

    move-result-object v0

    .line 598191
    :goto_0
    return-object v0

    .line 598192
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 598193
    const v0, 0x7f080db6

    new-array v1, v3, [Ljava/lang/Object;

    iget-wide v2, p2, LX/2fs;->a:J

    invoke-static {v2, v3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 598194
    :cond_1
    sget-object v1, LX/CCw;->b:[I

    iget-object v2, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->m()LX/2qY;

    move-result-object v2

    invoke-virtual {v2}, LX/2qY;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 598195
    const v1, 0x7f080db1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 598196
    :pswitch_0
    const v1, 0x7f080db3

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 598197
    :pswitch_1
    const v1, 0x7f080db2

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/CCx;LX/CCy;Landroid/widget/FrameLayout;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CCx;",
            "LX/CCy;",
            "Landroid/widget/FrameLayout;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 598150
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 598151
    iget-object v0, p2, LX/CCy;->a:LX/16X;

    check-cast v0, LX/CCz;

    .line 598152
    const v1, 0x7f0d0cdd

    invoke-virtual {p3, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/widget/TextView;

    .line 598153
    const v1, 0x7f0d0cde

    invoke-virtual {p3, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 598154
    const v1, 0x7f0d0cdc

    invoke-virtual {p3, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 598155
    const v1, 0x7f0d07f4

    invoke-virtual {p3, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/fbui/glyph/GlyphView;

    .line 598156
    iput-object v4, v0, LX/CCz;->d:Landroid/widget/TextView;

    .line 598157
    iput-object v5, v0, LX/CCz;->e:Landroid/widget/TextView;

    .line 598158
    iput-object v6, v0, LX/CCz;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 598159
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/CCz;->g:Z

    .line 598160
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598161
    invoke-virtual {v6}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 598162
    invoke-virtual {v6}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0203fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 598163
    :cond_0
    iget-object v0, p2, LX/CCy;->b:Landroid/view/View$OnClickListener;

    check-cast v0, LX/D7X;

    invoke-virtual {v0}, LX/D7X;->a()V

    .line 598164
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598165
    invoke-virtual {v6, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 598166
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 598167
    iget-object v0, p2, LX/CCy;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598168
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 598169
    :goto_0
    iget-object v0, p1, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598170
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 598171
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    .line 598172
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v9

    .line 598173
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->l:LX/19w;

    invoke-virtual {v0, v9}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 598174
    iget-object v0, v1, LX/2fs;->c:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v0, v2, :cond_2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v0

    int-to-long v2, v0

    .line 598175
    :goto_1
    invoke-static {v2, v3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p0

    .line 598176
    invoke-static/range {v0 .. v6}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a$redex0(Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;LX/2fs;JLandroid/widget/TextView;Landroid/widget/TextView;Lcom/facebook/fbui/glyph/GlyphView;)V

    .line 598177
    sget-object v0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598178
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->r:LX/BUA;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/BUA;->a(Ljava/lang/String;Z)V

    .line 598179
    return-void

    .line 598180
    :cond_1
    invoke-virtual {v6, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 598181
    iget-object v0, p2, LX/CCy;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598182
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 598183
    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 598184
    :cond_2
    iget-wide v2, v1, LX/2fs;->a:J

    goto :goto_1
.end method

.method private static a(LX/2fs;)Z
    .locals 2

    .prologue
    .line 598149
    iget-object v0, p0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2fs;->d:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;LX/2fs;JLandroid/widget/TextView;Landroid/widget/TextView;Lcom/facebook/fbui/glyph/GlyphView;)V
    .locals 6

    .prologue
    .line 598099
    invoke-virtual {p6}, Lcom/facebook/fbui/glyph/GlyphView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 598100
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 598101
    invoke-virtual {p6}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 598102
    :cond_0
    sget-object v0, LX/CCw;->a:[I

    iget-object v1, p1, LX/2fs;->c:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 598103
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->j:LX/0yI;

    sget-object v1, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 598104
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->W:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 598105
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 598106
    :goto_0
    move-object v0, v0

    .line 598107
    const v1, 0x7f020841

    invoke-virtual {p6, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 598108
    const v1, 0x7f080da7

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 598109
    :goto_1
    invoke-virtual {p5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598110
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->g:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition$2;

    invoke-direct {v1, p0, v2, p1, p6}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition$2;-><init>(Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;Landroid/content/Context;LX/2fs;Lcom/facebook/fbui/glyph/GlyphView;)V

    const v2, -0x5ec62e43

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 598111
    return-void

    .line 598112
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->E()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 598113
    const v0, 0x7f080db6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 598114
    :goto_2
    move-object v0, v0

    .line 598115
    const v1, 0x7f0207d6

    invoke-virtual {p6, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 598116
    const v1, 0x7f080daa

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 598117
    :pswitch_1
    invoke-direct {p0, v2, p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(Landroid/content/Context;LX/2fs;)Ljava/lang/String;

    move-result-object v1

    .line 598118
    invoke-static {p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/2fs;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f020841

    :goto_3
    invoke-virtual {p6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 598119
    invoke-static {p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/2fs;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598120
    invoke-virtual {p6}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a008a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 598121
    :cond_1
    const v0, 0x7f080da8

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(I)V

    move-object v0, v1

    .line 598122
    goto :goto_1

    .line 598123
    :cond_2
    const v0, 0x7f020818

    goto :goto_3

    .line 598124
    :pswitch_2
    invoke-direct {p0, v2, p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(Landroid/content/Context;LX/2fs;)Ljava/lang/String;

    move-result-object v1

    .line 598125
    invoke-static {p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/2fs;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f020841

    :goto_4
    invoke-virtual {p6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 598126
    invoke-static {p1}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/2fs;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 598127
    invoke-virtual {p6}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a008a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p6, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 598128
    :cond_3
    const v0, 0x7f080da8

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(I)V

    .line 598129
    iget-object v0, p1, LX/2fs;->d:LX/2ft;

    sget-object v3, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    if-ne v0, v3, :cond_6

    .line 598130
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->V:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 598131
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 598132
    const v0, 0x7f0819fd

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 598133
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7WS;

    invoke-virtual {v0}, LX/7WS;->a()V

    move-object v0, v1

    goto/16 :goto_1

    .line 598134
    :cond_4
    const v0, 0x7f020818

    goto :goto_4

    .line 598135
    :cond_5
    const v1, 0x7f0819fe

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move-object v0, v1

    goto/16 :goto_1

    .line 598136
    :cond_7
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7WS;

    invoke-virtual {v0}, LX/7WS;->a()V

    .line 598137
    :cond_8
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->p:LX/15V;

    invoke-virtual {v0}, LX/15V;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 598138
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->q:LX/15X;

    invoke-virtual {v0, v2}, LX/15X;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598139
    :cond_9
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->E()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 598140
    const v0, 0x7f080db6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598141
    :cond_a
    sget-object v0, LX/CCw;->b:[I

    iget-object v1, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 598142
    const v0, 0x7f080daf

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598143
    :pswitch_3
    const v0, 0x7f080dae

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598144
    :pswitch_4
    const v0, 0x7f080db0

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 598145
    :cond_b
    sget-object v0, LX/CCw;->b:[I

    iget-object v1, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 598146
    const v0, 0x7f080dab

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 598147
    :pswitch_5
    const v0, 0x7f080dad

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 598148
    :pswitch_6
    const v0, 0x7f080dac

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;
    .locals 14

    .prologue
    .line 598097
    new-instance v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;

    invoke-static {p0}, LX/15V;->a(LX/0QB;)LX/15V;

    move-result-object v1

    check-cast v1, LX/15V;

    invoke-static {p0}, LX/BUA;->a(LX/0QB;)LX/BUA;

    move-result-object v2

    check-cast v2, LX/BUA;

    invoke-static {p0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v3

    check-cast v3, LX/1V7;

    const-class v4, LX/D7Y;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/D7Y;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v5

    check-cast v5, LX/0tQ;

    invoke-static {p0}, LX/15X;->a(LX/0QB;)LX/15X;

    move-result-object v6

    check-cast v6, LX/15X;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {p0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v8

    check-cast v8, LX/16U;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v10

    check-cast v10, LX/19w;

    invoke-static {p0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v11

    check-cast v11, LX/0yI;

    const/16 v12, 0xf9a

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x390f

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;-><init>(LX/15V;LX/BUA;LX/1V7;LX/D7Y;LX/0tQ;LX/15X;LX/0iA;LX/16U;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/19w;LX/0yI;LX/0Ot;LX/0Ot;)V

    .line 598098
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598096
    sget-object v0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 598073
    check-cast p2, LX/CCx;

    check-cast p3, LX/1Ps;

    .line 598074
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->k:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p2, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598075
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 598076
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 598077
    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 598078
    invoke-static {v2}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->e:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->i()LX/1Ua;

    move-result-object v2

    .line 598079
    :goto_0
    new-instance v4, LX/1X6;

    invoke-direct {v4, v3, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    move-object v1, v4

    .line 598080
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598081
    iget-object v0, p2, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598082
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 598083
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 598084
    iget-object v0, p2, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 598085
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 598086
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 598087
    new-instance v3, LX/CCz;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {v3, p0, v1, v4, v5}, LX/CCz;-><init>(Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;Ljava/lang/String;J)V

    .line 598088
    iget-object v1, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->h:LX/16U;

    const-class v4, LX/1ub;

    invoke-virtual {v1, v4, v3}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 598089
    instance-of v1, p3, LX/3Ij;

    if-eqz v1, :cond_1

    move-object v1, p3

    .line 598090
    check-cast v1, LX/1Po;

    invoke-interface {v1}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    check-cast p3, LX/3Ij;

    .line 598091
    iget-object v4, p3, LX/3Ij;->p:Ljava/lang/String;

    move-object v4, v4

    .line 598092
    invoke-static {v1, v4}, LX/3Ik;->a(LX/1PT;Ljava/lang/String;)LX/04D;

    move-result-object v1

    .line 598093
    :goto_1
    new-instance v4, LX/CCy;

    iget-object v5, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->m:LX/D7Y;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    invoke-virtual {v5, v1, v2, v0, v6}, LX/D7Y;->a(LX/04D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Z)LX/D7X;

    move-result-object v0

    invoke-direct {v4, v0, v3}, LX/CCy;-><init>(Landroid/view/View$OnClickListener;LX/16X;)V

    return-object v4

    .line 598094
    :cond_1
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v1}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v1

    goto :goto_1

    .line 598095
    :cond_2
    iget-object v2, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->e:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->j()LX/1Ua;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 3

    .prologue
    .line 598070
    check-cast p2, LX/CCy;

    .line 598071
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->h:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p2, LX/CCy;->a:LX/16X;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 598072
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xf5bab65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598069
    check-cast p1, LX/CCx;

    check-cast p2, LX/CCy;

    check-cast p4, Landroid/widget/FrameLayout;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->a(LX/CCx;LX/CCy;Landroid/widget/FrameLayout;)V

    const/16 v1, 0x1f

    const v2, 0x4aea9d4d    # 7687846.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 598053
    check-cast p1, LX/CCx;

    const/4 v1, 0x0

    .line 598054
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->f:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 598055
    :goto_0
    return v0

    .line 598056
    :cond_0
    iget-object v0, p1, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 598057
    iget-object v0, p1, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598058
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 598059
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 598060
    if-eqz v3, :cond_1

    invoke-static {v2}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->l:LX/19w;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v3}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598061
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 598062
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 598063
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 598064
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 598065
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/video/DownloadVideosPartDefinition;->r:LX/BUA;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/BUA;->a(Ljava/lang/String;Z)V

    move v0, v1

    .line 598066
    goto :goto_0

    .line 598067
    :cond_2
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 598068
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 598041
    check-cast p1, LX/CCx;

    check-cast p2, LX/CCy;

    .line 598042
    iget-object v0, p1, LX/CCx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 598043
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 598044
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 598045
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 598046
    sget-object p0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 598047
    iget-object v0, p2, LX/CCy;->a:LX/16X;

    check-cast v0, LX/CCz;

    const/4 p1, 0x0

    .line 598048
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/CCz;->g:Z

    .line 598049
    iput-object p1, v0, LX/CCz;->d:Landroid/widget/TextView;

    .line 598050
    iput-object p1, v0, LX/CCz;->e:Landroid/widget/TextView;

    .line 598051
    iput-object p1, v0, LX/CCz;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 598052
    return-void
.end method
