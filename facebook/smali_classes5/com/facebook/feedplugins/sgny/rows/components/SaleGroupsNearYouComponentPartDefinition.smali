.class public Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/JYE;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597659
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/JYE;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 597660
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 597661
    iput-object p2, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->e:LX/JYE;

    .line 597662
    iput-object p3, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->f:LX/1V0;

    .line 597663
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 597622
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->e:LX/JYE;

    const/4 v1, 0x0

    .line 597623
    new-instance v2, LX/JYD;

    invoke-direct {v2, v0}, LX/JYD;-><init>(LX/JYE;)V

    .line 597624
    iget-object v3, v0, LX/JYE;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JYC;

    .line 597625
    if-nez v3, :cond_0

    .line 597626
    new-instance v3, LX/JYC;

    invoke-direct {v3, v0}, LX/JYC;-><init>(LX/JYE;)V

    .line 597627
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JYC;->a$redex0(LX/JYC;LX/1De;IILX/JYD;)V

    .line 597628
    move-object v2, v3

    .line 597629
    move-object v1, v2

    .line 597630
    move-object v1, v1

    .line 597631
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 597632
    iget-object v2, v1, LX/JYC;->a:LX/JYD;

    iput-object v0, v2, LX/JYD;->b:LX/1Po;

    .line 597633
    iget-object v2, v1, LX/JYC;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 597634
    move-object v0, v1

    .line 597635
    iget-object v1, v0, LX/JYC;->a:LX/JYD;

    iput-object p2, v1, LX/JYD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597636
    iget-object v1, v0, LX/JYC;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 597637
    move-object v0, v0

    .line 597638
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 597639
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 597640
    iget-object v2, p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;
    .locals 6

    .prologue
    .line 597647
    const-class v1, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;

    monitor-enter v1

    .line 597648
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 597649
    sput-object v2, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 597650
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597651
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 597652
    new-instance p0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JYE;->a(LX/0QB;)LX/JYE;

    move-result-object v4

    check-cast v4, LX/JYE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;-><init>(Landroid/content/Context;LX/JYE;LX/1V0;)V

    .line 597653
    move-object v0, p0

    .line 597654
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 597655
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597656
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 597657
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 597658
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 597646
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 597641
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 597642
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 597643
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    .line 597644
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->k()LX/0Px;

    move-result-object v0

    .line 597645
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 597621
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 597620
    sget-object v0, Lcom/facebook/feedplugins/sgny/rows/components/SaleGroupsNearYouComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
