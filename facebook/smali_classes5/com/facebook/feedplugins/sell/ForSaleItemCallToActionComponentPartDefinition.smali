.class public Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IC8;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599864
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IC8;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599860
    invoke-direct {p0, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 599861
    iput-object p1, p0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->e:LX/0Ot;

    .line 599862
    iput-object p2, p0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->f:LX/0Ot;

    .line 599863
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 599843
    iget-object v0, p0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IC8;

    const/4 v1, 0x0

    .line 599844
    new-instance v2, LX/IC7;

    invoke-direct {v2, v0}, LX/IC7;-><init>(LX/IC8;)V

    .line 599845
    iget-object v3, v0, LX/IC8;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IC6;

    .line 599846
    if-nez v3, :cond_0

    .line 599847
    new-instance v3, LX/IC6;

    invoke-direct {v3, v0}, LX/IC6;-><init>(LX/IC8;)V

    .line 599848
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/IC6;->a$redex0(LX/IC6;LX/1De;IILX/IC7;)V

    .line 599849
    move-object v2, v3

    .line 599850
    move-object v1, v2

    .line 599851
    move-object v0, v1

    .line 599852
    iget-object v1, v0, LX/IC6;->a:LX/IC7;

    iput-object p2, v1, LX/IC7;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599853
    iget-object v1, v0, LX/IC6;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 599854
    move-object v0, v0

    .line 599855
    iget-object v1, v0, LX/IC6;->a:LX/IC7;

    iput-object p3, v1, LX/IC7;->b:LX/1Pf;

    .line 599856
    iget-object v1, v0, LX/IC6;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 599857
    move-object v0, v0

    .line 599858
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 599859
    iget-object v0, p0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v0, p1, p3, v2, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 599832
    const-class v1, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 599833
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599834
    sput-object v2, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599835
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599836
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599837
    new-instance v4, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;

    const/16 v3, 0x2167

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x6fd

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v5, p0, v3}, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;-><init>(LX/0Ot;LX/0Ot;Landroid/content/Context;)V

    .line 599838
    move-object v0, v4

    .line 599839
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599840
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599841
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 599820
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 599821
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 599822
    invoke-static {v0}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 599823
    if-nez v0, :cond_0

    move v0, v1

    .line 599824
    :goto_0
    return v0

    :cond_0
    const v2, -0x3625f733

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    if-nez v2, :cond_1

    const v2, -0x4bb9a0ed

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 599831
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 599830
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599828
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599829
    invoke-static {p1}, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 599826
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 599827
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 599825
    sget-object v0, Lcom/facebook/feedplugins/sell/ForSaleItemCallToActionComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
