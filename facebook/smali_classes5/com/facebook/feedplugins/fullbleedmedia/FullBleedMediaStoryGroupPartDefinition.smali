.class public Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 582444
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 582445
    iput-object p1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    .line 582446
    iput-object p2, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    .line 582447
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;
    .locals 5

    .prologue
    .line 582427
    const-class v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    monitor-enter v1

    .line 582428
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 582429
    sput-object v2, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 582430
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582431
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 582432
    new-instance p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;)V

    .line 582433
    move-object v0, p0

    .line 582434
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 582435
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 582436
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 582437
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 582440
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 582441
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 582442
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedMediaStoryGroupPartDefinition;->b:Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    new-instance v1, LX/C4b;

    invoke-direct {v1, p2, p3, p3}, LX/C4b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1Pn;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 582443
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 582438
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 582439
    invoke-static {p1}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
