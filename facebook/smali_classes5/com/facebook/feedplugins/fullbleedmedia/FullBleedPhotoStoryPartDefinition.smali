.class public Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/C4V;

.field private final f:LX/C4X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593509
    new-instance v0, LX/3XZ;

    invoke-direct {v0}, LX/3XZ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/C4V;LX/C4X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593505
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 593506
    iput-object p2, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->e:LX/C4V;

    .line 593507
    iput-object p3, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->f:LX/C4X;

    .line 593508
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 593504
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->e:LX/C4V;

    invoke-virtual {v0, p1}, LX/C4V;->c(LX/1De;)LX/C4T;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/C4T;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C4T;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->f:LX/C4X;

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, p2, v3, v0}, LX/C4X;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, LX/C4T;->h(I)LX/C4T;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/C4T;->a(LX/1Pb;)LX/C4T;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;
    .locals 6

    .prologue
    .line 593484
    const-class v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;

    monitor-enter v1

    .line 593485
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593486
    sput-object v2, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593489
    new-instance p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C4V;->a(LX/0QB;)LX/C4V;

    move-result-object v4

    check-cast v4, LX/C4V;

    invoke-static {v0}, LX/C4X;->b(LX/0QB;)LX/C4X;

    move-result-object v5

    check-cast v5, LX/C4X;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;-><init>(Landroid/content/Context;LX/C4V;LX/C4X;)V

    .line 593490
    move-object v0, p0

    .line 593491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 593503
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 593502
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 593498
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 593499
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593500
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 593501
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 593496
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593497
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 593495
    sget-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
