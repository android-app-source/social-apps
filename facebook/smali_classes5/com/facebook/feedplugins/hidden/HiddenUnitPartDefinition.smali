.class public Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/HideableUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/Void;",
        "LX/1Po;",
        "LX/C8R;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static k:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Landroid/support/v4/app/FragmentActivity;

.field private final d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final e:LX/0Zb;

.field private final f:LX/17Q;

.field private final g:LX/0ad;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0qn;

.field public final j:LX/9yI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588484
    new-instance v0, LX/3Vk;

    invoke-direct {v0}, LX/3Vk;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/support/v4/app/FragmentActivity;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/17Q;LX/0ad;LX/0Ot;LX/0qn;LX/9yI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Landroid/support/v4/app/FragmentActivity;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0qn;",
            "LX/9yI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588473
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588474
    iput-object p1, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 588475
    iput-object p2, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->c:Landroid/support/v4/app/FragmentActivity;

    .line 588476
    iput-object p3, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 588477
    iput-object p4, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->e:LX/0Zb;

    .line 588478
    iput-object p5, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->f:LX/17Q;

    .line 588479
    iput-object p6, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->g:LX/0ad;

    .line 588480
    iput-object p7, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->h:LX/0Ot;

    .line 588481
    iput-object p8, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->i:LX/0qn;

    .line 588482
    iput-object p9, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->j:LX/9yI;

    .line 588483
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;
    .locals 13

    .prologue
    .line 588462
    const-class v1, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;

    monitor-enter v1

    .line 588463
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588464
    sput-object v2, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588465
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588466
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588467
    new-instance v3, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v6

    check-cast v6, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x259

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v11

    check-cast v11, LX/0qn;

    invoke-static {v0}, LX/9yI;->b(LX/0QB;)LX/9yI;

    move-result-object v12

    check-cast v12, LX/9yI;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/support/v4/app/FragmentActivity;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/17Q;LX/0ad;LX/0Ot;LX/0qn;LX/9yI;)V

    .line 588468
    move-object v0, v3

    .line 588469
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588470
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588471
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/HideableUnit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 588459
    instance-of v0, p0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-nez v0, :cond_0

    .line 588460
    const/4 v0, 0x0

    .line 588461
    :goto_0
    return-object v0

    :cond_0
    check-cast p0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {p0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/C8R;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;",
            "LX/1Po;",
            "LX/C8R;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    .line 588416
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588417
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 588418
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v1, v2, :cond_0

    .line 588419
    invoke-virtual {p3, v3}, LX/C8R;->setContentVisibility(I)V

    .line 588420
    :goto_0
    return-void

    .line 588421
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, LX/C8R;->setContentVisibility(I)V

    .line 588422
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v1, v2, :cond_2

    .line 588423
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v1

    invoke-virtual {p3}, LX/C8R;->getHeight()I

    move-result v2

    .line 588424
    iget-object v4, p3, LX/C8R;->a:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 588425
    iget-object v4, p3, LX/C8R;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v1, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 588426
    new-instance v4, LX/AhY;

    iget-object v5, p3, LX/C8R;->a:Landroid/view/View;

    invoke-direct {v4, v5, v1, v2}, LX/AhY;-><init>(Landroid/view/View;II)V

    iput-object v4, p3, LX/C8R;->f:LX/AhY;

    .line 588427
    iget-object v4, p3, LX/C8R;->f:LX/AhY;

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, LX/AhY;->setDuration(J)V

    .line 588428
    iget-object v4, p3, LX/C8R;->f:LX/AhY;

    invoke-virtual {p3, v4}, LX/C8R;->startAnimation(Landroid/view/animation/Animation;)V

    .line 588429
    :goto_1
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;LX/1PT;)LX/0wD;

    move-result-object v1

    const/4 p1, 0x0

    const/16 v7, 0x8

    .line 588430
    iget-object v2, p3, LX/C8R;->b:Landroid/widget/TextView;

    move-object v3, v2

    .line 588431
    iget-object v2, p3, LX/C8R;->c:Landroid/widget/TextView;

    move-object v4, v2

    .line 588432
    iget-object v2, p3, LX/C8R;->d:Landroid/view/View;

    move-object v5, v2

    .line 588433
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588434
    const v2, 0x7f08104a

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 588435
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588436
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 588437
    instance-of v2, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-nez v2, :cond_3

    .line 588438
    invoke-virtual {p3, v7}, LX/C8R;->setContentVisibility(I)V

    .line 588439
    :cond_1
    :goto_2
    goto :goto_0

    .line 588440
    :cond_2
    iget-object v1, p3, LX/C8R;->a:Landroid/view/View;

    move-object v1, v1

    .line 588441
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move-object v2, v0

    .line 588442
    check-cast v2, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v2}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->r()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    .line 588443
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v2, v6, :cond_4

    sget-object v6, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    if-ne v1, v6, :cond_4

    .line 588444
    invoke-virtual {p3, v7}, LX/C8R;->setContentVisibility(I)V

    .line 588445
    :cond_4
    if-eqz v2, :cond_8

    sget-object v6, LX/0wD;->TIMELINE_SELF:LX/0wD;

    if-ne v1, v6, :cond_8

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-eq v2, v6, :cond_5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v2, v6, :cond_8

    .line 588446
    :cond_5
    const v2, 0x7f081049

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(I)V

    .line 588447
    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588448
    invoke-virtual {v5, p1}, Landroid/view/View;->setVisibility(I)V

    .line 588449
    const v2, 0x7f08104b

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 588450
    :goto_3
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    .line 588451
    instance-of v4, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v4, :cond_9

    .line 588452
    :cond_6
    :goto_4
    move v2, v2

    .line 588453
    if-eqz v2, :cond_1

    .line 588454
    :cond_7
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 588455
    :cond_8
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588456
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 588457
    :cond_9
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588458
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->i:LX/0qn;

    invoke-virtual {v4, v0}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-nez v4, :cond_b

    :cond_a
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v4

    if-nez v4, :cond_c

    :cond_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v4

    if-nez v4, :cond_c

    sget-object v4, LX/0wD;->TIMELINE_SELF:LX/0wD;

    if-ne v1, v4, :cond_6

    :cond_c
    const/4 v2, 0x1

    goto :goto_4
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;LX/1PT;)LX/0wD;
    .locals 4

    .prologue
    .line 588404
    sget-object v0, LX/C8Q;->a:[I

    invoke-interface {p1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 588405
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "Unknown FeedListType"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FeedLitType was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v3}, LX/1Qt;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 588406
    sget-object v0, LX/0wD;->UNKNOWN:LX/0wD;

    :goto_0
    return-object v0

    .line 588407
    :pswitch_0
    sget-object v0, LX/0wD;->NEWSFEED:LX/0wD;

    goto :goto_0

    .line 588408
    :pswitch_1
    sget-object v0, LX/0wD;->PERMALINK:LX/0wD;

    goto :goto_0

    .line 588409
    :pswitch_2
    sget-object v0, LX/0wD;->GROUP:LX/0wD;

    goto :goto_0

    .line 588410
    :pswitch_3
    sget-object v0, LX/0wD;->EVENT:LX/0wD;

    goto :goto_0

    .line 588411
    :pswitch_4
    sget-object v0, LX/0wD;->TIMELINE_SELF:LX/0wD;

    goto :goto_0

    .line 588412
    :pswitch_5
    sget-object v0, LX/0wD;->THROWBACK:LX/0wD;

    goto :goto_0

    .line 588413
    :pswitch_6
    sget-object v0, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    goto :goto_0

    .line 588414
    :pswitch_7
    sget-object v0, LX/0wD;->PAGE_TIMELINE:LX/0wD;

    goto :goto_0

    .line 588415
    :pswitch_8
    sget-object v0, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588403
    sget-object v0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 588393
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 588394
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/C8P;

    invoke-direct {v1, p0, p2, p3}, LX/C8P;-><init>(Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588395
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x74b83bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588402
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    check-cast p4, LX/C8R;

    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/C8R;)V

    const/16 v1, 0x1f

    const v2, 0xe921590

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588401
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 588396
    check-cast p4, LX/C8R;

    .line 588397
    iget-object p0, p4, LX/C8R;->f:LX/AhY;

    if-eqz p0, :cond_0

    iget-object p0, p4, LX/C8R;->f:LX/AhY;

    invoke-virtual {p0}, LX/AhY;->hasEnded()Z

    move-result p0

    if-nez p0, :cond_0

    .line 588398
    iget-object p0, p4, LX/C8R;->f:LX/AhY;

    invoke-virtual {p0}, LX/AhY;->cancel()V

    .line 588399
    const/4 p0, 0x0

    iput-object p0, p4, LX/C8R;->f:LX/AhY;

    .line 588400
    :cond_0
    return-void
.end method
