.class public Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
        "E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "LX/C8M;",
        "TE;",
        "LX/C8O;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field public final c:Landroid/support/v4/app/FragmentActivity;

.field public final d:LX/0bH;

.field public final e:LX/1du;

.field public final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final g:LX/03V;

.field public final h:LX/9yI;

.field private final i:LX/0fO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588487
    const-class v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->b:Ljava/lang/String;

    .line 588488
    new-instance v0, LX/3Vl;

    invoke-direct {v0}, LX/3Vl;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/0bH;LX/1du;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/9yI;LX/0fO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588596
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588597
    iput-object p1, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->c:Landroid/support/v4/app/FragmentActivity;

    .line 588598
    iput-object p2, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->d:LX/0bH;

    .line 588599
    iput-object p3, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    .line 588600
    iput-object p4, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 588601
    iput-object p5, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->g:LX/03V;

    .line 588602
    iput-object p6, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->h:LX/9yI;

    .line 588603
    iput-object p7, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->i:LX/0fO;

    .line 588604
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;
    .locals 11

    .prologue
    .line 588585
    const-class v1, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    monitor-enter v1

    .line 588586
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588587
    sput-object v2, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588588
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588589
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588590
    new-instance v3, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    invoke-static {v0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/1du;->a(LX/0QB;)LX/1du;

    move-result-object v6

    check-cast v6, LX/1du;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/9yI;->b(LX/0QB;)LX/9yI;

    move-result-object v9

    check-cast v9, LX/9yI;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v10

    check-cast v10, LX/0fO;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;-><init>(Landroid/support/v4/app/FragmentActivity;LX/0bH;LX/1du;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/9yI;LX/0fO;)V

    .line 588591
    move-object v0, v3

    .line 588592
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588593
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588594
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/C8O;Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C8O;",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
            "LX/C8M;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 588544
    invoke-virtual {p1}, LX/C8O;->i()V

    move v8, v9

    .line 588545
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 588546
    invoke-virtual {p2}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;

    .line 588547
    invoke-virtual {v7}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;->j()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    .line 588548
    new-instance v0, LX/C8C;

    invoke-virtual {p1}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/C8C;-><init>(Landroid/content/Context;)V

    .line 588549
    iget-object v1, p1, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 588550
    move-object v2, v0

    .line 588551
    invoke-virtual {v7}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 588552
    iget-object v1, v2, LX/C8C;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588553
    move-object v10, v2

    .line 588554
    new-instance v0, LX/C8H;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/C8H;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8C;LX/C8O;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V

    invoke-virtual {v10, v0}, LX/C8C;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588555
    invoke-virtual {p1}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 588556
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 588557
    invoke-virtual {v7}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;->j()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v3

    .line 588558
    sget-object v5, LX/C8E;->b:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    .line 588559
    invoke-virtual {v7}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;->k()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v3

    .line 588560
    sget-object v5, LX/C8E;->c:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_1

    .line 588561
    const/4 v1, 0x0

    :goto_1
    move-object v0, v1

    .line 588562
    iget-object v1, v2, LX/C8C;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 588563
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    iget-object v1, p3, LX/C8M;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/1du;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z

    move-result v0

    invoke-virtual {v2, v0}, LX/C8C;->a(Z)LX/C8C;

    .line 588564
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0

    .line 588565
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 588566
    invoke-virtual {p1}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080032

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 588567
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 588568
    new-instance v1, LX/C8F;

    invoke-direct {v1, p0, p1, p4, p3}, LX/C8F;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V

    .line 588569
    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x12

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 588570
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v3, " "

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 588571
    iget-object v2, p1, LX/C8O;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588572
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    iget-object v1, p3, LX/C8M;->a:Ljava/lang/String;

    .line 588573
    iget-object v2, v0, LX/1du;->h:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    move v0, v2

    .line 588574
    if-nez v0, :cond_1

    const/4 v9, 0x1

    :cond_1
    invoke-virtual {p1, v9}, LX/C8O;->setEnabled(Z)V

    .line 588575
    return-void

    .line 588576
    :pswitch_0
    const v3, 0x7f020a9e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 588577
    :pswitch_1
    const v3, 0x7f020a9c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 588578
    :pswitch_2
    const v3, 0x7f020a9d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 588579
    :pswitch_3
    const v3, 0x7f020a9f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    .line 588580
    :pswitch_4
    const v3, 0x7f020a9a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    .line 588581
    :pswitch_5
    const v3, 0x7f020a98

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    .line 588582
    :pswitch_6
    const v3, 0x7f020a9b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    .line 588583
    :pswitch_7
    const v3, 0x7f020a97

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    .line 588584
    :pswitch_8
    const v3, 0x7f020a96

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 588541
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->i:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 588542
    :goto_0
    return-void

    .line 588543
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8M;)I
    .locals 3

    .prologue
    .line 588534
    iget-object v0, p1, LX/C8M;->b:LX/1Va;

    .line 588535
    iget v1, v0, LX/1Va;->a:I

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 588536
    if-nez v0, :cond_0

    .line 588537
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->g:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->b:Ljava/lang/String;

    const-string v2, "Unit pre-hidden height not set before hiding"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 588538
    :cond_0
    iget-object v0, p1, LX/C8M;->b:LX/1Va;

    .line 588539
    iget v1, v0, LX/1Va;->a:I

    move v0, v1

    .line 588540
    return v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C8O;",
            "LX/C8M;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 588522
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    iget-object v2, p2, LX/C8M;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/1du;->b(Ljava/lang/String;)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    move-result-object v0

    .line 588523
    if-nez v0, :cond_1

    .line 588524
    :cond_0
    :goto_0
    return-void

    .line 588525
    :cond_1
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a(LX/C8O;Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 588526
    iget-object v2, p1, LX/C8O;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 588527
    if-eqz v2, :cond_3

    .line 588528
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 588529
    iget-object v3, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    iget-object v4, p2, LX/C8M;->a:Ljava/lang/String;

    const-class v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v2, v0, v1, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v3, v4, v0}, LX/1du;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_0

    .line 588530
    invoke-static {p0, p2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8M;)I

    move-result v0

    invoke-virtual {p1}, LX/C8O;->g()I

    move-result v1

    .line 588531
    iget-object v2, p1, LX/C8O;->g:LX/C8D;

    iget-object v3, p1, LX/C8O;->f:Landroid/view/View;

    new-instance v4, LX/C8N;

    invoke-direct {v4, p1, v0, v1}, LX/C8N;-><init>(LX/C8O;II)V

    invoke-virtual {v2, v3, v4}, LX/C8D;->a(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    .line 588532
    goto :goto_0

    :cond_2
    move v0, v1

    .line 588533
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 588520
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, LX/C8G;

    invoke-direct {v2, p0}, LX/C8G;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 588521
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/graphql/enums/StoryVisibility;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C8M;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/StoryVisibility;",
            "TT;",
            "LX/C8M;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 588517
    iget-object v6, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->d:LX/0bH;

    new-instance v0, LX/1Nd;

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p3}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8M;)I

    move-result v5

    move-object v3, v2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 588518
    iget-object v0, p0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->d:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 588519
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588516
    sget-object v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 588511
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 588512
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588513
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 588514
    check-cast p3, LX/1Pr;

    new-instance v1, LX/1VZ;

    invoke-direct {v1, v0}, LX/1VZ;-><init>(Lcom/facebook/graphql/model/HideableUnit;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Va;

    .line 588515
    new-instance v2, LX/C8M;

    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, LX/C8M;-><init>(Ljava/lang/String;LX/1Va;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x24a75d01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588496
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C8M;

    check-cast p4, LX/C8O;

    const/4 p3, 0x0

    .line 588497
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 588498
    check-cast v1, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 588499
    sget-object v2, LX/C8E;->a:[I

    invoke-interface {v1}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/StoryVisibility;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 588500
    invoke-virtual {p4, p3}, LX/C8O;->setContentVisibility(I)V

    .line 588501
    invoke-static {p0, p4}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/view/View;)V

    .line 588502
    invoke-static {p0, p4, p2, p1}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 588503
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x5b865b07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 588504
    :pswitch_0
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/C8O;->setContentVisibility(I)V

    goto :goto_0

    .line 588505
    :pswitch_1
    invoke-virtual {p4, p3}, LX/C8O;->setContentVisibility(I)V

    .line 588506
    invoke-static {p0, p4}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/view/View;)V

    .line 588507
    new-instance v2, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition$2;

    invoke-direct {v2, p0, v1, p2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition$2;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C8M;)V

    invoke-virtual {p4, v2}, LX/C8O;->post(Ljava/lang/Runnable;)Z

    .line 588508
    invoke-static {p0, p2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8M;)I

    move-result v1

    invoke-virtual {p4, v1}, LX/C8O;->setHeight(I)V

    .line 588509
    invoke-virtual {p4}, LX/C8O;->a()V

    .line 588510
    invoke-virtual {p4}, LX/C8O;->i()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588495
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 588489
    check-cast p4, LX/C8O;

    .line 588490
    iget-object p0, p4, LX/C8O;->g:LX/C8D;

    .line 588491
    iget-object p4, p0, LX/C8D;->a:Landroid/view/animation/Animation;

    if-eqz p4, :cond_0

    iget-object p4, p0, LX/C8D;->a:Landroid/view/animation/Animation;

    invoke-virtual {p4}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result p4

    if-nez p4, :cond_0

    .line 588492
    iget-object p4, p0, LX/C8D;->a:Landroid/view/animation/Animation;

    invoke-virtual {p4}, Landroid/view/animation/Animation;->cancel()V

    .line 588493
    const/4 p4, 0x0

    iput-object p4, p0, LX/C8D;->a:Landroid/view/animation/Animation;

    .line 588494
    :cond_0
    return-void
.end method
