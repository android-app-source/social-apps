.class public Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/C3y;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final e:LX/1VF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593381
    new-instance v0, LX/3XU;

    invoke-direct {v0}, LX/3XU;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 593406
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 593407
    iput-object p1, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 593408
    iput-object p2, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 593409
    iput-object p3, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->c:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    .line 593410
    iput-object p4, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->e:LX/1VF;

    .line 593411
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;
    .locals 7

    .prologue
    .line 593395
    const-class v1, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    monitor-enter v1

    .line 593396
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 593397
    sput-object v2, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 593398
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593399
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 593400
    new-instance p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v6

    check-cast v6, LX/1VF;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;LX/1VF;)V

    .line 593401
    move-object v0, p0

    .line 593402
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 593403
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 593404
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 593405
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 593412
    sget-object v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 593384
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pk;

    .line 593385
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 593386
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 593387
    iget-object v1, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->c:Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593388
    iget-object v1, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 593389
    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v1

    .line 593390
    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 593391
    :goto_0
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    if-eqz v0, :cond_1

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    :goto_1
    invoke-direct {v3, p2, v0}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 593392
    const/4 v0, 0x0

    return-object v0

    .line 593393
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 593394
    :cond_1
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 593382
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 593383
    invoke-static {p1}, LX/1VF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
