.class public Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:LX/17W;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/Ezy;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final g:LX/9Tk;

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595408
    const v0, 0x7f030643

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/Ezy;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9Tk;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595409
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595410
    iput-object p1, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 595411
    iput-object p2, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->c:LX/17W;

    .line 595412
    iput-object p3, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 595413
    iput-object p4, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->e:LX/Ezy;

    .line 595414
    iput-object p5, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 595415
    iput-object p6, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->g:LX/9Tk;

    .line 595416
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->h:Ljava/lang/String;

    .line 595417
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;
    .locals 10

    .prologue
    .line 595418
    const-class v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    monitor-enter v1

    .line 595419
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595420
    sput-object v2, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595421
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595422
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595423
    new-instance v3, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Ezy;->a(LX/0QB;)LX/Ezy;

    move-result-object v7

    check-cast v7, LX/Ezy;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v9

    check-cast v9, LX/9Tk;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/Ezy;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/9Tk;)V

    .line 595424
    move-object v0, v3

    .line 595425
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595426
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595427
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595429
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 595430
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    check-cast p3, LX/1Ps;

    .line 595431
    const v0, 0x7f0d1157

    iget-object v1, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 595432
    new-instance v2, LX/JOQ;

    invoke-direct {v2, p0}, LX/JOQ;-><init>(Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;)V

    move-object v2, v2

    .line 595433
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 595434
    new-instance v1, LX/JOR;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/JOR;-><init>(Ljava/lang/String;Z)V

    move-object v0, p3

    .line 595435
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 595436
    iget-object v0, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->g:LX/9Tk;

    iget-object v2, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->h:Ljava/lang/String;

    const-string v3, "find_friends"

    .line 595437
    iget-object p0, v0, LX/9Tk;->a:LX/0Zb;

    sget-object p1, LX/9Tj;->FIND_FRIENDS_SHOWN:LX/9Tj;

    invoke-virtual {p1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "empty_feed_uuid"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "unit"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 595438
    check-cast p3, LX/1Pr;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 595439
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595440
    const/4 v0, 0x1

    return v0
.end method
