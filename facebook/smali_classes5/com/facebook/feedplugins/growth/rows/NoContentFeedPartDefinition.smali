.class public Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNoContentFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595441
    new-instance v0, LX/3Y8;

    invoke-direct {v0}, LX/3Y8;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595442
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 595443
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;
    .locals 3

    .prologue
    .line 595444
    const-class v1, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;

    monitor-enter v1

    .line 595445
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595446
    sput-object v2, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595447
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595448
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 595449
    new-instance v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;-><init>()V

    .line 595450
    move-object v0, v0

    .line 595451
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595452
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595453
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 595455
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/NoContentFeedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 595456
    const/4 v0, 0x1

    return v0
.end method
