.class public Lcom/facebook/permalink/rows/SeenByPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/CZj;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final f:LX/14w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588248
    new-instance v0, LX/3Vi;

    invoke-direct {v0}, LX/3Vi;-><init>()V

    sput-object v0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588241
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588242
    iput-object p1, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588243
    iput-object p2, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 588244
    iput-object p3, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 588245
    iput-object p4, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 588246
    iput-object p5, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->f:LX/14w;

    .line 588247
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/SeenByPartDefinition;
    .locals 9

    .prologue
    .line 588230
    const-class v1, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    monitor-enter v1

    .line 588231
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588232
    sput-object v2, Lcom/facebook/permalink/rows/SeenByPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588233
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588234
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588235
    new-instance v3, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v8

    check-cast v8, LX/14w;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/permalink/rows/SeenByPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/14w;)V

    .line 588236
    move-object v0, v3

    .line 588237
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588238
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/SeenByPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588239
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588240
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588206
    sget-object v0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 588222
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 588223
    iget-object v0, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588224
    iget-object v1, p0, Lcom/facebook/permalink/rows/SeenByPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 588225
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588226
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588227
    new-instance v2, LX/CZi;

    invoke-direct {v2, p0, v0, p3}, LX/CZi;-><init>(Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V

    move-object v0, v2

    .line 588228
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588229
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3e32fe03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588213
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/CZj;

    .line 588214
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 588215
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588216
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    :cond_0
    move-object v1, v1

    .line 588217
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result v1

    .line 588218
    invoke-virtual {p4}, LX/CZj;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0810d7

    const p2, 0x7f0810d8

    invoke-static {v2, p0, p2, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v1

    .line 588219
    invoke-virtual {p4, v1}, LX/CZj;->setText(Ljava/lang/CharSequence;)V

    .line 588220
    const v1, 0x7f0d008e

    invoke-virtual {p4, v1}, LX/CZj;->setId(I)V

    .line 588221
    const/16 v1, 0x1f

    const v2, -0x18e6b0bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 588208
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588209
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588210
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588211
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 588212
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 588207
    sget-object v0, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    return-object v0
.end method
