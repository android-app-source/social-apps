.class public Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "LX/1Ps;",
        "LX/CZf;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final f:LX/1Uf;

.field public final g:LX/14w;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1za;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588077
    new-instance v0, LX/3Vg;

    invoke-direct {v0}, LX/3Vg;-><init>()V

    sput-object v0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/1za;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/1Uf;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/14w;",
            "LX/1za;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588067
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588068
    iput-object p1, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->b:Landroid/content/Context;

    .line 588069
    iput-object p2, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588070
    iput-object p3, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 588071
    iput-object p4, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 588072
    iput-object p5, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->f:LX/1Uf;

    .line 588073
    iput-object p7, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->g:LX/14w;

    .line 588074
    iput-object p6, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->h:LX/0Ot;

    .line 588075
    iput-object p8, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->i:LX/1za;

    .line 588076
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;
    .locals 12

    .prologue
    .line 588056
    const-class v1, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    monitor-enter v1

    .line 588057
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588058
    sput-object v2, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588059
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588060
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588061
    new-instance v3, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v8

    check-cast v8, LX/1Uf;

    const/16 v9, 0x97

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v10

    check-cast v10, LX/14w;

    invoke-static {v0}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v11

    check-cast v11, LX/1za;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/1za;)V

    .line 588062
    move-object v0, v3

    .line 588063
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588064
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588065
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588055
    sget-object v0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 588050
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588051
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588052
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588053
    iget-object v1, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588054
    new-instance v1, LX/CZe;

    invoke-direct {v1, p0, v0}, LX/CZe;-><init>(Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6ffb75d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588023
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/CZf;

    .line 588024
    invoke-virtual {p4}, LX/CZf;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f01029c

    const v4, 0x7f0a0158

    invoke-static {v1, v2, v4}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    .line 588025
    invoke-virtual {p4}, LX/CZf;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p4, v1}, LX/CZf;->setTextColor(I)V

    .line 588026
    invoke-static {p1}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 588027
    :goto_0
    move-object v1, p1

    .line 588028
    const/4 v5, 0x0

    .line 588029
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 588030
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 588031
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588032
    invoke-virtual {p4, v5}, LX/CZf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588033
    invoke-virtual {p4, v5}, LX/CZf;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 588034
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    .line 588035
    if-eqz p3, :cond_3

    invoke-static {p3}, LX/153;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 588036
    if-eqz v4, :cond_2

    .line 588037
    iget-object v4, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->g:LX/14w;

    invoke-virtual {v4, v2}, LX/14w;->l(Lcom/facebook/graphql/model/GraphQLStory;)LX/0am;

    move-result-object v4

    .line 588038
    :goto_2
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 588039
    iget-object p3, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->f:LX/1Uf;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v4}, LX/1eD;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    move-result-object v4

    const/4 p1, 0x1

    invoke-virtual {p3, v4, p1, v6}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v4

    .line 588040
    invoke-virtual {p4, p2}, LX/CZf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588041
    invoke-virtual {p4, v5}, LX/CZf;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 588042
    invoke-virtual {p4, v4}, LX/CZf;->setText(Ljava/lang/CharSequence;)V

    .line 588043
    iget-object v4, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->i:LX/1za;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v4, v2, p4}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;)V

    .line 588044
    :goto_3
    const/16 v1, 0x1f

    const v2, -0x48aacf70

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 588045
    :cond_0
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 588046
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 588047
    :cond_1
    iget-object v2, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f080fc6

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 588048
    invoke-virtual {p4, v2}, LX/CZf;->setText(Ljava/lang/CharSequence;)V

    .line 588049
    invoke-static {p4}, LX/1za;->a(Landroid/widget/TextView;)V

    goto :goto_3

    :cond_2
    move-object v4, v5

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 588015
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588016
    iget-object v1, p0, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->g:LX/14w;

    .line 588017
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588018
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/14w;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 588022
    sget-object v0, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 588019
    check-cast p4, LX/CZf;

    .line 588020
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/CZf;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588021
    return-void
.end method
