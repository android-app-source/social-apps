.class public Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "LX/1Ps;",
        "LX/CZh;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static n:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final f:LX/1Uf;

.field public final g:LX/14w;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0ad;

.field public final j:LX/1zf;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/CZh;

.field public m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 588203
    new-instance v0, LX/3Vh;

    invoke-direct {v0}, LX/3Vh;-><init>()V

    sput-object v0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/0ad;LX/1zf;LX/0Or;)V
    .locals 0
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/1Uf;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/1zf;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 588191
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 588192
    iput-object p1, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->b:Landroid/content/Context;

    .line 588193
    iput-object p2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 588194
    iput-object p3, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 588195
    iput-object p4, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 588196
    iput-object p5, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->f:LX/1Uf;

    .line 588197
    iput-object p7, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->g:LX/14w;

    .line 588198
    iput-object p6, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->h:LX/0Ot;

    .line 588199
    iput-object p8, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->i:LX/0ad;

    .line 588200
    iput-object p9, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->j:LX/1zf;

    .line 588201
    iput-object p10, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->k:LX/0Or;

    .line 588202
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;
    .locals 14

    .prologue
    .line 588180
    const-class v1, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    monitor-enter v1

    .line 588181
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 588182
    sput-object v2, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 588183
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588184
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 588185
    new-instance v3, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v7

    check-cast v7, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v8

    check-cast v8, LX/1Uf;

    const/16 v9, 0x97

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v10

    check-cast v10, LX/14w;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v12

    check-cast v12, LX/1zf;

    const/16 v13, 0x15e8

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/0ad;LX/1zf;LX/0Or;)V

    .line 588186
    move-object v0, v3

    .line 588187
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 588188
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588189
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 588190
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588176
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 588177
    :goto_0
    return-object p0

    .line 588178
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 588175
    sget-object v0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 588080
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 588081
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588082
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588083
    iget-object v1, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 588084
    new-instance v1, LX/CZg;

    invoke-direct {v1, p0, v0}, LX/CZg;-><init>(Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7bfd2f94

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 588110
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, LX/CZh;

    .line 588111
    iput-object p4, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    .line 588112
    invoke-static {p1}, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 588113
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588114
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588115
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 588116
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 588117
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 588118
    iget-object v5, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->g:LX/14w;

    invoke-virtual {v5, v2}, LX/14w;->l(Lcom/facebook/graphql/model/GraphQLStory;)LX/0am;

    move-result-object v2

    .line 588119
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 588120
    iget-object v5, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->f:LX/1Uf;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2}, LX/1eD;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    move-result-object v2

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v6, v4}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    .line 588121
    invoke-virtual {p4, p2}, LX/CZh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588122
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->m:Ljava/lang/String;

    .line 588123
    :goto_0
    iget-object v1, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    iget-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->i:LX/0ad;

    sget v4, LX/227;->e:I

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, LX/0ad;->a(II)I

    move-result v2

    .line 588124
    iput v2, v1, LX/CZh;->e:I

    .line 588125
    iget-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    .line 588126
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 588127
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v1

    .line 588128
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 588129
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    .line 588130
    iget-object p2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->j:LX/1zf;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p4

    invoke-virtual {p2, p4}, LX/1zf;->a(I)LX/1zt;

    move-result-object p2

    .line 588131
    invoke-virtual {p2}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 588132
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 588133
    new-instance p4, LX/6UY;

    invoke-direct {p4, v4, p2}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 588134
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 588135
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v1, v4

    .line 588136
    iget-object v4, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->m:Ljava/lang/String;

    .line 588137
    invoke-virtual {v2}, LX/CZh;->a()V

    .line 588138
    const/4 p4, 0x0

    const/4 v7, 0x0

    .line 588139
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 588140
    :cond_1
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 588141
    :goto_2
    move-object v5, v5

    .line 588142
    iput-object v5, v2, LX/CZh;->f:LX/0Px;

    .line 588143
    const/4 v7, 0x0

    .line 588144
    iget-object v5, v2, LX/CZh;->f:LX/0Px;

    if-eqz v5, :cond_2

    iget-object v5, v2, LX/CZh;->f:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    iget v5, v2, LX/CZh;->g:I

    if-gtz v5, :cond_8

    .line 588145
    :cond_2
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 588146
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v1

    .line 588147
    iget-object v4, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    if-lez v1, :cond_9

    const/4 v2, 0x0

    .line 588148
    :goto_3
    iget-object p0, v4, LX/CZh;->c:Landroid/view/View;

    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 588149
    const/16 v1, 0x1f

    const v2, -0xb410f2f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 588150
    :cond_3
    iget-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f080fc6

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 588151
    :cond_4
    new-instance p3, LX/0Pz;

    invoke-direct {p3}, LX/0Pz;-><init>()V

    .line 588152
    invoke-static {}, LX/9Iy;->newBuilder()LX/9Iw;

    move-result-object v5

    move v6, v7

    move v8, v7

    move v9, v7

    move-object v10, v5

    .line 588153
    :goto_4
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_7

    .line 588154
    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6UY;

    .line 588155
    iget-object p2, v5, LX/6UY;->a:Landroid/net/Uri;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    iget-object v5, v5, LX/6UY;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, p2, v5, p4, p4}, LX/9Iw;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;)LX/9Iw;

    .line 588156
    iget v5, v2, LX/CZh;->g:I

    add-int/lit8 v5, v5, -0x1

    if-ne v9, v5, :cond_5

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v6, v5, :cond_5

    iget v5, v2, LX/CZh;->e:I

    add-int/lit8 v5, v5, -0x1

    if-ge v8, v5, :cond_5

    .line 588157
    invoke-virtual {v10}, LX/9Iw;->a()LX/9Iy;

    move-result-object v5

    invoke-virtual {p3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 588158
    add-int/lit8 v8, v8, 0x1

    .line 588159
    invoke-static {}, LX/9Iy;->newBuilder()LX/9Iw;

    move-result-object v10

    move v9, v7

    .line 588160
    :goto_5
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    .line 588161
    :cond_5
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v6, v5, :cond_6

    .line 588162
    iput-object v4, v10, LX/9Iw;->b:Ljava/lang/CharSequence;

    .line 588163
    invoke-virtual {v10}, LX/9Iw;->a()LX/9Iy;

    move-result-object v5

    invoke-virtual {p3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 588164
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 588165
    :cond_7
    invoke-virtual {p3}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    goto/16 :goto_2

    .line 588166
    :cond_8
    iget-object v5, v2, LX/CZh;->f:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    move v6, v7

    :goto_6
    if-ge v6, v8, :cond_2

    iget-object v5, v2, LX/CZh;->f:LX/0Px;

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9Iy;

    .line 588167
    new-instance v9, Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v2}, LX/CZh;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/facebook/fig/facepile/FigFacepileView;-><init>(Landroid/content/Context;)V

    .line 588168
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/facebook/fig/facepile/FigFacepileView;->setFaceSize(I)V

    .line 588169
    invoke-virtual {v9, v5}, Lcom/facebook/fig/facepile/FigFacepileView;->setModel(LX/9Iy;)V

    .line 588170
    move-object v5, v9

    .line 588171
    iget v9, v2, LX/CZh;->b:I

    invoke-virtual {v5, v7, v7, v7, v9}, Lcom/facebook/fig/facepile/FigFacepileView;->setPadding(IIII)V

    .line 588172
    iget-object v9, v2, LX/CZh;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 588173
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_6

    .line 588174
    :cond_9
    const/16 v2, 0x8

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 588091
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 588092
    invoke-static {p1}, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->g:LX/14w;

    .line 588093
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588094
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/14w;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588095
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588096
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 588097
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588098
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 588099
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588100
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->i:LX/0ad;

    sget-short v2, LX/227;->d:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588101
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 588102
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 588103
    iget-object v2, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->i:LX/0ad;

    sget-short p1, LX/227;->c:S

    invoke-interface {v2, p1, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 588104
    if-nez v2, :cond_1

    move v2, v3

    .line 588105
    :goto_0
    move v0, v2

    .line 588106
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 588107
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    iget-object p1, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->k:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 588108
    goto :goto_0

    :cond_2
    move v2, v4

    .line 588109
    goto :goto_0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 588090
    sget-object v0, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 588085
    check-cast p4, LX/CZh;

    const/4 v1, 0x0

    .line 588086
    iget-object v0, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    invoke-virtual {v0}, LX/CZh;->a()V

    .line 588087
    iput-object v1, p0, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->l:LX/CZh;

    .line 588088
    invoke-virtual {p4, v1}, LX/CZh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 588089
    return-void
.end method
