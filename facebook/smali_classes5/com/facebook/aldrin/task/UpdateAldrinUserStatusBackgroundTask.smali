.class public Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;
.super LX/1Eg;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0SG;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2YW;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ExecutorService;

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinEnabled;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/2YW;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573984
    const-class v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 573985
    iput-object p1, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->a:Landroid/content/Context;

    .line 573986
    iput-object p2, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->b:LX/0SG;

    .line 573987
    iput-object p3, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->c:LX/0Or;

    .line 573988
    iput-object p4, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 573989
    iput-object p5, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->e:LX/0Ot;

    .line 573990
    iput-object p6, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->f:Ljava/util/concurrent/ExecutorService;

    .line 573991
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;
    .locals 10

    .prologue
    .line 573944
    sget-object v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->h:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    if-nez v0, :cond_1

    .line 573945
    const-class v1, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    monitor-enter v1

    .line 573946
    :try_start_0
    sget-object v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->h:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573947
    if-eqz v2, :cond_0

    .line 573948
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 573949
    new-instance v3, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x1439

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x7b

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;-><init>(Landroid/content/Context;LX/0SG;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;Ljava/util/concurrent/ExecutorService;)V

    .line 573950
    move-object v0, v3

    .line 573951
    sput-object v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->h:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573952
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573953
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573954
    :cond_1
    sget-object v0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->h:Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    return-object v0

    .line 573955
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573956
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 3

    .prologue
    .line 573979
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 573980
    :goto_0
    monitor-exit p0

    return-void

    .line 573981
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2YW;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2YW;->a(Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 573982
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/2Yf;

    invoke-direct {v1, p0}, LX/2Yf;-><init>(Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;)V

    iget-object v2, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 573983
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()J
    .locals 10

    .prologue
    const-wide/32 v8, 0x6ddd00

    const-wide/16 v0, 0x0

    .line 573970
    iget-object v2, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 573971
    iget-object v4, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2YT;->a:LX/0Tn;

    invoke-interface {v4, v5, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 573972
    add-long v6, v4, v8

    cmp-long v6, v2, v6

    if-gez v6, :cond_1

    .line 573973
    add-long v0, v4, v8

    sub-long/2addr v0, v2

    .line 573974
    :cond_0
    :goto_0
    return-wide v0

    .line 573975
    :cond_1
    iget-object v4, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2YT;->b:LX/0Tn;

    invoke-interface {v4, v5, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 573976
    invoke-static {p0}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->m(Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;)J

    move-result-wide v6

    .line 573977
    add-long v8, v4, v6

    cmp-long v8, v2, v8

    if-gez v8, :cond_0

    .line 573978
    add-long v0, v4, v6

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static m(Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 573967
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2YT;->c:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 573968
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 573969
    const-wide/32 v2, 0x6ddd00

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final f()J
    .locals 4

    .prologue
    .line 573964
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 573965
    const-wide/16 v0, -0x1

    .line 573966
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->l()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573963
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 573959
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 573960
    :goto_0
    return v0

    .line 573961
    :cond_0
    invoke-direct {p0}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->l()J

    move-result-wide v2

    .line 573962
    iget-object v0, p0, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573957
    invoke-direct {p0}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->k()V

    .line 573958
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
