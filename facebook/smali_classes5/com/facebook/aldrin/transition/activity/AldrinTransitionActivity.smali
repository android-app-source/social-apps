.class public Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/GQW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/GQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 575544
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;LX/GQW;LX/GQh;)V
    .locals 0

    .prologue
    .line 575545
    iput-object p1, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    iput-object p2, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->q:LX/GQh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;

    invoke-static {v1}, LX/GQW;->a(LX/0QB;)LX/GQW;

    move-result-object v0

    check-cast v0, LX/GQW;

    invoke-static {v1}, LX/GQh;->a(LX/0QB;)LX/GQh;

    move-result-object v1

    check-cast v1, LX/GQh;

    invoke-static {p0, v0, v1}, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->a(Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;LX/GQW;LX/GQh;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 575546
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 575547
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->setContentView(I)V

    .line 575548
    invoke-static {p0, p0}, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 575549
    iget-object v0, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    invoke-virtual {v0}, LX/GQW;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 575550
    invoke-virtual {p0}, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->finish()V

    .line 575551
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 575552
    iget-object v0, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->q:LX/GQh;

    sget-object v1, LX/GQf;->GO_BACK:LX/GQf;

    invoke-virtual {v0, v1}, LX/GQh;->a(LX/GQf;)V

    .line 575553
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7fde3c14

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 575554
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 575555
    iget-object v1, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    const/4 v2, 0x0

    .line 575556
    iput-boolean v2, v1, LX/GQW;->e:Z

    .line 575557
    const/16 v1, 0x23

    const v2, 0x29873ce2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x4e8da74f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 575558
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 575559
    iget-object v1, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    invoke-virtual {v1}, LX/GQW;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 575560
    invoke-virtual {p0}, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->finish()V

    .line 575561
    :goto_0
    const v1, 0x7377954e

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 575562
    :cond_0
    iget-object v1, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    const/4 v2, 0x1

    .line 575563
    iput-boolean v2, v1, LX/GQW;->e:Z

    .line 575564
    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x35afa88

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 575565
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 575566
    iget-object v1, p0, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;->p:LX/GQW;

    const/4 v2, 0x0

    .line 575567
    iput-boolean v2, v1, LX/GQW;->e:Z

    .line 575568
    const/16 v1, 0x23

    const v2, -0x7f5134b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
