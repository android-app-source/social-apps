.class public Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/aldrin/status/AldrinUserStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 575524
    const-class v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    new-instance v1, Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;

    invoke-direct {v1}, Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 575525
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 575526
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/aldrin/status/AldrinUserStatus;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 575527
    if-nez p0, :cond_0

    .line 575528
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 575529
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 575530
    invoke-static {p0, p1, p2}, Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;->b(Lcom/facebook/aldrin/status/AldrinUserStatus;LX/0nX;LX/0my;)V

    .line 575531
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 575532
    return-void
.end method

.method private static b(Lcom/facebook/aldrin/status/AldrinUserStatus;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 575533
    const-string v0, "effective_region"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575534
    const-string v0, "current_region"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575535
    const-string v0, "tos_transition_type"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575536
    const-string v0, "tos_terms_url"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTermsUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 575537
    const-string v0, "tos_privacy_url"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosPrivacyUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 575538
    const-string v0, "tos_cookies_url"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosCookiesUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 575539
    const-string v0, "tos_version"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 575540
    const-string v0, "fetch_time"

    iget-wide v2, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 575541
    const-string v0, "user_id"

    iget-object v1, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 575542
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 575543
    check-cast p1, Lcom/facebook/aldrin/status/AldrinUserStatus;

    invoke-static {p1, p2, p3}, Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;->a(Lcom/facebook/aldrin/status/AldrinUserStatus;LX/0nX;LX/0my;)V

    return-void
.end method
