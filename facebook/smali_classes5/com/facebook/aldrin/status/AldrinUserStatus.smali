.class public Lcom/facebook/aldrin/status/AldrinUserStatus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/aldrin/status/AldrinUserStatusDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/aldrin/status/AldrinUserStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "current_region"
    .end annotation
.end field

.field public final effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "effective_region"
    .end annotation
.end field

.field public final fetchTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fetch_time"
    .end annotation
.end field

.field public final tosCookiesUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tos_cookies_url"
    .end annotation
.end field

.field public final tosPrivacyUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tos_privacy_url"
    .end annotation
.end field

.field public final tosTermsUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tos_terms_url"
    .end annotation
.end field

.field public final tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tos_transition_type"
    .end annotation
.end field

.field public final tosVersion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tos_version"
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 575522
    const-class v0, Lcom/facebook/aldrin/status/AldrinUserStatusDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 575523
    const-class v0, Lcom/facebook/aldrin/status/AldrinUserStatusSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 575510
    new-instance v0, LX/2Wh;

    invoke-direct {v0}, LX/2Wh;-><init>()V

    sput-object v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 575511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575512
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575513
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575514
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->NOOP:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 575515
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTermsUrl:Ljava/lang/String;

    .line 575516
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosPrivacyUrl:Ljava/lang/String;

    .line 575517
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosCookiesUrl:Ljava/lang/String;

    .line 575518
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    .line 575519
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    .line 575520
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    .line 575521
    return-void
.end method

.method public constructor <init>(LX/2Yl;)V
    .locals 4

    .prologue
    .line 575490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575491
    iget-object v0, p1, LX/2Yl;->a:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-object v0, v0

    .line 575492
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575493
    iget-object v0, p1, LX/2Yl;->b:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-object v0, v0

    .line 575494
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575495
    iget-object v0, p1, LX/2Yl;->c:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    move-object v0, v0

    .line 575496
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 575497
    iget-object v0, p1, LX/2Yl;->f:Ljava/lang/String;

    move-object v0, v0

    .line 575498
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTermsUrl:Ljava/lang/String;

    .line 575499
    iget-object v0, p1, LX/2Yl;->g:Ljava/lang/String;

    move-object v0, v0

    .line 575500
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosPrivacyUrl:Ljava/lang/String;

    .line 575501
    iget-object v0, p1, LX/2Yl;->h:Ljava/lang/String;

    move-object v0, v0

    .line 575502
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosCookiesUrl:Ljava/lang/String;

    .line 575503
    iget-object v0, p1, LX/2Yl;->i:Ljava/lang/String;

    move-object v0, v0

    .line 575504
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    .line 575505
    iget-wide v2, p1, LX/2Yl;->j:J

    move-wide v0, v2

    .line 575506
    iput-wide v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    .line 575507
    iget-object v0, p1, LX/2Yl;->k:Ljava/lang/String;

    move-object v0, v0

    .line 575508
    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    .line 575509
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 575479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575480
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575481
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 575482
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 575483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTermsUrl:Ljava/lang/String;

    .line 575484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosPrivacyUrl:Ljava/lang/String;

    .line 575485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosCookiesUrl:Ljava/lang/String;

    .line 575486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    .line 575487
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    .line 575488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    .line 575489
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 575478
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 575468
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 575469
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 575470
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 575471
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTermsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575472
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosPrivacyUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575473
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosCookiesUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575474
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575475
    iget-wide v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->fetchTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 575476
    iget-object v0, p0, Lcom/facebook/aldrin/status/AldrinUserStatus;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575477
    return-void
.end method
