.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final d:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611200
    new-instance v0, LX/3bX;

    invoke-direct {v0}, LX/3bX;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611201
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611202
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    .line 611203
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 611204
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    .line 611205
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    .line 611206
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 611207
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 611208
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611209
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611212
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;)V

    .line 611213
    move-object v0, p0

    .line 611214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611218
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 611219
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611220
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611221
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b16d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 611222
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b16d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b16d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v3, v0, v4, v0}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 611224
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611225
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 611226
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 611227
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
