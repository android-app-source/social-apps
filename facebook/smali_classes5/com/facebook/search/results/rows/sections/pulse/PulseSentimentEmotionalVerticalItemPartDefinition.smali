.class public Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EP3;",
        "LX/EP5;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/0wM;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 609528
    new-instance v0, LX/3bD;

    invoke-direct {v0}, LX/3bD;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a:LX/1Cz;

    .line 609529
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 609530
    iput v1, v0, LX/1UY;->b:F

    .line 609531
    move-object v0, v0

    .line 609532
    iput v1, v0, LX/1UY;->c:F

    .line 609533
    move-object v0, v0

    .line 609534
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609535
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609536
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->c:LX/0wM;

    .line 609537
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609538
    return-void
.end method

.method private a(LX/EP3;Landroid/content/Context;)LX/EP5;
    .locals 9

    .prologue
    .line 609539
    iget-object v0, p1, LX/EP3;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    if-nez v0, :cond_0

    .line 609540
    new-instance v0, LX/EP5;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->c:LX/0wM;

    iget-object v2, p1, LX/EP3;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, -0xa76f01

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget v2, p1, LX/EP3;->b:I

    .line 609541
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v4

    int-to-long v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 609542
    const v5, 0x7f082278

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-virtual {p2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 609543
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 609544
    invoke-static {v6, v5, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 609545
    move-object v2, v6

    .line 609546
    invoke-direct {v0, v1, v2}, LX/EP5;-><init>(Landroid/graphics/drawable/Drawable;Landroid/text/SpannableStringBuilder;)V

    .line 609547
    :goto_0
    return-object v0

    .line 609548
    :cond_0
    iget-object v0, p1, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 609549
    :goto_1
    new-instance v1, LX/EP5;

    .line 609550
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v4

    iget v5, p1, LX/EP3;->b:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 609551
    const v5, 0x7f082277

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    iget-object v8, p1, LX/EP3;->a:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {p2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 609552
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 609553
    invoke-static {v6, v5, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 609554
    iget-object v4, p1, LX/EP3;->a:Ljava/lang/String;

    invoke-static {v6, v5, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 609555
    move-object v2, v6

    .line 609556
    invoke-direct {v1, v0, v2}, LX/EP5;-><init>(Landroid/net/Uri;Landroid/text/SpannableStringBuilder;)V

    move-object v0, v1

    goto :goto_0

    .line 609557
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;
    .locals 5

    .prologue
    .line 609558
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    monitor-enter v1

    .line 609559
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609560
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609561
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609562
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609563
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;-><init>(LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 609564
    move-object v0, p0

    .line 609565
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609566
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609567
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 609569
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 609570
    :cond_0
    :goto_0
    return-void

    .line 609571
    :cond_1
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 609572
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 609573
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 609574
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/16 v3, 0x21

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609575
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 609576
    check-cast p2, LX/EP3;

    check-cast p3, LX/1Pn;

    .line 609577
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609578
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a(LX/EP3;Landroid/content/Context;)LX/EP5;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x13cb84a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609579
    check-cast p2, LX/EP5;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 609580
    if-eqz p2, :cond_2

    .line 609581
    iget-object v1, p2, LX/EP5;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 609582
    iget-object v1, p2, LX/EP5;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 609583
    :cond_0
    iget-object v1, p2, LX/EP5;->b:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 609584
    iget-object v1, p2, LX/EP5;->b:Landroid/net/Uri;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 609585
    :cond_1
    sget-object v1, LX/6VF;->XSMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 609586
    iget-object v1, p2, LX/EP5;->c:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 609587
    const v1, 0x7f0e01e8

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 609588
    :cond_2
    const/16 v1, 0x1f

    const v2, 0x41aab8e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609589
    const/4 v0, 0x1

    return v0
.end method
