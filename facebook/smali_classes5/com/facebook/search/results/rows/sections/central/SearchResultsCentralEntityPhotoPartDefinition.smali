.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/widget/RelativeLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x3f400000    # -6.0f

    .line 609621
    const v0, 0x7f03025f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->a:LX/1Cz;

    .line 609622
    const-class v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 609623
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 609624
    iput v2, v0, LX/1UY;->b:F

    .line 609625
    move-object v0, v0

    .line 609626
    iput v2, v0, LX/1UY;->c:F

    .line 609627
    move-object v0, v0

    .line 609628
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 609629
    iput v1, v0, LX/1UY;->d:F

    .line 609630
    move-object v0, v0

    .line 609631
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609660
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609661
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 609662
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 609663
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 609664
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;
    .locals 6

    .prologue
    .line 609649
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;

    monitor-enter v1

    .line 609650
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609651
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609652
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609653
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609654
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 609655
    move-object v0, p0

    .line 609656
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609657
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609658
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/RelativeLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609665
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 609635
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609636
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 609637
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;

    .line 609638
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v2, LX/3aw;

    sget-object v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->c:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609639
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;->q()Ljava/lang/String;

    move-result-object v1

    .line 609640
    if-eqz v1, :cond_0

    .line 609641
    const v2, 0x7f0d08f3

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    invoke-virtual {v4, v1}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v4, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 609642
    iput-object v4, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 609643
    move-object v1, v1

    .line 609644
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609645
    :cond_0
    const v1, 0x7f0d08f4

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609646
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 609647
    const v1, 0x7f0d08f5

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;->n()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609648
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609632
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609633
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 609634
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
