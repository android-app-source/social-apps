.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 607203
    new-instance v0, LX/3am;

    invoke-direct {v0}, LX/3am;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->a:LX/1Cz;

    .line 607204
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 607205
    iput v1, v0, LX/1UY;->b:F

    .line 607206
    move-object v0, v0

    .line 607207
    const/high16 v1, -0x3f800000    # -4.0f

    .line 607208
    iput v1, v0, LX/1UY;->c:F

    .line 607209
    move-object v0, v0

    .line 607210
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607237
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607238
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 607239
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607240
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->c:Landroid/content/res/Resources;

    .line 607241
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;
    .locals 6

    .prologue
    .line 607226
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    monitor-enter v1

    .line 607227
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607228
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607229
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607230
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607231
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 607232
    move-object v0, p0

    .line 607233
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607234
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607235
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 607225
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0822b3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v4

    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607224
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607215
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    .line 607216
    const v0, 0x7f0d0546

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607217
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 607218
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607219
    const v0, 0x7f0d0547

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607220
    iget v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->b:I

    move v2, v2

    .line 607221
    invoke-direct {p0, v2}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607222
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, p2, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607212
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    .line 607213
    iget v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->b:I

    move v0, v0

    .line 607214
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 607211
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    return-object v0
.end method
