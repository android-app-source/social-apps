.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40c00000    # 6.0f

    .line 610520
    const v0, 0x7f0312aa

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a:LX/1Cz;

    .line 610521
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 610522
    iput v1, v0, LX/1UY;->b:F

    .line 610523
    move-object v0, v0

    .line 610524
    iput v1, v0, LX/1UY;->c:F

    .line 610525
    move-object v0, v0

    .line 610526
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610527
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610528
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610529
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    .line 610530
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 610531
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    .line 610532
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    .line 610533
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;
    .locals 9

    .prologue
    .line 610534
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    monitor-enter v1

    .line 610535
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610536
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610537
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610538
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610539
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;)V

    .line 610540
    move-object v0, v3

    .line 610541
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610542
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610543
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 610545
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610546
    check-cast v0, LX/A2T;

    .line 610547
    invoke-interface {v0}, LX/A2T;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v0

    invoke-interface {v0}, LX/A2S;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610548
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 610549
    check-cast p2, LX/CzL;

    .line 610550
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610551
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610552
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 610553
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610554
    check-cast v0, LX/A2T;

    invoke-interface {v0}, LX/A2T;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610555
    const v0, 0x7f0d2b9f

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 610556
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610557
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610558
    check-cast p1, LX/CzL;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method
