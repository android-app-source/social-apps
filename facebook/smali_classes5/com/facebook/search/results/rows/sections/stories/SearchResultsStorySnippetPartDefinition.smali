.class public Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EPc;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Landroid/content/res/Resources;

.field public final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607067
    new-instance v0, LX/3ak;

    invoke-direct {v0}, LX/3ak;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607068
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607069
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607070
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607071
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 607072
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->e:Landroid/content/res/Resources;

    .line 607073
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->f:LX/0Uh;

    .line 607074
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;
    .locals 9

    .prologue
    .line 607075
    const-class v1, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    monitor-enter v1

    .line 607076
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607077
    sput-object v2, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607078
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607079
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607080
    new-instance v3, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Landroid/content/res/Resources;LX/0Uh;)V

    .line 607081
    move-object v0, v3

    .line 607082
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607083
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607084
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607085
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 607086
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607087
    sget-object v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 607088
    check-cast p2, LX/EPc;

    check-cast p3, LX/1Ps;

    .line 607089
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/EPc;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->d:LX/1Ua;

    check-cast p3, LX/1Po;

    .line 607090
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    sget-object v5, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->f:LX/0Uh;

    sget v5, LX/2SU;->h:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 607091
    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    .line 607092
    :goto_0
    move-object v4, v4

    .line 607093
    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607094
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v1, p2, LX/EPc;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607095
    iget-object v0, p2, LX/EPc;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607096
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EPb;

    invoke-direct {v1, p0, p2}, LX/EPb;-><init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;LX/EPc;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607097
    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    sget-object v4, LX/1X9;->TOP:LX/1X9;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607098
    check-cast p1, LX/EPc;

    .line 607099
    iget-object v0, p1, LX/EPc;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
