.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "LX/EJm;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

.field private final d:LX/0y2;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610616
    new-instance v0, LX/3bM;

    const v1, 0x7f031288

    invoke-direct {v0, v1}, LX/3bM;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a:LX/1Cz;

    .line 610617
    const-class v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;LX/0y2;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610611
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610612
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    .line 610613
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->d:LX/0y2;

    .line 610614
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->e:LX/0ad;

    .line 610615
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;
    .locals 6

    .prologue
    .line 610559
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    monitor-enter v1

    .line 610560
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610561
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610562
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610563
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610564
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v4

    check-cast v4, LX/0y2;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;LX/0y2;LX/0ad;)V

    .line 610565
    move-object v0, p0

    .line 610566
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610567
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610568
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/16 v4, 0x11

    .line 610604
    new-instance v0, LX/3DI;

    const-string v1, " "

    invoke-direct {v0, v1}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 610605
    if-eqz p2, :cond_0

    .line 610606
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0e0884

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p2, v1, v4}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 610607
    :cond_0
    if-eqz p1, :cond_1

    .line 610608
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v0, p1, v1, v4}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 610609
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0e0886

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 610610
    :cond_1
    return-object v0
.end method

.method private static a(LX/CzL;LX/EJm;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;",
            "LX/EJm;",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 610591
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610592
    check-cast v0, LX/A2T;

    .line 610593
    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/CzL;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610594
    invoke-interface {v0}, LX/A2T;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductName(Ljava/lang/CharSequence;)V

    .line 610595
    iget-object v1, p1, LX/EJm;->b:Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductPrice(Ljava/lang/CharSequence;)V

    .line 610596
    invoke-static {p0}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 610597
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_0

    .line 610598
    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductSubtitle(Ljava/lang/CharSequence;)V

    .line 610599
    :goto_0
    iget-boolean v0, p1, LX/EJm;->a:Z

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setNearbyIndicatorEnabled(Z)V

    .line 610600
    return-void

    .line 610601
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_1

    .line 610602
    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->e()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$ToModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$ToModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 610603
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported product item role"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 610588
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610589
    check-cast v0, LX/A2T;

    .line 610590
    invoke-interface {v0}, LX/A2T;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v0

    invoke-interface {v0}, LX/A2S;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610587
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 610572
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    .line 610573
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610574
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610575
    check-cast v0, LX/A2T;

    .line 610576
    invoke-interface {v0}, LX/A2T;->aq()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 610577
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 610578
    :goto_0
    invoke-interface {v0}, LX/A2T;->aG()D

    move-result-wide v4

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    invoke-interface {v0}, LX/A2T;->aH()D

    move-result-wide v4

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2

    .line 610579
    :cond_0
    new-instance v0, LX/EJm;

    invoke-direct {v0, v2, v1}, LX/EJm;-><init>(ZLjava/lang/CharSequence;)V

    .line 610580
    :goto_1
    return-object v0

    .line 610581
    :cond_1
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 610582
    :cond_2
    invoke-interface {v0}, LX/A2T;->aG()D

    move-result-wide v4

    invoke-interface {v0}, LX/A2T;->aH()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 610583
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->d:LX/0y2;

    invoke-virtual {v3}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    .line 610584
    if-eqz v3, :cond_3

    invoke-static {v3, v0}, LX/3GC;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)F

    move-result v0

    float-to-double v4, v0

    const-wide v6, 0x40d86a0000000000L    # 25000.0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_3

    const/4 v0, 0x1

    .line 610585
    :goto_2
    new-instance v2, LX/EJm;

    invoke-direct {v2, v0, v1}, LX/EJm;-><init>(ZLjava/lang/CharSequence;)V

    move-object v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    .line 610586
    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x270cc186

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610571
    check-cast p1, LX/CzL;

    check-cast p2, LX/EJm;

    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;

    invoke-static {p1, p2, p4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(LX/CzL;LX/EJm;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;)V

    const/16 v1, 0x1f

    const v2, -0x2e250d9b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610570
    check-cast p1, LX/CzL;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method
