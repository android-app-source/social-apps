.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ":",
        "LX/Cxc;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final h:LX/0ad;

.field public final i:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40c00000    # 6.0f

    .line 607641
    const v0, 0x7f030493

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->a:LX/1Cz;

    .line 607642
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 607643
    iput v1, v0, LX/1UY;->b:F

    .line 607644
    move-object v0, v0

    .line 607645
    iput v1, v0, LX/1UY;->c:F

    .line 607646
    move-object v0, v0

    .line 607647
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607648
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607649
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607650
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607651
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    .line 607652
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607653
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607654
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->h:LX/0ad;

    .line 607655
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->i:LX/0Uh;

    .line 607656
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;
    .locals 11

    .prologue
    .line 607657
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    monitor-enter v1

    .line 607658
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607659
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607662
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;LX/0ad;LX/0Uh;)V

    .line 607663
    move-object v0, v3

    .line 607664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607668
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 607669
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x0

    .line 607670
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607671
    new-instance v0, LX/1X6;

    sget-object v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->b:LX/1Ua;

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v5, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 607672
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607673
    const v0, 0x7f0d0d8e

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    new-instance v2, LX/ELL;

    const/4 v3, 0x4

    iget-object v4, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v4}, LX/ELM;->e(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/ELL;-><init>(ILX/0Px;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607674
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607675
    check-cast p3, LX/Cxc;

    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3, v1}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->i:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 607676
    :cond_0
    const/4 v1, 0x0

    invoke-static {p2, v1}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v1

    .line 607677
    :cond_1
    :goto_0
    move-object v1, v1

    .line 607678
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607679
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v1}, LX/ELM;->a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607680
    return-object v5

    .line 607681
    :cond_2
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 607682
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 607683
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 607684
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 607685
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 607686
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 607687
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 607688
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607689
    const/4 v0, 0x0

    return v0
.end method
