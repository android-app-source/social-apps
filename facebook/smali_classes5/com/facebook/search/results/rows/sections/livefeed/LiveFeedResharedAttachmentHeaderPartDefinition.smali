.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/CxV;",
        "LX/ENG;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611120
    new-instance v0, LX/3bU;

    invoke-direct {v0}, LX/3bU;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611121
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611122
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    .line 611123
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    .line 611124
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;
    .locals 5

    .prologue
    .line 611125
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    monitor-enter v1

    .line 611126
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611127
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611130
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;)V

    .line 611131
    move-object v0, p0

    .line 611132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/ENG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611136
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 611137
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611138
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611139
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    sget-object v1, LX/1X9;->TOP:LX/1X9;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611140
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611141
    const/4 v0, 0x1

    return v0
.end method
