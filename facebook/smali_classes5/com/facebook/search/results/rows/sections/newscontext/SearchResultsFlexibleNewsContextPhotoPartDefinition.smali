.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataInterfaces$SearchResultsFlexibleContextMetadata;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/EK4;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/EK4;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/CvY;

.field public final e:LX/0Uh;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608967
    new-instance v0, LX/3b2;

    invoke-direct {v0}, LX/3b2;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->a:LX/1Cz;

    .line 608968
    const-class v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/CvY;LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/CvY;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608937
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608938
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 608939
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 608940
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->d:LX/CvY;

    .line 608941
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->e:LX/0Uh;

    .line 608942
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->f:LX/0Ot;

    .line 608943
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/EK4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608966
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 608959
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    .line 608960
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608961
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 608962
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608963
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 608964
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/ENe;

    invoke-direct {v2, p0, v0, p3, p2}, LX/ENe;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;Landroid/net/Uri;LX/1Ps;LX/CzL;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608965
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6d79c695

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 608949
    check-cast p1, LX/CzL;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/EK4;

    const/4 v2, 0x0

    .line 608950
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kb;

    invoke-virtual {v1}, LX/0kb;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->e:LX/0Uh;

    sget v4, LX/2SU;->T:I

    invoke-virtual {v1, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    move v2, v1

    .line 608951
    :cond_0
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 608952
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 608953
    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v4

    .line 608954
    sget-object p2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, p2, v2}, LX/EK4;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V

    .line 608955
    check-cast p3, LX/1Pp;

    .line 608956
    iget-object v1, p4, LX/EK4;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v1

    .line 608957
    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    const/4 v2, 0x0

    sget-object p2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v1, v2, v4, p2}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 608958
    const/16 v1, 0x1f

    const v2, 0x5ca2dfe4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608944
    check-cast p1, LX/CzL;

    .line 608945
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608946
    if-eqz v0, :cond_0

    .line 608947
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608948
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
