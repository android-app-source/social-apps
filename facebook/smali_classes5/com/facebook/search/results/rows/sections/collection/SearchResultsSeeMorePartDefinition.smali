.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<TT;>;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/3ag;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final g:LX/0ad;

.field private final h:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 606974
    new-instance v0, LX/3aj;

    invoke-direct {v0}, LX/3aj;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a:LX/3ag;

    .line 606975
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 606976
    iput v1, v0, LX/1UY;->b:F

    .line 606977
    move-object v0, v0

    .line 606978
    const/high16 v1, 0x40a00000    # 5.0f

    .line 606979
    iput v1, v0, LX/1UY;->c:F

    .line 606980
    move-object v0, v0

    .line 606981
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607016
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607017
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607018
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    .line 607019
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->e:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    .line 607020
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607021
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->g:LX/0ad;

    .line 607022
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->h:LX/0Uh;

    .line 607023
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;
    .locals 10

    .prologue
    .line 607005
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    monitor-enter v1

    .line 607006
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607007
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607008
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607009
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607010
    new-instance v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0ad;LX/0Uh;)V

    .line 607011
    move-object v0, v3

    .line 607012
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607013
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607014
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0ad;LX/0Uh;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 607004
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/BaseFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;LX/0Px;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607003
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 606986
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 606987
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606988
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 606989
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0822b4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606990
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->g:LX/0ad;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->h:LX/0Uh;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/BaseFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 606991
    iget-object v5, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v5, v5

    .line 606992
    invoke-static {v1, v2, v3, v4, v5}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0am;)LX/CyI;

    move-result-object v2

    .line 606993
    if-eqz v2, :cond_1

    .line 606994
    sget-object v1, LX/CyI;->PLACES:LX/CyI;

    if-ne v2, v1, :cond_0

    invoke-static {}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a()LX/0Px;

    move-result-object v1

    .line 606995
    :goto_0
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->e:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    new-instance v4, LX/EKB;

    invoke-direct {v4, v2, v0, v1}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/0Px;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606996
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606997
    const/4 v0, 0x0

    return-object v0

    .line 606998
    :cond_0
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 606999
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 607000
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    new-instance v2, LX/EK9;

    .line 607001
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n:LX/D0L;

    move-object v3, v3

    .line 607002
    sget-object v4, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a:LX/3ag;

    const v5, 0x7f0822b4

    invoke-direct {v2, v0, v3, v4, v5}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 606982
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 606983
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606984
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 606985
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->g:LX/0ad;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->h:LX/0Uh;

    invoke-static {v1, v2, v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a(LX/0ad;LX/0Uh;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    return v0
.end method
