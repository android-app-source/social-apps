.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611230
    new-instance v0, LX/3bY;

    invoke-direct {v0}, LX/3bY;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611231
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611232
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    .line 611233
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    .line 611234
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    .line 611235
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;
    .locals 6

    .prologue
    .line 611236
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    monitor-enter v1

    .line 611237
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611238
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611239
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611240
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611241
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;)V

    .line 611242
    move-object v0, p0

    .line 611243
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611244
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611245
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611246
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611247
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 611248
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611249
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611250
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611251
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 611252
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611253
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 611254
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->c:Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;

    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/VideoAttachmentsSelectorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
