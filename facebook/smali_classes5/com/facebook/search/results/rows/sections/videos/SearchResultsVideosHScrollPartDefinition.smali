.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field private final c:LX/0hB;

.field private final d:LX/E3C;

.field private final e:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPagePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final i:LX/2dq;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f800000    # -4.0f

    .line 609692
    sget-object v0, LX/2eA;->c:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->a:LX/1Cz;

    .line 609693
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 609694
    iput v1, v0, LX/1UY;->c:F

    .line 609695
    move-object v0, v0

    .line 609696
    iput v1, v0, LX/1UY;->d:F

    .line 609697
    move-object v0, v0

    .line 609698
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0hB;LX/E3C;Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0Ot;LX/0Ot;LX/2dq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hB;",
            "LX/E3C;",
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPagePartDefinition;",
            ">;",
            "LX/2dq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609733
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609734
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->c:LX/0hB;

    .line 609735
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->d:LX/E3C;

    .line 609736
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 609737
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 609738
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->g:LX/0Ot;

    .line 609739
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->h:LX/0Ot;

    .line 609740
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->i:LX/2dq;

    .line 609741
    return-void
.end method

.method private a(LX/CzL;LX/1Pn;)LX/2eF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<*>;TE;)",
            "LX/2eF;"
        }
    .end annotation

    .prologue
    .line 609728
    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->d:LX/E3C;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0, v1, v2}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result v0

    int-to-float v0, v0

    .line 609729
    :goto_0
    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 609730
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->i:LX/2dq;

    int-to-float v1, v1

    const/high16 v3, 0x41000000    # 8.0f

    add-float/2addr v1, v3

    sget-object v3, LX/2eF;->a:LX/1Ua;

    const/4 p1, 0x1

    invoke-virtual {v2, v1, v3, p1}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    move-object v0, v1

    .line 609731
    return-object v0

    .line 609732
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;
    .locals 11

    .prologue
    .line 609717
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;

    monitor-enter v1

    .line 609718
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609719
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609720
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609721
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609722
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/E3C;->a(LX/0QB;)LX/E3C;

    move-result-object v5

    check-cast v5, LX/E3C;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const/16 v8, 0x11ac

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x11a5

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v10

    check-cast v10, LX/2dq;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;-><init>(LX/0hB;LX/E3C;Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/0Ot;LX/0Ot;LX/2dq;)V

    .line 609723
    move-object v0, v3

    .line 609724
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609725
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609726
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609716
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 609701
    check-cast p2, LX/0Px;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    .line 609702
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/CzL;

    .line 609703
    invoke-virtual {v6}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 609704
    iget-object v0, v6, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 609705
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609706
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v3

    .line 609707
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609708
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 609709
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 609710
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 609711
    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    invoke-direct {p0, v6, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->a(LX/CzL;LX/1Pn;)LX/2eF;

    move-result-object v1

    .line 609712
    new-instance v3, LX/EPq;

    invoke-direct {v3, p0, p2}, LX/EPq;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;LX/0Px;)V

    move-object v3, v3

    .line 609713
    new-instance v5, LX/EPp;

    invoke-direct {v5, p0, v4}, LX/EPp;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;Ljava/lang/String;)V

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609714
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v6, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609715
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 609699
    check-cast p1, LX/0Px;

    const/4 v0, 0x1

    .line 609700
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
