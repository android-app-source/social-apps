.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611196
    new-instance v0, LX/3bW;

    invoke-direct {v0}, LX/3bW;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611177
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611178
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    .line 611179
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    .line 611180
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;
    .locals 5

    .prologue
    .line 611185
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    monitor-enter v1

    .line 611186
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611187
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611188
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611189
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611190
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;)V

    .line 611191
    move-object v0, p0

    .line 611192
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611193
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611194
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611195
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611197
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 611181
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611182
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611183
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611184
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 611173
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611174
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 611175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 611176
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
