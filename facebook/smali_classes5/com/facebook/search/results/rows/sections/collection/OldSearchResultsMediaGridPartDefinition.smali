.class public Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxG;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
        ">;",
        "LX/EJY;",
        "TE;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
        "<",
        "LX/ByV;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:LX/1X6;

.field private static final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static q:LX/0Xm;


# instance fields
.field private final f:LX/26H;

.field private final g:LX/1Ad;

.field public final h:LX/1qa;

.field public final i:LX/23R;

.field private final j:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final k:LX/ByX;

.field private final l:LX/Byg;

.field public final m:LX/1nD;

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/CvY;

.field public final p:LX/D3w;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 607855
    new-instance v0, LX/3aq;

    invoke-direct {v0}, LX/3aq;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    .line 607856
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->c:LX/0am;

    .line 607857
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->d:LX/0am;

    .line 607858
    const-class v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 607859
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->b:LX/1X6;

    return-void
.end method

.method public constructor <init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/ByX;LX/Byg;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/CvY;LX/D3w;LX/CvM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607842
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607843
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    .line 607844
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->m:LX/1nD;

    .line 607845
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->f:LX/26H;

    .line 607846
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->g:LX/1Ad;

    .line 607847
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->h:LX/1qa;

    .line 607848
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->i:LX/23R;

    .line 607849
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->j:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607850
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->k:LX/ByX;

    .line 607851
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->l:LX/Byg;

    .line 607852
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->o:LX/CvY;

    .line 607853
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->p:LX/D3w;

    .line 607854
    return-void
.end method

.method private a(Ljava/util/HashMap;LX/0Px;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Ps;)LX/Aj7;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;>;",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;TE;)",
            "LX/Aj7",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607841
    new-instance v0, LX/EJU;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/EJU;-><init>(Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Ps;Ljava/util/HashMap;LX/0Px;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;
    .locals 3

    .prologue
    .line 607833
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    monitor-enter v1

    .line 607834
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607835
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607836
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607837
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607838
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607839
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607840
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 607826
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 607827
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 607828
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 607829
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v1, v1

    .line 607830
    sget-object v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->c:LX/0am;

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v1, v2, :cond_0

    .line 607831
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v1

    .line 607832
    sget-object v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->d:LX/0am;

    invoke-virtual {v0, v1}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/1Ps;LX/26O;LX/1qa;LX/1Ad;)[LX/1aZ;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InstanceMethodCanBeStatic"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/26O",
            "<",
            "LX/ByV;",
            ">;",
            "LX/1qa;",
            "LX/1Ad;",
            ")[",
            "LX/1aZ;"
        }
    .end annotation

    .prologue
    .line 607817
    invoke-virtual {p1}, LX/26O;->a()LX/0Px;

    move-result-object v2

    .line 607818
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    new-array v3, v0, [LX/1aZ;

    .line 607819
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    .line 607820
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 607821
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ByV;

    invoke-virtual {v0}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v5, LX/26P;->Photo:LX/26P;

    invoke-virtual {p2, v0, v5}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v5

    .line 607822
    invoke-virtual {v4, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    aput-object v0, v3, v1

    move-object v0, p0

    .line 607823
    check-cast v0, LX/1Pp;

    aget-object v6, v3, v1

    const/4 v7, 0x0

    sget-object v8, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v6, v7, v5, v8}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 607824
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 607825
    :cond_0
    return-object v3
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;
    .locals 13

    .prologue
    .line 607860
    new-instance v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    const-class v1, LX/26H;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/26H;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v3

    check-cast v3, LX/1qa;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v4

    check-cast v4, LX/23R;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v6, LX/ByX;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ByX;

    const-class v7, LX/Byg;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Byg;

    invoke-static {p0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v8

    check-cast v8, LX/1nD;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v10

    check-cast v10, LX/CvY;

    invoke-static {p0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v11

    check-cast v11, LX/D3w;

    invoke-static {p0}, LX/CvM;->a(LX/0QB;)LX/CvM;

    move-result-object v12

    check-cast v12, LX/CvM;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;-><init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/ByX;LX/Byg;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/CvY;LX/D3w;LX/CvM;)V

    .line 607861
    return-object v0
.end method

.method public static b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 607816
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 607815
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 607775
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/4 v3, 0x0

    .line 607776
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 607777
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 607778
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->j:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->b:LX/1X6;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607779
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 607780
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move v2, v3

    .line 607781
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 607782
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNode;

    .line 607783
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v7, 0x4ed245b

    if-ne v4, v7, :cond_2

    .line 607784
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 607785
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 607786
    :goto_1
    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 607787
    invoke-virtual {p2, v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 607788
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607789
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 607790
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 607791
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v7, 0x1eaef984

    if-ne v4, v7, :cond_4

    .line 607792
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 607793
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    .line 607794
    :cond_3
    const/4 v4, 0x0

    .line 607795
    :goto_2
    move-object v4, v4

    .line 607796
    goto :goto_1

    .line 607797
    :cond_4
    invoke-static {v1}, LX/6X3;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    .line 607798
    if-eqz v4, :cond_0

    .line 607799
    invoke-static {v4}, LX/6X5;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    goto :goto_1

    .line 607800
    :cond_5
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v2

    .line 607801
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 607802
    if-eqz v2, :cond_7

    .line 607803
    invoke-static {v4}, LX/Byg;->a(LX/0Px;)LX/Byf;

    move-result-object v1

    .line 607804
    :goto_3
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->f:LX/26H;

    invoke-virtual {v5, v1}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v1

    .line 607805
    invoke-direct {p0, v6, v4, v0, p3}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a(Ljava/util/HashMap;LX/0Px;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Ps;)LX/Aj7;

    move-result-object v0

    .line 607806
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->h:LX/1qa;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->g:LX/1Ad;

    invoke-static {p3, v1, v5, v6}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a(LX/1Ps;LX/26O;LX/1qa;LX/1Ad;)[LX/1aZ;

    move-result-object v5

    .line 607807
    new-instance v6, LX/EJY;

    if-eqz v2, :cond_6

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    const/4 v4, 0x3

    if-lt v2, v4, :cond_6

    const/4 v3, 0x1

    :cond_6
    invoke-direct {v6, v5, v1, v0, v3}, LX/EJY;-><init>([LX/1aZ;LX/26O;LX/Aj7;Z)V

    return-object v6

    .line 607808
    :cond_7
    invoke-static {v4}, LX/ByX;->a(LX/0Px;)LX/ByW;

    move-result-object v1

    goto :goto_3

    :cond_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    .line 607809
    iput-object v7, v4, LX/4XB;->T:Ljava/lang/String;

    .line 607810
    move-object v4, v4

    .line 607811
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 607812
    iput-object v7, v4, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 607813
    move-object v4, v4

    .line 607814
    invoke-virtual {v4}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x9170035

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 607770
    check-cast p2, LX/EJY;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-static {p2, p4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/EJY;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    const/16 v1, 0x1f

    const v2, 0xaf9356c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607774
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 607771
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 607772
    invoke-static {p4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    .line 607773
    return-void
.end method
