.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherInterfaces$SearchResultsWeather$WeatherHourlyForecast;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0W9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 611071
    new-instance v0, LX/3bR;

    const v1, 0x7f0312a5

    invoke-direct {v0, v1}, LX/3bR;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->a:LX/1Cz;

    .line 611072
    const-class v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Ot;LX/0Ot;LX/0W9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            ">;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611065
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611066
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 611067
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->d:LX/0Ot;

    .line 611068
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->e:LX/0Ot;

    .line 611069
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->f:LX/0W9;

    .line 611070
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;
    .locals 7

    .prologue
    .line 611054
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;

    monitor-enter v1

    .line 611055
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611056
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611057
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611058
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611059
    new-instance v5, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    const/16 v4, 0x2a87

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0xe12

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Ot;LX/0Ot;LX/0W9;)V

    .line 611060
    move-object v0, v5

    .line 611061
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611062
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611063
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;)Z
    .locals 4

    .prologue
    .line 611053
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611052
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 611024
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    .line 611025
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 611026
    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    .line 611027
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 611028
    const v1, 0x7f0d2b96

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 611029
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->d()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 611030
    const v6, 0x7f082291

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 611031
    :goto_0
    move-object v5, v6

    .line 611032
    invoke-interface {p1, v1, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 611033
    const v1, 0x7f0d2b97

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 611034
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->b()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\u00b0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 611035
    invoke-interface {p1, v1, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 611036
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->b()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 611037
    :goto_1
    if-nez v1, :cond_1

    .line 611038
    const v1, 0x7f0d2b98

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    const v4, 0x7f020263

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {p1, v1, v0, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 611039
    :goto_2
    return-object v2

    :cond_0
    move-object v1, v2

    .line 611040
    goto :goto_1

    .line 611041
    :cond_1
    const v3, 0x7f0d2b98

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v4, LX/2f8;

    invoke-direct {v4}, LX/2f8;-><init>()V

    invoke-virtual {v4, v1}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v4, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 611042
    iput-object v4, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 611043
    move-object v1, v1

    .line 611044
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v3, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_2

    .line 611045
    :cond_2
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "h a"

    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->f:LX/0W9;

    invoke-virtual {v8}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 611046
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 611047
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->d()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 611048
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611049
    check-cast p1, LX/CzL;

    .line 611050
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 611051
    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;)Z

    move-result v0

    return v0
.end method
