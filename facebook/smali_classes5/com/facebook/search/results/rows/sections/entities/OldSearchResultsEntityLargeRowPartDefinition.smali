.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Uh;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607470
    const v0, 0x7f030490

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607471
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607472
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    .line 607473
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607474
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607475
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607476
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607477
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607478
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607479
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;
    .locals 11

    .prologue
    .line 607480
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    monitor-enter v1

    .line 607481
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607482
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607483
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607484
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607485
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;-><init>(LX/0Uh;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;)V

    .line 607486
    move-object v0, v3

    .line 607487
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607488
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607489
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607491
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 607492
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 607493
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607494
    const/4 p3, 0x0

    .line 607495
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607496
    invoke-static {p2, p3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;I)Ljava/lang/String;

    move-result-object v0

    .line 607497
    :goto_0
    move-object v0, v0

    .line 607498
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607499
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607500
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    invoke-static {v2}, LX/ELM;->a(LX/0Uh;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 607501
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 607502
    :goto_1
    move-object v0, v2

    .line 607503
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607504
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607505
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 607506
    const/4 v1, 0x1

    invoke-static {p2, v1}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;I)Ljava/lang/String;

    move-result-object v1

    .line 607507
    :goto_2
    move-object v1, v1

    .line 607508
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607509
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607510
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/3ap;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607511
    const/4 v0, 0x0

    return-object v0

    .line 607512
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    sget v3, LX/2SU;->F:I

    invoke-virtual {v2, v3, p3}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 607513
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->b:LX/0Uh;

    sget v5, LX/2SU;->F:I

    const/4 p3, 0x0

    invoke-virtual {v4, v5, p3}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-static {v2, v3, v4}, LX/ELM;->b(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1

    :cond_3
    invoke-static {p2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607514
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 607515
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
