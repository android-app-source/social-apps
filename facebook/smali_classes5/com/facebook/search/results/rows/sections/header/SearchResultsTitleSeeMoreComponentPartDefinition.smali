.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        "T::",
        "Lcom/facebook/search/results/protocol/SearchResultsModuleTitleInterfaces$SearchResultsModuleTitle;",
        ":",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+TT;>;TE;>;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final d:LX/0ad;

.field private final e:LX/0Uh;

.field private final f:LX/EN3;

.field private final g:LX/1V0;

.field public final h:LX/1vg;

.field private final i:LX/EPK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0Uh;LX/EN3;LX/1V0;LX/1vg;LX/EPK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 606871
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 606872
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->d:LX/0ad;

    .line 606873
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->e:LX/0Uh;

    .line 606874
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->f:LX/EN3;

    .line 606875
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->g:LX/1V0;

    .line 606876
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->h:LX/1vg;

    .line 606877
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->i:LX/EPK;

    .line 606878
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/Cxi;)LX/1X1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+TT;>;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 606845
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->f:LX/EN3;

    invoke-virtual {v0, p1}, LX/EN3;->c(LX/1De;)LX/EN1;

    move-result-object v1

    .line 606846
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 606847
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 606848
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 606849
    invoke-virtual {p2}, LX/CzL;->g()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0Px;)Ljava/lang/Integer;

    move-result-object v3

    .line 606850
    invoke-direct {p0, p2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->b(LX/CzL;)Z

    move-result v4

    .line 606851
    if-eqz v3, :cond_2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v2, v5, :cond_2

    const/4 v5, 0x1

    .line 606852
    :goto_0
    if-eqz v5, :cond_3

    sget-object v6, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v6, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 606853
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->h:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    sget-object v5, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v5, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v5}, LX/2xv;->i(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 606854
    :goto_1
    move-object v3, v5

    .line 606855
    invoke-virtual {v1, v3}, LX/EN1;->a(LX/1dc;)LX/EN1;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/EN1;->b(Ljava/lang/String;)LX/EN1;

    move-result-object v0

    const v3, 0x7f0e0129

    .line 606856
    iget-object v5, v0, LX/EN1;->a:LX/EN2;

    iput v3, v5, LX/EN2;->c:I

    .line 606857
    move-object v3, v0

    .line 606858
    if-nez v4, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v3, v0}, LX/EN1;->a(Landroid/view/View$OnClickListener;)LX/EN1;

    .line 606859
    invoke-direct {p0, v2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 606860
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0822a6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 606861
    iget-object v2, v1, LX/EN1;->a:LX/EN2;

    iput-object v0, v2, LX/EN2;->d:Ljava/lang/String;

    .line 606862
    move-object v0, v1

    .line 606863
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->i:LX/EPK;

    const/4 v3, 0x1

    invoke-virtual {v2, p2, p3, v3}, LX/EPK;->a(LX/CzL;LX/Cxi;Z)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 606864
    iget-object v3, v0, LX/EN1;->a:LX/EN2;

    iput-object v2, v3, LX/EN2;->f:Landroid/view/View$OnClickListener;

    .line 606865
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, LX/EMz;->a:LX/1X6;

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-virtual {v0, p1, p3, v2, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 606866
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->i:LX/EPK;

    const/4 v4, 0x0

    invoke-virtual {v0, p2, p3, v4}, LX/EPK;->a(LX/CzL;LX/Cxi;Z)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_2

    .line 606867
    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 606868
    :cond_3
    if-eqz v5, :cond_4

    .line 606869
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, LX/1nm;->h(I)LX/1nm;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    goto :goto_1

    .line 606870
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;
    .locals 11

    .prologue
    .line 606834
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;

    monitor-enter v1

    .line 606835
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 606836
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 606837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 606839
    new-instance v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/EN3;->a(LX/0QB;)LX/EN3;

    move-result-object v7

    check-cast v7, LX/EN3;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v9

    check-cast v9, LX/1vg;

    invoke-static {v0}, LX/EPK;->b(LX/0QB;)LX/EPK;

    move-result-object v10

    check-cast v10, LX/EPK;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/0Uh;LX/EN3;LX/1V0;LX/1vg;LX/EPK;)V

    .line 606840
    move-object v0, v3

    .line 606841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 606842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 606844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z
    .locals 3
    .param p1    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 606833
    if-eqz p1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->d:LX/0ad;

    sget-short v2, LX/100;->G:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private b(LX/CzL;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 606828
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 606829
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v5

    .line 606830
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->d:LX/0ad;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->e:LX/0Uh;

    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 606831
    iget-object v3, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 606832
    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-interface {v5}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5}, LX/8dH;->fn_()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->e:LX/0Uh;

    sget v1, LX/2SU;->f:I

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 606827
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cxi;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 606826
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cxi;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 606820
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 606821
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 606822
    if-eqz v0, :cond_0

    .line 606823
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 606824
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 606825
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;->b(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 606819
    const/4 v0, 0x0

    return-object v0
.end method
