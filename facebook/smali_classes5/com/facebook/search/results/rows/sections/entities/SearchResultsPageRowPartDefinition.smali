.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/ELj;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field public final j:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607411
    const v0, 0x7f030e40

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/ELj;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607400
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607401
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->b:LX/0ad;

    .line 607402
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->c:LX/ELj;

    .line 607403
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607404
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607405
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    .line 607406
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607407
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607408
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607409
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->j:LX/0Uh;

    .line 607410
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;
    .locals 13

    .prologue
    .line 607389
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    monitor-enter v1

    .line 607390
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607391
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607392
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607393
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607394
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/ELj;->b(LX/0QB;)LX/ELj;

    move-result-object v5

    check-cast v5, LX/ELj;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;-><init>(LX/0ad;LX/ELj;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/0Uh;)V

    .line 607395
    move-object v0, v3

    .line 607396
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607397
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607398
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607412
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 607375
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    const/4 v7, 0x0

    .line 607376
    move-object v0, p3

    check-cast v0, LX/1Ps;

    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    move-object v1, v0

    .line 607377
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607378
    const v2, 0x7f0d1286

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->c:LX/ELj;

    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v5

    iget-object v6, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, p3

    check-cast v0, LX/Cxd;

    invoke-virtual {v4, v5, v6, v0}, LX/ELj;->a(Lcom/facebook/graphql/model/GraphQLPageCallToAction;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)LX/EMF;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607379
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607380
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->j:LX/0Uh;

    invoke-static {v2}, LX/ELM;->a(LX/0Uh;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 607381
    const/4 v2, 0x0

    invoke-static {p2, v2}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v2

    .line 607382
    :goto_1
    move-object v2, v2

    .line 607383
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607384
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->j:LX/0Uh;

    invoke-static {p2, v2}, LX/ELM;->a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607385
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607386
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/3ap;->a:LX/1Ua;

    invoke-direct {v2, v7, v3, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607387
    return-object v7

    .line 607388
    :cond_0
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0822b2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 607373
    check-cast p1, LX/EJZ;

    const/4 v0, 0x0

    .line 607374
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->b:LX/0ad;

    sget-short v2, LX/100;->bs:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
