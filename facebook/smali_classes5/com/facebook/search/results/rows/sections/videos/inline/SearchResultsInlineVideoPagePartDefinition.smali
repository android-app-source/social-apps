.class public Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/EPt;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/EPt;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609852
    new-instance v0, LX/3bG;

    invoke-direct {v0}, LX/3bG;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609870
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609871
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    .line 609872
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->c:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    .line 609873
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;
    .locals 5

    .prologue
    .line 609859
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;

    monitor-enter v1

    .line 609860
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609861
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609862
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609863
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609864
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;)V

    .line 609865
    move-object v0, p0

    .line 609866
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609867
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609868
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609869
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/EPt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609858
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 609854
    check-cast p2, LX/CzL;

    .line 609855
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609856
    const v0, 0x7f0d177e

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->c:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    const-wide v2, 0x3ffc71c720000000L    # 1.7777777910232544

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609857
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609853
    const/4 v0, 0x1

    return v0
.end method
