.class public Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "TE;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609186
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 609187
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->d:LX/0Ot;

    .line 609188
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    .line 609189
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->f:LX/0Uh;

    .line 609190
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)LX/1X1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 609149
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EK7;

    invoke-virtual {v0, p1}, LX/EK7;->c(LX/1De;)LX/EK5;

    move-result-object v0

    .line 609150
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_3

    .line 609151
    :cond_0
    const/4 v1, 0x0

    .line 609152
    :goto_0
    move-object v1, v1

    .line 609153
    invoke-virtual {v0, v1}, LX/EK5;->c(Ljava/lang/String;)LX/EK5;

    move-result-object v0

    .line 609154
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 609155
    invoke-virtual {v0, v1}, LX/EK5;->a(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    .line 609156
    const/4 v3, 0x0

    .line 609157
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 609158
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 609159
    :goto_1
    iget-object v3, v0, LX/EPA;->c:LX/0ad;

    sget-short v5, LX/100;->Y:S

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 609160
    iget-object v3, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EJ5;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6, p1}, LX/EJ5;->b(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 609161
    :cond_1
    move-object v0, v4

    .line 609162
    invoke-virtual {v1, v0}, LX/EK5;->b(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    .line 609163
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, v0, LX/EPA;->c:LX/0ad;

    sget-short v3, LX/100;->W:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 609164
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    .line 609165
    :goto_2
    move-object v0, v2

    .line 609166
    invoke-virtual {v1, v0}, LX/EK5;->b(Ljava/lang/String;)LX/EK5;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 609167
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 609168
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    .line 609169
    :goto_3
    move v1, v1

    .line 609170
    invoke-virtual {v0, v1}, LX/EK5;->h(I)LX/EK5;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    move-object v1, p3

    check-cast v1, LX/1Ps;

    const/4 v4, 0x0

    .line 609171
    iget-object v3, v0, LX/EPA;->c:LX/0ad;

    sget-short v5, LX/100;->Y:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {v1}, LX/EPA;->a(LX/1Ps;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 609172
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 609173
    :goto_4
    move-object v0, v3

    .line 609174
    invoke-virtual {v2, v0}, LX/EK5;->c(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    move-object v1, p3

    check-cast v1, LX/Cxe;

    .line 609175
    new-instance v3, LX/EP9;

    invoke-direct {v3, v0, v1, p2}, LX/EP9;-><init>(LX/EPA;LX/Cxe;Lcom/facebook/graphql/model/GraphQLNode;)V

    move-object v0, v3

    .line 609176
    invoke-virtual {v2, v0}, LX/EK5;->a(Landroid/view/View$OnClickListener;)LX/EK5;

    move-result-object v0

    const v1, 0x7f0208ca

    invoke-virtual {v0, v1}, LX/EK5;->n(I)LX/EK5;

    move-result-object v1

    .line 609177
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p3, v1}, LX/EPA;->a(LX/1Ps;LX/EK5;)V

    .line 609178
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 609179
    const v0, 0x7f02077d

    invoke-virtual {v1, v0}, LX/EK5;->i(I)LX/EK5;

    .line 609180
    :cond_2
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 609181
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 609182
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/EJ5;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_1

    :cond_5
    move-object v4, v3

    goto/16 :goto_1

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 609183
    :cond_8
    iget-object v3, v0, LX/EPA;->c:LX/0ad;

    sget-short v5, LX/100;->Y:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {v1}, LX/EPA;->a(LX/1Ps;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 609184
    const/4 v3, 0x0

    goto :goto_4

    .line 609185
    :cond_9
    iget-object v3, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EJ5;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v5

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;->a()I

    move-result v4

    :cond_a
    invoke-virtual {v3, v5, v6, v4, p1}, LX/EJ5;->a(JILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;
    .locals 7

    .prologue
    .line 609133
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    monitor-enter v1

    .line 609134
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609135
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609138
    new-instance v5, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x33bc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0x3480

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 609139
    move-object v0, v5

    .line 609140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 609148
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 609147
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 609146
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->f:LX/0Uh;

    sget v1, LX/2SU;->ab:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 609145
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 609144
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
