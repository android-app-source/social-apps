.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:LX/0Uh;

.field private final e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:LX/EPK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 607048
    new-instance v0, LX/EPL;

    invoke-direct {v0}, LX/EPL;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a:LX/1Cz;

    .line 607049
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 607050
    iput v1, v0, LX/1UY;->b:F

    .line 607051
    move-object v0, v0

    .line 607052
    const/high16 v1, 0x40a00000    # 5.0f

    .line 607053
    iput v1, v0, LX/1UY;->c:F

    .line 607054
    move-object v0, v0

    .line 607055
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EPK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607041
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607042
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->c:LX/0ad;

    .line 607043
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->d:LX/0Uh;

    .line 607044
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 607045
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 607046
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->g:LX/EPK;

    .line 607047
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .locals 9

    .prologue
    .line 607056
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    monitor-enter v1

    .line 607057
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607058
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607059
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607060
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607061
    new-instance v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/EPK;->b(LX/0QB;)LX/EPK;

    move-result-object v8

    check-cast v8, LX/EPK;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;-><init>(LX/0ad;LX/0Uh;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EPK;)V

    .line 607062
    move-object v0, v3

    .line 607063
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607064
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607065
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607040
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607036
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v4, 0x0

    .line 607037
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->g:LX/EPK;

    invoke-virtual {v1, p2, p3}, LX/EPK;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607038
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607039
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 607030
    check-cast p1, LX/CzL;

    .line 607031
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 607032
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v5

    .line 607033
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->c:LX/0ad;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->d:LX/0Uh;

    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 607034
    iget-object v3, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 607035
    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-interface {v5}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5}, LX/8dH;->fn_()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
