.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

.field private final d:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/23r;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607758
    const v0, 0x7f031586

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/0Ot;LX/23r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;",
            "Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/23r;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607748
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607749
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 607750
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    .line 607751
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607752
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    .line 607753
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607754
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607755
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->h:LX/0Ot;

    .line 607756
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->i:LX/23r;

    .line 607757
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;
    .locals 12

    .prologue
    .line 607759
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;

    monitor-enter v1

    .line 607760
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607761
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607762
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607763
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607764
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    const/16 v10, 0x848

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/23r;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/23r;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/0Ot;LX/23r;)V

    .line 607765
    move-object v0, v3

    .line 607766
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607767
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607768
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607769
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607747
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 607692
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 607693
    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v0

    invoke-interface {v0}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v0

    .line 607694
    :goto_0
    new-instance v1, LX/3Qv;

    invoke-direct {v1}, LX/3Qv;-><init>()V

    const/4 v3, 0x0

    .line 607695
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 607696
    invoke-static {v4}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v6

    .line 607697
    if-eqz v4, :cond_0

    if-nez v6, :cond_2

    .line 607698
    :cond_0
    :goto_1
    move-object v3, v3

    .line 607699
    iput-object v3, v1, LX/3Qv;->k:LX/D4s;

    .line 607700
    move-object v1, v1

    .line 607701
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v1

    const-string v3, "SEARCH"

    .line 607702
    iput-object v3, v1, LX/3Qv;->d:Ljava/lang/String;

    .line 607703
    move-object v1, v1

    .line 607704
    sget-object v3, LX/04g;->BY_USER:LX/04g;

    .line 607705
    iput-object v3, v1, LX/3Qv;->h:LX/04g;

    .line 607706
    move-object v1, v1

    .line 607707
    sget-object v3, LX/04D;->SEARCH_RESULTS:LX/04D;

    .line 607708
    iput-object v3, v1, LX/3Qv;->g:LX/04D;

    .line 607709
    move-object v1, v1

    .line 607710
    iput-object v0, v1, LX/3Qv;->m:Ljava/lang/String;

    .line 607711
    move-object v0, v1

    .line 607712
    sget-object v1, LX/0JG;->SEARCH_RESULTS:LX/0JG;

    .line 607713
    iput-object v1, v0, LX/3Qv;->l:LX/0JG;

    .line 607714
    move-object v0, v0

    .line 607715
    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v1

    .line 607716
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607717
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v4

    invoke-static {v3, v4, v5}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607718
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v4

    invoke-static {v3, v4, v5}, LX/ELM;->b(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607719
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-static {v3}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607720
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    sget-object v3, LX/3ap;->b:LX/1X6;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607721
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->d:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    new-instance v3, LX/D4q;

    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v3, v1, v4, v2}, LX/D4q;-><init>(LX/3Qw;Ljava/util/concurrent/atomic/AtomicReference;LX/D6L;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607722
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1Nt;

    new-instance v7, LX/3FZ;

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->i:LX/23r;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    .line 607723
    new-instance v1, LX/EPi;

    invoke-direct {v1, p0, p3, p2, v0}, LX/EPi;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;LX/1Pn;Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;LX/3Qx;)V

    move-object v0, v1

    .line 607724
    invoke-direct {v7, v0}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v6, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607725
    return-object v2

    :cond_1
    move-object v0, v2

    .line 607726
    goto/16 :goto_0

    .line 607727
    :cond_2
    invoke-static {p2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 607728
    new-instance v8, LX/D4r;

    invoke-direct {v8}, LX/D4r;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v9

    .line 607729
    iput-object v9, v8, LX/D4r;->c:Ljava/lang/String;

    .line 607730
    move-object v8, v8

    .line 607731
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 607732
    :cond_3
    iput-object v3, v8, LX/D4r;->d:Ljava/lang/String;

    .line 607733
    move-object v3, v8

    .line 607734
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 607735
    iput-object v6, v3, LX/D4r;->f:Ljava/lang/String;

    .line 607736
    move-object v3, v3

    .line 607737
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    .line 607738
    iput-object v6, v3, LX/D4r;->a:Ljava/lang/String;

    .line 607739
    move-object v3, v3

    .line 607740
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->aW()Z

    move-result v6

    .line 607741
    iput-boolean v6, v3, LX/D4r;->i:Z

    .line 607742
    move-object v3, v3

    .line 607743
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->lo()Z

    move-result v4

    .line 607744
    iput-boolean v4, v3, LX/D4r;->g:Z

    .line 607745
    move-object v3, v3

    .line 607746
    invoke-virtual {v3}, LX/D4r;->a()LX/D4s;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607690
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 607691
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
