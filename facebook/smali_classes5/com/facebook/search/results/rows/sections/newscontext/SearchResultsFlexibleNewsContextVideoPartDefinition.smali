.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/8dK;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

.field private final d:LX/19B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609011
    new-instance v0, LX/3b3;

    invoke-direct {v0}, LX/3b3;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/19B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609006
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609007
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    .line 609008
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    .line 609009
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->d:LX/19B;

    .line 609010
    return-void
.end method

.method private static b(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "LX/8dK;",
            ">;)",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 608994
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 608995
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    .line 608996
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 608997
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    .line 608998
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fq_()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fq_()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 608999
    :goto_1
    return-object v0

    .line 609000
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 609001
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    goto :goto_0

    .line 609002
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fq_()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 609003
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 609004
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 609005
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608971
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 608977
    check-cast p2, LX/CzL;

    .line 608978
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v1

    .line 608979
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608980
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->d:LX/19B;

    invoke-virtual {v0}, LX/19B;->a()Landroid/graphics/Point;

    move-result-object v2

    .line 608981
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608982
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 608983
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 608984
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v0

    int-to-float v3, v0

    .line 608985
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608986
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 608987
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 608988
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v0

    int-to-float v0, v0

    div-float v1, v3, v0

    .line 608989
    iget v0, v2, Landroid/graphics/Point;->y:I

    iget v3, v2, Landroid/graphics/Point;->x:I

    if-ge v0, v3, :cond_0

    iget v0, v2, Landroid/graphics/Point;->y:I

    .line 608990
    :goto_0
    int-to-float v2, v0

    div-float v1, v2, v1

    float-to-int v1, v1

    .line 608991
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    new-instance v3, LX/24k;

    invoke-direct {v3, v0, v1}, LX/24k;-><init>(II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608992
    const/4 v0, 0x0

    return-object v0

    .line 608993
    :cond_0
    iget v0, v2, Landroid/graphics/Point;->x:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608972
    check-cast p1, LX/CzL;

    .line 608973
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v0

    .line 608974
    if-eqz v0, :cond_0

    .line 608975
    const/4 v0, 0x1

    move v0, v0

    .line 608976
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
