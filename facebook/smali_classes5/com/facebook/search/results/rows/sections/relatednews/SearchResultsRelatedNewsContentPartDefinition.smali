.class public Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608627
    new-instance v0, LX/3ax;

    invoke-direct {v0}, LX/3ax;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->a:LX/1Cz;

    .line 608628
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 608629
    iput v1, v0, LX/1UY;->b:F

    .line 608630
    move-object v0, v0

    .line 608631
    const/high16 v1, -0x40000000    # -2.0f

    .line 608632
    iput v1, v0, LX/1UY;->c:F

    .line 608633
    move-object v0, v0

    .line 608634
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608635
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608636
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    .line 608637
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->d:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    .line 608638
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608639
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;
    .locals 6

    .prologue
    .line 608640
    const-class v1, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    monitor-enter v1

    .line 608641
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608642
    sput-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608643
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608644
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608645
    new-instance p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 608646
    move-object v0, p0

    .line 608647
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608648
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608649
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 608651
    sget-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 608652
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 608653
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    .line 608654
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v1

    const-string v2, "Insufficient data to bind provided node: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p2, v3, p3

    invoke-static {v1, v2, v3}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 608655
    new-instance v2, LX/CzK;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ki()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->kh()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, LX/CzK;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 608656
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608657
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608658
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->d:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608659
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608660
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method
