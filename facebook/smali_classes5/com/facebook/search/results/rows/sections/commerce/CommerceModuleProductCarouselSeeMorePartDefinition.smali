.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:LX/EPK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610639
    new-instance v0, LX/3bN;

    invoke-direct {v0}, LX/3bN;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EPK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610635
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610636
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 610637
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->c:LX/EPK;

    .line 610638
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;
    .locals 5

    .prologue
    .line 610624
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    monitor-enter v1

    .line 610625
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610626
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610627
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610628
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610629
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/EPK;->b(LX/0QB;)LX/EPK;

    move-result-object v4

    check-cast v4, LX/EPK;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EPK;)V

    .line 610630
    move-object v0, p0

    .line 610631
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610632
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610633
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610634
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610640
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 610621
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    .line 610622
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->c:LX/EPK;

    invoke-virtual {v1, p2, p3}, LX/EPK;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610623
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610620
    const/4 v0, 0x1

    return v0
.end method
