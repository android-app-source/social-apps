.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ":",
        "LX/Cxc;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fig/listitem/FigListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

.field private final g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final h:LX/0ad;

.field public final i:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607413
    const v0, 0x7f030491

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607414
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607415
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    .line 607416
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    .line 607417
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    .line 607418
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->e:Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;

    .line 607419
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->f:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    .line 607420
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607421
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->h:LX/0ad;

    .line 607422
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->i:LX/0Uh;

    .line 607423
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;
    .locals 12

    .prologue
    .line 607424
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    monitor-enter v1

    .line 607425
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607426
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607427
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607428
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607429
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0ad;LX/0Uh;)V

    .line 607430
    move-object v0, v3

    .line 607431
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607432
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607433
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fig/listitem/FigListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607435
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 607436
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 607437
    move-object v0, p3

    check-cast v0, LX/1Ps;

    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    .line 607438
    if-nez v0, :cond_1

    .line 607439
    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    .line 607440
    :goto_0
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->g(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    :goto_1
    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607441
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    .line 607442
    move-object v2, p3

    check-cast v2, LX/Cxc;

    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v2, v3}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-static {v2}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->i:LX/0Uh;

    invoke-static {v2}, LX/ELM;->a(LX/0Uh;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 607443
    :cond_0
    const/4 v2, 0x0

    invoke-static {p2, v2}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v2

    .line 607444
    :goto_2
    move-object v2, v2

    .line 607445
    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607446
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->e:Lcom/facebook/multirow/parts/ImageBlockLayoutContentDescriptionPartDefinition;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v2}, LX/ELM;->a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607447
    new-instance v1, LX/8Cq;

    invoke-direct {v1}, LX/8Cq;-><init>()V

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8Cq;->a(Landroid/net/Uri;)LX/8Cq;

    move-result-object v1

    invoke-virtual {v1}, LX/8Cq;->a()LX/8Cr;

    move-result-object v1

    .line 607448
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->f:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607449
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->g:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/3ap;->a:LX/1Ua;

    invoke-direct {v2, v4, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607450
    return-object v4

    .line 607451
    :cond_1
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_0

    .line 607452
    :cond_2
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    goto :goto_1

    :cond_3
    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f0822b2

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x56e70f07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 607453
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x1

    .line 607454
    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 607455
    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyMaxLines(I)V

    .line 607456
    const/16 v1, 0x1f

    const v2, 0x6bb10247

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607457
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 607458
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 607459
    invoke-virtual {p4}, Lcom/facebook/fig/listitem/FigListItem;->d()V

    .line 607460
    return-void
.end method
