.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/A5I;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1X6;

.field private static final c:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 610930
    const v0, 0x7f030260

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->a:LX/1Cz;

    .line 610931
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ec00000    # -12.0f

    .line 610932
    iput v1, v0, LX/1UY;->b:F

    .line 610933
    move-object v0, v0

    .line 610934
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->c:LX/1Ua;

    .line 610935
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->c:LX/1Ua;

    sget-object v3, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->b:LX/1X6;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610936
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610937
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    .line 610938
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610939
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;
    .locals 5

    .prologue
    .line 610940
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    monitor-enter v1

    .line 610941
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610942
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610943
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610944
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610945
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610946
    move-object v0, p0

    .line 610947
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610948
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610949
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610950
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610951
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 610952
    check-cast p2, LX/CzL;

    .line 610953
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610954
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610955
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610956
    check-cast p1, LX/CzL;

    .line 610957
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610958
    check-cast v0, LX/A5I;

    invoke-interface {v0}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
