.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;",
        "Ljava/lang/Void;",
        "LX/CxV;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static final c:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 607186
    new-instance v0, LX/3al;

    invoke-direct {v0}, LX/3al;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a:LX/1Cz;

    .line 607187
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 607188
    iput v1, v0, LX/1UY;->b:F

    .line 607189
    move-object v0, v0

    .line 607190
    const/high16 v1, -0x3f800000    # -4.0f

    .line 607191
    iput v1, v0, LX/1UY;->c:F

    .line 607192
    move-object v0, v0

    .line 607193
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->b:LX/1Ua;

    .line 607194
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f600000    # -5.0f

    .line 607195
    iput v1, v0, LX/1UY;->b:F

    .line 607196
    move-object v0, v0

    .line 607197
    const/high16 v1, -0x40800000    # -1.0f

    .line 607198
    iput v1, v0, LX/1UY;->c:F

    .line 607199
    move-object v0, v0

    .line 607200
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607182
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607183
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 607184
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607185
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;
    .locals 5

    .prologue
    .line 607171
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    monitor-enter v1

    .line 607172
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607173
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607174
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607175
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607176
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 607177
    move-object v0, p0

    .line 607178
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607179
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607180
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607170
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607160
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    check-cast p3, LX/CxV;

    .line 607161
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 607162
    iget-object v1, p2, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 607163
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607164
    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->c:LX/1Ua;

    .line 607165
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v2, LX/3aw;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v2, p2, v0, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607166
    const/4 v0, 0x0

    return-object v0

    .line 607167
    :cond_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->b:LX/1Ua;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607169
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 607168
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    return-object v0
.end method
