.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field public final c:LX/17W;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40c00000    # 6.0f

    .line 607282
    new-instance v0, LX/3ao;

    invoke-direct {v0}, LX/3ao;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a:LX/1Cz;

    .line 607283
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 607284
    iput v1, v0, LX/1UY;->b:F

    .line 607285
    move-object v0, v0

    .line 607286
    iput v1, v0, LX/1UY;->c:F

    .line 607287
    move-object v0, v0

    .line 607288
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/17W;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607289
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607290
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->c:LX/17W;

    .line 607291
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607292
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 607293
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    .line 607294
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    .line 607295
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 607296
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;
    .locals 10

    .prologue
    .line 607297
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    monitor-enter v1

    .line 607298
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607299
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607302
    new-instance v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;-><init>(LX/17W;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 607303
    move-object v0, v3

    .line 607304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 607308
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x1eaef984

    if-eq v1, v2, :cond_1

    .line 607309
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607310
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 607311
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 607312
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607313
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607314
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607315
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 607316
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 607317
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607318
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJW;

    invoke-direct {v1, p0, p2, p3}, LX/EJW;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607319
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607320
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method
