.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextIconPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610076
    const v0, 0x7f0312c2

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610105
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610106
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 610107
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->c:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    .line 610108
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610109
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;
    .locals 6

    .prologue
    .line 610094
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;

    monitor-enter v1

    .line 610095
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610096
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610097
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610098
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610099
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextIconPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610100
    move-object v0, p0

    .line 610101
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610102
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610103
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610104
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610093
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 610078
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    .line 610079
    sget-object v1, LX/103;->GROUP:LX/103;

    .line 610080
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 610081
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;

    .line 610082
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;->b:LX/103;

    move-object v0, v2

    .line 610083
    invoke-virtual {v1, v0}, LX/103;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0822aa

    move v1, v0

    .line 610084
    :goto_0
    const v2, 0x7f0d2bad

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 610085
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 610086
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;

    .line 610087
    iget-object p3, v0, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;->a:Ljava/lang/String;

    move-object v0, p3

    .line 610088
    aput-object v0, v5, v6

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 610089
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->c:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    new-instance v1, LX/8Ct;

    const v2, 0x7f0217dd

    const v3, -0xc4a668

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0b0065

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/8Ct;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610090
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610091
    const/4 v0, 0x0

    return-object v0

    .line 610092
    :cond_0
    const v0, 0x7f0822a7

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610077
    const/4 v0, 0x1

    return v0
.end method
