.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;>;",
        "LX/EJo;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/7j6;

.field public final e:LX/0hy;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610689
    const-class v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    const-string v1, "places_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 610690
    const v0, 0x7f03128e

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610691
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610692
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610693
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->d:LX/7j6;

    .line 610694
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->e:LX/0hy;

    .line 610695
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 610696
    return-void
.end method

.method private a(LX/CzL;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 610697
    new-instance v0, LX/EJn;

    invoke-direct {v0, p0, p1, p2}, LX/EJn;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;LX/CzL;LX/1Ps;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;
    .locals 7

    .prologue
    .line 610698
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    monitor-enter v1

    .line 610699
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610700
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610703
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v4

    check-cast v4, LX/7j6;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 610704
    move-object v0, p0

    .line 610705
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610706
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610707
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;LX/CzL;Landroid/view/View$OnClickListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 610709
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610710
    check-cast v0, LX/A2T;

    .line 610711
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/CzL;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610712
    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 610713
    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    .line 610714
    :goto_0
    invoke-virtual {p0, p2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610715
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    .line 610716
    return-void

    .line 610717
    :cond_0
    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610718
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 610719
    check-cast p2, LX/0Px;

    check-cast p3, LX/1Ps;

    .line 610720
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610721
    new-instance v1, LX/EJo;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CzL;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(LX/CzL;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v2

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CzL;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(LX/CzL;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CzL;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(LX/CzL;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/EJo;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4882ab03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610722
    check-cast p1, LX/0Px;

    check-cast p2, LX/EJo;

    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;

    .line 610723
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v2, v1

    .line 610724
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v4, v1

    .line 610725
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object p0, v1

    .line 610726
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CzL;

    iget-object p3, p2, LX/EJo;->a:Landroid/view/View$OnClickListener;

    invoke-static {v2, v1, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;LX/CzL;Landroid/view/View$OnClickListener;)V

    .line 610727
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CzL;

    iget-object v2, p2, LX/EJo;->b:Landroid/view/View$OnClickListener;

    invoke-static {v4, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;LX/CzL;Landroid/view/View$OnClickListener;)V

    .line 610728
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CzL;

    iget-object v2, p2, LX/EJo;->c:Landroid/view/View$OnClickListener;

    invoke-static {p0, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;LX/CzL;Landroid/view/View$OnClickListener;)V

    .line 610729
    const/16 v1, 0x1f

    const v2, -0x2f1e055a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 610730
    check-cast p1, LX/0Px;

    .line 610731
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 610732
    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;

    const/4 v1, 0x0

    .line 610733
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610734
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610735
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610736
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610737
    return-void
.end method
