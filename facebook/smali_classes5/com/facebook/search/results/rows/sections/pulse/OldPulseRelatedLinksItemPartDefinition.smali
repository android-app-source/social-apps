.class public Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static j:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final i:LX/EJ5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609124
    new-instance v0, LX/3b6;

    invoke-direct {v0}, LX/3b6;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a:LX/1Cz;

    .line 609125
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 609126
    iput v1, v0, LX/1UY;->c:F

    .line 609127
    move-object v0, v0

    .line 609128
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EJ5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609115
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609116
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609117
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 609118
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    .line 609119
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    .line 609120
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    .line 609121
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609122
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->i:LX/EJ5;

    .line 609123
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;
    .locals 11

    .prologue
    .line 609104
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    monitor-enter v1

    .line 609105
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609106
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609107
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609108
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609109
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/EJ5;->a(LX/0QB;)LX/EJ5;

    move-result-object v10

    check-cast v10, LX/EJ5;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/EJ5;)V

    .line 609110
    move-object v0, v3

    .line 609111
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609112
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609113
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 609129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x1eaef984

    if-eq v1, v2, :cond_1

    .line 609130
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 609103
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 609093
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 609094
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609095
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609096
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609097
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609098
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 609099
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 609100
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609101
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EOk;

    invoke-direct {v1, p0, p3, p2}, LX/EOk;-><init>(Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;LX/1Pn;Lcom/facebook/graphql/model/GraphQLNode;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609102
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609092
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method
