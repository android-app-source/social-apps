.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ":",
        "LX/Cxc;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field public final i:LX/Bnh;

.field private final j:LX/0ad;

.field public final k:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607323
    const v0, 0x7f03048f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/Bnh;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607324
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607325
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607326
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607327
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;

    .line 607328
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607329
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607330
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607331
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607332
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->i:LX/Bnh;

    .line 607333
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->j:LX/0ad;

    .line 607334
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->k:LX/0Uh;

    .line 607335
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;
    .locals 14

    .prologue
    .line 607336
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    monitor-enter v1

    .line 607337
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607338
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607339
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607340
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607341
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607342
    new-instance v12, LX/Bnh;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v11

    check-cast v11, LX/6RZ;

    invoke-direct {v12, v11}, LX/Bnh;-><init>(LX/6RZ;)V

    .line 607343
    move-object v11, v12

    .line 607344
    check-cast v11, LX/Bnh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;LX/Bnh;LX/0ad;LX/0Uh;)V

    .line 607345
    move-object v0, v3

    .line 607346
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607347
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607348
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607350
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 607351
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 607352
    move-object v0, p3

    check-cast v0, LX/1Ps;

    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    .line 607353
    :goto_0
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->g(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/OldNavigationalLinkPartDefinition;

    :goto_1
    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607354
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    const v3, 0x403827a

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->j:LX/0ad;

    sget-short v3, LX/100;->i:S

    invoke-interface {v1, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 607355
    :goto_2
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607356
    move-object v3, p3

    check-cast v3, LX/Cxc;

    iget-object v5, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v3, v5}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-static {v3}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->k:LX/0Uh;

    invoke-static {v3}, LX/ELM;->a(LX/0Uh;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 607357
    :cond_0
    const/4 v3, 0x0

    invoke-static {p2, v3}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v3

    .line 607358
    :goto_3
    move-object v3, v3

    .line 607359
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607360
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607361
    check-cast p3, LX/Cxc;

    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3, v3}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-static {v3}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->k:LX/0Uh;

    invoke-static {v3}, LX/ELM;->a(LX/0Uh;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 607362
    :cond_1
    const/4 v3, 0x1

    invoke-static {p2, v3}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v3

    .line 607363
    :goto_4
    move-object v1, v3

    .line 607364
    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607365
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->k:LX/0Uh;

    invoke-static {p2, v2}, LX/ELM;->a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607366
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607367
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/3ap;->a:LX/1Ua;

    invoke-direct {v2, v4, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607368
    return-object v4

    .line 607369
    :cond_2
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto/16 :goto_0

    .line 607370
    :cond_3
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    goto/16 :goto_1

    :cond_4
    move v1, v2

    .line 607371
    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->i:LX/Bnh;

    iget-object v5, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3, v5}, LX/Bnh;->b(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_6
    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0822b2

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_3

    :cond_7
    if-eqz v1, :cond_8

    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v3}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_8
    const/4 v3, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607372
    const/4 v0, 0x1

    return v0
.end method
