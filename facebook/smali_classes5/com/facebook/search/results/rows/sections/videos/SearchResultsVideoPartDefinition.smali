.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A4u;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1Ua;

.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static m:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final g:LX/8i7;

.field private final h:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

.field private final i:LX/23s;

.field private final j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition",
            "<TE;",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40400000    # 3.0f

    .line 608261
    const v0, 0x7f031343    # 1.7422888E38f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->a:LX/1Cz;

    .line 608262
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 608263
    iput v1, v0, LX/1UY;->b:F

    .line 608264
    move-object v0, v0

    .line 608265
    iput v1, v0, LX/1UY;->c:F

    .line 608266
    move-object v0, v0

    .line 608267
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->b:LX/1Ua;

    .line 608268
    const-class v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/8i7;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;LX/23s;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608269
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608270
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 608271
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 608272
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608273
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->g:LX/8i7;

    .line 608274
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->h:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    .line 608275
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->i:LX/23s;

    .line 608276
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    .line 608277
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->k:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    .line 608278
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->l:LX/0ad;

    .line 608279
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;
    .locals 13

    .prologue
    .line 608249
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;

    monitor-enter v1

    .line 608250
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608251
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608252
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608253
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608254
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v7

    check-cast v7, LX/8i7;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v9

    check-cast v9, LX/23s;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/8i7;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;LX/23s;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/0ad;)V

    .line 608255
    move-object v0, v3

    .line 608256
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608257
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608258
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608260
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 608188
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 608189
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->l:LX/0ad;

    invoke-static {v0}, LX/7Ax;->c(LX/0ad;)Z

    move-result v2

    .line 608190
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608191
    check-cast v0, LX/A4u;

    .line 608192
    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 608193
    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 608194
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608195
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608196
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 608197
    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v2, :cond_2

    invoke-interface {v0}, LX/A4u;->ap()Z

    move-result v1

    if-nez v1, :cond_2

    .line 608198
    :cond_1
    const v5, 0x7f0d2ba7

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v2, :cond_4

    .line 608199
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 608200
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->O()Z

    move-result v8

    if-eqz v8, :cond_8

    const v8, 0x7f080d4d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    move-object v1, v7

    .line 608201
    :goto_1
    invoke-interface {p1, v5, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608202
    :cond_2
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 608203
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 608204
    if-nez v5, :cond_9

    .line 608205
    const/4 v1, 0x0

    .line 608206
    :goto_2
    move-object v1, v1

    .line 608207
    if-eqz v1, :cond_3

    .line 608208
    const v5, 0x7f0d2ca0

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-interface {p1, v5, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608209
    :cond_3
    const v5, 0x7f0d02c4

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v2, :cond_5

    .line 608210
    invoke-static {v3}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 608211
    if-eqz v1, :cond_a

    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    :goto_3
    move-object v1, v1

    .line 608212
    :goto_4
    invoke-interface {p1, v5, v6, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608213
    const v1, 0x7f0d02c3

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v2, :cond_6

    .line 608214
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 608215
    if-nez v0, :cond_b

    .line 608216
    const/4 v0, 0x0

    .line 608217
    :goto_5
    move-object v0, v0

    .line 608218
    :goto_6
    invoke-interface {p1, v1, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608219
    const v0, 0x7f0d2bac

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->h:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608220
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608221
    check-cast v0, LX/A4u;

    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 608222
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 608223
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 608224
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    move-object v1, v0

    .line 608225
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->i:LX/23s;

    .line 608226
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608227
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v0

    .line 608228
    sget-object v2, LX/04H;->ELIGIBLE:LX/04H;

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    :goto_7
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608229
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608230
    const/4 v0, 0x0

    return-object v0

    .line 608231
    :cond_4
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 608232
    invoke-interface {v0}, LX/A4u;->ah()Z

    move-result v8

    if-eqz v8, :cond_d

    const v8, 0x7f080d4d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_8
    move-object v1, v7

    .line 608233
    goto/16 :goto_1

    .line 608234
    :cond_5
    invoke-interface {v0}, LX/A4u;->av()LX/175;

    move-result-object v1

    .line 608235
    if-eqz v1, :cond_e

    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    :goto_9
    move-object v1, v1

    .line 608236
    goto/16 :goto_4

    .line 608237
    :cond_6
    invoke-interface {v0}, LX/A4u;->aB()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v2

    .line 608238
    if-nez v2, :cond_f

    .line 608239
    const/4 v2, 0x0

    .line 608240
    :goto_a
    move-object v0, v2

    .line 608241
    goto/16 :goto_6

    .line 608242
    :cond_7
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->k:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    goto :goto_7

    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v7

    int-to-long v7, v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    invoke-static {v7, v8}, LX/D0O;->a(J)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    :cond_9
    new-instance v6, LX/2f8;

    invoke-direct {v6}, LX/2f8;-><init>()V

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v5

    sget-object v6, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 608243
    iput-object v6, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 608244
    move-object v5, v5

    .line 608245
    const v6, 0x7f0b0633

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f0b0634

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v5, v6, v1}, LX/2f8;->a(II)LX/2f8;

    move-result-object v1

    sget-object v5, LX/1Up;->h:LX/1Up;

    .line 608246
    iput-object v5, v1, LX/2f8;->i:LX/1Up;

    .line 608247
    move-object v1, v1

    .line 608248
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    goto/16 :goto_2

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->g:LX/8i7;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_d
    invoke-interface {v0}, LX/A4u;->aF()I

    move-result v7

    int-to-long v7, v7

    invoke-static {v7, v8}, LX/D0O;->a(J)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_8

    :cond_e
    const/4 v1, 0x0

    goto :goto_9

    :cond_f
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->b()Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->g:LX/8i7;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_a

    :cond_10
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2f6bac73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 608173
    check-cast p1, LX/CzL;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 608174
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->l:LX/0ad;

    invoke-static {v1}, LX/7Ax;->c(LX/0ad;)Z

    move-result v2

    .line 608175
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 608176
    check-cast v1, LX/A4u;

    invoke-interface {v1}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 608177
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 608178
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608179
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 608180
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    .line 608181
    if-nez v2, :cond_0

    .line 608182
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 608183
    check-cast v1, LX/A4u;

    invoke-interface {v1}, LX/A4u;->ap()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 608184
    :cond_1
    const v1, 0x7f0d2c9f

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomFrameLayout;

    .line 608185
    const v2, 0x7f0d2ba5

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 608186
    :cond_2
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x4594de44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 608187
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/widget/CustomFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const p2, 0x7f0312b5    # 1.74226E38f

    const/4 p0, 0x1

    invoke-virtual {v2, p2, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 608165
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 608166
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608167
    check-cast v0, LX/A4u;

    .line 608168
    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 608169
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 608170
    :goto_0
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->l:LX/0ad;

    sget-short v2, LX/100;->bk:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 608171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 608172
    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 608159
    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 608160
    const p0, 0x7f0d2c9f

    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/widget/CustomFrameLayout;

    .line 608161
    const p1, 0x7f0d2ba5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 608162
    if-eqz p1, :cond_0

    .line 608163
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->removeView(Landroid/view/View;)V

    .line 608164
    :cond_0
    return-void
.end method
