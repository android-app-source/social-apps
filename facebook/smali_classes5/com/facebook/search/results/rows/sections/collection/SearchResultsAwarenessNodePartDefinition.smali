.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/components/ComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/ComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610051
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610052
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610053
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->b:Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    .line 610054
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;
    .locals 4

    .prologue
    .line 610055
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;

    monitor-enter v1

    .line 610056
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610057
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610058
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610059
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610060
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;)V

    .line 610061
    move-object v0, p0

    .line 610062
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610063
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610064
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/ComponentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610066
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 610067
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 610068
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 610069
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;

    .line 610070
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->b:Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    new-instance v2, LX/EJG;

    .line 610071
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;->a:Ljava/lang/String;

    move-object v3, v3

    .line 610072
    iget-object p0, v0, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;->b:Ljava/lang/String;

    move-object v0, p0

    .line 610073
    invoke-direct {v2, v3, v0}, LX/EJG;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610074
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610075
    const/4 v0, 0x1

    return v0
.end method
