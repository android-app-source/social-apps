.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TT;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Ljava/lang/Object;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 607145
    const v0, 0x7f0312ac

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->a:LX/1Cz;

    .line 607146
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 607147
    iput v1, v0, LX/1UY;->b:F

    .line 607148
    move-object v0, v0

    .line 607149
    const/high16 v1, 0x41400000    # 12.0f

    .line 607150
    iput v1, v0, LX/1UY;->d:F

    .line 607151
    move-object v0, v0

    .line 607152
    const/high16 v1, -0x3f400000    # -6.0f

    .line 607153
    iput v1, v0, LX/1UY;->c:F

    .line 607154
    move-object v0, v0

    .line 607155
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607156
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607157
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 607158
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;
    .locals 4

    .prologue
    .line 607134
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    monitor-enter v1

    .line 607135
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607136
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607137
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607138
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607139
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V

    .line 607140
    move-object v0, p0

    .line 607141
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607142
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607143
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 607159
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 607131
    const/4 v5, 0x0

    .line 607132
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, v5, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607133
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 607129
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 607130
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    return-object v0
.end method
