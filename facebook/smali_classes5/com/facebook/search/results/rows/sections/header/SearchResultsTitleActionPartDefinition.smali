.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<*>;>;",
        "Landroid/graphics/drawable/Drawable;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0ad;

.field private final d:LX/0Uh;

.field private final e:LX/0wM;

.field private final f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPF;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 606807
    const v0, 0x7f0312bf

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0Uh;LX/0wM;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0wM;",
            "Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EPF;",
            ">;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 606794
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 606795
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->b:Landroid/content/Context;

    .line 606796
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->c:LX/0ad;

    .line 606797
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->d:LX/0Uh;

    .line 606798
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->e:LX/0wM;

    .line 606799
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 606800
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 606801
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->h:LX/0Ot;

    .line 606802
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->i:LX/0Ot;

    .line 606803
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->j:LX/0Ot;

    .line 606804
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->k:LX/0Ot;

    .line 606805
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->l:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 606806
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;
    .locals 15

    .prologue
    .line 606808
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    monitor-enter v1

    .line 606809
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 606810
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 606811
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606812
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 606813
    new-instance v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    const/16 v10, 0x33c3

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x33c2

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xe0f

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3488

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/0Uh;LX/0wM;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 606814
    move-object v0, v3

    .line 606815
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 606816
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606817
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 606818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 606789
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->c:LX/0ad;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->d:LX/0Uh;

    invoke-static {v1, v2, p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a(LX/0ad;LX/0Uh;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->d:LX/0Uh;

    sget v2, LX/2SU;->f:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 606790
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v1, v1

    .line 606791
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606792
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v1, v1

    .line 606793
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->c:LX/0ad;

    sget-short v2, LX/100;->G:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606788
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 606746
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 606747
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->l:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/EMz;->a:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606748
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606749
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 606750
    invoke-direct {p0, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v3

    .line 606751
    invoke-direct {p0, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v2

    .line 606752
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 606753
    iget-object v4, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v4, v4

    .line 606754
    const-string v5, ""

    invoke-virtual {v4, v5}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606755
    const v4, 0x7f0d0393

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->g:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v3, :cond_2

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f0822a6

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {p1, v4, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606756
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->c:LX/0ad;

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->d:LX/0Uh;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/BaseFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 606757
    iget-object v7, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v7, v7

    .line 606758
    invoke-static {v1, v4, v5, v6, v7}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0am;)LX/CyI;

    move-result-object v4

    .line 606759
    if-eqz v4, :cond_4

    .line 606760
    if-eqz v2, :cond_0

    .line 606761
    const v2, 0x7f0d2ba8

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v5, LX/EKB;

    invoke-direct {v5, v4, v0}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V

    invoke-interface {p1, v2, v1, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606762
    :cond_0
    if-eqz v3, :cond_1

    .line 606763
    if-eqz v3, :cond_3

    const v1, 0x7f0d0393

    move v2, v1

    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v3, LX/EKB;

    invoke-direct {v3, v4, v0}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V

    invoke-interface {p1, v2, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606764
    :cond_1
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 606765
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v2

    .line 606766
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/Integer;

    move-result-object v0

    .line 606767
    if-eqz v0, :cond_6

    sget-object v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 606768
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->e:LX/0wM;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 606769
    :goto_3
    return-object v0

    .line 606770
    :cond_2
    const-string v1, ""

    goto :goto_0

    .line 606771
    :cond_3
    const v1, 0x7f0d2ba8

    move v2, v1

    goto :goto_1

    .line 606772
    :cond_4
    if-eqz v2, :cond_5

    .line 606773
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->s()LX/0Px;

    .line 606774
    goto :goto_5

    .line 606775
    :cond_5
    :goto_4
    if-eqz v3, :cond_1

    .line 606776
    const v2, 0x7f0d0393

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v3, LX/EK9;

    sget-object v4, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a:LX/3ag;

    const v5, 0x7f0822b4

    const/4 v6, 0x1

    invoke-direct {v3, v0, v4, v5, v6}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;IZ)V

    invoke-interface {p1, v2, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_2

    .line 606777
    :goto_5
    const v2, 0x7f0d2ba8

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v4, LX/EK9;

    sget-object v5, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a:LX/3ag;

    const v6, 0x7f0822b4

    const/4 v7, 0x0

    invoke-direct {v4, v0, v5, v6, v7}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;IZ)V

    invoke-interface {p1, v2, v1, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_4

    .line 606778
    :cond_6
    if-eqz v0, :cond_7

    .line 606779
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_3

    .line 606780
    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6a2dc51d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 606785
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 606786
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 606787
    const/16 v1, 0x1f

    const v2, -0x8e9c219

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 606781
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 606782
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606783
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 606784
    invoke-direct {p0, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
