.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/Cxh;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8dB;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/ELm;

.field private final f:LX/1V0;

.field private final g:LX/8hu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40800000    # 4.0f

    .line 608579
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 608580
    iput v1, v0, LX/1UY;->b:F

    .line 608581
    move-object v0, v0

    .line 608582
    iput v1, v0, LX/1UY;->c:F

    .line 608583
    move-object v0, v0

    .line 608584
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/ELm;LX/1V0;LX/8hu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608574
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 608575
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->e:LX/ELm;

    .line 608576
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->f:LX/1V0;

    .line 608577
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->g:LX/8hu;

    .line 608578
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 608508
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608509
    check-cast v0, LX/8dB;

    .line 608510
    invoke-interface {v0}, LX/8dB;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v2, v3

    .line 608511
    :goto_0
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 608512
    invoke-static {v1}, LX/ELM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 608513
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->g:LX/8hu;

    .line 608514
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 608515
    invoke-interface {v0}, LX/8dB;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v4

    .line 608516
    invoke-interface {v0}, LX/8dB;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 608517
    invoke-interface {v0}, LX/8dB;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 608518
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 608519
    invoke-static {v1, p1, v4}, LX/8hu;->a(LX/8hu;Landroid/content/Context;Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v6, v4}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608520
    invoke-interface {v0}, LX/8dB;->J()LX/0Px;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v6, v4}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608521
    invoke-interface {v0}, LX/8dB;->I()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608522
    move-object v6, v6

    .line 608523
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->g:LX/8hu;

    invoke-virtual {v1, v0}, LX/8hu;->a(LX/8dB;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 608524
    invoke-static {p1, v0}, LX/8hu;->a(Landroid/content/Context;LX/8dB;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 608525
    invoke-interface {v0}, LX/8dB;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    move v4, v1

    .line 608526
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->e:LX/ELm;

    const/4 v9, 0x0

    .line 608527
    new-instance v10, LX/ELl;

    invoke-direct {v10, v1}, LX/ELl;-><init>(LX/ELm;)V

    .line 608528
    iget-object v11, v1, LX/ELm;->b:LX/0Zi;

    invoke-virtual {v11}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/ELk;

    .line 608529
    if-nez v11, :cond_0

    .line 608530
    new-instance v11, LX/ELk;

    invoke-direct {v11, v1}, LX/ELk;-><init>(LX/ELm;)V

    .line 608531
    :cond_0
    invoke-static {v11, p1, v9, v9, v10}, LX/ELk;->a$redex0(LX/ELk;LX/1De;IILX/ELl;)V

    .line 608532
    move-object v10, v11

    .line 608533
    move-object v9, v10

    .line 608534
    move-object v1, v9

    .line 608535
    iget-object v9, v1, LX/ELk;->a:LX/ELl;

    iput-object p2, v9, LX/ELl;->b:LX/CzL;

    .line 608536
    iget-object v9, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 608537
    move-object v9, v1

    .line 608538
    move-object v1, p3

    check-cast v1, LX/CxA;

    .line 608539
    iget-object v10, v9, LX/ELk;->a:LX/ELl;

    iput-object v1, v10, LX/ELl;->a:LX/CxA;

    .line 608540
    iget-object v10, v9, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/BitSet;->set(I)V

    .line 608541
    move-object v1, v9

    .line 608542
    invoke-interface {v0}, LX/8dB;->dW_()Ljava/lang/String;

    move-result-object v9

    .line 608543
    iget-object v10, v1, LX/ELk;->a:LX/ELl;

    iput-object v9, v10, LX/ELl;->c:Ljava/lang/String;

    .line 608544
    iget-object v10, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/util/BitSet;->set(I)V

    .line 608545
    move-object v1, v1

    .line 608546
    invoke-interface {v0}, LX/8dB;->d()Ljava/lang/String;

    move-result-object v9

    .line 608547
    iget-object v10, v1, LX/ELk;->a:LX/ELl;

    iput-object v9, v10, LX/ELl;->d:Ljava/lang/String;

    .line 608548
    iget-object v10, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Ljava/util/BitSet;->set(I)V

    .line 608549
    move-object v1, v1

    .line 608550
    iget-object v9, v1, LX/ELk;->a:LX/ELl;

    iput-object v2, v9, LX/ELl;->f:Ljava/lang/String;

    .line 608551
    iget-object v9, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v10, 0x5

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 608552
    move-object v1, v1

    .line 608553
    iget-object v2, v1, LX/ELk;->a:LX/ELl;

    iput-object v5, v2, LX/ELl;->g:Ljava/lang/CharSequence;

    .line 608554
    iget-object v2, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v9, 0x6

    invoke-virtual {v2, v9}, Ljava/util/BitSet;->set(I)V

    .line 608555
    move-object v1, v1

    .line 608556
    iget-object v2, v1, LX/ELk;->a:LX/ELl;

    iput-object v6, v2, LX/ELl;->h:Ljava/lang/CharSequence;

    .line 608557
    iget-object v2, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v5, 0x7

    invoke-virtual {v2, v5}, Ljava/util/BitSet;->set(I)V

    .line 608558
    move-object v1, v1

    .line 608559
    iget-object v2, v1, LX/ELk;->a:LX/ELl;

    iput-object v7, v2, LX/ELl;->j:Ljava/lang/CharSequence;

    .line 608560
    move-object v1, v1

    .line 608561
    iget-object v2, v1, LX/ELk;->a:LX/ELl;

    iput-object v8, v2, LX/ELl;->k:Ljava/lang/CharSequence;

    .line 608562
    move-object v1, v1

    .line 608563
    invoke-interface {v0}, LX/8dB;->o()Z

    move-result v0

    .line 608564
    iget-object v2, v1, LX/ELk;->a:LX/ELl;

    iput-boolean v0, v2, LX/ELl;->e:Z

    .line 608565
    iget-object v2, v1, LX/ELk;->e:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Ljava/util/BitSet;->set(I)V

    .line 608566
    move-object v0, v1

    .line 608567
    iget-object v1, v0, LX/ELk;->a:LX/ELl;

    iput-boolean v4, v1, LX/ELl;->i:Z

    .line 608568
    iget-object v1, v0, LX/ELk;->e:Ljava/util/BitSet;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 608569
    move-object v0, v0

    .line 608570
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 608571
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, v3, v4, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 608572
    :cond_1
    invoke-interface {v0}, LX/8dB;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_0

    .line 608573
    :cond_2
    invoke-interface {v0}, LX/8dB;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1, v4}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v4, v1

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;
    .locals 7

    .prologue
    .line 608585
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    monitor-enter v1

    .line 608586
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608587
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608588
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608589
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608590
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/ELm;->a(LX/0QB;)LX/ELm;

    move-result-object v4

    check-cast v4, LX/ELm;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/8hu;->a(LX/0QB;)LX/8hu;

    move-result-object v6

    check-cast v6, LX/8hu;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;-><init>(Landroid/content/Context;LX/ELm;LX/1V0;LX/8hu;)V

    .line 608591
    move-object v0, p0

    .line 608592
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608593
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608594
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608595
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 608507
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 608506
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608503
    check-cast p1, LX/CzL;

    .line 608504
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608505
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 608501
    check-cast p1, LX/CzL;

    .line 608502
    new-instance v0, LX/ELn;

    invoke-direct {v0, p0, p1}, LX/ELn;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;LX/CzL;)V

    return-object v0
.end method
