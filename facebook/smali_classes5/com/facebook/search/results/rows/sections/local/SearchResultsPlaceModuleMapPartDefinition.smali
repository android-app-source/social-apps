.class public Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field private final c:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608495
    new-instance v0, LX/3av;

    invoke-direct {v0}, LX/3av;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608474
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608475
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->b:LX/0Uh;

    .line 608476
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->c:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    .line 608477
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;
    .locals 5

    .prologue
    .line 608484
    const-class v1, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    monitor-enter v1

    .line 608485
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608486
    sput-object v2, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608489
    new-instance p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;)V

    .line 608490
    move-object v0, p0

    .line 608491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608483
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 608480
    check-cast p2, LX/CzL;

    .line 608481
    const v0, 0x7f0d2564

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->c:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608482
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 608478
    check-cast p1, LX/CzL;

    const/4 v0, 0x0

    .line 608479
    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, LX/CzL;->k()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->b:LX/0Uh;

    const/16 v2, 0x23d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
