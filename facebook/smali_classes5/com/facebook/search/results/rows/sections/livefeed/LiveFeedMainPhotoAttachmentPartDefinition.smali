.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611144
    new-instance v0, LX/3bV;

    invoke-direct {v0}, LX/3bV;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611145
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611146
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    .line 611147
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 611148
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    .line 611149
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 611150
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 611151
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611152
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611153
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611154
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611155
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;)V

    .line 611156
    move-object v0, p0

    .line 611157
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611158
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611159
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611160
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611161
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 611162
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611163
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedPhotoAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611164
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b16d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 611165
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b16d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b16d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v3, v0, v4, v0}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611166
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 611167
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 611168
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 611169
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 611170
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
