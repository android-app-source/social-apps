.class public Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "LX/EJz;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610684
    const v0, 0x7f03129d

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->a:LX/1Cz;

    .line 610685
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 610686
    iput v1, v0, LX/1UY;->b:F

    .line 610687
    move-object v0, v0

    .line 610688
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610680
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610681
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->c:LX/0ad;

    .line 610682
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610683
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;
    .locals 5

    .prologue
    .line 610669
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    monitor-enter v1

    .line 610670
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610671
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610672
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610673
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610674
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;-><init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610675
    move-object v0, p0

    .line 610676
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610677
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610678
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610668
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 610663
    check-cast p3, LX/1Pr;

    .line 610664
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->b:LX/1Ua;

    sget-object v4, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610665
    new-instance v1, LX/EJw;

    invoke-direct {v1, p0, p3}, LX/EJw;-><init>(Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;LX/1Pr;)V

    .line 610666
    new-instance v2, LX/EJx;

    invoke-direct {v2, p0, p3}, LX/EJx;-><init>(Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;LX/1Pr;)V

    .line 610667
    new-instance v3, LX/EJz;

    new-instance v0, LX/EJy;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v4}, LX/EJy;-><init>(Ljava/lang/Boolean;)V

    invoke-interface {p3, v0}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {v3, v1, v2, v0}, LX/EJz;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/Boolean;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6e758e94

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610650
    check-cast p1, LX/CzL;

    check-cast p2, LX/EJz;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 610651
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 610652
    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    .line 610653
    const v2, 0x7f0d2b8b

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 610654
    const p0, 0x7f0d2b8c

    invoke-virtual {v1, p0}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 610655
    iget-object p0, p2, LX/EJz;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610656
    iget-object p0, p2, LX/EJz;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610657
    iget-object p0, p2, LX/EJz;->c:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 610658
    iget-object v2, p2, LX/EJz;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 610659
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 610660
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 610661
    const/16 v1, 0x1f

    const v2, -0x7b5a9e91

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 610662
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610649
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 610643
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x0

    .line 610644
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 610645
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 610646
    const v1, 0x7f0d2b8b

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610647
    const v1, 0x7f0d2b8c

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610648
    return-void
.end method
