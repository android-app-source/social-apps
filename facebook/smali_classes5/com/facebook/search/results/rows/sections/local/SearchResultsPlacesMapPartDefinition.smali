.class public Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field private final c:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608377
    new-instance v0, LX/3au;

    invoke-direct {v0}, LX/3au;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608378
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608379
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->b:LX/0Uh;

    .line 608380
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->c:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    .line 608381
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;
    .locals 5

    .prologue
    .line 608382
    const-class v1, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;

    monitor-enter v1

    .line 608383
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608384
    sput-object v2, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608385
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608386
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608387
    new-instance p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;)V

    .line 608388
    move-object v0, p0

    .line 608389
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608390
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608391
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608392
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608393
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 608394
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 608395
    const v0, 0x7f0d2564

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->c:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608396
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 608397
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    const/4 v0, 0x0

    .line 608398
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->b:LX/0Uh;

    const/16 v2, 0x23d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
