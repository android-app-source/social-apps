.class public Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/1nD;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609592
    const v0, 0x7f0310ae

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609593
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609594
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609595
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609596
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->d:LX/1nD;

    .line 609597
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 609598
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->f:Landroid/content/Context;

    .line 609599
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;
    .locals 9

    .prologue
    .line 609600
    const-class v1, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    monitor-enter v1

    .line 609601
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609602
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609603
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609604
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609605
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v6

    check-cast v6, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    .line 609606
    move-object v0, v3

    .line 609607
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609608
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609609
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 609611
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 609612
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Ps;

    .line 609613
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609614
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EPD;

    invoke-direct {v1, p0, p2, p3}, LX/EPD;-><init>(Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Ps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609615
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70598bb5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609616
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 609617
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 609618
    const/16 v1, 0x1f

    const v2, 0x4005146e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609619
    const/4 v0, 0x1

    move v0, v0

    .line 609620
    return v0
.end method
