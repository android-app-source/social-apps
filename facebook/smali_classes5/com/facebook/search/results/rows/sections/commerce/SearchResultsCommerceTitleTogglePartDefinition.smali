.class public Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;>;",
        "LX/EK3;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610455
    const v0, 0x7f03129d

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->a:LX/1Cz;

    .line 610456
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 610457
    iput v1, v0, LX/1UY;->b:F

    .line 610458
    move-object v0, v0

    .line 610459
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610451
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610452
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->c:LX/0ad;

    .line 610453
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610454
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;
    .locals 5

    .prologue
    .line 610440
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    monitor-enter v1

    .line 610441
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610442
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610443
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610444
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610445
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;-><init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610446
    move-object v0, p0

    .line 610447
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610448
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610449
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610439
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 610434
    check-cast p3, LX/Cxo;

    .line 610435
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->b:LX/1Ua;

    sget-object v4, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610436
    new-instance v1, LX/EK0;

    invoke-direct {v1, p0, p3}, LX/EK0;-><init>(Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;LX/Cxo;)V

    .line 610437
    new-instance v2, LX/EK1;

    invoke-direct {v2, p0, p3}, LX/EK1;-><init>(Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;LX/Cxo;)V

    .line 610438
    new-instance v3, LX/EK3;

    new-instance v0, LX/EK2;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v0, v4}, LX/EK2;-><init>(Ljava/lang/Boolean;)V

    invoke-interface {p3, v0}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {v3, v1, v2, v0}, LX/EK3;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/Boolean;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x430c1858

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610419
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/EK3;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 610420
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 610421
    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    .line 610422
    const v2, 0x7f0d2b8b

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 610423
    const p0, 0x7f0d2b8c

    invoke-virtual {v1, p0}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 610424
    iget-object p0, p2, LX/EK3;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610425
    iget-object p0, p2, LX/EK3;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610426
    iget-object p0, p2, LX/EK3;->c:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-virtual {v2, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 610427
    iget-object v2, p2, LX/EK3;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 610428
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 610429
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 610430
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v1, v2

    .line 610431
    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 610432
    const/16 v1, 0x1f

    const v2, 0x2d3cedde

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 610433
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610412
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 610413
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x0

    .line 610414
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 610415
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 610416
    const v1, 0x7f0d2b8b

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610417
    const v1, 0x7f0d2b8c

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610418
    return-void
.end method
