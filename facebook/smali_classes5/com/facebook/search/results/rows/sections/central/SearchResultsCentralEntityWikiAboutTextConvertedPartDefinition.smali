.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/A5I;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1Ua;

.field public static final c:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f400000    # -6.0f

    .line 610919
    const v0, 0x7f03025e

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a:LX/1Cz;

    .line 610920
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 610921
    iput v1, v0, LX/1UY;->b:F

    .line 610922
    move-object v0, v0

    .line 610923
    iput v1, v0, LX/1UY;->c:F

    .line 610924
    move-object v0, v0

    .line 610925
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->b:LX/1Ua;

    .line 610926
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 610927
    iput v1, v0, LX/1UY;->b:F

    .line 610928
    move-object v0, v0

    .line 610929
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610915
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610916
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    .line 610917
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610918
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;
    .locals 5

    .prologue
    .line 610904
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    monitor-enter v1

    .line 610905
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610906
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610909
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610910
    move-object v0, p0

    .line 610911
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610912
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610913
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610914
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/widget/text/BetterTextView;LX/EJT;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 610892
    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 610893
    invoke-virtual {p1}, LX/EJT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610894
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b16ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 610895
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 610896
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 610897
    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxHeight(I)V

    .line 610898
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/4 v5, 0x0

    .line 610899
    new-instance v4, Landroid/graphics/LinearGradient;

    sub-int v6, v1, v0

    int-to-float v6, v6

    int-to-float v8, v1

    const/4 v7, 0x2

    new-array v9, v7, [I

    const/4 v7, 0x0

    aput v2, v9, v7

    const/4 v7, 0x1

    const/4 v10, -0x1

    aput v10, v9, v7

    const/4 v10, 0x0

    sget-object v11, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v7, v5

    invoke-direct/range {v4 .. v11}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    move-object v0, v4

    .line 610900
    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 610901
    :goto_0
    return-void

    .line 610902
    :cond_0
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setMaxHeight(I)V

    .line 610903
    invoke-virtual {p0}, Lcom/facebook/widget/text/BetterTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610873
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 610883
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pr;

    const/4 v4, 0x0

    .line 610884
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610885
    check-cast v0, LX/A5I;

    invoke-static {v0}, LX/EJT;->a(LX/A5I;)LX/1KL;

    move-result-object v0

    new-instance v1, LX/EJL;

    invoke-direct {v1, p0}, LX/EJL;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;)V

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EJT;

    .line 610886
    iget-boolean v1, v0, LX/EJT;->b:Z

    move v1, v1

    .line 610887
    if-eqz v1, :cond_0

    .line 610888
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->e:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderConvertedPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610889
    :cond_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-virtual {v0}, LX/EJT;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->b:LX/1Ua;

    :goto_0
    invoke-virtual {v0}, LX/EJT;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    :goto_1
    invoke-direct {v3, v4, v1, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610890
    return-object v4

    .line 610891
    :cond_1
    sget-object v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->c:LX/1Ua;

    goto :goto_0

    :cond_2
    sget-object v0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x626a11d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610877
    check-cast p1, LX/CzL;

    check-cast p3, LX/1Pr;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 610878
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 610879
    check-cast v1, LX/A5I;

    invoke-static {v1}, LX/EJT;->a(LX/A5I;)LX/1KL;

    move-result-object v1

    new-instance v2, LX/EJM;

    invoke-direct {v2, p0}, LX/EJM;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;)V

    invoke-interface {p3, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EJT;

    .line 610880
    iget-object v2, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 610881
    check-cast v2, LX/A5I;

    invoke-interface {v2}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p4, v1, v2}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a(Lcom/facebook/widget/text/BetterTextView;LX/EJT;Ljava/lang/String;)V

    .line 610882
    const/16 v1, 0x1f

    const v2, -0x5792903b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610874
    check-cast p1, LX/CzL;

    .line 610875
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610876
    check-cast v0, LX/A5I;

    invoke-interface {v0}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
