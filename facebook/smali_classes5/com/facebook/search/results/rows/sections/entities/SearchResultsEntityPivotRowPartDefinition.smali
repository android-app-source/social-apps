.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static final c:LX/3ag;

.field private static l:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

.field private final h:LX/0wM;

.field private final i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final j:LX/23P;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 606879
    const v0, 0x7f0312ae

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->a:LX/1Cz;

    .line 606880
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 606881
    iput v1, v0, LX/1UY;->b:F

    .line 606882
    move-object v0, v0

    .line 606883
    const/high16 v1, 0x40800000    # 4.0f

    .line 606884
    iput v1, v0, LX/1UY;->c:F

    .line 606885
    move-object v0, v0

    .line 606886
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->b:LX/1Ua;

    .line 606887
    new-instance v0, LX/3af;

    invoke-direct {v0}, LX/3af;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->c:LX/3ag;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;LX/0Ot;LX/23P;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/TextAppearancePartDefinition;",
            "LX/0wM;",
            "Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;",
            "LX/23P;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 606888
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 606889
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 606890
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 606891
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 606892
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->h:LX/0wM;

    .line 606893
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->g:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    .line 606894
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->k:LX/0Ot;

    .line 606895
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->j:LX/23P;

    .line 606896
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 606897
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;
    .locals 12

    .prologue
    .line 606898
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    monitor-enter v1

    .line 606899
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 606900
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 606901
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606902
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 606903
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    const/16 v9, 0x33c2

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v10

    check-cast v10, LX/23P;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;LX/0Ot;LX/23P;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 606904
    move-object v0, v3

    .line 606905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 606906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 606908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606909
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 606910
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x0

    .line 606911
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    invoke-direct {v1, v5, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606912
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606913
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 606914
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 606915
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 606916
    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606917
    iget v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->d:I

    move v1, v1

    .line 606918
    if-lez v1, :cond_0

    .line 606919
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v2, LX/EK9;

    sget-object v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->c:LX/3ag;

    const v4, 0x7f0822b7

    invoke-direct {v2, v0, v3, v4}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 606920
    const v2, 0x7f0822b7

    .line 606921
    const v1, 0x7f0e0125

    .line 606922
    :goto_0
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 606923
    if-nez v2, :cond_1

    const-string v2, ""

    .line 606924
    :goto_1
    const v3, 0x7f0d2ba2

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606925
    const v2, 0x7f0d2ba2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->f:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606926
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/Integer;

    move-result-object v0

    .line 606927
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->h:LX/0wM;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 606928
    const v1, 0x7f0d2ba1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->g:Lcom/facebook/multirow/parts/ImageViewDrawablePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 606929
    return-object v5

    .line 606930
    :cond_0
    const v2, 0x7f0822b8

    .line 606931
    const v1, 0x7f0e0123

    goto :goto_0

    .line 606932
    :cond_1
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->j:LX/23P;

    invoke-virtual {v3, v2, v5}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 606933
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 606934
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 606935
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 606936
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 606937
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
