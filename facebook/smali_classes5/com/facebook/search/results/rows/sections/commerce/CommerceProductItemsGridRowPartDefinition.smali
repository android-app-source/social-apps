.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;",
        "LX/EJt;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/7j6;

.field public final e:LX/0hy;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610400
    const-class v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    const-string v1, "places_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 610401
    const v0, 0x7f03128c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610405
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610406
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610407
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->d:LX/7j6;

    .line 610408
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->e:LX/0hy;

    .line 610409
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 610410
    return-void
.end method

.method private a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 610402
    if-nez p1, :cond_0

    .line 610403
    const/4 v0, 0x0

    .line 610404
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/EJs;

    invoke-direct {v0, p0, p1, p2}, LX/EJs;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;
    .locals 7

    .prologue
    .line 610389
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    monitor-enter v1

    .line 610390
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610391
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610392
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610393
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610394
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v4

    check-cast v4, LX/7j6;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 610395
    move-object v0, p0

    .line 610396
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610397
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610398
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610411
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 610386
    check-cast p2, LX/0Px;

    check-cast p3, LX/1Ps;

    .line 610387
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610388
    new-instance v1, LX/EJt;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v2

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/EJt;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xf3996e5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610338
    check-cast p1, LX/0Px;

    check-cast p2, LX/EJt;

    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;

    const/4 p3, 0x4

    const/4 p0, 0x0

    .line 610339
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v2, v1

    .line 610340
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v4, v1

    .line 610341
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 610342
    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 610343
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610344
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 610345
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 610346
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 610347
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    .line 610348
    :goto_0
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 610349
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 610350
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 610351
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 610352
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v5

    .line 610353
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610354
    :cond_0
    iget-object v1, p2, LX/EJt;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610355
    invoke-virtual {v2, p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    .line 610356
    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_5

    .line 610357
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 610358
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v2

    sget-object v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v2, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610359
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610360
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 610361
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610362
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    .line 610363
    :goto_2
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610364
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 610365
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610366
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 610367
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v2

    .line 610368
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610369
    :cond_1
    iget-object v1, p2, LX/EJt;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610370
    invoke-virtual {v4, p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    .line 610371
    :goto_3
    const/16 v1, 0x1f

    const v2, 0x5cce86dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 610372
    :cond_2
    iget-object v5, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 610373
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 610374
    :cond_3
    invoke-virtual {v2, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    goto :goto_1

    .line 610375
    :cond_4
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610376
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    goto :goto_2

    .line 610377
    :cond_5
    invoke-virtual {v4, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 610384
    check-cast p1, LX/0Px;

    .line 610385
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 610378
    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;

    const/4 v1, 0x0

    .line 610379
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610380
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610381
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610382
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610383
    return-void
.end method
