.class public Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;",
        ">;",
        "LX/1aZ;",
        "LX/1Ps;",
        "LX/3b5;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static o:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0WJ;

.field public final i:LX/1ay;

.field private final j:LX/1Kf;

.field public final k:Ljava/util/concurrent/ExecutorService;

.field private final l:Ljava/util/concurrent/ExecutorService;

.field public final m:LX/929;

.field public final n:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609081
    new-instance v0, LX/3b4;

    invoke-direct {v0}, LX/3b4;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->a:LX/1Cz;

    .line 609082
    const-class v0, LX/3b5;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0WJ;LX/0Or;LX/1ay;LX/1Kf;LX/929;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/1ay;",
            "LX/1Kf;",
            "LX/929;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609067
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609068
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->c:Landroid/content/Context;

    .line 609069
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609070
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609071
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 609072
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->g:LX/0Or;

    .line 609073
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->h:LX/0WJ;

    .line 609074
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->i:LX/1ay;

    .line 609075
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->j:LX/1Kf;

    .line 609076
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->m:LX/929;

    .line 609077
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->k:Ljava/util/concurrent/ExecutorService;

    .line 609078
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->l:Ljava/util/concurrent/ExecutorService;

    .line 609079
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 609080
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;
    .locals 15

    .prologue
    .line 609056
    const-class v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    monitor-enter v1

    .line 609057
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609058
    sput-object v2, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609059
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609060
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609061
    new-instance v3, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v8

    check-cast v8, LX/0WJ;

    const/16 v9, 0x509

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v10

    check-cast v10, LX/1ay;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v11

    check-cast v11, LX/1Kf;

    invoke-static {v0}, LX/929;->b(LX/0QB;)LX/929;

    move-result-object v12

    check-cast v12, LX/929;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v14

    check-cast v14, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/0WJ;LX/0Or;LX/1ay;LX/1Kf;LX/929;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V

    .line 609062
    move-object v0, v3

    .line 609063
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609064
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609065
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V
    .locals 3

    .prologue
    .line 609054
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition$5;-><init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    const v2, 0x1ca7ed1d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 609055
    return-void
.end method

.method public static c(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V
    .locals 4

    .prologue
    .line 609047
    sget-object v0, LX/21D;->SEARCH:LX/21D;

    const-string v1, "keywordSearchStatus"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 609048
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v1

    .line 609049
    if-eqz v1, :cond_0

    .line 609050
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v1

    .line 609051
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 609052
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->j:LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->c:Landroid/content/Context;

    invoke-interface {v1, v2, v0, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 609053
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 609014
    sget-object v0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 609022
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 609023
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 609024
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    .line 609025
    iget-object v6, v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v6, v6

    .line 609026
    if-eqz v6, :cond_2

    .line 609027
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->r:LX/1Ua;

    sget-object v5, LX/1X9;->BOX:LX/1X9;

    invoke-direct {v3, v1, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609028
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609029
    new-instance v3, LX/EKC;

    invoke-direct {v3, p0, v0}, LX/EKC;-><init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V

    move-object v3, v3

    .line 609030
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609031
    const v2, 0x7f0d1dbd

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609032
    new-instance v4, LX/EKD;

    invoke-direct {v4, p0}, LX/EKD;-><init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;)V

    move-object v4, v4

    .line 609033
    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609034
    const v2, 0x7f0d02a7

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v0, :cond_0

    .line 609035
    iget-object v4, v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->a:Ljava/lang/String;

    move-object v0, v4

    .line 609036
    :goto_1
    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609037
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->h:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    .line 609038
    if-nez v0, :cond_1

    move-object v0, v1

    .line 609039
    :goto_2
    return-object v0

    .line 609040
    :cond_0
    const-string v0, ""

    goto :goto_1

    .line 609041
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 609042
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_2

    .line 609043
    :cond_2
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->m:LX/929;

    const-string v7, "fetch_composer_in_tnv2_search_results"

    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->k:Ljava/util/concurrent/ExecutorService;

    .line 609044
    iget-object v9, v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->b:Ljava/lang/String;

    move-object v9, v9

    .line 609045
    iget-object v10, v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->c:Ljava/lang/String;

    move-object v10, v10

    .line 609046
    new-instance v11, LX/EKE;

    invoke-direct {v11, p0, v0}, LX/EKE;-><init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V

    invoke-virtual/range {v6 .. v11}, LX/929;->a(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1a9db360

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609016
    check-cast p2, LX/1aZ;

    check-cast p4, LX/3b5;

    .line 609017
    const/4 v1, 0x0

    .line 609018
    iput-boolean v1, p4, LX/3b5;->a:Z

    .line 609019
    if-eqz p2, :cond_0

    .line 609020
    iget-object v1, p4, LX/3b5;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 609021
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x60e22ed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609015
    const/4 v0, 0x1

    return v0
.end method
