.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxa;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/3ag;

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607276
    const v0, 0x7f0312e5

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->a:LX/1Cz;

    .line 607277
    new-instance v0, LX/3an;

    invoke-direct {v0}, LX/3an;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->b:LX/3ag;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/feed/rows/styling/PaddingPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EPF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607247
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607248
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->c:LX/0ad;

    .line 607249
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 607250
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->f:LX/0Ot;

    .line 607251
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->g:LX/0Ot;

    .line 607252
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    .line 607253
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->h:LX/0Ot;

    .line 607254
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;
    .locals 10

    .prologue
    .line 607264
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;

    monitor-enter v1

    .line 607265
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607266
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607267
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607268
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607269
    new-instance v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    const/16 v7, 0x33c2

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xe0f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3488

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;-><init>(LX/0ad;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 607270
    move-object v0, v3

    .line 607271
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607272
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607273
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607274
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607275
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607255
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    check-cast p3, LX/1Ps;

    .line 607256
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, p2, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607257
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->s()LX/0Px;

    .line 607258
    goto :goto_1

    .line 607259
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    .line 607260
    iget-object v1, p2, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->c:Ljava/lang/String;

    move-object v1, v1

    .line 607261
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607262
    const/4 v0, 0x0

    return-object v0

    .line 607263
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/EK9;

    sget-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->b:LX/3ag;

    const v3, 0x7f0822a4

    invoke-direct {v1, p2, v2, v3}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607244
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    .line 607245
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->c:Ljava/lang/String;

    move-object v0, v0

    .line 607246
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
