.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/2mZ;

.field private final c:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/23s;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609900
    new-instance v0, LX/3bI;

    invoke-direct {v0}, LX/3bI;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/23s;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609901
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609902
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->b:LX/2mZ;

    .line 609903
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->c:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    .line 609904
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    .line 609905
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    .line 609906
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->f:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    .line 609907
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->g:LX/23s;

    .line 609908
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;
    .locals 10

    .prologue
    .line 609909
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    monitor-enter v1

    .line 609910
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609911
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609912
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609913
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609914
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v9

    check-cast v9, LX/23s;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;-><init>(LX/2mZ;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/23s;)V

    .line 609915
    move-object v0, v3

    .line 609916
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609917
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609918
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609919
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609920
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 609921
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Po;

    .line 609922
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 609923
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609924
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->c:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchPartDefinition;

    new-instance v2, LX/36r;

    const/4 v3, -0x1

    invoke-direct {v2, v0, v3}, LX/36r;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609925
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 609926
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 609927
    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    move-object v1, p3

    .line 609928
    check-cast v1, LX/Cxj;

    invoke-interface {v1, p2}, LX/Cxj;->d(LX/CzL;)LX/CyM;

    move-result-object v1

    .line 609929
    invoke-static {v3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    .line 609930
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->b:LX/2mZ;

    invoke-virtual {v4, v0, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v2

    invoke-virtual {v2}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 609931
    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 609932
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->g:LX/23s;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    invoke-interface {v7}, LX/1PT;->a()LX/1Qt;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v0

    .line 609933
    new-instance v6, LX/0AW;

    invoke-direct {v6, v2}, LX/0AW;-><init>(LX/162;)V

    .line 609934
    iput-boolean v5, v6, LX/0AW;->d:Z

    .line 609935
    move-object v2, v6

    .line 609936
    iput-object v0, v2, LX/0AW;->c:LX/04H;

    .line 609937
    move-object v0, v2

    .line 609938
    invoke-virtual {v0}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v5

    .line 609939
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    new-instance v2, LX/EPn;

    .line 609940
    new-instance v6, LX/EPo;

    invoke-direct {v6, p0, v1}, LX/EPo;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;LX/CyM;)V

    move-object v1, v6

    .line 609941
    invoke-direct {v2, p2, v1, v4}, LX/EPn;-><init>(LX/CzL;LX/3It;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609942
    new-instance v0, LX/EPh;

    .line 609943
    iget-object v1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v1

    .line 609944
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    new-instance v3, LX/093;

    invoke-direct {v3}, LX/093;-><init>()V

    sget-object v6, LX/04D;->SEARCH_RESULTS:LX/04D;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, LX/EPh;-><init>(LX/CzL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;)V

    .line 609945
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609946
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->f:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609947
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609948
    const/4 v0, 0x1

    move v0, v0

    .line 609949
    return v0
.end method
