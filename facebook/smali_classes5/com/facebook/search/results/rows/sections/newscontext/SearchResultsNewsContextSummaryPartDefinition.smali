.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608766
    const v0, 0x7f030c01

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->a:LX/1Cz;

    .line 608767
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 608768
    iput v1, v0, LX/1UY;->b:F

    .line 608769
    move-object v0, v0

    .line 608770
    const/4 v1, 0x0

    .line 608771
    iput v1, v0, LX/1UY;->c:F

    .line 608772
    move-object v0, v0

    .line 608773
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608774
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608775
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608776
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608777
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 608778
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 608779
    check-cast p2, Ljava/lang/String;

    check-cast p3, LX/1Ps;

    const/4 v4, 0x0

    .line 608780
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608781
    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    if-nez v0, :cond_0

    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 608782
    :goto_0
    if-eqz v0, :cond_2

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    .line 608783
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, v4, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608784
    return-object v4

    .line 608785
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 608786
    :cond_2
    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608787
    const/4 v0, 0x1

    return v0
.end method
