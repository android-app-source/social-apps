.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzK;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608736
    const v0, 0x7f030c02

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->a:LX/1Cz;

    .line 608737
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 608738
    iput v1, v0, LX/1UY;->b:F

    .line 608739
    move-object v0, v0

    .line 608740
    const/high16 v1, -0x3fc00000    # -3.0f

    .line 608741
    iput v1, v0, LX/1UY;->c:F

    .line 608742
    move-object v0, v0

    .line 608743
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608744
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608745
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    .line 608746
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608747
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;
    .locals 5

    .prologue
    .line 608748
    const-class v1, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    monitor-enter v1

    .line 608749
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608750
    sput-object v2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608751
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608752
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608753
    new-instance p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 608754
    move-object v0, p0

    .line 608755
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608756
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608757
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608759
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 608760
    check-cast p2, LX/CzK;

    const/4 v4, 0x0

    .line 608761
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    .line 608762
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608763
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->b:LX/1Ua;

    invoke-direct {v2, v4, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608764
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608765
    const/4 v0, 0x1

    return v0
.end method
