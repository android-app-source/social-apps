.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/A5I;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610849
    sget-object v0, LX/3ab;->b:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610850
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610851
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610852
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 610853
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;
    .locals 5

    .prologue
    .line 610854
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    monitor-enter v1

    .line 610855
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610856
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610857
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610858
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610859
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 610860
    move-object v0, p0

    .line 610861
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610862
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610863
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/Object;)LX/1X6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 610865
    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    if-nez p0, :cond_0

    sget-object v0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    :goto_0
    invoke-direct {v1, v2, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    return-object v1

    :cond_0
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610866
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 610867
    check-cast p2, LX/A5I;

    .line 610868
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p2}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->b(Ljava/lang/Object;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610869
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p2}, LX/A5I;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610870
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610871
    const/4 v0, 0x1

    move v0, v0

    .line 610872
    return v0
.end method
