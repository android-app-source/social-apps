.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "LX/EJr;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

.field private final d:LX/0y2;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610298
    new-instance v0, LX/3bK;

    const v1, 0x7f031288

    invoke-direct {v0, v1}, LX/3bK;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a:LX/1Cz;

    .line 610299
    const-class v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;LX/0y2;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610293
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610294
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    .line 610295
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->d:LX/0y2;

    .line 610296
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->e:LX/0ad;

    .line 610297
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;
    .locals 6

    .prologue
    .line 610282
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    monitor-enter v1

    .line 610283
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610284
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610285
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610286
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610287
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v4

    check-cast v4, LX/0y2;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;LX/0y2;LX/0ad;)V

    .line 610288
    move-object v0, p0

    .line 610289
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610290
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610291
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/16 v4, 0x11

    .line 610275
    new-instance v0, LX/3DI;

    const-string v1, " "

    invoke-direct {v0, v1}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 610276
    if-eqz p2, :cond_0

    .line 610277
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0e0884

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p2, v1, v4}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 610278
    :cond_0
    if-eqz p1, :cond_1

    .line 610279
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v0, p1, v1, v4}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    .line 610280
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const v2, 0x7f0e0886

    invoke-direct {v1, p0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3DI;->setSpan(Ljava/lang/Object;III)V

    .line 610281
    :cond_1
    return-object v0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 610300
    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v1

    .line 610301
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0xa7c5482

    if-eq v2, v3, :cond_1

    .line 610302
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610274
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 610249
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    check-cast p3, LX/1Pn;

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 610250
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610251
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 610252
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eB()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610253
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 610254
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610255
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v2

    .line 610256
    iget-object v3, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v3, v3

    .line 610257
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 610258
    :goto_0
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610259
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hg()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 610260
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610261
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hh()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    .line 610262
    :cond_0
    new-instance v2, LX/EJr;

    invoke-direct {v2, v1, v0}, LX/EJr;-><init>(ZLjava/lang/CharSequence;)V

    move-object v0, v2

    .line 610263
    :goto_1
    return-object v0

    .line 610264
    :cond_1
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    .line 610265
    iget-object v3, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v3, v3

    .line 610266
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 610267
    :cond_2
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 610268
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hg()D

    move-result-wide v2

    .line 610269
    iget-object v4, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v4, v4

    .line 610270
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hh()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v2

    invoke-virtual {v2}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 610271
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->d:LX/0y2;

    invoke-virtual {v3}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    .line 610272
    if-eqz v3, :cond_3

    invoke-static {v3, v2}, LX/3GC;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x40d86a0000000000L    # 25000.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_3

    const/4 v1, 0x1

    .line 610273
    :cond_3
    new-instance v2, LX/EJr;

    invoke-direct {v2, v1, v0}, LX/EJr;-><init>(ZLjava/lang/CharSequence;)V

    move-object v0, v2

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3ee549f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610239
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    check-cast p2, LX/EJr;

    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;

    .line 610240
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v1

    .line 610241
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v2

    sget-object p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v2, p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610242
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductName(Ljava/lang/CharSequence;)V

    .line 610243
    iget-object v2, p2, LX/EJr;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductPrice(Ljava/lang/CharSequence;)V

    .line 610244
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, p0, :cond_0

    .line 610245
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductSubtitle(Ljava/lang/CharSequence;)V

    .line 610246
    :goto_0
    iget-boolean v1, p2, LX/EJr;->a:Z

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setNearbyIndicatorEnabled(Z)V

    .line 610247
    const/16 v1, 0x1f

    const v2, 0x6f638e1d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 610248
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->setProductSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610238
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z

    move-result v0

    return v0
.end method
