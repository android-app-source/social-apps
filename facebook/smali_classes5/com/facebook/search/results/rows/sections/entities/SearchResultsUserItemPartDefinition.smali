.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d1;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8d1;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Uh;

.field private final l:LX/D0N;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610166
    const v0, 0x7f03155b

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/0Uh;LX/D0N;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/D0N;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610153
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610154
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 610155
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    .line 610156
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    .line 610157
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 610158
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 610159
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 610160
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 610161
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->i:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    .line 610162
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->j:LX/0Ot;

    .line 610163
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->l:LX/D0N;

    .line 610164
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->k:LX/0Uh;

    .line 610165
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;
    .locals 15

    .prologue
    .line 610167
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    monitor-enter v1

    .line 610168
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610169
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610170
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610171
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610172
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    const/16 v11, 0x3417

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static {v0}, LX/D0N;->b(LX/0QB;)LX/D0N;

    move-result-object v14

    check-cast v14, LX/D0N;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/0Uh;LX/D0N;)V

    .line 610173
    move-object v0, v3

    .line 610174
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610175
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610176
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610177
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/CzL;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 610143
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 610144
    invoke-static {v0}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 610145
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->k:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610146
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 610147
    :goto_0
    return-object v0

    .line 610148
    :cond_1
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610149
    check-cast v0, LX/8d1;

    .line 610150
    invoke-interface {v0}, LX/8d1;->L()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object p0

    .line 610151
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object p0

    :goto_1
    move-object v0, p0

    .line 610152
    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610142
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 610111
    check-cast p2, LX/CzL;

    .line 610112
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610113
    check-cast v0, LX/8d1;

    .line 610114
    invoke-direct {p0, p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->a(LX/CzL;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 610115
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v3, LX/EJI;

    sget-object v4, LX/3ap;->a:LX/1Ua;

    invoke-direct {v3, p2, v4}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610116
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    invoke-interface {p1, v2, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610117
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailMediaDataPartDefinition;

    .line 610118
    invoke-interface {v0}, LX/8d1;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    .line 610119
    if-nez v4, :cond_3

    .line 610120
    const/4 v3, 0x0

    .line 610121
    :goto_0
    move-object v3, v3

    .line 610122
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610123
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610124
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 610125
    invoke-static {v2}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 610126
    invoke-static {v2}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->k:LX/0Uh;

    invoke-static {v2}, LX/ELM;->a(LX/0Uh;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 610127
    :cond_0
    const/4 v2, 0x1

    invoke-static {p2, v2}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v2

    .line 610128
    :goto_1
    move-object v2, v2

    .line 610129
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-interface {p1, v3, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610130
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-interface {v0}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_2
    invoke-static {v3, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610131
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610132
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->l:LX/D0N;

    invoke-virtual {v1, v0}, LX/D0N;->a(LX/8d1;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 610133
    const v1, 0x7f0d0d8d

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v1, v0, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 610134
    :goto_3
    const/4 v0, 0x0

    return-object v0

    .line 610135
    :cond_1
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 610136
    :cond_2
    const v0, 0x7f0d0d8d

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->i:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_3

    :cond_3
    new-instance v3, LX/8Ch;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->c()I

    move-result p3

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->a()I

    move-result v4

    invoke-direct {v3, v5, v6, p3, v4}, LX/8Ch;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    .line 610137
    :cond_4
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 610138
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->o()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v2

    .line 610139
    if-nez v2, :cond_5

    .line 610140
    const/4 v2, 0x0

    goto :goto_1

    .line 610141
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->b()Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    move-result-object v2

    invoke-static {v2}, LX/8g7;->a(Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610110
    const/4 v0, 0x1

    return v0
.end method
