.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40c00000    # 6.0f

    .line 610231
    const v0, 0x7f0312aa

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a:LX/1Cz;

    .line 610232
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 610233
    iput v1, v0, LX/1UY;->b:F

    .line 610234
    move-object v0, v0

    .line 610235
    iput v1, v0, LX/1UY;->c:F

    .line 610236
    move-object v0, v0

    .line 610237
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610200
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610201
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610202
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;

    .line 610203
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 610204
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    .line 610205
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    .line 610206
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;
    .locals 9

    .prologue
    .line 610220
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    monitor-enter v1

    .line 610221
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610222
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610223
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610224
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610225
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;)V

    .line 610226
    move-object v0, v3

    .line 610227
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610228
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610229
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 610217
    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v1

    .line 610218
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0xa7c5482

    if-eq v2, v3, :cond_1

    .line 610219
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610216
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 610208
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 610209
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 610210
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610211
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610212
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610213
    const v0, 0x7f0d2b9f

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 610214
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610215
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610207
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z

    move-result v0

    return v0
.end method
