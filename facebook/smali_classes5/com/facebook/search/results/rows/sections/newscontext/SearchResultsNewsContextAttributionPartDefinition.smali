.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608803
    const v0, 0x7f030c00

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->a:LX/1Cz;

    .line 608804
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 608805
    iput v1, v0, LX/1UY;->b:F

    .line 608806
    move-object v0, v0

    .line 608807
    const/4 v1, 0x0

    .line 608808
    iput v1, v0, LX/1UY;->c:F

    .line 608809
    move-object v0, v0

    .line 608810
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608796
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608797
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608798
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608799
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 608800
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 608801
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 608802
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 608788
    check-cast p2, Ljava/lang/String;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 608789
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 608790
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/ENk;

    invoke-direct {v2, p0, v0, p3}, LX/ENk;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;Landroid/net/Uri;LX/1Pn;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608791
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 608792
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    const-string v2, "www."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608793
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608794
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608795
    const/4 v0, 0x1

    return v0
.end method
