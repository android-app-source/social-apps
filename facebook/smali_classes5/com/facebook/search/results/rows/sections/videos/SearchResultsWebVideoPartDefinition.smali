.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A52;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Ljava/lang/String;

.field private static m:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final g:LX/154;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field private final i:LX/11S;

.field public final j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final k:LX/0wM;

.field public final l:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608302
    const v0, 0x7f031343    # 1.7422888E38f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a:LX/1Cz;

    .line 608303
    const-class v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 608304
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x1eaef984

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/154;Lcom/facebook/content/SecureContextHelper;LX/11S;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608366
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608367
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 608368
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 608369
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608370
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->g:LX/154;

    .line 608371
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->h:Lcom/facebook/content/SecureContextHelper;

    .line 608372
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->i:LX/11S;

    .line 608373
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->k:LX/0wM;

    .line 608374
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 608375
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->l:LX/CvY;

    .line 608376
    return-void
.end method

.method public static synthetic a(Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;)LX/CvY;
    .locals 1

    .prologue
    .line 608365
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->l:LX/CvY;

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;
    .locals 13

    .prologue
    .line 608354
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    monitor-enter v1

    .line 608355
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608356
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608357
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608358
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608359
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v7

    check-cast v7, LX/154;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v9

    check-cast v9, LX/11S;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v12

    check-cast v12, LX/CvY;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/154;Lcom/facebook/content/SecureContextHelper;LX/11S;LX/0wM;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/CvY;)V

    .line 608360
    move-object v0, v3

    .line 608361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;LX/A52;LX/1Ps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/A52;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 608336
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 608337
    invoke-interface {p2}, LX/A52;->ca()LX/A56;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, LX/A52;->ca()LX/A56;

    move-result-object v1

    invoke-interface {v1}, LX/A56;->c()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 608338
    const v1, 0x7f0d2ca0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-interface {p2}, LX/A52;->ca()LX/A56;

    move-result-object v4

    invoke-interface {v4}, LX/A56;->c()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v3

    sget-object v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 608339
    iput-object v4, v3, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 608340
    move-object v3, v3

    .line 608341
    const v4, 0x7f0b0633

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0b0634

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/2f8;->a(II)LX/2f8;

    move-result-object v3

    sget-object v4, LX/1Up;->h:LX/1Up;

    .line 608342
    iput-object v4, v3, LX/2f8;->i:LX/1Up;

    .line 608343
    move-object v3, v3

    .line 608344
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608345
    :cond_0
    const v1, 0x7f080d4e

    new-array v2, v7, [Ljava/lang/Object;

    invoke-interface {p2}, LX/A52;->aX()LX/A57;

    move-result-object v3

    invoke-interface {v3}, LX/A57;->b()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, LX/D0O;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 608346
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->k:LX/0wM;

    const v3, 0x7f0208f8

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 608347
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 608348
    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 608349
    invoke-virtual {v2, v6, v6, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 608350
    new-instance v0, Landroid/text/style/ImageSpan;

    invoke-direct {v0, v2, v7}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 608351
    const/16 v1, 0x12

    invoke-interface {v3, v0, v6, v7, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 608352
    const v0, 0x7f0d2ba7

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608353
    return-void
.end method

.method private b(LX/1aD;LX/A52;LX/1Ps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/A52;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 608324
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 608325
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->i:LX/11S;

    sget-object v2, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-interface {p2}, LX/A52;->W()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 608326
    const v2, 0x7f080d4c

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    const/4 v6, 0x4

    .line 608327
    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 608328
    :cond_0
    const-string v4, ""

    .line 608329
    :goto_0
    move-object v4, v4

    .line 608330
    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 608331
    const v1, 0x7f0d02c3

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608332
    return-void

    .line 608333
    :cond_1
    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "www."

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_2

    .line 608334
    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 608335
    :cond_2
    invoke-interface {p2}, LX/A52;->bZ()LX/175;

    move-result-object v4

    invoke-interface {v4}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private c(LX/1aD;LX/A52;LX/1Ps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/A52;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 608320
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 608321
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->g:LX/154;

    invoke-interface {p2}, LX/A52;->cb()LX/A55;

    move-result-object v2

    invoke-interface {v2}, LX/A55;->a()I

    move-result v2

    invoke-virtual {v1, v2, v7}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 608322
    const v2, 0x7f0d2bac

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    const v4, 0x7f0f0052

    invoke-interface {p2}, LX/A52;->cb()LX/A55;

    move-result-object v5

    invoke-interface {v5}, LX/A55;->a()I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608323
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608319
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 608306
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    .line 608307
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608308
    check-cast v0, LX/A52;

    invoke-direct {p0, p1, v0, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a(LX/1aD;LX/A52;LX/1Ps;)V

    .line 608309
    const v1, 0x7f0d02c4

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608310
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608311
    check-cast v0, LX/A52;

    invoke-interface {v0}, LX/A52;->bY()LX/175;

    move-result-object v0

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608312
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608313
    check-cast v0, LX/A52;

    invoke-direct {p0, p1, v0, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->b(LX/1aD;LX/A52;LX/1Ps;)V

    .line 608314
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608315
    check-cast v0, LX/A52;

    invoke-direct {p0, p1, v0, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->c(LX/1aD;LX/A52;LX/1Ps;)V

    .line 608316
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EPs;

    invoke-direct {v1, p0, p3, p2}, LX/EPs;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;LX/1Ps;LX/CzL;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608317
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608318
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608305
    const/4 v0, 0x1

    return v0
.end method
