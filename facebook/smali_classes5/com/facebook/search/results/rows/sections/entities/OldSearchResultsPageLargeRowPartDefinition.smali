.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field public final b:LX/0Uh;

.field private final c:LX/ELj;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

.field private final i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607547
    const v0, 0x7f030e41

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/ELj;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607548
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607549
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->b:LX/0Uh;

    .line 607550
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->c:LX/ELj;

    .line 607551
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607552
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607553
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607554
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607555
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    .line 607556
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 607557
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;
    .locals 12

    .prologue
    .line 607536
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;

    monitor-enter v1

    .line 607537
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607538
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607541
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/ELj;->b(LX/0QB;)LX/ELj;

    move-result-object v5

    check-cast v5, LX/ELj;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;-><init>(LX/0Uh;LX/ELj;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;)V

    .line 607542
    move-object v0, v3

    .line 607543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607535
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607516
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    check-cast p3, LX/1Pn;

    .line 607517
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607518
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    const/4 v3, 0x0

    .line 607519
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->b:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 607520
    invoke-static {p2, v3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;I)Ljava/lang/String;

    move-result-object v1

    .line 607521
    :goto_0
    move-object v1, v1

    .line 607522
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607523
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607524
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->b:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 607525
    invoke-static {p2}, LX/ELM;->b(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 607526
    :goto_1
    move-object v1, v1

    .line 607527
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607528
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v0

    .line 607529
    const v1, 0x7f0d1286

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsRowSecondaryActionPartDefinition;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->c:LX/ELj;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    check-cast p3, LX/Cxd;

    invoke-virtual {v3, v0, v4, p3}, LX/ELj;->a(Lcom/facebook/graphql/model/GraphQLPageCallToAction;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)LX/EMF;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607530
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607531
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/3ap;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607532
    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v2

    invoke-static {v1, v2, v3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/ELM;->b(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 607533
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 607534
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
