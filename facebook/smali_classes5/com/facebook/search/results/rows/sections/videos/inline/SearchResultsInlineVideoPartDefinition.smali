.class public Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609893
    new-instance v0, LX/3bH;

    invoke-direct {v0}, LX/3bH;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609894
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609895
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    .line 609896
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    .line 609897
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;
    .locals 5

    .prologue
    .line 609882
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    monitor-enter v1

    .line 609883
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609884
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609887
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;)V

    .line 609888
    move-object v0, p0

    .line 609889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609881
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 609876
    check-cast p2, LX/CzL;

    .line 609877
    const v0, 0x7f0d2ca1

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609878
    const v0, 0x7f0d1771

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609879
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609880
    const/4 v0, 0x1

    return v0
.end method
