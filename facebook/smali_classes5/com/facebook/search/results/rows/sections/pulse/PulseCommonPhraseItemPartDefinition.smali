.class public Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;",
        "Landroid/text/Spannable;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/1nD;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609417
    new-instance v0, LX/3bB;

    invoke-direct {v0}, LX/3bB;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->a:LX/1Cz;

    .line 609418
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40000000    # -2.0f

    .line 609419
    iput v1, v0, LX/1UY;->b:F

    .line 609420
    move-object v0, v0

    .line 609421
    const/high16 v1, 0x40c00000    # 6.0f

    .line 609422
    iput v1, v0, LX/1UY;->c:F

    .line 609423
    move-object v0, v0

    .line 609424
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609425
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609426
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609427
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609428
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->e:LX/1nD;

    .line 609429
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 609430
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;
    .locals 7

    .prologue
    .line 609431
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    monitor-enter v1

    .line 609432
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609433
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609434
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609435
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609436
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v5

    check-cast v5, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;)V

    .line 609437
    move-object v0, p0

    .line 609438
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609439
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609440
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609441
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 609442
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 609443
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    check-cast p3, LX/1Pn;

    .line 609444
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609445
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EOl;

    invoke-direct {v1, p0, p2, p3}, LX/EOl;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609446
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->n()I

    move-result v0

    .line 609447
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->m()I

    move-result v1

    .line 609448
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->p()Ljava/lang/String;

    move-result-object v2

    .line 609449
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 609450
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/2addr v1, v0

    const/16 v4, 0x11

    invoke-interface {v3, v2, v0, v1, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 609451
    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5557101e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609452
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    check-cast p2, Landroid/text/Spannable;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 609453
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 609454
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 609455
    :cond_0
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 609456
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0108

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->j()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->j()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v5, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 609457
    const/16 v1, 0x1f

    const v2, -0x3f5d6fe0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609458
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    .line 609459
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
