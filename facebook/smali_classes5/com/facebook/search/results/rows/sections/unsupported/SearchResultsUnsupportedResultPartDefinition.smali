.class public Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EPd;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/3aw;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 608596
    const v0, 0x7f031548

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->a:LX/1Cz;

    .line 608597
    new-instance v0, LX/3aw;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOX:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->b:LX/3aw;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608598
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608599
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608600
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 608601
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;
    .locals 5

    .prologue
    .line 608602
    const-class v1, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    monitor-enter v1

    .line 608603
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608604
    sput-object v2, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608605
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608606
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608607
    new-instance p0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V

    .line 608608
    move-object v0, p0

    .line 608609
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608610
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608611
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608613
    sget-object v0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 608614
    check-cast p2, LX/EPd;

    .line 608615
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "[Employees Only] Unsupported Result\nGraphQL type: %s\nType: %s\nDisplay Style: %s\nPlease rage shake and file a report."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, LX/EPd;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, LX/EPd;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p2, LX/EPd;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 608616
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608617
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->d:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    sget-object v1, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->b:LX/3aw;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608618
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608619
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 608620
    return v0
.end method
