.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f400000    # -6.0f

    .line 609380
    const v0, 0x7f0310a8

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->a:LX/1Cz;

    .line 609381
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 609382
    iput v1, v0, LX/1UY;->b:F

    .line 609383
    move-object v0, v0

    .line 609384
    iput v1, v0, LX/1UY;->c:F

    .line 609385
    move-object v0, v0

    .line 609386
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609376
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609377
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609378
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 609379
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;
    .locals 5

    .prologue
    .line 609365
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    monitor-enter v1

    .line 609366
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609367
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609368
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609369
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609370
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 609371
    move-object v0, p0

    .line 609372
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609373
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609374
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609375
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609364
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 609360
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 609361
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609362
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609363
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609358
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 609359
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
