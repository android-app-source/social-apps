.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsModuleTitleInterfaces$SearchResultsModuleTitle;",
        ">;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final h:LX/0wM;

.field private final i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const v6, -0xcac97

    const v5, -0x943145

    const v4, -0xbd984e

    const v3, -0xbf7f01

    .line 605854
    const v0, 0x7f0312be

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a:LX/1Cz;

    .line 605855
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const v1, 0x7f0200aa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x5c318f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0200bb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0202cb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0202e0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x25d6d7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f020683

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f020ca2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f020e0b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0213f1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x86dc5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f02142b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f021460

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f021491

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0xaea91

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f021118

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0xf66e01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f02161b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0217dd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f021894

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, -0x1dd7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f0202ac

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0xb95800

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f021a5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f020e4a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x7f02161c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    .line 605856
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0202cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0202e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f020e0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021894

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021491

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021118

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f02161b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f02161c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f02161c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0217dd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GAMETIME_FAN_FAVORITE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021894

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f02161b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const v2, 0x7f020e4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->c:LX/0P1;

    .line 605857
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const v1, -0x70671f61

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x2238d5d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x37da4ee4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x5c6a201d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x3ba7c68c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0202ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x25b64cd3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0202cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x4bf8efca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0202e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x3c4e1b9e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020426

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0xc99f576

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021491

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x5c030707

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021118

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x59549df1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x2ea62261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x2a731e0d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020e0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0xd7270e3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02161c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x71885993

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0217dd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0xc17efcc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021894

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x5eba76a7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02161b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x5bd4bd3d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x696d5b57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x5389cb28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020e4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->d:LX/0P1;

    .line 605858
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f0200aa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f020683

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f020ca2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f0213f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f021460

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f021491

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f0200bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f02142b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v2, 0x7f021a5a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605812
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 605813
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->f:Landroid/content/Context;

    .line 605814
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 605815
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->h:LX/0wM;

    .line 605816
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 605817
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;
    .locals 7

    .prologue
    .line 605843
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    monitor-enter v1

    .line 605844
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 605845
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 605846
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605847
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 605848
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 605849
    move-object v0, p0

    .line 605850
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 605851
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605852
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 605853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/Integer;
    .locals 1
    .param p1    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 605840
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->c:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 605841
    if-eqz v0, :cond_0

    .line 605842
    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->e:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0Px;)Ljava/lang/Integer;
    .locals 5
    .param p0    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 605859
    const/4 v1, 0x0

    .line 605860
    const/4 v0, 0x0

    .line 605861
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 605862
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const/4 v3, 0x0

    .line 605863
    if-nez p0, :cond_2

    move-object v2, v3

    .line 605864
    :cond_0
    :goto_1
    move-object v0, v2

    .line 605865
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 605866
    :cond_1
    return-object v0

    .line 605867
    :cond_2
    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->d:LX/0P1;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 605868
    if-nez v2, :cond_0

    .line 605869
    if-nez v0, :cond_3

    move-object v2, v3

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->e:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 605839
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 605824
    check-cast p2, LX/CzL;

    .line 605825
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 605826
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 605827
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605828
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    .line 605829
    invoke-virtual {p2}, LX/CzL;->g()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0Px;)Ljava/lang/Integer;

    move-result-object v1

    .line 605830
    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 605831
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v3, LX/EMz;->a:LX/1X6;

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 605832
    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 605833
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->h:LX/0wM;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 605834
    :goto_1
    return-object v0

    .line 605835
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 605836
    :cond_1
    if-eqz v0, :cond_2

    .line 605837
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 605838
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x9c69a67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 605821
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 605822
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 605823
    const/16 v1, 0x1f

    const v2, -0x382e0eb0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 605818
    check-cast p1, LX/CzL;

    .line 605819
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 605820
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
