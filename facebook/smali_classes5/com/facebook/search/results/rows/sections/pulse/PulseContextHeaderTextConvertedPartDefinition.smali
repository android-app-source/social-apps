.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/A3T;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f400000    # -6.0f

    .line 610768
    const v0, 0x7f0310a8

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->a:LX/1Cz;

    .line 610769
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    .line 610770
    iput v1, v0, LX/1UY;->b:F

    .line 610771
    move-object v0, v0

    .line 610772
    iput v1, v0, LX/1UY;->c:F

    .line 610773
    move-object v0, v0

    .line 610774
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610775
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610776
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610777
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 610778
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;
    .locals 5

    .prologue
    .line 610779
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    monitor-enter v1

    .line 610780
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610781
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610782
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610783
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610784
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 610785
    move-object v0, p0

    .line 610786
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610787
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610788
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610790
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 610791
    check-cast p2, LX/A3T;

    .line 610792
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610793
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p2}, LX/A3T;->cq()LX/A4I;

    move-result-object v1

    invoke-interface {v1}, LX/A4I;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610794
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610795
    check-cast p1, LX/A3T;

    .line 610796
    invoke-interface {p1}, LX/A3T;->cq()LX/A4I;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/A3T;->cq()LX/A4I;

    move-result-object v0

    invoke-interface {v0}, LX/A4I;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
