.class public Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsRelatedSharesModuleInterfaces$SearchResultsRelatedSharesModule$ModuleResults$Edges;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final e:LX/154;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610030
    new-instance v0, LX/3bJ;

    invoke-direct {v0}, LX/3bJ;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/154;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610043
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610044
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    .line 610045
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610046
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 610047
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->e:LX/154;

    .line 610048
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;
    .locals 7

    .prologue
    .line 610031
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    monitor-enter v1

    .line 610032
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610033
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610034
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610035
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610036
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v6

    check-cast v6, LX/154;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/154;)V

    .line 610037
    move-object v0, p0

    .line 610038
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610039
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610040
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610041
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 610042
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 610016
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxk;

    const/4 v6, 0x0

    .line 610017
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610018
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 610019
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    .line 610020
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 610021
    invoke-static {v1, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    .line 610022
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610023
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/3ap;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, v6, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610024
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;->a()I

    move-result v0

    .line 610025
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->e:LX/154;

    invoke-virtual {v1, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 610026
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0109

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 610027
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610028
    return-object v6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610029
    const/4 v0, 0x1

    return v0
.end method
