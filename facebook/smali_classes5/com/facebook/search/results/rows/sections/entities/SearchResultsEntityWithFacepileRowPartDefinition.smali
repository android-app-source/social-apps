.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ":",
        "LX/Cxc;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field public final h:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 607558
    const v0, 0x7f030492

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607559
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 607560
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 607561
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    .line 607562
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    .line 607563
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 607564
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 607565
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607566
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->h:LX/0Uh;

    .line 607567
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;
    .locals 11

    .prologue
    .line 607568
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    monitor-enter v1

    .line 607569
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607570
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607571
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607572
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 607573
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/0Uh;)V

    .line 607574
    move-object v0, v3

    .line 607575
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607576
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607577
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/EJZ;LX/1Pn;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EJZ;",
            "TE;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 607579
    check-cast p2, LX/Cxc;

    iget-object v0, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p2, v0}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->h:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607580
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v0

    .line 607581
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607582
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 607583
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    .line 607584
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607585
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/3ap;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607586
    const v0, 0x7f0d0d8e

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    new-instance v2, LX/ELL;

    const/4 v3, 0x4

    iget-object v4, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v4}, LX/ELM;->e(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/ELL;-><init>(ILX/0Px;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 607587
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-direct {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a(LX/EJZ;LX/1Pn;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607588
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->h:LX/0Uh;

    invoke-static {p2, v1}, LX/ELM;->a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607589
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->g:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 607590
    check-cast p3, LX/Cxc;

    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {p3, v1}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->h:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 607591
    :cond_0
    const/4 v1, 0x1

    invoke-static {p2, v1}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v1

    .line 607592
    :goto_0
    move-object v1, v1

    .line 607593
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607594
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v1, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->f(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 607595
    check-cast p1, LX/EJZ;

    .line 607596
    iget-object v0, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v0}, LX/ELM;->h(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method
