.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataInterfaces$SearchResultsTrendingTopicData;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/EK4;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/EK4;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0Uh;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608685
    new-instance v0, LX/3ay;

    invoke-direct {v0}, LX/3ay;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    .line 608686
    const-class v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608681
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608682
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->b:LX/0Uh;

    .line 608683
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->c:LX/0Ot;

    .line 608684
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/EK4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608687
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x140f536e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 608667
    check-cast p1, LX/CzL;

    check-cast p3, LX/1Ps;

    check-cast p4, LX/EK4;

    const/4 v4, 0x0

    .line 608668
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 608669
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 608670
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->c()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 608671
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->b:LX/0Uh;

    sget p2, LX/2SU;->T:I

    invoke-virtual {v2, p2, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 608672
    :goto_0
    sget-object v4, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v5, v4, v2}, LX/EK4;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V

    .line 608673
    invoke-static {v5}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v4

    .line 608674
    check-cast p3, LX/1Pp;

    .line 608675
    iget-object v2, p4, LX/EK4;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v2, v2

    .line 608676
    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    const/4 v5, 0x0

    sget-object p2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v2, v5, v4, p2}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 608677
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->fr_()Ljava/lang/String;

    move-result-object v1

    .line 608678
    iget-object v2, p4, LX/EK4;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 608679
    const/16 v1, 0x1f

    const v2, -0x5a6074e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move v2, v4

    .line 608680
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 608663
    check-cast p1, LX/CzL;

    .line 608664
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608665
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 608666
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->c()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
