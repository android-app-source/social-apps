.class public Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x40800000    # 4.0f

    .line 608117
    new-instance v0, LX/3as;

    invoke-direct {v0}, LX/3as;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->a:LX/1Cz;

    .line 608118
    invoke-static {}, LX/1UY;->b()LX/1UY;

    move-result-object v0

    .line 608119
    iput v1, v0, LX/1UY;->b:F

    .line 608120
    move-object v0, v0

    .line 608121
    iput v1, v0, LX/1UY;->c:F

    .line 608122
    move-object v0, v0

    .line 608123
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608083
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608084
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608085
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608086
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;
    .locals 5

    .prologue
    .line 608106
    const-class v1, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;

    monitor-enter v1

    .line 608107
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608108
    sput-object v2, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608109
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608110
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608111
    new-instance p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 608112
    move-object v0, p0

    .line 608113
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608114
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608115
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 608124
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 608087
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/4 v5, 0x0

    .line 608088
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 608089
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;

    .line 608090
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608091
    check-cast p3, LX/CxV;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    .line 608092
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 608093
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 608094
    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 608095
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608096
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;->a:Ljava/lang/String;

    move-object v0, v2

    .line 608097
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608098
    :goto_0
    return-object v5

    .line 608099
    :cond_1
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 608100
    invoke-virtual {v2, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 608101
    new-instance v2, Landroid/text/SpannableString;

    .line 608102
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;->a:Ljava/lang/String;

    move-object v0, v3

    .line 608103
    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 608104
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-interface {v2, v0, v1, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 608105
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608082
    const/4 v0, 0x1

    return v0
.end method
