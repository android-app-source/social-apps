.class public Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EP3;",
        "LX/EP4;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final c:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609526
    const-class v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 609527
    const v0, 0x7f0310aa

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609523
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609524
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->c:LX/0wM;

    .line 609525
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;
    .locals 4

    .prologue
    .line 609512
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    monitor-enter v1

    .line 609513
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609514
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609515
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609516
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609517
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;-><init>(LX/0wM;)V

    .line 609518
    move-object v0, p0

    .line 609519
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609520
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609521
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609522
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609491
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 609502
    check-cast p2, LX/EP3;

    check-cast p3, LX/1Ps;

    .line 609503
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 609504
    iget-object v1, p2, LX/EP3;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p2, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    if-nez v1, :cond_0

    .line 609505
    new-instance v1, LX/EP4;

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v2

    iget v3, p2, LX/EP3;->b:I

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f082275

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->c:LX/0wM;

    iget-object v5, p2, LX/EP3;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const v6, -0xa76f01

    invoke-virtual {v4, v5, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/EP4;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 609506
    :goto_0
    move-object v0, v1

    .line 609507
    return-object v0

    .line 609508
    :cond_0
    const v1, 0x7f082276

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v4

    iget v5, p2, LX/EP3;->b:I

    int-to-long v5, v5

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 609509
    iget-object v1, p2, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v1, :cond_1

    iget-object v1, p2, LX/EP3;->c:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 609510
    :goto_1
    new-instance v2, LX/EP4;

    iget-object v4, p2, LX/EP3;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v3, v1}, LX/EP4;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v1, v2

    goto :goto_0

    .line 609511
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x596c72a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609493
    check-cast p2, LX/EP4;

    check-cast p4, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;

    .line 609494
    if-eqz p2, :cond_2

    .line 609495
    iget-object v1, p2, LX/EP4;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 609496
    iget-object v1, p2, LX/EP4;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->setEmotionalDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 609497
    :cond_0
    iget-object v1, p2, LX/EP4;->d:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 609498
    iget-object v1, p2, LX/EP4;->d:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 609499
    :cond_1
    iget-object v1, p2, LX/EP4;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->setEmotionName(Ljava/lang/String;)V

    .line 609500
    iget-object v1, p2, LX/EP4;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->setEmotionCount(Ljava/lang/String;)V

    .line 609501
    :cond_2
    const/16 v1, 0x1f

    const v2, 0xedefafe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609492
    const/4 v0, 0x1

    return v0
.end method
