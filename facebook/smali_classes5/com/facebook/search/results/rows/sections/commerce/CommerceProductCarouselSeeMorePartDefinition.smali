.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

.field private final d:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610335
    new-instance v0, LX/3bL;

    invoke-direct {v0}, LX/3bL;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610330
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610331
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    .line 610332
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    .line 610333
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->d:LX/0ad;

    .line 610334
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ")",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 610325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p0, v0, :cond_0

    const-string v0, "Shops"

    .line 610326
    :goto_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p0, v1, :cond_1

    const-string v1, "shops"

    .line 610327
    :goto_1
    new-instance v2, LX/CyH;

    const-string v3, "rp_commerce_source"

    invoke-direct {v2, v3, v0, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 610328
    :cond_0
    const-string v0, "People and Groups"

    goto :goto_0

    .line 610329
    :cond_1
    const-string v1, "groups"

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;
    .locals 6

    .prologue
    .line 610305
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    monitor-enter v1

    .line 610306
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610307
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610310
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;LX/0ad;)V

    .line 610311
    move-object v0, p0

    .line 610312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610324
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 610317
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 610318
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 610319
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)LX/0Px;

    move-result-object v1

    .line 610320
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    new-instance v3, LX/EKB;

    sget-object v4, LX/CyI;->MARKETPLACE:LX/CyI;

    .line 610321
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 610322
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-direct {v3, v4, v0, v1}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/0Px;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610323
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610316
    const/4 v0, 0x1

    return v0
.end method
