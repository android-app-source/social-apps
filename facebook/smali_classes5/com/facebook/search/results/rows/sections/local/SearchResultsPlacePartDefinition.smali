.class public Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxc;",
        ":",
        "LX/Cxe;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fig/listitem/FigListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static p:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

.field public final i:LX/0Uh;

.field private final j:LX/8hv;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/8i7;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608404
    const v0, 0x7f030f6a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;LX/0Uh;LX/8hv;LX/0Ot;LX/0Ot;LX/0Ot;LX/8i7;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;",
            "Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/8hv;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/8i7;",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608405
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608406
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->m:LX/0Ot;

    .line 608407
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->i:LX/0Uh;

    .line 608408
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->l:LX/0Ot;

    .line 608409
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->j:LX/8hv;

    .line 608410
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->n:LX/8i7;

    .line 608411
    iput-object p14, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->o:LX/0Ot;

    .line 608412
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608413
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    .line 608414
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->k:LX/0Ot;

    .line 608415
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;

    .line 608416
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->e:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    .line 608417
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->d:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    .line 608418
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->g:Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;

    .line 608419
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->h:Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    .line 608420
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;
    .locals 3

    .prologue
    .line 608421
    const-class v1, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    monitor-enter v1

    .line 608422
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608423
    sput-object v2, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608426
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608427
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608428
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Ps;ZLcom/facebook/fig/button/FigToggleButton;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "TE;Z",
            "Lcom/facebook/fig/button/FigToggleButton;",
            ")V"
        }
    .end annotation

    .prologue
    .line 608429
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/5up;

    if-eqz p3, :cond_0

    sget-object v0, LX/5uo;->SAVE:LX/5uo;

    move-object v7, v0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v8

    const-string v9, "native_search"

    const-string v10, "toggle_button"

    new-instance v0, LX/ENR;

    move-object v1, p0

    move v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/ENR;-><init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;ZLcom/facebook/graphql/model/GraphQLNode;LX/1Ps;Lcom/facebook/fig/button/FigToggleButton;)V

    move-object v1, v6

    move-object v2, v7

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 608430
    return-void

    .line 608431
    :cond_0
    sget-object v0, LX/5uo;->UNSAVE:LX/5uo;

    move-object v7, v0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;
    .locals 15

    .prologue
    .line 608432
    new-instance v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {p0}, LX/8hv;->a(LX/0QB;)LX/8hv;

    move-result-object v9

    check-cast v9, LX/8hv;

    const/16 v10, 0x3398

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x32d4

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x12c4

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v13

    check-cast v13, LX/8i7;

    const/16 v14, 0x329d

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;LX/0Uh;LX/8hv;LX/0Ot;LX/0Ot;LX/0Ot;LX/8i7;LX/0Ot;)V

    .line 608433
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fig/listitem/FigListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608434
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 608435
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Ps;

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 608436
    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    .line 608437
    :goto_0
    new-instance v1, LX/1X6;

    sget-object v2, LX/3ap;->a:LX/1Ua;

    invoke-direct {v1, v7, v2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 608438
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    new-instance v3, LX/8Cn;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->n:LX/8i7;

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_1
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/8Cn;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608439
    const/4 v0, 0x0

    .line 608440
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->i:LX/0Uh;

    const/16 v3, 0x23e

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 608441
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    .line 608442
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 608443
    :cond_1
    if-eqz v2, :cond_2

    if-nez v0, :cond_6

    .line 608444
    :cond_2
    const/4 v0, 0x0

    .line 608445
    :goto_2
    move-object v0, v0

    .line 608446
    if-eqz v0, :cond_3

    .line 608447
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->h:Lcom/facebook/multirow/parts/FigListItemToggleButtonPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608448
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608449
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->g:Lcom/facebook/multirow/parts/FigListItemThumbnailUriPartDefinition;

    invoke-static {p2}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608450
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemBodyPartDefinition;

    new-instance v2, LX/8Cl;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->j:LX/8hv;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 608451
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 608452
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fX()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v5

    .line 608453
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->gx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    .line 608454
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->gy()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v10

    .line 608455
    new-instance v11, Landroid/text/SpannableStringBuilder;

    invoke-direct {v11}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 608456
    invoke-static {v3, v4, v5}, LX/8hv;->a(LX/8hv;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLRating;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v11, v5}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608457
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->ip()LX/0Px;

    move-result-object v5

    const/4 p3, 0x0

    invoke-virtual {v5, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v11, v5}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608458
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->gX()Ljava/lang/String;

    move-result-object v5

    invoke-static {v11, v5}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608459
    invoke-static {v8, v9, v10}, LX/8hv;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v11, v5}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 608460
    move-object v3, v11

    .line 608461
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/8Cl;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608462
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->e:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    new-instance v2, LX/8Cm;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->j:LX/8hv;

    invoke-virtual {v3, p2}, LX/8hv;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/8Cm;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608463
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->d:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    .line 608464
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Z)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v2, v2

    .line 608465
    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608466
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608467
    return-object v7

    .line 608468
    :cond_4
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto/16 :goto_0

    .line 608469
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 608470
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3, v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 608471
    new-instance v2, LX/8Co;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f020781

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v4, LX/ENQ;

    invoke-direct {v4, p0, p2, p3}, LX/ENQ;-><init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Ps;)V

    invoke-direct {v2, v0, v3, v4}, LX/8Co;-><init>(Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V

    move-object v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608472
    const/4 v0, 0x1

    move v0, v0

    .line 608473
    return v0
.end method
