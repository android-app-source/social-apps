.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/A3T;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static final d:LX/1X6;

.field private static j:LX/0Xm;


# instance fields
.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final h:LX/1xP;

.field public final i:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 610839
    const v0, 0x7f0310a7

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->a:LX/1Cz;

    .line 610840
    const-class v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 610841
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 610842
    iput v1, v0, LX/1UY;->b:F

    .line 610843
    move-object v0, v0

    .line 610844
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 610845
    iput v1, v0, LX/1UY;->d:F

    .line 610846
    move-object v0, v0

    .line 610847
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->c:LX/1Ua;

    .line 610848
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->c:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/1xP;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610832
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610833
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610834
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 610835
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 610836
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->h:LX/1xP;

    .line 610837
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->i:LX/CvY;

    .line 610838
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;
    .locals 9

    .prologue
    .line 610821
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    monitor-enter v1

    .line 610822
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610823
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610824
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610825
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610826
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v7

    check-cast v7, LX/1xP;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v8

    check-cast v8, LX/CvY;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;LX/1xP;LX/CvY;)V

    .line 610827
    move-object v0, v3

    .line 610828
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610829
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610830
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610820
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 610804
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    const/4 v1, 0x0

    .line 610805
    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 610806
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->d:LX/1X6;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610807
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610808
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->ct()LX/A4F;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610809
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610810
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->ct()LX/A4F;

    move-result-object v0

    invoke-interface {v0}, LX/A4F;->fm_()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel$PulseCoverPhotoModel;

    move-result-object v0

    .line 610811
    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 610812
    :goto_1
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->g:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v0

    sget-object v4, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 610813
    iput-object v4, v0, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 610814
    move-object v0, v0

    .line 610815
    invoke-virtual {v0}, LX/2f8;->a()LX/2f9;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610816
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v3, LX/EOw;

    invoke-direct {v3, p0, p3, p2, v2}, LX/EOw;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;LX/1Ps;LX/CzL;Landroid/content/Context;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610817
    return-object v1

    :cond_0
    move-object v0, v1

    .line 610818
    goto :goto_0

    .line 610819
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel$PulseCoverPhotoModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610797
    check-cast p1, LX/CzL;

    .line 610798
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610799
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->ct()LX/A4F;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610800
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610801
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->ct()LX/A4F;

    move-result-object v0

    invoke-interface {v0}, LX/A4F;->fm_()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel$PulseCoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610802
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610803
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->ct()LX/A4F;

    move-result-object v0

    invoke-interface {v0}, LX/A4F;->fm_()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel$PulseCoverPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$LinkMediaModel$PulseCoverPhotoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
