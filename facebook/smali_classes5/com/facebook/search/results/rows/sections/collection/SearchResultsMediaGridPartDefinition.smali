.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsCommonMediaModuleInterfaces$SearchResultsCommonMediaModule;",
        ">;",
        "LX/EJY;",
        "TE;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
        "<",
        "LX/ByV;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:LX/1X6;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static r:LX/0Xm;


# instance fields
.field private final e:LX/26H;

.field private final f:LX/1Ad;

.field public final g:LX/1qa;

.field public final h:LX/23R;

.field private final i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final j:LX/ByX;

.field private final k:LX/Byg;

.field private final l:LX/EKF;

.field public final m:LX/1nD;

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/CvY;

.field public final p:LX/D3w;

.field private final q:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 607994
    const-class v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->c:Ljava/lang/Class;

    .line 607995
    new-instance v0, LX/3ar;

    invoke-direct {v0}, LX/3ar;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    .line 607996
    const-class v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 607997
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->b:LX/1X6;

    return-void
.end method

.method public constructor <init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/ByX;LX/Byg;LX/EKF;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/CvY;LX/D3w;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 607999
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608000
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    .line 608001
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->m:LX/1nD;

    .line 608002
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->e:LX/26H;

    .line 608003
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->f:LX/1Ad;

    .line 608004
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->g:LX/1qa;

    .line 608005
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->h:LX/23R;

    .line 608006
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608007
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->j:LX/ByX;

    .line 608008
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->l:LX/EKF;

    .line 608009
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->k:LX/Byg;

    .line 608010
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->o:LX/CvY;

    .line 608011
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->p:LX/D3w;

    .line 608012
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->q:LX/0ad;

    .line 608013
    return-void
.end method

.method private a(LX/0Px;LX/0Px;LX/CzL;LX/1Ps;)LX/Aj7;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;>;",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsCommonMediaModuleInterfaces$SearchResultsCommonMediaModule;",
            ">;TE;)",
            "LX/Aj7",
            "<",
            "LX/ByV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607998
    new-instance v0, LX/EJX;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/EJX;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;LX/CzL;LX/1Ps;LX/0Px;LX/0Px;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;)LX/CvY;
    .locals 1

    .prologue
    .line 607954
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->o:LX/CvY;

    return-object v0
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 607986
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 607987
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 607988
    iput v1, v0, LX/2dc;->c:I

    .line 607989
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 607990
    iput v1, v0, LX/2dc;->i:I

    .line 607991
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 607992
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 607993
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;
    .locals 3

    .prologue
    .line 607978
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    monitor-enter v1

    .line 607979
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 607980
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 607981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 607983
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607984
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 607985
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1qa;Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/ByV;ILX/0Px;LX/23R;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qa;",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;",
            "LX/ByV;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;>;",
            "LX/23R;",
            ")V"
        }
    .end annotation

    .prologue
    .line 607962
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 607963
    invoke-virtual {p4}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 607964
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 607965
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 607966
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x4984e12

    if-ne v4, v5, :cond_0

    .line 607967
    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 607968
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 607969
    :cond_1
    invoke-virtual {p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v1, LX/26P;->Photo:LX/26P;

    invoke-virtual {p0, v0, v1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v1

    .line 607970
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->e(LX/0Px;)LX/9hE;

    move-result-object v2

    invoke-virtual {p4, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 607971
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 607972
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    sget-object v2, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    invoke-virtual {v0, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    const/4 v2, 0x1

    .line 607973
    iput-boolean v2, v0, LX/9hD;->C:Z

    .line 607974
    move-object v0, v0

    .line 607975
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    .line 607976
    invoke-virtual {p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p1, v1, p3}, LX/BrA;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;I)LX/9hN;

    move-result-object v1

    invoke-interface {p5, v2, v0, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 607977
    return-void
.end method

.method public static a(LX/EJY;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EJY;",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 607955
    iget-object v0, p0, LX/EJY;->b:LX/26O;

    iget-object v1, p0, LX/EJY;->a:[LX/1aZ;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 607956
    iget-object v0, p0, LX/EJY;->c:LX/Aj7;

    .line 607957
    iput-object v0, p1, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 607958
    iget-boolean v0, p0, LX/EJY;->d:Z

    if-eqz v0, :cond_0

    .line 607959
    const/16 v0, 0x64

    .line 607960
    iput v0, p1, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 607961
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 608014
    invoke-virtual {p0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 608015
    const/4 v0, 0x0

    .line 608016
    iput v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 608017
    const/4 v0, 0x0

    .line 608018
    iput-object v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 608019
    return-void
.end method

.method private static a(LX/1Ps;LX/26O;LX/1qa;LX/1Ad;)[LX/1aZ;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InstanceMethodCanBeStatic"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/26O",
            "<",
            "LX/ByV;",
            ">;",
            "LX/1qa;",
            "LX/1Ad;",
            ")[",
            "LX/1aZ;"
        }
    .end annotation

    .prologue
    .line 607864
    invoke-virtual {p1}, LX/26O;->a()LX/0Px;

    move-result-object v2

    .line 607865
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    new-array v3, v0, [LX/1aZ;

    .line 607866
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    .line 607867
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 607868
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ByV;

    invoke-virtual {v0}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v5, LX/26P;->Photo:LX/26P;

    invoke-virtual {p2, v0, v5}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v5

    .line 607869
    invoke-virtual {v4, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    aput-object v0, v3, v1

    move-object v0, p0

    .line 607870
    check-cast v0, LX/1Pp;

    aget-object v6, v3, v1

    const/4 v7, 0x0

    sget-object v8, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v6, v7, v5, v8}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 607871
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 607872
    :cond_0
    return-object v3
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;
    .locals 14

    .prologue
    .line 607873
    new-instance v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    const-class v1, LX/26H;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/26H;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v3

    check-cast v3, LX/1qa;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v4

    check-cast v4, LX/23R;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v6, LX/ByX;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ByX;

    const-class v7, LX/Byg;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Byg;

    const-class v8, LX/EKF;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/EKF;

    invoke-static {p0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v9

    check-cast v9, LX/1nD;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v11

    check-cast v11, LX/CvY;

    invoke-static {p0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v12

    check-cast v12, LX/D3w;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;-><init>(LX/26H;LX/1Ad;LX/1qa;LX/23R;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/ByX;LX/Byg;LX/EKF;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/CvY;LX/D3w;LX/0ad;)V

    .line 607874
    return-object v0
.end method

.method public static b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Z
    .locals 2

    .prologue
    .line 607875
    invoke-static {p0}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 607876
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 607877
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 607878
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 607879
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Q()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v4

    .line 607880
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->i:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 607881
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 607882
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    move v1, v2

    .line 607883
    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 607884
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->c()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v7

    .line 607885
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v8, 0x4ed245b

    if-ne v0, v8, :cond_2

    .line 607886
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 607887
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 607888
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 607889
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 607890
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 607891
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 607892
    goto :goto_1

    .line 607893
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v8, 0x1eaef984

    if-ne v0, v8, :cond_4

    .line 607894
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 607895
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 607896
    :cond_3
    const/4 v0, 0x0

    .line 607897
    :goto_3
    move-object v0, v0

    .line 607898
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 607899
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v8, 0x4984e12

    if-ne v0, v8, :cond_5

    .line 607900
    new-instance v0, LX/4Xy;

    invoke-direct {v0}, LX/4Xy;-><init>()V

    .line 607901
    invoke-interface {v7}, LX/8d2;->dW_()Ljava/lang/String;

    move-result-object v8

    .line 607902
    iput-object v8, v0, LX/4Xy;->I:Ljava/lang/String;

    .line 607903
    invoke-interface {v7}, LX/8d2;->B()LX/1Fb;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    .line 607904
    iput-object v8, v0, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 607905
    invoke-interface {v7}, LX/8d2;->C()LX/1Fb;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    .line 607906
    iput-object v8, v0, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 607907
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    move-object v0, v0

    .line 607908
    invoke-static {v0}, LX/6X5;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 607909
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 607910
    :cond_5
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->c:Ljava/lang/Class;

    const-string v8, "Result object is not of video, external url or photo type"

    invoke-static {v0, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v3

    goto/16 :goto_2

    .line 607911
    :cond_6
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 607912
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Z

    move-result v0

    .line 607913
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 607914
    if-eqz v0, :cond_8

    .line 607915
    invoke-static {v1}, LX/Byg;->a(LX/0Px;)LX/Byf;

    move-result-object v0

    .line 607916
    :goto_4
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->e:LX/26H;

    invoke-virtual {v3, v0}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v0

    .line 607917
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {p0, v3, v1, p2, p3}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/0Px;LX/0Px;LX/CzL;LX/1Ps;)LX/Aj7;

    move-result-object v3

    .line 607918
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->g:LX/1qa;

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->f:LX/1Ad;

    invoke-static {p3, v0, v4, v5}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1Ps;LX/26O;LX/1qa;LX/1Ad;)[LX/1aZ;

    move-result-object v4

    .line 607919
    new-instance v5, LX/EJY;

    .line 607920
    iget-object v6, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v6, v6

    .line 607921
    invoke-static {v6}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, -0x696d5b57

    if-eq v6, v7, :cond_7

    .line 607922
    iget-object v6, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v6, v6

    .line 607923
    invoke-static {v6}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, 0x581d640b

    if-eq v6, v7, :cond_7

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v6, 0x3

    if-lt v1, v6, :cond_7

    const/4 v2, 0x1

    :cond_7
    invoke-direct {v5, v4, v0, v3, v2}, LX/EJY;-><init>([LX/1aZ;LX/26O;LX/Aj7;Z)V

    return-object v5

    .line 607924
    :cond_8
    invoke-static {v1}, LX/ByX;->a(LX/0Px;)LX/ByW;

    move-result-object v0

    goto :goto_4

    .line 607925
    :cond_9
    new-instance v0, LX/4XB;

    invoke-direct {v0}, LX/4XB;-><init>()V

    .line 607926
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;->d()LX/1Fb;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    .line 607927
    iput-object v8, v0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 607928
    move-object v8, v0

    .line 607929
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;->e()LX/1Fb;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    .line 607930
    iput-object p1, v8, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 607931
    move-object v8, v8

    .line 607932
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object p1

    .line 607933
    iput-object p1, v8, LX/4XB;->T:Ljava/lang/String;

    .line 607934
    move-object v8, v8

    .line 607935
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p1

    .line 607936
    iput-object p1, v8, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 607937
    move-object v8, v8

    .line 607938
    invoke-virtual {v8}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    .line 607939
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto/16 :goto_3
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x527ba8a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 607940
    check-cast p2, LX/EJY;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-static {p2, p4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/EJY;Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    const/16 v1, 0x1f

    const v2, 0x57970f59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 607941
    check-cast p1, LX/CzL;

    .line 607942
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 607943
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Q()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    .line 607944
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 607945
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 607946
    invoke-static {v0}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v1, :cond_0

    .line 607947
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 607948
    invoke-static {v0}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x696d5b57

    if-eq v0, v1, :cond_0

    .line 607949
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 607950
    invoke-static {v0}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 607951
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 607952
    invoke-static {p4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V

    .line 607953
    return-void
.end method
