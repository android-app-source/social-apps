.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsNavigationalExternalUrlInterfaces$SearchResultsNavigationalExternalUrl;",
        ">;",
        "LX/ELW;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final c:LX/0wM;

.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/17W;

.field public final g:LX/CvY;

.field private final h:Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

.field private final i:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final j:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609952
    const v0, 0x7f03048f

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;LX/CvY;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609953
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609954
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609955
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->c:LX/0wM;

    .line 609956
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 609957
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609958
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->f:LX/17W;

    .line 609959
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->g:LX/CvY;

    .line 609960
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->h:Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    .line 609961
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 609962
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->j:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 609963
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;
    .locals 13

    .prologue
    .line 609964
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    monitor-enter v1

    .line 609965
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609966
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609967
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609968
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609969
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v9

    check-cast v9, LX/CvY;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;LX/CvY;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;)V

    .line 609970
    move-object v0, v3

    .line 609971
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609972
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609973
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609974
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609975
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 609976
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 609977
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, LX/3ap;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609978
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->c:LX/0wM;

    const v1, 0x7f0208f8

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 609979
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 609980
    const v2, 0x7f08226a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 609981
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 609982
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 609983
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609984
    new-instance v3, LX/ELV;

    invoke-direct {v3, p0, p2, p3}, LX/ELV;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v3, v3

    .line 609985
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v4, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609986
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 609987
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->j:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609988
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->i:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v4, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609989
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->h:Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    .line 609990
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 609991
    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609992
    new-instance v0, LX/ELW;

    invoke-direct {v0, v1, v2, v3}, LX/ELW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x89288ca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609993
    check-cast p2, LX/ELW;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 609994
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 609995
    instance-of v1, v1, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    if-eqz v1, :cond_0

    .line 609996
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 609997
    check-cast v1, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    iget-object v2, p2, LX/ELW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 609998
    :goto_0
    iget-object v1, p2, LX/ELW;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 609999
    iget-object v1, p2, LX/ELW;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 610000
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 610001
    iget-object v1, p2, LX/ELW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610002
    const/16 v1, 0x1f

    const v2, -0x45cea30b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 610003
    :cond_0
    iget-object v1, p2, LX/ELW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 610004
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610005
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2
    .annotation build Lcom/facebook/infer/annotation/NoAllocation;
    .end annotation

    .annotation build Lcom/facebook/infer/annotation/PerformanceCritical;
    .end annotation

    .prologue
    .line 610006
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x0

    .line 610007
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 610008
    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    if-eqz v0, :cond_0

    .line 610009
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 610010
    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 610011
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 610012
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610013
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 610014
    return-void

    .line 610015
    :cond_0
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
