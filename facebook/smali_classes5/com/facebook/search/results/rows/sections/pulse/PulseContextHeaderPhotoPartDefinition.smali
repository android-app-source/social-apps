.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxG;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static final c:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/1xP;

.field public final g:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609320
    const-class v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 609321
    const v0, 0x7f0310a7

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    .line 609322
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 609323
    iput v1, v0, LX/1UY;->b:F

    .line 609324
    move-object v0, v0

    .line 609325
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 609326
    iput v1, v0, LX/1UY;->d:F

    .line 609327
    move-object v0, v0

    .line 609328
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1xP;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609351
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609352
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 609353
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 609354
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->f:LX/1xP;

    .line 609355
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->g:LX/CvY;

    .line 609356
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;
    .locals 7

    .prologue
    .line 609340
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    monitor-enter v1

    .line 609341
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609342
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609345
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v5

    check-cast v5, LX/1xP;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v6

    check-cast v6, LX/CvY;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1xP;LX/CvY;)V

    .line 609346
    move-object v0, p0

    .line 609347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 609357
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 609335
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    check-cast p3, LX/1Ps;

    const/4 v5, 0x0

    .line 609336
    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 609337
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->c:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, v5, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609338
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/EOx;

    invoke-direct {v2, p0, p3, p2, v0}, LX/EOx;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Landroid/content/Context;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 609339
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1168fd7e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 609331
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 609332
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 609333
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 609334
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x11d3051b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609329
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 609330
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
