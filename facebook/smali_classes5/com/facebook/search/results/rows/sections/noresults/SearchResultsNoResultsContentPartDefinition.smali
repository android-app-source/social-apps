.class public Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/ENm;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608156
    new-instance v0, LX/3at;

    invoke-direct {v0}, LX/3at;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608153
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608154
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608155
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;
    .locals 4

    .prologue
    .line 608142
    const-class v1, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    monitor-enter v1

    .line 608143
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608144
    sput-object v2, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608145
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608146
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608147
    new-instance p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 608148
    move-object v0, p0

    .line 608149
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608150
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608151
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 608141
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 608131
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 608132
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608133
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x52c6a98e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 608135
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/ENm;

    .line 608136
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 608137
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;

    .line 608138
    iget-object p1, v1, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;->a:Ljava/lang/String;

    move-object v1, p1

    .line 608139
    iget-object p1, p4, LX/ENm;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 608140
    const/16 v1, 0x1f

    const v2, -0x31352231

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608134
    const/4 v0, 0x1

    return v0
.end method
