.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;",
        "LX/EJv;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/7j6;

.field public final e:LX/0hy;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610511
    const-class v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    const-string v1, "places_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 610512
    const v0, 0x7f03128e

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610514
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610515
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610516
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->d:LX/7j6;

    .line 610517
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->e:LX/0hy;

    .line 610518
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 610519
    return-void
.end method

.method private a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 610513
    new-instance v0, LX/EJu;

    invoke-direct {v0, p0, p1, p2}, LX/EJu;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;
    .locals 7

    .prologue
    .line 610490
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    monitor-enter v1

    .line 610491
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610492
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610493
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610494
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610495
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v4

    check-cast v4, LX/7j6;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 610496
    move-object v0, p0

    .line 610497
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610498
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610499
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 610501
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 610502
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 610503
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610504
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 610505
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    .line 610506
    :goto_0
    invoke-virtual {p0, p2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610507
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setVisibility(I)V

    .line 610508
    return-void

    .line 610509
    :cond_0
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 610510
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setProductPrice(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610489
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 610486
    check-cast p2, LX/0Px;

    check-cast p3, LX/1Ps;

    .line 610487
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610488
    new-instance v1, LX/EJv;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v2

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/EJv;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x65cffabb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610478
    check-cast p1, LX/0Px;

    check-cast p2, LX/EJv;

    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;

    .line 610479
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v2, v1

    .line 610480
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v4, v1

    .line 610481
    iget-object v1, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object p0, v1

    .line 610482
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iget-object p3, p2, LX/EJv;->a:Landroid/view/View$OnClickListener;

    invoke-static {v2, v1, p3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;Landroid/view/View$OnClickListener;)V

    .line 610483
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iget-object v2, p2, LX/EJv;->b:Landroid/view/View$OnClickListener;

    invoke-static {v4, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;Landroid/view/View$OnClickListener;)V

    .line 610484
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iget-object v2, p2, LX/EJv;->c:Landroid/view/View$OnClickListener;

    invoke-static {p0, v1, v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;Landroid/view/View$OnClickListener;)V

    .line 610485
    const/16 v1, 0x1f

    const v2, -0x4b383223

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 610476
    check-cast p1, LX/0Px;

    .line 610477
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 610470
    check-cast p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;

    const/4 v1, 0x0

    .line 610471
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610472
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610473
    iget-object v0, p4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    move-object v0, v0

    .line 610474
    invoke-virtual {v0, v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610475
    return-void
.end method
