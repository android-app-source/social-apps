.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8cw;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/EJ9;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f400000    # -6.0f

    .line 611017
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    .line 611018
    iput v1, v0, LX/1UY;->c:F

    .line 611019
    move-object v0, v0

    .line 611020
    iput v1, v0, LX/1UY;->b:F

    .line 611021
    move-object v0, v0

    .line 611022
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EJ9;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610982
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 610983
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->e:LX/EJ9;

    .line 610984
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->f:LX/1V0;

    .line 610985
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "LX/8cw;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 611003
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->e:LX/EJ9;

    const/4 v1, 0x0

    .line 611004
    new-instance v2, LX/EJ8;

    invoke-direct {v2, v0}, LX/EJ8;-><init>(LX/EJ9;)V

    .line 611005
    sget-object v3, LX/EJ9;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EJ7;

    .line 611006
    if-nez v3, :cond_0

    .line 611007
    new-instance v3, LX/EJ7;

    invoke-direct {v3}, LX/EJ7;-><init>()V

    .line 611008
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EJ7;->a$redex0(LX/EJ7;LX/1De;IILX/EJ8;)V

    .line 611009
    move-object v2, v3

    .line 611010
    move-object v1, v2

    .line 611011
    move-object v0, v1

    .line 611012
    iget-object v1, v0, LX/EJ7;->a:LX/EJ8;

    iput-object p2, v1, LX/EJ8;->a:LX/CzL;

    .line 611013
    iget-object v1, v0, LX/EJ7;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 611014
    move-object v0, v0

    .line 611015
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 611016
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->d:LX/1Ua;

    sget-object v5, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;
    .locals 6

    .prologue
    .line 610992
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    monitor-enter v1

    .line 610993
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610994
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610995
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610996
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610997
    new-instance p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EJ9;->a(LX/0QB;)LX/EJ9;

    move-result-object v4

    check-cast v4, LX/EJ9;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;-><init>(Landroid/content/Context;LX/EJ9;LX/1V0;)V

    .line 610998
    move-object v0, p0

    .line 610999
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611000
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611001
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 611023
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 610991
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610987
    check-cast p1, LX/CzL;

    .line 610988
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 610989
    check-cast v0, LX/8cw;

    .line 610990
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->a(LX/8cw;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 610986
    const/4 v0, 0x0

    return-object v0
.end method
